#include "ga_base_custom_environment.hpp"

#include <iostream>
#include <string>
#include <iostream>
#include <fstream>
#include <vector>

#include "ga_foundry.hpp"

namespace data {


//  map [object_id, <object weight, num. objects>]

std::unordered_map<int, std::pair<int, int>> objs =
{
		{0, {175, 3270}},
		{1, {145, 3300}},
		{2, {65, 3300}},
		{3, {55, 3300}},
		{4, {95, 3300}},
		{5, {75, 3300}},
		{6, {195, 3300}},
		{7, {20, 3300}},
		{8, {125, 3300}},
		{9, {50, 3300}}
};

/*
std::unordered_map<int, std::pair<int, int>> objs =
{
		{0, {175, 127}},
		{1, {145, 130}},
		{2, {65, 130}},
		{3, {55, 130}},
		{4, {95, 130}},
		{5, {75, 130}},
		{6, {195, 130}},
		{7, {20, 130}},
		{8, {125, 130}},
		{9, {50, 130}}
};
*/
/*
std::unordered_map<int, std::pair<int, int>> objs =
{
		{0, {175, 59227}},
		{1, {145, 58329}},
		{2, {65, 53327}},
		{3, {55, 53229}},
		{4, {95, 53429}},
		{5, {75, 53526}},
		{6, {195, 57022}},
		{7, {20, 52322}},
		{8, {125, 58229}},
		{9, {50, 52026}}
};
*/
/*
std::unordered_map<int, std::pair<int, int>> objs =
{
		{0, {175, 63}},
		{1, {145, 65}},
		{2, {65, 65}},
		{3, {55, 65}},
		{4, {95, 65}},
		{5, {75, 65}},
		{6, {195, 65}},
		{7, {20, 65}},
		{8, {125, 65}},
		{9, {50, 65}}
};
*/
/*
std::unordered_map<int, std::pair<int, int>> objs =
{
		{0, {175, 327}},
		{1, {145, 330}},
		{2, {65, 330}},
		{3, {55, 330}},
		{4, {95, 330}},
		{5, {75, 330}},
		{6, {195, 330}},
		{7, {20, 330}},
		{8, {125, 330}},
		{9, {50, 330}}
};
*/

constexpr int W = 650;
constexpr int H = 5085;
constexpr int N = 10;
constexpr int UB = 15;
}  // namespace data

class GACustomEnvironmentDef : public GABaseCustomEnvironment {
public:
	void initGenes(std::vector<int>& x) override
	{
		x = initializeIndividual(data::UB, data::N, data::H, data::W, data::objs);
		//mutation1(x, data::N, data::H, data::W, data::objs);
		//mutation2(x, data::N, data::H, data::W, data::objs);
	}

  std::vector<int> mutate(const std::vector<int>& x) override
	{
		std::vector<int> xnew = x;
		mutation1(xnew, data::N, data::H, data::W, data::objs);
		mutation2(xnew, data::N, data::H, data::W, data::objs);
		return xnew;
	}

  std::vector<int> crossover(const std::vector<int>& x, const std::vector<int>& y) override
	{
		std::vector<int> xnew;
		xnew = recombination(x, y, data::N, data::H, data::W, data::objs);
		mutation1(xnew, data::N, data::H, data::W, data::objs);
		mutation2(xnew, data::N, data::H, data::W, data::objs);
		return xnew;
	}

  virtual double objectiveFunction(const std::vector<int>& x) override
  {
		double currentCost = getIndividualValue(x, data::N, data::H, data::W, data::objs);
		double penalty = getPenalty(x, data::N, data::H, data::W, data::objs);
		const double currentCostDiff = currentCost - penalty;
		return -currentCostDiff;
	}
};

// the class factories
extern "C" GABaseCustomEnvironment* create() {
    return new GACustomEnvironmentDef();
}

extern "C" void destroy(GABaseCustomEnvironment* p) {
    delete p;
}
