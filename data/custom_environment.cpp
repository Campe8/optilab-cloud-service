#include "ga_base_custom_environment.hpp"

#include <iostream>
#include <string>
#include <iostream>
#include <fstream>
#include <vector>

#include "ga_foundry.hpp"

namespace data {

std::unordered_map<int, std::pair<int, int>> objs =
{
		{0, {175, 63}},
		{1, {145, 65}},
		{2, {65, 65}},
		{3, {55, 65}},
		{4, {95, 65}},
		{5, {75, 65}},
		{6, {195, 65}},
		{7, {20, 65}},
		{8, {125, 65}},
		{9, {50, 65}}
};

constexpr int W = 650;
constexpr int POP = 20;
constexpr int H = 100;
constexpr int N = 10;
constexpr int UB = 15;
}  // namespace data

class GACustomEnvironmentDef : public GABaseCustomEnvironment {
public:
	void initGenes(std::vector<int>& x) override
	{
		x = initializeIndividual(data::UB, data::N, data::H, data::W, data::objs);
		mutation1(x, data::N, data::H, data::W, data::objs);
		mutation2(x, data::N, data::H, data::W, data::objs);
	}

  std::vector<int> mutate(const std::vector<int>& x) override
	{
		std::vector<int> xnew = x;
		mutation1(xnew, data::N, data::H, data::W, data::objs);
		mutation2(xnew, data::N, data::H, data::W, data::objs);
		return xnew;
	}

  std::vector<int> crossover(const std::vector<int>& x, const std::vector<int>& y) override
	{
		std::vector<int> xnew;
		xnew = recombination(x, y, data::N, data::H, data::W, data::objs);
		mutation1(xnew, data::N, data::H, data::W, data::objs);
		mutation2(xnew, data::N, data::H, data::W, data::objs);
		return xnew;
	}

  virtual double objectiveFunction(const std::vector<int>& x) override
  {
		double currentCost = getIndividualValue(x, data::N, data::H, data::W, data::objs);
		double penalty = getPenalty(x, data::N, data::H, data::W, data::objs);
		const double currentCostDiff = currentCost - penalty;
		return -currentCostDiff;
	}
};

// the class factories
extern "C" GABaseCustomEnvironment* create() {
    return new GACustomEnvironmentDef();
}

extern "C" void destroy(GABaseCustomEnvironment* p) {
    delete p;
}
