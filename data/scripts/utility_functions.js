// Self-invoking anonymous function expression: pg. 179
(function (window, undefined) {

  // Cross-Browser Event Utilities

  // When a particular user-agent doesn't support a feature
  // that you want to use, don't test for that particular
  // user-agent (browser detection), test for the existence
  // of the feature (feature detection) (pg. 123, 329)
  function wireEvent(obj, eventName, callback, capture) {

    if (window.addEventListener) {

      // W3C DOM Event Model (pg. 458)
      // Event propagation (pg. 463)
      obj.addEventListener(eventName, callback, capture);

    } else {

      // IE 8 or less (pg. 459):
      obj.attachEvent("on" + eventName, callback);
    }

  }

  // You would want to call this when using older 
  // versions of IE and you'd do it from the 
  // window’s unload event.  This helps to mitigate
  // a well-known memory leak issue in IE.
  function unwireEvent(obj, eventName, callback, capture) {

    if (window.removeEventListener) {

      // W3C DOM Event Model:
      obj.removeEventListener(eventName, callback, capture);

    } else {

      // IE 8 or less:
      obj.detachEvent("on" + eventName, callback);
    }
  }

  function cancelEvent(evt) {
    // (pg. 464)
    if (evt.stopPropagation) {
      // DOM Event Standard
      evt.preventDefault();  // stop the event
      evt.stopPropagation(); // don't bubble/capture
    } else {

      // IE 8 Event Model
      evt.returnValue = false;  // stop the event
      evt.cancelBubble = true;  // don't bubble the event
    }
  }

  // Expose just what we want to the global scope:

  // Create a new, empty object on the fly (pg. 117)...
  var o = {};

  // Map our functions to new properties of the new object (pg. 122):
  o.wireEvent = wireEvent;
  o.unwireEvent = unwireEvent;
  o.cancelEvent = cancelEvent;

  // Expose it as a new property of the global window obejct:
  window.Utilities = o;

}(window));  // Scope Chain (pg. 55-56)








