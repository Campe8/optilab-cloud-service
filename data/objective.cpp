
#include "ga_environment_objective_fcn.hpp"

#include <algorithm>
#include <array>
#include <cassert>
#include <cmath>
#include <cstddef>
#include <cstdio>
#include <functional>
#include <iostream>
#include <map>
#include <memory>
#include <set>
#include <thread>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <vector>

class CustomObjectiveFcn : public GAObjectiveFcn {
public:

double objectiveFunction(const std::vector<int>& x) override {

static const std::vector<std::pair<int, int>> cities =
{
    {160, 20}, {200, 60}, {180, 80}
};

int size = x.size();
double distance = 0;
for (int i = 0; i < size-1; ++i)
{
    auto left = cities[x[i]].first - cities[x[i+1]].first;
    auto right = cities[x[i]].second - cities[x[i+1]].second;
    const auto diff = abs(left - right);
    distance += diff != 0 ? sqrt(diff) : 0;
}

// Wrap back
auto left = cities[x[size-1]].first - cities[x[0]].first;
auto right = cities[x[size-1]].second - cities[x[0]].second;
const auto diff = abs(left - right);
distance += diff != 0 ? sqrt(diff) : 0;

return distance;
}
};

// the class factories
extern "C" GAObjectiveFcn* create() {
    return new CustomObjectiveFcn();
}

extern "C" void destroy(GAObjectiveFcn* p) {
    delete p;
}
