#pragma once

#include <iostream>
#include <cstdlib>
#include <algorithm>
#include <cmath>
#include <climits>
#include <vector>
#include <utility>
#include <map>
#include <cassert>
#include <unordered_map>
#include <unordered_set>

using Individual = std::vector<int>;
using Population = std::vector<Individual>;

bool myfunction(const std::pair<int, int>& i, const std::pair<int, int>& j) { return (i.first <j.first); }

/// Params:
/// - map [object_id, <object weight, num. objects>]
/// - weight W of crucible in Kg
/// - metal utilization in %
/// Return:
/// - number of heats
/// - expected maximum heat utilization
std::pair<int, double> estimateHeats(const std::unordered_map<int,
  std::pair<int, int>>& objs,
  int crucibleWeight, double metalUtilization)
{
  int totalMetalNeeded = 0;
  for (const auto& it : objs)
  {
    totalMetalNeeded += it.second.first * it.second.second;
  }

  int estimatedH = 1;
  double metal = 0;
  do
  {
    metal += metalUtilization * crucibleWeight;

    // Done if the metal is enough to cover the total need
    if (metal >= totalMetalNeeded) break;

    // Increment the estimated cost/number of Heats
    estimatedH++;
  }
  while(true);
  // std::cout << "totalMetalNeeded " << totalMetalNeeded << std::endl;
  return {estimatedH, static_cast<double>(totalMetalNeeded) / (estimatedH * crucibleWeight)};
}  // estimateHeats

// map [object_id, <object weight, num. objects>]
double getPenalty(const Individual& individual, int N, int H, int W,
                  const std::unordered_map<int, std::pair<int, int>>& objs,
                  bool debug = false)
{
  static const int R = 1000;

  // Calculate the square difference of how many objects are required
  // vs how many objects are produced
  int totDiffSum = 0;
  for (int j = 0; j < N; ++j)
  {
    int diffSum = 0; //-objs.at(j).second;
    for (int i = 0; i < H; ++i)
    {
      diffSum += individual[i * N + j];
    }
    diffSum = diffSum - objs.at(j).second;
    totDiffSum += (diffSum * diffSum);
  }

  if (debug) std::cout << "equality fail " << totDiffSum << std::endl;

  double totalRatio = 0;
  for (int i = 0; i < H; ++i)
  {
    double sum = 0;
    for (int j = 0; j < N; ++j)
    {
      sum += (individual[i * N + j] * objs.at(j).first);
    }

    double ratio = std::max<double>((static_cast<double>(sum)/W) - 1, 0.0);
    totalRatio += (ratio != 0) ? (ratio * ratio) : 0;
  }
  if (debug) std::cout << "disequality fail " << totalRatio << std::endl;

  return R * (totDiffSum + totalRatio);
}  // getPenalty

void mutation1(Individual& individual, int N, int H, int W,
               const std::unordered_map<int, std::pair<int, int>>& objs)
{
  int ctr = 0;
  for (int j = 0; j < N; ++j)
  {
    int r_j = objs.at(j).second;
    while (true)
    {
      ctr++;

      // Get sum for all objects of the same type 'j' over all the heats
      int totObj_j = 0;
      for (int i = 0; i < H; ++i)
      {
        totObj_j += individual[i * N + j];
      }

      // Quantity for object j matched, constraint satified.
      // Break and try next constraint
      if (totObj_j == r_j) break;
      if (ctr > N*H) break;
      // Remaining capacity of crucible in each heat
      std::map<int, int> U;
      for (int i = 0; i < H; ++i)
      {
        // Get the sumof all objects at the given heat 'i'
        int sum = 0;
        for (int jj = 0; jj < N; ++jj)
        {
          sum += objs.at(jj).first * individual[i * N + jj];
        }
        U[W - sum] = i;
      }

      if (totObj_j > r_j)
      {
        // If more copies of the object j than requires are assigned,
        // pick the heat that occupies minimum (or negative) space in crucible,
        // i.e., the heat that has more "stuff" in it and decrease one element
        // in it
        for (const auto& it : U)
        {
          // U is ordered from min to max wight
          int candidate_heat = it.second;

          // Reduce the first non-zero element
          if (individual[candidate_heat * N + j] > 0)
          {
            individual[candidate_heat * N + j]--;
            break;
          }
        }
      }
      else if (totObj_j < r_j)
      {
        // If less copies of the given object are assigned,
        // check the heat that occupies maximum space in crucible,
        // i.e., the one with more "free" space, and increase on of the objects
        // in that heat
        const auto& it = U.rbegin();
        int candidate_heat = it->second;
        individual[candidate_heat * N + j]++;
      }
    } // while
  }
}  // mutation1

void mutation2(Individual& individual, int N, int H, int W,
               const std::unordered_map<int, std::pair<int, int>>& objs)
{
  std::map<int, int> U;
  for (int i = 0; i < H; ++i)
  {
    // Get the sum of all the objects in the heat
    int sum = 0;
    for (int j = 0; j < N; ++j)
    {
      sum += objs.at(j).first * individual[i * N + j];
    }

    U[W - sum] = i;
  }

  // Heat that leaves minimum (or negative) free space in crucible
  int minID = U.begin()->second;
  int minHeat = U.begin()->first;

  int loop = 0;
  std::unordered_map<int, std::unordered_set<int>> heatSet;
  while(minHeat < 0)
  {
    // As long as required weight exceeds crucible capacity...

    // 1 - identify a random object in that heat with non-zero assigned copies
    int numCopies = 0;
    int objId_j = 0;

    do {
      objId_j = rand() % N;
    }
    while (individual[minID * N + objId_j] <= 0);

    // heatSet.insert(minID);

    // Identify the maximum available space
    int maxID = U.rbegin()->second;

    int sumTempMin = 0;
    for (int j = 0; j < N; ++j)
    {
      sumTempMin += objs.at(j).first * individual[minID * N + j];
    }

    int sumTempMax = 0;
    for (int j = 0; j < N; ++j)
    {
      sumTempMax += objs.at(j).first * individual[maxID * N + j];
    }

    // Remove one assignment of that object from exceeded heat
    individual[minID * N + objId_j]--;

    // Add one assignment to that object to most available heat
    individual[maxID * N + objId_j]++;

    if (heatSet[minID].find(objId_j) != heatSet[minID].end() &&
        heatSet[maxID].find(objId_j) != heatSet[maxID].end())
    {
      // break;
    }
    heatSet[minID].insert(objId_j);
    heatSet[maxID].insert(objId_j);

    if (loop > /* 10 * */ H || heatSet.size() == H)
    {
       break;
    }
    loop++;

    // Repeat until all heats are satisfied
    U.clear();
    for (int i = 0; i < H; ++i)
    {
      // Get the sum of all the objects in the heat
      int sum = 0;
      for (int j = 0; j < N; ++j)
      {
        sum += objs.at(j).first * individual[i * N + j];
      }

      U[W - sum] = i;
    }

    // Most violated heat for next round in while loop
    minID = U.begin()->second;
    minHeat = U.begin()->first;

  }
}  // mutation2

Individual initializeIndividual(int upperBound, int N, int H, int W,
  const std::unordered_map<int, std::pair<int, int>>& objs)
{
  Individual individual(H * N);
  for (int j = 0; j < N; ++j)
  {
    int sum = 0;
    for (int i = 0; i < H; ++i)
    {
      int& x_i_j = individual[i * N + j];
      x_i_j = rand() % (upperBound + 1);
      sum += x_i_j;
    }

    // Normalization step
    for (int i = 0; i < H; ++i)
    {
      int& x_i_j = individual[i * N + j];
      int r_j = objs.at(j).second;
      x_i_j = static_cast<int>(round((r_j/static_cast<double>(sum)) * x_i_j));
    }
  }
  return individual;
}

// map [object_id, <object weight, num. objects>]
Population initialization(int populationSize, int upperBound, int N, int H,
  int W, const std::unordered_map<int, std::pair<int, int>>& objs)
{
  Population p;
  for (int k = 0; k < populationSize; ++k)
  {
    Individual individual(H * N, 0);
    for (int j = 0; j < N; ++j)
    {
      int sum = 0;
      for (int i = 0; i < H; ++i)
      {
        int& x_i_j = individual[i * N + j];
        x_i_j = rand() % (upperBound + 1);
        sum += x_i_j;
      }

      // Normalization step
      for (int i = 0; i < H; ++i)
      {
        int& x_i_j = individual[i * N + j];
        int r_j = objs.at(j).second;
        x_i_j = static_cast<int>(round((r_j/static_cast<double>(sum)) * x_i_j));
      }
    }

    mutation1(individual, N, H, W, objs);
    mutation2(individual, N, H, W, objs);

    p.push_back(individual);
  }

  return p;
}  // initialization

// map [object_id, <object weight, num. objects>]
double getIndividualValue(const Individual& individual, int N, int H,
  int W, const std::unordered_map<int, std::pair<int, int>>& objs)
{
  double bestCost = 0;
  double ratio = 0;
  for (int i = 0; i < H; ++i)
  {
    double sum = 0;
    for (int j = 0; j < N; ++j)
    {
      sum += individual[i * N + j] * objs.at(j).first;
    }
    ratio += sum / (1.0 * W);
  }
  return ratio/H;
}  // getIndividualValue

std::pair<int, double> updateBest(const Population& p, int N, int H,
  int W, const std::unordered_map<int, std::pair<int, int>>& objs)
{
  int bestIndividual = 0;
  double divH = 1.0/H;
  double bestCost = -std::numeric_limits<double>::max();
  for (int k = 0; k < static_cast<int>(p.size()); ++k)
  {
    const auto& individual = p[k];
    double currentCost = getIndividualValue(individual, N, H, W, objs);
    double penalty = getPenalty(individual, N, H, W, objs);

    currentCost -= penalty;
    if (currentCost > bestCost)
    {
      bestCost = currentCost;
      bestIndividual = k;
    }
  }

  return {bestIndividual, bestCost};
}  // updateBest

Individual recombination(const Individual& p1, const Individual& p2, int N,
  int H, int W, const std::unordered_map<int, std::pair<int, int>>& objs)
{
  Individual offspring(N * H, 0);

  for (int i = 0; i < H; ++i)
  {
    int sum1 = 0;
    int sum2 = 0;
    for (int j = 0; j < N; ++j)
    {
      sum1 += objs.at(j).first * p1[i * N + j];
      sum2 += objs.at(j).first * p2[i * N + j];
    }
    int U1 = W - sum1;
    int U2 = W - sum2;

    if ((U1 >= 0 && U2 >= 0) || (U1 <= 0 && U2 <= 0) || (U1 > 0 && U2 < 0))
    {
      // First parent has better metal utilization
      for (int j = 0; j < N; ++j)
      {
        // Assignment from first parent
        offspring[i * N + j] = p1[i * N + j];
      }
    }
    else
    {
      // Second parent has better metal utilization
      for (int j = 0; j < N; ++j)
      {
        // Assignment from second parent
        offspring[i * N + j] = p2[i * N + j];
      }
    }
  }

  return offspring;
}  // recombination
