if [ "$1" == "linux" ]; then
  export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$(pwd)/build/install/lib
  export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$(pwd)/lib/linux/zeromq
  export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$(pwd)/lib/linux/protobuf_3_7_1
  export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$(pwd)/lib/linux/boost
  export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$(pwd)/lib/linux/gecode
  export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$(pwd)/lib/linux/ssl
  export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$(pwd)/lib/linux/scip
else
  export DYLD_LIBRARY_PATH=$DYLD_LIBRARY_PATH:$(pwd)/build/install/lib
  export DYLD_LIBRARY_PATH=$DYLD_LIBRARY_PATH:$(pwd)/lib/mac/zeromq
  export DYLD_LIBRARY_PATH=$DYLD_LIBRARY_PATH:$(pwd)/lib/mac/protobuf_3_7_1
  export DYLD_LIBRARY_PATH=$DYLD_LIBRARY_PATH:$(pwd)/lib/mac/boost
  export DYLD_LIBRARY_PATH=$DYLD_LIBRARY_PATH:$(pwd)/lib/mac/gecode
  export DYLD_LIBRARY_PATH=$DYLD_LIBRARY_PATH:$(pwd)/lib/mac/ssl
  export DYLD_LIBRARY_PATH=$DYLD_LIBRARY_PATH:$(pwd)/lib/mac/scip
fi
