import sys
sys.path.append("./parallelsdk")
from parallelsdk.client import *
from parallelsdk.scheduling_toolbox import *
from parallelsdk.SchedulingModels.Employee import Employee
from parallelsdk.SchedulingModels.Staff import Staff
from parallelsdk.routing_toolbox import *
from parallelsdk.connector_toolbox import *
from parallelsdk.RoutingModels.Vehicle import Vehicle
from parallelsdk.RoutingModels.Location import Location, Depot
import time
import random
import numpy as np

# Create a scheduler for the Employees problem
scheduler = BuildEmployeesScheduler('test')
scheduling = scheduler.get_instance()

### Team 1 ###
team_1 = Staff("Team_1", "575.123.4567", "TruckBest")
mario = Employee("Mario", "M", "587.234.1543", "TruckBest", "Boston")
john = Employee("John", "M", "878.232.5873", "TruckBest", "Boston")
cost_per_hour = 100
team_1.add_staff_member(mario, cost_per_hour)
team_1.add_staff_member(john, cost_per_hour)
day = 1
shift = 0
discount = 10
team_1.add_shift_preference(day, shift, discount)

### Team 2 ###
team_2 = Staff("Team_2", "565.254.9403", "OnlyTrucks")
maria = Employee("Maria", "F", "587.234.1543", "OnlyTrucks", "Boston")
luigi = Employee("Luigi", "M", "878.232.5873", "OnlyTrucks", "New Hampshire")
cost_per_hour = 80
team_2.add_staff_member(maria, cost_per_hour)
team_2.add_staff_member(luigi, cost_per_hour)
day = 1
shift = 2
discount = 5
team_2.add_shift_preference(day, shift, discount)

### Team 3 ###
team_3 = Staff("Team_3", "676.673.9000", "TheDeliveryCompany")
grace = Employee("Grace", "F", "587.234.1543", "TheDeliveryCompany", "Boston")
cost_per_hour = 70
team_3.add_staff_member(grace, cost_per_hour)
day = 2
day_aux = 1
shift = 0
shift_aux = 1
discount = 15
team_3.add_shift_preference(day, shift, discount)
team_3.add_shift_preference(day_aux, shift_aux, discount)

### Team 4 ###
team_4 = Staff("Team_4", "878.164.4898", "RoutingBest")
jack = Employee("Jack", "M", "587.234.1543", "RoutingBest", "Boston")
dick = Employee("Dick", "M", "587.234.1543", "RoutingBest", "Boston")
shaun = Employee("Shaun", "M", "587.234.1543", "RoutingBest", "Boston")
cost_per_hour = 120
team_4.add_staff_member(jack, cost_per_hour)
team_4.add_staff_member(dick, cost_per_hour)
team_4.add_staff_member(shaun, cost_per_hour)

# Set the schedule
schedule_num_days = 5
schedule_num_shift = 3
scheduling.set_schedule_num_days(schedule_num_days)
scheduling.set_shift_per_day(schedule_num_shift)

# Add the staff/teams to schedule
scheduling.add_staff(team_1)
scheduling.add_staff(team_2)
scheduling.add_staff(team_3)
scheduling.add_staff(team_4)

# Create an optimizer and connect it to the Parallel back-end platform
optimizer = ParallelClient('127.0.0.1')
optimizer.connect()

# Run the scheduler-optimizer on the back-end server
optimizer.run_optimizer(scheduler)

# Create a VRP problem
vrp = BuildVRP("vrp_example")

# Build a converter scheduling -> routing and link the models
converter = BuildEmployeesSchedulerToVehicleRoutingConverter()
converter.connect(scheduling, vrp)

# Add vehicles and depots
team_1_vehicles = []
team_1_vehicles.append(Vehicle("team_1_v1", 0, 150))
team_1_vehicles.append(Vehicle("team_1_v2", 0, 100))
team_1_depot = np.random.uniform(0.0, 10.0, size=(2,))
converter.map_staff_vehicle(team_1, team_1_vehicles, team_1_depot)

team_2_vehicles = []
team_2_vehicles.append(Vehicle("team_2_v1", 0, 70))
team_2_vehicles.append(Vehicle("team_2_v2", 0, 120))
team_2_vehicles.append(Vehicle("team_2_v3", 0, 150))
team_2_depot = np.random.uniform(0.0, 10.0, size=(2,))
converter.map_staff_vehicle(team_2, team_2_vehicles, team_2_depot)

team_3_vehicles = []
team_3_vehicles.append(Vehicle("team_3_v1", 0, 150))
team_3_vehicles.append(Vehicle("team_3_v2", 0, 200))
team_3_depot = np.random.uniform(0.0, 10.0, size=(2,))
converter.map_staff_vehicle(team_3, team_3_vehicles, team_3_depot)

team_4_vehicles = []
team_4_vehicles.append(Vehicle("team_4_v1", 0, 150))
team_4_vehicles.append(Vehicle("team_4_v2", 0, 150))
team_4_vehicles.append(Vehicle("team_4_v3", 0, 150))
team_4_vehicles.append(Vehicle("team_4_v4", 0, 150))
team_4_depot = np.random.uniform(0.0, 10.0, size=(2,))
converter.map_staff_vehicle(team_4, team_4_vehicles, team_4_depot)

# Map scheduling and locations to deliver items
for day in range(schedule_num_days):
    for shift in range(schedule_num_shift):
        locations = []
        for k in range(10):
            locations.append(Location(position=np.random.uniform(0.0, 10.0, size=(2,)), demand=random.randint(5,15)))
        converter.map_shift_location(day, shift, locations)

converter.run(optimizer)

optimizer.disconnect()

converter.print_scheduling()
