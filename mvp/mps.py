from parallelsdk.client import ParallelClient
from parallelsdk.mp_toolbox import *

model = MPModel('test')

# The following is not needed but shows the performance difference between solvers.
# For example, try change SCIP with CBC and see the difference in performance (lower)
model.set_package_type(OptimizerPackageType.SCIP)

model.load_model_from_file('/home/ubuntu/OptiLab/data/bell5.mps', 'bell5')

optimizer = ParallelClient('3.19.30.245')
optimizer.connect()

optimizer.run_optimizer(model)

optimizer.disconnect()

# Print objective
model.get_objective_value()
