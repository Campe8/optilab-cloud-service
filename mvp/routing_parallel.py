import sys
sys.path.append("./parallelsdk")
from parallelsdk.client import *
from parallelsdk.routing_toolbox import *
import time
import random
import numpy as np

# Create a VRP problem
vrp = BuildVRP("vrp_example")

# Add depot location
vrp.AddDepot( np.random.uniform(0.0, 10.0, size=(2,)) )

# Add locations and vehicles
for k in range(10):
    vrp.AddLocation( position=np.random.uniform(0.0, 10.0, size=(2,)), demand=random.randint(5,10))

for k in range(3):
    vrp.AddVehicle( name="vehicle_%d" %(k), load=0.0, capacity=30)

# Calculate the distance matrix
vrp.InferDistanceMatrix()

# Create an optimizer and connect it to the Parallel back-end platform
optimizer = ParallelClient('127.0.0.1')
optimizer.connect()

optimizer.run_optimizer(vrp)

optimizer.disconnect()

solution = vrp.get_solution()
for route in solution:
    print("Route: " + str(route.get_route()))
    print("Total distance: " + str(route.get_total_distance()))
