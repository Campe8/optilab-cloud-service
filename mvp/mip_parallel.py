from parallelsdk.client import ParallelClient
from parallelsdk.mp_toolbox import *

##########################################
###         MODEL DECLARATION          ###
##########################################

### Create model ###
model = MPModel('mip_example')

### Variables ###
inf = Infinity()
x = model.IntVar(0.0, inf, 'x')
y = model.IntVar(0.0, inf, 'y')

### Constraints ###
# x + 7y <= 17.5
c1 = model.Constraint(x + 7*y <= 17.5)

# x <= 3.5
c2 = model.Constraint(x <= 3.5)

### Objective ###
# Maximize x + 10 * y
model.Objective(x + 10*y, maximize=True)

##########################################
##########################################
##########################################

optimizer = ParallelClient('127.0.0.1')
optimizer.connect()

optimizer.run_optimizer(model)

optimizer.disconnect()

# Print objective
model.get_objective_value()
