import sys
sys.path.append("./parallelsdk")
from parallelsdk.client import *
from parallelsdk.evolutionary_toolbox import *
from parallelsdk.EvolutionaryModels.Chromosome import Chromosome, ChromosomeType
from parallelsdk.EvolutionaryModels.GeneticEnvironment import GeneticEnvironment, StandardEnvironment, CustomEnvironment

# Create a new empty genetic model
evolution = BuildGeneticModel('test')

# Get the instance the genetic algorithm will run on
model = evolution.get_instance()

# Create the environment for the genetic algorithm
env = model.build_custom_environment()
env.add_path_to_environment('/home/federicocampeotto/personal/evolutionary_data/custom_environment.cpp')
env.add_path_to_environment('/home/federicocampeotto/personal/evolutionary_data/ga_foundry.hpp')

chromosome_name = 'x'
chromosome_length = 1000
chromosome_type = ChromosomeType.CHROMO_INT
chromo = model.build_chromosome(chromosome_name, chromosome_length, chromosome_type)

# Foundry problem chromosome
chromo.set_lower_bound(0)
chromo.set_upper_bound(15)

# Standard GA settings
model.set_num_generations(100)
model.set_population_size(20)
model.set_stall_max_generations(10)
model.set_mutation_rate(0.2)
model.set_crossover_fraction(0.7)
model.set_random_seed(0)
model.set_timeout_seconds(120)
model.set_stall_best(0.0001)
model.set_multi_thread(False)

# Set foundry GA settings.
# Note: the objective function is part of the custom environment
model.set_environment(env)
model.set_chromosome(chromo)

# Create an optimizer and connect it to the Parallel back-end platform
optimizer = ParallelClient('127.0.0.1')
optimizer.connect()

# Run the evolutionary-optimizer on the back-end server
optimizer.run_optimizer(evolution)

# Run the evolutionary-optimizer on the back-end server
optimizer.run_optimizer(evolution)

model.get_objective_value()
