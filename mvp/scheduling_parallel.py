import sys
sys.path.append("./parallelsdk")
from parallelsdk.client import *
from parallelsdk.scheduling_toolbox import *

# Create a scheduler for the Employees problem
scheduler = BuildEmployeesScheduler('test')

# Get the instance the scheduler will run on
model = scheduler.get_instance()

mario = model.add_employee("mario", "male", "123.456.7890")
luigi = model.add_employee("luigi", "male", "123.456.7890")
clara = model.add_employee("clara", "female", "123.456.7890")
maria = model.add_employee("maria", "female", "123.456.7890")
#print(mario)

# Set the number of days to schedule
num_days = 5
model.set_shift_span(num_days)

# Set the number of shifts per day.
# In this example, 3 shifts of 8 hours each
num_shifts = 3
model.set_shift_per_day(num_shifts)

# Add shift preferences
# Mario would like to have the second shift of the third day
mario_day = 3
mario_shift = 2
model.add_shift_preference("mario", mario_day, mario_shift)

# Create an optimizer and connect it to the Parallel back-end platform
optimizer = ParallelClient('127.0.0.1')
optimizer.connect()

# Run the scheduler-optimizer on the back-end server
optimizer.run_optimizer(scheduler)

optimizer.disconnect()

mario.print_shift()
