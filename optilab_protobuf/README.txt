While programming, you may want to build the protobuf files
without building the whole system (e.g., to enable smart search on IDEs).
To build protobuf from proto file, run the following:
export DYLD_LIBRARY_PATH=$DYLD_LIBRARY_PATH:/Users/federico/optilab-cloud-service/lib/mac/protobuf_3_12_2
../bin/mac/protobuf/protoc -I=. --cpp_out=. <proto_file>.proto
