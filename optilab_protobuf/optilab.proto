syntax = "proto3";

import "google/protobuf/any.proto";

package optilab;

enum RequestType
{
   GET = 0;
   POST = 1;
   PUT = 2;
   DELETE = 3;
   UNKNOWN = 4;
}

enum ReplyStatus
{
  // Reply contains a valid message
  OK = 0;

  // Reply contains an error message
  ERROR = 100;
}

enum PkgType {
  UNDEF_PKG_TYPE = 0;
  OR_TOOLS = 1;
  GECODE = 2;
  OPEN_GA = 3;
  SCIP = 4;
}

enum FrameworkType {
  UNDEF_FRAMEWORK_TYPE = 0;
  
  // The following:
  // - OR
  // - CONNECTORS
  // - BACKEND_SERVICE
  // are old frameworks and they are deprecated
  OR = 1;
  CONNECTORS = 2;
  BACKEND_SERVICE = 3;
  EC = 4;
  
  // Routing toolbox
  ROUTING = 5;
  
  // Scheduling toolbox
  SCHEDULING = 6;
  
  // Operation Research Mathematical Programming toolbox
  OR_MP = 7;
  
  // Evolutionary strategies toolbox
  EVOLUTIONARY = 8;
  
  // Data and ports toolbox
  DATA_AND_PORTS = 9;
  
  // CP toolbox
  CONSTRAINT_PROGRAMMING = 10;
}

// Request message sent from client to workers
message OptilabRequestMessage {
  enum Type {
    Wait = 0;
    EngineEvent = 1;
    InformationalMessage = 2;
  }

  // Type of message
  Type type = 1;

  // REST request type
  RequestType requestType = 2;

  // Information contained in the message
  string messageInfo = 3;

  // Unique identifier for this message
  int32 messageId = 4;

  // Package type is optional and it is used
  // to "cast" to solver-specific type of messages
  PkgType pkgType = 5;

  // Framework is used to address the request
  // to the specific framework back-end in case back-ends
  // are different depending on the current framework
  FrameworkType frameworkType = 6;

  // Details of this message (specific message information).
  // Specific request messages will be encoded as Any message.
  // For more information, see PackFrom(...), UnpackTo(...), and Is(...):
  // https://developers.google.com/protocol-buffers/docs/proto3#any
  google.protobuf.Any details = 7;
}

// Reply message sent from workers to clients
message OptiLabReplyMessage {
  enum Type {
    Solution = 0;
    InformationalMessage = 1;
    ServiceLogin = 2;
    ServiceLogoff = 3;
    Metrics = 4;
    Metadata = 5;
    EngineProcessCompleted = 6;
  }

  // Type of reply, e.g., Solution
  Type type = 1;

  // Reply status, e.g., OK
  ReplyStatus replyStatus = 2;

  // Information encoded in this message
  string messageInfo = 3;

  // Type of package this reply is coming from
  PkgType pkgType = 4;

  // Unique identifier for this message
  int32 messageId = 5;

  // Details of this message (specific message information).
  // Specific replies messages will be encoded as Any message.
  // For more information, see PackFrom(...), UnpackTo(...), and Is(...):
  // https://developers.google.com/protocol-buffers/docs/proto3#any
  google.protobuf.Any details = 6;
}

//-----------------------------
// Request messages
//-----------------------------

// ==== ALL - FRAMEWORKS ==== //

// Wait requests for engines
message OptimizerWaitReq
{
  // Wait timeout in msec (-1 means no timeout)
  int32 waitTimeout = 1;
}

// ==== BACKEND SERVICE - FRAMEWORKS ==== //

message BackendServiceEngineReq
{
  enum ServiceClassType
  {
    SC_CP = 0;
    SC_MIP = 1;
    SC_COMBINATOR = 2;
    SC_GA = 3;
    SC_VRP = 4;
    SC_SCHEDULING = 5;
  }

  // Engine request descriptor (content of the request)
  string serviceDescriptor = 1;

  // Engine class type,
  // i.e., the type of engine receiving this request
  ServiceClassType serviceClassType = 2;

  // Type of framework this request is addressed to
  // e.g., OR, EC, etc.
  FrameworkType engineFrameworkType = 3;
}

// ==== OR/GA - FRAMEWORK ==== //

// Lifetime management for optimizer requests.
// According to the request type, it can create
// a new optimizer or delete an existing one.
// @note optimizer identifier is contained
// in the messageInfo field
message CreateOptimizerReq
{
   enum EngineType
   {
        CP = 0;
        MIP = 1;
        GA = 2;
        VRP = 3;
        SCHEDULING = 4;
        EVOLUTIONARY = 5;
        DATA_AND_PORTS = 6;
        CONSTRAINT_PROGRAMMING = 7;
   }

   // Type of engine/optimizer to create
   EngineType engineType = 1;
}

// Load/run model request according to the type or request
message RunModelReq
{
  // Actual model to load and run
  // TODO change this with path to the model
  string jsonModel = 1;

  // Path(s) to model descriptor.
  // TODO use only this instead of JSON models serialized as strings
  repeated string pathToModelDescriptor = 2;
}

// Get solutions request
message ModelSolutionsReq
{
  // Number of requested solutions
  int32 numSolutions = 1;
}

// ==== CONNECTORS - FRAMEWORK ==== //

// Lifetime management for connector requests.
// @note connector identifier is contained
// in the messageInfo field
message CreateConnectorReq
{
   enum ConnectorType
   {
        COMBINATOR = 0;
   }

   // Type of connector,
   // e.g., COMBINATOR, SWITCH, etc.
   ConnectorType connectorType = 1;
}

// Load/run model connections according to the type or request
message ExecuteConnectorReq
{
  // Descriptor for the connector,
  // this contains the models to run.
  // TODO use path(s) to connection descriptors without inserting here
  // the full JSON models
  string connectionsDescriptor = 1;

  // TODO use these path(s) to connections descriptors
  repeated string pathToConnectionsDescriptor = 2;
}

//-----------------------------
// Reply messages
//-----------------------------

// Optimizer solution response
message OptimizerSolutionRep
{
  enum ModelSemantic
   {
        SATISFIED = 0;
        UNSATISFIED = 1;
        UNKNOWN = 2;
   }

   // String solution
   string solution = 1;

   // Semantics of the solution
   ModelSemantic semantic = 2;
}

message OptimizerMetricsRep
{
   // Metrics JSON object
   string metrics = 1;
}

message OptimizerMetadataRep
{
   // Metadata JSON object
   string metadata = 1;
}

// Service login reply.
// DEPRECATED
message ServiceLoginRep
{
  enum LoginResult
  {
    SUCCESS = 0;
  }

  LoginResult login = 1;
}

// Service logoff reply.
// DEPRECATED
message ServiceLogoffRep
{
  enum LogoffResult
  {
    SUCCESS = 0;
  }

  LogoffResult logoff = 1;
}
