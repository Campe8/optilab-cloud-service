//
// Copyright OptiLab 2020. All rights reserved.
//
// Base class for a CP instance.
//

#pragma once

#include <memory>  // for std::unique_ptr

#include "optilab_protobuf/constraint_model.pb.h"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace toolbox {

class SYS_EXPORT_CLASS CPInstance {
 public:
  using UPtr = std::unique_ptr<CPInstance>;
  using SPtr = std::shared_ptr<CPInstance>;

 public:
  CPInstance(const toolbox::ConstraintModelProto& model)
 : pCPModel(model)
 {
 }

  inline const ConstraintModelProto& getConstraintModel() const noexcept
  {
    return pCPModel;
  }

 private:
  /// Protobuf containing the constraint programming model
  ConstraintModelProto pCPModel;
};

}  // namespace toolbox
}  // namespace optilab
