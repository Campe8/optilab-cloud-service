//
// Copyright OptiLab 2020. All rights reserved.
//
// A CP Sat optimizer is an asynchronous engine that
// holds a pipeline containing CP Sat processors.
//

#pragma once

#include <memory>  // for std::shared_ptr
#include <string>

#include "data_structure/metrics/metrics_register.hpp"
#include "engine/async_engine.hpp"
#include "engine/processor_pipeline.hpp"
#include "cp_toolbox/cp_instance.hpp"
#include "cp_toolbox/cp_sat_processor.hpp"
#include "cp_toolbox/cp_utilities.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace toolbox {

class SYS_EXPORT_CLASS CPSatOptimizer : public AsyncEngine<cpengine::CPWork::SPtr> {
 public:
  using SPtr = std::shared_ptr<CPSatOptimizer>;

 public:
  CPSatOptimizer(const std::string& engineName, cpengine::CPResult::SPtr result,
                 MetricsRegister::SPtr metricsRegister);

  /// Initializes this optimizer with the given model
  void loadEnvironment(CPInstance::UPtr instance);

  /// Runs this optimizer on the loaded model
  void runOptimizer(CPInstance::UPtr instance);

  /// Interrupts the solving processes on this optimizer.
  /// Returns true if the solver was successfully interrupted.
  /// Returns false otherwise
  bool interruptOptimizer();

 protected:
  ProcessorPipeline::SPtr buildPipeline(int pipeline = 0) override;

 private:
  using Work = cpengine::CPWork;
  using BaseClass = AsyncEngine<Work::SPtr>;

 private:
  /// Pointer to the GA results
  cpengine::CPResult::SPtr pResult;

  /// Pointer to the instance of the processor in the pipeline
  CPSatProcessor::SPtr pCPSatProcessor;

  /// Pointer to the metrics register given to each work task processed
  /// by each processor
  MetricsRegister::SPtr pMetricsRegister;
};

}  // namespace toolbox
}  // namespace optilab
