//
// Copyright OptiLab 2020. All rights reserved.
//
// Solver class for Constraint Programming.
//

#pragma once

#include <memory>  // for std::shared_ptr

#include "cp_toolbox/cp_utilities.hpp"
#include "system/system_export_defs.hpp"

#include "optilab_protobuf/constraint_model.pb.h"

namespace optilab {
namespace toolbox {

class SYS_EXPORT_CLASS CPSolver {
 public:
  using SPtr = std::shared_ptr<CPSolver>;

 public:
  CPSolver() = default;

  virtual ~CPSolver() = default;

  /// Returns the status of the solver
  virtual cpengine::CPResult::OptimizerStatus getStatus() noexcept = 0;

  /// Runs the solver on the given model and returns the status.
  /// The solution can be retrieved by calling the "getSolution" method
  virtual cpengine::CPResult::OptimizerStatus solve(const ConstraintModelProto& model) = 0;

  /// Returns the solutions stored after running the "solve" method
  virtual const toolbox::ConstraintSolutionProto& getSolution() noexcept = 0;
};

}  // namespace toolbox
}  // namespace optilab
