//
// Copyright OptiLab 2020. All rights reserved.
//
// Base class for a Constraint Programming engine.
//

#pragma once

#include "engine/engine.hpp"

#include <memory>  // for std::shared_ptr
#include <string>

#include "cp_toolbox/cp_instance.hpp"
#include "cp_toolbox/cp_utilities.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace toolbox {

class SYS_EXPORT_CLASS CPEngine {
 public:
  using SPtr = std::shared_ptr<CPEngine>;

 public:
  virtual ~CPEngine() = default;

  /// Registers the given instance/environment the evolution should run on.
  /// @note this method does not run on the environment.
  /// @throw std::runtime_error if this model is called while the engine is running
  virtual void registerInstance(CPInstance::UPtr instance) = 0;

  /// Notifies the optimizer on a given EngineEvent
  virtual void notifyEngine(const cpengine::CPEvent& event) = 0;

  /// Wait for the current jobs in the queue to finish.
  /// @note if "timeoutSec" is -1, waits for all jobs in the queue to complete
  virtual void engineWait(int timeoutMsec) = 0;

  /// Shuts down the engine
  virtual void turnDown() = 0;
};

}  // namespace toolbox
}  // namespace optilab
