#include "cp_toolbox/cp_sat_processor.hpp"

#include <cassert>
#include <exception>
#include <stdexcept>   // for std::runtime_error

#include <spdlog/spdlog.h>

#include "data_structure/metrics/metrics_register.hpp"
#include "engine/engine_constants.hpp"
#include "utilities/timer.hpp"

namespace optilab {
namespace toolbox {

CPSatProcessor::CPSatProcessor(const std::string& procName, cpengine::CPResult::SPtr result)
: BaseProcessor(procName),
  pSolver(nullptr),
  pLatencyInitMsec(0),
  pOptimizerLatencyMsec(0)
{
  pResult = std::dynamic_pointer_cast<cpengine::CPSatResult>(result);
  if (!pResult)
  {
    throw std::runtime_error("CPSatProcessor - empty pointer to result");
  }
}

CPSatProcessor::~CPSatProcessor()
{
  try
  {
    cleanup();
  }
  catch(...)
  {
    spdlog::error("CPSatProcessor - exception in destructor");
  }
}

void CPSatProcessor::loadInstanceAndCreateSolver(CPInstance::UPtr instance)
{
  if (instance == nullptr)
  {
    throw std::invalid_argument("CPSatProcessor - loadInstanceAndCreateSolver: "
        "empty instance");
  }

  // Create the CP model instance
  const auto& model = instance->getConstraintModel();
  if (!model.has_cp_sat_model())
  {
    throw std::invalid_argument("CPSatProcessor - loadInstanceAndCreateSolver: "
        "model is not a CP-Sat model");
  }

  const CPSatProto& cpSatModel = model.cp_sat_model();
  if (!cpSatModel.has_cp_model())
  {
    throw std::invalid_argument("CPSatProcessor - loadInstanceAndCreateSolver: "
        "CP-Sat model not set");
  }

  // Clear current model (if any)
  pCPModel.Clear();

  // Load the model
  pCPModel.CopyFrom(model);

  // Load the solver
  if (pSolver == nullptr)
  {
    try
    {
      pSolver = std::make_shared<ortools::CPSatSolver>();
    }
    catch(...)
    {
      const std::string errMsg = "CPSatProcessor - loadInstanceAndCreateSolver: "
          "error initializing the solver";
      spdlog::error(errMsg);
      throw;
    }
  }

  pResult->modelName = pCPModel.model_id();
}  // loadEnvironmentAndCreateSolver

bool CPSatProcessor::interruptSolver()
{
  if (pSolver == nullptr)
  {
    return false;
  }

  /// TODO register a callback in the OR-Tools solver to interrupt
  /// the search "on-demand"
  return true;
}  // interruptSolver

void CPSatProcessor::cleanup()
{
  pCPModel.Clear();
  pSolver.reset();
}  // cleanup

void CPSatProcessor::processWork(Work work)
{
  if (work->workType == cpengine::CPWork::kRunOptimizer)
  {
    runSolver(work);
  }
  else
  {
    spdlog::warn("CPSatProcessor - processWork: unrecognized work type");
  }
}  // processWork

void CPSatProcessor::runSolver(Work& work)
{
  if (!pSolver)
  {
    const std::string errMsg = "CPSatProcessor - runSolver: no solver available";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  timer::Timer timer;

  if (work->cpInstance != nullptr)
  {
    loadInstanceAndCreateSolver(std::move(work->cpInstance));
  }
  runSolverImpl();

  const auto wallclockMsec = timer.getWallClockTimeMsec();
  work->metricsRegister->setMetric(jsonmetrics::OPTIMIZER_LATENCY_MSEC, wallclockMsec);
}  // runSolver

void CPSatProcessor::runSolverImpl()
{
  // Start the timer
  timer::Timer optimizerTimer;

  try
  {
    // Run the optimizer
    pSolver->solve(pCPModel);
  }
  catch(std::exception& e)
  {
    spdlog::error("GAProcessor - runSolverImpl: "
        "runOptimizer exception: " + std::string(e.what()));

    // Cleanup state
    cleanup();

    // Re-throw
    throw;
  }
  catch (...)
  {
    spdlog::error("GAProcessor - runSolverImpl: runOptimizer unknown exception");

    // Cleanup state
    cleanup();

    // Re-throw
    throw;
  }

  // Get the overall wall-clock time
  pOptimizerLatencyMsec = optimizerTimer.getWallClockTimeMsec();

  // Store the solution
  storeSolution();
}  // runSolverImpl

cpengine::CPResult::OptimizerStatus CPSatProcessor::getOptimizerStatus() const
{
  if (!pSolver)
  {
    throw std::runtime_error("CPSatProcessor - getOptimizerStatus: no solver loaded");
  }

  return pSolver->getStatus();
}  // getOptimizerStatus

void CPSatProcessor::storeSolution()
{
  // Lock mutex on critical section
  LockGuard lock(pResultMutex);

  // Retrieve the solution from the environment
  pResult->status = getOptimizerStatus();
  pResult->solutionProto.CopyFrom(pSolver->getSolution());
}  // storeSolution

}  // namespace toolbox
}  // namespace optilab
