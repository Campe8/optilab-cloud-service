//
// Copyright OptiLab 2020. All rights reserved.
//
// Utilities for CP optimizers.
//

#pragma once

#include <algorithm>      // for std::sort
#include <cassert>
#include <functional>     // for std::function
#include <cstddef>        // for std::size_t
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <utility>        // for std::pair
#include <vector>

#include "cp_toolbox/cp_instance.hpp"
#include "data_structure/metrics/metrics_register.hpp"
#include "optilab_protobuf/constraint_model.pb.h"

namespace optilab {
namespace toolbox {

namespace cpengine {

enum class ConstraintProcessorType : int {
  /// Processor for CP Sat based on Google OR-TOOLS
  CP_CP_SAT = 0,

  /// Undefined CP processor type.
  /// @note this is the default if the processor type
  /// is not specified
  CP_UNDEF
};


struct SYS_EXPORT_STRUCT CPWork {
using SPtr = std::shared_ptr<CPWork>;

enum WorkType {
  /// Runs the optimizer on the loaded model
  kRunOptimizer,
  kWorkTypeUndef
};

CPWork(WorkType wtype, MetricsRegister::SPtr metReg)
: workType(wtype), metricsRegister(metReg)
{
}

WorkType workType;
MetricsRegister::SPtr metricsRegister;
CPInstance::UPtr cpInstance{nullptr};
};

class SYS_EXPORT_CLASS CPEvent {
 public:
  enum EventType {
    /// Starts the solving process
    kRunEngine,
    /// Kills any ongoing computation
    kInterruptEngine,
    /// Collects solutions
    kSolutions
  };

 public:
  explicit CPEvent(EventType aType)
  : pEventType(aType)
  {
  }

  inline EventType getType() const noexcept
  {
    return pEventType;
  }

  inline void setNumSolutions(int numSolutions) noexcept
  {
    pNumSolutions = numSolutions;
  }

  inline int getNumSolutions() const noexcept
  {
    return pNumSolutions;
  }

  inline void setInstance(CPInstance::UPtr instance) const noexcept
  {
    pCPInstance = std::move(instance);
  }

  inline CPInstance::UPtr getInstance() const noexcept
  {
    return std::move(pCPInstance);
  }

 private:
  /// Type of this event
  EventType pEventType;

  /// Number of solutions for "kSolutions" events
  int pNumSolutions{0};

  /// Possible instance type
  mutable CPInstance::UPtr pCPInstance{nullptr};
};

struct SYS_EXPORT_STRUCT CPResult {
  /// Status of the optimizer
  /// Result status
  enum OptimizerStatus {
    /// Solution is the global optimum
    OPTIMUM = 0,

    /// Feasible solution but not optimal
    FEASIBLE,

    /// Infeasible model, no result
    INFEASIBLE,

    /// Model proven to be unbounded
    UNBOUNDED,

    /// Error of some kind
    ABNORMAL,

    /// Invalid model
    MODEL_INVALID,

    /// Model not yet solved
    NOT_SOLVED,

    /// Unknown status
    UNKNOWN
  };

  using SPtr = std::shared_ptr<CPResult>;

  /// Name of the model
  std::string modelName;

  /// Status of the solution
  OptimizerStatus status{OptimizerStatus::NOT_SOLVED};

  virtual ~CPResult() = default;
};

struct SYS_EXPORT_STRUCT CPSatResult : public CPResult {
  using SPtr = std::shared_ptr<CPSatResult>;
  ConstraintSolutionProto solutionProto;

  void clear()
  {
    solutionProto.Clear();
    status = OptimizerStatus::NOT_SOLVED;
  }
};

}  // namespace cpengine

}  // namespace toolbox
}  // namespace optilab
