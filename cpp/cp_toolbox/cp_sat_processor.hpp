//
// Copyright OptiLab 2020. All rights reserved.
//
// Processor for constraint problems solved with CP-Sat solvers.
//

#pragma once

#include <cstdint>  // for uint64_t
#include <memory>   // for std::shared_ptr
#include <string>
#include <map>
#include <utility>  // for std::pair
#include <vector>

#include <boost/thread.hpp>

#include "engine/optimizer_processor.hpp"
#include "cp_toolbox/cp_instance.hpp"
#include "cp_toolbox/cp_utilities.hpp"
#include "or_tools_engine/cp_sat_solver.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace toolbox {

class SYS_EXPORT_CLASS CPSatProcessor :
public OptimizerProcessor<cpengine::CPWork::SPtr, cpengine::CPWork::SPtr>{
public:
  using SPtr = std::shared_ptr<CPSatProcessor>;

public:
  CPSatProcessor(const std::string& procName, cpengine::CPResult::SPtr result);

  ~CPSatProcessor();

  /// Loads the genetic algorithm instance
  void loadInstanceAndCreateSolver(CPInstance::UPtr instance);

  /// Interrupts the current engine execution (if any).
  /// The engine remains in a non-executable state until another environment is loaded.
  /// Returns true on success, false otherwise
  bool interruptSolver();

protected:
  using Work = cpengine::CPWork::SPtr;
  using BaseProcessor = OptimizerProcessor<Work, Work>;

  /// Lock type on the internal mutex
  using LockGuard = boost::lock_guard<boost::mutex>;

protected:
  void processWork(Work work) override;

  inline cpengine::CPSatResult::SPtr getResult() const noexcept { return pResult; }

private:
  /// CP result
  cpengine::CPSatResult::SPtr pResult{nullptr};

  /// Pointer to the actual CP solver
  ortools::CPSatSolver::SPtr pSolver{nullptr};

  /// CP-Sat model to run on
  ConstraintModelProto pCPModel;

  /// Mutex synch.ing on the state of the result
  boost::mutex pResultMutex;

  /// Variable keeping track of the latency due to the initialization process in msec
  uint64_t pLatencyInitMsec{0};

  /// Variable keeping track of the overall latency due to the optimization process in msec
  uint64_t pOptimizerLatencyMsec{0};

  /// Returns the status of the internal optimizer
  cpengine::CPResult::OptimizerStatus getOptimizerStatus() const;

  /// Triggers the execution of the solver
  /// @note this doesn't check whether an engine is currently running or not.
  /// It is responsibility of the caller to handle that logic
  void runSolver(Work& work);
  void runSolverImpl();

  /// Utility function: store current solution
  void storeSolution();

  /// Cleanup state
  void cleanup();
};

}  // namespace toolbox
}  // namespace optilab
