#include "cp_toolbox/cp_sat_optimizer.hpp"

#include <cassert>
#include <stdexcept>  // for std::invalid_argument
#include <utility>    // for std::move

#include <spdlog/spdlog.h>

namespace optilab {
namespace toolbox {

CPSatOptimizer::CPSatOptimizer(const std::string& engineName, cpengine::CPResult::SPtr result,
                               MetricsRegister::SPtr metricsRegister)
: BaseClass(engineName),
  pResult(result),
  pCPSatProcessor(nullptr),
  pMetricsRegister(metricsRegister)
{
  // Initialize the engine,
  // e.g., the internal pipeline
  BaseClass::init();

  if (!pResult)
  {
    throw std::invalid_argument("CPSatOptimizer - empty pointer to result");
  }

  if (!pMetricsRegister)
  {
    throw std::invalid_argument("CPSatOptimizer - empty pointer to the metrics register");
  }
}

void CPSatOptimizer::loadEnvironment(CPInstance::UPtr instance)
{
  // Load the environment into the optimizer
  assert(pCPSatProcessor);
  pCPSatProcessor->loadInstanceAndCreateSolver(std::move(instance));
}  // loadEnvironment

void CPSatOptimizer::runOptimizer(CPInstance::UPtr instance)
{
  if (!isActive())
  {
    const std::string errMsg = "CPSatOptimizer - runOptimizer: optimizer is not active";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  Work::SPtr work = std::make_shared<Work>(cpengine::CPWork::WorkType::kRunOptimizer,
                                           pMetricsRegister);

  if (instance)
  {
    work->cpInstance = std::move(instance);
  }

  pushTask(work);
}  // runOptimizer

bool CPSatOptimizer::interruptOptimizer()
{
  assert(pCPSatProcessor);
  return pCPSatProcessor->interruptSolver();
}  // interruptOptimizer

ProcessorPipeline::SPtr CPSatOptimizer::buildPipeline(int)
{
  ProcessorPipeline::SPtr pipeline = std::make_shared<ProcessorPipeline>();
  pCPSatProcessor = std::make_shared<CPSatProcessor>(getEngineName(), pResult);
  pipeline->pushBackProcessor(pCPSatProcessor);
  return pipeline;
}  // buildPipeline

}  // namespace toolbox
}  // namespace optilab
