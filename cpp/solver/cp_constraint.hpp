//
// Copyright OptiLab 2019. All rights reserved.
//
// Class encapsulating a CP constraint
//

#pragma once

#include <cstdint>  // for int64_t
#include <memory>   // for std::shared_ptr
#include <string>
#include <vector>

#include "solver/cp_variable.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

/**
 * Struct holding information about the argument of a constraint.
 * An argument can be either a scalar value, a reference to a cp variable,
 * or an array to the above.
 */
struct SYS_EXPORT_STRUCT CPArgument {
  enum ArgumentType {
    /// Scalar integer value
    AT_INT_VAL = 0,

    /// List of integers values
    AT_INT_VAL_LIST,

    /// Scalar double value
    AT_DOUBLE_VAL,

    /// List of double values
    AT_DOUBLE_VAL_LIST,

    /// Reference to a variable
    AT_VAR,

    /// List of variable references
    AT_VAR_LIST,
  };

  static CPArgument buildIntValueArg(int64_t val);
  static CPArgument buildIntValueArrayArg(const std::vector<int64_t>& valArray);
  static CPArgument buildDoubleValueArg(double val);
  static CPArgument buildDoubleValueArrayArg(const std::vector<double>& valArray);
  static CPArgument buildVarRefArg(CPVariable::SPtr var);
  static CPArgument buildVarRefArrayArg(const std::vector<CPVariable::SPtr>& varArray);

  inline bool isIntArgVal() const { return argType == ArgumentType::AT_INT_VAL ||
      argType == ArgumentType::AT_INT_VAL_LIST; }
  inline bool isDoubleArgVal() const { return argType == ArgumentType::AT_DOUBLE_VAL ||
      argType == ArgumentType::AT_DOUBLE_VAL_LIST; }
  inline bool isArgVar() const { return argType == ArgumentType::AT_VAR ||
      argType == ArgumentType::AT_VAR_LIST; }

  inline int getNumIntVals() const { return static_cast<int>(valIntList.size()); }
  inline int getNumDoubleVals() const { return static_cast<int>(valDoubleList.size()); }
  inline int getNumVars() const { return static_cast<int>(varList.size()); }

  /// Returns the integer value at the given index (0-based).
  /// @note throws out_of_range on wrong index
  int64_t getIntValAt(int idx) const;

  /// Returns the double value at the given index (0-based).
  /// @note throws out_of_range on wrong index
  double getDoubleValAt(int idx) const;

  /// Returns the variable at the given index (0-based).
  /// @note throws out_of_range on wrong index
  CPVariable::SPtr getVarAt(int idx) const;

  /// Returns true if the argument has only one variable
  bool hasOneVar() const;

  /// Returns true if the argument has only one integer value (integer value, list of
  /// integers of size 1, variable with a singleton integer domain)
  bool hasOneIntValue() const;

  /// Returns true if the argument has only one double value (double value, list of
  /// doubles of size 1, variable with a singleton double domain)
  bool hasOneDoubleValue() const;

  /// Returns the variable of the argument.
  /// @note throws if "hasOneVar(...)" returns false
  CPVariable::SPtr getVariable() const;

  /// Returns the integer value of the argument.
  /// @note throws if "hasOneIntValue(...)" returns false
  int64_t getIntValue() const;

  /// Returns the double value of the argument.
  /// @note throws if "hasOneDoubleValue(...)" returns false
  double getDoubleValue() const;

  /// Type of this argument
  ArgumentType argType;

  /// List of integers scalar values
  std::vector<int64_t> valIntList;

  /// List of double scalar values
  std::vector<double> valDoubleList;

  /// List of the variables
  std::vector<CPVariable::SPtr> varList;
};

class SYS_EXPORT_CLASS CPConstraint {
 public:
  enum PropagationType {
    PT_DEFAULT = 0,
    PT_BOUNDS,
    PT_DOMAIN
  };

  using SPtr = std::shared_ptr<CPConstraint>;

 public:
  virtual ~CPConstraint() = default;

  /// Returns the type of this constraint, e.g., eq, lin_eq, etc.
  inline const std::string& getType() { return pType; }

  /// Returns the list of argument of this constraint
  inline const std::vector<CPArgument>& getArgumentList() const { return pArgumentList; }

  //// Returns the propagation type used on this constraint
  inline PropagationType getPropagationType() const { return pPropagationType; }

 protected:
  /// Creates a constraint according to the following arguments:
  /// - type: type of constraint, e.g., "eq" for equality constraint
  /// - name: and, element, or, etc.
  /// - list of arguments: variables in the constraint scope and additional arguments
  /// - propagation type.
  /// @note throws std::invalid_argument if the string "type" is empty
  CPConstraint(const std::string& type, const std::vector<CPArgument>& args,
               PropagationType propType);

 private:
  /// Type of propagation required for this constraint
  PropagationType pPropagationType;

  /// Constraint's type
  std::string pType;

  /// Constraint's arguments
  std::vector<CPArgument> pArgumentList;
};

}  // namespace optilab
