//
// Copyright OptiLab 2019. All rights reserved.
//
// Interface for building combinators.
// The builder represents the "catalog" of primitives that can be combined together
// to form complex combinator search strategies.
// For more information see
// Search Combinators, by Schrijvers et al. or follow the link:
// https://arxiv.org/abs/1203.1095.
//

#pragma once

#include <memory>  // for std::shared_ptr
#include <string>
#include <vector>

#include "solver/cp_variable.hpp"
#include "solver/search_combinator.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

/**
 * Search combinator builder class.
 * Each CP solver specific package should implement its own builder based on its specific
 * combinator instances
 */
class SYS_EXPORT_CLASS SearchCombinatorBuilder {
 public:
  using SPtr = std::shared_ptr<SearchCombinatorBuilder>;

 public:
  virtual ~SearchCombinatorBuilder() = default;

  /// Returns the default variable selection strategy to be used by this combinator builder
  virtual std::string getDefaultVariableSelectionStrategy() const = 0;

  /// Returns the default value selection strategy to be used by this combinator builder
  virtual std::string getDefaultValueSelectionStrategy() const = 0;

  /// Returns a "base heuristic" combinator for a constraint problem.
  /// This heuristic is expressed in terms of "variable selection" strategy and
  /// "value selection" strategy from the given input list of "CPVariable" variables.
  /// @note the selection strategies depend on the specific supported solvers
  virtual SearchCombinator::SPtr buildBaseSearchCombinator(
      const std::vector<CPVariable::SPtr>& variablesList, const std::string& varSelectionStrategy,
      const std::string& valSelectionStrategy) = 0;

  /// Returns an AND combinator, i.e., a combinator that performs the AND-sequential
  /// composition of all combinators given as input argument.
  /// And-sequential means, intuitively, that solutions are found by performing all the
  /// sub-searches expressed by the list of combinators given as input argument,
  /// sequentially down one branch of the search tree.
  /// @note throws std::invalid_argument if the input list is empty
  virtual SearchCombinator::SPtr buildAndCombinator(
      const std::vector<SearchCombinator::SPtr>& combList) = 0;

  /// Returns an OR combinator, i.e., a combinator that performs the disjunctive combination of
  /// the sub-searches expressed by the list of combinators given as input argument.
  /// A solution is found using ANY of the sub-searches.
  /// @note throws std::invalid_argument if the input list is empty
  virtual SearchCombinator::SPtr buildOrCombinator(
      const std::vector<SearchCombinator::SPtr>& combList) = 0;
};

}  // namespace optilab
