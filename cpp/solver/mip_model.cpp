#include "solver/mip_model.hpp"

#include <spdlog/spdlog.h>

#include <stdexcept>  // for std::invalid_argument

#include "model/model_constants.hpp"

namespace optilab
{
MIPModel::MIPModel(const Model::SPtr& model)
    : pModel(model), pModelObjectSearch(nullptr)
{
  if (!pModel)
  {
    throw std::invalid_argument("MIPModel - pointer to Model is NULL");
  }

  // Set the class type of the model
  const auto classType = model->getModelClassType();
  switch (classType)
  {
    case Model::ClassType::MIP:
      pProblemType = ProblemType::PT_MIP;
      break;
    case Model::ClassType::IP:
      pProblemType = ProblemType::PT_IP;
      break;
    case Model::ClassType::LP:
      pProblemType = ProblemType::PT_LP;
      break;
    default:
      throw std::runtime_error(
          "MIPModel - unrecognized optimization model problem type");
  }

  // Get the name of the model
  pModelName = pModel->getModelName();

  // Get the format of the model
  const auto& modelFormat = pModel->getModelFormat();
  if (modelFormat.empty() || modelFormat == jsonmodel::OPTILAB_MODEL_FORMAT)
  {
    pModelInputFormat = ModelInputFormat::INPUT_FORMAT_OPTILAB;
  }
  else
  {
    if (modelFormat == jsonmodel::MPS_MODEL_FORMAT)
    {
      pModelInputFormat = ModelInputFormat::INPUT_FORMAT_MPS;
    }
    else
    {
      throw std::runtime_error(
          "MIPModel - unrecognized optimization model format type:" +
          modelFormat);
    }
  }
}

MIPModel::MIPModel(const LinearModelSpecProto& model)
    : pModelInputFormat(ModelInputFormat::INPUT_FORMAT_PROTOBUF),
      pModel(nullptr)
{
  switch (model.class_type())
  {
    case LinearModelSpecProto_ClassType::LinearModelSpecProto_ClassType_IP:
      pProblemType = ProblemType::PT_IP;
      break;
    case LinearModelSpecProto_ClassType::LinearModelSpecProto_ClassType_MIP:
      pProblemType = ProblemType::PT_MIP;
      break;
    case LinearModelSpecProto_ClassType::LinearModelSpecProto_ClassType_LP:
      pProblemType = ProblemType::PT_LP;
      break;
    default:
      throw std::runtime_error(
          "MIPModel - unrecognized optimization model problem type");
  }
  pModelName = model.model_id();
}

SolverModel::ModelInputFormat MIPModel::getModelInputFormat() const noexcept
{
  return pModelInputFormat;
}  // getModelInputFormat

void MIPModel::initializeModel()
{
  switch (pModelInputFormat)
  {
    case ModelInputFormat::INPUT_FORMAT_OPTILAB:
      initializeWithOptiLabModelFormat();
      break;
    case ModelInputFormat::INPUT_FORMAT_MPS:
      initializeWithLegacyModelFormat();
      break;
    default:
      throw std::runtime_error(
          "MIPModel - unrecognized optimization model format");
  }
}  // initializeModel

void MIPModel::initializeModel(const LinearModelSpecProto& model)
{
  if (!model.has_model())
  {
    // Use MPS file descriptor
    pModel = std::make_shared<Model>();
    pModel->setLegacyModelDescriptor(model.model_path());

    // TODO check file extension to determine what model format is used.
    // Here it is assumed that legacy format implies MPS format
    pModelInputFormat = ModelInputFormat::INPUT_FORMAT_MPS;
    initializeWithLegacyModelFormat();
  }
  else
  {
    const OptModelProto& modelProto = model.model();

    // Add decision variables to the model
    addProtoVariables(modelProto);

    // Add constraints to the model
    addProtoConstraints(modelProto);

    /// Add objectives/objective functions
    addProtoObjective(modelProto);
  }
}  // initializeModel

const std::string& MIPModel::getLegacyModelData() const
{
  return pModel->getModelData();
}  // getLegacyModelData

const std::string& MIPModel::getLegacyModelDescriptor() const
{
  return pModel->getModelDescriptor();
}  // getLegacyModelDescriptor

void MIPModel::initializeWithOptiLabModelFormat()
{
  // Add decision variables to the model
  addVariables();

  // Add constraints to the model
  addConstraints();

  /// Add search strategy to the model
  addSearchStrategy();

  /// Add objectives/objective functions
  addObjective();
}  // initializeWithOptiLabModelFormat

void MIPModel::addProtoVariables(const OptModelProto& model)
{
  for (int idx = 0; idx < model.variable_size(); ++idx)
  {
    const OptVariableProto& var = model.variable(idx);
    /*
    spdlog::warn(
        "MIPModel: var " + var.name() + " lb: " +
        (var.lower_bound() < -10e10 ? "-inf"
                                    : std::to_string(var.lower_bound())) +
        " ub: " +
        (var.upper_bound() > 10e10 ? "inf"
                                   : std::to_string(var.upper_bound())) +
        " int: " + std::to_string(var.is_integer()) +
        " objective coeff: " + std::to_string(var.objective_coefficient()));
     */
    auto& MIPVar = pVarMap[var.name()];
    MIPVar = createVariableFromProto(var);
    if (MIPVar->isObjectiveVariable())
    {
      pObjectiveVarList.push_back(MIPVar->getVarName());
    }
  }
}  // addProtoVariables

void MIPModel::addVariables()
{
  for (const auto& varIter : pModel->getVariableMap())
  {
    const std::string& varName = varIter.first;
    const auto& varPtr = varIter.second;
    if (pVarMap.find(varName) != pVarMap.end())
    {
      throw std::runtime_error("MIPModel - variable " + varName +
                               " has been declared more than once");
    }

    // Create and add the variable to the model variable map
    auto& varMapPtr = pVarMap[varName];
    varMapPtr = createVariable(varPtr);
    if (varMapPtr->isObjectiveVariable())
    {
      pObjectiveVarList.push_back(varName);
    }
  }
}  // addVariables

void MIPModel::addConstraints()
{
  for (const auto& conIter :
       pModel->getObjectList(ModelObject::ModelObjectType::CONSTRAINT))
  {
    // Create and add the constraint to the model constraint list
    ModelObjectConstraint::SPtr ptr =
        std::dynamic_pointer_cast<ModelObjectConstraint>(conIter);
    if (!ptr)
    {
      throw std::runtime_error(
          "MIPModel - model object cast to model object constraint failed");
    }

    pConstraintsList.push_back(createConstraint(ptr, pVarMap));
  }
}  // addConstraints

void MIPModel::addProtoConstraints(const OptModelProto& model)
{
  pConstraintsList.reserve(model.constraint_size());
  for (int idx = 0; idx < model.constraint_size(); ++idx)
  {
    const OptConstraintProto& con = model.constraint(idx);
    /*
    std::string vidx = "var. index = [";
    std::string cidx = "coeff. = [";
    for (int idx1 = 0; idx1 < con.var_index_size(); ++idx1)
    {
      vidx += (std::to_string(con.var_index(idx1)) + " ");
      cidx += (std::to_string(con.coefficient(idx1)) + " ");
    }
    vidx += "]";
    cidx += "]";
    spdlog::warn(
        "MIPModel: constraint " + con.name() + " lb: " +
        (con.lower_bound() < -10e10 ? "-inf"
                                    : std::to_string(con.lower_bound())) +
        " ub: " +
        (con.upper_bound() > 10e10 ? "inf"
                                   : std::to_string(con.upper_bound())) +
        " " + vidx + " " + cidx);
     */
    pConstraintsList.push_back(createConstraintFromProto(con, pVarMap));
  }
}  // addProtoConstraints

void MIPModel::addObjective()
{
  for (const auto& objIter :
       pModel->getObjectList(ModelObject::ModelObjectType::OBJECTIVE))
  {
    // Create and add the objective to the model constraint list
    ModelObjectObjective::SPtr ptr =
        std::dynamic_pointer_cast<ModelObjectObjective>(objIter);
    if (!ptr)
    {
      throw std::runtime_error(
          "MIPModel - model object cast to model object objective failed");
    }

    pObjectiveFunctionsList.push_back(createObjective(ptr, pVarMap));
  }
}  // addObjective

void MIPModel::addProtoObjective(const OptModelProto& model)
{
  pObjectiveFunctionsList.push_back(createObjectiveFromProto(model, pVarMap));
}  // addProtoConstraints

void MIPModel::addSearchStrategy()
{
  // List of search strategies
  // @note only ONE search strategy is currently supported
  const auto& searchList = pModel->getSearchList();

  // If there is no search strategy specified, perform only constraint
  // propagation
  if (searchList.empty())
  {
    return;
  }

  // Otherwise check that there is exactly one search strategy specified
  if (searchList.size() > 1)
  {
    spdlog::warn("MIPModel: only one root search object is supported");
  }

  ModelObject::SPtr modelObj = searchList.front();
  pModelObjectSearch = std::dynamic_pointer_cast<ModelObjectSearch>(modelObj);
  if (!pModelObjectSearch)
  {
    throw std::runtime_error(
        "MIPModel - addSearchStrategy: invalid cast to "
        "ModelObjectSearch from ModelObject");
  }
}  // addSearchStrategy

}  // namespace optilab
