//
// Copyright OptiLab 2019. All rights reserved.
//
// Class encapsulating a MIP constraint.
//

#pragma once

#include <limits>   // for std::numeric_limits
#include <memory>   // for std::shared_ptr
#include <string>
#include <unordered_map>
#include <utility>  // for std::pair
#include <vector>

#include "model/model_object_mip_constraint.hpp"
#include "solver/mip_variable.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_STRUCT MIPConstraint {
 public:
  using SPtr = std::shared_ptr<MIPConstraint>;
  using VarCoeffPair = std::pair<MIPVariable::SPtr, double>;
  using VarCoeffList = std::vector<VarCoeffPair>;
  using Iter = VarCoeffList::iterator;

 public:
  /// Default constructor
  MIPConstraint() = default;

  /// Constructor: creates a MIP constraint.
  /// @note throws std::runtime_error if the coeff variables cannot be found
  /// in the given variable map
  MIPConstraint(ModelObjectMIPConstraint::SPtr mipConstraintObject,
                const std::unordered_map<std::string, MIPVariable::SPtr>& varMap);

  inline const std::string& getConstraintId() const { return pConstraintId; }

  inline double getConstraintLowerBound() const { return pConstraintLb; }
  inline double getConstraintUpperBound() const { return pConstraintUb; }

  bool isConstraintLowerBoundInfinity() const;
  bool isConstraintUpperBoundInfinity() const;

  inline const VarCoeffList& getVarCoeffList() const { return pVarCoeffList; }

  Iter begin() {
    return pVarCoeffList.begin();
  }
  Iter end() {
    return pVarCoeffList.end();
  }

 private:
  /// Identifier for this constraint
  std::string pConstraintId;

  /// Lower bound on this constraint
  double pConstraintLb{std::numeric_limits<double>::max()};

  /// Upper bound on this constraint
  double pConstraintUb{std::numeric_limits<double>::lowest()};

  /// List of variable-coefficient pairs for this constraint
  VarCoeffList pVarCoeffList;

  /// Utility function: initialize this constraint
  void initConstraint(ModelObjectMIPConstraint::SPtr mipConstraintObject,
                      const std::unordered_map<std::string, MIPVariable::SPtr>& varMap);
};

}  // namespace optilab
