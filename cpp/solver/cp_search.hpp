//
// Copyright OptiLab 2019. All rights reserved.
//
// Base class for CP search strategies used by CP frameworks.
//

#pragma once

#include <memory>  // for std::shared_ptr

#include "solver/search_strategy.hpp"
#include "system/system_export_defs.hpp"

// Forward declarations
namespace optilab {

class CPSolver;

}  // namespace optilab

namespace optilab {

class SYS_EXPORT_CLASS CPSearch : public SearchStrategy {
 public:
  enum SearchType {
    GLOBAL_SEARCH = 0,
    LOCAL_SEARCH
  };

  using SPtr = std::shared_ptr<CPSearch>;

 public:
  CPSearch(SearchType searchType)
      : pSearchType(searchType)
  {
  }

  virtual ~CPSearch() = default;

  /// Returns the type of this search
  inline SearchType getSearchType() const { return pSearchType; }

  /// Returns true if this search is "propagation-only",
  /// i.e., if no tree-search/local search is required but only constraint propagation
  inline bool isPropagationOnly() const { return pPropagationOnlySearch; }

  /// Set this search as "propagation-only" search
  inline void setSearchAsPropagationOnly() { pPropagationOnlySearch = true; }

 private:
  /// Type of this search
  SearchType pSearchType;

  /// Flag indicating whether the search is constraint propagation only (i.e., no search at all),
  /// or an actual search
  bool pPropagationOnlySearch{false};
};

}  // namespace optilab
