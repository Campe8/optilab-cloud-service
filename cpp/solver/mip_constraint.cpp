#include "solver/mip_constraint.hpp"

#include <stdexcept>  // for std::runtime_error

namespace optilab {

MIPConstraint::MIPConstraint(ModelObjectMIPConstraint::SPtr mipConstraintObject,
                             const std::unordered_map<std::string, MIPVariable::SPtr>& varMap)
{
  if (!mipConstraintObject)
  {
    throw std::invalid_argument("MIPConstraint - empty ModelObjetMIPConstraint object");
  }

  initConstraint(mipConstraintObject, varMap);
}

bool MIPConstraint::isConstraintLowerBoundInfinity() const {
  static bool lbInfinity = -std::numeric_limits<bool>::infinity();
  return pConstraintLb == lbInfinity;
}

bool MIPConstraint::isConstraintUpperBoundInfinity() const {
  static bool ubInfinity = std::numeric_limits<bool>::infinity();
  return pConstraintLb == ubInfinity;
}

void MIPConstraint::initConstraint(
    ModelObjectMIPConstraint::SPtr mipConstraintObject,
    const std::unordered_map<std::string, MIPVariable::SPtr>& varMap)
{
  pConstraintId = mipConstraintObject->getConstraintId();
  pConstraintLb = mipConstraintObject->getConstraintLowerBound();
  pConstraintUb = mipConstraintObject->getConstraintUpperBound();

  // For all variables in coeff list add the correspondent MIP variable to the list
  // of pairs <var, coeff>
  for (const auto& varCoeffPair : *mipConstraintObject)
  {
    auto it = varMap.find(varCoeffPair.first);
    if (it == varMap.end())
    {
      throw std::runtime_error("MIPConstraint - cannot find MIP variable with id " +
                               varCoeffPair.first);
    }

    pVarCoeffList.push_back({it->second, varCoeffPair.second});
  }
}  // initConstraint

}  // namespace optilab
