//
// Copyright OptiLab 2019. All rights reserved.
//
// Base class for any search strategy used by any optimizer package.
// Search strategies are differentiated by their framework (e.g, CP, MIP, etc.),
// their heuristics (e.g., complete, local search, etc.), etc.
//

#pragma once

#include <memory>  // for std::shared_ptr

#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS SearchStrategy {
 public:
  using SPtr = std::shared_ptr<SearchStrategy>;

 public:
  SearchStrategy() = default;

  virtual ~SearchStrategy() = default;
};

}  // namespace optilab
