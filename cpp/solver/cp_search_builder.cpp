#include "solver/cp_search_builder.hpp"

#include <cassert>
#include <stdexcept>  // for std::invalid_argumnet
#include <vector>

#include "solver/cp_global_search.hpp"
#include "solver/cp_variable.hpp"

namespace {

optilab::SearchCombinator::SPtr buildBaseSearchCombinator(
    const optilab::SearchObject::SPtr& searchObj, const optilab::CPModel::SPtr& cpModel,
    const optilab::SearchCombinatorBuilder::SPtr& combinatorBuilder)
{
  optilab::BaseSearchObject* comb = static_cast<optilab::BaseSearchObject*>(searchObj.get());

  // Create the branching variable list
  std::vector<optilab::CPVariable::SPtr> varList;

  const auto& varMap = cpModel->getVariablesMap();
  for (const auto& varObj : *comb)
  {
    if (varMap.find(varObj.first) == varMap.end())
    {
      throw std::runtime_error("CPSearchBuilder - buildBaseSearchCombinator " +
                               varObj.first + " not found in the model");
    }

    const auto& var = varMap.at(varObj.first);
    if (varObj.second.empty())
    {
      varList.push_back(var);
    }
    else
    {
      varList.push_back(var->getSubVariable(varObj.second));
    }
  }

  const auto& varSelection = comb->getVariableSelectionStrategy();
  const auto& valSelection = comb->getValueSelectionStrategy();
  return combinatorBuilder->buildBaseSearchCombinator(varList, varSelection, valSelection);
}  // buildBaseSearchCombinator

}  // namespace

namespace optilab {

CPSearchBuilder::CPSearchBuilder(CPModel::SPtr cpModel,
                                 SearchCombinatorBuilder::SPtr combinatorBuilder)
: pCPModel(cpModel),
  pCombinatorBuilder(combinatorBuilder)
{
  if (!pCPModel)
  {
    throw std::invalid_argument("CPSearchBuilder - pointer to CPModel is NULL");
  }

  if (!pCombinatorBuilder)
  {
    throw std::invalid_argument("CPSearchBuilder - pointer to SearchCombinatorBuilder is NULL");
  }
}

CPSearch::SPtr CPSearchBuilder::build()
{
  // Check if the CP search is propagation only
  if (pCPModel->isPropagationOnlyModel())
  {
    // If so, create and return a "propagation-only" search
    return  buildPropagationOnlySearch();
  }

  // Get the ModelObject holding the information about the search strategy
  auto objSearch = pCPModel->getObjectSearch();
  if (!objSearch)
  {
    throw std::runtime_error("CPSearchBuilder - search object not specified");
  }

  // Get search type and build the correspondent CPSearch instance
  const auto searchType = objSearch->getSearchType();
  switch(searchType)
  {
    case ModelObjectSearch::SearchType::GLOBAL:
    {
      return buildGlobalSearch(objSearch);
    }
    default:
    {
      assert(searchType == ModelObjectSearch::SearchType::LOCAL);
      throw std::runtime_error("CPSearchBuilder - LOCAL search type not supported");
    }
  }
}  // build

CPSearch::SPtr CPSearchBuilder::buildGlobalSearch(const ModelObjectSearch::SPtr& objectSearch)
{
  // Start from the root of the search and recursively create the search object
  // using the combinator builder
  const auto rootId = objectSearch->getRootModelSearchObjectId();

  SearchCombinator::SPtr rootCombinator = buildSearchCombinator(rootId, objectSearch);

  // Create a global search strategy
  CPGlobalSearchStrategy::SPtr globalSearch =
      std::make_shared<CPGlobalSearchStrategy>(rootCombinator);

  return globalSearch;
}  // buildGlobalSearch

SearchCombinator::SPtr CPSearchBuilder::buildSearchCombinator(
    const std::string& combinatorId, const ModelObjectSearch::SPtr& objectSearch)
{
  auto searchObj = objectSearch->getSearchObject(combinatorId);
  if (!searchObj)
  {
    throw std::runtime_error("CPSearchBuilder - cannot find combinator object with id " +
                             combinatorId);
  }

  auto combinatoryType = searchObj->getSearchObjectType();
  switch(combinatoryType)
  {
    case SearchObject::SearchObjectType::BASE_SEARCH:
    {
      // Base case
      return buildBaseSearchCombinator(searchObj, pCPModel, pCombinatorBuilder);
    }
    case SearchObject::SearchObjectType::OR:
    {
      return buildOrCombinator(searchObj, objectSearch);
    }
    case SearchObject::SearchObjectType::AND:
    {
      return buildAndCombinator(searchObj, objectSearch);
    }
    default:
      throw std::runtime_error("CPSearchBuilder - combinator object type not recognized");
  }
}  // buildSearchCombinator

SearchCombinator::SPtr CPSearchBuilder::buildOrCombinator(
    const SearchObject::SPtr& combinator, const ModelObjectSearch::SPtr& objectSearch)
{
  OrObject* comb = static_cast<OrObject*>(combinator.get());

  std::vector<SearchCombinator::SPtr> combList;
  for (const auto& combId : comb->getSearchObjectList())
  {
    combList.push_back(buildSearchCombinator(combId, objectSearch));
  }

  return pCombinatorBuilder->buildOrCombinator(combList);
}  // buildOrCombinator

SearchCombinator::SPtr CPSearchBuilder::buildAndCombinator(
    const SearchObject::SPtr& combinator, const ModelObjectSearch::SPtr& objectSearch)
{
  OrObject* comb = static_cast<OrObject*>(combinator.get());

  std::vector<SearchCombinator::SPtr> combList;
  for (const auto& combId : comb->getSearchObjectList())
  {
    combList.push_back(buildSearchCombinator(combId, objectSearch));
  }

  return pCombinatorBuilder->buildAndCombinator(combList);
}  // buildAndCombinator

CPSearch::SPtr CPSearchBuilder::buildPropagationOnlySearch()
{
  // Create a BaseSearch combinator with no variables and default variable/value
  // selection strategy
  std::vector<CPVariable::SPtr> emptyVarList;
  auto searchCombinator = pCombinatorBuilder->buildBaseSearchCombinator(
      emptyVarList, pCombinatorBuilder->getDefaultVariableSelectionStrategy(),
      pCombinatorBuilder->getDefaultValueSelectionStrategy());

  // By default the propagation only search is a global search
  CPGlobalSearchStrategy::SPtr globalSearch =
      std::make_shared<CPGlobalSearchStrategy>(searchCombinator);

  // Set the search as propagation-only search
  globalSearch->setSearchAsPropagationOnly();

  return globalSearch;
}  // buildPropagationOnlySearch

}  // namespace optilab
