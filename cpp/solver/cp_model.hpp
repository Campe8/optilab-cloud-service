//
// Copyright OptiLab 2019. All rights reserved.
//
// Class representing a CP solver model.
//

#pragma once

#include <cstdint>  // for int64_t
#include <memory>   // for std::shared_ptr
#include <string>
#include <unordered_map>

#include "model/model.hpp"
#include "model/model_object_constraint.hpp"
#include "model/model_object_search.hpp"
#include "model/model_object_variable.hpp"
#include "solver/cp_constraint.hpp"
#include "solver/cp_search.hpp"
#include "solver/cp_variable.hpp"
#include "solver/solver_model.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS CPModel : public SolverModel {
 public:
  using SPtr = std::shared_ptr<CPModel>;

  /// Map of decision variables
  using VarMap = std::unordered_map<std::string, CPVariable::SPtr>;

 public:
  /// Constructor.
  /// @note throws std::invalid_argument if the given Model pointer is empty
  explicit CPModel(const Model::SPtr& model);

  virtual ~CPModel() = default;

  /// Returns the input format of this model
  ModelInputFormat getModelInputFormat() const noexcept override;

  /// Initializes the model, i.e., parses the input constructor model and
  /// creates CP variable, CP constraints, etc.
  void initializeModel();

  /// Returns the verbatim copy of the data on legacy model formats
  const std::string& getLegacyModelData() const;

  /// Returns the verbatim copy of the model on legacy model formats
  const std::string& getLegacyModelDescriptor() const;

  /// Returns true if this CPModel is a "propagation-only" model,
  /// i.e., if no search strategy is specified in the model.
  /// Returns false otherwise
  bool isPropagationOnlyModel() const { return pPropagationOnlyModel; }

  /// Returns the name of this model
  const std::string& getModelName() const { return pModelName; }

  const int64_t getNumberOfSolutionsToFind() const { return pNumSolutions; }

  /// Returns the reference to the internal map of variables in the model
  inline const VarMap& getVariablesMap() const { return pVarMap; }

  /// Returns the model object search, i.e., the model object describing the search strategy
  /// to be used to solve this model
  inline ModelObjectSearch::SPtr getObjectSearch() const { return pModelObjectSearch; }

 protected:
  typedef void (*ConstraintPoster) (CPConstraint::SPtr);

  /**
   * Registry of constraint poster.
   * The registry stores all the poster functions for posting constraint
   */
  class Registry {
   public:
    void add(const std::string& conName, ConstraintPoster poster);
    void post(CPConstraint::SPtr con);
    inline void cleanup() { pRegistry.clear(); }

   private:
    /// Actual constraint poster register
    std::unordered_map<std::string, ConstraintPoster> pRegistry;
  };

 protected:
  /// Initialize an OptiLab/non-legacy model format.
  /// @note this implementation parses the Json model and adds the following in order:
  /// 1 - variables: by calling "createVariable(...)";
  /// 2 - constraints: by calling "createConstraint(...)";
  /// 3 - search strategy: no-op;
  /// 4 - objective functions: by calling "createObjective(...)".
  virtual void initializeWithOptiLabModelFormat();

  /// Initialize a legacy model.
  /// @note this implementation is no-op.
  /// The underlying implementation of this MIP model should initialize legacy models
  /// according to the framework used.
  /// @note to get information about legacy models, use the following:
  /// - "getModelInputFormat(...)": to get the model format, e.g., MPS;
  /// - "getLegacyModelData(...)": to get the verbatim copy of the data;
  /// - "getLegacyModelDescriptor(...)" to get the verbatim copy of the model.
  virtual void initializeWithLegacyModelFormat() {};


  /// Registers the poster for the given constraint, identified by the constraint name/type
  virtual void registerConstraint(const std::string& name, ConstraintPoster poster);

  /// Creates a CPVariable (package-specific) given a ModelObjectVariable instance
  virtual CPVariable::SPtr createVariable(const ModelObjectVariable::SPtr& objVariable) = 0;

  /// Creates a CPConstraint (package-specific) given a ModelObjectConstraint instance
  virtual CPConstraint::SPtr createConstraint(const ModelObjectConstraint::SPtr& objConstraint) = 0;

  /// Registers the constraints supported by the specific package CP model into the
  /// constraint registry.
  /// For each supported constraint "constraint_name" this should be implement as follows:
  /// registerConstraint("constraint_name", p_constraint_name)
  /// where "p_constraint_name" is the ConstraintPoster function for the constraint
  /// "constraint_name"
  virtual void registerConstraints() = 0;

 private:
  /// Name of this model
  std::string pModelName;

  /// Input format for this model
  ModelInputFormat pModelInputFormat;

  /// Pointer to the base model
  Model::SPtr pModel;

  /// Number of solutions to find in this model
  int64_t pNumSolutions{0};

  /// Map of all the decision variables in the model
  VarMap pVarMap;

  /// Registry of constraint posters for all the constraint in this model
  Registry pConstraintRegistry;

  /// Model object encapsulating the search strategy to be used to solve this model
  ModelObjectSearch::SPtr pModelObjectSearch;

  /// Flag indicating whether this is a propagation only model or not.
  /// In other words, whether at least one search strategy is specified in the model or not
  bool pPropagationOnlyModel{false};

  /// Utility function: parses the model and adds decision variables to the model.
  /// @note all variables are stored in the variable map "pVarMap"
  void addVariables();

  /// Utility function: parses the model and adds constraints to the model
  void addConstraints();

  /// Utility function: adds the search strategy to the model
  void addSearchStrategy();
};

}  // namespace optilab
