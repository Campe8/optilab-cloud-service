//
// Copyright OptiLab 2019. All rights reserved.
//
// Base class for MIP search strategies used by MI{ frameworks.
//

#pragma once

#include <memory>  // for std::shared_ptr

#include "solver/search_strategy.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS MIPSearch : public SearchStrategy {
 public:
  using SPtr = std::shared_ptr<MIPSearch>;

 public:
  MIPSearch() = default;

  virtual ~MIPSearch() = default;
};

}  // namespace optilab
