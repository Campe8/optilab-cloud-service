//
// Copyright OptiLab 2019. All rights reserved.
//
// Class representing a MIP solver model.
// @note this model actually represents both integer and linear optimization
// problems. In other words, the same "MIP" model contains the information to
// determine whether the model should be solved as a MIP problem or as an LP
// problem.
//

#pragma once

#include <cstdint>  // for int64_t
#include <memory>   // for std::shared_ptr
#include <string>
#include <vector>

#include "model/model.hpp"
#include "model/model_object_search.hpp"
#include "model/model_object_variable.hpp"
#include "optilab_protobuf/linear_model.pb.h"
#include "solver/mip_constraint.hpp"
#include "solver/mip_objective.hpp"
#include "solver/mip_variable.hpp"
#include "solver/solver_model.hpp"
#include "system/system_export_defs.hpp"

namespace optilab
{
class SYS_EXPORT_CLASS MIPModel : public SolverModel
{
 public:
  enum ProblemType : int
  {
    /// Linear optimization problem
    PT_LP = 0,

    /// Integer programming problem
    PT_IP,

    /// Mixed integer optimization problem
    PT_MIP
  };

  using SPtr = std::shared_ptr<MIPModel>;

  /// Map of decision variables
  using VarMap = std::unordered_map<std::string, MIPVariable::SPtr>;

  /// List of objectives
  using ObjectiveList = std::vector<MIPObjective::SPtr>;

 public:
  /// Constructor.
  /// @note throws std::invalid_argument if the given Model pointer is empty
  explicit MIPModel(const Model::SPtr& model);

  /// Constructor
  explicit MIPModel(const LinearModelSpecProto& model);

  virtual ~MIPModel() = default;

  /// Returns the input format of this model
  ModelInputFormat getModelInputFormat() const noexcept override;

  /// Returns the optimization problem type of this model, i.e., MIP, LP, etc.
  inline ProblemType getProblemType() const { return pProblemType; }

  /// Initializes the model, i.e., parses the input constructor model and
  /// creates CP variable, CP constraints, etc.
  void initializeModel();

  /// Initializes the model parsing the given proto message.
  /// @note the given model MUST BE the same used for constructing this objeft
  void initializeModel(const LinearModelSpecProto& model);

  /// Returns the verbatim copy of the data on legacy model formats
  const std::string& getLegacyModelData() const;

  /// Returns the verbatim copy of the model on legacy model formats
  const std::string& getLegacyModelDescriptor() const;

  /// Returns the name of this model
  const std::string& getModelName() const { return pModelName; }

  /// Returns the reference to the internal map of variables in the model
  inline const VarMap& getVariablesMap() const { return pVarMap; }

  /// Returns the list of objectives
  inline const ObjectiveList& getObjectiveList() const
  {
    return pObjectiveFunctionsList;
  }

  /// Returns the model object search, i.e., the model object describing the
  /// search strategy to be used to solve this model
  inline ModelObjectSearch::SPtr getObjectSearch() const
  {
    return pModelObjectSearch;
  }

 protected:
  /// Get the pointer to the (original) model
  inline Model::SPtr getModel() const { return pModel; }

  /// Initialize an OptiLab/non-legacy model format.
  /// @note this implementation parses the Json model and adds the following in
  /// order: 1 - variables: by calling "createVariable(...)"; 2 - constraints:
  /// by calling "createConstraint(...)"; 3 - search strategy: no-op; 4 -
  /// objective functions: by calling "createObjective(...)".
  virtual void initializeWithOptiLabModelFormat();

  /// Initialize a legacy model.
  /// @note this implementation is no-op.
  /// The underlying implementation of this MIP model should initialize legacy
  /// models according to the framework used.
  /// @note to get information about legacy models, use the following:
  /// - "getModelInputFormat(...)": to get the model format, e.g., MPS;
  /// - "getLegacyModelData(...)": to get the verbatim copy of the data;
  /// - "getLegacyModelDescriptor(...)" to get the verbatim copy of the model.
  virtual void initializeWithLegacyModelFormat(){};

  /// Creates a MIPVariable (package-specific) given a ModelObjectVariable
  /// instance.
  /// @note the variable definition should be consistent with the class type of
  /// this model
  virtual MIPVariable::SPtr createVariable(
      const ModelObjectVariable::SPtr& objVariable) = 0;

  /// Creates  MIPVariable (package-specific) given a proto variable description
  virtual MIPVariable::SPtr createVariableFromProto(
      const OptVariableProto& protoVar)
  {
    return nullptr;
  }

  /// Creates a MIPConstraint (package-specific) given a ModelObjectConstraint
  /// instance and the map of all the variables in the model.
  /// @note the constraint definition should be consistent with the class type
  /// of this model
  virtual MIPConstraint::SPtr createConstraint(
      const ModelObjectConstraint::SPtr& objConstraint,
      const VarMap& varMap) = 0;

  /// Creates  MIPVariable (package-specific) given a proto constraint
  /// description
  virtual MIPConstraint::SPtr createConstraintFromProto(
      const OptConstraintProto& protoCon, const VarMap& varMap)
  {
    return nullptr;
  }

  /// Creates a MIPObjective (package-specific) given a ModelObjectObjective
  /// instance and the map of all the variables in the model.
  /// @note the objective definition should be consistent with the class type of
  /// this model
  virtual MIPObjective::SPtr createObjective(
      const ModelObjectObjective::SPtr& objObjective, const VarMap& varMap) = 0;

  /// Creates a MIPObjective (package-specific) given a the protobuf model
  /// and the map of all the variables in the model
  virtual MIPObjective::SPtr createObjectiveFromProto(
      const OptModelProto& model, const VarMap& varMap)
  {
    return nullptr;
  }

  inline const std::vector<std::string>& getObjectiveVarList() const noexcept
  {
    return pObjectiveVarList;
  }

 private:
  /// Type of optimization problem to be solver, i.e, LP, MIP, IP, etc.
  ProblemType pProblemType;

  /// Input format for this model
  ModelInputFormat pModelInputFormat;

  /// Name of this model
  std::string pModelName;

  /// Pointer to the base model
  Model::SPtr pModel;

  /// Map of all the decision variables in the model
  VarMap pVarMap;

  /// List of variable (names) part of the objective function
  std::vector<std::string> pObjectiveVarList;

  /// List of all the constraints in the model
  std::vector<MIPConstraint::SPtr> pConstraintsList;

  /// List of all the objective functions in the model
  ObjectiveList pObjectiveFunctionsList;

  /// Model object encapsulating the search strategy to be used to solve this
  /// model
  ModelObjectSearch::SPtr pModelObjectSearch;

  /// Utility function: parses the model and adds the variables to the model.
  /// @note all variables are stored in the variable map "pVarMap"
  void addVariables();

  /// Utility function: parses the model and adds constraints to the model
  void addConstraints();

  /// Utility function: adds the search strategy to the model
  void addSearchStrategy();

  /// Utility function: adds the objectives to the model
  void addObjective();

  /// Utility function: parses the model and adds the variables to the model.
  /// @note all variables are stored in the variable map "pVarMap"
  void addProtoVariables(const OptModelProto& model);

  /// Utility function: parses the model and adds constraints to the model
  void addProtoConstraints(const OptModelProto& model);

  /// Utility function: adds the objectives to the model
  void addProtoObjective(const OptModelProto& model);
};

}  // namespace optilab
