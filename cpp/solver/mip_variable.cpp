#include "solver/mip_variable.hpp"

#include <stdexcept>  // for std::runtime_error

namespace {

std::string getVariableIndexName(const std::string& baseName, const std::vector<int>& dims)
{
  std::string varName = baseName;
  for (auto dim : dims) {
    varName += "_" + std::to_string(dim);
  }

  return varName;
}  // getVariableIndexName

}  // end namespace

namespace optilab {

MIPVariable::MIPVariable(const std::string& name)
    : pVarName(name),
      pNumDim(0)
{
}

MIPVariable::MIPVariable(const std::string& name, int len)
    : pVarName(name),
      pNumDim(1)
{
  if (len < 1)
  {
    throw std::runtime_error("MIPVariable: Invalid array size");
  }

  pDims.push_back(len);
}

MIPVariable::MIPVariable(const std::string& name, const std::vector<int>& dims)
    : pVarName(name),
      pDims(dims),
      pNumDim(static_cast<int>(dims.size()))
{
  if (pNumDim < 2)
  {
    throw std::runtime_error("MIPVariable: Invalid number of dimensions for matrix");
  }
}

MIPVariable::SPtr MIPVariable::getVariable(const std::vector<int>& subscr)
{
  if (isScalar())
  {
    throw std::runtime_error("MIPVariable - invalid operand on scalar variable");
  }

  const std::string idxVarName = getVariableIndexName(pVarName, subscr);
  return getVariableFromIndex(idxVarName, subscr);
}  // getVariable

}  // namespace optilab
