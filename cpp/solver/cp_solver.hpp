//
// Copyright OptiLab 2019. All rights reserved.
//
// Solver class for CP frameworks.
//

#pragma once

#include <memory>  // for std::shared_ptr

#include "solver/solver.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS CPSolver : public Solver {
 public:
  /// Represent the status of the search after termination
  enum SolverStatus {
    /// For SAT problems this means "search complete"
    SS_OPT = 0,
    SS_SAT,
    SS_UNSAT,
    SS_UNBOUNDED,
    SS_UNSAT_OR_UNBOUNDED,
    SS_UNKNOWN,
    SS_ERROR,
    SS_NONE
  };

  /// Reason causing the status "SolverStatus"
  enum StatusReason {
    /// Solver run successfully
    SR_OK = 0,

    /// Timeout occurred
    SR_TIME,

    /// Memory problem
    SR_MEMORY,

    /// Limit in search
    SR_LIMIT,

    /// Other error
    SR_ERROR
  };

  using SPtr = std::shared_ptr<CPSolver>;

 public:
  CPSolver() = default;

  virtual ~CPSolver() = default;

  /// Returns the status of the solver
  virtual SolverStatus getStatus() { return pSolverStatus; }

  /// Returns the reason for the status given by "getStatus(...)"
  virtual StatusReason getStatusReason() { return pStatusReason; }

  /// Initializes the solver for the search
  virtual void initSearch() {};

  /// Executes cleanup code after search is completed
  virtual void cleanupSearch() {};

  /// Performs constraint propagation only.
  /// Returns true if the current variable assignment satisfies all constraints.
  /// Returns false otherwise
  virtual bool constraintPropagation() = 0;

  /// Runs the solver until a solution (next solution) is found.
  /// Returns true when there is a "next" solution, returns false otherwise.
  /// This method is supposed to be called within a loop, for example:
  /// while (nextSolution())
  /// {
  ///    use solution;
  /// }
  virtual bool nextSolution() = 0;

  /// Stops the (ongoing) search process and return
  virtual void interruptSearchProcess() = 0;

 protected:
  /// Sets the status of the solver
  inline void setSolverStatus(SolverStatus status) { pSolverStatus = status; }

  /// Sets the reason for the status on the solver
  inline void setSolverStatusReason(StatusReason reason) { pStatusReason = reason; }

 private:
  /// Status of the solving process
  SolverStatus pSolverStatus{SolverStatus::SS_UNKNOWN};

  /// Reason for the status of the solving process
  StatusReason pStatusReason{StatusReason::SR_OK};
};

}  // namespace optilab
