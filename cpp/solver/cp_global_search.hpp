//
// Copyright OptiLab 2019. All rights reserved.
//
// Class representing global search strategies,
// i.e., complete search strategies that fully explore a search tree (e.g., DFS)
// until an optimal solution, all solutions, the entire search tree,
// or a timeout has been reached.
//

#pragma once

#include <memory>  // for std::shared_ptr

#include "solver/cp_search.hpp"
#include "solver/search_combinator.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS CPGlobalSearchStrategy : public CPSearch
{
 public:
  using SPtr = std::shared_ptr<CPGlobalSearchStrategy>;

 public:
  /// Constructor.
  /// @note throws std::invalid_argument on empty pointer to a SearchCombinator
  CPGlobalSearchStrategy(SearchCombinator::SPtr heuristic);

  virtual ~CPGlobalSearchStrategy() = default;

  static bool isa(const CPSearch* cpSearch);
  static CPGlobalSearchStrategy* cast(CPSearch* cpSearch);
  static const CPGlobalSearchStrategy* cast(const CPSearch* cpSearch);

  /// Returns the search heuristic describing this search strategy.
  /// @note the search heuristic is represented as a search combinator
  inline SearchCombinator::SPtr getSearchHeuristic() const { return pSearchHeuristic; }

 private:
  /// Heuristic representing this search strategy
  SearchCombinator::SPtr pSearchHeuristic;
};

}  // namespace optilab
