//
// Copyright OptiLab 2019. All rights reserved.
//
// Generic class representing a solver model.
//

#pragma once

#include <memory>  // for std::shared_ptr

#include "system/system_export_defs.hpp"

namespace optilab
{
class SYS_EXPORT_CLASS SolverModel
{
 public:
  /// Input model format, i.e., the modeling language used to
  /// write the input model.
  /// The below methods can be used only on specific input model formats.
  /// For example, the method "getVariablesMap(...)" cannot be generally used
  /// if the input format is "INPUT_FORMAT_MPS"
  enum ModelInputFormat : int
  {
    /// MPS model format.
    /// For more info, see
    /// http://lpsolve.sourceforge.net/5.5/mps-format.htm
    INPUT_FORMAT_MPS = 0,

    /// FlatZinc model format.
    /// For more info, see
    /// https://www.minizinc.org/
    INPUT_FORMAT_FLATZINC,

    /// Standard OptiLab JSON input format
    INPUT_FORMAT_OPTILAB,

    /// Protobuf input format
    INPUT_FORMAT_PROTOBUF
  };

  using SPtr = std::shared_ptr<SolverModel>;

 public:
  SolverModel() = default;

  virtual ~SolverModel() = default;

  /// Returns the input format of this model
  virtual ModelInputFormat getModelInputFormat() const noexcept = 0;
};

}  // namespace optilab
