//
// Copyright OptiLab 2019. All rights reserved.
//
// Class encapsulating a CP variable
//

#pragma once

#include <cstdint>  // for int64_t
#include <memory>   // for std::shared_ptr
#include <string>
#include <utility>  // for std::pair
#include <vector>

#include "system/system_export_defs.hpp"

namespace optilab {

/**
 * Struct holding information about the domain of a CP variable.
 */
struct SYS_EXPORT_STRUCT CPDomain {

  /// Returns the Integer value corresponding to Boolean false/true
  static int boolToInt(bool val);

  /// Builds and returns a Boolean domain
  static CPDomain buildBooleanDomain();

  /// Builds and returns a singleton Boolean domain with specified value
  static CPDomain buildBooleanDomain(bool singletonVal);

  /// Builds and returns an interval domain: [lowerBound, upperBound].
  /// @note throws std::invalid_argument if lowerBound > upperBound
  static CPDomain buildIntDomain(int64_t lowerBound, int64_t upperBound);

  /// Builds and returns an interval domain: (lowerBound, upperBound).
  /// @note throws std::invalid_argument if lowerBound > upperBound
  static CPDomain buildContinuousDomain(double lowerBound, double upperBound);

  /// Builds and returns a set of Integer values domain.
  /// @note this method sorts and removes duplicates from the given list of values
  static CPDomain buildListOfValuesDomain(const std::vector<int64_t>& values);

  /// Builds and returns a set domain.
  /// @note a set domain is something like the following:
  /// { {}, {1}, {2}, {3}, {1, 2}, {1, 3}, {2, 3}, {1, 2, 3}}.
  /// @note not all back-end solvers support set domains.
  /// For example, Gecode solver supports set-domains whereas OR-Tools (currently) doesn't.
  /// @note the give list of values represents the set of elements used to build the power set.
  /// @note this method sorts and removes duplicates from the given list of values
  static CPDomain buildSetDomain(const std::vector<int64_t>& values);

  /// Builds and returns a domain with a set of intervals.
  /// For example, something like the following:
  /// [[1, 5], [7, 10], [15, 30]].
  /// @note throws std::invalid_argument if a pair the lower bound greater than the upper bound
  static CPDomain buildListOfRangesDomain(
      const std::vector<std::pair<int64_t, int64_t>>& listRange);

  /// Returns true if the domain is a singleton, returns false otherwise
  bool isSingleton() const;

  /// Returns true if the upper bound if +Inf. Returns false otherwise
  bool isUpperBoundInf() const;

  /// Returns true if the lower bound if -Inf. Returns false otherwise
  bool isLowerBoundInf() const;

  /// Indicates whether this domain is a set or not.
  /// A set domain is something like the following:
  /// { {}, {1}, {2}, {3}, {1, 2}, {1, 3}, {2, 3}, {1, 2, 3}}
  bool isSet;

  /// Indicates whether this domain is a Boolean domain or not
  bool isBool;

  /// Indicates whether this domain is described as a set of pairs of intervals or not.
  /// If this it true, "pIntValues" should contain 2*n Integer values where
  /// "n" is the number of intervals.
  /// For example, something like the following:
  /// [[1, 5], [7, 10], [15, 30]]
  bool isListOfRanges;

  /// Indicates whether this domain is described as a set of value.
  /// For example, something like the following:
  /// {1, 5, 3, 7, 10}
  bool isListOfValues;

  /// Indicates whether this domain is described as a single interval or not.
  /// For example, something like the following:
  /// [1, 100]
  bool isInterval;

  /// Vector of integer values describing the domain
  std::vector<int64_t> intValues;

  /// Vector of double values describing the domain
  std::vector<double> doubleValues;
};

class SYS_EXPORT_CLASS CPVariable {
 public:
  using SPtr = std::shared_ptr<CPVariable>;

 public:
  /// Creates a scalar variable with given name and domain
  CPVariable(const std::string& name, const CPDomain& domain);

  /// Creates an array of variables of given size and given domain.
  /// @note "len" must be greater than zero
  CPVariable(const std::string& name, const CPDomain& domain, int len);

  /// Creates an n-dimensional matrix of variables with given domain and with
  /// the given list of dimensions.
  /// @note dims size must be at least 2
  CPVariable(const std::string& name, const CPDomain& domain, const std::vector<int>& dims);

  virtual ~CPVariable() = default;

  /// Returns the name of this variable
  const std::string& getVarName() const { return pVarName; }

  /// Returns the domain of this variable.
  /// @note this is the original domain used to create this variable
  /// and it is not updated post-propagation
  inline const CPDomain& getDomain() const { return pDomain; }

  /// Set this variable as an output variable depending on the given Boolean value.
  /// @note by default, variables are all output variables
  inline void setOutput(bool out) { pOutputVariable = out; }

  /// Returns true if this is an output variable, returns false otherwise.
  /// @note some variables may not be output variables, e.g., auxiliary introduced variables
  inline bool isOutput() const { return pOutputVariable; }

  /// Returns true if this variable is a scalar variable, false otherwise
  inline bool isScalar() const
  {
    return getNumDim() == 0;
  }

  /// Returns true if this variable is an array variable, false otherwise
  inline bool isArray() const
  {
    return getNumDim() == 1;
  }

  /// Returns true if this variable is a matrix variable, false otherwise
  inline bool isMatrix() const
  {
    return getNumDim() > 1;
  }

  /// Returns the variable at the given subscript index.
  /// @note the identifier of the returned variable is the following:
  ///       <this_variable_name>_dim1_dim2_..._dim_n
  /// @note throws std::runtime_error on scalar variables.
  /// @note throws std::runtime_error on wrong indexing
  virtual CPVariable::SPtr getSubVariable(const std::vector<int>& subscr) = 0;

 protected:
  /// Creates a CP variable with given name.
  /// This variable doesn't have any domain set, which mean it is a place-holder for an actual
  /// CP variable. The domain should be immediately set back with the method "overrideDomain(...)"
  CPVariable(const std::string& name);

  /// Overrides the current domain with the given one
  inline void overrideDomain(const CPDomain& domain) { pDomain = domain; }

  /// Returns the number of dimensions
  inline int getNumDim() const { return pNumDim; }

  /// Returns the dimensions of this variable
  inline const std::vector<int> getVarDims() const { return pDims; }

  /// Returns the name to be used for the indexed variable.
  /// The indices are given by the vector "subscr".
  /// For exmaple, the variable X[1, 2, 3] will have name "X_1_2_3"
  std::string createNameForIndexedVariable(const std::vector<int>& subscr);

  /// Returns the flattened index w.r.t. the list of dimensions and the subscript indices
  int getFlattenedIndex(const std::vector<int> idxList);

 private:
  /// Variable identifier
  std::string pVarName;

  /// Number of dimensions of this variable
  int pNumDim;

  /// Flag indicating whether this is an output variable or not
  bool pOutputVariable{true};

  /// Dimensions of this variable
  std::vector<int> pDims;

  /// Domain of the variable
  CPDomain pDomain;
};

}  // namespace optilab
