//
// Copyright OptiLab 2019. All rights reserved.
//
// Solver class for MIP frameworks.
//

#pragma once

#include <memory>  // for std::shared_ptr

#include "or_tools_engine/or_tools_utilities.hpp"
#include "solver/solver.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS MIPSolver : public Solver {
 public:
  using SPtr = std::shared_ptr<MIPSolver>;

 public:
  MIPSolver() = default;

  virtual ~MIPSolver() = default;

  /// Initializes the solver for the search
  virtual void initSearch() {}

  /// Executes cleanup code after search is completed
  virtual void cleanupSearch() {}

  /// Runs the solver until a solution is found or a given timeout is reached
  virtual void solve() = 0;

  /// Returns the current status of the search result
  virtual ortoolsengine::ORToolsMIPResult::ResultStatus getResultStatus() const = 0;

  /// Stops the (ongoing) search process and returns true if the solver
  /// was actually interrupted, returns false otherwise.
  /// @note depending on the package in use, some solvers don't provide
  /// interruption functionalities
  virtual bool interruptSearchProcess() { return false; }

};

}  // namespace optilab
