//
// Copyright OptiLab 2019. All rights reserved.
//
// Generic class for a solver.
// A solver can be a CP, MIP, LP solver, etc.
//

#pragma once

#include <memory>  // for std::shared_ptr

#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS Solver {
 public:
  using SPtr = std::shared_ptr<Solver>;

 public:
  Solver() = default;

  virtual ~Solver() = default;
};

}  // namespace optilab
