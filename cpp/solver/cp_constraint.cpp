#include "solver/cp_constraint.hpp"

#include <stdexcept>  // for std::out_of_range

namespace {

constexpr int kBoolVarFalse = 0;
constexpr int kBoolVarTrue = 1;

}  // namespace

namespace optilab {

CPArgument CPArgument::buildIntValueArg(int64_t val)
{
  CPArgument arg;
  arg.argType = CPArgument::ArgumentType::AT_INT_VAL;
  arg.valIntList.push_back(val);
  return arg;
}  // buildIntValueArg

CPArgument CPArgument::buildIntValueArrayArg(const std::vector<int64_t>& valArray)
{
  CPArgument arg;
  arg.argType = CPArgument::ArgumentType::AT_INT_VAL_LIST;
  arg.valIntList = valArray;
  return arg;
}  // buildIntValueArrayArg

CPArgument CPArgument::buildDoubleValueArg(double val)
{
  CPArgument arg;
  arg.argType = CPArgument::ArgumentType::AT_DOUBLE_VAL;
  arg.valDoubleList.push_back(val);
  return arg;
}  // buildDoubleValueArg

CPArgument CPArgument::buildDoubleValueArrayArg(const std::vector<double>& valArray)
{
  CPArgument arg;
  arg.argType = CPArgument::ArgumentType::AT_DOUBLE_VAL_LIST;
  arg.valDoubleList = valArray;
  return arg;
}  // buildDoubleValueArrayArg

CPArgument CPArgument::buildVarRefArg(CPVariable::SPtr var)
{
  CPArgument arg;
  arg.argType = CPArgument::ArgumentType::AT_VAR;
  arg.varList.push_back(var);
  return arg;
}  // buildVarRefArg

CPArgument CPArgument::buildVarRefArrayArg(const std::vector<CPVariable::SPtr>& varArray)
{
  CPArgument arg;
  arg.argType = CPArgument::ArgumentType::AT_VAR_LIST;
  arg.varList = varArray;
  return arg;
}  // buildVarRefArrayArg

int64_t CPArgument::getIntValAt(int idx) const
{
  if (idx < 0 || idx >= getNumIntVals())
  {
    throw std::out_of_range("CPArgument - getIntValAt invalid index: " + std::to_string(idx));
  }

  return valIntList.at(idx);
}  // getIntValAt

double CPArgument::getDoubleValAt(int idx) const
{
  if (idx < 0 || idx >= isDoubleArgVal())
  {
    throw std::out_of_range("CPArgument - isDoubleArgVal invalid index: " + std::to_string(idx));
  }

  return valDoubleList.at(idx);
}  // getDoubleValAt

CPVariable::SPtr CPArgument::getVarAt(int idx) const
{
  if (idx < 0 || idx >= getNumVars())
  {
    throw std::out_of_range("CPArgument - getVarAt invalid index: " + std::to_string(idx));
  }

  return varList.at(idx);
}  // getVarAt

bool CPArgument::hasOneVar() const
{
  return (argType == ArgumentType::AT_VAR ||
      (argType == ArgumentType::AT_VAR_LIST && varList.size() == 1));
}  // hasOneVar

bool CPArgument::hasOneIntValue() const
{
  return (argType == ArgumentType::AT_INT_VAL ||
      (argType == ArgumentType::AT_INT_VAL_LIST && valIntList.size() == 1) ||
      (argType == ArgumentType::AT_VAR && varList[0]->isScalar() &&
          (varList[0]->getDomain()).isSingleton() &&
          (varList[0]->getDomain()).doubleValues.empty()));
}  // hasOneIntValue

bool CPArgument::hasOneDoubleValue() const
{
  return (argType == ArgumentType::AT_DOUBLE_VAL ||
      (argType == ArgumentType::AT_DOUBLE_VAL_LIST && valDoubleList.size() == 1) ||
      (argType == ArgumentType::AT_VAR && varList[0]->isScalar() &&
          (varList[0]->getDomain()).isSingleton() &&
          (varList[0]->getDomain()).intValues.empty()));
}  // hasOneDoubleValue

CPVariable::SPtr CPArgument::getVariable() const
{
  if (!hasOneVar())
  {
    throw std::runtime_error("Constraint argument doesn't have one single variable");
  }

  return varList[0];
}  // getVariable

int64_t CPArgument::getIntValue() const
{
  if (!hasOneIntValue())
  {
    throw std::runtime_error("Constraint argument doesn't have one single integer value");
  }

  if (argType == ArgumentType::AT_VAR)
  {
    return (varList[0]->getDomain()).intValues.at(0);
  }
  return valIntList.at(0);
}  // getIntValue

double CPArgument::getDoubleValue() const
{
  if (!hasOneDoubleValue())
  {
    throw std::runtime_error("Constraint argument doesn't have one single double value");
  }

  if (argType == ArgumentType::AT_VAR)
  {
    return (varList[0]->getDomain()).doubleValues.at(0);
  }
  return valDoubleList.at(0);
}  // getDoubleValue

CPConstraint::CPConstraint(const std::string& type, const std::vector<CPArgument>& args,
                           PropagationType propType)
: pPropagationType(propType),
  pType(type),
  pArgumentList(args)
{
  if (pType.empty())
  {
    throw std::invalid_argument("CPConstraint - empty type");
  }
}

}  // namespace optilab
