#include "solver/mip_objective.hpp"

#include <cassert>
#include <stdexcept>  // for std::runtime_error

namespace optilab {

MIPObjective::MIPObjective(ModelObjectMIPObjective::SPtr mipObjectiveObject,
                           const std::unordered_map<std::string, MIPVariable::SPtr>& varMap)
{
  if (!mipObjectiveObject)
  {
    throw std::invalid_argument("MIPObjective - empty ModelObjectMIPObjective object");
  }

  initObjective(mipObjectiveObject, varMap);
}

void MIPObjective::initObjective(ModelObjectMIPObjective::SPtr mipObjectiveObject,
                                 const std::unordered_map<std::string, MIPVariable::SPtr>& varMap)
{
  const auto dir = mipObjectiveObject->getOptimizationDirection();
  switch (dir)
  {
    case ModelObjectMIPObjective::OptimizationDirection::MAXIMIZE:
      pObjectiveDir = ObjectiveDirection::MAXIMIZE;
      break;
    default:
      assert(dir == ModelObjectMIPObjective::OptimizationDirection::MINIMIZE);
      pObjectiveDir = ObjectiveDirection::MINIMIZE;
      break;
  }

  // Initialize the variable coefficient list
  for (const auto& varCoeffPair : *mipObjectiveObject)
  {
    auto it = varMap.find(varCoeffPair.first);
    if (it == varMap.end())
    {
      throw std::runtime_error("MIPObjective - cannot find MIP variable with id " +
                               varCoeffPair.first);
    }

    pVarCoeffList.push_back({it->second, varCoeffPair.second});
  }
}  // initObjective

}  // namespace optilab
