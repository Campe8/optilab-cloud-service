//
// Copyright OptiLab 2019. All rights reserved.
//
// Class encapsulating a MIP objective.
//

#pragma once

#include <limits>   // for std::numeric_limits
#include <memory>   // for std::shared_ptr
#include <string>
#include <unordered_map>
#include <utility>  // for std::pair
#include <vector>

#include "model/model_object_mip_objective.hpp"
#include "solver/mip_variable.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_STRUCT MIPObjective {
 public:
  enum ObjectiveDirection {
    MAXIMIZE = 0,
    MINIMIZE
  };

  using SPtr = std::shared_ptr<MIPObjective>;
  using VarCoeffPair = std::pair<MIPVariable::SPtr, double>;
  using VarCoeffList = std::vector<VarCoeffPair>;
  using Iter = VarCoeffList::iterator;

 public:
  /// Default constructor
  MIPObjective() = default;

  /// Constructor: creates a MIP objective.
  /// @note throws std::runtime_error if the coeff variables cannot be found
  /// in the given variable map
  MIPObjective(ModelObjectMIPObjective::SPtr mipObjectiveObject,
               const std::unordered_map<std::string, MIPVariable::SPtr>& varMap);

  virtual ~MIPObjective() = default;

  inline ObjectiveDirection getObjectiveDirection() const { return pObjectiveDir; }

  inline const VarCoeffList& getVarCoeffList() const { return pVarCoeffList; }

  Iter begin() { return pVarCoeffList.begin(); }
  Iter end() { return pVarCoeffList.end(); }

 private:
  /// Objective direction
  ObjectiveDirection pObjectiveDir{ObjectiveDirection::MINIMIZE};

  /// List of variable-coefficient pairs for this objective
  VarCoeffList pVarCoeffList;

  /// Utility function: initialize this objective
  void initObjective(ModelObjectMIPObjective::SPtr mipObjectiveObject,
                     const std::unordered_map<std::string, MIPVariable::SPtr>& varMap);
};

}  // namespace optilab
