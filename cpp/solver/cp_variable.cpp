#include "solver/cp_variable.hpp"

#include <algorithm>  // for std::sort
#include <limits>     // for std::numeric_limits
#include <map>
#include <stdexcept>  // for std::runtime_error

#include "utilities/variables.hpp"

namespace {

constexpr int kBoolVarFalse = 0;
constexpr int kBoolVarTrue = 1;


/// Sorts and removes duplicates from a vector
template<typename T>
inline void sortAndRemoveDuplicates(T* ptr)
{
  std::sort(ptr->begin(), ptr->end());
  ptr->erase(std::unique(ptr->begin(), ptr->end()), ptr->end());
}  // sortAndRemoveDuplicates

std::string getVariableIndexName(const std::string& baseName, const std::vector<int>& dims)
{
  std::string varName = baseName;
  for (auto dim : dims)
  {
    varName += "_" + std::to_string(dim);
  }

  return varName;
}  // getVariableIndexName

}  // end namespace

namespace optilab {

int CPDomain::boolToInt(bool val)
{
  return val ? kBoolVarTrue : kBoolVarFalse;
}  // boolToInt

CPDomain CPDomain::buildBooleanDomain()
{
  CPDomain dom;
  dom.isSet = false;
  dom.isBool = true;
  dom.isListOfRanges = false;
  dom.isListOfValues = false;
  dom.isInterval = false;
  dom.intValues.push_back(boolToInt(false));
  dom.intValues.push_back(boolToInt(true));
  return dom;
}  // buildBooleanDomain

CPDomain CPDomain::buildBooleanDomain(bool singletonVal)
{
  CPDomain dom;
  dom.isSet = false;
  dom.isBool = true;
  dom.isListOfRanges = false;
  dom.isListOfValues = false;
  dom.isInterval = false;
  dom.intValues.push_back(boolToInt(singletonVal));
  dom.intValues.push_back(boolToInt(singletonVal));

  return dom;
}  // buildBooleanDomain

CPDomain CPDomain::buildIntDomain(int64_t lowerBound, int64_t upperBound)
{
  if (lowerBound > upperBound)
  {
    throw std::invalid_argument("CPDomain - lower bound " + std::to_string(lowerBound) +
                                " greater than upper bound " + std::to_string(upperBound));
  }

  CPDomain dom;
  dom.isSet = false;
  dom.isBool = false;
  dom.isListOfRanges = false;
  dom.isListOfValues = false;
  dom.isInterval = true;
  dom.intValues.push_back(lowerBound);
  dom.intValues.push_back(upperBound);
  return dom;
}  // buildIntDomain

CPDomain CPDomain::buildContinuousDomain(double lowerBound, double upperBound)
{
  if (lowerBound > upperBound)
  {
    throw std::invalid_argument("CPDomain - lower bound " + std::to_string(lowerBound) +
                                " greater than upper bound " + std::to_string(upperBound));
  }

  CPDomain dom;
  dom.isSet = false;
  dom.isBool = false;
  dom.isListOfRanges = false;
  dom.isListOfValues = false;
  dom.isInterval = true;
  dom.doubleValues.push_back(lowerBound);
  dom.doubleValues.push_back(upperBound);
  return dom;
}  // buildIntDomain

CPDomain CPDomain::buildListOfValuesDomain(const std::vector<int64_t>& values)
{
  // Remove duplicate values
  auto valsCopy = values;
  sortAndRemoveDuplicates(&valsCopy);

  CPDomain dom;
  dom.isSet = false;
  dom.isBool = false;
  dom.isListOfRanges = false;
  dom.isListOfValues = true;
  dom.isInterval = false;
  dom.intValues = valsCopy;
  return dom;
}  // buildListOfValuesDomain

CPDomain CPDomain::buildSetDomain(const std::vector<int64_t>& values)
{
  // Remove duplicate values
  auto valsCopy = values;
  sortAndRemoveDuplicates(&valsCopy);

  CPDomain dom;
  dom.isSet = true;
  dom.isBool = false;
  dom.isListOfRanges = false;
  dom.isListOfValues = false;
  dom.isInterval = false;
  dom.intValues = valsCopy;
  return dom;
}  // buildSetDomain

bool CPDomain::isSingleton() const
{
  if (isBool) return intValues[0] == intValues[1];
  if (isInterval && !intValues.empty() && intValues[0] == intValues[1]) return true;
  if (isInterval && !doubleValues.empty() && doubleValues[0] == doubleValues[1]) return true;
  if (isListOfRanges && intValues.size() == 2 && intValues[0] == intValues[1]) return true;
  if (isListOfValues && intValues.size() == 1) return true;

  return false;
}  // isSingleton

bool CPDomain::isUpperBoundInf() const
{
  static int64_t intInf = std::numeric_limits<int64_t>::infinity();
  static double dblInf = std::numeric_limits<double>::infinity();
  if (isBool) return false;
  if (!intValues.empty()) return intValues.back() == intInf;
  if (!doubleValues.empty()) return doubleValues.back() == dblInf;
  return false;
}  // isUpperBoundInf

bool CPDomain::isLowerBoundInf() const
{
  static int64_t intInf = -(std::numeric_limits<int64_t>::infinity());
  static double dblInf = -(std::numeric_limits<double>::infinity());
  if (isBool) return false;
  if (!intValues.empty()) return intValues.back() == intInf;
  if (!doubleValues.empty()) return doubleValues.back() == dblInf;
  return false;
}  // isLowerBoundInf

CPDomain CPDomain::buildListOfRangesDomain(
    const std::vector<std::pair<int64_t, int64_t>>& listRange)
{
  CPDomain dom;

  // Map used to keep track of "contained" pairs
  std::map<int64_t,int64_t> tempMap;
  for (const auto& pair : listRange)
  {
    if (pair.first > pair.second)
    {
      throw std::invalid_argument("CPDomain - pair lower bound " + std::to_string(pair.first) +
                                  " greater than pair upper bound " + std::to_string(pair.second));
    }

    if (tempMap.find(pair.first) == tempMap.end())
    {
      // First time this lower bound has been seen,
      // store the pair as is
      tempMap[pair.first] = pair.second;
    }
    else
    {
      // The lower bound is already present,
      // keep the highest upper bound
      if (tempMap[pair.first] < pair.second)
      {
        tempMap[pair.first] = pair.second;
      }
    }
  }

  // Traverse the (sorted) map and store the pair into the vector
  for (const auto& pair : tempMap)
  {
    dom.intValues.push_back(pair.first);
    dom.intValues.push_back(pair.second);
  }
  dom.isSet = false;
  dom.isBool = false;
  dom.isListOfRanges = true;
  dom.isListOfValues = false;
  dom.isInterval = false;
  return dom;
}  // buildListOfRangesDomain

CPVariable::CPVariable(const std::string& name)
: pVarName(name), pNumDim(0)
{
}

CPVariable::CPVariable(const std::string& name, const CPDomain& domain)
: pVarName(name), pNumDim(0), pDomain(domain)
{
}

CPVariable::CPVariable(const std::string& name, const CPDomain& domain, int len)
: pVarName(name), pNumDim(1), pDomain(domain)
{
  if (len < 1)
  {
    throw std::runtime_error("CPVariable: Invalid array size");
  }

  pDims.push_back(len);
}

CPVariable::CPVariable(const std::string& name, const CPDomain& domain,
                       const std::vector<int>& dims)
: pVarName(name),
  pDims(dims),
  pNumDim(static_cast<int>(dims.size())),
  pDomain(domain)
{
  if (pNumDim < 2)
  {
    throw std::runtime_error("CPVariable: Invalid number of dimensions for matrix");
  }
}

std::string CPVariable::createNameForIndexedVariable(const std::vector<int>& subscr)
{
  return getVariableIndexName(pVarName, subscr);
}

int CPVariable::getFlattenedIndex(const std::vector<int> idxList)
{
  return utilsvariables::getFlattenedIndex(pDims, idxList);
}  // getFlattenedIndex

}  // namespace optilab
