//
// Copyright OptiLab 2019. All rights reserved.
//
// Builder for OR Tools CPSearch classes.
//

#pragma once

#include <memory>   // for std::shared_ptr

#include "model/model_object_search.hpp"
#include "model/primitive_search_object.hpp"
#include "solver/cp_model.hpp"
#include "solver/cp_search.hpp"
#include "solver/search_combinator.hpp"
#include "solver/search_combinator_builder.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS CPSearchBuilder {
 public:
  using SPtr = std::shared_ptr<CPSearchBuilder>;

 public:
  /// Constructor.
  /// @note throws std::invalid_argument if any of the input arguments is an empty pointer
  CPSearchBuilder(CPModel::SPtr cpModel, SearchCombinatorBuilder::SPtr combinatorBuilder);

  virtual ~CPSearchBuilder() = default;

  /// Builds and returns a CP search according to the model and the combinator builder
  /// given as input to the constructor of this class
  CPSearch::SPtr build();

 private:
  /// CP Model containing the information for the builder,
  /// e.g., variables, search strategies, etc.
  CPModel::SPtr pCPModel;

  /// Package-specific combinator builder used to build search combinators
  SearchCombinatorBuilder::SPtr pCombinatorBuilder;

  /// Returns a "propagation-only" search
  CPSearch::SPtr buildPropagationOnlySearch();

  /// Returns a global search
  CPSearch::SPtr buildGlobalSearch(const ModelObjectSearch::SPtr& objectSearch);

  /// Recursively builds the combinator with specified identifier from the given object search.
  /// @note throws std::runtime_error if no combinator with given Id can be found in the
  /// given model object search
  SearchCombinator::SPtr buildSearchCombinator(const std::string& combinatorId,
                                               const ModelObjectSearch::SPtr& objectSearch);

  /// Returns an OR combinator
  SearchCombinator::SPtr buildOrCombinator(const SearchObject::SPtr& combinator,
                                           const ModelObjectSearch::SPtr& objectSearch);

  /// Returns an AND combinator
  SearchCombinator::SPtr buildAndCombinator(const SearchObject::SPtr& combinator,
                                            const ModelObjectSearch::SPtr& objectSearch);
};

}  // namespace optilab
