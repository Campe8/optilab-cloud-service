#include "solver/cp_model.hpp"

#include <stdexcept>  // for std::invalid_argument
#include <utility>    // for std::pair

#include <spdlog/spdlog.h>

#include "model/model_constants.hpp"

namespace optilab {

CPModel::CPModel(const Model::SPtr& model)
: pModel(model)
{
  if (!pModel)
  {
    throw std::invalid_argument("CPModel - pointer to Model is NULL");
  }

  pModelName = pModel->getModelName();

  // Get the format of the model
  const auto& modelFormat = pModel->getModelFormat();
  if (modelFormat.empty() || modelFormat == jsonmodel::OPTILAB_MODEL_FORMAT)
  {
    pModelInputFormat = ModelInputFormat::INPUT_FORMAT_OPTILAB;
  }
  else
  {
    if (modelFormat == jsonmodel::FLATZINC_MODEL_FORMAT)
    {
      pModelInputFormat = ModelInputFormat::INPUT_FORMAT_FLATZINC;
    }
    else
    {
      throw std::runtime_error("CPModel - unrecognized optimization model format type:" +
                               modelFormat);
    }
  }
}

SolverModel::ModelInputFormat CPModel::getModelInputFormat() const noexcept
{
  return pModelInputFormat;
}  // getModelInputFormat

void CPModel::initializeModel()
{
  switch(pModelInputFormat)
  {
    case ModelInputFormat::INPUT_FORMAT_OPTILAB:
      initializeWithOptiLabModelFormat();
      break;
    case ModelInputFormat::INPUT_FORMAT_FLATZINC:
      initializeWithLegacyModelFormat();
      break;
    default:
      throw std::runtime_error("CPModel - unrecognized optimization model format");
  }
}  // initializeModel

void CPModel::initializeWithOptiLabModelFormat()
{
  // Register constraint posters
  registerConstraints();

  // Add decision variables to the model
  addVariables();

  // Add constraints to the model
  addConstraints();

  /// Add search strategy to the model
  addSearchStrategy();
}  // initializeWithOptiLabModelFormat

const std::string& CPModel::getLegacyModelData() const
{
  return pModel->getModelData();
}  // getLegacyModelData

const std::string& CPModel::getLegacyModelDescriptor() const
{
  return pModel->getModelDescriptor();
}  // getLegacyModelDescriptor

void CPModel::registerConstraint(const std::string& name, ConstraintPoster poster)
{
  pConstraintRegistry.add(name, poster);
}  // registerConstraint

void CPModel::Registry::add(const std::string& conName, ConstraintPoster poster)
{
  pRegistry.insert(std::make_pair(conName, poster));
}  // add

void CPModel::Registry::post(CPConstraint::SPtr con)
{
  if (!con)
  {
    throw std::runtime_error("CPModel - registry: empty constraint pointer on post method");
  }

  auto it = pRegistry.find(con->getType());
  if (it == pRegistry.end())
  {
    const std::string errMsg = "CPModel - registry: package doesn't support constraint " +
        con->getType();
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  // Use the poster to post the constraint
  it->second(con);
}  // post

void CPModel::addVariables()
{
  for (const auto& varIter : pModel->getVariableMap())
  {
    const std::string& varName = varIter.first;
    const auto& varPtr = varIter.second;
    if (pVarMap.find(varName) != pVarMap.end())
    {
      throw std::runtime_error("CPModel - variable " + varName +
                               " has been declared more than once");
    }

    // Create and add the variable to the model variable map
    pVarMap[varName] = createVariable(varPtr);
  }
}  // addVariables

void CPModel::addConstraints()
{
  for (const auto& conIter : pModel->getObjectList(ModelObject::ModelObjectType::CONSTRAINT))
  {
    auto conObj = std::dynamic_pointer_cast<ModelObjectConstraint>(conIter);
    if (!conObj)
    {
      throw std::runtime_error("CPModel - addConstraints invalid cast to constraint object");
    }

    // Create the instance of the constraint
    auto cpCon = createConstraint(conObj);

    // Post the constraint
    pConstraintRegistry.post(cpCon);
  }
}  // addConstraints

void CPModel::addSearchStrategy()
{
  // List of search strategies
  // @note only ONE search strategy is currently supported
  const auto& searchList = pModel->getSearchList();

  // If there is no search strategy specified, perform only constraint propagation
  if (searchList.empty())
  {
    pPropagationOnlyModel = true;
    return;
  }

  // Otherwise check that there is exactly one search strategy specified
  if (searchList.size() > 1)
  {
    spdlog::warn("CPModel: only one root search object is currently supported");
  }

  ModelObject::SPtr modelObj = searchList.front();
  pModelObjectSearch = std::dynamic_pointer_cast<ModelObjectSearch>(modelObj);
  if (!pModelObjectSearch)
  {
    throw std::runtime_error("CPModel - addSearchStrategy: invalid cast to "
        "ModelObjectSearch from ModelObject");
  }

  // Add the number of solutions to find
  pNumSolutions = pModelObjectSearch->getNumberOfSolutionsToFind();
}  // addSearchStrategy

}  // namespace optilab
