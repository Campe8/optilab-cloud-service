//
// Copyright OptiLab 2019. All rights reserved.
//
// Class encapsulating a MIP variable
//

#pragma once

#include <memory>  // for std::shared_ptr
#include <string>
#include <vector>

#include "system/system_export_defs.hpp"

namespace optilab
{
class SYS_EXPORT_STRUCT MIPVariable
{
 public:
  using SPtr = std::shared_ptr<MIPVariable>;

 public:
  /// Default contructor
  MIPVariable() = default;

  /// Creates a scalar variable with given name and bounds
  explicit MIPVariable(const std::string& name);

  /// Creates an array of variables according to the given list of dimensions.
  /// @note "len" must be greater than zero
  MIPVariable(const std::string& name, int len);

  /// Creates an n-dimensional matrix of variables according to the given list
  /// of dimensions.
  /// @note dims size must be at least 2
  MIPVariable(const std::string& name, const std::vector<int>& dims);

  virtual ~MIPVariable() = default;

  /// Returns the name of this variable
  const std::string& getVarName() const noexcept { return pVarName; }

  /// Returns true if this variable is a scalar variable, false otherwise
  inline bool isScalar() const noexcept { return getNumDim() == 0; }

  /// Returns true if this variable is an array variable, false otherwise
  inline bool isArray() const noexcept { return getNumDim() == 1; }

  /// Returns true if this variable is a matrix variable, false otherwise
  inline bool isMatrix() const noexcept { return getNumDim() > 1; }

  /// Returns true if this variable is part of the objective function.
  /// Returns false otherwise
  inline bool isObjectiveVariable() const noexcept
  {
    return (getObjectiveCoeff() != 0.0);
  }

  /// Sets the objective coefficient for this variable
  inline void setObjectiveCoeff(double coeff) noexcept
  {
    pObjectiveCoeff = coeff;
  }

  /// Returns the objective coefficient of this variable
  inline double getObjectiveCoeff() const noexcept { return pObjectiveCoeff; }

  /// Returns the variable at the given subscript index.
  /// @note the identifier of the returned variable is the following:
  ///       <this_variable_name>_dim1_dim2_..._dim_n
  /// @note throws std::runtime_error on scalar variables.
  /// @note throws std::runtime_error on wrong indexing
  MIPVariable::SPtr getVariable(const std::vector<int>& subscr);

 protected:
  /// Returns the number of dimensions
  inline int getNumDim() const noexcept { return pNumDim; }

  /// Returns the dimensions of this variable
  inline const std::vector<int> getVarDims() const { return pDims; }

 protected:
  /// Returns the variable at the specified index with specified Id
  virtual MIPVariable::SPtr getVariableFromIndex(
      const std::string& varName, const std::vector<int>& subscr) = 0;

 private:
  /// Variable identifier
  std::string pVarName;

  /// Number of dimensions of this variable
  int pNumDim{0};

  /// Coefficient value of this variable in the
  /// objective function
  double pObjectiveCoeff{0.0};

  /// Dimensions of this variable
  std::vector<int> pDims;
};

}  // namespace optilab
