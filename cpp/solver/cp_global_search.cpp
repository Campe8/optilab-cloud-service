#include "solver/cp_global_search.hpp"

#include <stdexcept>

namespace optilab {

CPGlobalSearchStrategy::CPGlobalSearchStrategy(SearchCombinator::SPtr heuristic)
: CPSearch(CPSearch::SearchType::GLOBAL_SEARCH),
  pSearchHeuristic(heuristic)
{
  if (!pSearchHeuristic)
  {
    throw std::invalid_argument("CPGlobalSearchStrategy - empty SearchCombinator pointer");
  }
}

bool CPGlobalSearchStrategy::isa(const CPSearch* cpSearch)
{
  return cpSearch && (cpSearch->getSearchType() == CPSearch::SearchType::GLOBAL_SEARCH);
}  // isa

CPGlobalSearchStrategy* CPGlobalSearchStrategy::cast(CPSearch* cpSearch)
{
  if (!isa(cpSearch)) return nullptr;
  return static_cast<CPGlobalSearchStrategy*>(cpSearch);
}  // cast

const CPGlobalSearchStrategy* CPGlobalSearchStrategy::cast(const CPSearch* cpSearch)
{
  if (!isa(cpSearch)) return nullptr;
  return static_cast<const CPGlobalSearchStrategy*>(cpSearch);
}  // cast


}  // namespace optilab
