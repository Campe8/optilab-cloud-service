//
// Copyright OptiLab 2019. All rights reserved.
//
// Class encapsulating search combinators.
// A combinator represents a "message protocol",
// an interface that allows combinators to be "combined"
// in order to define modular search strategies.
// In other words, these objects combine search heuristics
// (which can be basic or themselves constructed using combinators)
// into more complex heuristics.
// For more information see
// Search Combinators, by Schrijvers et al. or follow the link:
// https://arxiv.org/abs/1203.1095.
//

#pragma once

#include <memory>  // for std::shared_ptr

#include "system/system_export_defs.hpp"

namespace optilab {

/**
 * Search combinator base class
 */
class SYS_EXPORT_CLASS SearchCombinator {
 public:
  using SPtr = std::shared_ptr<SearchCombinator>;

 public:
  SearchCombinator() = default;

  virtual ~SearchCombinator() = default;
};

}  // namespace optilab
