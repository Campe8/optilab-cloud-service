#include "or_toolbox/or_optimizer.hpp"

#include <cassert>
#include <stdexcept>  // for std::invalid_argument

#include <spdlog/spdlog.h>

namespace optilab {
namespace toolbox {

OROptimizer::OROptimizer(const std::string& engineName, orengine::ORResult::SPtr result,
                         MetricsRegister::SPtr metricsRegister)
: BaseClass(engineName),
  pProcessor(nullptr),
  pResult(result),
  pMetricsRegister(metricsRegister)
{
  if (!pResult)
  {
    throw std::runtime_error("OROptimizer - empty result");
  }

  if (!pMetricsRegister)
  {
    throw std::invalid_argument("OROptimizer - empty pointer to the metrics register");
  }

  // Initialize the engine,
  // e.g., the internal pipeline
  BaseClass::init();
}

void OROptimizer::loadModel(const LinearModelSpecProto& model)
{
  // Create the processor loading the model from the protobuf message
  pProcessor->loadModelAndCreateSolver(model);
}  // loadModel

void OROptimizer::runOptimizer()
{
  if (!isActive())
  {
    const std::string errMsg = "OROptimizer - runOptimizer: optimizer is not active";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  Work::SPtr work = std::make_shared<Work>(orengine::ORWork::WorkType::kRunOptimizer,
                                           pMetricsRegister);
  pushTask(work);
}  // runOptimizer

bool OROptimizer::interruptOptimizer()
{
  assert(pProcessor);
  return pProcessor->interruptSolver();
}  // interruptOptimizer

ProcessorPipeline::SPtr OROptimizer::buildPipeline(int)
{
  ProcessorPipeline::SPtr pipeline = std::make_shared<ProcessorPipeline>();
  pProcessor = std::make_shared<ORProcessor>(pResult);
  pipeline->pushBackProcessor(pProcessor);
  return pipeline;
}  // buildPipeline

}  // namespace toolbox
}  // namespace optilab
