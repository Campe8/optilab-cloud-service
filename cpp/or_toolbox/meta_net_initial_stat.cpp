#include "or_toolbox/meta_net_initial_stat.hpp"

#include <stdexcept>  // for std::runtime_error

#include "or_toolbox/meta_net_framework.hpp"

#include <google/protobuf/util/message_differencer.h>
#include <spdlog/spdlog.h>

#include "meta_bb/meta_bb_constants.hpp"
#include "meta_bb/meta_bb_struct.hpp"
#include "meta_bb/meta_bb_utilities.hpp"

namespace optilab {
namespace toolbox {

MetaNetInitialStat::MetaNetInitialStat(int maxDepth, int maxTotalDepth)
: metabb::InitialStat(),
  pMaxDepth(maxDepth),
  pMaxTotalDepth(maxTotalDepth)
{
}

metabb::InitialStat::SPtr MetaNetInitialStat::clone(const metabb::Framework::SPtr& framework)
{
  metabb::InitialStat::SPtr initStat = std::make_shared<MetaNetInitialStat>();
  auto frm = std::dynamic_pointer_cast<MetaNetFramework>(initStat);
  if (!frm)
  {
    throw std::runtime_error("MetaNetInitialStat - clone: invalid downcast to MetaNetFramework");
  }

  return frm->buildMetaNetInitialStat(pMaxDepth, pMaxTotalDepth);
}  // clone

int MetaNetInitialStat::getMaxDepth()
{
  return pMaxDepth;
}  //getMaxDepth

void MetaNetInitialStat::accumulateOn(MPSolver::SPtr solver)
{
  if (!solver)
  {
    throw std::runtime_error("MetaNetInitialStat - accumulateOn: "
            "empty pointer to solver");
  }
  // TODO
}  // accumulateOn

int MetaNetInitialStat::send(const metabb::Framework::SPtr& framework, int dest) noexcept
{
  if (!framework)
  {
    spdlog::error("MetaNetInitialStat - send: empty network communicator");
    return metabb::metaconst::ERR_GENERIC_ERROR;
  }

  // Create the optimizer node packet to send to the destination
  optnet::Packet::UPtr packet(new optnet::Packet());

  // Add the content as protobuf message
  metabb::InitialStatProtoPacket initStat;
  initStat.set_maxdepth(pMaxDepth);
  initStat.set_maxtotaldepth(pMaxTotalDepth);

  packet->networkPacket = optnet::BaseNetworkPacket::UPtr(
      new optnet::NetworkPacket<metabb::InitialStatProtoPacket>(initStat));

  // Add the node tag
  packet->packetTag = optnet::PacketTag::UPtr(new metabb::net::MetaBBPacketTag(
          metabb::net::MetaBBPacketTag::PacketTagType::PPT_INITIAL_STAT));

  if (framework->getComm()->send(std::move(packet), dest))
  {
    return metabb::metaconst::ERR_GENERIC_ERROR;
  }

  return metabb::metaconst::ERR_NO_ERROR;
}  // MetaNetInitialStat::

int MetaNetInitialStat::upload(optnet::Packet::UPtr packet) noexcept
{
  if (!packet)
  {
    spdlog::error("MetaNetInitialStat - upload: empty packet");
    return metabb::metaconst::ERR_GENERIC_ERROR;
  }

  try
  {
    const auto tag = metabb::utils::castToMetaBBPacketTagAndGetTagOrThrow(packet->packetTag);
    if (tag != metabb::net::MetaBBPacketTag::PacketTagType::PPT_INITIAL_STAT)
    {
      spdlog::error("MetaNetInitialStat - upload: invalid tag received");
      return metabb::metaconst::ERR_GENERIC_ERROR;
    }
  }
  catch(...)
  {
    spdlog::error("MetaNetInitialStat - upload: no tag received");
    return metabb::metaconst::ERR_GENERIC_ERROR;
  }

  // This packet was deserialized as a std::string packet containing
  // the bytes of the serialized original message.
  // Therefore, the packet must be first deserialized into a proper
  // NetworkPacket
  if (packet->networkPacket->packetType !=
          optnet::BaseNetworkPacket::BaseNetworkPacketType::BNPT_META_BB_PACKET)
  {
    spdlog::error("MetaNetInitialStat - upload: invalid network packet type received");
    return metabb::metaconst::ERR_GENERIC_ERROR;
  }

  auto networkPacket = optnet::castBaseToNetworkPacket<std::string>(
          std::move(packet->networkPacket));
  if (!networkPacket)
  {
    spdlog::error("MetaNetInitialStat - upload: error downcasting to NetworkPacket<std::string>");
    return metabb::metaconst::ERR_GENERIC_ERROR;
  }

  if (networkPacket->size() != 1)
  {
    spdlog::error("MetaNetInitialStat - upload: wrong packet data size " +
                  std::to_string(networkPacket->size()));
    return metabb::metaconst::ERR_GENERIC_ERROR;
  }

  // Get the calculation state proto packet and upload its content into this node
  metabb::InitialStatProtoPacket initialStatProto;
  initialStatProto.ParseFromString(networkPacket->data.at(0));
  pMaxDepth = initialStatProto.maxdepth();
  pMaxTotalDepth = initialStatProto.maxtotaldepth();

  return metabb::metaconst::ERR_NO_ERROR;
}  // upload

}  // namespace toolbox

template<> bool optnet::NetworkPacket<metabb::InitialStatProtoPacket>::operator==(
    const NetworkPacket<metabb::InitialStatProtoPacket>& other) const noexcept
{
  if (this->data.size() != other.data.empty()) return false;
  return google::protobuf::util::MessageDifferencer::Equals(this->data[0], other.data[0]);
}

template<>
std::pair<optnet::BaseNetworkPacket::BaseNetworkPacketType, std::string>
optnet::NetworkPacket<metabb::InitialStatProtoPacket>::serialize() const noexcept
{
  if (data.size() != 1)
  {
    spdlog::error("NetworkPacket<InitialStatProtoPacket> - invalid data size");
    return {BaseNetworkPacketType::BNPT_UNSPECIFIED_PACKET, ""};
  }

  try
  {
    return {BaseNetworkPacketType::BNPT_META_BB_PACKET, data.at(0).SerializeAsString()};
  }
  catch(...)
  {
    spdlog::error("NetworkPacket<InitialStatProtoPacket> - "
            "undefined error when serializing protobuf");
    return {BaseNetworkPacketType::BNPT_UNSPECIFIED_PACKET, ""};
  }
}  // serialize

}  // namespace optilab
