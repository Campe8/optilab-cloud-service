//
// Copyright OptiLab 2020. All rights reserved.
//
// Distributed OR implementation based on metabb framework for the
// RacingRampUpParamSet class.
//

#pragma once

#include <cstdint>  // for uint64_t
#include <limits>   // for std::numeric_limits
#include <memory>   // for std::shared_ptr

#include "meta_bb/framework.hpp"
#include "meta_bb/racing_rampup_paramset.hpp"
#include "optilab_protobuf/meta_bb_packet.pb.h"
#include "optimizer_network/packet.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace toolbox {

class SYS_EXPORT_CLASS MetaNetRacingRampUpParamSet : public metabb::RacingRampUpParamSet {
public:
  using SPtr = std::shared_ptr<MetaNetRacingRampUpParamSet>;

public:
  MetaNetRacingRampUpParamSet()
  : RacingRampUpParamSet()
  {
  }

  MetaNetRacingRampUpParamSet(RacingTerminationCriteria termCriteria, int numNodesLeft,
                              int racingSeed, int permuteProbSeed, int generateBranchOrderSeed,
                              uint64_t timeoutMsec=std::numeric_limits<uint64_t>::max())
  : RacingRampUpParamSet(termCriteria, numNodesLeft, timeoutMsec),
    pRacingParamSeed(racingSeed),
    pPermuteProbSeed(permuteProbSeed),
    pGenerateBranchOrderSeed(generateBranchOrderSeed)
  {
  }

  /// Returns the seed used to generate racing parameters
  inline int getRacingParamSeed() const noexcept { return pRacingParamSeed; }

  /// Returns the seed used to permute the problem
  inline int getPermuteProbSeed() const noexcept { return pPermuteProbSeed; }

  /// Returns the seed used to generate the branching order
  inline int getGenerateBranchOrderSeed() const noexcept { return pGenerateBranchOrderSeed; }

  /// Returns the strategy to be used for racing rampup
  /// Returns the strategy for racing-rampup
  int getStrategy() override { return getRacingParamSeed(); }

  /// Sends this racing ramp-up parameter set to the given destination
  int send(metabb::Framework::SPtr framework, int destination) override;

  /// Uploads a racing ramp-up parameter set.
  int upload(optnet::Packet::UPtr packet) override;

protected:
  /// Seed to generate racing parameters
  int pRacingParamSeed{-1};

  /// Seed to permute problem
  int pPermuteProbSeed{0};

  /// Seed to generate branching order
  int pGenerateBranchOrderSeed{0};

private:
  optnet::Packet::UPtr buildNetworkPacket();
};

}  // namespace toolbox

/// Specialized template for network node packet comparison
template<> bool optnet::NetworkPacket<metabb::RacingRampUpParamSetProtoPacket>::operator==(
    const NetworkPacket<metabb::RacingRampUpParamSetProtoPacket>& other) const noexcept;

/// Specialized template to serialize RacingRampUpParamSetProtoPacket(s)
template<>
std::pair<optnet::BaseNetworkPacket::BaseNetworkPacketType, std::string>
optnet::NetworkPacket<metabb::RacingRampUpParamSetProtoPacket>::serialize()
const noexcept;

}  // namespace optilab
