#include "or_toolbox/net_or_root_processor.hpp"

#include <stdexcept>  // for std::runtime_error
#include <string>

#include <spdlog/spdlog.h>

#include "meta_bb/solution.hpp"
#include "or_toolbox/meta_net_solution.hpp"

namespace optilab {
namespace toolbox {

NetORProcessorModel::NetORProcessorModel()
{
  pProtoModel = std::unique_ptr<OptimizerModel>(new OptimizerModel());
  auto model = pProtoModel->mutable_linear_model();
  model->set_class_type(LinearModelSpecProto::ClassType::LinearModelSpecProto_ClassType_MIP);
  model->set_package_type(LinearModelSpecProto::PackageType::LinearModelSpecProto_PackageType_SCIP);
}

const LinearModelSpecProto& NetORProcessorModel::getProtoModel() const noexcept
{
  return pProtoModel->linear_model();
}

void NetORProcessorModel::setName(const std::string& name)
{
  auto model = pProtoModel->mutable_linear_model();
  model->set_model_id(name);
  auto mipModel = model->mutable_model();
  mipModel->set_name(name);
}

int NetORProcessorModel::addVariable(const std::string& name,
                                     double lb, double ub,
                                     bool isInt,
                                     double objCoeff)
{
  static int varIdx{0};
  auto mipModel = pProtoModel->mutable_linear_model()->mutable_model();
  auto var = mipModel->add_variable();
  var->set_name(name);
  var->set_lower_bound(lb);
  var->set_upper_bound(ub);
  var->set_is_integer(isInt);
  var->set_objective_coefficient(objCoeff);
  return varIdx++;
}  // addVariable

void NetORProcessorModel::addConstraint(
        const std::vector<int>& varIdx,
        const std::vector<double>& varCoeff,
        double lb,
        double ub,
        const std::string& name)
{
  auto mipModel = pProtoModel->mutable_linear_model()->mutable_model();
  auto con = mipModel->add_constraint();
  con->set_name(name);
  con->set_lower_bound(lb);
  con->set_upper_bound(ub);

  for (auto idx : varIdx)
  {
    con->add_var_index(idx);
  }

  for (auto coeff : varCoeff)
  {
    con->add_coefficient(coeff);
  }
}  // addConstraint

void NetORProcessorModel::setObjectiveDirection(bool maximize)
{
  auto mipModel = pProtoModel->mutable_linear_model()->mutable_model();
  mipModel->set_maximize(maximize);
}  // setObjectiveDirection

NetORRootProcessor::NetORRootProcessor()
: NetORProcessor()
{
}

void NetORRootProcessor::loadModel(NetORProcessorModel::SPtr model)
{
  if (!model)
  {
    const std::string errMsg = "NetORRootProcessor - loadModel: "
            "empty pointer to NetORProcessorModel";
    spdlog::error(errMsg);
    throw std::invalid_argument(errMsg);
  }

  // Prepare the work to be executed
  bool isRoot{true};
  orengine::ORNetWork::SPtr work = std::make_shared<orengine::ORNetWork>(
          orengine::ORNetWork::WorkType::kNetORLoadModel, isRoot);

  // Set the model
  work->lmInstance.CopyFrom(model->getProtoModel());

  // Execute work
  processWork(work);
}  // loadModel

void NetORRootProcessor::initRootNetworkNode()
{
  // Prepare the work to be executed
  bool isRoot{true};
  orengine::ORNetWork::SPtr work = std::make_shared<orengine::ORNetWork>(
          orengine::ORNetWork::WorkType::kNetORInitSystem, isRoot);

  // Execute work
  processWork(work);
}  // initRootNetworkNode

void NetORRootProcessor::run()
{
  // Prepare the work to be executed
  bool isRoot{true};
  orengine::ORNetWork::SPtr work = std::make_shared<orengine::ORNetWork>(
          orengine::ORNetWork::WorkType::kNetORRunSystem, isRoot);

  // Execute work
  processWork(work);
}  // run

orengine::MIPResult::SPtr NetORRootProcessor::collectSolutions()
{
  // Prepare the work to be executed
  bool isRoot{true};
  orengine::ORNetWork::SPtr work = std::make_shared<orengine::ORNetWork>(
          orengine::ORNetWork::WorkType::kNetORCollectSolutions, isRoot);

  // Execute work
  processWork(work);

  // Collect the solutions
  const auto& pcollection = getProcessorCollection();
  auto solution = pcollection.initiator->getGlobalBestIncumbentSolution();

  auto metaNetSol = std::dynamic_pointer_cast<MetaNetSolution>(solution);
  if (metaNetSol == nullptr)
  {
    const std::string errMsg = "NetORRootProcessor - collectSolutions: "
            "invalid downcast to MetaNetSolution";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  return metaNetSol->getORSolution();
}  // collectSolutions

}  // namespace toolbox
}  // namespace optilab
