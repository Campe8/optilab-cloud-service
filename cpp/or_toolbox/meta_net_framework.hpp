//
// Copyright OptiLab 2019-2020. All rights reserved.
//
// Distributed OR implementation based on metabb framework for the
// NetFramework class.
//

#pragma once

#include <cstdint>  // for uint64_t
#include <limits>   // for std::numeric_limits
#include <memory>   // for std::shared_ptr

#include "meta_bb/net_framework.hpp"
#include "or_toolbox/linear_model_common.hpp"
#include "or_toolbox/meta_net_diff_subproblem.hpp"
#include "or_toolbox/meta_net_initial_stat.hpp"
#include "or_toolbox/meta_net_instance.hpp"
#include "or_toolbox/meta_net_racing_rampup_paramset.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace toolbox {

class SYS_EXPORT_CLASS MetaNetFramework : public metabb::NetFramework {
 public:
  using SPtr = std::shared_ptr<MetaNetFramework>;

 public:
  /// Constructor for MetNetFramework.
  /// The root framework has rank 0 and it is in charge of sending network
  /// synchronization messages to all other nodes in the network
  MetaNetFramework(bool isRoot);

  ~MetaNetFramework() override;

  /// Initializes this framework
  void init();

  /// Teardown this framework
  void tearDown();

  /// Builds and returns a default racing ramp-up parameter set
  std::shared_ptr<metabb::RacingRampUpParamSet> buildRacingRampUpParamSet() override;

  /// Builds and returns the metabb-net specific implementation of the racing ramp-up
  /// parameters set
  std::shared_ptr<toolbox::MetaNetRacingRampUpParamSet> buildRacingRampUpParamSet(
          metabb::RacingRampUpParamSet::RacingTerminationCriteria termCriteria,
          int numNodesLeft, int racingSeed, int permuteProbSeed, int generateBranchOrderSeed,
          uint64_t timeoutMsec=std::numeric_limits<uint64_t>::max());

  /// Builds and returns a new instance of an MPModel
  MetaNetInstance::SPtr buildInstance(lmcommon::OptimizationSolverPackageType packgageType);

  /// Builds and returns a default initial stat object
  std::shared_ptr<metabb::InitialStat> buildInitialStat() override;

  /// Builds and returns a meta-net initial stat object
  std::shared_ptr<MetaNetInitialStat> buildMetaNetInitialStat(int maxDepth, int maxTotalDepth);

  /// Builds and returns a new instance of a DiffSubproblem,
  /// i.e., the class storing the difference between instance and subproblem
  std::shared_ptr<metabb::DiffSubproblem> buildDiffSubproblem() override;

  /// Builds and returns a new instance of a MetaNetDiffSubproblem.
  /// TODO check and possibly remove this (unused ?) method
  std::shared_ptr<MetaNetDiffSubproblem> buildDiffSubproblem(
          std::shared_ptr<MetaNetSolver> solver,
          const std::vector<MPVariable::SPtr>& branchingVarList,
          const std::vector<double>& branchingBoundList,
          const std::vector<MetaNetDiffSubproblem::BoundType>& branchingBoundType,
          const std::vector<MPConstraint::SPtr>& constraintList);

  /// Builds and returns a new instance of a default solution
  std::shared_ptr<metabb::Solution> buildSolution() override;
};

}  // namespace toolbox
}  // namespace optilab
