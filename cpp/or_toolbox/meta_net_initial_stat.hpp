//
// Copyright OptiLab 2020. All rights reserved.
//
// Distributed OR implementation based on metabb framework for the
// Instance class.
//

#pragma once

#include <memory>  // for std::shared_ptr

#include "meta_bb/framework.hpp"
#include "meta_bb/initial_stat.hpp"
#include "optilab_protobuf/meta_bb_packet.pb.h"
#include "optimizer_network/packet.hpp"
#include "or_toolbox/linear_solver.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace  toolbox {

class SYS_EXPORT_CLASS MetaNetInitialStat : public metabb::InitialStat {
 public:
  using SPtr = std::shared_ptr<MetaNetInitialStat>;

 public:
  /// Default constructor
  MetaNetInitialStat() = default;

  /// Constructor for MetaNetInitialStat
  MetaNetInitialStat(int maxDepth, int maxTotalDepth);

  /// Returns a clone of this initial statistics object
  metabb::InitialStat::SPtr clone(const metabb::Framework::SPtr& framework) override;

  /// Returns the get maximum depth
  int getMaxDepth() override;

  /// Accumulate statistics
  void accumulateOn(MPSolver::SPtr solver);

  /// Sends a copy of this statistics to the given destination
  int send(const metabb::Framework::SPtr& framework, int dest) noexcept override;

  /// Uploads the content of the given network packet into this InitialStat object.
  /// If the given packet is not a node network packet, throws std::runtime_error
  /// Returns zero on success, non-zero otherwise
  int upload(optnet::Packet::UPtr packet) noexcept override;

 private:
  /// Maximum depth of all nodes in the current
  /// branch and bound run
  int pMaxDepth{0};

  /// Maximum depth of all nodes over all
  /// branch and bound runs
  int pMaxTotalDepth{0};
};

}  // namespace metabb

/// Specialized template for network node packet comparison
template<> bool optnet::NetworkPacket<metabb::InitialStatProtoPacket>::operator==(
    const NetworkPacket<metabb::InitialStatProtoPacket>& other) const noexcept;

/// Specialized template to serialize NodeProtoPacket(s)
template<>
std::pair<optnet::BaseNetworkPacket::BaseNetworkPacketType, std::string>
optnet::NetworkPacket<metabb::InitialStatProtoPacket>::serialize() const noexcept;

}  // namespace optilab
