#include "or_toolbox/or_processor.hpp"

#include <spdlog/spdlog.h>

#include <cassert>
#include <exception>
#include <functional>  // for std::bind
#include <sstream>     // for std::stringstream
#include <stdexcept>   // for std::runtime_error

#include "data_structure/metrics/metrics_register.hpp"
#include "engine/engine_constants.hpp"
#include "or_toolbox/or_constants.hpp"
#include "or_toolbox/or_utilities.hpp"
#include "utilities/file_sys.hpp"
#include "utilities/timer.hpp"

#include <chrono>

namespace {
bool isBoolianVar(const optilab::OptVariableProto& var)
{
  return var.is_integer() &&
          (var.lower_bound() == 0.0) &&
          (var.upper_bound() == 1.0);
}  // isBoolianVar
}  // namespace

namespace optilab {
namespace toolbox {

ORProcessor::ORProcessor(orengine::ORResult::SPtr result)
    : BaseProcessor("ORProcessor"),
      pResultStatus(lmcommon::ResultStatus::NOT_SOLVED)
{
  pResult = std::dynamic_pointer_cast<orengine::MIPResult>(result);
  if (!pResult)
  {
    throw std::runtime_error("ORProcessor - invalid downcast to MIPResult");
  }
}

void ORProcessor::loadModelAndCreateSolver(const LinearModelSpecProto& model)
{
  // Create a new MIP model from the given input model
  pSolver = orutils::createSolverGivenType(model.model_id(), model.package_type());
  if (!pSolver)
  {
    std::string errMsg = "ORProcessor - loadModelAndCreateSolver: empty pointer to solver";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  // Load the model
  // Get the input model type
  auto modelType = model.model_format_type();
  switch (model.model_format_type())
  {
    case ::optilab::LinearModelSpecProto_ModelFormatType::
    LinearModelSpecProto_ModelFormatType_PROTOBUF:
    {
      loadProtoModel(model.model());
      break;
    }
    case ::optilab::LinearModelSpecProto_ModelFormatType::
    LinearModelSpecProto_ModelFormatType_MPS:
    {
      loadMPSModel(model.model_path());
      break;
    }
    case ::optilab::LinearModelSpecProto_ModelFormatType::
    LinearModelSpecProto_ModelFormatType_CSV:
    {
      loadMatrixModel(model.model_path());
      break;
    }
    default:
      throw std::runtime_error("ORProcessor - loadModelAndCreateSolver: "
              "unrecognized input model format");
  }
}  // loadModelAndCreateSolver

void ORProcessor::loadMatrixModel(const std::string& path)
{
  // Get the paths to the files
  const auto matAEqPath = utilsfilesys::getFullPathToLocation({path, orconstants::CSV_MODEL_A_EQ});
  const auto matAIneqPath = utilsfilesys::getFullPathToLocation(
          {path, orconstants::CSV_MODEL_A_INEQ});
  const auto matBEqPath = utilsfilesys::getFullPathToLocation({path, orconstants::CSV_MODEL_B_EQ});
  const auto matBIneqPath = utilsfilesys::getFullPathToLocation(
          {path, orconstants::CSV_MODEL_B_INEQ});
  const auto fMaxObjPath = utilsfilesys::getFullPathToLocation({path, orconstants::CSV_MODEL_F_MAX});
  const auto fMinObjPath = utilsfilesys::getFullPathToLocation({path, orconstants::CSV_MODEL_F_MIN});

  if (!utilsfilesys::doesFileExists(matAEqPath))
  {
    const std::string errMsg = "ORProcessor - loadMatrixModel: file not found " + matAEqPath;
    spdlog::warn(errMsg);
  }

  if (!utilsfilesys::doesFileExists(matAIneqPath))
  {
    const std::string errMsg = "ORProcessor - loadMatrixModel: file not found " + matAIneqPath;
    spdlog::warn(errMsg);
  }

  if (!utilsfilesys::doesFileExists(matBEqPath))
  {
    const std::string errMsg = "ORProcessor - loadMatrixModel: file not found " + matBEqPath;
    spdlog::warn(errMsg);
  }

  if (!utilsfilesys::doesFileExists(matBIneqPath))
  {
    const std::string errMsg = "ORProcessor - loadMatrixModel: file not found " + matBIneqPath;
    spdlog::warn(errMsg);
  }

  if (!utilsfilesys::doesFileExists(fMaxObjPath) && !utilsfilesys::doesFileExists(fMinObjPath))
  {
    const std::string errMsg = "ORProcessor - loadMatrixModel: file not found " +
            fMaxObjPath + " or " + fMinObjPath;
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }
  const bool isMaximization = utilsfilesys::doesFileExists(fMaxObjPath);
  const std::string fObjPath = isMaximization ? fMaxObjPath : fMinObjPath;

  // The matrices above form two system of equations:
  // A_1 x <= B_1
  // A_2 x  = B_2
  // min f
  // The number of rows of A matrix is the number of constraints,
  // while the number of columns is the number of variables.
  // Load the matrices
  utilsreader::CSVReader csvAIneqReader(matAIneqPath);
  utilsreader::CSVReader csvAEqReader(matAEqPath);
  utilsreader::CSVReader csvBIneqReader(matBIneqPath);
  utilsreader::CSVReader csvBEqReader(matBEqPath);
  utilsreader::CSVReader csvFObjReader(fObjPath);

  // Get the CSV data
  auto matAIneqData = csvAIneqReader.getData();
  auto matAEqData = csvAEqReader.getData();
  auto matBIneqData = csvBIneqReader.getData();
  auto matBEqData = csvBEqReader.getData();
  auto matFData = csvFObjReader.getData();

  const int numVars = static_cast<int>(matAIneqData.at(0).size());
  const int numIneqCons = static_cast<int>(matAIneqData.size());
  const int numEqEqCons = static_cast<int>(matAEqData.size());

  // Load variables
  loadMatrixVariables(numVars);

  // Load inequality constraints
  const bool ineq{true};
  loadMatrixConstraints(matAIneqData, matBIneqData, numVars, numIneqCons, ineq);

  // Load equality constraints
  loadMatrixConstraints(matAEqData, matBEqData, numVars, numEqEqCons, !ineq, numIneqCons);

  // Load objective function
  loadMatrixObjective(matFData, isMaximization);
}  // loadMatrixModel

void ORProcessor::loadMatrixVariables(int numVars)
{
  for (int vidx = 0; vidx < numVars; ++vidx)
  {
    std::stringstream ss;
    ss << "var_" << vidx;
    pSolver->buildBoolVar(ss.str());
  }
}  // loadMatrixVariables

void ORProcessor::loadMatrixConstraints(const utilsreader::CSVReader::CSVData& dataA,
                                        const utilsreader::CSVReader::CSVData& dataB,
                                        int numVars, int numCons, bool isIneq, int startIdx)
{
  if (dataA.empty()) return;

  assert(dataB.size() == numCons);
  const auto& varList = pSolver->getVariableList();
  for (int cidx = 0; cidx < numCons; ++cidx)
  {
    assert(static_cast<int>(dataB.at(cidx).size()) == 1);

    std::stringstream ss;
    ss << "con_" << startIdx + cidx;

    std::shared_ptr<MPConstraint> con;
    if (isIneq)
    {
      con = pSolver->buildRowConstraint(-MPSolver::infinity(), std::stod(dataB.at(cidx).at(0)),
                                        ss.str());
    }
    else
    {
      const auto bound = std::stod(dataB.at(cidx).at(0));
      con = pSolver->buildRowConstraint(bound, bound, ss.str());
    }

    // Set coefficients
    const auto& conRow = dataA.at(cidx);
    assert(static_cast<int>(conRow.size()) == numVars);
    for (int vidx = 0; vidx < numVars; ++vidx)
    {
      con->setCoefficient(varList[vidx], std::stod(conRow.at(vidx)));
    }
  }
}  // loadMatrixConstraints

void ORProcessor::loadMatrixObjective(const utilsreader::CSVReader::CSVData& dataObj,
                                      bool isMaximization)
{
  const auto& varList = pSolver->getVariableList();

  assert(dataObj.size() == 1);
  assert(dataObj.at(0).size() == varList.size());

  auto obj = pSolver->getObjectivePtr();
  int fidx = 0;
  for (const auto& var : varList)
  {
    obj->setCoefficient(var, std::stod(dataObj.at(0).at(fidx++)));
  }

  if (isMaximization)
  {
    obj->setMaximization();
  }
  else
  {
    obj->setMinimization();
  }
}  // loadMatrixObjective


void ORProcessor::loadMPSModel(const std::string& path)
{
  if (!utilsfilesys::doesFileExists(path))
  {
    const std::string errMsg = "ORProcessor - loadMPSModel: file not found " + path;
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  // Load the model into proto object from the file
  const int resLoad = pSolver->loadMPSModelFromFile(path);
  if (resLoad != 0)
  {
    if (resLoad == 2)
    {
      spdlog::warn("ORProcessor - loadMPSModel: invalid mps model " + path);
      pModelInfeasible = true;
    }
  }
}  // loadMPSModel

void ORProcessor::loadProtoModel(const OptModelProto& model)
{
  // Add decision variables to the model
  addProtoVariables(model);

  // Add constraints to the model
  addProtoConstraints(model);

  /// Add objectives/objective functions
  addProtoObjective(model);
}  // loadProtoModel

void ORProcessor::addProtoVariables(const OptModelProto& model)
{
  for (int idx = 0; idx < model.variable_size(); ++idx)
  {
    const OptVariableProto& protoVar = model.variable(idx);

    const std::string& name = protoVar.name();
    const bool isInt = protoVar.is_integer();
    const double objCoeff = protoVar.objective_coefficient();

    MPVariable::SPtr var;
    if (isInt)
    {
      if (isBoolianVar(protoVar))
      {
        var = pSolver->buildBoolVar(name);
      }
      else
      {
        var = pSolver->buildIntVar(protoVar.lower_bound(), protoVar.upper_bound(), name);
      }
    }
    else
    {
      var = pSolver->buildNumVar(protoVar.lower_bound(), protoVar.upper_bound(), name);
    }
    var->setBranchingPriority(protoVar.branching_priority());

    if (objCoeff != 0)
    {
      pObjVarList.push_back({var, objCoeff});
    }
  }
}  // addProtoVariables

void ORProcessor::addProtoConstraints(const OptModelProto& model)
{
  for (int idx = 0; idx < model.constraint_size(); ++idx)
  {
    const OptConstraintProto& protoCon = model.constraint(idx);
    auto con = pSolver->buildRowConstraint(
            protoCon.lower_bound(), protoCon.upper_bound(), protoCon.name());

    if (protoCon.var_index_size() != protoCon.coefficient_size())
    {
      throw std::runtime_error(
              "ORProcessor - addProtoConstraints: "
              "invalid size for variables and coefficients " +
              std::to_string(protoCon.var_index_size()) +
              " != " + std::to_string(protoCon.coefficient_size()));
    }

    const auto& varList = pSolver->getVariableList();
    for (int idx = 0; idx < protoCon.var_index_size(); ++idx)
    {
      const int vidx = protoCon.var_index(idx);
      if (vidx < 0 || vidx >= varList.size())
      {
        throw std::runtime_error(
            "ORProcessor - addProtoConstraints: "
            "invalid variable index " +
            std::to_string(vidx) + " (it should be in [0, " +
            std::to_string(varList.size() - 1) + "]");
      }
      con->setCoefficient(varList[vidx], protoCon.coefficient(idx));
    }
    con->setIsLazy(protoCon.is_lazy());
  }
}  // addProtoConstraints

void ORProcessor::addProtoObjective(const OptModelProto& model)
{
  auto obj = pSolver->getObjectivePtr();
  for (const auto& varCoeff : pObjVarList)
  {
    obj->setCoefficient(varCoeff.first, varCoeff.second);
  }

  obj->setOffset(model.objective_offset());
  if (model.maximize())
  {
    obj->setMaximization();
  }
  else
  {
    obj->setMinimization();
  }
}  // addProtoObjective

bool ORProcessor::interruptSolver()
{
  if (!pSolver)
  {
    return false;
  }

  return pSolver->interruptSolve();
}  // interruptSolver

void ORProcessor::processWork(Work work)
{
  if (work->workType == orengine::ORWork::kRunOptimizer)
  {
    runSolver(work);
  }
  else
  {
    spdlog::warn("ORProcessor - processWork: unrecognized work type");
  }
}  // processWork

void ORProcessor::runSolver(Work& work)
{
  if (pModelInfeasible)
  {
    pResultStatus = lmcommon::ResultStatus::MODEL_INVALID;
    return;
  }

  timer::Timer timer;

  // Run the solver
  pResultStatus = pSolver->solve();

  const auto wallclockMsec = timer.getWallClockTimeMsec();
  work->metricsRegister->setMetric(jsonmetrics::OPTIMIZER_LATENCY_MSEC,
                                   wallclockMsec);

  // Store the solution found
  storeSolution();
}  // runSolver

void ORProcessor::storeSolution()
{
  // Critical section
  LockGuard lock(pResultMutex);

  orengine::MIPResult::Solution& solution = pResult->solution.second;
  orengine::MIPResult::ObjectiveValues& objVals = pResult->solution.first;

  assert(pSolver);

  // Set variables
  for (const auto& var : pSolver->getVariableList())
  {
    solution[var->getName()].push_back({var->getSolutionValue(), var->getSolutionValue()});
  }

  // Set objective
  objVals.push_back(pSolver->getObjectivePtr()->getValue());

  // Set the result status
  setResultStatus();
}  // storeSolution

void ORProcessor::setResultStatus()
{
  switch (pResultStatus)
  {
    case lmcommon::ResultStatus::NOT_SOLVED:
      pResult->resultStatus = orengine::MIPResult::ResultStatus::NOT_SOLVED;
      break;
    case lmcommon::ResultStatus::OPTIMAL:
      pResult->resultStatus = orengine::MIPResult::ResultStatus::OPTIMUM;
      break;
    case lmcommon::ResultStatus::FEASIBLE:
      pResult->resultStatus = orengine::MIPResult::ResultStatus::FEASIBLE;
      break;
    case lmcommon::ResultStatus::INFEASIBLE:
      pResult->resultStatus = orengine::MIPResult::ResultStatus::INFEASIBLE;
      break;
    case lmcommon::ResultStatus::UNBOUNDED:
      pResult->resultStatus = orengine::MIPResult::ResultStatus::UNBOUNDED;
      break;
    default:
      pResult->resultStatus = orengine::MIPResult::ResultStatus::NOT_SOLVED;
      break;
  }
}  // setResultStatus

}  // namespace toolbox
}  // namespace optilab
