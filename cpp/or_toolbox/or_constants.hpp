//
// Copyright OptiLab 2020. All rights reserved.
//
// Constants for OR-Toolbox.
//

#pragma once

#include <string>

namespace optilab {
namespace toolbox {

namespace orconstants {
extern const std::string CSV_MODEL_A_EQ;
extern const std::string CSV_MODEL_A_INEQ;
extern const std::string CSV_MODEL_B_EQ;
extern const std::string CSV_MODEL_B_INEQ;
extern const std::string CSV_MODEL_F_MAX;
extern const std::string CSV_MODEL_F_MIN;
}  // namespace orconstants

}  // namespace toolbox
}  // namespace optilab
