#include "or_toolbox/net_or_node_processor.hpp"

#include <stdexcept>  // for std::runtime_error
#include <string>

#include <spdlog/spdlog.h>

#include "meta_bb/solution.hpp"
#include "or_toolbox/meta_net_solution.hpp"

namespace optilab {
namespace toolbox {

NetORNodeProcessor::NetORNodeProcessor()
: NetORProcessor()
{
}

void NetORNodeProcessor::initNetworkNode()
{
  // Prepare the work to be executed
  bool isRoot{false};
  orengine::ORNetWork::SPtr work = std::make_shared<orengine::ORNetWork>(
          orengine::ORNetWork::WorkType::kNetORInitSystem, isRoot);

  // Execute work
  processWork(work);
}  // initNetworkNode

void NetORNodeProcessor::run()
{
  // Prepare the work to be executed
  bool isRoot{false};
  orengine::ORNetWork::SPtr work = std::make_shared<orengine::ORNetWork>(
          orengine::ORNetWork::WorkType::kNetORRunSystem, isRoot);

  // Execute work
  processWork(work);
}  // run

}  // namespace toolbox
}  // namespace optilab
