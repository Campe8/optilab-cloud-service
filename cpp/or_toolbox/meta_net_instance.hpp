//
// Copyright OptiLab 2020. All rights reserved.
//
// Distributed OR implementation based on metabb framework for the
// Instance class.
//

#pragma once

#include <memory>   // for std::shared_ptr
#include <string>
#include <unordered_map>
#include <utility>  // for std::pair
#include <vector>

#include "meta_bb/framework.hpp"
#include "meta_bb/instance.hpp"
#include "optilab_protobuf/linear_model.pb.h"
#include "optimizer_network/network_packet.hpp"
#include "or_toolbox/linear_model.hpp"
#include "or_toolbox/linear_model_common.hpp"
#include "or_toolbox/linear_solver.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace toolbox {

class SYS_EXPORT_CLASS MetaNetInstance : public metabb::Instance {
 public:
  enum class VarType {
    VT_BINARY,
    VT_INTEGER,
    VT_CONTINUOUS
  };

  using SPtr = std::shared_ptr<MetaNetInstance>;

 public:
  /// Default constructor.
  /// Creates an instance with SCIP as a back-end solver and a random model name
  MetaNetInstance(lmcommon::OptimizationSolverPackageType solverType =
          lmcommon::OptimizationSolverPackageType::OSPT_SCIP);

  /// Constructor: creates a new MetaNetInstance of given name.
  /// The instance holds and manages an MPSolver with an interface implemented
  /// according to the given solver type
  MetaNetInstance(const std::string& name,
                  lmcommon::OptimizationSolverPackageType solverType =
                          lmcommon::OptimizationSolverPackageType::OSPT_SCIP);

  ~MetaNetInstance() override = default;

  /// Returns the solver package type used in this instance
  inline lmcommon::OptimizationSolverPackageType getSolverPackageType() const noexcept
  {
    return pSolverType;
  }

  /// Resets the instance.
  /// This method re-builds all the internal data structures and solvers
  void reset();

  /// Initializes this MetaNetInstance.
  /// This method creates a new instance of MPSolver
  void initInstance(const LinearModelSpecProto& model);

  /// Returns the pointer to the underlying MPSolver
  inline MPSolver::SPtr getMPSolver() const noexcept { return pSolver; };

  /// Given the index of a variable in the model,
  /// returns its type
  VarType getVarType(int idx) const noexcept;

  /// Returns the variable's lower bound.
  /// @note throws std::out_of_range exception on invalid index
  double getVarLb(int idx) const noexcept;

  /// Returns the variable's upper bound.
  /// @note throws std::out_of_range exception on invalid index
  double getVarUb(int idx) const noexcept;

  /// Returns the number of variables in the instance
  int getNumVars() const noexcept;

  /// Convert an internal value to external value
  double convertToExternalValue(double internalValue) const noexcept;

  /// Returns the name of this instance (i.e., the problem's name)
  std::string getName() const noexcept override;

  /// Returns the number of variables in this instance
  int getVarIndexRange() const noexcept override;

  /// Broadcast this instance to all the solvers in the network
  int broadcast(const metabb::Framework::SPtr& framework, int root) noexcept override;

 private:
  /// Pointer to the solver instance
  MPSolver::SPtr pSolver;

  /// Name of the solver and instance
  std::string pSolverName;

  /// Package-type of back-end solver to use
  lmcommon::OptimizationSolverPackageType pSolverType;

  /// Variable indicating whether or not this instance
  /// holds a model, i.e., if the instance has been initialized
  /// and the pMPModelProto object is a valid proto message
  bool pHasModel{false};

  /// Copy of the original model
  LinearModelSpecProto pMPModelProto;

  /// List of variables part of the objective function
  std::vector<std::pair<MPVariable::SPtr, double>> pObjVarList;

  /// Utility data structure holding the type of variables
  /// in the model
  std::unordered_map<int, VarType> pVarIndexTypeMap;

  /// Utility function: parses the model and adds the variables to the model
  void addProtoVariables(const OptModelProto& model);

  /// Utility function: parses the model and adds constraints to the model
  void addProtoConstraints(const OptModelProto& model);

  /// Utility function: adds the objectives to the model
  void addProtoObjective(const OptModelProto& model);
};

}  // namespace metabb

template<> bool optnet::NetworkPacket<LinearModelSpecProto>::operator==(
    const NetworkPacket<LinearModelSpecProto>& other) const noexcept;

template<> std::pair<optnet::BaseNetworkPacket::BaseNetworkPacketType, std::string>
optnet::NetworkPacket<LinearModelSpecProto>::serialize() const noexcept;

}  // namespace optilab

