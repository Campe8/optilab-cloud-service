// Mock interface with no-op methods
// used for unit testing.
#include "or_toolbox/linear_solver.hpp"

#include <stdexcept>  // for std::out_of_bound
#include <string>
#include <vector>

#include <spdlog/spdlog.h>

#include "meta_bb/paramset.hpp"

namespace optilab {
namespace toolbox {

struct MockVariable {
  double lb{0.0};
  double ub{0.0};
  bool integer{false};
};

struct MockConstraint {
  double lb{0.0};
  double ub{0.0};
  double coeff{0.0};
};

class MockInterface : public MPSolverInterface {
 public:
  explicit MockInterface(MPSolver* const solver);

  ~MockInterface() override;

  /// Solves problem with specified parameter values
  lmcommon::ResultStatus solve(const metabb::ParamSet& param) override;

  /// Resets extracted models (from MPSolver to back-end solver)
  void reset() override;

  /// Some interfaces map values into their internal space representation.
  /// This method converts a value from internal to external space.
  /// @default return the same value
  double convertToExternalValue(double internalValue) override;

  /// Some interfaces map values into their internal space representation.
  /// This method converts a value from external to internal space.
  /// @default return the same value
  double convertToInternalValue(double externalValue) override;

  /// Sets the optimization direction (min/max):
  /// - maximize: true; or
  /// - minimize: false
  void setOptimizationDirection(bool maximize) override;

  /// Modifies bounds of an extracted variable.
  /// @note "index" is the index of the variable in the list of variables
  /// in the order created by the caller
  void setVariableBounds(int index, double lb, double ub) override;

  /// Modifies integrality of an extracted variable
  /// @note "index" is the index of the variable in the list of variables
  /// in the order created by the caller
  void setVariableInteger(int index, bool integer) override;

  /// Modify bounds of an extracted variable
  /// @note "index" is the index of the variable in the list of variables
  /// in the order created by the caller
  void setConstraintBounds(int index, double lb, double ub) override;

  /// Adds a linear constraint
  void addRowConstraint(MPConstraint* const ct) override;

  /// Adds a variable
  void addVariable(MPVariable* const var) override;

  /// Changes a coefficient in a constraint
  void setCoefficient(MPConstraint* const constraint, const MPVariable* const variable,
                      double newValue, double oldValue) override;

  /// Clears a constraint from all its terms
  void clearConstraint(MPConstraint* const constraint) override;

  /// Changes a coefficient in the linear objective.
  void setObjectiveCoefficient(const MPVariable* const variable, double coefficient) override;

  /// Changes the constant term in the linear objective
  void setObjectiveOffset(double value) override;

  /// Clears the objective from all its terms.
  void clearObjective() override;

  /// Signals the change in the priority of a variable.
  /// @note "varIndex" is the index of the variable in the list of variables
  /// in the order created by the caller
  void branchingPriorityChangedForVariable(int varIndex) override;

  /// Returns the number of simplex iterations.
  /// @note The problem must be discrete
  int64_t numSimplexIterations() const override;

  /// Returns the number of branch-and-bound nodes.
  /// @note The problem must be discrete
  int64_t numNodes() const override;

  /// Returns the number of nodes left in the tree:
  /// children + siblings + leaves
  int64_t numNodesLeft() const override;

  /// Returns the best objective bound.
  /// @note The problem must be discrete
  double bestObjectiveBound() const override;

  /// Returns true if the problem is continuous,
  /// returns false otherwise
  bool isContinuous() const override
  {
    return false;
  }

  /// Returns true if the problem is continuous and linear,
  /// returns false otherwise
  bool isLP() const override
  {
    return false;
  }

  /// Returns true if the problem is discrete and linear,
  /// returns false otherwise
  bool isMIP() const override
  {
    return true;
  }

  /// Returns the basis status of a row
  lmcommon::BasisStatus rowStatus(int constraintIndex) const override
  {
    return lmcommon::BasisStatus::FREE;
  }

  /// Returns the basis status of a column
  lmcommon::BasisStatus columnStatus(int variableIndex) const override
  {
    return lmcommon::BasisStatus::FREE;
  }

  /// Returns the pointer to the back-end underlying solver
  void* backendSolverPtr() override
  {
    return this;
  }

  void setStartingLPBasis(const std::vector<lmcommon::BasisStatus>& variableStatuses,
                          const std::vector<lmcommon::BasisStatus>& constraintStatuses) override
  {
    spdlog::error("Solver doesn't support setting an LP basis");
  }

  /// Interrupt the solving process of the back-end solver
  bool interruptSolve() override
  {
    return true;
  }

  /// Computes the next MIP solution, if any
  bool nextSolution() override { return false; }

 protected:
  /// Returns the time in msec. taken by the solver to load the model
  uint64_t getModelLoadTimeMsec() const override { return pLoadModelTimeMsec; }

  /// Returns the solve time in msec
  uint64_t getSolveTimeMsec() const override { return pSolveTimeMsec; }

  /// Extracts the variables that have not been extracted yet
  void extractNewVariables() override;

  /// Extracts the constraints that have not been extracted yet
  void extractNewConstraints() override;

  /// Extracts the objective
  void extractObjective() override;

  /// Sets all parameters in the underlying solver
  void setParameters(const metabb::ParamSet& param) override;

  /// Sets each parameter in the underlying solver
  void setRelativeMipGap(double value) override;

  void setPrimalTolerance(double value) override;

  void setDualTolerance(double value) override;

  void setPresolveMode(int value) override;

  /// Sets the number of threads to be used by the solver.
  /// Returns true if the back-end solver allows the caller to set
  /// the number of threads.
  /// Returns false otherwise
  bool setNumThreads(int numThreads) override;

  /// Sets the scaling mode
  void setScalingMode(int value) override;

  /// Sets the LP algorithm to be used by the back-end solver
  void setLPAlgorithm(int value) override;

 private:
  /// Reset flag for branching priority
  bool pBranchingPriorityReset;

  /// Optimization direction
  bool pOptimizationDirection;

  double pRelativeMinGap{-1.0};

  double pPrimalTolerance{-1.0};

  double pDualTolerance{-1.0};

  int pPresolveMode{0};

  int pNumThreads{0};

  /// Time taken to load the model by the SCIP solver in msec.
  uint64_t pLoadModelTimeMsec;

  /// Time taken to solve the model by the SCIP solver in msec.
  uint64_t pSolveTimeMsec;

  /// List of pointers to the SCIP variables
  std::vector<MockVariable> pVariableList;

  /// List of pointers to the SCIP constraints
  std::vector<MockConstraint> pConstraintList;
};

MockInterface::MockInterface(MPSolver* const solver)
: MPSolverInterface(solver),
  pBranchingPriorityReset(false),
  pOptimizationDirection(false),
  pLoadModelTimeMsec(0),
  pSolveTimeMsec(0)
{
  reset();
}

MockInterface::~MockInterface()
{
}

void MockInterface::reset()
{
  pVariableList.clear();
  pConstraintList.clear();
  pVariableList.resize(10);
  pConstraintList.resize(10);
}  // reset

double MockInterface::convertToExternalValue(double internalValue)
{
  return internalValue;
}  // convertToExternalValue

double MockInterface::convertToInternalValue(double externalValue)
{
  return externalValue;
}  // convertToInternalValue

void MockInterface::setOptimizationDirection(bool maximize)
{
  pOptimizationDirection = maximize;
}  // setOptimizationDirection

void MockInterface::setVariableBounds(int index, double lb, double ub)
{
  if (index < 0 || index >= pVariableList.size())
  {
    throw std::out_of_range("MockInterface - setVariableBounds: "
            "out of range value " + std::to_string(index));
  }
  pVariableList[index].lb = lb;
  pVariableList[index].ub = ub;
}  // pVariableList

void MockInterface::setVariableInteger(int index, bool integer)
{
  if (index < 0 || index >= pVariableList.size())
  {
    throw std::out_of_range("MockInterface - setVariableInteger: "
            "out of range value " + std::to_string(index));
  }
  pVariableList[index].integer = integer;
}  // setVariableInteger

void MockInterface::setConstraintBounds(int index, double lb, double ub)
{
  if (index < 0 || index >= pConstraintList.size())
  {
    throw std::out_of_range("MockInterface - setConstraintBounds: "
            "out of range value " + std::to_string(index));
  }
  pConstraintList[index].lb = lb;
  pConstraintList[index].ub = ub;
}  // setConstraintBounds


void MockInterface::addRowConstraint(MPConstraint* const ct)
{
  pSynchStatus = SynchronizationStatus::MUST_RELOAD;
}  // addRowConstraint

void MockInterface::addVariable(MPVariable* const var)
{
  pSynchStatus = SynchronizationStatus::MUST_RELOAD;
}  // addVariable

void MockInterface::setCoefficient(MPConstraint* const constraint, const MPVariable* const variable,
                                   double newValue, double oldValue)
{
}  // setCoefficient

void MockInterface::clearConstraint(MPConstraint* const constraint)
{
}  // clearConstraint

void MockInterface::setObjectiveCoefficient(const MPVariable* const variable, double coefficient)
{
  pSynchStatus = SynchronizationStatus::MUST_RELOAD;
}  // setObjectiveCoefficient

void MockInterface::setObjectiveOffset(double value)
{
  pSynchStatus = SynchronizationStatus::MUST_RELOAD;
}  // setObjectiveOffset

void MockInterface::clearObjective()
{
}  // clearObjective

void MockInterface::branchingPriorityChangedForVariable(int varIndex)
{
}  // branchingPriorityChangedForVariable

int64_t MockInterface::numSimplexIterations() const
{
  return -1;
}  // numSimplexIterations

int64_t MockInterface::numNodes() const
{
  return 100;
}  // numNodes

int64_t MockInterface::numNodesLeft() const
{
  return 100;
}  //numNodesLeft

double MockInterface::bestObjectiveBound() const
{
  return -1.0;
}  // bestObjectiveBound

void MockInterface::extractNewVariables()
{
}  //extractNewVariables

void MockInterface::extractNewConstraints()
{
}  // extractNewConstraints

void MockInterface::extractObjective()
{
}  // extractObjective

void MockInterface::setParameters(const metabb::ParamSet& param)
{
  setCommonParameters(param);
  setMIPParameters(param);
}  // setParameters

void MockInterface::setRelativeMipGap(double value)
{
  pRelativeMinGap = value;
}  // setRelativeMipGap

void MockInterface::setPrimalTolerance(double value)
{
  pPrimalTolerance = value;
}  // setPrimalTolerance

void MockInterface::setDualTolerance(double value)
{
  pDualTolerance = value;
}  // setDualTolerance

void MockInterface::setPresolveMode(int value)
{
  pPresolveMode = value;
}  // setPresolveMode

bool MockInterface::setNumThreads(int numThreads)
{
  pNumThreads = numThreads;
  return true;
}  // setNumThreads

void MockInterface::setScalingMode(int value)
{
  (void) value;
  // no-op (unsupported)
}  // setScalingMode

void MockInterface::setLPAlgorithm(int value)
{
}  // setLPAlgorithm

lmcommon::ResultStatus MockInterface::solve(const metabb::ParamSet& param)
{
  return lmcommon::ResultStatus::OPTIMAL;
}  // solve

}  // namespace toolbox
}  // namespace optilab

namespace optilab {
namespace toolbox {
MPSolverInterface* buildMockInterface(MPSolver* const solver)
{
  return new MockInterface(solver);
}
}  // namespace toolbox
}  // namespace optilab
