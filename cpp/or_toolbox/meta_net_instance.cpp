#include "or_toolbox/meta_net_instance.hpp"

#include <stdexcept>  // for std::runtime_error

#include <google/protobuf/util/message_differencer.h>
#include <spdlog/spdlog.h>

#include "meta_bb/meta_bb_constants.hpp"
#include "utilities/random_generators.hpp"

namespace {
bool isBoolianVar(const optilab::OptVariableProto& var)
{
  return var.is_integer() &&
          (var.lower_bound() == 0.0) &&
          (var.upper_bound() == 1.0);
}  // isBoolianVar
}  // namespace

namespace optilab {
namespace toolbox {

MetaNetInstance::MetaNetInstance(lmcommon::OptimizationSolverPackageType solverType)
: Instance(),
  pSolverName(utilsrandom::buildRandomAlphaNumString(10)),
  pSolverType(solverType)
{
  // The constructor create a new MPSolver instance running a back-end solver
  // from the given package type
  pSolver = std::make_shared<MPSolver>(pSolverName, pSolverType);
}

MetaNetInstance::MetaNetInstance(const std::string& name,
                                 lmcommon::OptimizationSolverPackageType solverType)
: Instance(),
  pSolverName(name),
  pSolverType(solverType)
{
  // The constructor create a new MPSolver instance running a back-end solver
  // from the given package type
  pSolver = std::make_shared<MPSolver>(pSolverName, pSolverType);
}

MetaNetInstance::VarType MetaNetInstance::getVarType(int idx) const noexcept
{
  return pVarIndexTypeMap.at(idx);
}  // getVarType

double MetaNetInstance::getVarLb(int idx) const noexcept
{
  return pSolver->getVarLowerBound(idx);
}  // getVarLb

double MetaNetInstance::getVarUb(int idx) const noexcept
{
  return pSolver->getVarUpperBound(idx);
}  // getVarUb

int MetaNetInstance::getNumVars() const noexcept
{
  return pSolver->getNumVariables();
}  // getNumVars

double MetaNetInstance::convertToExternalValue(double internalValue) const noexcept
{
  return pSolver->convertToExternalValue(internalValue);
}  // convertToExternalValue

std::string MetaNetInstance::getName() const noexcept
{
  return pSolverName;
}  // getName

int MetaNetInstance::getVarIndexRange() const noexcept
{
  return getNumVars();
}  // getVarIndexRange

void MetaNetInstance::initInstance(const LinearModelSpecProto& model)
{
  if (!model.has_model())
  {
    // File model path not supported
    std::string errMsg = "MetaNetInstance - initInstance: "
            "invalid input protobuf model (path to model not supported)";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  // Use the model to initialize the internal MPModel
  const OptModelProto& modelProto = pMPModelProto.model();

  // Add decision variables to the model
  addProtoVariables(modelProto);

  // Add constraints to the model
  addProtoConstraints(modelProto);

  /// Add objectives/objective functions
  addProtoObjective(modelProto);

  // At last, store the instance
  // Save the model
  if (&pMPModelProto != &model)
  {
    pMPModelProto.CopyFrom(model);
  }
  pHasModel = true;
}  // initInstance

void MetaNetInstance::reset()
{
  // Return if the model was not initialized
  if (!pHasModel) return;

  // Clear state
  pHasModel = false;
  pObjVarList.clear();
  pVarIndexTypeMap.clear();

  // Reset the solver
  pSolver.reset();
  pSolver = std::make_shared<MPSolver>(pSolverName, pSolverType);

  // Re-initialize the instance
  initInstance(pMPModelProto);
}  // reset

int MetaNetInstance::broadcast(const metabb::Framework::SPtr& framework, int root) noexcept
{
  if (!framework)
   {
     spdlog::error("MetaNetInstance - broadcast: empty network communicator");
     return metabb::metaconst::ERR_GENERIC_ERROR;
   }

  // Create the instance packet to send to the destination
  optnet::Packet::UPtr packet;
  if (framework->getComm()->getRank() == root)
  {
    // Broadcasting this packet to the network
    packet = optnet::Packet::UPtr(new optnet::Packet());
    packet->networkPacket = optnet::BaseNetworkPacket::UPtr(
            new optnet::NetworkPacket<LinearModelSpecProto>(pMPModelProto));
  }

  // Send/receive the packet
  if (framework->getComm()->broadcast(packet, root))
  {
    return metabb::metaconst::ERR_GENERIC_ERROR;
  }

  if (framework->getComm()->getRank() != root)
  {
    // Packet received
    if (optnet::canCastBaseToNetworkPacket<LinearModelSpecProto>(packet->networkPacket))
    {
      auto nodePacket = optnet::castBaseToNetworkPacket<LinearModelSpecProto>(
              std::move(packet->networkPacket));

      if (nodePacket->size() != 1)
      {
        spdlog::error("MetaNetInstance - receive: wrong packet data size " +
                      std::to_string(nodePacket->size()));
        return metabb::metaconst::ERR_GENERIC_ERROR;
      }
      pMPModelProto.CopyFrom(nodePacket->data[0]);
    }
    else
    {
      return metabb::metaconst::ERR_GENERIC_ERROR;
    }
  }

  // Initialize this instance
  initInstance(pMPModelProto);

  // Return no error
  return metabb::metaconst::ERR_NO_ERROR;
}  // broadcast

void MetaNetInstance::addProtoVariables(const OptModelProto& model)
{
  for (int idx = 0; idx < model.variable_size(); ++idx)
  {
    const OptVariableProto& protoVar = model.variable(idx);

    const std::string& name = protoVar.name();
    const bool isInt = protoVar.is_integer();
    const double objCoeff = protoVar.objective_coefficient();

    MPVariable::SPtr var;
    if (isInt)
    {
      if (isBoolianVar(protoVar))
      {
        var = pSolver->buildBoolVar(name);
        pVarIndexTypeMap[idx] = VarType::VT_BINARY;
      }
      else
      {
        var = pSolver->buildIntVar(protoVar.lower_bound(), protoVar.upper_bound(), name);
        pVarIndexTypeMap[idx] = VarType::VT_INTEGER;
      }
    }
    else
    {
      var = pSolver->buildNumVar(protoVar.lower_bound(), protoVar.upper_bound(), name);
      pVarIndexTypeMap[idx] = VarType::VT_CONTINUOUS;
    }
    var->setBranchingPriority(protoVar.branching_priority());

    if (objCoeff != 0)
    {
      pObjVarList.push_back({var, objCoeff});
    }
  }
}  // addProtoVariables

void MetaNetInstance::addProtoConstraints(const OptModelProto& model)
{
  for (int idx = 0; idx < model.constraint_size(); ++idx)
  {
    const OptConstraintProto& protoCon = model.constraint(idx);
    auto con = pSolver->buildRowConstraint(
            protoCon.lower_bound(), protoCon.upper_bound(), protoCon.name());

    if (protoCon.var_index_size() != protoCon.coefficient_size())
    {
      throw std::runtime_error(
              "MetaNetInstance - addProtoConstraints: "
              "invalid size for variables and coefficients " +
              std::to_string(protoCon.var_index_size()) +
              " != " + std::to_string(protoCon.coefficient_size()));
    }

    const auto& varList = pSolver->getVariableList();
    for (int idx = 0; idx < protoCon.var_index_size(); ++idx)
    {
      const int vidx = protoCon.var_index(idx);
      if (vidx < 0 || vidx >= varList.size())
      {
        throw std::runtime_error(
            "MetaNetInstance - addProtoConstraints: "
            "invalid variable index " +
            std::to_string(vidx) + " (it should be in [0, " +
            std::to_string(varList.size() - 1) + "]");
      }
      con->setCoefficient(varList[vidx], protoCon.coefficient(idx));
    }
    con->setIsLazy(protoCon.is_lazy());
  }
}  // addProtoConstraints

void MetaNetInstance::addProtoObjective(const OptModelProto& model)
{
  auto obj = pSolver->getObjectivePtr();
  for (const auto& varCoeff : pObjVarList)
  {
    obj->setCoefficient(varCoeff.first, varCoeff.second);
  }

  obj->setOffset(model.objective_offset());
  if (model.maximize())
  {
    obj->setMaximization();
  }
  else
  {
    obj->setMinimization();
  }
}  // addProtoObjective

}  // namespace metabb

template<> bool optnet::NetworkPacket<LinearModelSpecProto>::operator==(
    const NetworkPacket<LinearModelSpecProto>& other) const noexcept
{
  if (this->data.size() != other.data.empty()) return false;
  return google::protobuf::util::MessageDifferencer::Equals(this->data[0], other.data[0]);
}

template<>
std::pair<optnet::BaseNetworkPacket::BaseNetworkPacketType, std::string>
optnet::NetworkPacket<LinearModelSpecProto>::serialize() const noexcept
{
  if (data.size() != 1)
  {
    spdlog::error("NetworkPacket<LinearModelSpecProto> - invalid data size");
    return {BaseNetworkPacketType::BNPT_UNSPECIFIED_PACKET, ""};
  }

  try
  {
    return {BaseNetworkPacketType::BNPT_MP_LINEAR_MODEL, data.at(0).SerializeAsString()};
  }
  catch(...)
  {
    spdlog::error("NetworkPacket<LinearModelSpecProto> - "
            "undefined error when serializing protobuf");
    return {BaseNetworkPacketType::BNPT_UNSPECIFIED_PACKET, ""};
  }
}  // serialize

}  // namespace optilab
