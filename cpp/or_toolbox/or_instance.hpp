//
// Copyright OptiLab 2020. All rights reserved.
//
// Base class for a scheduling instance.
//

#pragma once

#include <memory>  // for std::unique_ptr

#include "optilab_protobuf/linear_model.pb.h"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace toolbox {

class SYS_EXPORT_CLASS ORInstance {
 public:
  using UPtr = std::unique_ptr<ORInstance>;
  using SPtr = std::shared_ptr<ORInstance>;

 public:
  ORInstance(const LinearModelSpecProto& schedulingModel)
 : pLinearModel(schedulingModel)
 {
 }

  inline const LinearModelSpecProto& getLinearModel() const noexcept
  {
    return pLinearModel;
  }

 private:
  /// Protobuf containing the linear model
  LinearModelSpecProto pLinearModel;
};

}  // namespace toolbox
}  // namespace optilab
