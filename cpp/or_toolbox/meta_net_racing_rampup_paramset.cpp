#include "or_toolbox/meta_net_racing_rampup_paramset.hpp"

#include <exception>
#include <stdexcept>  // for std::invalid_argument

#include <google/protobuf/util/message_differencer.h>
#include <spdlog/spdlog.h>

#include "meta_bb/meta_bb_constants.hpp"
#include "meta_bb/meta_bb_struct.hpp"
#include "meta_bb/meta_bb_utilities.hpp"

namespace optilab {
namespace toolbox {

int MetaNetRacingRampUpParamSet::send(metabb::Framework::SPtr framework, int destination)
{
  if (!framework)
  {
    spdlog::error("MetaNetRacingRampUpParamSet - send: empty network communicator");
    return metabb::metaconst::ERR_GENERIC_ERROR;
  }

  // Create the optimizer node packet to send to the destination
  optnet::Packet::UPtr packet = buildNetworkPacket();

  // Add the node tag
  packet->packetTag = optnet::PacketTag::UPtr(new metabb::net::MetaBBPacketTag(
          metabb::net::MetaBBPacketTag::PacketTagType::PPT_RACING_RAMP_UP_PARAMSET));

  if (framework->getComm()->send(std::move(packet), destination))
  {
    return metabb::metaconst::ERR_GENERIC_ERROR;
  }

  return metabb::metaconst::ERR_NO_ERROR;
}  // send

int MetaNetRacingRampUpParamSet::upload(optnet::Packet::UPtr packet)
{
  if (!packet)
  {
    spdlog::error("MetaNetRacingRampUpParamSet - upload: empty packet");
    return metabb::metaconst::ERR_GENERIC_ERROR;
  }

  // Check that the received packet is a NODE packet
  try
  {
    const auto tag = metabb::utils::castToMetaBBPacketTagAndGetTagOrThrow(packet->packetTag);
    if (tag != metabb::net::MetaBBPacketTag::PacketTagType::PPT_RACING_RAMP_UP_PARAMSET)
    {
      spdlog::error("MetaNetRacingRampUpParamSet - upload: invalid tag received");
      return metabb::metaconst::ERR_GENERIC_ERROR;
    }
  }
  catch(const std::exception& e)
  {
    spdlog::error(std::string("MetaNetRacingRampUpParamSet - upload: ") + e.what());
    return metabb::metaconst::ERR_GENERIC_ERROR;
  }
  catch(...)
  {
    spdlog::error("MetaNetRacingRampUpParamSet - upload: no tag received");
    return metabb::metaconst::ERR_GENERIC_ERROR;
  }

  // This packet was deserialized as a std::string packet containing
  // the bytes of the serialized original message.
  // Therefore, the packet must be first deserialized into a proper
  // NetworkPacket
  if (packet->networkPacket->packetType !=
          optnet::BaseNetworkPacket::BaseNetworkPacketType::BNPT_META_BB_PACKET)
  {
    spdlog::error("MetaNetRacingRampUpParamSet - upload: invalid network packet type received");
    return metabb::metaconst::ERR_GENERIC_ERROR;
  }

  auto networkPacket = optnet::castBaseToNetworkPacket<std::string>(
          std::move(packet->networkPacket));
  if (!networkPacket)
  {
    spdlog::error("MetaNetRacingRampUpParamSet - upload: "
            "error downcasting to NetworkPacket<std::string>");
    return metabb::metaconst::ERR_GENERIC_ERROR;
  }

  if (networkPacket->size() != 1)
  {
    spdlog::error("MetaNetRacingRampUpParamSet - upload: wrong packet data size " +
                  std::to_string(networkPacket->size()));
    return metabb::metaconst::ERR_GENERIC_ERROR;
  }

  // Get the calculation state proto packet and upload its content into this node
  metabb::RacingRampUpParamSetProtoPacket racingProto;
  racingProto.ParseFromString(networkPacket->data.at(0));

  pTerminationCriteria = static_cast<RacingTerminationCriteria>(racingProto.terminationcriteria());
  pNumNodesLeft = racingProto.numnodesleft();
  pTimeLimitMsec = racingProto.timelimitmsec();
  pRacingParamSeed = racingProto.racingparamseed();
  pPermuteProbSeed = racingProto.permuteprobseed();
  pGenerateBranchOrderSeed = racingProto.generatebranchorderseed();

  return metabb::metaconst::ERR_NO_ERROR;
}  // upload

optnet::Packet::UPtr MetaNetRacingRampUpParamSet::buildNetworkPacket()
{
  // Create the optimizer node packet to send to the destination
  optnet::Packet::UPtr packet(new optnet::Packet());

  // Add the content as protobuf message
  metabb::RacingRampUpParamSetProtoPacket racingProto;
  racingProto.set_terminationcriteria(static_cast<int>(pTerminationCriteria));
  racingProto.set_numnodesleft(pNumNodesLeft);
  racingProto.set_timelimitmsec(pTimeLimitMsec);
  racingProto.set_racingparamseed(pRacingParamSeed);
  racingProto.set_permuteprobseed(pPermuteProbSeed);
  racingProto.set_generatebranchorderseed(pGenerateBranchOrderSeed);

  // Set the protobuf message as new network packet
  packet->networkPacket = optnet::BaseNetworkPacket::UPtr(
      new optnet::NetworkPacket<metabb::RacingRampUpParamSetProtoPacket>(racingProto));

  return std::move(packet);
}  // buildNetworkPacket

}  // namespace toolbox

template<> bool optnet::NetworkPacket<metabb::RacingRampUpParamSetProtoPacket>::operator==(
    const NetworkPacket<metabb::RacingRampUpParamSetProtoPacket>& other) const noexcept
{
  if (this->data.size() != other.data.empty()) return false;
  return google::protobuf::util::MessageDifferencer::Equals(this->data[0], other.data[0]);
}

template<>
std::pair<optnet::BaseNetworkPacket::BaseNetworkPacketType, std::string>
optnet::NetworkPacket<metabb::RacingRampUpParamSetProtoPacket>::serialize() const noexcept
{
  if (data.size() != 1)
  {
    spdlog::error("NetworkPacket<RacingRampUpParamSetProtoPacket> - invalid data size");
    return {BaseNetworkPacketType::BNPT_UNSPECIFIED_PACKET, ""};
  }

  try
  {
    return {BaseNetworkPacketType::BNPT_META_BB_PACKET, data.at(0).SerializeAsString()};
  }
  catch(...)
  {
    spdlog::error("NetworkPacket<NodeProtoPacket> - undefined error when serializing protobuf");
    return {BaseNetworkPacketType::BNPT_UNSPECIFIED_PACKET, ""};
  }
}  // serialize

}  // namespace optilab
