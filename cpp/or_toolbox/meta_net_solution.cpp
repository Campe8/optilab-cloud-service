#include "or_toolbox/meta_net_solution.hpp"

#include <stdexcept>  // for std::runtime_error
#include <string>
#include <utility>    // for std::move

#include "meta_bb/meta_bb_constants.hpp"
#include "meta_bb/meta_bb_struct.hpp"
#include "meta_bb/meta_bb_utilities.hpp"

namespace optilab {
namespace toolbox {

MetaNetSolution::MetaNetSolution()
: Solution(),
  pORSolution(std::make_shared<orengine::MIPResult>())
{
}

double MetaNetSolution::getObjectiveFuntionValue()
{
  return pORSolution->baseSolution.first;
}  // getObjectiveFuntionValue

void MetaNetSolution::uploadSolution(MPSolver::SPtr solver)
{
  if (!solver)
  {
    throw std::runtime_error("MetaNetSolution - uploadSolution: "
            "empty pointer to the solver");
  }

  // Lock the mutex on update
  LockGuard lock(pSolutionMutex);

  // Get the list of variables
  pNumVars = 0;
  const auto& varList = solver->getVariableList();
  (pORSolution->baseSolution).second.resize(varList.size());
  for (const auto& var : varList)
  {
    (pORSolution->baseSolution).second[pNumVars++] = var->getSolutionValue();
  }

  // Set the objective value
  (pORSolution->baseSolution).first = solver->getObjective().getValue();

  // Set the result status
  setResultStatus(solver);
}  // uploadSolution

MetaNetSolution::SolutionList MetaNetSolution::getSolution() const noexcept
{
  // Solution to return
  SolutionList solution;

  // Get the values from the ORSolution
  const auto& sol = pORSolution->baseSolution;
  if (sol.second.empty())
  {
    return solution;
  }

  int idx{0};
  solution.resize(sol.second.size());
  for (auto val : sol.second)
  {
    solution[idx] = {idx, val};
    ++idx;
  }

  return solution;
}  //getSolution

void MetaNetSolution::setResultStatus(MPSolver::SPtr solver)
{
  switch (solver->getSolveStatus())
  {
    case lmcommon::ResultStatus::NOT_SOLVED:
      pORSolution->resultStatus = orengine::MIPResult::ResultStatus::NOT_SOLVED;
      break;
    case lmcommon::ResultStatus::OPTIMAL:
      pORSolution->resultStatus = orengine::MIPResult::ResultStatus::OPTIMUM;
      break;
    case lmcommon::ResultStatus::FEASIBLE:
      pORSolution->resultStatus = orengine::MIPResult::ResultStatus::FEASIBLE;
      break;
    case lmcommon::ResultStatus::INFEASIBLE:
      pORSolution->resultStatus = orengine::MIPResult::ResultStatus::INFEASIBLE;
      break;
    case lmcommon::ResultStatus::ABNORMAL:
      pORSolution->resultStatus = orengine::MIPResult::ResultStatus::INFEASIBLE;
      break;
    case lmcommon::ResultStatus::UNBOUNDED:
      pORSolution->resultStatus = orengine::MIPResult::ResultStatus::ABNORMAL;
      break;
    default:
      pORSolution->resultStatus = orengine::MIPResult::ResultStatus::NOT_SOLVED;
      break;
  }
}  // setResultStatus

metabb::Solution::SPtr MetaNetSolution::clone()
{
  auto sol = std::make_shared<MetaNetSolution>();

  // Lock the mutex on clone
  LockGuard lock(pSolutionMutex);

  sol->pORSolution->baseSolution = pORSolution->baseSolution;
  sol->pORSolution->resultStatus = pORSolution->resultStatus;

  // Unlock the mutex when returning from this call
  return sol;
}  //clone

int MetaNetSolution::send(const metabb::Framework::SPtr& framework, int destination) noexcept
{
  if (!framework)
  {
    spdlog::error("MetaNetSolution - send: empty framework");
    return metabb::metaconst::ERR_GENERIC_ERROR;
  }

  // Upload the solution before sending it
  pORSolution->uploadToProtoMessage();

  // Create the optimizer node packet to send to the destination
  optnet::Packet::UPtr packet(new optnet::Packet());

  // Set the protobuf message as new network packet
  packet->networkPacket = optnet::BaseNetworkPacket::UPtr(
          new optnet::NetworkPacket<LinearModelSolutionProto>(pORSolution->solutionProto));

  // Add the node tag
  packet->packetTag = optnet::PacketTag::UPtr(new metabb::net::MetaBBPacketTag(
          metabb::net::MetaBBPacketTag::PacketTagType::PPT_SOLUTION));

  if (framework->getComm()->send(std::move(packet), destination))
  {
    return metabb::metaconst::ERR_GENERIC_ERROR;
  }

  return metabb::metaconst::ERR_NO_ERROR;
}  // send

/// Uploads the content of the given network packet into this solution.
/// If the given packet is not a node network packet, throws std::runtime_error
/// Returns zero on success, non-zero otherwise
int MetaNetSolution::upload(optnet::Packet::UPtr packet) noexcept
{
  if (!packet)
  {
    spdlog::error("MetaNetSolution - upload: empty packet");
    return metabb::metaconst::ERR_GENERIC_ERROR;
  }

  // Check that the received packet is a NODE packet
  try
  {
    const auto tag = metabb::utils::castToMetaBBPacketTagAndGetTagOrThrow(packet->packetTag);
    if (tag != metabb::net::MetaBBPacketTag::PacketTagType::PPT_SOLUTION)
    {
      spdlog::error("MetaNetSolution - upload: invalid tag received");
      return metabb::metaconst::ERR_GENERIC_ERROR;
    }
  }
  catch(...)
  {
    spdlog::error("MetaNetSolution - upload: no tag received");
    return metabb::metaconst::ERR_GENERIC_ERROR;
  }

  auto solPacket = optnet::castBaseToNetworkPacket<LinearModelSolutionProto>(
          std::move(packet->networkPacket));

  if (!solPacket)
  {
    spdlog::error("MetaNetSolution - upload: "
            "error casting the packet to a LinearModelSolutionProto packet");
    return metabb::metaconst::ERR_GENERIC_ERROR;
  }

  if (solPacket->size() != 1)
  {
    spdlog::error(
            "MetaNetSolution - receive: "
            "wrong packet data size " + std::to_string(solPacket->size()));
    return metabb::metaconst::ERR_GENERIC_ERROR;
  }

  // Get the node proto packet and upload its content into this solution
  const LinearModelSolutionProto& solProto = solPacket->data.at(0);

  // Set status
  switch (solProto.status())
  {
    case LinearModelSolutionStatusProto::SOLVER_FEASIBLE:
    {
      pORSolution->resultStatus = orengine::MIPResult::ResultStatus::FEASIBLE;
      break;
    }
    case LinearModelSolutionStatusProto::SOLVER_INFEASIBLE:
    {
      pORSolution->resultStatus = orengine::MIPResult::ResultStatus::INFEASIBLE;
      break;
    }
    case LinearModelSolutionStatusProto::SOLVER_NOT_SOLVED:
    {
      pORSolution->resultStatus = orengine::MIPResult::ResultStatus::NOT_SOLVED;
      break;
    }
    case LinearModelSolutionStatusProto::SOLVER_OPTIMAL:
    {
      pORSolution->resultStatus = orengine::MIPResult::ResultStatus::OPTIMUM;
      break;
    }
    case LinearModelSolutionStatusProto::SOLVER_ABNORMAL:
    {
      pORSolution->resultStatus = orengine::MIPResult::ResultStatus::ABNORMAL;
      break;
    }
    case LinearModelSolutionStatusProto::SOLVER_MODEL_INVALID:
    {
      pORSolution->resultStatus = orengine::MIPResult::ResultStatus::MODEL_INVALID;
      break;
    }
    default:
    {
      pORSolution->resultStatus = orengine::MIPResult::ResultStatus::UNBOUNDED;
      break;
    }
  }

  // Set objective value
  pORSolution->baseSolution.first = solProto.objective_value();

  // Set variable values
  pNumVars = solProto.variable_assign_size();
  for (int idx = 0; idx < pNumVars; ++idx)
  {
    pORSolution->baseSolution.second.push_back(solProto.variable_assign(idx).var_value(0));
  }

  return metabb::metaconst::ERR_NO_ERROR;
}  // upload

int MetaNetSolution::broadcast(const metabb::Framework::SPtr& framework, int root) noexcept
{
  if (!framework)
  {
    spdlog::error("MetaNetSolution - broadcast: empty framework");
    return metabb::metaconst::ERR_GENERIC_ERROR;
  }

  // Broadcast or received a broadcasted packet containing a node
  optnet::Packet::UPtr packet;
  if (framework->getComm()->getRank() == root)
  {
    // This is the broadcasting node.
    // Create the optimizer node packet to send to the destination

    // Upload the solution before sending it
    pORSolution->uploadToProtoMessage();

    // Create the optimizer node packet to send to the destination
    optnet::Packet::UPtr packet(new optnet::Packet());

    // Set the protobuf message as new network packet
    packet->networkPacket = optnet::BaseNetworkPacket::UPtr(
            new optnet::NetworkPacket<LinearModelSolutionProto>(pORSolution->solutionProto));
  }

  // Send/receive the node
  if (framework->getComm()->broadcast(packet, root))
  {
    return metabb::metaconst::ERR_GENERIC_ERROR;
  }

  if (framework->getComm()->getRank() != root)
  {
    const auto errCode = upload(std::move(packet));
    if (errCode != metabb::metaconst::ERR_NO_ERROR) return errCode;
  }

  return metabb::metaconst::ERR_NO_ERROR;
}  // broadcast

}  // namespace metabb

template<> bool optnet::NetworkPacket<LinearModelSolutionProto>::operator==(
    const NetworkPacket<LinearModelSolutionProto>& other) const noexcept
{
  if (this->data.size() != other.data.empty()) return false;
  return google::protobuf::util::MessageDifferencer::Equals(this->data[0], other.data[0]);
}

template<> std::pair<optnet::BaseNetworkPacket::BaseNetworkPacketType, std::string>
optnet::NetworkPacket<LinearModelSolutionProto>::serialize() const noexcept
{
  if (data.size() != 1)
  {
    spdlog::error("NetworkPacket<LinearModelSolutionProto> - invalid data size");
    return {BaseNetworkPacketType::BNPT_UNSPECIFIED_PACKET, ""};
  }

  try
  {
    return {BaseNetworkPacketType::BNPT_MP_LINEAR_MODEL_SOLUTION, data.at(0).SerializeAsString()};
  }
  catch(...)
  {
    spdlog::error("NetworkPacket<LinearModelSolutionProto> - "
            "undefined error when serializing protobuf");
    return {BaseNetworkPacketType::BNPT_UNSPECIFIED_PACKET, ""};
  }
}  // serialize
}  // namespace optilab
