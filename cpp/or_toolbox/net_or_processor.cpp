#include "or_toolbox/net_or_processor.hpp"

#include <spdlog/spdlog.h>

#include <cassert>
#include <exception>
#include <functional>  // for std::bind
#include <limits>      // for std::numeric_limits
#include <sstream>
#include <stdexcept>   // for std::runtime_error

#include "data_structure/metrics/metrics_register.hpp"
#include "engine/engine_constants.hpp"
#include "meta_bb/load_manager.hpp"
#include "meta_bb/meta_bb_struct.hpp"
#include "meta_bb/meta_bb_utilities.hpp"
#include "meta_bb/net_node.hpp"
#include "meta_bb/tree_id.hpp"
#include "optimizer_network/network_packet.hpp"
#include "optimizer_network/packet.hpp"
#include "or_toolbox/linear_model_common.hpp"
#include "or_toolbox/meta_net_framework.hpp"
#include "or_toolbox/meta_net_initiator.hpp"
#include "or_toolbox/meta_net_instance.hpp"
#include "or_toolbox/meta_net_paramset.hpp"
#include "or_toolbox/meta_net_racing_rampup_paramset.hpp"
#include "or_toolbox/meta_net_solver.hpp"
#include "utilities/timer.hpp"

namespace {
constexpr int kRootRank = 0;
constexpr int kRampUpPhaseNormal = 0;
constexpr int kRampUpPhaseRacing = 1;
constexpr int kRampUpPhaseRebuildTreeAfterRacing = 2;
}  // namespace

namespace optilab {
namespace toolbox {

NetORProcessor::NetORProcessor()
: BaseProcessor("NetORProcessor")
{
}

NetORProcessor::~NetORProcessor()
{
  if (pProcCollection.framework)
  {
    pProcCollection.framework.reset();
  }

  if (pProcCollection.initiator)
  {
    pProcCollection.initiator.reset();
  }

  if (pProcCollection.instance)
  {
    pProcCollection.instance.reset();
  }
}  // NetORProcessor

void NetORProcessor::processWork(Work work)
{
  const auto workType = work->workType;
  switch(workType)
  {
    case orengine::ORNetWork::WorkType::kNetORLoadModel:
    {
      loadModel(work);
      break;
    }
    case orengine::ORNetWork::WorkType::kNetORInitSystem:
    {
      initNetSystem(work);
      break;
    }
    case orengine::ORNetWork::WorkType::kNetORRunSystem:
    {
      runFramework(work);
      break;
    }
    case orengine::ORNetWork::WorkType::kNetORCollectSolutions:
    {
      collectSolutions(work);
      break;
    }
    default:
    {
      spdlog::error("NetORProcessor - processWork: unrecognized work type");
      break;
    }
  }
}  // processWork

void NetORProcessor::loadModel(Work& work)
{
  if (!work->isRootWork)
  {
    spdlog::error("NetORProcessor - loadModel: cannot load a model on a non-root node");
    return;
  }

  // Create a MetaNetInstance that will be used to build an initiator.
  // Notice that by default the instance creates a SCIP solver behind the scenes.
  // If another type of solver is requires, provide its type as input parameter
  // to the MetaNetInstance constructor
  const auto& lmInstance = work->lmInstance;
  pProcCollection.instance = std::make_shared<MetaNetInstance>();

  // Initialize the instance on the given input model
  pProcCollection.instance->initInstance(lmInstance);
}  // loadModel

void NetORProcessor::initNetSystem(Work& work)
{
  // Initialize the processor w.r.t. its role in the network (i.e., root vs standard node)
  const auto isRoot = work->isRootWork;
  if (isRoot)
  {
    initNetRootSystem();
  }
  else
  {
    initNetNodeSystem();
  }
}  // initNetSystem

void NetORProcessor::runFramework(Work& work)
{
  // Initialize the processor w.r.t. its role in the network (i.e., root vs standard node)
  const auto isRoot = work->isRootWork;
  if (isRoot)
  {
    runNetRootSystem();
  }
  else
  {
    runNetNodeSystem();
  }
}  // runFramework

void NetORProcessor::initNetRootSystem()
{
  // Create the parameter set.
  // TODO Create the paramset with the input from file/user
  pProcCollection.paramset = std::make_shared<MetaNetParamSet>();

  // Set verbose mode
  (pProcCollection.paramset->getDescriptor()).set_quiet(false);

  // Set solver package instance type
  (pProcCollection.paramset->getDescriptor()).set_optimizationsolverpackagetype(
          static_cast<int>(pProcCollection.instance->getSolverPackageType()));

  // Set normal ramp-up.
  // TODO implement racing ramp-up (parameters in back-end solver interface)
  (pProcCollection.paramset->getDescriptor()).set_rampupphaseprocess(kRampUpPhaseNormal);

  // Create time and initialize the root framework
  const bool isRoot{true};
  pProcCollection.timer = std::make_shared<timer::Timer>();
  pProcCollection.framework = std::make_shared<MetaNetFramework>(isRoot);
  pProcCollection.framework->init();
  assert(pProcCollection.framework->getComm());

  // Broadcast the parameter set to all nodes in the network
  std::stringstream ss1;
  ss1 << "NetORProcessor - initNetRootSystem: broadcasting parameter set at (msec) " <<
          pProcCollection.timer->getWallClockTimeMsec();
  spdlog::info(ss1.str());
  pProcCollection.paramset->broadcast(pProcCollection.framework, kRootRank);

  // Create the initiator
  pProcCollection.initiator = std::make_shared<MetaNetInitiator>(
          pProcCollection.instance,
          pProcCollection.framework,
          pProcCollection.timer);

  // Initialize the initiator based on the input paramset
  if (pProcCollection.initiator->init(pProcCollection.paramset))
  {
    std::string errMsg = "NetORProcessor - initNetRootSystem: "
            "error on initiator initialization";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  // Broadcast the problem instance to the nodes
  pProcCollection.initiator->getInstance()->broadcast(pProcCollection.framework, kRootRank);

  // Send initialization message to the nodes
  pProcCollection.initiator->sendSolverInitializationMessage();
  std::stringstream ss2;
  ss2 << "NetORProcessor - initNetRootSystem: "
          "problem instance sent to solvers at (msec) " <<
          pProcCollection.timer->getWallClockTimeMsec();
  spdlog::info(ss2.str());

  // Instantiate the LoadManager
  pProcCollection.loadManager = std::make_shared<metabb::LoadManager>(
          pProcCollection.framework,
          pProcCollection.paramset,
          pProcCollection.initiator,
          pProcCollection.timer);

  std::stringstream ss3;
  ss3 << "NetORProcessor - initNetRootSystem: "
          "LoadManager instantiated at (msec) " <<
          pProcCollection.timer->getWallClockTimeMsec();
  spdlog::info(ss3.str());
}  // initNetRootSystem

void NetORProcessor::initNetNodeSystem()
{
  // Create the parameter set
  pProcCollection.paramset = std::make_shared<MetaNetParamSet>();

  // Create timer and initialize a non-root framework
  const bool isRoot{false};
  pProcCollection.timer = std::make_shared<timer::Timer>();
  pProcCollection.framework = std::make_shared<MetaNetFramework>(isRoot);

  pProcCollection.framework->init();
  assert(pProcCollection.framework->getComm());

  // Receive the broadcasted parameters set from the root node
  pProcCollection.paramset->broadcast(pProcCollection.framework, kRootRank);

  // Create and receive the problem instance from the root node
  pProcCollection.instance = pProcCollection.framework->buildInstance(
          static_cast<lmcommon::OptimizationSolverPackageType>(
                  pProcCollection.paramset->getDescriptor().optimizationsolverpackagetype()));
  pProcCollection.instance->broadcast(pProcCollection.framework, kRootRank);

  // Create the solver running the model.
  // @note the solver will wait for initialization messages from the LoadManager
  pProcCollection.solver = std::make_shared<MetaNetSolver>(
          pProcCollection.framework,
          pProcCollection.paramset,
          pProcCollection.instance,
          pProcCollection.timer);
}  // initNetRootSystem

void NetORProcessor::runNetRootSystem()
{
  if (pProcCollection.loadManager == nullptr)
  {
    throw std::runtime_error("NetORProcessor - runNetRootSystem: "
            "empty pointer to the LoadManager instance");
  }

  // Runs the load manager owned by the root node
  if (pProcCollection.paramset->getDescriptor().rampupphaseprocess() == kRampUpPhaseNormal)
  {
    // Create the root node:
    // - default node id (undefined, -1),
    // - default generator node id (undefined, -1),
    // - depth tree zero,
    // - dual bound -inf,
    // - original dual bound -inf,
    // - estimated value -inf,
    // - root node diff subproblem
    auto rootNode = std::make_shared<metabb::NetNode>(
            metabb::NodeId(),
            metabb::NodeId(),
            0,
            std::numeric_limits<double>::lowest(),
            std::numeric_limits<double>::lowest(),
            std::numeric_limits<double>::lowest(),
            pProcCollection.initiator->buildRootNodeDiffSubproblem());

    // Run the load manger
    spdlog::info(std::string("NetORProcessor - runNetRootSystem: "
            "start normal ramp-up at (msec) ") +
                 std::to_string(pProcCollection.timer->getWallClockTimeMsec()));
    pProcCollection.loadManager->run(rootNode);
  }
  else if ((pProcCollection.paramset->getDescriptor().rampupphaseprocess() == kRampUpPhaseRacing) ||
           (pProcCollection.paramset->getDescriptor().rampupphaseprocess() ==
                   kRampUpPhaseRebuildTreeAfterRacing))
  {
    // Generate the racing ramp-up parameter set
    std::vector<metabb::RacingRampUpParamSet::SPtr> racingList(
            pProcCollection.framework->getNetworkSize());
    pProcCollection.initiator->generateRacingRampUpParameterSets(racingList);

    // Send the rampup parameters to each node-solver
    for (int nodeIdx = 1; nodeIdx < pProcCollection.framework->getNetworkSize(); ++nodeIdx)
    {
      racingList[nodeIdx-1]->send(pProcCollection.framework, nodeIdx);
    }

    // Create the node to run the load manager on:
    // - default node id (undefined, -1),
    // - default generator node id (undefined, -1),
    // - depth tree zero,
    // - dual bound -inf,
    // - original dual bound -inf,
    // - estimated value -inf,
    // - root node diff subproblem
    auto rootNode = std::make_shared<metabb::NetNode>(
            metabb::NodeId(),
            metabb::NodeId(),
            0,
            std::numeric_limits<double>::lowest(),
            std::numeric_limits<double>::lowest(),
            std::numeric_limits<double>::lowest(),
            pProcCollection.initiator->buildRootNodeDiffSubproblem());

    // Run the load manger
    spdlog::info(std::string("NetORProcessor - runNetRootSystem: "
            "start racing ramp-up at (msec) ") +
                 std::to_string(pProcCollection.timer->getWallClockTimeMsec()));
    pProcCollection.loadManager->run(
            rootNode,
            (pProcCollection.framework->getNetworkSize() - 1),
            racingList);

    // Delete all the racing parameter sets
    racingList.clear();
  }
  else
  {
    throw std::runtime_error("NetORProcessor - runNetRootSystem: "
            "invalid run type (normal/racing rampup)");
  }
}  //runNetRootSystem

void NetORProcessor::runNetNodeSystem()
{
  if (pProcCollection.solver == nullptr)
  {
    throw std::runtime_error("NetORProcessor - runNetNodeSystem: "
            "empty pointer to the Solver instance");
  }

  if (pProcCollection.paramset->getDescriptor().rampupphaseprocess() == kRampUpPhaseNormal ||
          pProcCollection.solver->isWarmStarted())
  {
    // Run the solver
    pProcCollection.solver->run();
  }
  else if ((pProcCollection.paramset->getDescriptor().rampupphaseprocess() == kRampUpPhaseRacing) ||
           (pProcCollection.paramset->getDescriptor().rampupphaseprocess() ==
                   kRampUpPhaseRebuildTreeAfterRacing))
  {
    // Listen for incoming packets
    optnet::Packet::UPtr recvPacket(new optnet::Packet());
    pProcCollection.framework->getComm()->probe(recvPacket);

    // Get the tag
    auto tag = metabb::utils::castToMetaBBPacketTagAndGetTagOrThrow(recvPacket->packetTag);
    if (tag == metabb::net::MetaBBPacketTag::PacketTagType::PPT_RACING_RAMP_UP_PARAMSET)
    {
      auto racingParams = std::make_shared<MetaNetRacingRampUpParamSet>();
      racingParams->upload(std::move(recvPacket));

      // Run the solver
      pProcCollection.solver->run(racingParams);
    }
    else if (tag == metabb::net::MetaBBPacketTag::PacketTagType::PPT_TERMINATE_REQUEST)
    {
      // Terminate request message is sent by the LoadManager on tear-down.
      // Nothing to do here, terminate and return
    }
    else
    {
      throw std::runtime_error("NetORProcessor - runNetNodeSystem: "
              "packet tag not recognized");
    }
  }
  else
  {
    throw std::runtime_error("NetORProcessor - runNetNodeSystem: "
            "invalid run type (normal/racing rampup)");
  }
}  // runNetNodeSystem

void NetORProcessor::collectSolutions(Work& work)
{
  if (!work->isRootWork)
  {
    spdlog::error("NetORProcessor - collectSolutions: "
            "cannot collect solutions on a non-root node");
    return;
  }

  // Call the destructor of the load manager.
  // This will send terminate messages to all solvers and collects the solutions
  if (!pProcCollection.loadManager)
  {
    spdlog::warn("NetORProcessor - collectSolutions: "
            "no load manager found, return");
    return;
  }

  // Call the load manager destructor
  pProcCollection.loadManager.reset();

  // The initiator contains the global best incumbent solution found
  if (!pProcCollection.initiator)
  {
    spdlog::error("NetORProcessor - collectSolutions: "
            "no initiator found, return");
    return;
  }

  auto solution = pProcCollection.initiator->getGlobalBestIncumbentSolution();
  if (!solution)
  {
    spdlog::error("NetORProcessor - collectSolutions: "
            "no solution found, return");
    return;
  }
}  // collectSolutions

}  // namespace toolbox
}  // namespace optilab
