//
// Copyright OptiLab 2019. All rights reserved.
//
// Base class for ORTools engines.
// ORTools engines are engines that solve satisfaction and optimization
// problems using the ORTools libraries.
//

#pragma once

#include <memory>  // for std::shared_ptr
#include <string>

#include "optilab_protobuf/linear_model.pb.h"
#include "or_toolbox/or_instance.hpp"
#include "or_toolbox/or_utilities.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace toolbox {

class SYS_EXPORT_CLASS OREngine
{
 public:
  using SPtr = std::shared_ptr<OREngine>;

 public:
  virtual ~OREngine() = default;

  /// Registers the given optimizer instance specified by the given protobuf
  /// message
  /// @note this method does not run the model.
  /// @throw std::runtime_error if this model is called while the engine is
  /// running
  virtual void registerInstance(ORInstance::UPtr instance) = 0;

  /// Notifies the engine on a given EngineEvent
  virtual void notifyEngine(const orengine::OREvent& event) = 0;

  /// Wait for the current jobs in the queue to finish.
  /// @note if "timeoutSec" is -1, waits for all jobs in the queue to complete
  virtual void engineWait(int timeoutMsec) = 0;

  /// Shuts down the engine
  virtual void turnDown() = 0;
};

}  // namespace toolbox
}  // namespace optilab
