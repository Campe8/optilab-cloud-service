//
// Copyright OptiLab 2020. All rights reserved.
//
// Class for MP engines.
// MP engines are engines that solve Mathematical Programming
// problems such as MIP, LP, IP.
//

#pragma once

#include <memory>  // for std::shared_ptr
#include <string>

#include "data_structure/metrics/metrics_register.hpp"
#include "engine/engine_callback_handler.hpp"
#include "optilab_protobuf/linear_model.pb.h"
#include "or_toolbox/or_engine.hpp"
#include "or_toolbox/or_instance.hpp"
#include "or_toolbox/or_optimizer.hpp"
#include "or_toolbox/or_utilities.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace toolbox {

class SYS_EXPORT_CLASS ORMPEngine : public OREngine{
 public:
  using SPtr = std::shared_ptr<ORMPEngine>;

 public:
  ORMPEngine(const std::string& engineId, const EngineCallbackHandler::SPtr& handler);

  ~ORMPEngine() override;

  /// Registers the given (linear) model specified by the given protobuf message
  /// @note this method does not run the model.
  /// @throw std::runtime_error if this model is called while the engine is running
  void registerInstance(ORInstance::UPtr instance) override;

  /// Notifies the engine on a given EngineEvent
  void notifyEngine(const orengine::OREvent& event) override;

  /// Wait for the current jobs in the queue to finish.
  /// @note if "timeoutSec" is -1, waits for all jobs in the queue to complete
  void engineWait(int timeoutMsec) override;

  /// Shuts down the engine
  void turnDown() override;

 private:
  /// Engine identifier
  std::string pEngineId;

  /// Handler for callbacks to send back results to caller methods
  EngineCallbackHandler::SPtr pCallbackHandler;

  /// Pointer to the result instance collecting result from the optimizer
  orengine::ORResult::SPtr pMIPResult;

  /// Latency in msec. to create this engine
  std::atomic<uint64_t> pEngineCreationTimeMsec{0};

  /// Latency in msec. caused by loading a model into the optimizer
  std::atomic<uint64_t> pLoadModelLatencyMsec{0};

  /// Latency in msec. caused by running the optimizer
  std::atomic<uint64_t> pOptimizerLatencyMsec{0};

  /// Register for metrics
  MetricsRegister::SPtr pMetricsRegister;

  /// Flag indicating whether this engine already started running
  std::atomic<bool> pActiveEngine{false};

  /// Asynchronous optimizer
  OROptimizer::SPtr pOptimizer;

  // Builds the instance of a ORTools optimizer with a MIP processor
  void buildOptimizer();

  /// Runs the registered model
  void processRunModelEvent();

  /// Sends back using the handler all the solutions collected so far
  void processCollectSolutionsEvent(int numSolutions);

  /// Interrupts the current computation, if any, and returns
  void processInterruptEngineEvent();

  /// Collects metrics in the internal metrics register
  void collectMetrics();
};

}  // namespace toolbox
}  // namespace optilab
