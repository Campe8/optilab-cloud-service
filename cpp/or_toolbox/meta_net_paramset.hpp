//
// Copyright OptiLab 2020. All rights reserved.
//
// Distributed OR implementation based on metabb framework for the
// ParamSet class.
//

#pragma once

#include <memory>  // for std::shared_ptr

#include "meta_bb/net_paramset.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace  toolbox {

class SYS_EXPORT_CLASS MetaNetParamSet : public metabb::NetParamSet {
 public:
  using SPtr = std::shared_ptr<MetaNetParamSet>;
};

}  // namespace metabb
}  // namespace optilab

