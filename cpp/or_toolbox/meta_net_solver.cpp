#include "or_toolbox/meta_net_solver.hpp"

#include <algorithm>  // for std::max
#include <cassert>
#include <cmath>      // for ceil
#include <cstdint>    // for uint64_t
#include <stdexcept>  // for std::runtime_error
#include <string>
#include <utility>    // for std::make_pair

#include <spdlog/spdlog.h>

#include "meta_bb/meta_bb_constants.hpp"
#include "meta_bb/meta_bb_macros.hpp"
#include "optilab_protobuf/meta_bb_packet.pb.h"
#include "optimizer_network/network_packet.hpp"
#include "optimizer_network/packet.hpp"
#include "or_toolbox/meta_net_initial_stat.hpp"
#include "or_toolbox/meta_net_instance.hpp"
#include "or_toolbox/meta_net_paramset.hpp"
#include "or_toolbox/meta_net_solution.hpp"

namespace {
constexpr int kRootRank = 0;

// LM sends the root node to all solvers which start solving it immediately.
// Each solver uses a different version of parameter settings, branching variable selection,
// and permutation of variables and constraints, in order to generate different search trees
constexpr int kRampUpPhaseRacing = 1;

// Same as "kRampUpPhaseRacing" but rebuilds the tree after racing and proceeds from there
constexpr int kRampUpPhaseRebuildTreeAfterRacing = 2;
}  // namespace

namespace optilab {
namespace toolbox {

MetaNetSolver::MetaNetSolver(metabb::Framework::SPtr framework,
                             metabb::ParamSet::SPtr paramSet, metabb::Instance::SPtr instance,
                             timer::Timer::SPtr timer)
: metabb::Solver(framework, paramSet, instance, timer)
{
  // Notice that the input instance contains both the MPSolver and the MP model.
  // This data should be received from a broadcasted message from the root node
  auto metaNetInstance = std::dynamic_pointer_cast<MetaNetInstance>(pInstance);
  if (!metaNetInstance)
  {
    throw std::invalid_argument("MetaSolver: invalid downcast to MetaNetInstance");
  }

  // Get the MPSolver instance already allocated from the MetaNetInstance
  pSolver = metaNetInstance->getMPSolver();
  if (!pSolver)
  {
    throw std::invalid_argument("MetaSolver: the input instance does not contain "
            "a valid MPSolver");
  }

  // Set the timeout w.r.t. this point
  if (pParamSet->getDescriptor().timelimitmsec() > 0)
  {
    auto diffTime = pParamSet->getDescriptor().timelimitmsec() - pTimer->getWallClockTimeMsec();
    pSolver->setTimeoutMsec(diffTime);
  }

  // Get the init. message broadcasted by the initiator from the root node
  optnet::Packet::UPtr recvPacket;
  pFramework->getComm()->broadcast(recvPacket, kRootRank);
  if (!optnet::canCastBaseToNetworkPacket<metabb::InitiatorInitSystemMessageProtoPacket>(
          recvPacket->networkPacket))
  {
    throw std::runtime_error("MetaNetSolver - MetaNetSolver: "
            "invalid downcast to metabb::InitiatorInitSystemMessageProtoPacket");
  }
  const auto rank = std::to_string(pFramework->getComm()->getRank());
  spdlog::info(std::string("MetaNetSolver - solver (rank) ") + rank + ": initiator message received");

  auto initMessage = optnet::castBaseToNetworkPacket<metabb::InitiatorInitSystemMessageProtoPacket>(
          std::move(recvPacket->networkPacket));
  if (initMessage->size() != 1)
  {
    throw std::runtime_error(std::string("MetaNetSolver - MetaNetSolver: "
            "invalid message size ") + std::to_string(initMessage->size()));
  }
  auto initProtoMessage = initMessage->data[0];
  pWarmStarted = initProtoMessage.warmstarted();
  pGlobalBestIncumbentValue = initProtoMessage.incumbentvalue();
  if (initProtoMessage.solutionexists())
  {
    // Get the solution...
    // TODO
  }

  // Set epsilon
  pEps = metabb::metaconst::DEAFULT_NUM_EPSILON;

  // Save original problem bounds
  saveOriginalProblemBounds();

  // Save original priority for changing node selector
  saveOriginalPriority();
  spdlog::info(std::string("MetaNetSolver - solver (rank) ") + rank + ": constructor done");
}

void MetaNetSolver::saveOriginalPriority()
{
  /// TODO implement this method
}  // saveOriginalPriority

void MetaNetSolver::saveOriginalProblemBounds()
{
  pNumOriginalVars = pSolver->getNumVariables();
  pInitialVarLBList.resize(pNumOriginalVars);
  pInitialVarUBList.resize(pNumOriginalVars);
  if ((pParamSet->getDescriptor().rampupphaseprocess() == kRampUpPhaseRacing ||
          pParamSet->getDescriptor().rampupphaseprocess() == kRampUpPhaseRebuildTreeAfterRacing) &&
          pParamSet->getDescriptor().communicatetighterboundsinracing())
  {
    pTightenedVarLBList.resize(pNumOriginalVars);
    pTightenedVarUBList.resize(pNumOriginalVars);
    for (int varIdx = 0; varIdx < pNumOriginalVars; ++varIdx)
    {
      pTightenedVarLBList[varIdx] = pInitialVarLBList[varIdx] =
              pSolver->getVarOrigLowerBound(varIdx);
      pTightenedVarUBList[varIdx] = pInitialVarUBList[varIdx] =
              pSolver->getVarOrigLowerBound(varIdx);
    }
  }
  else
  {
    for (int varIdx = 0; varIdx < pNumOriginalVars; ++varIdx)
    {
      pInitialVarLBList[varIdx] = pSolver->getVarOrigLowerBound(varIdx);
      pInitialVarUBList[varIdx] = pSolver->getVarOrigLowerBound(varIdx);
    }
  }
}  // saveOriginalProblemBounds

void MetaNetSolver::tryNewSolution(metabb::Solution::SPtr sol)
{
  auto solution = std::dynamic_pointer_cast<MetaNetSolution>(sol);
  if (!solution)
  {
    throw std::runtime_error("MetaSolver - tryNewSolution: "
            "invalid downcast to MetaSolution");
  }

  // The solution is set as warm-start for the solver
  const auto& varList = getSolver()->getVariableList();
  std::vector<std::pair<std::shared_ptr<MPVariable>, double>> warmStart;
  for (const auto& solPair : solution->getSolution())
  {
    warmStart.push_back(std::make_pair(varList.at(solPair.first), solPair.second));
  }

  getSolver()->setWarmStart(warmStart);
}  // tryNewSolution

void MetaNetSolver::setLightWeightRootNodeProcess()
{
  // No-op
  // TODO implement this method solver-specific
}  //setLightWeightRootNodeProcess

void MetaNetSolver::setOriginalRootNodeProcess()
{
  // No-op
  // TODO implement this method solver-specific
}  //setOriginalRootNodeProcess

void MetaNetSolver::createSubproblem()
{
  assert(pNode);
  if (pNode->isRootNode())
  {
    /// TODO set parameters in solver from root node
  }
  else
  {
    /// TODO set default parameters after racing if needeed
  }

  // Change the bounds of the variables according to the diff. subproblem of the
  // current solving node
  const auto dualBoundValue = pNode->getDualBoundValue();
  auto nodeDiffSubproblem = std::dynamic_pointer_cast<MetaNetDiffSubproblem>(
          pNode->getDiffSubproblem());
  if (nodeDiffSubproblem)
  {
    const auto& varList = getSolver()->getVariableList();
    for (int vidx = 0; vidx < nodeDiffSubproblem->getNBoundChanges(); ++vidx)
    {
      if (nodeDiffSubproblem->getBranchBoundType(vidx) ==
              MetaNetDiffSubproblem::BoundType::BT_LOWER)
      {
        (varList.at(nodeDiffSubproblem->getIndex(vidx)))->setLowerBound(
                nodeDiffSubproblem->getBranchBound(vidx));
      }
      else
      {
        (varList.at(nodeDiffSubproblem->getIndex(vidx)))->setUpperBound(
                nodeDiffSubproblem->getBranchBound(vidx));
      }
    }

    // Add new branching linear constraints if needed
    if (nodeDiffSubproblem->hasBranchLinearConstraints())
    {
      const auto& varList = getSolver()->getVariableList();
      for (int idx = 0; idx < nodeDiffSubproblem->getNBranchLinearConstraints(); ++idx)
      {
        // Build row constraint corresponding to the new constraints in the
        // given node diff. subproblem instance
        const auto& diffCon = nodeDiffSubproblem->getBranchLinearConstraint(idx);
        auto con = getSolver()->buildRowConstraint(
                diffCon.lowerBound, diffCon.upperBound, diffCon.name);
        assert(con);

        // Add the variables in the constraint's scope
        for (const auto& coeff : diffCon.coeffList)
        {
          con->setCoefficient(varList.at(coeff.first), coeff.second);
        }

        pAddedBranchConList.push_back(con);
      }
    }
  }

  // @note here it would be possible to add a linear constraint to bound the
  // current received dual bound.
  // However this is not as efficient as one images since it creates many
  // degenerate solutions
  if (!(pParamSet->getDescriptor().setalldefaultsafterracing()) &&
          (pWinnerRacingRampUpParamSet != nullptr))
  {
    setWinnerRacingParams(pWinnerRacingRampUpParamSet);
  }
}  // createSubproblem

void MetaNetSolver::freeSubproblem()
{
  if (isRacingStage() && pParamSet->getDescriptor().racingstatbranching() && !pRestartingRacing)
  {
    // Reset initial statistics
    auto initialStat = std::dynamic_pointer_cast<MetaNetInitialStat>(
pFramework->buildInitialStat());
    if (!initialStat)
    {
      throw std::runtime_error("MetaSolver - freeSubproblem: "
              "invalid downcast to MetaInitialStat");
    }
    initialStat->send(pFramework, 0);
  }

  // Remove all added branch linear constraints
  if (not pAddedBranchConList.empty())
  {
    // Currently it is not possible to directly remove a constraint.
    // However, it is possible to reset the constraint such that it does not
    // constraint any variable
    for (auto con : pAddedBranchConList)
    {
      con->setBounds(-MPSolver::infinity(), MPSolver::infinity());
      con->clear();
    }
  }
  pAddedBranchConList.clear();

  // Reset the boundaries of the variables changed by this DiffSubproblem
  auto nodeDiffSubproblem = std::dynamic_pointer_cast<MetaNetDiffSubproblem>(
          pNode->getDiffSubproblem());
  if (nodeDiffSubproblem)
  {
    const auto& varList = getSolver()->getVariableList();
    for (int vidx = 0; vidx < nodeDiffSubproblem->getNBoundChanges(); ++vidx)
    {
      if (nodeDiffSubproblem->getBranchBoundType(vidx) ==
              MetaNetDiffSubproblem::BoundType::BT_LOWER)
      {
        (varList.at(nodeDiffSubproblem->getIndex(vidx)))->resetOrigLowerBound();
      }
      else
      {
        (varList.at(nodeDiffSubproblem->getIndex(vidx)))->resetOrigUpperBound();
      }
    }
  }

  if (pRacingWinner)
  {
    pNumVarTightened = getNumVarTightened();
    pNumIntVarTightened = getNumIntVarTightened();
  }

  if ((pParamSet->getDescriptor().rampupphaseprocess() == kRampUpPhaseRacing ||
          pParamSet->getDescriptor().rampupphaseprocess() == kRampUpPhaseRebuildTreeAfterRacing) &&
          pParamSet->getDescriptor().communicatetighterboundsinracing())
  {
    recoverOriginalSettings();
  }
}  //freeSubproblem

void MetaNetSolver::recoverOriginalSettings()
{
  // TODO implement this method
}  // recoverOriginalSettings

void MetaNetSolver::solve()
{
  // All options, e.g., timeout should be set once the MPSolver is declared.
  // Reset timeout here
  if (pParamSet->getDescriptor().timelimitmsec() > 0.0)
  {
    getSolver()->setTimeoutMsec(static_cast<uint64_t>(pParamSet->getDescriptor().timelimitmsec()));
  }

  // Solve the problem
  pSolvingResult = getSolver()->solve(*pParamSet);
  if (pSolvingResult == lmcommon::ResultStatus::ABNORMAL ||
          pSolvingResult == lmcommon::ResultStatus::MODEL_INVALID)
  {
    throw std::runtime_error("MetaSolver - solve: solver terminated with invalid result");
  }

  // Check if notification message has to complete.
  // If so, wait for notification message
  if (pNotificationProcessed)
  {
    waitNotificationIdMessage();
  }

  // Proceed sending local solution if optimal
  if (pSolvingResult == lmcommon::ResultStatus::OPTIMAL)
  {
    auto sol = std::dynamic_pointer_cast<MetaNetSolution>(pFramework->buildSolution());
    if (!sol)
    {
      throw std::runtime_error("MetaSolver - solve: "
              "invalid downcast to MetaSolution");
    }

    sol->uploadSolution(getSolver());
    saveIfImprovedSolutionWasFound(sol);

    // Send the local solution back to the LM
    sendLocalSolution();
  }

  pSolverDualBound = -metabb::metaconst::MAX_DOUBLE;
  if (pSolvingResult == lmcommon::ResultStatus::OPTIMAL ||
          pSolvingResult == lmcommon::ResultStatus::UNBOUNDED)
  {
    pSolverDualBound = getSolver()->getDualBound();
    if (pSolvingResult == lmcommon::ResultStatus::OPTIMAL)
    {
      pSolverDualBound = std::max(pSolverDualBound, getGlobalBestIncumbentValue());
    }
  }
  else if (pSolvingResult == lmcommon::ResultStatus::INFEASIBLE)
  {
    if (EPSEQ(getGlobalBestIncumbentValue(), metabb::metaconst::MAX_DOUBLE, pEps))
    {
      pSolverDualBound = getSolver()->getDualBound();
    }
    else
    {
      pSolverDualBound = getGlobalBestIncumbentValue();
    }
  }
  else
  {
    if (pSolvingResult == lmcommon::ResultStatus::FEASIBLE)
    {
      // Check if the solver was interrupted
      const auto interrupt = isInterrupting();
      if (!interrupt)
      {
        // Timeout
        pSolverDualBound = getSolver()->getDualBound();
        setTerminationMode(metabb::solver::TerminationMode::TM_TIME_LIMIT_TERMINATION);
        if (isRacingStage())
        {
          pRacingIsInterrupted = true;
        }
      }
      else
      {
        if (ceil(getCurrentNode()->getDualBoundValue()) >= getGlobalBestIncumbentValue())
        {
          pSolverDualBound = ceil(getCurrentNode()->getDualBoundValue());
        }
        else
        {
          pSolverDualBound = std::max(
                  getCurrentNode()->getDualBoundValue(), getSolver()->getDualBound());
        }
      }
    }
  }
}  // solve

long long MetaNetSolver::getNumNodesSolved()
{
  return static_cast<long long>(getSolver()->numNodes());
}  // getNumNodesSolved

int MetaNetSolver::getNumNodesLeft()
{
  return static_cast<long long>(getSolver()->numNodesLeft());
}  // getNumNodesLeft

double MetaNetSolver::getDualBoundValue()
{
  return (pSolvingResult == lmcommon::ResultStatus::NOT_SOLVED) ?
          pNode->getDualBoundValue() :
          getSolver()->getDualBound();
}  // getDualBoundValue

void MetaNetSolver::reinitializeInstance()
{
  // No-op
  // TODO implement this method
}  // reinitializeInstance

void MetaNetSolver::setOriginalNodeSelectionStrategy()
{
  // No-op
  // TODO implement this method
}  // setOriginalNodeSelectionStrategy

int MetaNetSolver::lowerBoundTightened(int source, optnet::Packet::UPtr packet)
{
  auto dpacket = optnet::castBaseToNetworkPacket<double>(std::move(packet->networkPacket));
  if (dpacket->size() != 2)
  {
    const std::string errMsg = "MetaSolver - lowerBoundTightened: "
            "invalid data size, expected 2, received " + std::to_string(dpacket->size());
    spdlog::error(errMsg);
    return metabb::metaconst::ERR_GENERIC_ERROR;
  }

  // Target bound as first message
  // const auto varIdx = static_cast<int>(dpacket->data[0]);
  // const auto varBound = dpacket->data[1];

  // TODO implement this method

  return metabb::metaconst::ERR_NO_ERROR;
}  // lowerBoundTightened

int MetaNetSolver::upperBoundTightened(int source, optnet::Packet::UPtr packet)
{
  auto dpacket = optnet::castBaseToNetworkPacket<double>(std::move(packet->networkPacket));
  if (dpacket->size() != 2)
  {
    const std::string errMsg = "MetaSolver - upperBoundTightened: "
            "invalid data size, expected 2, received " + std::to_string(dpacket->size());
    spdlog::error(errMsg);
    return metabb::metaconst::ERR_GENERIC_ERROR;
  }

  // Target bound as first message
  // const auto varIdx = static_cast<int>(dpacket->data[0]);
  // const auto varBound = dpacket->data[1];

  // TODO implement this method

  return metabb::metaconst::ERR_NO_ERROR;
}  // upperBoundTightened

void MetaNetSolver::setWinnerRacingParams(metabb::RacingRampUpParamSet::SPtr racingParams)
{
  if (!pParamSet->getDescriptor().setalldefaultsafterracing())
  {
    setRacingParams(racingParams, true);
  }
}  // setWinnerRacingParams

void MetaNetSolver::setRacingParams(metabb::RacingRampUpParamSet::SPtr racingParams, bool winnerParam)
{
  auto rparams = std::dynamic_pointer_cast<MetaNetRacingRampUpParamSet>(racingParams);
  if (!rparams)
  {
    std::string errMsg = "MetaNetSolver - setWinnerRacingParams: "
            "invalid downvast to MetaNetRacingRampUpParamSet";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  // TODO
}  // setRacingParams

}  // namespace toolbox
}  // namespace optilab
