//
// Copyright OptiLab 2020. All rights reserved.
//
// Base class interface to a linear and mixed integer
// programming solver (e.g., CBC, SCIP, etc.).
// Linear Programming (LP):
// - technique for optimization of a linear objective
//   function , subject to linear equality and linear
//   inequality constraints.
// Example of Linear Programming:
// maximize:
//   3x + y
// subject to:
//   1.5x + 2y <= 12
//   0 <= x <= 3
//   0 <= y <= 5
// Components of a linear programming solver:
// a) linear objective function;
// b) linear constraints that can be equalities of inequalities;
// c) bounds on variables that can be positive, negative, finite
//    or infinite.
// Mixed Integer Programming (MIP):
// - similar to LP but with additional integrality requirements
//   on variables.
// If all variable are required to be integers, then the problem
// becomes an Integer Programming (IP) problem.
// If some variables are required to be integers and some are
// continuous, then the problem is a MIP problem.
// Both IP and MIP are generally NP-hard problems.
//

#pragma once

#include <cstdint>    // for uint64_t
#include <limits>     // for std::numeric_limits
#include <memory>     // for std::shared_ptr
#include <stdexcept>  // for std::invalid_argument
#include <string>
#include <utility>    // for std::pair
#include <vector>

#include <ortools/linear_solver/linear_solver.pb.h>
#include <sparsepp/spp.h>
#include <spdlog/spdlog.h>

#include "meta_bb/paramset.hpp"
#include "or_toolbox/linear_model.hpp"
#include "or_toolbox/linear_model_common.hpp"
#include "system/system_export_defs.hpp"

// Forward declarations
namespace optilab {
namespace  toolbox {
class MPSolverInterface;
}  // namespace toolbox
}  // namespace optilab

namespace optilab {
namespace  toolbox {

/**
 * Mathematical Programming (MP) solver class.
 */
class SYS_EXPORT_CLASS MPSolver {
 public:
  using SPtr = std::shared_ptr<MPSolver>;

 public:
  /// Constructor: creates a solver with given name and using
  /// the given back-end solver.
  /// @note throws std::runtime_error if the package type is undefined
  MPSolver(const std::string& solverName,
           lmcommon::OptimizationSolverPackageType solverPackageType);

  virtual ~MPSolver();

  /// Deleted copy constructor
  MPSolver(const MPSolver&) = delete;

  /// Deleted assignment operator
  void operator=(const MPSolver&) = delete;

  /// Takes a starting basis to be used in the next LP "solve(...)" calls.
  /// The statuses of a current solution can be retrieved via the
  /// "basisStatus(...)" of an MPVariable or an MPConstraint
  void setStartingLpBasis(const std::vector<lmcommon::BasisStatus>& variable_statuses,
                          const std::vector<lmcommon::BasisStatus>& constraint_statuses);

  /// Infinity value used, for example, on unbounded constraints
  static double infinity() { return std::numeric_limits<double>::infinity(); }

  /// Returns the name of the solver
  inline const std::string& getName() const noexcept { return pSolverName; }

  /// Clears the objective, variables, and constraints.
  /// All other properties of the solver (e.g., time limit) are not touched
  void clear();

  /// Loads an mps model from the given file.
  /// Returns the following:
  /// 0: success;
  /// 1: invalid model;
  /// 2: infeasible model.
  int loadMPSModelFromFile(const std::string& filePath);

  /// Returns the package type of the back-end optimization solver
  inline lmcommon::OptimizationSolverPackageType getOptimizationPackageType() const noexcept
  {
    return pSolverPackage;
  }

  /// Returns the objective function
  const MPObjective& getObjective() const { return *pObjective; }
  MPObjective* const getObjectivePtr() const { return pObjective.get(); }

  /// Returns the number of variables in the model
  inline int getNumVariables() const noexcept { return static_cast<int>(pVariableList.size()); }

  /// Returns the list of variables in this solver
  inline const std::vector<std::shared_ptr<MPVariable>>& getVariableList() const noexcept
  {
    return pVariableList;
  }

  /// Returns the lower bound of the variable with given index
  inline double getVarLowerBound(int varIdx) const
  {
    return pVariableList.at(varIdx)->lowerBound();
  }

  /// Returns the upper bound of the variable with given index
  inline double getVarUpperBound(int varIdx) const
  {
    return pVariableList.at(varIdx)->upperBound();
  }

  /// Returns the original lower bound of the variable with given index
  inline double getVarOrigLowerBound(int varIdx) const
  {
    return pVariableList.at(varIdx)->origLowerBound();
  }

  /// Returns the original upper bound of the variable with given index
  inline double getVarOrigUpperBound(int varIdx) const
  {
    return pVariableList.at(varIdx)->origUpperBound();
  }

  /// Some interfaces map values into their internal space representation.
  /// This method converts a value from internal to external space
  double convertToExternalValue(double internalValue);

  /// Some interfaces map values into their internal space representation.
  /// This method converts a value from external to internal space
  double convertToInternalValue(double externalValue);

  /// Looks up and returns a variable by name from the list of variables in the model.
  /// Returns nullptr if no such variable is present
  std::shared_ptr<MPVariable> getVariableByName(const std::string& var) const noexcept;

  /// Builds and returns a continuous variable with specified bounds and name
  std::shared_ptr<MPVariable> buildNumVar(double lb, double ub, const std::string& name);

  /// Builds and returns an integer variable with specified bounds and name
  std::shared_ptr<MPVariable> buildIntVar(double lb, double ub, const std::string& name);

  /// Builds and returns a boolean variable with specified name
  std::shared_ptr<MPVariable> buildBoolVar(const std::string& name);

  /// Builds and returns a list of "num" continuous variables with name:
  /// "name_0, name_1, ..., name_(num-1)".
  /// @note throws std::runtime_error if num is less than or equal zero
  std::vector<std::shared_ptr<MPVariable>> buildNumVarArray(int num, double lb, double ub,
                                                            const std::string& name);

  /// Builds and returns a list of "num" integer variables with name:
  /// "name_0, name_1, ..., name_(num-1)"
  /// @note throws std::runtime_error if num is less than or equal zero
  std::vector<std::shared_ptr<MPVariable>> buildIntVarArray(int num, double lb, double ub,
                                                            const std::string& name);

  /// Builds and returns a list of "num" boolean variables with name:
  /// "name_0, name_1, ..., name_(num-1)"
  /// @note throws std::runtime_error if num is less than or equal zero
  std::vector<std::shared_ptr<MPVariable>> buildBoolVarArray(int num, const std::string& name);

  inline int getNumConstraints() const noexcept { return static_cast<int>(pConstraintList.size()); }

  /// Looks up and returns a constraint by name from the list of constraints in the model.
  /// Returns nullptr if no such constraint is present
  std::shared_ptr<MPConstraint> getConstraintByName(const std::string& con) const noexcept;

  /// Builds and returns a linear constraint with -inf and +inf bounds.
  /// The caller can provide a name for the constraint
  std::shared_ptr<MPConstraint> buildRowConstraint(const std::string& name="");

  /// Builds and returns a linear constraint with specified bounds.
  /// The caller can provide a name for the constraint
  std::shared_ptr<MPConstraint> buildRowConstraint(double lb, double ub,
                                                   const std::string& name="");

  /// Returns the last solve status
  inline lmcommon::ResultStatus getSolveStatus() const noexcept { return pSolveStatus; }

  /// Runs the solver using default parameter values
  lmcommon::ResultStatus solve();

  /// Runs the solver using the specified parameter values
  lmcommon::ResultStatus solve(const metabb::ParamSet& params);

  /// Resets the model to solve from scratch.
  /// This doesn't reset parameters like timeout.
  /// It only makes sure that next call to "solve(...)" will be as if everything
  /// was reconstructed from scratch.
  /// @note this does not clear the model but resets the internal data-structures
  /// to run the already existing model from scratch
  void reset();

  /// Interrupts the solving process if possible.
  /// Returns true if the back-end solver (can be and) is interrupted.
  /// Returns false otherwise
  bool interruptSolve();

  /// Sets a warm start for the solver
  void setWarmStart(std::vector<std::pair<std::shared_ptr<MPVariable>, double>> warmStart);

  /// Returns the solver timeout limit in msec.
  inline uint64_t getTimeoutMsec() const noexcept { return pTimeoutMsec; }

  /// Returns the solver timeout limit in msec.
  inline void setTimeoutMsec(uint64_t timeoutMsec) noexcept { pTimeoutMsec = timeoutMsec; }

  /// Resets the values of out of bound variables to the corresponding bounds
  /// according to the respective domains and returns false if any of the variables
  /// have NaN value. Returns true otherwise
  bool clampSolutionWithinBounds();

  /// Returns the number of simplex iterations
  uint64_t numSimplexIterations() const;

  /// Returns the number of branch-and-bound nodes evaluated during
  /// the solving process
  /// @note this is only available for discrete problems
  uint64_t numBBNodes() const;

  /// Returns the total number of nodes visited during
  /// the solving process
  int64_t numNodes() const;

  /// Returns the number of nodes left in the tree:
  /// children + siblings + leaves
  int64_t numNodesLeft() const;

  /// Returns the solution's dual bound
  double getDualBound() const;

  /// Sets the number of threads to use by the underlying solver.
  /// Returns true on success, false otherwise.
  /// @note the number of threads must be equal or greater than 1
  bool setNumThreads(int numThreads);

  /// Returns the time spent in building the model in msec.
  uint64_t getBuildModelTimeMsec() const;

  /// Returns the time spent in solving the model in msec.
  uint64_t getSolveModelTimeMsec() const;

  /// Returns the number of threads to be used during solve process
  /// by the underlying solver
  inline int getNumThreads() const noexcept { return pNumThreads; }

 protected:
  friend class MPSolverInterface;

  /// Make the SCIP back-end solver interface a friend class
  /// of this class
  friend class SCIPInterface;

  /// Returns internal interface
  inline MPSolverInterface* getInterface() const noexcept { return pInterface.get(); }

 private:
  /// Solver's name
  const std::string pSolverName;

  /// Solver package used as back-end solver
  lmcommon::OptimizationSolverPackageType pSolverPackage;

  /// Status of the solver after calling solve
  lmcommon::ResultStatus pSolveStatus;

  /// Solver timeout limit in msec
  uint64_t pTimeoutMsec;

  /// Number of threads to be used by the back-end solver
  int pNumThreads;

  /// Pointer to the linear objective function
  std::unique_ptr<MPObjective> pObjective;

  /// List of variables
  std::vector<std::shared_ptr<MPVariable>> pVariableList;

  /// Map of variable names to variable indices into the list of variables
  mutable spp::sparse_hash_map<std::string, int> pVarNameToIdxMap;

  /// List of variables that have been extracted (true) or not (false)
  /// to the underlying solver interface
  std::vector<bool> pExtractedVariableList;

  /// List of constraints
  std::vector<std::shared_ptr<MPConstraint>> pConstraintList;

  /// Map of constraints names to constraint indices into the list of constraints
  mutable spp::sparse_hash_map<std::string, int> pConNameToIdxMap;

  /// List of constraint that have been extracted (true) or not (false)
  /// to the underlying solver interface
  std::vector<bool> pExtractedConstraintList;

  /// The back-end solver's interface
  std::unique_ptr<MPSolverInterface> pInterface;

  /// Warm start (if any) for the MIP solver
  std::vector<std::pair<std::shared_ptr<MPVariable>, double>> pWarmStart;

  /// Computes the size of the constraint with the largest number of
  /// coefficients with index in [min_constraint_index, max_constraint_index)
  int computeMaxConstraintSize(int minConstraintIndex, int maxConstraintIndex) const;

  /// Returns true if the model has constraints with lower bound > upper bound.
  bool hasInfeasibleConstraints() const;

  /// Returns true if the model has at least 1 integer variable.
  bool hasIntegerVariables() const;

  /// Generates the map from variable names to their indices
  void generateVariableNameIndex() const;

  /// Generates the map from constraint names to their indices
  void generateConstraintNameIndex() const;

  /// Loads a model from the given protobuf instance.
  /// Returns the following:
  /// 0: success;
  /// 1: invalid model;
  /// 2: infeasible model.
  int loadModelFromProto(const operations_research::MPModelProto& inputModel, bool clearNames,
                         bool checkModelValidity);

  /// Builds and returns a continuous (isInt is false) or an integer (isInt is true) variable
  /// with specified bounds and name
  std::shared_ptr<MPVariable> buildVar(double lb, double ub, bool isInt, const std::string& name);

  /// Similar to "buildVar(...)", builds and returns an array of variables
  std::vector<std::shared_ptr<MPVariable>> buildVarArray(int num, double lb, double ub, bool isInt,
                                                         const std::string& name);
};

/**
 * Class encapsulating the actual back-end solver system package.
 * Each back-end solver has its own implementation of this interface.
 */
class SYS_EXPORT_CLASS MPSolverInterface {
 public:
  enum SynchronizationStatus {
    // The underlying solver and MPSolver are not in
    // sync for the model nor for the solution
    MUST_RELOAD,
    // The underlying solver and MPSolver are in sync for the model
    // but not for the solution: the model has changed since the
    // solution was computed last
    MODEL_SYNCHRONIZED,
    // The underlying solver and MPSolver are in sync for the model and
    // the solution.
    SOLUTION_SYNCHRONIZED
  };

  using SPtr = std::shared_ptr<MPSolverInterface>;

 public:
  explicit MPSolverInterface(MPSolver* const solver);

  virtual ~MPSolverInterface() = default;

  /// Solves problem with specified parameter values
  virtual lmcommon::ResultStatus solve(const metabb::ParamSet& param) = 0;

  /// Resets extracted models (from MPSolver to back-end solver)
  virtual void reset() = 0;

  /// Some interfaces map values into their internal space representation.
  /// This method converts a value from internal to external space.
  /// @default return the same value
  virtual double convertToExternalValue(double internalValue) = 0;

  /// Some interfaces map values into their internal space representation.
  /// This method converts a value from external to internal space.
  /// @default return the same value
  virtual double convertToInternalValue(double externalValue) = 0;

  /// Sets the optimization direction (min/max):
  /// - maximize: true; or
  /// - minimize: false
  virtual void setOptimizationDirection(bool maximize) = 0;

  /// Modifies bounds of an extracted variable.
  /// @note "index" is the index of the variable in the list of variables
  /// in the order created by the caller
  virtual void setVariableBounds(int index, double lb, double ub) = 0;

  /// Modifies integrality of an extracted variable
  /// @note "index" is the index of the variable in the list of variables
  /// in the order created by the caller
  virtual void setVariableInteger(int index, bool integer) = 0;

  /// Modify bounds of an extracted variable
  /// @note "index" is the index of the variable in the list of variables
  /// in the order created by the caller
  virtual void setConstraintBounds(int index, double lb, double ub) = 0;

  /// Adds a linear constraint
  virtual void addRowConstraint(MPConstraint* const ct) = 0;

  /// Adds an indicator constraint.
  /// Returns true if the feature is supported by the underlying solver.
  virtual bool addIndicatorConstraint(MPConstraint* const ct)
  {
    spdlog::error("Solver doesn't support indicator constraints");
    return false;
  }

  /// Adds a variable
  virtual void addVariable(MPVariable* const var) = 0;

  /// Changes a coefficient in a constraint
  virtual void setCoefficient(MPConstraint* const constraint, const MPVariable* const variable,
                              double newValue, double oldValue) = 0;

  /// Clears a constraint from all its terms
  virtual void clearConstraint(MPConstraint* const constraint) = 0;

  /// Changes a coefficient in the linear objective.
  virtual void setObjectiveCoefficient(const MPVariable* const variable, double coefficient) = 0;

  /// Changes the constant term in the linear objective
  virtual void setObjectiveOffset(double value) = 0;

  /// Clears the objective from all its terms.
  virtual void clearObjective() = 0;

  /// Signals the change in the priority of a variable.
  /// @note "varIndex" is the index of the variable in the list of variables
  /// in the order created by the caller
  virtual void branchingPriorityChangedForVariable(int varIndex) {}

  /// Returns the number of simplex iterations.
  /// @note The problem must be discrete
  virtual int64_t numSimplexIterations() const = 0;

  /// Returns the number of branch-and-bound nodes.
  /// @note The problem must be discrete
  virtual int64_t numNodes() const = 0;

  /// Returns the number of nodes left in the tree:
  /// children + siblings + leaves
  virtual int64_t numNodesLeft() const = 0;

  /// Returns the best objective bound.
  /// @note The problem must be discrete
  virtual double bestObjectiveBound() const = 0;

  /// A trivial objective bound: the worst possible value of the objective.
  /// The worst possible value is:
  /// +infinity, if minimizing; and
  /// -infinity if maximizing
  double trivialWorstObjectiveBound() const;

  /// Returns the objective value of the best solution found so far
  double objectiveValue() const;

  /// Checks whether the solution is synchronized with the model,
  /// i.e. whether the model has changed since the solution was computed last
  bool checkSolutionIsSynchronized() const;

  /// Checks whether a feasible solution exists
  virtual bool checkSolutionExists() const;

  /// Shortcut
  inline bool checkSolutionIsSynchronizedAndExists() const
  {
    return checkSolutionIsSynchronized() && checkSolutionExists();
  }

  /// Checks whether information on the best objective bound exists
  virtual bool checkBestObjectiveBoundExists() const;

  /// Returns true if the problem is continuous,
  /// returns false otherwise
  virtual bool isContinuous() const = 0;

  /// Returns true if the problem is continuous and linear,
  /// returns false otherwise
  virtual bool isLP() const = 0;

  /// Returns true if the problem is discrete and linear,
  /// returns false otherwise
  virtual bool isMIP() const = 0;

  /// Returns the basis status of a row
  virtual lmcommon::BasisStatus rowStatus(int constraintIndex) const = 0;

  /// Returns the basis status of a column
  virtual lmcommon::BasisStatus columnStatus(int variableIndex) const = 0;

  /// Returns the index of the last variable extracted
  inline int lastVariableIndex() const noexcept { return pLastVariableIndex; }

  inline bool variableIsExtracted(int varIndex) const
  {
    return pSolver->pExtractedVariableList.at(varIndex);
  }

  inline void setVariableAsExtracted(int varIndex, bool extracted)
  {
    if (varIndex < 0 || varIndex >= pSolver->pExtractedVariableList.size())
    {
      throw std::out_of_range("MPSolverInterface - setVariableAsExtracted: "
              "out of range value " + std::to_string(varIndex));
    }
    pSolver->pExtractedVariableList[varIndex] = extracted;
  }

  inline bool constraintIsExtracted(int conIndex) const
  {
    return pSolver->pExtractedConstraintList.at(conIndex);
  }

  inline void setConstraintAsExtracted(int conIndex, bool extracted)
  {
    if (conIndex < 0 || conIndex >= pSolver->pExtractedConstraintList.size())
    {
      throw std::out_of_range("MPSolverInterface - setConstraintAsExtracted: "
              "out of range value " + std::to_string(conIndex));
    }
    pSolver->pExtractedConstraintList[conIndex] = extracted;
  }

  /// Returns the result status of the last solve
  lmcommon::ResultStatus resultStatus() const
  {
    checkSolutionIsSynchronized();
    return pResultStatus;
  }

  /// Returns the pointer to the back-end underlying solver
  virtual void* backendSolverPtr() = 0;

  virtual void setStartingLPBasis(const std::vector<lmcommon::BasisStatus>& variableStatuses,
                                  const std::vector<lmcommon::BasisStatus>& constraintStatuses)
  {
    spdlog::error("Solver doesn't support setting an LP basis");
  }

  /// Interrupt the solving process of the back-end solver
  virtual bool interruptSolve() { return false; }

  /// Computes the next MIP solution, if any
  virtual bool nextSolution() { return false; }

 protected:
  friend class MPSolver;
  friend class MPConstraint;
  friend class MPObjective;

 protected:
  /// Pointer to the MP solver
  MPSolver* const pSolver;

  /// Indicates whether or not the model and the solution are synchronized
  SynchronizationStatus pSynchStatus;

  /// Indicates whether or not the solve has reached optimality,
  /// infeasibility, a limit, etc.
  lmcommon::ResultStatus pResultStatus;

  /// Optimization direction
  bool pMaximize;

  /// Index of the last constraint extracted
  int pLastConstraintIndex;

  /// Index of the last variable extracted
  int pLastVariableIndex;

  /// The value of the objective function
  double pObjectiveValue;

  /// Extracts the model stored in MPSolver
  void extractModel();

  /// Returns the time in msec. taken by the solver to load the model
  virtual uint64_t getModelLoadTimeMsec() const = 0;

  /// Returns the solving time in msec
  virtual uint64_t getSolveTimeMsec() const = 0;

  /// Extracts the variables that have not been extracted yet
  virtual void extractNewVariables() = 0;

  /// Extracts the constraints that have not been extracted yet
  virtual void extractNewConstraints() = 0;

  /// Extracts the objective
  virtual void extractObjective() = 0;

  /// Resets the extraction information
  void resetExtractionInformation();

  /// Change synchronization status from SOLUTION_SYNCHRONIZED to  MODEL_SYNCHRONIZED.
  /// To be used for model changes
  void invalidateSolutionSynchronization();

  /// Sets parameters common to LP and MIP in the underlying solver
  void setCommonParameters(const metabb::ParamSet& param);

  /// Sets MIP specific parameters in the underlying solver
  void setMIPParameters(const metabb::ParamSet& param);

  /// Sets all parameters in the underlying solver
  virtual void setParameters(const metabb::ParamSet& param) = 0;

  /// Sets each parameter in the underlying solver.
  virtual void setRelativeMipGap(double value) = 0;
  virtual void setPrimalTolerance(double value) = 0;
  virtual void setDualTolerance(double value) = 0;
  virtual void setPresolveMode(int value) = 0;

  /// Sets the number of threads to be used by the solver.
  /// Returns true if the back-end solver allows the caller to set
  /// the number of threads.
  /// Returns false otherwise
  virtual bool setNumThreads(int numThreads)
  {
    (void)numThreads;
    return false;
  }

  /// Sets the scaling mode
  virtual void setScalingMode(int value) = 0;

  /// Sets the LP algorithm to be used by the back-end solver
  virtual void setLPAlgorithm(int value) = 0;
};

}  // namespace toolbox
}  // namespace optilab
