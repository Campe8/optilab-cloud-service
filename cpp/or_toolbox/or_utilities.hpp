//
// Copyright OptiLab 2020. All rights reserved.
//
// General utilities for OR-toolbox.
//

#pragma once

#include <memory>   // for std::shared_ptr
#include <string>
#include <unordered_map>
#include <utility>  // for std::pair
#include <vector>

#include "optilab_protobuf/linear_model.pb.h"
#include "data_structure/metrics/metrics_register.hpp"
#include "system/system_export_defs.hpp"
#include "or_toolbox/linear_solver.hpp"
#include "or_toolbox/or_instance.hpp"

namespace optilab {
namespace toolbox {

/**
 * Namespace for OR engine-related utilities
 */
namespace orengine {

/**
 * General OR toolbox work executed by OR processors.
 */
struct SYS_EXPORT_STRUCT ORWork {
using SPtr = std::shared_ptr<ORWork>;

enum WorkType {
  /// Runs the optimizer on the loaded model
  kRunOptimizer,
  kWorkTypeUndef,
};

ORWork(WorkType wtype, MetricsRegister::SPtr metReg)
: workType(wtype), metricsRegister(metReg)
{
}

// Type of work to be executed
WorkType workType;
MetricsRegister::SPtr metricsRegister;
};

/**
 * General OR toolbox work executed by OR processors.
 */
struct SYS_EXPORT_STRUCT ORNetWork {
using SPtr = std::shared_ptr<ORNetWork>;

enum class WorkType {
  /// Loads an MP model
  kNetORLoadModel,

  /// Initializes the distributed system
  kNetORInitSystem,

  /// Runs the framework
  kNetORRunSystem,

  /// Collect solution
  kNetORCollectSolutions
};

ORNetWork(WorkType wtype, bool isRoot)
: workType(wtype),
  isRootWork(isRoot)
{
}

/// Root flag indicating if this is work executed
/// by the root node or not
bool isRootWork;

/// Type of work to be executed
WorkType workType;

/// Linear model.
/// Valid only for "kNetORLoadModel" work type
LinearModelSpecProto lmInstance;
};


/**
 * Event class representing any event received by the engine,
 * triggering compute actions.
 */
class SYS_EXPORT_CLASS OREvent {
 public:
  enum EventType {
    /// Starts the solving process
    kRunEngine,
    /// Load a model instance
    kLoadInstance,
    /// Kills any ongoing computation
    kInterruptEngine,
    /// Collects solutions
    kSolutions
  };

 public:
  explicit OREvent(EventType aType)
  : pEventType(aType)
  {
  }

  inline EventType getType() const noexcept
  {
    return pEventType;
  }

  inline void setNumSolutions(int numSolutions) noexcept
  {
    pNumSolutions = numSolutions;
  }

  inline int getNumSolutions() const noexcept
  {
    return pNumSolutions;
  }

  void setInstance(ORInstance::UPtr instance)
  {
    pInstance = std::move(instance);
  }

  ORInstance::UPtr getInstance() const { return std::move(pInstance); }

 private:
  /// Type of this event
  EventType pEventType;

  /// Pointer to the instance to load
  mutable ORInstance::UPtr pInstance;

  /// Number of solutions for "kSolutions" events
  int pNumSolutions{0};
};

/**
 * Base class for any result computed by
 * an OR engine.
 * For example, solution of a MIP problem.
 */
struct SYS_EXPORT_STRUCT ORResult {
  using SPtr = std::shared_ptr<ORResult>;

  virtual ~ORResult() = default;

  /// Name of the model producing this result
  std::string modelName;
};

/**
 * Class representing the result of a MIP problem.
 */
struct SYS_EXPORT_STRUCT MIPResult : public ORResult {
  LinearModelSolutionProto solutionProto;

  /// Result status
  enum ResultStatus {
    /// Solution is the global optimum
    OPTIMUM = 0,

    /// Feasible solution but not optimal
    FEASIBLE,

    /// Infeasible model, no result
    INFEASIBLE,

    /// Model proven to be unbounded
    UNBOUNDED,

    /// Error of some kind
    ABNORMAL,

    /// Invalid model
    MODEL_INVALID,

    /// Model not yet solved
    NOT_SOLVED
  };

  using SPtr = std::shared_ptr<MIPResult>;

  MIPResult()
  {
    baseSolution.first = 0.0;
  }

  /// Uploads the solution into the proto message "solutionProto"
  void uploadToProtoMessage();

  /// Collection of solutions.
  /// @note an assignment is a list of pairs of values to a variable which may be scalar or
  /// a multi-dimensional variable
  using VarAssign = std::vector<std::pair<double, double>>;
  using Solution = std::unordered_map<std::string, VarAssign>;

  /// List of variable values.
  /// The values are ordered as the order of the variables in the model
  using SolutionList = std::vector<double>;

  /// Vector of objective values, in the order declared in the model
  using ObjectiveValues = std::vector<double>;

  /// A MIP solution is a pair of objective values and variable assignments
  std::pair<ObjectiveValues, Solution> solution;

  /// A base MIP solution, i.e., one objective and a list of variable values
  std::pair<double, SolutionList> baseSolution;

  /// Status of the solution
  ResultStatus resultStatus{ResultStatus::NOT_SOLVED};

  /// Clear the results
  void clear()
  {
    solutionProto.Clear();
    solutionProto.set_status(LinearModelSolutionStatusProto::SOLVER_UNKNOWN_STATUS);

    solution.first.clear();
    solution.second.clear();
    baseSolution.second.clear();
    resultStatus = ResultStatus::NOT_SOLVED;
  }
};

}  // namespace ortoolsengine

/**
 * Namespace for general utilities for OR toolbox.
 */
namespace orutils {

/// Factory method to build MPSolvers given the back-end solver packages to use
SYS_EXPORT_FCN MPSolver::SPtr createSolverGivenType(
        const std::string& solverId, ::optilab::LinearModelSpecProto_PackageType type);

/// Builds a protobuf object representing the MIP solution given as parameter
SYS_EXPORT_FCN LinearModelSolutionProto buildMIPSolutionProto(orengine::MIPResult::SPtr res);
}  // namespace orutils

}  // namespace toolbox
}  // namespace optilab
