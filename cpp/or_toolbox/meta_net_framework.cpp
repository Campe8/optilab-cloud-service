#include "or_toolbox/meta_net_framework.hpp"

#include <stdexcept>  // for std::invalid_argument

#include "meta_bb/initial_stat.hpp"
#include "or_toolbox/meta_net_solution.hpp"

namespace optilab {
namespace toolbox {

MetaNetFramework::MetaNetFramework(bool isRoot)
: NetFramework(isRoot)
{
}

MetaNetFramework::~MetaNetFramework()
{
  try
  {
    tearDown();
  }
  catch(...)
  {
    // no-op
  }
}  // MetaNetFramework

void MetaNetFramework::init()
{
  // Create network connections
  metabb::NetFramework::initConnections();
}  // init

void MetaNetFramework::tearDown()
{
  // Tear down network connections
  metabb::NetFramework::tearDown();
}  // init

std::shared_ptr<metabb::RacingRampUpParamSet> MetaNetFramework::buildRacingRampUpParamSet()
{
  return std::make_shared<MetaNetRacingRampUpParamSet>();
}  // buildRacingRampUpParamSet

std::shared_ptr<toolbox::MetaNetRacingRampUpParamSet> MetaNetFramework::buildRacingRampUpParamSet(
        metabb::RacingRampUpParamSet::RacingTerminationCriteria termCriteria,
        int numNodesLeft, int racingSeed, int permuteProbSeed, int generateBranchOrderSeed,
        uint64_t timeoutMsec)
{
  return std::make_shared<MetaNetRacingRampUpParamSet>(termCriteria, numNodesLeft, racingSeed,
                                                       permuteProbSeed, generateBranchOrderSeed,
                                                       timeoutMsec);
}  // buildRacingRampUpParamSet

MetaNetInstance::SPtr MetaNetFramework::buildInstance(
        lmcommon::OptimizationSolverPackageType packgageType)
{
  return std::make_shared<MetaNetInstance>(packgageType);
}  // buildInstance

std::shared_ptr<metabb::InitialStat> MetaNetFramework::buildInitialStat()
{
  return std::make_shared<MetaNetInitialStat>();
}  //buildInitialStat

/// Builds and returns a meta-net initial stat object
std::shared_ptr<MetaNetInitialStat> MetaNetFramework::buildMetaNetInitialStat(int maxDepth,
                                                                              int maxTotalDepth)
{
  return std::make_shared<MetaNetInitialStat>(maxDepth, maxTotalDepth);
}  // buildMetaNetInitialStat

std::shared_ptr<metabb::DiffSubproblem> MetaNetFramework::buildDiffSubproblem()
{
  return std::make_shared<MetaNetDiffSubproblem>();
}  // buildDiffSubproblem

std::shared_ptr<MetaNetDiffSubproblem> MetaNetFramework::buildDiffSubproblem(
        std::shared_ptr<MetaNetSolver> solver,
        const std::vector<MPVariable::SPtr>& branchingVarList,
        const std::vector<double>& branchingBoundList,
        const std::vector<MetaNetDiffSubproblem::BoundType>& branchingBoundType,
        const std::vector<MPConstraint::SPtr>& constraintList)
{
  return std::make_shared<MetaNetDiffSubproblem>(solver, branchingVarList, branchingBoundList,
                                                 branchingBoundType, constraintList);
}  // buildDiffSubproblem

std::shared_ptr<metabb::Solution> MetaNetFramework::buildSolution()
{
  return std::make_shared<MetaNetSolution>();
}  // buildSolution

}  // namespace toolbox
}  // namespace optilab
