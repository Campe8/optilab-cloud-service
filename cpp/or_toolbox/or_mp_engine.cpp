// Base class
#include "or_toolbox/or_mp_engine.hpp"

#include <spdlog/spdlog.h>

#include <stdexcept>  // for std::runtime_error

#include "data_structure/json/json.hpp"
#include "engine/engine_constants.hpp"
#include "engine/engine_utils.hpp"
#include "or_toolbox/or_utilities.hpp"
#include "utilities/timer.hpp"

namespace
{
/// Waiting value for an idle processor
constexpr int kWaitOnIdleProcessor{-1};
}  // namespace

namespace optilab {
namespace toolbox {

ORMPEngine::ORMPEngine(const std::string& engineId,
                       const EngineCallbackHandler::SPtr& handler)
    : pEngineId(engineId), pCallbackHandler(handler)
{
  if (pEngineId.empty())
  {
    throw std::invalid_argument("ORMPEngine - empty engine identifier");
  }

  if (!pCallbackHandler)
  {
    throw std::invalid_argument("ORMPEngine - empty engine callback handler");
  }

  timer::Timer timer;

  pMIPResult = std::make_shared<orengine::MIPResult>();

  pMetricsRegister = std::make_shared<MetricsRegister>();

  // Builds the optimizer running ORTools CP models
  buildOptimizer();
  assert(pOptimizer);

  // Start  the asynchronous thread in the optimizer
  pOptimizer->startUp();

  pActiveEngine = true;

  pEngineCreationTimeMsec = timer.getWallClockTimeMsec();
}

ORMPEngine::~ORMPEngine()
{
  try
  {
    turnDown();
  }
  catch (...)
  {
    // Do not throw in destructor
    spdlog::warn("ORMPEngine - thrown in destructor");
  }
}

void ORMPEngine::turnDown()
{
  if (pActiveEngine)
  {
    pActiveEngine = false;
    pOptimizer->tearDown();
    pOptimizer.reset();
  }
}  // turnDown

void ORMPEngine::buildOptimizer()
{
  pOptimizer = std::make_shared<OROptimizer>(pEngineId, pMIPResult, pMetricsRegister);
}  // buildOptimizer

void ORMPEngine::registerInstance(ORInstance::UPtr instance)
{
  if (!pActiveEngine)
  {
    const std::string errMsg =
        "ORMPEngine - registerModel: "
        "trying to register a model on an inactive engine " +
        pEngineId;
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  if (!instance)
  {
    const std::string errMsg =
        "ORMPEngine - registerModel: "
        "empty pointer to the instance for engine " +
        pEngineId;
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  // Start timer for loading the model
  timer::Timer timer;

  // Load the model into the optimizer
  const auto modelProto = instance->getLinearModel();
  try
  {
    pOptimizer->loadModel(modelProto);
  }
  catch (std::exception& ex)
  {
    const std::string errMsg =
        "ORMPEngine - registerModel: "
        "error while loading the model on engine " +
        pEngineId + " " + std::string(ex.what());
    spdlog::error(errMsg);
    throw;
  }
  catch (...)
  {
    const std::string errMsg =
        "ORMPEngine - registerModel: "
        "undefined error while loading the model on engine " +
        pEngineId;
    spdlog::error(errMsg);
    throw;
  }

  pLoadModelLatencyMsec = timer.getWallClockTimeMsec();

  // Reset solution
  std::dynamic_pointer_cast<orengine::MIPResult>(pMIPResult)->clear();
  pMIPResult->modelName = modelProto.model_id();
}  // registerModel

void ORMPEngine::engineWait(int timeoutMsec)
{
  if (pActiveEngine)
  {
    pOptimizer->waitOnTaskCompletion(timeoutMsec);
  }
}  // engineWait

void ORMPEngine::notifyEngine(const orengine::OREvent& event)
{
  switch (event.getType())
  {
    case orengine::OREvent::EventType::kRunEngine:
    {
      processRunModelEvent();
      break;
    }
    case orengine::OREvent::EventType::kLoadInstance:
    {
      registerInstance(event.getInstance());
      break;
    }
    case orengine::OREvent::EventType::kSolutions:
    {
      processCollectSolutionsEvent(event.getNumSolutions());
      break;
    }
    default:
    {
      assert(event.getType() ==
              orengine::OREvent::EventType::kInterruptEngine);
      processInterruptEngineEvent();
      break;
    }
  }
}  // notifyEngine

void ORMPEngine::processRunModelEvent()
{
  // Return if the engine is already active
  if (!pActiveEngine)
  {
    spdlog::warn(
        "ORMPEngine - processRunModelEvent: "
        "inactive engine, returning " +
        pEngineId);

    return;
  }

  timer::Timer timer;

  // Wait on previous tasks to complete.
  // For example, it waits on model registration
  engineWait(kWaitOnIdleProcessor);

  // Trigger the engine to run and return
  pOptimizer->runOptimizer();

  // Latency for running the optimizer
  pOptimizerLatencyMsec = timer.getWallClockTimeMsec();
}  // processRunModelEvent

void ORMPEngine::processInterruptEngineEvent()
{
  if (!pActiveEngine)
  {
    spdlog::warn(
        "ORMPEngine - processInterruptEngineEvent: "
        "inactive engine, returning " +
        pEngineId);
    return;
  }

  pOptimizer->interruptOptimizer();
}  // processKillEngineEvent

void ORMPEngine::processCollectSolutionsEvent(int numSolutions)
{
  (void)numSolutions;

  // @note it is assumed that the processor
  // is not currently adding result into the solution
  orengine::MIPResult::SPtr res =
      std::dynamic_pointer_cast<orengine::MIPResult>(pMIPResult);
  if (!res)
  {
    const std::string errMsg =
        "ORMPEngine - processCollectSolutionsEvent: "
        "empty pointer to result " +
        pEngineId;
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  // Callback method to send results back to the client.
  // @note this was previously done using JSON replies:
  //   auto solutionJson = sciputils::buildMIPSolutionJson(res);
  //   pCallbackHandler->resultCallback(pEngineId, solutionJson->toString());
  pCallbackHandler->resultCallback(pEngineId, orutils::buildMIPSolutionProto(res));

  // Callback to send the metrics back to the client
  collectMetrics();
  auto jsonMetrics = engineutils::createMetricsJson(pMetricsRegister);

  // Callback method to send metrics back to the client
  pCallbackHandler->metricsCallback(pEngineId, jsonMetrics->toString());

  // Completed run
  pCallbackHandler->processCompletionCallback(pEngineId);
}  // processRunModelEvent

void ORMPEngine::collectMetrics()
{
  assert(pMetricsRegister);
  pMetricsRegister->setMetric(jsonmetrics::ENGINE_CREATION_TIME_MSEC,
                              pEngineCreationTimeMsec);
  pMetricsRegister->setMetric(jsonmetrics::LOAD_MODEL_LATENCY_MSEC,
                              pLoadModelLatencyMsec);
}  // collectMetrics

}  // namespace toolbox
}  // namespace optilab
