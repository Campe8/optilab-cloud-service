//
// Copyright OptiLab 2020. All rights reserved.
//
// Base class to a linear and mixed integer
// programming model for Mathematical Programming (MP) problems.
// Components of a linear programming model:
// a) linear objective function;
// b) linear constraints that can be equalities of inequalities;
// c) bounds on variables that can be positive, negative, finite
//    or infinite.
// Mixed Integer Programming (MIP):
// - similar to LP but with additional integrality requirements
//   on variables.
// If all variable are required to be integers, then the problem
// becomes an Integer Programming (IP) problem.
// If some variables are required to be integers and some are
// continuous, then the problem is a MIP problem.
// Both IP and MIP are generally NP-hard problems.
//

#pragma once

#include <cstdint>    // for uint64_t
#include <limits>     // for std::numeric_limits
#include <memory>     // for std::shared_ptr
#include <stdexcept>  // for std::invalid_argument
#include <string>
#include <utility>    // for std::pair
#include <vector>

#include <sparsepp/spp.h>

#include "or_toolbox/linear_model_common.hpp"

#include "system/system_export_defs.hpp"

// Forward declarations
namespace optilab {
namespace toolbox {
class MPSolverInterface;
}  // namespace toolbox
}  // namespace optilab

namespace optilab {
namespace  toolbox {

/**
 * Class encapsulating an MP variable.
 */
class SYS_EXPORT_CLASS MPVariable {
 public:
  using SPtr = std::shared_ptr<MPVariable>;

 public:
  /// Deleted copy constructor
  MPVariable(const MPVariable&) = delete;

  /// Deleted assignment operator
  void operator=(const MPVariable&) = delete;

  /// Returns the name of the variable
  inline const std::string& getName() const noexcept { return pName; }

  /// Sets the integrality requirement of this variable
  void setInteger(bool integer);

  /// Returns true if this is an integer variable,
  /// returns false otherwise
  bool isInteger() const noexcept { return pIsInteger; }

  /// Returns the value (cast to double) of the variable in the
  /// current solution
  double getSolutionValue() const;

  /// Returns the index of the variable in the model
  inline int getIndex() const noexcept { return pIndex; }

  /// Returns the lower bound
  inline double lowerBound() const noexcept { return pLowerBound; }

  /// Returns the upper bound
  inline double upperBound() const noexcept { return pUpperBound; }

  /// Returns the original lower bound
  inline double origLowerBound() const noexcept { return pOrigLowerBound; }

  /// Returns the original upper bound
  inline double origUpperBound() const noexcept { return pOrigUpperBound; }

  /// Sets the lower bound for this variable
  void setLowerBound(double lb) { setBounds(lb, pUpperBound); }

  /// Sets the upper bound for this variable
  void setUpperBound(double ub) { setBounds(pLowerBound, ub); }

  /// Resets the original lower bound for this variable
  void resetOrigLowerBound() { setBounds(pOrigLowerBound, pUpperBound); }

  /// Resets the original upper bound for this variable
  void resetOrigUpperBound() { setBounds(pLowerBound, pOrigUpperBound); }

  /// Sets both the lower and upper bounds for this variable
  void setBounds(double lb, double ub);

  /// Returns the reduced cost of the variable in the current solution.
  /// @note this is only available for continuous problems
  double getReducedCost() const;

  /// Returns the branching priority for this variable.
  /// Higher branching priority means that this variable
  /// is considered first for branching
  inline int branchingPriority() const noexcept { return pBranchingPriority; }

  /// Sets the branching priority.
  /// @note zero or lower means no priority
  void setBranchingPriority(int priority);

  /// Returns the basis status of the variable in the current solution.
  /// @note only available for continuous problems
  lmcommon::BasisStatus basisStatus() const;

 protected:
  friend class MPSolver;
  friend class MPSolverInterface;
  friend class SCIPInterface;

  MPVariable(int index, double lb, double ub, bool isInteger, const std::string& name,
             MPSolverInterface* const solverInterface)
  : pIndex(index),
    pLowerBound(lb),
    pUpperBound(ub),
    pOrigLowerBound(lb),
    pOrigUpperBound(ub),
    pName(name),
    pSolutionValue(0.0),
    pReducedCost(0.0),
    pInterface(solverInterface),
    pIsInteger(isInteger)
  {
    if (!pInterface)
    {
      throw std::invalid_argument("MPVariable: empty pointer to the solver's interface");
    }
  }

  inline void setSolutionValue(double val) noexcept { pSolutionValue = val; }
  inline void setReducedCost(double val) noexcept { pReducedCost = val; }

 private:
  /// Sequential index of the variable in a model
  const int pIndex;

  /// Branching priority of this variables w.r.t.
  /// other variables in the model.
  /// @note The higher the value the higher the priority.
  /// @note a value of zero means no branching priority
  int pBranchingPriority{0};

  /// Current lower and upper bound
  double pLowerBound;
  double pUpperBound;

  /// Original lower and upper bound
  double pOrigLowerBound;
  double pOrigUpperBound;

  /// Name of the variable
  const std::string pName;

  /// Value of the variable on solution
  double pSolutionValue;

  /// Reduced cost
  double pReducedCost;

  /// Pointer to the solver's interface.
  /// @note this variable doesn't manage
  /// the solver's interface life-time
  MPSolverInterface* pInterface;

  /// Boolean flag indicating whether or not
  /// this is an integer variable
  bool pIsInteger;
};

class SYS_EXPORT_CLASS MPConstraint {
 public:
  using SPtr = std::shared_ptr<MPConstraint>;

 public:
  /// Deleted copy constructor
  MPConstraint(const MPConstraint&) = delete;

  /// Deleted assignment operator
  void operator=(const MPConstraint&) = delete;

  /// Returns the name of this constraint
  inline const std::string& getName() const noexcept { return pName; }

  /// Clears all variables and coefficients.
  /// @note it does not clear the bounds
  void clear();

  /// Sets the coefficient of the variable on the constraint.
  /// @note the variable must belong to the same solver that created this constraint
  void setCoefficient(const MPVariable::SPtr& var, double coeff);

  /// Returns the coefficient of the given variable in the scope of this constraint.
  /// @note returns zero if the variable is not in the scope of this constraint
  double getCoefficient(const MPVariable::SPtr& var) const;

  /// Returns the lower bound
  inline double lowerBound() const noexcept { return pLowerBound; }

  /// Returns the upper bound
  inline double upperBound() const noexcept { return pUpperBound; }

  /// Sets the lower bound for this constraint
  void setLowerBound(double lb) { setBounds(lb, pUpperBound); }

  /// Sets the upper bound for this constraint
  void setUpperBound(double ub) { setBounds(pLowerBound, ub); }

  /// Sets both the lower and upper bounds for this variable
  void setBounds(double lb, double ub);

  /// Returns the list of <variable index, coefficient> pairs for this constraint
  std::vector<std::pair<int, double>> getVarCoefficientList() const noexcept;

  /// Returns the dual value of the constraint in the current solution.
  /// @note the dual value is only available for continuous problems
  double getDualValue() const;

  /// Returns the index of this constraint in the constraint list
  /// of the model that created this constraint
  inline int getIndex() const noexcept { return pIndex; }

  /// Returns true if this constraint is lazy.
  /// Returns false otherwise.
  /// @note see also "setIsLazy(...)"
  inline bool isLazy() const noexcept { return pIsLazy; }

  /// If true, the constraint is only considered by the LP solver if
  /// its current solution violates the constraint.
  /// In this case, the constraint is definitively added to the problem.
  /// This may be useful in some MIP problem and may have a lot of impact
  /// on performance.
  /// For example, see http://tinyurl.com/lazy-constraints
  void setIsLazy(bool isLazy) noexcept { pIsLazy = isLazy; }

  MPVariable::SPtr getIndicatorVariable() const { return pIndicatorVariable; }
  bool getIndicatorValue() const noexcept { return pIndicatorValue; }

  /// Returns the basis status of the constraint.
  /// @note only available for continuous problems
  lmcommon::BasisStatus basisStatus() const;

 protected:
  friend class MPSolver;
  friend class MPSolverInterface;
  friend class SCIPInterface;

  MPConstraint(int index, double lb, double ub, const std::string& name,
               MPSolverInterface* const solverInterface)
  : pCoefficients(1),
    pIndex(index),
    pLowerBound(lb),
    pUpperBound(ub),
    pName(name),
    pIndicatorVariable(nullptr),
    pIndicatorValue(false),
    pIsLazy(false),
    pDualValue(0.0),
    pInterface(solverInterface)
  {
  }

 private:
  /// Name of the constraint
  const std::string pName;

  /// Mapping of variables to coefficient
  spp::sparse_hash_map<const MPVariable*, double> pCoefficients;

  double pLowerBound;
  double pUpperBound;

  /// Index of this constraint on the list
  /// of constraints in the model
  const int pIndex;

  // If given, this constraint is only active if "pIndicatorVariable"'s value
  // is equal to "pIndicatorValue"
  MPVariable::SPtr pIndicatorVariable;
  bool pIndicatorValue;

  // True if the constraint is "lazy",
  // i.e. the constraint is added to the underlying LP solver
  // only if it is violated.
  // By default this parameter is 'false'.
  bool pIsLazy;

  double pDualValue;

  /// Pointer to the solver's interface.
  /// @note this constraint doesn't manage
  /// the solver's interface life-time
  MPSolverInterface* const pInterface;

  /// Returns true if the constraint contains variables that have not
  /// been extracted yet. Returns false otherwise
  bool containsNewVariables();
};

class SYS_EXPORT_CLASS MPObjective {
 public:
  using SPtr = std::shared_ptr<MPObjective>;

 public:
  /// Deleted copy constructor
  MPObjective(const MPObjective&) = delete;

  /// Deleted assignment operator
  void operator=(const MPObjective&) = delete;

  /// Clears the offset, all variables and coefficients,
  /// and the optimization direction
  void clear();

  /// Sets the coefficient of the variable in the objective
  void setCoefficient(const MPVariable::SPtr& var, double coeff);

  /// Returns the coefficient of the given variable in the objective
  double getCoefficient(const MPVariable::SPtr& var) const;

  /// Sets the constant term in the objective
  void setOffset(double value);

  /// Returns the constant term in the objective.
  /// DEPRECATED: use "getOffset(...)" instead
  inline double offset() const noexcept { return pOffset; }

  /// Returns the constant term in the objective
  inline double getOffset() const noexcept { return pOffset; }

  /// Sets the optimization direction:
  /// - maximize: true; or
  /// - minimize: false
  void setOptimizationDirection(bool maximize);

  /// Sets the optimization direction to minimize
  void setMinimization() { setOptimizationDirection(false); }

  /// Sets the optimization direction to maximize.
  void setMaximization() { setOptimizationDirection(true); }

  /// Is the optimization direction set to maximize?
  bool maximization() const;

  /// Is the optimization direction set to minimize?
  bool minimization() const;

  /// Returns the objective value of the best solution
  /// found so far
  double getValue() const;

  /// Returns the best objective bound.
  /// @note this represent the lower bound on the objective value
  /// of the optimal integer solution.
  /// @note this is only available for discrete problem
  double getBestBound() const;

 protected:
  friend class MPSolver;
  friend class MPSolverInterface;
  friend class SCIPInterface;

  explicit MPObjective(MPSolverInterface* const solverInterface)
  : pInterface(solverInterface), pOffset(0.0) {}

 private:
  /// Constant term
  double pOffset;

  /// Pointer to the solver's interface.
  /// @note this objective doesn't manage
  /// the solver's interface life-time
  MPSolverInterface* const pInterface;

  /// Mapping of variables to coefficient
  spp::sparse_hash_map<const MPVariable*, double> pCoefficients;
};

}  // namespace toolbox
}  // namespave optilab
