//
// Copyright OptiLab 2020. All rights reserved.
//
// Distributed OR implementation based on metabb framework for the
// Initiator class.
//

#pragma once

#include <limits>   // for std::numeric_limits
#include <memory>   // for std::shared_ptr
#include <string>
#include <utility>  // for std::pair
#include <vector>

#include "meta_bb/framework.hpp"
#include "meta_bb/initiator.hpp"
#include "meta_bb/meta_bb_constants.hpp"
#include "optilab_protobuf/meta_bb_packet.pb.h"
#include "or_toolbox/linear_model.hpp"
#include "or_toolbox/linear_solver.hpp"
#include "or_toolbox/meta_net_instance.hpp"
#include "or_toolbox/meta_net_paramset.hpp"
#include "or_toolbox/meta_net_solution.hpp"
#include "system/system_export_defs.hpp"
#include "utilities/timer.hpp"

namespace optilab {
namespace toolbox {

class SYS_EXPORT_CLASS MetaNetInitiator : public metabb::Initiator {
 public:
  using SPtr = std::shared_ptr<MetaNetInitiator>;

 public:
  MetaNetInitiator(MetaNetInstance::SPtr instance,
                   metabb::Framework::SPtr framework,
                   timer::Timer::SPtr timer=nullptr);

  /// Sets warm start, if any.
  /// @note this should be done BEFORE calling "init(...)"
  void setWarmStart(const metabb::Initiator::WarmStartHint& warmStart);

  /// Initialize the initiator given a paramset
  int init(metabb::ParamSet::SPtr paramset) override;

  /// Re-initialize given the number of restart racing previously done
  int reInit(int numRestartedRacing) override;

  /// Returns the optimization instance build by this initiator
  metabb::Instance::SPtr getInstance() override;
  inline MetaNetInstance::SPtr getMetaNetInstance() const noexcept { return pInstance; }

  /// Builds and return the DiffSubproblem object for the root node
  metabb::DiffSubproblem::SPtr buildRootNodeDiffSubproblem() override;

  /// Sends solver initialization message over the network
  void sendSolverInitializationMessage() override;

  /// Generates and fills the list of racing ramp-up parameter sets
  void generateRacingRampUpParameterSets(
          std::vector<metabb::RacingRampUpParamSet::SPtr>& racingRampUpParamSet) override;

  /// Converts (and returns) an internal value to an external value
  double convertToExternalValue(double internalValue) override;

  /// Converts (and returns) an external value to an internal value
  double convertToInternalValue(double externalValue);

  /// Returns the best incumbent optimization solution
  metabb::Solution::SPtr getGlobalBestIncumbentSolution() override;

  /// Tries to set the incumbent solution
  bool tryToSetIncumbentSolution(metabb::Solution::SPtr sol, bool checksol) override;

  /// Returns the absolute value
  double getAbsGap(double dualBoundValue) override;

  /// Returns the gap
  double getGap(double dualBoundValue) override;

  /// Returns the absolute value gap
  double getAbsGapValue() override;

  /// Returns the gap value
  double getGapValue() override;

  /// Returns the epsilon value to be used in the framework.
  /// If a value is less than this epsilon, it will be replaced with zero
  double getEpsilon() const noexcept override;

  /// Sets the final status of the solver
  void setFinalSolverStatus(metabb::solver::FinalSolverState state) noexcept override;

  /// Sets the total number of solved nodes
  void setNumSolvedNodes(long long numNodes) noexcept override;

  /// Sets the (final) dual bound
  void setDualBound(double bound) noexcept override;

  /// Returns true if a feasible solution exists,
  /// returns false otherwise
  bool isFeasibleSolution() override;

  /// Sets initial stat on initiator
  void accumulateInitialStat(metabb::InitialStat::SPtr initialStat) override;

  /// Writes the solution
  void writeSolution(const std::string& message) override;

  /// Returns true if  the objective value is known to be integral in every feasible solution,
  /// returns false otherwise
  bool isObjIntegral() override { return false; }

  /// Returns true if the initiator can generate a special cut-off value.
  /// @note default returns false
  bool canGenerateSpecialCutOffValue() override { return false; }

  /// Sets initial statistics on DiffSubproblem
  void setInitialStatOnDiffSubproblem(int minDepth, int maxDepth,
                                      metabb::DiffSubproblem::SPtr diffSubproblem) override;

 private:
  /// Model instance and solver.
  /// @note the instance contains the MP solver
  MetaNetInstance::SPtr pInstance;

  /// Solution
  MetaNetSolution::SPtr pSolution;

  /// Paramset at the root
  MetaNetParamSet::SPtr pRootParamSet;

  /// Paramset for the instance
  MetaNetParamSet::SPtr pParamSet;

  /// Final dual bound value
  double pFinalDualBound{-std::numeric_limits<double>::max()};

  /// Final state of the solver
  metabb::solver::FinalSolverState pFinalSolverState{metabb::solver::FinalSolverState::FSS_ABORTED};

  /// Number of nodes solved
  long long pNumSolved{0};

  /// Absolute gap
  double pAbsGap{-1.0};

  /// Gap dual/primal bounds
  double pGap{-1.0};

  /// Limit on objective value
  double pObjLimit{std::numeric_limits<double>::max()};
};

}  // namespace metabb

/// Specialized template for network node packet comparison
template<> bool optnet::NetworkPacket<metabb::InitiatorInitSystemMessageProtoPacket>::operator==(
    const NetworkPacket<metabb::InitiatorInitSystemMessageProtoPacket>& other) const noexcept;

/// Specialized template to serialize InitiatorInitSystemMessageProtoPacket(s)
template<>
std::pair<optnet::BaseNetworkPacket::BaseNetworkPacketType, std::string>
optnet::NetworkPacket<metabb::InitiatorInitSystemMessageProtoPacket>::serialize() const noexcept;

}  // namespace optilab

