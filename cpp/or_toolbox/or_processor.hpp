//
// Copyright OptiLab 2020. All rights reserved.
//
// Processor for MP models.
//

#pragma once

#include <memory>   // for std::shared_ptr
#include <string>
#include <map>
#include <utility>  // for std::pair
#include <vector>

#include "engine/optimizer_processor.hpp"
#include "optilab_protobuf/linear_model.pb.h"
#include "or_toolbox/linear_model_common.hpp"
#include "or_toolbox/linear_solver.hpp"
#include "or_toolbox/or_utilities.hpp"
#include "system/system_export_defs.hpp"
#include "utilities/csv_reader.hpp"

#include <boost/thread.hpp>

namespace optilab {
namespace toolbox {

class SYS_EXPORT_CLASS ORProcessor :
public OptimizerProcessor<orengine::ORWork::SPtr, orengine::ORWork::SPtr> {
public:
  using SPtr = std::shared_ptr<ORProcessor>;

public:
  explicit ORProcessor(orengine::ORResult::SPtr result);

  /// Loads the protobuf linear model and creates a solver to run on it.
  /// @note throw std::invalid_argument on empty model
  void loadModelAndCreateSolver(const LinearModelSpecProto& model);

  /// Interrupts the internal solving process (if running).
  /// The solver remains in a non-executable state until another model is loaded
  bool interruptSolver();

protected:
  using Work = orengine::ORWork::SPtr;
  using BaseProcessor = OptimizerProcessor<Work, Work>;

  /// Lock type on the internal mutex
  using LockGuard = boost::lock_guard<boost::mutex>;

protected:
  void processWork(Work work) override;

  /// Returns the internal solver
  inline MPSolver::SPtr getSolver() const { return pSolver; }

private:
  //
  bool pModelInfeasible{false};

  /// Solver for MP models
  MPSolver::SPtr pSolver;

  /// Pointer to the downcast MIP result instance
  orengine::MIPResult::SPtr pResult;

  /// Mutex synch.ing the state of the result
  boost::mutex pResultMutex;

  /// List of variables part of the objective function
  std::vector<std::pair<MPVariable::SPtr, double>> pObjVarList;

  /// Status of the search result
  lmcommon::ResultStatus pResultStatus;

  /// Utility function: runs the solver
  void runSolver(Work& work);

  /// Utility function: store solution
  void storeSolution();

  /// Utility function: sets the result status of the solving process
  void setResultStatus();

  /// Utility function: load the given OptModelProto
  void loadProtoModel(const OptModelProto& model);

  /// Utility function: parses the model and adds the variables to the model
  void addProtoVariables(const OptModelProto& model);

  /// Utility function: parses the model and adds constraints to the model
  void addProtoConstraints(const OptModelProto& model);

  /// Utility function: adds the objectives to the model
  void addProtoObjective(const OptModelProto& model);

  /// Utility function: load the given MPS model.
  /// The input is the path to the .mps file
  void loadMPSModel(const std::string& path);

  /// Utility function: load the given model represented as a matrix:
  /// A x <= b
  /// A x  = b
  /// min cx
  /// The input is the path to the csv files
  void loadMatrixModel(const std::string& path);

  /// Load "numVars" Boolean variables into the model
  void loadMatrixVariables(int numVars);

  /// Load the constraint into the model
  void loadMatrixConstraints(const utilsreader::CSVReader::CSVData& dataA,
                             const utilsreader::CSVReader::CSVData& dataB,
                             int numVars, int numCons, bool isIneq, int startIdx = 0);

  /// Load the objective function into the model
  void loadMatrixObjective(const utilsreader::CSVReader::CSVData& dataObj, bool isMaximization);
};

}  // namespace toolbox
}  // namespace optilab
