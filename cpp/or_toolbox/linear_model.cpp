#include "or_toolbox/linear_model.hpp"

#include <cassert>
#include <cmath>  // for std::isnan

#include "or_toolbox/linear_solver.hpp"

namespace optilab {
namespace toolbox {

void MPVariable::setInteger(bool integer)
{
  if (pIsInteger != integer)
  {
    pIsInteger = integer;
    if (pInterface->variableIsExtracted(pIndex))
    {
      pInterface->setVariableInteger(pIndex, integer);
    }
  }
}  // setInteger

void MPVariable::setBranchingPriority(int priority)
{
  if (priority == pBranchingPriority) return;
  pBranchingPriority = priority;
  pInterface->branchingPriorityChangedForVariable(pIndex);
}  // setBranchingPriority

lmcommon::BasisStatus MPVariable::basisStatus() const
{
  if (!pInterface->isContinuous())
  {
    spdlog::error("MPVariable - basisStatus: only available for continuous problems");
    return lmcommon::BasisStatus::FREE;
  }

  if (!pInterface->checkSolutionIsSynchronizedAndExists())
  {
    return lmcommon::BasisStatus::FREE;
  }

  return pInterface->columnStatus(pIndex);
}  // basisStatus

void MPVariable::setBounds(double lb, double ub)
{
  const bool change = lb != pLowerBound || ub != pUpperBound;
  pLowerBound = lb;
  pUpperBound = ub;

  if (change && pInterface->variableIsExtracted(pIndex))
  {
    pInterface->setVariableBounds(pIndex, pLowerBound, pUpperBound);
  }
}  // setBounds

double MPVariable::getSolutionValue() const
{
  if (!pInterface->checkSolutionIsSynchronizedAndExists()) return 0.0;

  // If the underlying solver supports integer variables, and this is an integer
  // variable, round the solution value (i.e., clients usually expect precise
  // integer values for integer variables).
  return (pIsInteger && pInterface->isMIP()) ? round(pSolutionValue) : pSolutionValue;
}

double MPVariable::getReducedCost() const {
  if (!pInterface->isContinuous())
  {
    spdlog::error("MPVariable - getReducedCost: "
        "reduced cost only available for continuous problems");
    return 0.0;
  }
  if (!pInterface->checkSolutionIsSynchronizedAndExists()) return 0.0;
  return pReducedCost;
}  // getReducedCost


bool MPConstraint::containsNewVariables()
{
  const int lastVariableIndex = pInterface->lastVariableIndex();
  for (const auto& entry : pCoefficients)
  {
    const int varIdx = entry.first->getIndex();
    if (varIdx >= lastVariableIndex || !pInterface->variableIsExtracted(varIdx))
    {
      return true;
    }
  }
  return false;
}  // containsNewVariables

double MPConstraint::getDualValue() const
{
  if (!pInterface->isContinuous())
  {
    spdlog::error("MPVariable - getDualValue: "
        "dual cost only available for continuous problems");
    return 0.0;
  }
  if (!pInterface->checkSolutionIsSynchronizedAndExists()) return 0.0;
  return pDualValue;
}  // getDualValue

lmcommon::BasisStatus MPConstraint::basisStatus() const
{
  if (!pInterface->isContinuous())
  {
    spdlog::error("MPConstraint - basisStatus: only available for continuous problems");
    return lmcommon::BasisStatus::FREE;
  }
  if (!pInterface->checkSolutionIsSynchronizedAndExists())
  {
    return lmcommon::BasisStatus::FREE;
  }

  return pInterface->rowStatus(pIndex);
}  // basisStatus

void MPConstraint::clear()
{
  pInterface->clearConstraint(this);
  pCoefficients.clear();
}

void MPConstraint::setBounds(double lb, double ub)
{
  const bool change = lb != pLowerBound || ub != pUpperBound;
  pLowerBound = lb;
  pUpperBound = ub;
  if (change && pInterface->constraintIsExtracted(pIndex))
  {
    pInterface->setConstraintBounds(pIndex, pLowerBound, pUpperBound);
  }
}  // setBounds

std::vector<std::pair<int, double>> MPConstraint::getVarCoefficientList() const noexcept
{
  std::vector<std::pair<int, double>> coeffList;
  for (const auto& it : pCoefficients)
  {
    coeffList.push_back(std::make_pair(it.first->getIndex(), it.second));
  }
  return coeffList;
}  //getVarCoefficientList

double MPConstraint::getCoefficient(const MPVariable::SPtr& var) const
{
  if (!var) return 0.0;
  auto it = pCoefficients.find(var.get());
  if (it == pCoefficients.end()) return 0.0;
  return it->second;
}  // getCoefficient

void MPConstraint::setCoefficient(const MPVariable::SPtr& var, double coeff)
{
  if (!var) return;
  if (coeff == 0.0)
  {
    auto it = pCoefficients.find(var.get());

    // If setting a coefficient to 0 when this coefficient did not
    // exist or was already 0, do nothing: skip
    // interface_->SetCoefficient() and do not store a coefficient in
    // the map.  Note that if the coefficient being set to 0 did exist
    // and was not 0, we do have to keep a 0 in the coefficients_ map,
    // because the extraction of the constraint might rely on it,
    // depending on the underlying solver.
    if (it != pCoefficients.end() && it->second != 0.0)
    {
      const double oldValue = it->second;
      it->second = 0.0;
      pInterface->setCoefficient(this, var.get(), 0.0, oldValue);
    }
    return;
  }

  auto insertionResult = pCoefficients.insert(std::make_pair(var.get(), coeff));
  const double oldValue = insertionResult.second ? 0.0 : insertionResult.first->second;

  insertionResult.first->second = coeff;
  pInterface->setCoefficient(this, var.get(), coeff, oldValue);
}  // setCoefficient

double MPObjective::getCoefficient(const MPVariable::SPtr& var) const
{
  if (!var) return 0.0;
  auto it = pCoefficients.find(var.get());
  if (it == pCoefficients.end()) return 0.0;
  return it->second;
}  // getCoefficient

void MPObjective::setCoefficient(const MPVariable::SPtr& var, double coeff)
{
  if (!var) return;
  if (coeff == 0.0)
  {
    auto it = pCoefficients.find(var.get());
    if (it == pCoefficients.end() || it->second == 0.0) return;
    it->second = 0.0;
  }
  else
  {
    pCoefficients[var.get()] = coeff;
  }
  pInterface->setObjectiveCoefficient(var.get(), coeff);
}  // setCoefficient

void MPObjective::setOffset(double value)
{
  pOffset = value;
  pInterface->setObjectiveOffset(pOffset);
}  // setOffset

void MPObjective::clear()
{
  pInterface->clearObjective();
  pCoefficients.clear();
  pOffset = 0.0;

  // Default set minimization
  setMinimization();
}  // clear

void MPObjective::setOptimizationDirection(bool maximize)
{
  pInterface->pMaximize = maximize;
  pInterface->setOptimizationDirection(maximize);
}  // setOptimizationDirection

bool MPObjective::maximization() const { return pInterface->pMaximize; }

bool MPObjective::minimization() const { return !pInterface->pMaximize; }

double MPObjective::getValue() const
{
  return pInterface->objectiveValue();
}  // getValue

double MPObjective::getBestBound() const
{
  return pInterface->bestObjectiveBound();
}  // getBestBound

}  // namespace toolbox
}  // namespace optilab
