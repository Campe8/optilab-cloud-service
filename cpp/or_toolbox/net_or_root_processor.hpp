//
// Copyright OptiLab 2020. All rights reserved.
//
// Processor for the root node of the
// distributed OR (MP) engine.
// Notice that a processor is usually part of
// an optimizer pipeline calling work task asynchronously.
// This processor has implemented synchronous method in
// order to provide a stand-alone object executable.
// The optimizer should not use the synchronous methods
// but push Work tasks asynchronously.
//

#pragma once

#include "or_toolbox/net_or_processor.hpp"

#include <memory>  // for std::shared_ptr
#include <vector>

#include "optilab_protobuf/linear_model.pb.h"
#include "optilab_protobuf/optimizer_model.pb.h"
#include "or_toolbox/or_utilities.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace toolbox {

class SYS_EXPORT_CLASS NetORProcessorModel {
 public:
  using SPtr = std::shared_ptr<NetORProcessorModel>;
 public:
  NetORProcessorModel();
  ~NetORProcessorModel() = default;

  /// Sets model name
  void setName(const std::string& name);

  /// Adds a variable to the model.
  /// Returns the index of the variable in the model
  int addVariable(const std::string& name, double lb, double ub, bool isInt,
                  double objCoeff=0.0);

  /// Adds a constraint to the model
  void addConstraint(
          const std::vector<int>& varIdx,
          const std::vector<double>& varCoeff,
          double lb,
          double ub,
          const std::string& name);

  void setObjectiveDirection(bool maximize=false);

  const LinearModelSpecProto& getProtoModel() const noexcept;

 private:
  std::unique_ptr<OptimizerModel> pProtoModel;
};

class SYS_EXPORT_CLASS NetORRootProcessor : NetORProcessor {
public:
  using SPtr = std::shared_ptr<NetORRootProcessor>;

public:
  NetORRootProcessor();

  /// Loads the model.
  /// @note this must be called BEFORE initializing the network
  void loadModel(NetORProcessorModel::SPtr model);

  /// Initializes the root network node.
  /// @note this must be done AFTER the model is loaded
  void initRootNetworkNode();

  /// Runs the processor on the loaded model
  void run();

  /// Collects solutions and returns the MIP result
  orengine::MIPResult::SPtr collectSolutions();
};

}  // namespace toolbox
}  // namespace optilab
