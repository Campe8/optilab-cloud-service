//
// Copyright OptiLab 2020. All rights reserved.
//
// Entry point for the OptiLab Net-OR (Network OR)
// application.
//

#include <getopt.h>
#include <signal.h>

#include <iostream>
#include <limits>  // for std::numeric_limits
#include <memory>  // for std::shared_ptr
#include <string>
#include <vector>

#include "data_structure/metrics/metrics_register.hpp"
#include "or_toolbox/net_or_node_processor.hpp"
#include "or_toolbox/net_or_root_processor.hpp"
#include "or_toolbox_client/mock_or_processor.hpp"
#include "or_toolbox/or_utilities.hpp"

extern int optind;

#ifdef LINK_TEST_WITH_PROTOBUF
namespace {

const std::string kCSVMatrixPath = "/home/federicocampeotto/personal/gtech";

void printHelp(const std::string& programName) {
  std::cerr << "Usage: " << programName << " [options]"
      << std::endl
      << "options:" << std::endl
      << "  --root|-r           Execute as root node." << std::endl
      << "  --single|-s         Execute as single instance." << std::endl
      << "  --help|-h           Print this help message."
      << std::endl;
}  // printHelp

optilab::toolbox::NetORRootProcessor::SPtr orRootProc;
optilab::toolbox::NetORNodeProcessor::SPtr orNodeProc;

void runSingleOR(const std::string& name)
{
  using namespace optilab;
  using namespace toolbox;

  // Prepare a model
  std::cout << "Single OR processor - create processor..." << std::endl;
  auto proc = std::make_shared<MockORProcessor>();
  std::cout << "...done" << std::endl;

  // Create the model

  //proc->loadModelAndCreateSolver(model);
  std::cout << "Single OR processor - load model..." << std::endl;
  proc->loadModelAndCreateSolver(name, kCSVMatrixPath);
  std::cout << "...done" << std::endl;

  std::cout << "Single OR processor - run model..." << std::endl;
  proc->runProcessor();
  std::cout << "...done" << std::endl;

  // Delete processor instance
  proc.reset();
}  // runSingleOR

void runRootNetOR(const std::string& name)
{
  using namespace optilab;
  using namespace toolbox;

  // Prepare a model
  auto model = std::make_shared<NetORProcessorModel>();
  model->setName(name);

  // Maximize x + 10y

  // x integer [0, 1000]
  // y integer [0, 1000]
  const bool isInt{true};
  int varIdx_x = model->addVariable("x", 0.0, std::numeric_limits<double>::max(), isInt, 1.0);
  int varIdx_y = model->addVariable("y", 0.0, std::numeric_limits<double>::max(), isInt, 10.0);

  // x + 7y <= 17.5
  model->addConstraint(
          {varIdx_x, varIdx_y},
          {1.0, 7.0},
          std::numeric_limits<double>::lowest(),
          17.5,
          "c1");

  // x <= 3.5
  model->addConstraint(
          {varIdx_x},
          {1.0},
          std::numeric_limits<double>::lowest(),
          3.5,
          "c2");

  const bool maximize{true};
  model->setObjectiveDirection(maximize);

  // Create the root processor
  orRootProc = std::make_shared<NetORRootProcessor>();

  // Load the model
  std::cout << "Root NetOR - load model..." << std::endl;
  orRootProc->loadModel(model);
  std::cout << "...done" << std::endl;

  // Initialize the network node
  std::cout << "Root NetOR - initialize root network node..." << std::endl;
  orRootProc->initRootNetworkNode();
  std::cout << "...done" << std::endl;

  // Run the processor
  std::cout << "Root NetOR - run root network node..." << std::endl;
  orRootProc->run();
  std::cout << "...done" << std::endl;

  // Get the solution
  std::cout << "Root NetOR - collect solution on root network node..." << std::endl;
  auto solution = orRootProc->collectSolutions();
  std::cout << "...done" << std::endl;

  // Print solution
  const auto& sol = solution->baseSolution;
  std::cout << "Solution for model \"" << solution->modelName <<
          "\":" << std::endl;
  std::cout << "\tObjective value: " << sol.first << std::endl;

  int idx{0};
  for (auto val : sol.second)
  {
    std::cout << "\tvar_" << idx++ << ": " <<  val << std::endl;
  }

  // Delete processor instance
  orRootProc.reset();
}  // runRootNetOR

void runNodeNetOR()
{
  using namespace optilab;
  using namespace toolbox;

  // Create the node processor
  orNodeProc = std::make_shared<NetORNodeProcessor>();

  // Initialize the network node
  std::cout << "Network NetOR - initialize network node..." << std::endl;
  orNodeProc->initNetworkNode();
  std::cout << "...done" << std::endl;

  // Run the processor
  std::cout << "Network NetOR - run network node..." << std::endl;
  orNodeProc->run();
  std::cout << "...done" << std::endl;

  // Delete processor instance
  orNodeProc.reset();
}  // runNodeNetOR

void sigHandler(int)
{
  if (orRootProc)
  {
    orRootProc.reset();
  }

  if (orNodeProc)
  {
    orNodeProc.reset();
  }
}  // sigHandler

}  // namespace

#endif

int main(int argc, char* argv[]) {

  char optString[] = "rhs";
  struct option longOptions[] =
  {
      { "root", no_argument, NULL, 'r' },
      { "single", no_argument, NULL, 's' },
      { "help", no_argument, NULL, 'h' },
      { 0, 0, 0, 0 }
  };

#ifdef LINK_TEST_WITH_PROTOBUF
  // Init empty OR processors
  orRootProc = nullptr;
  orNodeProc = nullptr;

  // Capture signals
  signal(SIGINT, sigHandler);

  // Parse options
  int opt;
  bool runAsRoot{false};
  bool runAsSingle{true};
  while (-1 != (opt = getopt_long(argc, argv, optString, longOptions, NULL)))
  {
    switch (opt)
    {
      case 'r':
      {
        runAsRoot = true;
        break;
      }
      case 's':
      {
        runAsSingle = true;
        break;
      }
      case 'h':
      default:
        printHelp(argv[0]);
        return 0;
    }
  }  // while

  const std::string modelName{"MIP"};
  if (runAsRoot)
  {
    try
    {
      std::cout << "=== Running Root OR node ===" << std::endl;
      runRootNetOR(modelName);
      std::cout << "=== Done running Root OR node ===" << std::endl;
    }
    catch(...)
    {
      std::cerr << "Error while running root OR node" << std::endl;
      return 1;
    }
  }
  else if (runAsSingle)
  {
    try
    {
      std::cout << "=== Running as single instance ===" << std::endl;
      runSingleOR(modelName);
      std::cout << "=== Done running as single instance ===" << std::endl;
    }
    catch(...)
    {
      std::cerr << "Error while running as single instance" << std::endl;
      return 1;
    }
  }
  else
  {
    try
    {
      std::cout << "=== Running Network OR node ===" << std::endl;
      runNodeNetOR();
      std::cout << "=== Done running Network OR node ===" << std::endl;
    }
    catch(...)
    {
      std::cerr << "Error while running network OR node" << std::endl;
      return 1;
    }
  }
#endif
  return 0;
}
