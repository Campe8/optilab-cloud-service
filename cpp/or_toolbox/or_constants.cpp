#include "or_toolbox/or_constants.hpp"

namespace optilab {
namespace toolbox {

namespace orconstants {
const std::string CSV_MODEL_A_EQ = "A_eq.csv";
const std::string CSV_MODEL_A_INEQ = "A_ineq.csv";
const std::string CSV_MODEL_B_EQ = "B_eq.csv";
const std::string CSV_MODEL_B_INEQ = "B_ineq.csv";
const std::string CSV_MODEL_F_MAX = "f_max.csv";
const std::string CSV_MODEL_F_MIN = "f_min.csv";
}  // namespace orconstants

}  // namespace toolbox
}  // namespace optilab
