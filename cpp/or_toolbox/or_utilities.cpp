#include "or_toolbox/or_utilities.hpp"

#include <cassert>
#include <cstdint>    // for int64_t
#include <stdexcept>  // for std::runtime_error

#include <boost/any.hpp>
#include <sparsepp/spp.h>

#include "engine/engine_constants.hpp"
#include "engine/engine_utils.hpp"
#include "model/model_constants.hpp"
#include "model/model_object_constraint.hpp"
#include "model/model_object_intension_constraint.hpp"
#include "or_toolbox/linear_model_common.hpp"
#include "utilities/strings.hpp"
#include "utilities/variables.hpp"

namespace optilab {
namespace toolbox {

namespace orengine {

void MIPResult::uploadToProtoMessage()
{
  solutionProto.Clear();

  switch(resultStatus)
  {
    case orengine::MIPResult::ResultStatus::FEASIBLE:
      solutionProto.set_status(LinearModelSolutionStatusProto::SOLVER_FEASIBLE);
      break;
    case orengine::MIPResult::ResultStatus::INFEASIBLE:
      solutionProto.set_status(LinearModelSolutionStatusProto::SOLVER_INFEASIBLE);
      break;
    case orengine::MIPResult::ResultStatus::NOT_SOLVED:
      solutionProto.set_status(LinearModelSolutionStatusProto::SOLVER_NOT_SOLVED);
      break;
    case orengine::MIPResult::ResultStatus::OPTIMUM:
      solutionProto.set_status(LinearModelSolutionStatusProto::SOLVER_OPTIMAL);
      break;
    case orengine::MIPResult::ResultStatus::ABNORMAL:
      solutionProto.set_status(LinearModelSolutionStatusProto::SOLVER_ABNORMAL);
      break;
    case orengine::MIPResult::ResultStatus::MODEL_INVALID:
      solutionProto.set_status(LinearModelSolutionStatusProto::SOLVER_MODEL_INVALID);
      break;
    default:
      assert(resultStatus == orengine::MIPResult::ResultStatus::UNBOUNDED);
      solutionProto.set_status(LinearModelSolutionStatusProto::SOLVER_UNBOUNDED);
      break;
  }

  // Objective value
  solutionProto.set_objective_value(baseSolution.first);

  // Variable assignment
  for (const auto val : baseSolution.second)
  {
    auto vassign = solutionProto.add_variable_assign();
    vassign->add_var_value(val);
  }
}  // uploadToProtoMessage

}  // namespace orengine

namespace orutils {
LinearModelSolutionProto buildMIPSolutionProto(orengine::MIPResult::SPtr res)
{
  if (!res)
  {
    throw std::runtime_error("OR Toolbox::buildMIPSolutionProto - empty result");
  }

  LinearModelSolutionProto sol;

  if ((res->solution).first.empty() && (res->solution).second.empty())
  {
    sol.set_status(LinearModelSolutionStatusProto::SOLVER_MODEL_INVALID);
    sol.set_status_str("OR-MIP: empty solution. "
            "Probably invalid model, contact Parallel support.");
    return sol;
  }

  // Set status
  switch (res->resultStatus)
  {
    case orengine::MIPResult::ResultStatus::FEASIBLE:
      sol.set_status(LinearModelSolutionStatusProto::SOLVER_FEASIBLE);
      break;
    case orengine::MIPResult::ResultStatus::INFEASIBLE:
      sol.set_status(LinearModelSolutionStatusProto::SOLVER_INFEASIBLE);
      break;
    case orengine::MIPResult::ResultStatus::NOT_SOLVED:
      sol.set_status(LinearModelSolutionStatusProto::SOLVER_NOT_SOLVED);
      break;
    case orengine::MIPResult::ResultStatus::OPTIMUM:
      sol.set_status(LinearModelSolutionStatusProto::SOLVER_OPTIMAL);
      break;
    default:
      assert(res->resultStatus == orengine::MIPResult::ResultStatus::UNBOUNDED);
      sol.set_status(LinearModelSolutionStatusProto::SOLVER_UNBOUNDED);
      break;
  }

  // Objective value
  sol.set_objective_value((res->solution).first.at(0));

  // Variable assignment
  for (const auto& varSolution : (res->solution).second)
  {
    auto vassign = sol.add_variable_assign();
    vassign->set_var_name(varSolution.first);
    for (const auto& val : varSolution.second)
    {
      // Using only the first value since for MIP problems
      // the variables are all assigned
      vassign->add_var_value(val.first);
    }
  }

  return sol;
}  // buildMIPSolutionProto

MPSolver::SPtr createSolverGivenType(const std::string& solverId,
                                     ::optilab::LinearModelSpecProto_PackageType type)
{
  MPSolver::SPtr solver;
  switch(type)
  {
    case ::optilab::LinearModelSpecProto_PackageType::LinearModelSpecProto_PackageType_SCIP:
      return std::make_shared<MPSolver>(
              solverId, lmcommon::OptimizationSolverPackageType::OSPT_SCIP);
    default:
      return nullptr;
  }
}  // createSolverGivenType

}  // namespace orutils

}  // namespace ortoolbox
}  // namespace optilab
