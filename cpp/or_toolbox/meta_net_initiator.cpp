#include "or_toolbox/meta_net_initiator.hpp"

#include <cassert>
#include <cmath>      // for std::abs
#include <cstdint>    // for uint64_t
#include <stdexcept>  // for std::invalid_argument

#include <google/protobuf/util/message_differencer.h>
#include <spdlog/spdlog.h>

#include "meta_bb/meta_bb_constants.hpp"
#include "meta_bb/meta_bb_macros.hpp"
#include "meta_bb/racing_rampup_paramset.hpp"

#include "or_toolbox/meta_net_framework.hpp"
#include "or_toolbox/meta_net_initial_stat.hpp"
#include "or_toolbox/meta_net_racing_rampup_paramset.hpp"

namespace {
constexpr int kRootNodeRank = 0;
constexpr int kRampUpPhaseRacing = 1;
constexpr int kRampUpPhaseRebuildTreeAfterRacing = 2;
}  // namespace

namespace optilab {
namespace toolbox {

MetaNetInitiator::MetaNetInitiator(MetaNetInstance::SPtr instance,
                                   metabb::Framework::SPtr framework,
                                   timer::Timer::SPtr timer)
: metabb::Initiator(framework, timer),
  pInstance(instance)
{
  if (!pInstance)
  {
    throw std::invalid_argument("MetaNetInitiator - empty pointer to the instance");
  }
}

void MetaNetInitiator::setWarmStart(const metabb::Initiator::WarmStartHint& warmStart)
{
  pWarmStart = warmStart;
}  // setWarmStart

int MetaNetInitiator::init(metabb::ParamSet::SPtr paramset)
{
  // Initialize the initiator.
  // Notice that the MPSolver has been initialized already from the instance
  if (!paramset)
  {
    spdlog::error("MetaNetInitiator - init: empty pointer to parameter set");
    return 1;
  }

  // Save the parameter set
  pParamSet = std::dynamic_pointer_cast<MetaNetParamSet>(paramset);
  if (!pParamSet)
  {
    spdlog::error("MetaNetInitiator - init: "
            "invalid downcast to MetaNetParamSet");
    return 1;
  }
  pRootParamSet = pParamSet;

  // Initialize the final dual bound
  pFinalDualBound = std::numeric_limits<double>::max();

  // Create the solution
  // pSolution = std::make_shared<MetaNetSolution>();

  // Set the warm start, if any
  if (isWarmStarted())
  {
    const auto& wstart = getRawWarmStartData();
    auto solver = pInstance->getMPSolver();
    if (!solver)
    {
      spdlog::error("MetaNetInitiator - init: empty pointer to solver");
      return 1;
    }

    std::vector<std::pair<std::shared_ptr<MPVariable>, double>> warmStart;
    const auto& varList = solver->getVariableList();
    for (auto vpair : wstart)
    {
      const auto varIdx = vpair.first;
      if (varIdx < 0 || varIdx >= static_cast<int>(varList.size()))
      {
        std::string errMsg = "MetaNetInitiator - init: invalid warm-start variable index " +
                std::to_string(varIdx);
        spdlog::error(errMsg);
        return 1;
      }
      warmStart.push_back({varList[varIdx], vpair.second});
    }

    // Set the warm start into the solver
    solver->setWarmStart(warmStart);
  }

  // Return no error
  return 0;
} // init

int MetaNetInitiator::reInit(int numRestartedRacing)
{
  (void)numRestartedRacing;

  // Regenerate the problem instance
  pInstance->reset();

  // Restart initialization
  init(pParamSet);

  // Return no error
  return 0;
}  // reInit

metabb::Instance::SPtr MetaNetInitiator::getInstance()
{
  return pInstance;
}  // getInstance

metabb::DiffSubproblem::SPtr MetaNetInitiator::buildRootNodeDiffSubproblem()
{
  return nullptr;
}  // buildRootNodeDiffSubproblem

bool MetaNetInitiator::tryToSetIncumbentSolution(metabb::Solution::SPtr sol, bool checksol)
{
  auto metaNetSol = std::dynamic_pointer_cast<MetaNetSolution>(sol);
  if (!metaNetSol)
  {
    throw std::runtime_error("MetaNetInitiator - tryToSetIncumbentSolution: "
            "invalid downcast to MetaNetSolution");
  }

  if (metaNetSol->getNumVars() == 0)
  {
    return false;
  }

  pSolution = std::dynamic_pointer_cast<MetaNetSolution>(metaNetSol->clone());
  return true;
}  // tryToSetIncumbentSolution

void MetaNetInitiator::sendSolverInitializationMessage()
{
  assert(pRootParamSet && pParamSet);

  // Prepare the message to send
  metabb::InitiatorInitSystemMessageProtoPacket initProtoMessage;
  initProtoMessage.set_warmstarted(isWarmStarted());
  if (pSolution)
  {
    initProtoMessage.set_incumbentvalue(pSolution->getObjectiveFuntionValue());
  }
  else
  {
    initProtoMessage.set_incumbentvalue(std::numeric_limits<double>::max());
  }
  initProtoMessage.set_solutionexists(false);

  // Send the initialization value which should be received
  // by the solver during construction phase
  optnet::Packet::UPtr packet(new optnet::Packet());
  packet->networkPacket = optnet::BaseNetworkPacket::UPtr(
          new optnet::NetworkPacket<metabb::InitiatorInitSystemMessageProtoPacket>(
                  initProtoMessage));
  pFramework->getComm()->broadcast(packet, kRootNodeRank);

  if ((pParamSet->getDescriptor().rampupphaseprocess() == kRampUpPhaseRacing ||
          pParamSet->getDescriptor().rampupphaseprocess() == kRampUpPhaseRebuildTreeAfterRacing) &&
          pParamSet->getDescriptor().communicatetighterboundsinracing())
  {
    assert(pInstance->getNumVars() > 0);
    pThightenedVarLBList.resize(pInstance->getNumVars(), -std::numeric_limits<double>::max());
    pThightenedVarUBList.resize(pInstance->getNumVars(),  std::numeric_limits<double>::max());
  }
}  // sendSolverInitializationMessage

void MetaNetInitiator::generateRacingRampUpParameterSets(
        std::vector<metabb::RacingRampUpParamSet::SPtr>& racingRampUpParamSet)
{
  if (racingRampUpParamSet.empty()) return;

  auto metaFramework = std::dynamic_pointer_cast<MetaNetFramework>(pFramework);
  if (!metaFramework)
  {
    std::string errMsg = "MetaNetInitiator - generateRacingRampUpParameterSets: "
            "invalid downcast to MetaNetRacingRampUpParamSet";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  // Number of generated parameters
  int numParams{0};

  // Number of variable permutation seed
  int numPermutationSeed{0};

  // Number of branching order seed
  int numBranchingOrderSeed{0};
  const int numTotParams = static_cast<int>(racingRampUpParamSet.size());
  while (true)
  {
    // Iterate over the max-seed value
    for (int idx = 0; idx < pParamSet->getDescriptor().maxnracingparamsetseed(); ++idx)
    {
      // Reset permutation seed if needed
      if (numPermutationSeed > (pParamSet->getDescriptor().trynumvariableorderinracing() - 1))
      {
        numPermutationSeed = 0;
      }

      // Reset branching order if needed
      if (numBranchingOrderSeed > (pParamSet->getDescriptor().trynumbranchingorderinracing()))
      {
        numBranchingOrderSeed = 0;
      }

      // Instantiate the racing ramp-up parameter set
      racingRampUpParamSet[numParams] = metaFramework->buildRacingRampUpParamSet(
              static_cast<metabb::RacingRampUpParamSet::RacingTerminationCriteria>(
                      pParamSet->getDescriptor().racingrampupterminationcriteria()),
              pParamSet->getDescriptor().stopracingnumnodesleft(),
              idx,
              numPermutationSeed,
              numBranchingOrderSeed,
              static_cast<uint64_t>(pParamSet->getDescriptor().stopracingtimelimitmsec()));

      // Update seeds
      numPermutationSeed++;
      numBranchingOrderSeed++;
      numParams++;

      // Exit loop
      if (numParams >= numTotParams) return;
    }
  }
  // buildRacingRampUpParamSet
}  // generateRacingRampUpParameterSets

double MetaNetInitiator::convertToExternalValue(double internalValue)
{
  return pInstance->convertToExternalValue(internalValue);
}  //convertToExternalValue

double MetaNetInitiator::convertToInternalValue(double externalValue)
{
  return pInstance->getMPSolver()->convertToInternalValue(externalValue);
}  // convertToInternalValue

metabb::Solution::SPtr MetaNetInitiator::getGlobalBestIncumbentSolution()
{
  return pSolution;
}  // getGlobalBestIncumbentSolution

double MetaNetInitiator::getAbsGap(double dualBoundValue)
{
  if (pSolution == nullptr)
  {
    return std::numeric_limits<double>::max();
  }

  auto primalBound = pInstance->convertToExternalValue(pSolution->getObjectiveFuntionValue());
  auto dualBound = pInstance->convertToExternalValue(dualBoundValue);
  return primalBound - dualBound;
}  // getAbsGap

double MetaNetInitiator::getGap(double dualBoundValue)
{
  if (pSolution == nullptr)
  {
    return std::numeric_limits<double>::max();
  }

  auto primalBound = pInstance->convertToExternalValue(pSolution->getObjectiveFuntionValue());
  auto dualBound = pInstance->convertToExternalValue(dualBoundValue);
  if (EPSEQ(primalBound, dualBound, metabb::metaconst::DEAFULT_NUM_EPSILON))
  {
    return 0.0;
  }
  else if ((std::abs(primalBound) <= metabb::metaconst::DEAFULT_NUM_EPSILON) ||
           (std::abs(dualBound) < metabb::metaconst::DEAFULT_NUM_EPSILON) ||
           std::abs(primalBound) == std::numeric_limits<double>::max() ||
           std::abs(dualBound) == std::numeric_limits<double>::max() ||
           primalBound * dualBound < 0.0)
  {
    return std::numeric_limits<double>::max() ;
  }
  else
  {
    return std::abs((primalBound - dualBound)/std::min(std::abs(primalBound), std::abs(dualBound)));
  }
}  // getGap

double MetaNetInitiator::getAbsGapValue()
{
  return pAbsGap;
}  // getAbsGapValue

double MetaNetInitiator::getGapValue()
{
  return pGap;
}  // getGapValue

double MetaNetInitiator::getEpsilon() const noexcept
{
  return metabb::metaconst::DEAFULT_NUM_EPSILON;
}  // getEpsilon

void MetaNetInitiator::setFinalSolverStatus(metabb::solver::FinalSolverState state) noexcept
{
  pFinalSolverState = state;
}  // setFinalSolverStatus

void MetaNetInitiator::setNumSolvedNodes(long long numNodes) noexcept
{
  pNumSolved = numNodes;
}  // setNumSolvedNodes

void MetaNetInitiator::setDualBound(double bound) noexcept
{
  pFinalDualBound = bound;
}  // setDualBound

bool MetaNetInitiator::isFeasibleSolution()
{
  throw std::runtime_error("MetaNetInitiator - isFeasibleSolution: method not supported");
}  // isFeasibleSolution

void MetaNetInitiator::accumulateInitialStat(metabb::InitialStat::SPtr initialStat)
{
  auto initStat = std::dynamic_pointer_cast<MetaNetInitialStat>(initialStat);
  if (!initStat)
  {
    throw std::runtime_error("MetaNetInitiator - accumulateInitialStat: "
            "invalid downcast to MetaNetInitialStat");
  }
  initStat->accumulateOn(pInstance->getMPSolver());
}  // accumulateInitialStat

void MetaNetInitiator::writeSolution(const std::string& message)
{
  (void)message;
  // No-op
}  // writeSolution

void MetaNetInitiator::setInitialStatOnDiffSubproblem(int, int, metabb::DiffSubproblem::SPtr)
{
  // No-op
}  // setInitialStatOnDiffSubproblem

}  // namespace metabb

template<> bool optnet::NetworkPacket<metabb::InitiatorInitSystemMessageProtoPacket>::operator==(
    const NetworkPacket<metabb::InitiatorInitSystemMessageProtoPacket>& other) const noexcept
{
  if (this->data.size() != other.data.empty()) return false;
  return google::protobuf::util::MessageDifferencer::Equals(this->data[0], other.data[0]);
}

template<>
std::pair<optnet::BaseNetworkPacket::BaseNetworkPacketType, std::string>
optnet::NetworkPacket<metabb::InitiatorInitSystemMessageProtoPacket>::serialize()
const noexcept
{
  if (data.size() != 1)
  {
    spdlog::error("NetworkPacket<InitiatorInitSystemMessageProtoPacket> - invalid data size");
    return {BaseNetworkPacketType::BNPT_UNSPECIFIED_PACKET, ""};
  }

  try
  {
    return {BaseNetworkPacketType::BNPT_META_BB_INITIATOR, data.at(0).SerializeAsString()};
  }
  catch(...)
  {
    spdlog::error("NetworkPacket<NodeProtoPacket> - undefined error when serializing protobuf");
    return {BaseNetworkPacketType::BNPT_UNSPECIFIED_PACKET, ""};
  }
}  // serialize

}  // namespace optilab
