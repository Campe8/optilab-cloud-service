//
// Copyright OptiLab 2020. All rights reserved.
//
// Distributed OR implementation based on metabb framework for the
// DiffSubproblem class.
// A DiffSubproblem is a collection of bound changes for the
// variables in a model.
// Instead of sending the whole (updated) model to each distributed node,
// the DiffSubproblem captures only the difference between the original
// instance and the current model to solve in terms of variables
// bound changes.
//

#pragma once

#include <limits>   // for std::numeric_limits
#include <memory>   // for std::shared_ptr
#include <string>
#include <utility>  // for std::pair
#include <vector>

#include "meta_bb/diff_subproblem.hpp"
#include "meta_bb/framework.hpp"
#include "meta_bb/meta_bb_struct.hpp"
#include "meta_bb/instance.hpp"
#include "optilab_protobuf/meta_bb_packet.pb.h"
#include "optimizer_network/network_packet.hpp"
#include "optimizer_network/packet.hpp"
#include "or_toolbox/linear_model.hpp"
#include "or_toolbox/linear_solver.hpp"
#include "system/system_export_defs.hpp"

// Forward declarations
namespace optilab {
namespace  metabb {
class Initiator;
}  // namespace metabb
}  // namespace optilab

namespace optilab {
namespace  toolbox {
class MetaNetSolver;
}  // namespace toolbox
}  // namespace optilab

namespace optilab {
namespace  toolbox {

class SYS_EXPORT_CLASS MetaNetDiffSubproblem : public metabb::DiffSubproblem {
 public:
  /// Type of variable bound
  enum class BoundType : int {
    BT_LOWER = 0,
    BT_UPPER
  };

  /**
   * Auxiliary data structure used to store information about linear constraints
   * that can be part of a DiffSubproblem.
   */
  struct BranchLinearConstraintInfo {
    std::string name;
    double lowerBound{std::numeric_limits<double>::max()};
    double upperBound{std::numeric_limits<double>::lowest()};
    std::vector<std::pair<int, double>> coeffList;
  };

  using SPtr = std::shared_ptr<MetaNetDiffSubproblem>;

 public:
  /// Default constructor
  MetaNetDiffSubproblem();

  /// Constructor based on a linear solver and model
  MetaNetDiffSubproblem(std::shared_ptr<MetaNetSolver> solver,
                        const std::vector<MPVariable::SPtr>& branchingVarList,
                        const std::vector<double>& branchingBoundList,
                        const std::vector<BoundType>& branchingBoundType,
                        const std::vector<MPConstraint::SPtr>& constraintList);

  /// Copy and assignment operator
  MetaNetDiffSubproblem(const MetaNetDiffSubproblem& other);
  MetaNetDiffSubproblem& operator=(const MetaNetDiffSubproblem& other);

  virtual ~MetaNetDiffSubproblem() = default;

  /// Returns the index of the variable in the original model given its
  /// ordinal (zero-based) number of a variable
  inline int getIndex(int idx) const { return pVarIndexList.at(idx); }

  /// Returns the branching bound value of this diff. subproblem given
  /// ordinal (zero-based) number of a variable
  inline double getBranchBound(int idx) const { return pBranchBoundList.at(idx); }

  /// Returns the branching bound type of this diff. subproblem given
  /// ordinal (zero-based) number of a variable
  inline BoundType getBranchBoundType(int idx) const { return pBranchBoundTypeList.at(idx); }

  /// Returns the number of bound changes
  int getNBoundChanges() const noexcept override;

  /// Returns true if this difference subproblem has branch linear constraints.
  /// Returns false otherwise
  inline bool hasBranchLinearConstraints() const noexcept
  {
    return getNBranchLinearConstraints() > 0;
  }

  /// Returns the number of branch linear constraint in this difference subproblem
  inline int getNBranchLinearConstraints() const noexcept
  {
    return static_cast<int>(pBranchLinearConsList.size());
  }

  /// Returns the branch linear constraint info at specified index, if any.
  /// @note throws std::out_of_bound exception on invalid index
  const BranchLinearConstraintInfo& getBranchLinearConstraint(int idx) const
  {
    return pBranchLinearConsList.at(idx);
  }

  /// Sets up the given list with the fixed variables and returns the number of variables
  /// in the list (i.e., its size)
  int getFixedVariables(
          const metabb::Instance::SPtr instance,
          std::vector<metabb::mergenodes::FixedVariable::SPtr>& fixedVarList) override;

  /// Builds and returns a new instance of DiffSubproblem from the given list of fixed variables
  metabb::DiffSubproblem::SPtr createDiffSubproblem(
          metabb::Framework::SPtr framework,
          std::shared_ptr<metabb::Initiator> initiator,
          const std::vector<metabb::mergenodes::FixedVariable*>& fixedVarList) override;

  /// Returns a clone of this object
  metabb::DiffSubproblem::SPtr clone(const metabb::Framework::SPtr& framework) override;

  /// Sends this diff. subproblem to given destination.
  /// Returns zero on success, non-zero otherwise
  int send(const metabb::Framework::SPtr& framework, int destination) noexcept override;

  /// Receives a DiffSubproblem from the given source.
  /// Returns zero on success, non-zero otherwise.
  /// @note this is a blocking call
  int receive(const metabb::Framework::SPtr& framework, int source) noexcept override;

  /// Broadcast this diff. subproblem to the network
  int broadcast(const metabb::Framework::SPtr& framework, int root) noexcept override;

  /// Pretty print this DiffSubproblem instance to string
  std::string toString() const noexcept;

 protected:
  using BranchingLinearConsList = std::vector<BranchLinearConstraintInfo>;

 protected:
  /// Number of branching variables (bound changes)
  int pNumBoundChanges{0};

  /// Variable index list (unique index)
  std::vector<int> pVarIndexList;

  /// Branching bounds list difference w.r.t. the original problem
  std::vector<double> pBranchBoundList;

  /// Branching bounds type list difference w.r.t. the original problem
  std::vector<BoundType> pBranchBoundTypeList;

  /// Linear constraints branching list
  BranchingLinearConsList pBranchLinearConsList;

 private:
  /// Add branching linear constraints in the diff. subproblem
  void addBranchingLinearConstraints(std::shared_ptr<MetaNetSolver> solver,
                                     const std::vector<MPConstraint::SPtr>& constraintList);

  /// Add node information in the diff. subproblem
  void addNodeInfo(std::shared_ptr<MetaNetSolver> solver);

  /// Builds and returns a network packet encoding this DiffSubproblem
  optnet::Packet::UPtr buildNetworkPacket();

  /// Upload the serialized proto packet into this DiffSubproblem
  int uploadPacket(const std::string& bytes);
};

}  // namespace metabb

/// Specialized template for network MetaNetDiffSubproblem packet comparison
template<> bool optnet::NetworkPacket<metabb::DiffSubproblemProtoPacket>::operator==(
    const NetworkPacket<metabb::DiffSubproblemProtoPacket>& other) const noexcept;

/// Specialized template to serialize DiffSubproblemProtoPacket(s)
template<>
std::pair<optnet::BaseNetworkPacket::BaseNetworkPacketType, std::string>
optnet::NetworkPacket<metabb::DiffSubproblemProtoPacket>::serialize() const noexcept;

}  // namespace optilab

