// Interface implementation of SCIP solver.
// SCIP is used as MIP back-end solver
#include "or_toolbox/linear_solver.hpp"

#include <stdexcept>  // for std::invalid_argument
#include <string>
#include <vector>

#include <scip/cons_indicator.h>
#include <scip/scip.h>
#include <scip/scip_prob.h>
#include <scip/scipdefplugins.h>

#include <spdlog/spdlog.h>

#include "meta_bb/paramset.hpp"
#include "meta_bb/meta_bb_utilities.hpp"
#include "utilities/timer.hpp"

namespace {

// Emphasize search towards feasibility.
// This may or may not result in speedups in some problems.
#define scip_feasibility_emphasis false

// the internal SCIP error code to the user.
inline optilab::metabb::utils::OpStatus scipCodeToUtilStatus(
    int retcode,
    const char* sourceFile,
    int sourceLine,
    const char* scipStatement)
  {
  if (retcode == /*SCIP_OKAY*/ 1) return optilab::metabb::utils::OpStatus();

  const std::string errMsg = "SCIP error code " + std::to_string(retcode) +
      " (file " + std::string(sourceFile) + ", line " + std::to_string(sourceLine) +
      " on " + std::string(scipStatement);
  spdlog::error(errMsg);
  return optilab::metabb::utils::OpStatus(errMsg);
}  // scipCodeToUtilStatus

#define SCIP_TO_STATUS(x) \
  scipCodeToUtilStatus(x, __FILE__, __LINE__, #x)

#define RETURN_IF_ERROR(expr)                                                \
  do {                                                                       \
    /* Using pStatus below to avoid capture problems if expr is "status". */ \
    const optilab::metabb::utils::OpStatus pStatus = (expr);                 \
    if (!pStatus.isOk()) return pStatus;                                     \
  } while (0)

#define RETURN_IF_SCIP_ERROR(x) RETURN_IF_ERROR(SCIP_TO_STATUS(x));

#define RETURN_IF_ALREADY_IN_ERROR_STATE                                 \
  do {                                                                   \
    if (!pStatus.isOk()) {                                               \
      spdlog::warn("Early abort: SCIP is in error state.");              \
      return;                                                            \
    }                                                                    \
  } while (false)

#define RETURN_AND_STORE_IF_SCIP_ERROR(x) \
  do {                                    \
    pStatus = SCIP_TO_STATUS(x);          \
    if (!pStatus.isOk()) return;          \
  } while (false)

}  // namespace

namespace optilab {
namespace toolbox {

class SCIPInterface : public MPSolverInterface {
 public:
  explicit SCIPInterface(MPSolver* const solver);

  ~SCIPInterface() override;

  /// Solves problem with specified parameter values
  lmcommon::ResultStatus solve(const metabb::ParamSet& param) override;

  /// Resets extracted models (from MPSolver to back-end solver)
  void reset() override;

  /// Some interfaces map values into their internal space representation.
  /// This method converts a value from internal to external space.
  /// @default return the same value
  double convertToExternalValue(double internalValue) override;

  /// Some interfaces map values into their internal space representation.
  /// This method converts a value from external to internal space.
  /// @default return the same value
  double convertToInternalValue(double externalValue) override;

  /// Sets the optimization direction (min/max):
  /// - maximize: true; or
  /// - minimize: false
  void setOptimizationDirection(bool maximize) override;

  /// Modifies bounds of an extracted variable.
  /// @note "index" is the index of the variable in the list of variables
  /// in the order created by the caller
  void setVariableBounds(int index, double lb, double ub) override;

  /// Modifies integrality of an extracted variable
  /// @note "index" is the index of the variable in the list of variables
  /// in the order created by the caller
  void setVariableInteger(int index, bool integer) override;

  /// Modify bounds of an extracted variable
  /// @note "index" is the index of the variable in the list of variables
  /// in the order created by the caller
  void setConstraintBounds(int index, double lb, double ub) override;

  /// Adds a linear constraint
  void addRowConstraint(MPConstraint* const ct) override;

  /// Adds a variable
  void addVariable(MPVariable* const var) override;

  /// Changes a coefficient in a constraint
  void setCoefficient(MPConstraint* const constraint, const MPVariable* const variable,
                      double newValue, double oldValue) override;

  /// Clears a constraint from all its terms
  void clearConstraint(MPConstraint* const constraint) override;

  /// Changes a coefficient in the linear objective.
  void setObjectiveCoefficient(const MPVariable* const variable, double coefficient) override;

  /// Changes the constant term in the linear objective
  void setObjectiveOffset(double value) override;

  /// Clears the objective from all its terms.
  void clearObjective() override;

  /// Signals the change in the priority of a variable.
  /// @note "varIndex" is the index of the variable in the list of variables
  /// in the order created by the caller
  void branchingPriorityChangedForVariable(int varIndex) override;

  /// Returns the number of simplex iterations.
  /// @note The problem must be discrete
  int64_t numSimplexIterations() const override;

  /// Returns the number of branch-and-bound nodes.
  /// @note The problem must be discrete
  int64_t numNodes() const override;

  /// Returns the number of nodes left in the tree:
  /// children + siblings + leaves
  int64_t numNodesLeft() const override;

  /// Returns the best objective bound.
  /// @note The problem must be discrete
  double bestObjectiveBound() const override;

  /// Returns true if the problem is continuous,
  /// returns false otherwise
  bool isContinuous() const override
  {
    return false;
  }

  /// Returns true if the problem is continuous and linear,
  /// returns false otherwise
  bool isLP() const override
  {
    return false;
  }

  /// Returns true if the problem is discrete and linear,
  /// returns false otherwise
  bool isMIP() const override
  {
    return true;
  }

  /// Returns the basis status of a row
  lmcommon::BasisStatus rowStatus(int constraintIndex) const override
  {
    (void)constraintIndex;
    spdlog::error("rowStatus - unavailable in SCIP");
    return lmcommon::BasisStatus::FREE;
  }

  /// Returns the basis status of a column
  lmcommon::BasisStatus columnStatus(int variableIndex) const override
  {
    (void)variableIndex;
    spdlog::error("rowStatus - unavailable in SCIP");
    return lmcommon::BasisStatus::FREE;
  }

  /// Returns the pointer to the back-end underlying solver
  void* backendSolverPtr() override
  {
    return reinterpret_cast<void*>(pSCIPSolver);
  }

  void setStartingLPBasis(const std::vector<lmcommon::BasisStatus>& variableStatuses,
                          const std::vector<lmcommon::BasisStatus>& constraintStatuses) override
  {
    (void)variableStatuses;
    (void)constraintStatuses;
    spdlog::error("Solver doesn't support setting an LP basis");
  }

  /// Interrupt the solving process of the back-end solver
  bool interruptSolve() override
  {
    if (!pSCIPSolver) return true;
    return SCIPinterruptSolve(pSCIPSolver) == SCIP_Retcode::SCIP_OKAY;
  }

  /// Computes the next MIP solution, if any
  bool nextSolution() override { return false; }

 protected:
  /// Returns the time in msec. taken by the solver to load the model
  uint64_t getModelLoadTimeMsec() const override { return pLoadModelTimeMsec; }

  /// Returns the solve time in msec
  uint64_t getSolveTimeMsec() const override { return pSolveTimeMsec; }

  /// Extracts the variables that have not been extracted yet
  void extractNewVariables() override;

  /// Extracts the constraints that have not been extracted yet
  void extractNewConstraints() override;

  /// Extracts the objective
  void extractObjective() override;

  /// Sets all parameters in the underlying solver
  void setParameters(const metabb::ParamSet& param) override;

  /// Sets each parameter in the underlying solver
  void setRelativeMipGap(double value) override;

  void setPrimalTolerance(double value) override;

  void setDualTolerance(double value) override;

  void setPresolveMode(int value) override;

  /// Sets the number of threads to be used by the solver.
  /// Returns true if the back-end solver allows the caller to set
  /// the number of threads.
  /// Returns false otherwise
  bool setNumThreads(int numThreads) override;

  /// Sets the scaling mode
  void setScalingMode(int value) override;

  /// Sets the LP algorithm to be used by the back-end solver
  void setLPAlgorithm(int value) override;

 private:
  /// Pointer to the SCIP solver
  SCIP* pSCIPSolver;

  /// Reset flag for branching priority
  bool pBranchingPriorityReset;

  /// Time taken to load the model by the SCIP solver in msec.
  uint64_t pLoadModelTimeMsec;

  /// Time taken to solve the model by the SCIP solver in msec.
  uint64_t pSolveTimeMsec;

  /// List of pointers to the SCIP variables
  std::vector<SCIP_VAR*> pSCIPVariableList;

  /// List of pointers to the SCIP constraints
  std::vector<SCIP_CONS*> pSCIPConstraintList;

  /// Current status of the operations invoked on
  /// this interface
  metabb::utils::OpStatus pStatus;

  /// Creates a new instance of a SCIP solver
  metabb::utils::OpStatus createSCIP();

  /// Deletes the current instance of the SCIP  solver
  void deleteSCIP();
};

SCIPInterface::SCIPInterface(MPSolver* const solver)
: MPSolverInterface(solver),
  pSCIPSolver(nullptr),
  pBranchingPriorityReset(false),
  pLoadModelTimeMsec(0),
  pSolveTimeMsec(0)
{
  // Create a new SCIP solver instance
  pStatus = createSCIP();
}

SCIPInterface::~SCIPInterface()
{
  deleteSCIP();
}

void SCIPInterface::reset()
{
  deleteSCIP();
  pStatus = createSCIP();
  resetExtractionInformation();
}  // reset

metabb::utils::OpStatus SCIPInterface::createSCIP()
{
  RETURN_IF_SCIP_ERROR(SCIPcreate(&pSCIPSolver));
  RETURN_IF_SCIP_ERROR(SCIPincludeDefaultPlugins(pSCIPSolver));

  // Set the emphasis to enum SCIP_PARAMEMPHASIS_FEASIBILITY.
  // This emphasizes search towards feasibility.
  // @note It does not print the new parameter (quiet = true).
  if (scip_feasibility_emphasis)
  {
    RETURN_IF_SCIP_ERROR(SCIPsetEmphasis(pSCIPSolver, SCIP_PARAMEMPHASIS_FEASIBILITY, true));
  }

  // Default clock type.
  // @note  Wall clock time is used because getting CPU user seconds
  // involves calling times() which is very expensive.
  RETURN_IF_SCIP_ERROR(SCIPsetIntParam(pSCIPSolver, "timing/clocktype", SCIP_CLOCKTYPE_WALL));

  // Create a new empty SCIP problem with given name
  RETURN_IF_SCIP_ERROR(SCIPcreateProb(pSCIPSolver, pSolver->pSolverName.c_str(), nullptr,
                                      nullptr, nullptr, nullptr, nullptr,
                                      nullptr, nullptr));

  // Set objective direction
  RETURN_IF_SCIP_ERROR(SCIPsetObjsense(
      pSCIPSolver, pMaximize ?
          SCIP_OBJSENSE_MAXIMIZE : SCIP_OBJSENSE_MINIMIZE));
  return metabb::utils::OpStatus();
}  // createSCIP

void SCIPInterface::deleteSCIP()
{
  if (!pSCIPSolver) return;

  // Free variables
  for (int idx = 0; idx < static_cast<int>(pSCIPVariableList.size()); ++idx)
  {
    auto res = SCIPreleaseVar(pSCIPSolver, &pSCIPVariableList[idx]);
    assert(res == SCIP_OKAY);
  }
  pSCIPVariableList.clear();

  // Free constraints
  for (int idx = 0; idx < static_cast<int>(pSCIPConstraintList.size()); ++idx)
  {
    auto res = SCIPreleaseCons(pSCIPSolver, &pSCIPConstraintList[idx]);
    assert(res == SCIP_OKAY);
  }
  pSCIPConstraintList.clear();

  // Free the solver and set pointer to NULL to avoid re-deletes
  SCIPfree(&pSCIPSolver);
  pSCIPSolver = nullptr;
}  // deleteSCIP

double SCIPInterface::convertToExternalValue(double internalValue)
{
  if (!pSCIPSolver) return internalValue;

  const auto stage = SCIPgetStage(pSCIPSolver);
  switch (stage)
  {
    case SCIP_Stage::SCIP_STAGE_TRANSFORMING:
    case SCIP_Stage::SCIP_STAGE_TRANSFORMED:
    case SCIP_Stage::SCIP_STAGE_INITPRESOLVE:
    case SCIP_Stage::SCIP_STAGE_PRESOLVING:
    case SCIP_Stage::SCIP_STAGE_EXITPRESOLVE:
    case SCIP_Stage::SCIP_STAGE_PRESOLVED:
    case SCIP_Stage::SCIP_STAGE_INITSOLVE:
    case SCIP_Stage::SCIP_STAGE_SOLVING:
    case SCIP_Stage::SCIP_STAGE_SOLVED:
      return SCIPretransformObj(pSCIPSolver, internalValue);
    default:
      return internalValue;
  }
}  // convertToExternalValue

double SCIPInterface::convertToInternalValue(double externalValue)
{
  if (!pSCIPSolver) return externalValue;

  const auto stage = SCIPgetStage(pSCIPSolver);
  switch (stage)
  {
    case SCIP_Stage::SCIP_STAGE_TRANSFORMING:
    case SCIP_Stage::SCIP_STAGE_TRANSFORMED:
    case SCIP_Stage::SCIP_STAGE_INITPRESOLVE:
    case SCIP_Stage::SCIP_STAGE_PRESOLVING:
    case SCIP_Stage::SCIP_STAGE_EXITPRESOLVE:
    case SCIP_Stage::SCIP_STAGE_PRESOLVED:
    case SCIP_Stage::SCIP_STAGE_INITSOLVE:
    case SCIP_Stage::SCIP_STAGE_SOLVING:
    case SCIP_Stage::SCIP_STAGE_SOLVED:
      return SCIPtransformObj(pSCIPSolver, externalValue);
    default:
      return externalValue;
  }
}  // convertToInternalValue

void SCIPInterface::setOptimizationDirection(bool maximize)
{
  RETURN_IF_ALREADY_IN_ERROR_STATE;
  invalidateSolutionSynchronization();

  // Free all solution process data
  RETURN_AND_STORE_IF_SCIP_ERROR(SCIPfreeTransform(pSCIPSolver));

  // Set optimization direction
  RETURN_AND_STORE_IF_SCIP_ERROR(SCIPsetObjsense(
      pSCIPSolver, maximize ? SCIP_OBJSENSE_MAXIMIZE : SCIP_OBJSENSE_MINIMIZE));
}  // setOptimizationDirection

void SCIPInterface::setVariableBounds(int index, double lb, double ub)
{
  RETURN_IF_ALREADY_IN_ERROR_STATE;
  invalidateSolutionSynchronization();

  // Set variable bounds only if the variable has been already extracted
  if (variableIsExtracted(index))
  {
    // Check index consistency
    assert(index < pLastVariableIndex);
    RETURN_AND_STORE_IF_SCIP_ERROR(SCIPfreeTransform(pSCIPSolver));

    // Change lower bound of the indexed variable
    RETURN_AND_STORE_IF_SCIP_ERROR(
        SCIPchgVarLb(pSCIPSolver, pSCIPVariableList[index], lb));

    // Change upper bound of the indexed variable
    RETURN_AND_STORE_IF_SCIP_ERROR(
        SCIPchgVarUb(pSCIPSolver, pSCIPVariableList[index], ub));
  }
  else
  {
    // The solver needs to reload and extract the model
    pSynchStatus = SynchronizationStatus::MUST_RELOAD;
  }
}  // setVariableBounds

void SCIPInterface::setVariableInteger(int index, bool integer)
{
  RETURN_IF_ALREADY_IN_ERROR_STATE;
  invalidateSolutionSynchronization();


  // Set variable integer only if the variable has been already extracted
  if (variableIsExtracted(index))
  {
    // Check index consistency
    assert(index < pLastVariableIndex);
    RETURN_AND_STORE_IF_SCIP_ERROR(SCIPfreeTransform(pSCIPSolver));

#if (SCIP_VERSION >= 210)
    SCIP_Bool infeasible = false;

    // Change the type of the given variable
    RETURN_AND_STORE_IF_SCIP_ERROR(SCIPchgVarType(
        pSCIPSolver, pSCIPVariableList[index],
        integer ? SCIP_VARTYPE_INTEGER : SCIP_VARTYPE_CONTINUOUS, &infeasible));
#else
    RETURN_AND_STORE_IF_SCIP_ERROR(SCIPchgVarType(
        pSCIPSolver, pSCIPVariableList[index],
        integer ? SCIP_VARTYPE_INTEGER : SCIP_VARTYPE_CONTINUOUS));
#endif  // SCIP_VERSION >= 210
  }
  else
  {
    // The solver needs to reload and extract the model
    pSynchStatus = SynchronizationStatus::MUST_RELOAD;
  }
}  // setVariableInteger

void SCIPInterface::setConstraintBounds(int index, double lb, double ub)
{
  RETURN_IF_ALREADY_IN_ERROR_STATE;
  invalidateSolutionSynchronization();

  // Set constraint bounds only if the constraint has been already extracted
  if (constraintIsExtracted(index))
  {
    // Check index consistency
    assert(index < pLastConstraintIndex);
    RETURN_AND_STORE_IF_SCIP_ERROR(SCIPfreeTransform(pSCIPSolver));

    // Change left hand side of the given linear constraint
    RETURN_AND_STORE_IF_SCIP_ERROR(
        SCIPchgLhsLinear(pSCIPSolver, pSCIPConstraintList[index], lb));

    // Change right hand side of the given linear constraint
    RETURN_AND_STORE_IF_SCIP_ERROR(
        SCIPchgRhsLinear(pSCIPSolver, pSCIPConstraintList[index], ub));
  }
  else
  {
    // The solver needs to reload and extract the model
    pSynchStatus = SynchronizationStatus::MUST_RELOAD;
  }
}  // setConstraintBounds


void SCIPInterface::addRowConstraint(MPConstraint* const ct)
{
  pSynchStatus = SynchronizationStatus::MUST_RELOAD;
}  // addRowConstraint

void SCIPInterface::addVariable(MPVariable* const var)
{
  pSynchStatus = SynchronizationStatus::MUST_RELOAD;
}  // addVariable

void SCIPInterface::setCoefficient(MPConstraint* const constraint, const MPVariable* const variable,
                                   double newValue, double oldValue)
{
  RETURN_IF_ALREADY_IN_ERROR_STATE;
  invalidateSolutionSynchronization();

  // Set coefficient only if both the variable and the constraint
  // havebeen already extracted
  if (variableIsExtracted(variable->getIndex()) &&
      constraintIsExtracted(constraint->getIndex()))
  {
    // Check index consistency
    assert(variable->getIndex() < pLastVariableIndex);
    assert(constraint->getIndex() < pLastConstraintIndex);
    RETURN_AND_STORE_IF_SCIP_ERROR(SCIPfreeTransform(pSCIPSolver));

    // SCIP does not allow to set a coefficient directly.
    // Therefore, add the difference between the new and the old value
    RETURN_AND_STORE_IF_SCIP_ERROR(SCIPfreeTransform(pSCIPSolver));

    // @note SCIPaddCoefLinear adds a coefficient, it does not reset it
    RETURN_AND_STORE_IF_SCIP_ERROR(SCIPaddCoefLinear(
        pSCIPSolver, pSCIPConstraintList[constraint->getIndex()],
        pSCIPVariableList[variable->getIndex()], newValue - oldValue));
  }
  else
  {
    // The solver needs to reload and extract the model
    pSynchStatus = SynchronizationStatus::MUST_RELOAD;
  }
}  // setCoefficient

void SCIPInterface::clearConstraint(MPConstraint* const constraint)
{
  RETURN_IF_ALREADY_IN_ERROR_STATE;
  invalidateSolutionSynchronization();

  const int constraintIndex = constraint->getIndex();

  // Constraint may not have been extracted yet
  if (!constraintIsExtracted(constraintIndex)) return;
  for (const auto& entry : constraint->pCoefficients)
  {
    // Get the variable in the constraint's scope and its coefficient value
    const int varIndex = entry.first->getIndex();
    const double oldCoeffValue = entry.second;
    assert(variableIsExtracted(varIndex));

    // Set coefficient to zero by subtracting the old coefficient value
    RETURN_AND_STORE_IF_SCIP_ERROR(SCIPfreeTransform(pSCIPSolver));
    RETURN_AND_STORE_IF_SCIP_ERROR(
        SCIPaddCoefLinear(pSCIPSolver, pSCIPConstraintList[constraintIndex],
                          pSCIPVariableList[varIndex], -oldCoeffValue));
  }
}  // clearConstraint

void SCIPInterface::setObjectiveCoefficient(const MPVariable* const variable, double coefficient)
{
  pSynchStatus = SynchronizationStatus::MUST_RELOAD;
}  // setObjectiveCoefficient

void SCIPInterface::setObjectiveOffset(double value)
{
  pSynchStatus = SynchronizationStatus::MUST_RELOAD;
}  // setObjectiveOffset

void SCIPInterface::clearObjective()
{
  RETURN_IF_ALREADY_IN_ERROR_STATE;
  pSynchStatus = SynchronizationStatus::MUST_RELOAD;
  invalidateSolutionSynchronization();

  RETURN_AND_STORE_IF_SCIP_ERROR(SCIPfreeTransform(pSCIPSolver));

  // Clear linear terms
  for (const auto& entry : pSolver->pObjective->pCoefficients)
  {
    const int varIndex = entry.first->getIndex();

    // Variable may have not been extracted yet.
    if (!variableIsExtracted(varIndex))
    {
      assert(pSynchStatus != SynchronizationStatus::MODEL_SYNCHRONIZED);
    }
    else
    {
      RETURN_AND_STORE_IF_SCIP_ERROR(
          SCIPchgVarObj(pSCIPSolver, pSCIPVariableList[varIndex], 0.0));
    }
  }
}  // clearObjective

void SCIPInterface::branchingPriorityChangedForVariable(int varIndex)
{
  if (variableIsExtracted(varIndex))
  {
    pBranchingPriorityReset = true;
  }
}  // branchingPriorityChangedForVariable

int64_t SCIPInterface::numSimplexIterations() const
{
  // If solution is not yet synchronized, return undef. number
  if (!checkSolutionIsSynchronized()) return -1;
  return SCIPgetNLPIterations(pSCIPSolver);
}  // numSimplexIterations

int64_t SCIPInterface::numNodes() const
{
  // If solution is not yet synchronized, return undef. number
  if (!checkSolutionIsSynchronized()) return -1;

  // This is the total number of nodes used in the solve, potentially across
  // multiple branch-and-bound trees. Use limits/totalnodes (rather than
  // limits/nodes) to control this value.
  return SCIPgetNTotalNodes(pSCIPSolver);
}  // numNodes

int64_t SCIPInterface::numNodesLeft() const
{
  // If solution is not yet synchronized, return undef. number
  if (!checkSolutionIsSynchronized()) return -1;

  return SCIPgetNNodesLeft(pSCIPSolver);
}  // numNodesLeft

double SCIPInterface::bestObjectiveBound() const
{
  // If solution is not yet synchronized, return undef. bound
  if (!checkSolutionIsSynchronized() || !checkBestObjectiveBoundExists())
  {
    return trivialWorstObjectiveBound();
  }

  if (pSolver->pVariableList.empty() && pSolver->pConstraintList.empty())
  {
    // Special case for empty model.
    return pSolver->getObjective().offset();
  }
  else
  {
    /// Returns the global dual bound
    return SCIPgetDualbound(pSCIPSolver);
  }
}  // bestObjectiveBound

void SCIPInterface::extractNewVariables()
{
  RETURN_IF_ALREADY_IN_ERROR_STATE;

  int totalNumVars = pSolver->pVariableList.size();
  if (totalNumVars > pLastVariableIndex)
  {
    RETURN_AND_STORE_IF_SCIP_ERROR(SCIPfreeTransform(pSCIPSolver));

    // Define new variables
    for (int idx = pLastVariableIndex; idx < totalNumVars; ++idx)
    {
      MPVariable* const var = pSolver->pVariableList[idx].get();
      assert(!variableIsExtracted(idx));
      setVariableAsExtracted(idx, true);

      // Create a correspondent SCIP variable
      SCIP_VAR* scipVar = nullptr;

      // The true objective coefficient will be set later in extractObjective
      double tmpObjCoeff = 0.0;

      // Instantiate a new SCIP variable
      RETURN_AND_STORE_IF_SCIP_ERROR(SCIPcreateVar(
          pSCIPSolver, &scipVar, var->getName().c_str(), var->lowerBound(), var->upperBound(),
          tmpObjCoeff,
          var->isInteger() ? SCIP_VARTYPE_INTEGER : SCIP_VARTYPE_CONTINUOUS, true,
          false, nullptr, nullptr, nullptr, nullptr, nullptr));

      // Add the variable into the solver
      RETURN_AND_STORE_IF_SCIP_ERROR(SCIPaddVar(pSCIPSolver, scipVar));
      pSCIPVariableList.push_back(scipVar);

      // Set branching priority, if any
      const int branchingPriority = var->branchingPriority();
      if (branchingPriority != 0)
      {
        const int index = var->getIndex();
        RETURN_AND_STORE_IF_SCIP_ERROR(SCIPchgVarBranchPriority(
            pSCIPSolver, pSCIPVariableList[index], branchingPriority));
      }
    }

    // Add new variables to existing constraints
    for (int cIdx = 0; cIdx < pLastConstraintIndex; ++cIdx)
    {
      MPConstraint* const ct = pSolver->pConstraintList[cIdx].get();
      for (const auto& entry : ct->pCoefficients)
      {
        const int varIndex = entry.first->getIndex();
        assert(variableIsExtracted(varIndex));

        if (varIndex >= pLastVariableIndex)
        {
          // The variable is new, and the previous coefficient value was zero.
          // Therefore it is possible to directly add the coefficient
          RETURN_AND_STORE_IF_SCIP_ERROR(
              SCIPaddCoefLinear(pSCIPSolver, pSCIPConstraintList[cIdx],
                                pSCIPVariableList[varIndex], entry.second));
        }
      }
    }
  }
}  //extractNewVariables

void SCIPInterface::extractNewConstraints()
{
  RETURN_IF_ALREADY_IN_ERROR_STATE;

  int totalNumRows = pSolver->pConstraintList.size();
  if (pLastConstraintIndex < totalNumRows)
  {
    RETURN_AND_STORE_IF_SCIP_ERROR(SCIPfreeTransform(pSCIPSolver));

    // Find the length of the longest row
    int maxRowLength = 0;
    for (int idx = pLastConstraintIndex; idx < totalNumRows; ++idx)
    {
      MPConstraint* const ct = pSolver->pConstraintList[idx].get();
      assert(!constraintIsExtracted(idx));

      setConstraintAsExtracted(idx, true);
      if (static_cast<int>(ct->pCoefficients.size()) > maxRowLength) {
        maxRowLength = static_cast<int>(ct->pCoefficients.size());
      }
    }

    // Create arrays of variables and coefficients
    std::unique_ptr<SCIP_VAR*[]> vars(new SCIP_VAR*[maxRowLength]);
    std::unique_ptr<double[]> coeffs(new double[maxRowLength]);

    // Add each new constraint
    for (int idx = pLastConstraintIndex; idx < totalNumRows; ++idx)
    {
      MPConstraint* const ct = pSolver->pConstraintList[idx].get();
      assert(constraintIsExtracted(idx));

      // Fill the arrays of variables and coefficients for the
      // current constraint
      const int size = static_cast<int>(ct->pCoefficients.size());
      int ctr = 0;
      for (const auto& entry : ct->pCoefficients)
      {
        const int varIndex = entry.first->getIndex();
        assert(variableIsExtracted(varIndex));
        vars[ctr] = pSCIPVariableList[varIndex];
        coeffs[ctr] = entry.second;
        ctr++;
      }

      // Check for lazyness and indicator variables.
      // @note with an indicator variable, this constraint becomes active
      // only if the value of the variable is equal to the indicator value
      SCIP_CONS* scipConstraint = nullptr;
      const bool isLazy = ct->isLazy();
      if (ct->getIndicatorVariable() != nullptr)
      {
        // There is an indicator variable
        const int indIndex = ct->getIndicatorVariable()->getIndex();
        assert(variableIsExtracted(indIndex));

        SCIP_VAR* indVar = pSCIPVariableList[indIndex];
        if (ct->getIndicatorValue() == 0)
        {
          // Negate the variable x' = lb + ub - x
          RETURN_AND_STORE_IF_SCIP_ERROR(
              SCIPgetNegatedVar(pSCIPSolver, pSCIPVariableList[indIndex], &indVar));
        }

        // Instantiate an indicator constraint with a given upper bound
        if (ct->upperBound() < std::numeric_limits<double>::infinity())
        {
          RETURN_AND_STORE_IF_SCIP_ERROR(SCIPcreateConsIndicator(
              pSCIPSolver, &scipConstraint, ct->getName().c_str(), indVar, size,
              vars.get(), coeffs.get(), ct->upperBound(),
              /*initial=*/!isLazy,
              /*separate=*/true,
              /*enforce=*/true,
              /*check=*/true,
              /*propagate=*/true,
              /*local=*/false,
              /*dynamic=*/false,
              /*removable=*/isLazy,
              /*stickingatnode=*/false));
          RETURN_AND_STORE_IF_SCIP_ERROR(SCIPaddCons(pSCIPSolver, scipConstraint));
          pSCIPConstraintList.push_back(scipConstraint);
        }

        // Check if the constraint has a lower bound.
        // If so, create a new indicator constraint with the specified lower-bound (and same name
        // as the previous constraint constrained on the the upper bound)
        if (ct->lowerBound() > -std::numeric_limits<double>::infinity())
        {
          for (int coefIdx = 0; coefIdx < size; ++coefIdx)
          {
            coeffs[coefIdx] *= -1;
          }

          RETURN_AND_STORE_IF_SCIP_ERROR(SCIPcreateConsIndicator(
              pSCIPSolver, &scipConstraint, ct->getName().c_str(), indVar, size,
              vars.get(), coeffs.get(), -ct->lowerBound(),
              /*initial=*/!isLazy,
              /*separate=*/true,
              /*enforce=*/true,
              /*check=*/true,
              /*propagate=*/true,
              /*local=*/false,
              /*dynamic=*/false,
              /*removable=*/isLazy,
              /*stickingatnode=*/false));
          RETURN_AND_STORE_IF_SCIP_ERROR(SCIPaddCons(pSCIPSolver, scipConstraint));
          pSCIPConstraintList.push_back(scipConstraint);
        }
      }
      else
      {
        // No indicator variable.
        // Instantiate a new SCIP constraint.
        // See also
        // http://scip.zib.de/doc/html/cons__linear_8h.php#aa7aed137a4130b35b168812414413481
        // for an explanation of the parameters
        RETURN_AND_STORE_IF_SCIP_ERROR(SCIPcreateConsLinear(
            pSCIPSolver, &scipConstraint, ct->getName().c_str(), size, vars.get(),
            coeffs.get(), ct->lowerBound(), ct->upperBound(),
            /*initial=*/!isLazy,
            /*separate=*/true,
            /*enforce=*/true,
            /*check=*/true,
            /*propagate=*/true,
            /*local=*/false,
            /*modifiable=*/false,
            /*dynamic=*/false,
            /*removable=*/isLazy,
            /*stickingatnode=*/false));
        RETURN_AND_STORE_IF_SCIP_ERROR(SCIPaddCons(pSCIPSolver, scipConstraint));
        pSCIPConstraintList.push_back(scipConstraint);
      }
    }
  }
}  // extractNewConstraints

void SCIPInterface::extractObjective()
{
  RETURN_IF_ALREADY_IN_ERROR_STATE;
  RETURN_AND_STORE_IF_SCIP_ERROR(SCIPfreeTransform(pSCIPSolver));

  // Linear objective: set objective coefficients for all variables (some might
  // have been modified)
  for (const auto& entry : pSolver->pObjective->pCoefficients)
  {
    const int varIndex = entry.first->getIndex();
    const double objCoef = entry.second;
    RETURN_AND_STORE_IF_SCIP_ERROR(
        SCIPchgVarObj(pSCIPSolver, pSCIPVariableList[varIndex], objCoef));
  }

  // Constant term: change objective offset
  RETURN_AND_STORE_IF_SCIP_ERROR(SCIPaddOrigObjoffset(
      pSCIPSolver, pSolver->getObjective().getOffset() - SCIPgetOrigObjoffset(pSCIPSolver)));
}  // extractObjective

void SCIPInterface::setParameters(const metabb::ParamSet& param)
{
  setCommonParameters(param);
  setMIPParameters(param);
}  // setParameters

void SCIPInterface::setRelativeMipGap(double value)
{
  // NOTE(user): We don't want to call RETURN_IF_ALREADY_IN_ERROR_STATE here,
  // because even if the solver is in an error state, the user might be setting
  // some parameters and then "restoring" the solver to a non-error state by
  // calling Reset(), which should *not* reset the parameters.
  // So we want the parameter-setting functions to be resistant to being in an
  // error state, essentially. What we do is:
  // - we call the parameter-setting function anyway (I'm assuming that SCIP
  //   won't crash even if we're in an error state. I did *not* verify this).
  // - if that call yielded an error *and* we weren't already in an error state,
  //   set the state to that error we just got.
  const auto status =
      SCIP_TO_STATUS(SCIPsetRealParam(pSCIPSolver, "limits/gap", value));
  if (status.isOk()) pStatus = status;
}  // setRelativeMipGap

void SCIPInterface::setPrimalTolerance(double value)
{
  // SCIP automatically updates numerics/lpfeastol if the primal tolerance is
  // tighter. Doing that it unconditionally logs this modification to stderr. By
  // setting numerics/lpfeastol first we avoid this unwanted log
  double currentLPfeasTol = 0.0;
  assert(SCIP_Retcode::SCIP_OKAY ==
         SCIPgetRealParam(pSCIPSolver, "numerics/lpfeastol", &currentLPfeasTol));
  if (value < currentLPfeasTol)
  {
    const auto status =
        SCIP_TO_STATUS(SCIPsetRealParam(pSCIPSolver, "numerics/lpfeastol", value));
    if (status.isOk()) pStatus = status;
  }

  const auto status =
      SCIP_TO_STATUS(SCIPsetRealParam(pSCIPSolver, "numerics/feastol", value));
  if (status.isOk()) pStatus = status;
}  // setPrimalTolerance

void SCIPInterface::setDualTolerance(double value)
{
  const auto status =
      SCIP_TO_STATUS(SCIPsetRealParam(pSCIPSolver, "numerics/dualfeastol", value));
  if (status.isOk()) pStatus = status;
}  // setDualTolerance

void SCIPInterface::setPresolveMode(int value)
{
  constexpr int presolveOff = 0; //paramset::kPresolveOff;
  constexpr int presolveOn =  1; //paramset::kPresolveOn;
  switch (value) {
    case presolveOff:
    {
      const auto status =
          SCIP_TO_STATUS(SCIPsetIntParam(pSCIPSolver, "presolving/maxrounds", 0));
      if (status.isOk()) pStatus = status;
      return;
    }
    case presolveOn:
    {
      const auto status =
          SCIP_TO_STATUS(SCIPsetIntParam(pSCIPSolver, "presolving/maxrounds", -1));
      if (status.isOk()) pStatus = status;
      return;
    }
    default:
    {
      throw std::runtime_error("SCIPInterface - setPresolveMode: "
          "unrecognized mode " + std::to_string(value));
    }
  }
}  // setPresolveMode

bool SCIPInterface::setNumThreads(int numThreads)
{
  const auto status =
      SCIP_TO_STATUS(SCIPsetIntParam(pSCIPSolver, "parallel/maxnthreads", numThreads));
  if (status.isOk())
  {
    pStatus = status;
    return true;
  }
  return false;
}  // setNumThreads

void SCIPInterface::setScalingMode(int value)
{
  (void) value;
  // no-op (unsupported)
}  // setScalingMode

void SCIPInterface::setLPAlgorithm(int value)
{
  switch (value)
  {
    case 1 /*paramset::kLPAlgorithmDual*/: {
      const auto status =
          SCIP_TO_STATUS(SCIPsetCharParam(pSCIPSolver, "lp/initalgorithm", 'd'));
      if (status.isOk()) pStatus = status;
      return;
    }
    case 2 /*paramset::kLPAlgorithmPrimal*/: {
      const auto status =
          SCIP_TO_STATUS(SCIPsetCharParam(pSCIPSolver, "lp/initalgorithm", 'p'));
      if (status.isOk()) pStatus = status;
      return;
    }
    case 3 /*paramset::kLPAlgorithmBarrier*/: {
      // Barrier with crossover
      const auto status =
          SCIP_TO_STATUS(SCIPsetCharParam(pSCIPSolver, "lp/initalgorithm", 'p'));
      if (status.isOk()) pStatus = status;
      return;
    }
    default:
    {
      throw std::runtime_error("SCIPInterface - setLPAlgorithm: "
          "unrecognized mode " + std::to_string(value));
    }
  }
}  // setLPAlgorithm

#define RETURN_ABNORMAL_IF_BAD_STATUS             \
  do {                                            \
    if (!pStatus.isOk()) {                        \
      return lmcommon::ResultStatus::ABNORMAL;    \
    }                                             \
  } while (false)

#define RETURN_ABNORMAL_IF_SCIP_ERROR(x) \
  do {                                   \
    RETURN_ABNORMAL_IF_BAD_STATUS;       \
    pStatus = SCIP_TO_STATUS(x);         \
    RETURN_ABNORMAL_IF_BAD_STATUS;       \
  } while (false);

lmcommon::ResultStatus SCIPInterface::solve(const metabb::ParamSet& param)
{
  if (!pStatus.isOk())
  {
    return lmcommon::ResultStatus::ABNORMAL;
  }

  timer::Timer preSolveTimer;
  if (param.getSolverParamset().incrementalityvalue() == metabb::paramset::kIncrementalityOff)
  {
    reset();
    pBranchingPriorityReset = false;
  }

  // Set log level: suppress (true) screen messages
  SCIPsetMessagehdlrQuiet(pSCIPSolver, true);

  // Special case if the model is empty.
  // @note SCIP expects a non-empty model
  if (pSolver->pVariableList.empty() && pSolver->pConstraintList.empty())
  {
    // Solution is in sync. and optimal (i.e., empty solution)
    pSynchStatus = SynchronizationStatus::SOLUTION_SYNCHRONIZED;
    pResultStatus = lmcommon::ResultStatus::OPTIMAL;
    pObjectiveValue = pSolver->getObjective().getOffset();
    return pResultStatus;
  }

  // Extract and build the model
  extractModel();
  pLoadModelTimeMsec = preSolveTimer.getWallClockTimeMsec();

  // Set solver's timeout
  if (pSolver->getTimeoutMsec() != 0)
  {
    RETURN_ABNORMAL_IF_SCIP_ERROR(
        SCIPsetRealParam(pSCIPSolver, "limits/time",
                         static_cast<double>(pSolver->getTimeoutMsec() / 1000.0)));
  }
  else
  {
    // Use default value for timeout
    RETURN_ABNORMAL_IF_SCIP_ERROR(SCIPresetParam(pSCIPSolver, "limits/time"));
  }

  // Set parameters
  setParameters(param);

  // Use warm start, if any
  if (!pSolver->pWarmStart.empty())
  {
    SCIP_SOL* solution;
    bool isSolutionPartial = false;

    const int numVars = static_cast<int>(pSolver->pVariableList.size());
    if (static_cast<int>(pSolver->pWarmStart.size()) != numVars)
    {
      // Start by creating an empty partial solution
      RETURN_ABNORMAL_IF_SCIP_ERROR(
          SCIPcreatePartialSol(pSCIPSolver, &solution, nullptr));
      isSolutionPartial = true;
    }
    else
    {
      // Start by creating the all-zero solution.
      RETURN_ABNORMAL_IF_SCIP_ERROR(SCIPcreateSol(pSCIPSolver, &solution, nullptr));
    }

    // Fill the variables from the given warm start
    for (const auto& pair : pSolver->pWarmStart)
    {
      // Sets value of variable in primal CIP solution
      RETURN_ABNORMAL_IF_SCIP_ERROR(SCIPsetSolVal(
          pSCIPSolver, solution, pSCIPVariableList[pair.first->getIndex()], pair.second));
    }

    // If warm-start is a complete assignment,
    // check if it is also a valid solution
    if (!isSolutionPartial)
    {
      // Checks solution for feasibility without adding it to the solution store
      SCIP_Bool isFeasible;
      RETURN_ABNORMAL_IF_SCIP_ERROR(SCIPcheckSol(
          pSCIPSolver,
          solution,
          /*printreason=*/      false,
          /*completely=*/       true,
          /*checkbounds=*/      true,
          /*checkintegrality=*/ true,
          /*checklprows=*/      true,
          &isFeasible));
    }

    SCIP_Bool isStored;
    if (!isSolutionPartial && SCIPisTransformed(pSCIPSolver))
    {
      RETURN_ABNORMAL_IF_SCIP_ERROR(SCIPtrySolFree(
          pSCIPSolver,
          &solution,
          /*printreason=*/      false,
          /*completely=*/       true,
          /*checkbounds=*/      true,
          /*checkintegrality=*/ true,
          /*checklprows=*/      true,
          &isStored));
    }
    else
    {
      RETURN_ABNORMAL_IF_SCIP_ERROR(
          SCIPaddSolFree(pSCIPSolver, &solution, &isStored));
    }
  }

  timer::Timer solveTimer;

  // Solve the model
  RETURN_ABNORMAL_IF_SCIP_ERROR(pSolver->getNumThreads() > 1 ?
      SCIPsolveConcurrent(pSCIPSolver) :
      SCIPsolve(pSCIPSolver));

  pSolveTimeMsec = solveTimer.getWallClockTimeMsec();

  // Get the results.
  // Gets best feasible primal solution found so far if the problem is transformed
  SCIP_SOL* const solution = SCIPgetBestSol(pSCIPSolver);
  if (solution != nullptr)
  {
    // If optimal or feasible solution is found set the objective value.
    // @note the following call returns objective value of primal CIP solution
    // w.r.t. original problem, or current LP/pseudo objective value
    pObjectiveValue = SCIPgetSolOrigObj(pSCIPSolver, solution);

    for (int idx = 0; idx < static_cast<int>(pSolver->pVariableList.size()); ++idx)
    {
      MPVariable* const var = pSolver->pVariableList[idx].get();
      const int varIndex = var->getIndex();

      // Returns value of variable in primal CIP solution,
      // or in current LP/pseudo solution
      const double val =
          SCIPgetSolVal(pSCIPSolver, solution, pSCIPVariableList[varIndex]);
      var->setSolutionValue(val);
    }
  }
  else
  {
    spdlog::info(std::string("No feasible solution found for problem ") + pSolver->getName());
  }

  // Check the status: optimal, infeasible, etc.
  SCIP_STATUS scipStatus = SCIPgetStatus(pSCIPSolver);
  switch(scipStatus)
  {
    case SCIP_Status::SCIP_STATUS_OPTIMAL:
      pResultStatus = lmcommon::ResultStatus::OPTIMAL;
      break;
    case SCIP_Status::SCIP_STATUS_GAPLIMIT:
      // To be consistent with the other solvers
      pResultStatus = lmcommon::ResultStatus::OPTIMAL;
      break;
    case SCIP_Status::SCIP_STATUS_INFEASIBLE:
      pResultStatus = lmcommon::ResultStatus::INFEASIBLE;
      break;
    case SCIP_Status::SCIP_STATUS_UNBOUNDED:
      pResultStatus = lmcommon::ResultStatus::UNBOUNDED;
      break;
    case SCIP_Status::SCIP_STATUS_INFORUNBD:
      pResultStatus = lmcommon::ResultStatus::INFEASIBLE;
      break;
    default:
      if (solution != nullptr)
      {
        pResultStatus = lmcommon::ResultStatus::FEASIBLE;
      }
      else if (scipStatus == SCIP_Status::SCIP_STATUS_TIMELIMIT)
      {
        pResultStatus = lmcommon::ResultStatus::NOT_SOLVED;
      }
      else
      {
        pResultStatus = lmcommon::ResultStatus::ABNORMAL;
      }
      break;
  }

  // Resets all parameters to their default values
  RETURN_ABNORMAL_IF_SCIP_ERROR(SCIPresetParams(pSCIPSolver));

  // Solution is now in synch.
  pSynchStatus = SynchronizationStatus::SOLUTION_SYNCHRONIZED;
  return pResultStatus;
}  // solve

}  // namespace toolbox
}  // namespace optilab

namespace optilab {
namespace toolbox {
MPSolverInterface* buildSCIPInterface(MPSolver* const solver)
{
  return new SCIPInterface(solver);
}
}  // namespace toolbox
}  // namespace optilab
