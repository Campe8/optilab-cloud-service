#include "or_toolbox/meta_net_diff_subproblem.hpp"

#include <cassert>
#include <cmath>      // for ceil
#include <stdexcept>  // for std::runtime_error
#include <sstream>

#include <google/protobuf/util/message_differencer.h>
#include <spdlog/spdlog.h>

#include "meta_bb/meta_bb_constants.hpp"
#include "meta_bb/meta_bb_macros.hpp"
#include "meta_bb/meta_bb_struct.hpp"
#include "meta_bb/meta_bb_utilities.hpp"
#include "meta_bb/paramset.hpp"
#include "or_toolbox/meta_net_initiator.hpp"
#include "or_toolbox/meta_net_instance.hpp"
#include "or_toolbox/meta_net_solver.hpp"

namespace optilab {
namespace  toolbox {

MetaNetDiffSubproblem::MetaNetDiffSubproblem()
{
}

MetaNetDiffSubproblem::MetaNetDiffSubproblem(std::shared_ptr<MetaNetSolver> solver,
                                             const std::vector<MPVariable::SPtr>& branchingVarList,
                                             const std::vector<double>& branchingBoundList,
                                             const std::vector<BoundType>& branchingBoundType,
                                             const std::vector<MPConstraint::SPtr>& constraintList)
{
  pNumBoundChanges = static_cast<int>(branchingVarList.size());
  int numParentBranchingVars{0};
  int varIdx{0};

  auto parentDiffSubproblem = solver->getParentDiffSubproblem();
  if (parentDiffSubproblem != nullptr)
  {
    numParentBranchingVars = parentDiffSubproblem->getNBoundChanges();
    pNumBoundChanges += numParentBranchingVars;
  }

  if (pNumBoundChanges > 0)
  {
    pVarIndexList.resize(pNumBoundChanges, -1);
    pBranchBoundList.resize(pNumBoundChanges);
    pBranchBoundTypeList.resize(pNumBoundChanges);

    // Fill the first variables with the parents variables
    for (int idx = 0; idx < numParentBranchingVars; ++idx)
    {
      pVarIndexList[idx] = parentDiffSubproblem->getIndex(idx);
      pBranchBoundList[idx] = parentDiffSubproblem->getBranchBound(idx);
      pBranchBoundTypeList[idx] = parentDiffSubproblem->getBranchBoundType(idx);
    }
  }

  const auto numNewBranchingVars = static_cast<int>(branchingVarList.size());
  for (int idx = numNewBranchingVars - 1; idx >= 0; --idx)
  {
    auto var = branchingVarList[idx];

    // Do not set added (non-original) variables
    if (var->getIndex() >= solver->getNumOriginalVars()) continue;
    pVarIndexList[idx] = var->getIndex();
    assert(pVarIndexList[idx] >= 0);

    pBranchBoundList[idx] = branchingBoundList[idx];
    pBranchBoundTypeList[idx] = branchingBoundType[idx];
    if (var->isInteger())
    {
      if (pBranchBoundTypeList[idx] == BoundType::BT_LOWER)
      {
        pBranchBoundList[idx] = ceil(pBranchBoundList[idx]);
      }
      else
      {
        pBranchBoundList[idx] = floor(pBranchBoundList[idx]);
      }
    }
    varIdx++;
  }
  pNumBoundChanges = varIdx;

  // Add the linear constraints
  addBranchingLinearConstraints(solver, constraintList);

  if (((solver->getParamSet()->getDescriptor()).transferlocalcuts()) ||
          ((solver->getParamSet()->getDescriptor()).transferconflictscuts()))
  {
    addNodeInfo(solver);
  }
}

MetaNetDiffSubproblem::MetaNetDiffSubproblem(const MetaNetDiffSubproblem& other)
{
  pNumBoundChanges = other.pNumBoundChanges;
  pVarIndexList = other.pVarIndexList;
  pBranchBoundList = other.pBranchBoundList;
  pBranchBoundTypeList = other.pBranchBoundTypeList;
}

MetaNetDiffSubproblem& MetaNetDiffSubproblem::operator=(const MetaNetDiffSubproblem& other)
{
  if (this == &other)
  {
    return *this;
  }

  pNumBoundChanges = other.pNumBoundChanges;
  pVarIndexList = other.pVarIndexList;
  pBranchBoundList = other.pBranchBoundList;
  pBranchBoundTypeList = other.pBranchBoundTypeList;

  return *this;
}

void MetaNetDiffSubproblem::addBranchingLinearConstraints(
        std::shared_ptr<MetaNetSolver> solver,
        const std::vector<MPConstraint::SPtr>& constraintList)
{
  // First put the linear constraints of the parent (if any)
  auto parentDiffSubproblem = solver->getParentDiffSubproblem();
  if (parentDiffSubproblem)
  {
    pBranchLinearConsList = parentDiffSubproblem->pBranchLinearConsList;
  }

  // Then put the linear constraint given as input to this method
  for (auto con : constraintList)
  {
    BranchLinearConstraintInfo info;
    info.name = con->getName();
    info.lowerBound = con->lowerBound();
    info.upperBound = con->upperBound();
    info.coeffList = con->getVarCoefficientList();
    pBranchLinearConsList.push_back(info);
  }
}  // addBranchingLinearConstraints

void MetaNetDiffSubproblem::addNodeInfo(std::shared_ptr<MetaNetSolver> solver)
{
  /// TODO add this method
}  // addNodeInfo

int MetaNetDiffSubproblem::getNBoundChanges() const noexcept
{
  return pNumBoundChanges;
}  // getNBoundChanges

metabb::DiffSubproblem::SPtr MetaNetDiffSubproblem::createDiffSubproblem(
        metabb::Framework::SPtr framework,
        std::shared_ptr<metabb::Initiator> initiator,
        const std::vector<metabb::mergenodes::FixedVariable*>& fixedVarList)
{
  auto diffSubproblem = std::dynamic_pointer_cast<MetaNetDiffSubproblem>(
          framework->buildDiffSubproblem());
  auto minitiator = std::dynamic_pointer_cast<MetaNetInitiator>(initiator);
  if (!diffSubproblem)
  {
    throw std::runtime_error("MetaNetDiffSubproblem - createDiffSubproblem: "
            "invalid downcast to MetaDiffSubproblem");
  }

  if (!minitiator)
  {
    throw std::runtime_error("MetaNetDiffSubproblem - createDiffSubproblem: "
            "invalid downcast to MetaInitiator");
  }

  int numNewBranches{0};
  diffSubproblem->pVarIndexList.resize(pNumBoundChanges);
  diffSubproblem->pBranchBoundList.resize(pNumBoundChanges);
  diffSubproblem->pBranchBoundTypeList.resize(pNumBoundChanges);

  if (minitiator->areTightenedVarBounds())
  {
    // Check all the changed bounds
    for (int idx = 0; idx < pNumBoundChanges; ++idx)
    {
      bool updated{false};
      const int varListSize = static_cast<int>(fixedVarList.size());
      for (int varIdx = 0; varIdx < varListSize; ++varIdx)
      {
        // Check if there is a variable in the given fixed variable list that matches
        // one of the variables in this diff. subproblem
        if (pVarIndexList[idx] == fixedVarList[varIdx]->index)
        {
          // Index match, variable found: copy variable
          diffSubproblem->pVarIndexList[numNewBranches] = pVarIndexList[idx];
          diffSubproblem->pBranchBoundList[numNewBranches] = pBranchBoundList[idx];
          diffSubproblem->pBranchBoundTypeList[numNewBranches] = pBranchBoundTypeList[idx];
          numNewBranches++;
          updated = true;
          break;
        }
      }

      // @note tightened local bound changes including global bound changes
      // MUST be received already.
      // If there is no match with the given list of fixed variables, update the lower and
      // upper bound anyways if the given initiator has a tightened lower/upper bound
      if (!updated)
      {
        const auto initLB = minitiator->getTightenedVarLB(pVarIndexList[idx]);
        const auto initUB = minitiator->getTightenedVarUB(pVarIndexList[idx]);
        assert(initLB < std::numeric_limits<double>::max());
        assert(initUB > std::numeric_limits<double>::lowest());
        if (pBranchBoundTypeList[idx] == BoundType::BT_LOWER &&
                EPSGT(initLB, pBranchBoundList[idx], metabb::metaconst::MIN_EPSILON))
        {
          // Initiator has a higher LB -> set it
          diffSubproblem->pVarIndexList[numNewBranches] = pVarIndexList[idx];
          diffSubproblem->pBranchBoundList[numNewBranches] = initLB;
          diffSubproblem->pBranchBoundTypeList[numNewBranches] = pBranchBoundTypeList[idx];
          numNewBranches++;
        }

        if (pBranchBoundTypeList[idx] == BoundType::BT_UPPER &&
                EPSLT(initUB, pBranchBoundList[idx], metabb::metaconst::MIN_EPSILON))
        {
          // Initiator has a lower UB -> set it
          diffSubproblem->pVarIndexList[numNewBranches] = pVarIndexList[idx];
          diffSubproblem->pBranchBoundList[numNewBranches] =
                  minitiator->getTightenedVarUB(initUB);
          diffSubproblem->pBranchBoundTypeList[numNewBranches] = pBranchBoundTypeList[idx];
          numNewBranches++;
        }
      }
    }  // idx
  }
  else
  {
    // Variable do not have tightened bounds
    for (int idx = 0; idx < pNumBoundChanges; ++idx)
    {
      const int varListSize = static_cast<int>(fixedVarList.size());
      for (int varIdx = 0; varIdx < varListSize; ++varIdx)
      {
        // Check if there is a variable in the given fixed variable list that matches
        // one of the variables in this diff. subproblem
        if (pVarIndexList[idx] == fixedVarList[varIdx]->index)
        {
          // Index match, variable found: copy variable
          diffSubproblem->pVarIndexList[numNewBranches] = pVarIndexList[idx];
          diffSubproblem->pBranchBoundList[numNewBranches] = pBranchBoundList[idx];
          diffSubproblem->pBranchBoundTypeList[numNewBranches] = pBranchBoundTypeList[idx];
          numNewBranches++;
          break;
        }
      }
    }
  }

  // Set the number of bound changes
  diffSubproblem->pNumBoundChanges = numNewBranches;

  // Return the diff. subproblem
  return diffSubproblem;
}  // createDiffSubproblem

int MetaNetDiffSubproblem::getFixedVariables(
        const metabb::Instance::SPtr instance,
        std::vector<metabb::mergenodes::FixedVariable::SPtr>& fixedVarList)
{
  auto minstance = std::dynamic_pointer_cast<MetaNetInstance>(instance);

  if (!minstance)
  {
    throw std::runtime_error("MetaNetDiffSubproblem - getFixedVariables: "
            "invalid downcast to MetaInstance");
  }

  fixedVarList.resize(pNumBoundChanges);

  int k{0};
  int fixedVarCtr{0};
  for (int idx = 0; idx < pNumBoundChanges; ++idx)
  {
    int probIdx = pVarIndexList[idx];

    // Skip inactive variables
    if (probIdx < 0) continue;
    switch(minstance->getVarType(probIdx))
    {
      case MetaNetInstance::VarType::VT_BINARY:
      {
        // If the variable is changed on lower bound,
        // i.e., pBranchBoundTypeList[idx] == BoundType::BT_LOWER,
        // since the variable is binary, the lower bound should match the upper bound.
        // Vice-versa if variable is changed on lower bound,
        // i.e., pBranchBoundTypeList[idx] == BoundType::BT_UPPER,
        // the upper bound should match the lower bound
        assert(EPSEQ(pBranchBoundList[idx], minstance->getVarUb(probIdx),
                     metabb::metaconst::DEAFULT_NUM_EPSILON) ||
               EPSEQ(pBranchBoundList[idx], minstance->getVarLb(probIdx),
                     metabb::metaconst::DEAFULT_NUM_EPSILON));

        for (k = 0 ; k < fixedVarCtr; k++)
        {
           if (pVarIndexList[idx] == fixedVarList[k]->index) break;
        }
        if (k != fixedVarCtr) break;

        fixedVarList[fixedVarCtr]->numSameValue = 0;
        fixedVarList[fixedVarCtr]->index = pVarIndexList[idx];
        fixedVarList[fixedVarCtr]->value = pBranchBoundList[idx];
        fixedVarList[fixedVarCtr]->mnode = nullptr;
        fixedVarList[fixedVarCtr]->next = nullptr;
        fixedVarList[fixedVarCtr]->prev = nullptr;
        fixedVarCtr++;
        break;

        break;
      }
      case MetaNetInstance::VarType::VT_INTEGER:
      case MetaNetInstance::VarType::VT_CONTINUOUS:
      {
        if (pBranchBoundTypeList[idx] == BoundType::BT_LOWER)
        {
          // The change in lower bound does not match the upper bound
          if (!EPSEQ(pBranchBoundList[idx], minstance->getVarUb(probIdx),
                     metabb::metaconst::DEAFULT_NUM_EPSILON))
          {
            // Change on lower bound but lower bound does not match the upper bound
            int j = idx + 1;
            for (; j < pNumBoundChanges; ++j)
            {
              if (pVarIndexList[idx] == pVarIndexList[j] &&
                      pBranchBoundTypeList[idx] == BoundType::BT_UPPER &&
                      EPSEQ(pBranchBoundList[idx], pBranchBoundList[j],
                            metabb::metaconst::DEAFULT_NUM_EPSILON))
              {
                // The same variable also changed its upper bound and the
                // lower and upper bounds are the same
                break;
              }
            }

            if (j >= pNumBoundChanges) break;
          }
        }
        else
        {
          // Change in upper bound
          if (!EPSEQ(pBranchBoundList[idx], minstance->getVarLb(probIdx),
                     metabb::metaconst::MIN_EPSILON))
          {
            // The change in upper bound does not match the lower bound
            int j = idx + 1;
            for (; j < pNumBoundChanges; ++j)
            {
              if (pVarIndexList[idx] == pVarIndexList[j] &&
                      pBranchBoundTypeList[idx] == BoundType::BT_LOWER &&
                      EPSEQ(pBranchBoundList[idx], pBranchBoundList[j],
                            metabb::metaconst::DEAFULT_NUM_EPSILON))
              {
                // The same variable also changed its lower bound and the
                // lower and upper bounds are the same
                break;
              }
            }

            if (j >= pNumBoundChanges) break;
          }
        }

        for (k = 0 ; k < fixedVarCtr; k++)
        {
           if (pVarIndexList[idx] == fixedVarList[k]->index) break;
        }
        if (k != fixedVarCtr) break;

        fixedVarList[fixedVarCtr]->numSameValue = 0;
        fixedVarList[fixedVarCtr]->index = pVarIndexList[idx];
        fixedVarList[fixedVarCtr]->value = pBranchBoundList[idx];
        fixedVarList[fixedVarCtr]->mnode = nullptr;
        fixedVarList[fixedVarCtr]->next = nullptr;
        fixedVarList[fixedVarCtr]->prev = nullptr;
        fixedVarCtr++;

        break;
      }
      default:
        throw std::runtime_error("MetaNetDiffSubproblem - getFixedVariables: "
                "undefined variable type");
    }
  }

  if (fixedVarCtr == 0)
  {
    fixedVarList.clear();
  }
  return fixedVarCtr;
}  // getFixedVariables

metabb::DiffSubproblem::SPtr MetaNetDiffSubproblem::clone(const metabb::Framework::SPtr& framework)
{
  (void) framework;
  return metabb::DiffSubproblem::SPtr(new MetaNetDiffSubproblem(*this));
}  // clone

int MetaNetDiffSubproblem::send(const metabb::Framework::SPtr& framework, int destination) noexcept
{
  if (!framework)
  {
    spdlog::error("MetaNetDiffSubproblem - send: empty framework");
    return metabb::metaconst::ERR_GENERIC_ERROR;
  }

  // Create the optimizer MetaNetDiffSubproblem packet to send to the destination
  optnet::Packet::UPtr packet = buildNetworkPacket();

  // Add the node tag
  packet->packetTag = optnet::PacketTag::UPtr(new metabb::net::MetaBBPacketTag(
          metabb::net::MetaBBPacketTag::PacketTagType::PPT_DIFF_SUBPROBLEM));

  if (framework->getComm()->send(std::move(packet), destination))
  {
    return metabb::metaconst::ERR_GENERIC_ERROR;
  }

  return metabb::metaconst::ERR_NO_ERROR;
}  // MetaNetDiffSubproblem::

int MetaNetDiffSubproblem::receive(const metabb::Framework::SPtr& framework, int source) noexcept
{
  if (!framework)
  {
    spdlog::error("MetaNetDiffSubproblem - receive: empty framework");
    return metabb::metaconst::ERR_GENERIC_ERROR;
  }

  // Wait for the incoming message with diff-suproblem from the given source
  optnet::Packet::UPtr packet(new optnet::Packet());
  packet->packetTag = metabb::net::MetaBBPacketTag::UPtr(new metabb::net::MetaBBPacketTag(
          metabb::net::MetaBBPacketTag::PacketTagType::PPT_DIFF_SUBPROBLEM));
  try
  {
    ASSERT_COMM_CALL(framework->getComm()->receive(packet, source));
  }
  catch(...)
  {
    spdlog::error("MetaNetDiffSubproblem - receive: error");
    return metabb::metaconst::ERR_GENERIC_ERROR;
  }

  if (!packet)
  {
    spdlog::error("MetaNetDiffSubproblem - receive: empty packet");
    return metabb::metaconst::ERR_GENERIC_ERROR;
  }

  // This packet was deserialized as a std::string packet containing
  // the bytes of the serialized original message.
  // Therefore, the packet must be first deserialized into a proper
  // NetworkPacket
  if (packet->networkPacket->packetType !=
          optnet::BaseNetworkPacket::BaseNetworkPacketType::BNPT_META_BB_PACKET)
  {
    spdlog::error("MetaNetDiffSubproblem - receive: invalid network packet type received");
    return metabb::metaconst::ERR_GENERIC_ERROR;
  }

  auto networkPacket = optnet::castBaseToNetworkPacket<std::string>(
          std::move(packet->networkPacket));
  if (!networkPacket)
  {
    spdlog::error("MetaNetDiffSubproblem - receive: error downcasting to NetworkPacket<std::string>");
    return metabb::metaconst::ERR_GENERIC_ERROR;
  }

  if (networkPacket->size() != 1)
  {
    spdlog::error("MetaNetDiffSubproblem - receive: wrong packet data size " +
                  std::to_string(networkPacket->size()));
    return metabb::metaconst::ERR_GENERIC_ERROR;
  }

  if (uploadPacket(networkPacket->data.at(0))) return metabb::metaconst::ERR_GENERIC_ERROR;
  return metabb::metaconst::ERR_NO_ERROR;
}  // receive

int MetaNetDiffSubproblem::broadcast(const metabb::Framework::SPtr& framework, int root) noexcept
{
  if (!framework)
  {
    spdlog::error("MetaNetDiffSubproblem - broadcast: empty framework");
    return metabb::metaconst::ERR_GENERIC_ERROR;
  }

  // Broadcast or received a broadcasted packet containing a DiffSubproblem
  optnet::Packet::UPtr packet;
  if (framework->getComm()->getRank() == root)
  {
    // This is the broadcasting node.
    // Create the optimizer DiffSubproblem packet to send to the destination
    packet = buildNetworkPacket();
  }

  // Send/receive the DiffSubproblem
  if (framework->getComm()->broadcast(packet, root))
  {
    return metabb::metaconst::ERR_GENERIC_ERROR;
  }

  if (framework->getComm()->getRank() != root)
  {
    // This packet was deserialized as a std::string packet containing
    // the bytes of the serialized original message.
    // Therefore, the packet must be first deserialized into a proper
    // NetworkPacket
    if (packet->networkPacket->packetType !=
            optnet::BaseNetworkPacket::BaseNetworkPacketType::BNPT_META_BB_PACKET)
    {
      spdlog::error("MetaNetDiffSubproblem - broadcast: invalid network packet type received");
      return metabb::metaconst::ERR_GENERIC_ERROR;
    }

    auto networkPacket = optnet::castBaseToNetworkPacket<std::string>(
            std::move(packet->networkPacket));
    if (!networkPacket)
    {
      spdlog::error("MetaNetDiffSubproblem - broadcast: "
              "error downcasting to NetworkPacket<std::string>");
      return metabb::metaconst::ERR_GENERIC_ERROR;
    }

    if (networkPacket->size() != 1)
    {
      spdlog::error("MetaNetDiffSubproblem - broadcast: wrong packet data size " +
                    std::to_string(networkPacket->size()));
      return metabb::metaconst::ERR_GENERIC_ERROR;
    }

    if (uploadPacket(networkPacket->data.at(0))) return metabb::metaconst::ERR_GENERIC_ERROR;
    return metabb::metaconst::ERR_NO_ERROR;
  }

  return metabb::metaconst::ERR_NO_ERROR;
}  // broadcast

optnet::Packet::UPtr MetaNetDiffSubproblem::buildNetworkPacket()
{
  // Create the optimizer MetaNetDiffSubproblem packet to send to the destination
  optnet::Packet::UPtr packet(new optnet::Packet());

  metabb::DiffSubproblemProtoPacket protoPacket;
  protoPacket.set_numboundchanges(pNumBoundChanges);
  for (auto idx : pVarIndexList)
  {
    protoPacket.add_varindexlist(idx);
  }

  for (auto bound : pBranchBoundList)
  {
    protoPacket.add_branchboundlist(bound);
  }

  for (auto boundType : pBranchBoundTypeList)
  {
    protoPacket.add_branchboundtypelist(static_cast<int>(boundType));
  }

  for (const auto& con : pBranchLinearConsList)
  {
    auto protoCon = protoPacket.add_branchlinearconslist();
    protoCon->set_name(con.name);
    protoCon->set_lowerbound(con.lowerBound);
    protoCon->set_upperbound(con.upperBound);

    for (const auto& coeff : con.coeffList)
    {
      protoCon->add_coefflistvaridx(coeff.first);
      protoCon->add_coefflistcoeff(coeff.second);
    }
  }

  // Set the protobuf message as new network packet
  packet->networkPacket = optnet::BaseNetworkPacket::UPtr(
          new optnet::NetworkPacket<metabb::DiffSubproblemProtoPacket>(protoPacket));

  // Return the packet
  return std::move(packet);
}  // buildNetworkPacket

int MetaNetDiffSubproblem::uploadPacket(const std::string& bytes)
{
  // Get the node proto packet and upload its content into this MetaNetDiffSubproblem
  metabb::DiffSubproblemProtoPacket diffSubproblemProto;
  diffSubproblemProto.ParseFromString(bytes);

  pNumBoundChanges = diffSubproblemProto.numboundchanges();
  pVarIndexList.clear();
  for (int idx = 0; idx < diffSubproblemProto.varindexlist_size(); ++idx)
  {
    pVarIndexList.push_back(diffSubproblemProto.varindexlist(idx));
  }

  pBranchBoundList.clear();
  for (int idx = 0; idx < diffSubproblemProto.branchboundlist_size(); ++idx)
  {
    pBranchBoundList.push_back(diffSubproblemProto.branchboundlist(idx));
  }

  pBranchBoundTypeList.clear();
  for (int idx = 0; idx < diffSubproblemProto.branchboundtypelist_size(); ++idx)
  {
    pBranchBoundTypeList.push_back(static_cast<MetaNetDiffSubproblem::BoundType>(
            diffSubproblemProto.branchboundtypelist(idx)));
  }

  pBranchLinearConsList.clear();
  pBranchLinearConsList.resize(diffSubproblemProto.branchlinearconslist_size());
  for (int idx = 0; idx < diffSubproblemProto.branchlinearconslist_size(); ++idx)
  {
    const auto& conProto = diffSubproblemProto.branchlinearconslist(idx);
    BranchLinearConstraintInfo& conInfo = pBranchLinearConsList[idx];

    conInfo.name = conProto.name();
    conInfo.lowerBound = conProto.lowerbound();
    conInfo.upperBound = conProto.upperbound();

    if (conProto.coefflistvaridx_size() != conProto.coefflistcoeff_size())
    {
      spdlog::error("MetaNetDiffSubproblem - uploadPacket: "
              "wrong coeff index data size");
      return metabb::metaconst::ERR_GENERIC_ERROR;
    }
    for (int coeffIdx = 0; coeffIdx < conProto.coefflistvaridx_size(); ++coeffIdx)
    {
      conInfo.coeffList.push_back({conProto.coefflistvaridx(coeffIdx),
        conProto.coefflistcoeff(coeffIdx)});
    }
  }

  return metabb::metaconst::ERR_NO_ERROR;
}  // uploadPacket

std::string MetaNetDiffSubproblem::toString() const noexcept
{
  std::ostringstream ss;
  ss << "MetaNetDiffSubproblem:\n";
  ss << "\tnumber of branches: " << pNumBoundChanges << std::endl;
  for (int idx = 0; idx < pNumBoundChanges; ++idx)
  {
    ss << "\t\tvar index [" << idx << "] = " << pVarIndexList[idx] << std::endl;
    ss << "\t\tbranch bound [" << idx << "] = " << pBranchBoundList[idx] << std::endl;
    ss << "\t\tbound type [" << idx << "] = ";
    if (pBranchBoundTypeList[idx] == BoundType::BT_LOWER) ss << "lower" << std::endl;
    else ss << "upper" << std::endl;
  }

  return ss.str();
}  // toString

}  // namespace toolbox

template<> bool optnet::NetworkPacket<metabb::DiffSubproblemProtoPacket>::operator==(
    const NetworkPacket<metabb::DiffSubproblemProtoPacket>& other) const noexcept
{
  if (this->data.size() != other.data.empty()) return false;
  return google::protobuf::util::MessageDifferencer::Equals(this->data[0], other.data[0]);
}

template<>
std::pair<optnet::BaseNetworkPacket::BaseNetworkPacketType, std::string>
optnet::NetworkPacket<metabb::DiffSubproblemProtoPacket>::serialize() const noexcept
{
  if (data.size() != 1)
  {
    spdlog::error("NetworkPacket<DiffSubproblemProtoPacket> - invalid data size");
    return {BaseNetworkPacketType::BNPT_UNSPECIFIED_PACKET, ""};
  }

  try
  {
    return {BaseNetworkPacketType::BNPT_META_BB_PACKET, data.at(0).SerializeAsString()};
  }
  catch(...)
  {
    spdlog::error("NetworkPacket<DiffSubproblemProtoPacket> - "
            "undefined error when serializing protobuf");
    return {BaseNetworkPacketType::BNPT_UNSPECIFIED_PACKET, ""};
  }
}  // serialize

}  // namespace optilab
