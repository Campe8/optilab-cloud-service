//
// Copyright OptiLab 2020. All rights reserved.
//
// Processor for a distributed network node of the
// distributed OR (MP) engine.
// Notice that a processor is usually part of
// an optimizer pipeline calling work task asynchronously.
// This processor has implemented synchronous method in
// order to provide a stand-alone object executable.
// The optimizer should not use the synchronous methods
// but push Work tasks asynchronously.
//

#pragma once

#include "or_toolbox/net_or_processor.hpp"

#include <memory> // for std::shared_ptr

#include "or_toolbox/or_utilities.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace toolbox {

class SYS_EXPORT_CLASS NetORNodeProcessor : NetORProcessor {
public:
  using SPtr = std::shared_ptr<NetORNodeProcessor>;

public:
  NetORNodeProcessor();

  /// Initializes the network node
  void initNetworkNode();

  /// Runs the processor
  void run();
};

}  // namespace toolbox
}  // namespace optilab
