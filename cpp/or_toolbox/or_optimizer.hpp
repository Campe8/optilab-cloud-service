//
// Copyright OptiLab 2020. All rights reserved.
//
// An optimizer is an asynchronous engine that
// holds a pipeline containing MP processors.
//

#pragma once

#include <memory>  // for std::shared_ptr
#include <string>

#include "data_structure/metrics/metrics_register.hpp"
#include "engine/async_engine.hpp"
#include "engine/processor_pipeline.hpp"
#include "optilab_protobuf/linear_model.pb.h"
#include "or_toolbox/or_processor.hpp"
#include "or_toolbox/or_utilities.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace toolbox {

class SYS_EXPORT_CLASS OROptimizer : public AsyncEngine<orengine::ORWork::SPtr> {
 public:
  using SPtr = std::shared_ptr<OROptimizer>;

 public:
  OROptimizer(const std::string& engineName, orengine::ORResult::SPtr result,
              MetricsRegister::SPtr metricsRegister);

  /// Initializes this optimizer with the given linear model represented
  /// as a protobuf message.
  /// In other words, it loads the model into the internal state and creates
  /// the optimizer processor that will solve it.
  /// @note this is a blocking call
  void loadModel(const LinearModelSpecProto& model);

  /// Runs this optimizer on the loaded model
  void runOptimizer();

  /// Interrupts the solving processes on this optimizer.
  /// Returns true if the solver was successfully interrupted.
  /// Returns false otherwise
  bool interruptOptimizer();

 protected:
  ProcessorPipeline::SPtr buildPipeline(int pipeline = 0) override;

 private:
  using Work = orengine::ORWork;
  using BaseClass = AsyncEngine<Work::SPtr>;

 private:
  /// Pointer to the instance of the processor in the pipeline
  ORProcessor::SPtr pProcessor;

  /// Pointer to the result instance
  orengine::ORResult::SPtr pResult;

  /// Pointer to the metrics register given to each work task processed
  /// by each processor
  MetricsRegister::SPtr pMetricsRegister;
};

}  // namespace toolbox
}  // namespace optilab
