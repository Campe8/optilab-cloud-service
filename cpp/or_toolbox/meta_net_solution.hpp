//
// Copyright OptiLab 2020. All rights reserved.
//
// Distributed OR implementation based on metabb framework for the
// Solution class.
//

#pragma once

#include <memory>   // for std::shared_ptr
#include <utility>  // for std::pair
#include <vector>

#include <boost/thread.hpp>

#include <google/protobuf/util/message_differencer.h>

#include "meta_bb/framework.hpp"
#include "meta_bb/solution.hpp"
#include "optilab_protobuf/linear_model.pb.h"
#include "optimizer_network/network_packet.hpp"
#include "optimizer_network/packet.hpp"
#include "or_toolbox/linear_model.hpp"
#include "or_toolbox/linear_solver.hpp"
#include "or_toolbox/or_utilities.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace toolbox {

class SYS_EXPORT_CLASS MetaNetSolution : public metabb::Solution {
 public:
  enum class VarType {
    VT_BINARY,
    VT_INTEGER,
    VT_CONTINUOUS
  };

  using SPtr = std::shared_ptr<MetaNetSolution>;

  /// Solution is a list of pairs
  /// <int, double>
  /// where the first element is the index of the variable in the model,
  /// while the second is the solution value of the variable
  using SolutionList = std::vector<std::pair<int, double>>;

 public:
  MetaNetSolution();

  ~MetaNetSolution() override = default;

  /// Returns the value of the objective function
  double getObjectiveFuntionValue() override;

  /// Returns the number of variables in the solution
  inline int getNumVars() const noexcept { return pNumVars; }

  /// Returns a clone of this object
  metabb::Solution::SPtr clone() override;

  /// Broadcasts solution data to all other network nodes
  int broadcast(const metabb::Framework::SPtr& framework, int root) noexcept override;

  /// Sends solution to the given destination
  int send(const metabb::Framework::SPtr& framework, int destination) noexcept override;

  /// Uploads the content of the given network packet into this solution.
  /// If the given packet is not a node network packet, throws std::runtime_error
  /// Returns zero on success, non-zero otherwise
  int upload(optnet::Packet::UPtr packet) noexcept override;

  /// Uploads the solution from the MP solver
  void uploadSolution(MPSolver::SPtr solver);

  /// Returns the actual solution with values
  SolutionList getSolution() const noexcept;

  /// Returns the pointer to the ORSolution
  inline orengine::MIPResult::SPtr getORSolution() const noexcept { return pORSolution; }


 private:
   /// Lock type on the internal mutex
   using LockGuard = boost::lock_guard<boost::mutex>;

 private:
  /// Mutex synchronizing the access on the internal solution
  boost::mutex pSolutionMutex;

  /// Number of variables (and values) in the solution
  int pNumVars{0};

  /// Pointer to the actual solution
  orengine::MIPResult::SPtr pORSolution;

  /// Utility function: sets the result status of the solving process
  void setResultStatus(MPSolver::SPtr solver);
};

}  // namespace metabb

/// Specialized template for network LinearModelSolutionProto packet comparison
template<> bool optnet::NetworkPacket<LinearModelSolutionProto>::operator==(
    const NetworkPacket<LinearModelSolutionProto>& other) const noexcept;

/// Specialized template to serialize LinearModelSolutionProto(s)
template<> std::pair<optnet::BaseNetworkPacket::BaseNetworkPacketType, std::string>
optnet::NetworkPacket<LinearModelSolutionProto>::serialize() const noexcept;

}  // namespace optilab

