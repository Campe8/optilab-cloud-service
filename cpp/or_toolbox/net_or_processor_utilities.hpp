//
// Copyright OptiLab 2020. All rights reserved.
//
// Utilities for NetORProcessors.
//

#include "system/system_export_defs.hpp"

#include "meta_bb/load_manager.hpp"
#include "meta_bb/solver.hpp"
#include "or_toolbox/meta_net_framework.hpp"
#include "or_toolbox/meta_net_initiator.hpp"
#include "or_toolbox/meta_net_instance.hpp"
#include "or_toolbox/meta_net_paramset.hpp"
#include "utilities/timer.hpp"

namespace optilab {
namespace toolbox {

/**
 * Collection of objects for the NetORProcessor.
 */
struct SYS_EXPORT_STRUCT NetProcessorCollection {
  metabb::LoadManager::SPtr loadManager;
  metabb::Solver::SPtr solver;
  MetaNetFramework::SPtr framework;
  MetaNetInstance::SPtr instance;
  MetaNetInitiator::SPtr initiator;
  MetaNetParamSet::SPtr paramset;
  timer::Timer::SPtr timer;
};

}  // namespace toolbox
}  // namespace optilab
