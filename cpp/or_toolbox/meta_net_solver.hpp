//
// Copyright OptiLab 2020. All rights reserved.
//
// Distributed OR implementation based on metabb framework for the
// Solver class.
//

#pragma once

#include <memory>  // for std::shared_ptr
#include <vector>

#include "meta_bb/node.hpp"
#include "meta_bb/racing_rampup_paramset.hpp"
#include "meta_bb/solution.hpp"
#include "meta_bb/solver.hpp"
#include "or_toolbox/linear_model.hpp"
#include "or_toolbox/linear_solver.hpp"
#include "or_toolbox/meta_net_diff_subproblem.hpp"
#include "or_toolbox/meta_net_initiator.hpp"
#include "or_toolbox/meta_net_racing_rampup_paramset.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace  toolbox {

class SYS_EXPORT_CLASS MetaNetSolver : public metabb::Solver {
 public:
  using SPtr = std::shared_ptr<MetaNetSolver>;

 public:
  /// Constructor.
  /// @note if timer is not specified, it builds and starts a new timer
  MetaNetSolver(metabb::Framework::SPtr framework,
                metabb::ParamSet::SPtr paramSet, metabb::Instance::SPtr instance,
                timer::Timer::SPtr timer = nullptr);

  /// Returns the result of the latest solving process
  inline lmcommon::ResultStatus getSolvingResult() const noexcept { return pSolvingResult; }

  /// Returns the parent DiffSubproblem,
  /// i.e., the diffSubproblem paired with the current solving node
  inline MetaNetDiffSubproblem::SPtr getParentDiffSubproblem() const
  {
    return std::dynamic_pointer_cast<MetaNetDiffSubproblem>(pNode->getDiffSubproblem());
  }

  /// Returns the internal MP solver instance used by this solver to solve the MP model
  inline MPSolver::SPtr getSolver() const noexcept { return pSolver; }

  /// Returns the number of
  inline int getNumOriginalVars() const noexcept { return pNumOriginalVars; }

  /// Returns the original lower bound of the specified variable
  inline double getOriginalVarLowerBound(int varIdx) const
  {
    return ((pSolver->getVariableList()).at(varIdx))->origLowerBound();
  }

  /// Returns the original upper bound of the specified variable
  inline double getOriginalVarUpperBound(int varIdx) const
  {
    return ((pSolver->getVariableList()).at(varIdx))->origUpperBound();
  }


  /// Returns the number of simplex iterations
  long long getSimplexIter() override { return getNumSimplexIter(); }
  long long getNumSimplexIter() const
  {
    return static_cast<long long>(pSolver->numSimplexIterations());
  }

  /// Returns true if this solver terminated normally.
  /// Returns false otherwise
  bool wasTerminatedNormally() override { return true; }

  /// Tries the given solution as new solution
  void tryNewSolution(metabb::Solution::SPtr sol) override;

  /// Sets light weight root node process,
  /// i.e., sets fast root node computation
  void setLightWeightRootNodeProcess() override;

  /// Sets the original root node process
  void setOriginalRootNodeProcess() override;

 protected:
  /// Sets the racing parameters indicating whether or not those are
  /// the parameters of the winning solver
  void setRacingParams(metabb::RacingRampUpParamSet::SPtr racingParams, bool winnerParam) override;

  /// Sets the winning racing parameters
  void setWinnerRacingParams(metabb::RacingRampUpParamSet::SPtr racingParams) override;

  /// Creates a subproblem
  void createSubproblem() override;

  /// Free subproblem
  void freeSubproblem() override;

  /// Solving method
  void solve() override;

  /// Returns the number of nodes solved
  long long getNumNodesSolved() override;

  /// Returns the number of nodes left in the solver
  int getNumNodesLeft() override;

  /// Returns the dual bound value
  double getDualBoundValue() override;

  /// Perform re-initialization of the model instance
  void reinitializeInstance() override;

  /// Resets the original node selection strategy
  void setOriginalNodeSelectionStrategy() override;

  int lowerBoundTightened(int source, optnet::Packet::UPtr packet) override;
  int upperBoundTightened(int source, optnet::Packet::UPtr packet) override;

  /// Returns the number of tightened variables during racing
  int getNumVarTightened() override { return 0; }

  /// Returns the number of tightened integer variables during racing
  int getNumIntVarTightened() override { return 0; }

 private:
  /// Number of variables in the initial, original model
  int pNumOriginalVars{0};

  /// List of original lower bound of variables
  std::vector<double> pInitialVarLBList;

  /// List of original upper bound of variables
  std::vector<double> pInitialVarUBList;

  /// List of tightened lower bound of variables
  std::vector<double> pTightenedVarLBList;

  /// List of tightened upper bound of variables
  std::vector<double> pTightenedVarUBList;

  /// List of added branch constraints from received
  /// diff. subproblem instances
  std::vector<MPConstraint::SPtr> pAddedBranchConList;

  /// Pointer to the linear solver used by the MetaSolver.
  /// @note MetaSolver does not derive from MPSolver.
  /// Instead, it owns an instance of an MPSolver that is used to
  /// actually solve a model
  MPSolver::SPtr pSolver;

  /// Result of the latest solving process
  lmcommon::ResultStatus pSolvingResult{lmcommon::ResultStatus::NOT_SOLVED};

  /// Save original problem variable bounds
  void saveOriginalProblemBounds();

  /// Recover original setting of this solver
  void recoverOriginalSettings();

  /// Save original priority for changing node selector
  void saveOriginalPriority();
};

}  // namespace metabb
}  // namespace optilab

