//
// Copyright OptiLab 2020. All rights reserved.
//
// Common definitions for Linear Model and Solver.
//

#pragma once

namespace optilab {
namespace toolbox {
namespace lmcommon {

/// Type of optimization solver used
/// as a back-end LP/IP/MIP solver
enum class OptimizationSolverPackageType : int {
  /// SCIP solver
  OSPT_SCIP = 0,

  /// MOCK solver used for unit testing
  OSPT_MOCK,
  OSPT_UNDEF
};

enum class ResultStatus : int {
  /// Optimal
  OPTIMAL = 0,
  /// Feasible, or stopped by limit
  FEASIBLE,
  /// Proven infeasible
  INFEASIBLE,
  /// Proven unbounded
  UNBOUNDED,
  /// Abnormal, i.e., error of some kind
  ABNORMAL,
  /// The model is trivially invalid (NaN coefficients, etc.)
  MODEL_INVALID,
  /// Not been solved yet.
  NOT_SOLVED
};

/// Possible basis status values for a variable and the slack
/// variable of a linear constraint.
enum class BasisStatus {
  FREE,
  AT_LOWER_BOUND,
  AT_UPPER_BOUND,
  FIXED_VALUE,
  BASIC
};

}// namespace lmcommon
}  // namespace toolbox
}  // namespace optilab
