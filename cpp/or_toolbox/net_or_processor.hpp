//
// Copyright OptiLab 2020. All rights reserved.
//
// Processor for distributed OR (MP) engines.
//

#pragma once

#include <memory>   // for std::shared_ptr
#include <string>
#include <map>
#include <utility>  // for std::pair
#include <vector>

#include "engine/optimizer_processor.hpp"
#include "model/model.hpp"
#include "optilab_protobuf/linear_model.pb.h"
#include "or_toolbox/net_or_processor_utilities.hpp"
#include "or_toolbox/or_utilities.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace toolbox {

class SYS_EXPORT_CLASS NetORProcessor :
public OptimizerProcessor<orengine::ORNetWork::SPtr, orengine::ORNetWork::SPtr> {
public:
  using SPtr = std::shared_ptr<NetORProcessor>;

public:
  NetORProcessor();

  ~NetORProcessor() override;

protected:
  using Work = orengine::ORNetWork::SPtr;
  using BaseProcessor = OptimizerProcessor<Work, Work>;

  /// Lock type on the internal mutex
  using LockGuard = boost::lock_guard<boost::mutex>;

protected:
  /// Processes the given work
  void processWork(Work work) override;

  /// Returns the processor collection of data structures
  inline const NetProcessorCollection& getProcessorCollection() const noexcept
  {
    return pProcCollection;
  }

private:
  /// Load a model as part of the root node
  void loadModel(Work& work);

  /// Initializes the distributed system
  void initNetSystem(Work& work);

  /// Initializes the root node
  void initNetRootSystem();

  /// Initializes a general non-root node
  void initNetNodeSystem();

  /// Runs the distributed framework.
  /// This runs either the load manager or the solvers,
  /// according to the network node type
  void runFramework(Work& work);

  /// Runs the root node
  void runNetRootSystem();

  /// Runs a general non-root node
  void runNetNodeSystem();

  /// Collects the solutions of a solved problem instance
  void collectSolutions(Work& work);

  /// Collection of metabb-net classes instances
  /// needed to run this processor
  NetProcessorCollection pProcCollection;
};

}  // namespace toolbox
}  // namespace optilab
