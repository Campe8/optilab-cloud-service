//
// Copyright OptiLab 2019. All rights reserved.
//
// Implementation of a thread-pool class.
//

#pragma once

#include <atomic>
#include <cstddef>    // for std::size_t
#include <memory>
#include <mutex>
#include <stdexcept>  // for std::runtime_error
#include <thread>

#include <boost/asio/io_service.hpp>
#include <boost/scoped_ptr.hpp>
#include <boost/thread.hpp>

#include <spdlog/spdlog.h>

#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS ThreadPool {
public:
  explicit ThreadPool(std::size_t numThreads);
  ~ThreadPool();

  template <typename JobHandler>
  void execute(const JobHandler& job)
  {
    LockGuard lock(pPoolStateMutex);
    if (pRunningPool)
    {
      // Use lambda to have the thread log its job creation and deletion
      auto work = [&, job]() {
        // Increase the counter of active jobs
        pActiveJobCount++;

        // Decrement the counter of queued jobs since the current job
        // is about to be executes
        pQueueJobCount--;

        try
        {
          // Execute the job
          job();
        }
        catch (...)
        {
          spdlog::error("ThreadPool - execute: exception while running a job");
        }
        // Done with work
        pActiveJobCount--;
      };

      // Increase the counter of queued jobs
      pQueueJobCount++;

      // Run work
      pIoService->post(work);
    }
    else
    {
      throw std::runtime_error("Executing a job on a disabled thread pool instance");
    }
  }

  /// Returns true if this pool is running, returns false otherwise
  inline bool isRunning() const { return pRunningPool; }

  /// Resets the pool.
  /// @note it cancels all work in progress
  void reset();

  /// Turn down the thread pool
  void shutdown();

  // Waits for all the tasks in the pool (active and queued) to be completed
  // (if timeoutMsec = -1) or until the given timeout in milliseconds.
  // While waiting, the pool cannot receive new tasks.
  // @note throws std::logic_error if the timeout is less than -1
  void backlogWait(int timeoutMsec);

  /// Returns the size in terms of number of tasks/jobs in the backlog
  inline int backlogSize() const { return getActiveJobCount() + getQueuedJobCount(); }

  /// Returns the number of threads in the thread pool
  inline std::size_t getNumThreads() const noexcept { return pNumThreads; }

  /// Returns the number of active jobs
  inline int getActiveJobCount() const noexcept { return pActiveJobCount.load(); }

  /// Returns the number of queued jobs
  inline int getQueuedJobCount() const noexcept { return pQueueJobCount.load(); }

private:
  using LockGuard = std::lock_guard<std::recursive_mutex>;

private:
  /// Number of threads in the thread pool
  const std::size_t pNumThreads;

  /// Flag indicating whether the pool is running or not
  std::atomic<bool> pRunningPool;

  /// Mutex to coordinate on the state of the thread pool
  std::recursive_mutex pPoolStateMutex;

  /// Number of jobs currently active
  std::atomic<int> pActiveJobCount{0};

  /// Number of jobs currently queued and not yet started
  std::atomic<int> pQueueJobCount{0};

  std::shared_ptr<boost::asio::io_service> pIoService;
  std::shared_ptr<boost::thread_group> pThreadPool;
  std::shared_ptr<boost::asio::io_service::work> pAsioWork;

  /// Performs the actual shutdown of the pool
  void performShutdown();
};

}  // namespace optilab
