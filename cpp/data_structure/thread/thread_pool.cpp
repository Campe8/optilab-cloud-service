#include "data_structure/thread/thread_pool.hpp"

#include <chrono>
#include <functional>  // for std::bind
#include <sstream>

#include <boost/bind.hpp>
#include <boost/chrono.hpp>
#include <boost/lexical_cast.hpp>

#include <spdlog/spdlog.h>

namespace {
constexpr int kThreadSleepingTime{50};
}  // namespace

namespace optilab {

ThreadPool::ThreadPool(std::size_t numThreads)
: pNumThreads(numThreads),
  pRunningPool(true),
  pIoService(std::make_shared<boost::asio::io_service>()),
  pThreadPool(std::make_shared<boost::thread_group>()),
  pAsioWork(std::make_shared<boost::asio::io_service::work>(*pIoService))
{
  LockGuard lock(pPoolStateMutex);
  for (std::size_t thrIdx = 0; thrIdx < pNumThreads; ++thrIdx)
  {
    pThreadPool->create_thread(boost::bind(&boost::asio::io_service::run, pIoService.get()));
  }
}

ThreadPool::~ThreadPool()
{
  shutdown();
}

void ThreadPool::backlogWait(int timeoutMsec)
{
  if (!pRunningPool) return;

  using boost::chrono::milliseconds;
  using TimePoint = boost::chrono::system_clock::time_point;

  if (timeoutMsec < -1)
  {
    std::ostringstream msg;
    msg << "wait - invalid timeout (msec): " << timeoutMsec;
    throw std::logic_error(msg.str());
  }

  // @note to print boost thread id for this thread, use the following:
  // spdlog::error("ThreadPool - backlogWait " +
  //              boost::lexical_cast<std::string>(boost::this_thread::get_id()));

  // Get the time to wait until based on the give timeout in msec
  const TimePoint waitOn = boost::chrono::system_clock::now() + milliseconds(timeoutMsec);
  {
    // Critical section.
    // @note acquire the mutex so no other thread can execute another job
    // Lock the queue to prevent waking up threads to do more work
    LockGuard lock(pPoolStateMutex);

    // Loop until all available threads are waiting on some work to do,
    // i.e., until all threads have completed their jobs
    while (backlogSize() > 0)
    {
      TimePoint waitUntil;
      waitUntil = boost::chrono::system_clock::now();

      // Return asap if waited already too long
      if (timeoutMsec != -1 && waitUntil >= waitOn)
      {
        return;
      }

      // Sleep a bit before checking again
      waitUntil += boost::chrono::milliseconds(kThreadSleepingTime);

      if (timeoutMsec != -1)
      {
        waitUntil = std::min(waitUntil, waitOn);
      }

      try
      {
        // @note the following can throw an exception of type
        // boost::thread_interrupted
        // if this thread was interrupted for some reasons (e.g., on shutdown).
        // In this case, return asap since there is no backlog to wait on
        boost::this_thread::sleep_until(waitUntil);
      }
      catch(boost::thread_interrupted&)
      {
        return;
      }
      catch(...)
      {
        spdlog::error("ThreadPool - backlogWait: undefined error, return");
      }
    }

    // Exit critical section and release the mutex
  }
}  // backlogWait

void ThreadPool::reset()
{
  LockGuard lock(pPoolStateMutex);
  performShutdown();

  // Restart
  pThreadPool.reset(new boost::thread_group());
  pIoService.reset(new boost::asio::io_service());
  pAsioWork.reset(new boost::asio::io_service::work(*pIoService));
  for (std::size_t thrIdx = 0; thrIdx < pNumThreads; ++thrIdx)
  {
    pThreadPool->create_thread(boost::bind(&boost::asio::io_service::run, pIoService.get()));
  }

  pRunningPool = true;
}  // reset

void ThreadPool::shutdown()
{
  LockGuard lock(pPoolStateMutex);
  performShutdown();
}  // shutdown

// The caller of doShutdown() must hold the mPoolMutex
void ThreadPool::performShutdown()
{
  if (!pRunningPool) return;

  pRunningPool = false;

  // Try to stop in-progress threads
  pThreadPool->interrupt_all();

  // Tell the service to start shutting down
  pIoService->stop();

  // Wait until all the threads are done
  pThreadPool->join_all();

  // Reset all pointers
  pThreadPool.reset();
  pAsioWork.reset();
  pIoService.reset();
}  // performShutdown

}  // namespace optilab
