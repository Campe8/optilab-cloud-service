//
// Copyright OptiLab 2019. All rights reserved.
//
// Metric register.
// Implements a register of metrics.
//

#pragma once

#include <cstdint>    // int64_t
#include <memory>     // for std::shared_ptr
#include <stdexcept>  // for std::runtime_error
#include <string>
#include <map>

#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS MetricsRegister {
 public:
  using SPtr = std::shared_ptr<MetricsRegister>;
  using metric_name = std::string;
  using metric_value = int64_t;

  /// Register-map of metrics as pairs <metric_name, metric_value>.
  /// @note using map to have a sorted iterator based on the name of the metrics
  using MetRegister = std::map<metric_name, metric_value>;
  using iter = MetRegister::iterator;
  using citer = MetRegister::const_iterator;

public:
  /// Adds the given metric with given value into the register.
  /// @note this overrides any metric with same name already present in the register
  inline void setMetric(const metric_name& name, metric_value val)
  {
    pMetricsRegister[name] = val;
  }

  /// Returns true if the register contains the given metric.
  /// Returns false otherwise
  inline bool hasMetric(const metric_name& name) const noexcept
  {
    return pMetricsRegister.find(name) != pMetricsRegister.end();
  }

  /// Returns the value of the given metric.
  /// @note throws std::runtime_error is no such metric is registered
  inline metric_value getMetric(const metric_name& name) const
  {
    if (!hasMetric(name))
    {
      throw std::runtime_error("MetricsRegister - getMetric: metric not found " + name);
    }
    return pMetricsRegister.at(name);
  }

  /// Iterators
  iter begin() { return pMetricsRegister.begin(); }
  iter end() { return pMetricsRegister.end(); }
  citer cbegin() const { return pMetricsRegister.cbegin(); }
  citer cend() const { return pMetricsRegister.cend(); }

private:
  MetRegister pMetricsRegister;
};

}  // namespace optilab
