#include "data_structure/json/json.hpp"

#include <cassert>
#include <stdexcept>  // for std::invalid_argument

#include "rapidjson/writer.h"

namespace optilab {

JSONValue::JSONValue()
: pObject(rapidjson::kArrayType),
  pDocument(nullptr)
{
}

std::string JSONValue::jsonObjectToString(const JSONValue::JsonObject& jsonObj)
{
  rapidjson::StringBuffer strbuf;
  rapidjson::Writer<rapidjson::StringBuffer> writer(strbuf);
  jsonObj.Accept(writer);

  return strbuf.GetString();
}  // jsonObjectToString

JSONValue::JsonObject& JSONValue::getContainedObject(JsonObject& obj, const std::string& objName)
{
  for (rapidjson::Value::MemberIterator itr = obj.MemberBegin(); itr != obj.MemberEnd(); ++itr)
  {
    std::string name = itr->name.GetString();
    if (name == objName)
    {
      return itr->value;
    }
  }
  throw std::runtime_error("JSONValue - object not found: " + objName);
}  // getContainedObject

std::string JSONValue::toString() const
{
  return jsonObjectToString(pObject);
}  // toString

JSONValue::JSONValue(const std::string& value, rapidjson::Document* document)
: pObject(),
  pDocument(document)
{
  assert(pDocument);
  pObject.SetString(value.c_str(), value.size(), document->GetAllocator());
}

void JSONValue::setDocument(rapidjson::Document* document)
{
  assert(document);
  pDocument = document;
}

void JSONValue::add(const std::string& value, const JSONValue::SPtr& val)
{
  assert(val);
  getObject().AddMember(JsonObject(value.c_str(), getAllocator()),
                        val->getObject(), getAllocator());
}  // add

JSONValueArray::JSONValueArray()
: JSONValue()
{
}

void JSONValueArray::add(const std::string& value, const JSONValue::SPtr& val)
{
  (void) value;
  (void) val;

  // Arrays are not supposed to use this method
  throw std::runtime_error("Array objects do not support add(...) method");
}  // add

JSON::JSON()
: pDocument(std::make_shared<rapidjson::Document>())
{
  pDocument->SetObject();
}

JSON::JSON(const std::string& json)
: pDocument(std::make_shared<rapidjson::Document>())
{
  if (pDocument->Parse(json.c_str()).HasParseError())
  {
    throw std::invalid_argument("The given string is not in JSON format: " + json);
  }
}

JSONValue::JsonObject& JSON::getObject(const std::string& key)
{
  JSONValue::JsonObject& result = (*pDocument)[key.c_str()];
  return result;
}// getObject

std::string JSON::toString()
{
  rapidjson::StringBuffer strbuf;
  rapidjson::Writer<rapidjson::StringBuffer> writer(strbuf);
  pDocument->Accept(writer);

  return strbuf.GetString();
}  // toString

template<>
JSONValue::SPtr JSON::createValue<std::string>(const std::string& value)
{
  auto ptr = std::make_shared<JSONValue>(value, pDocument.get());
  return ptr;
}  // createValue

}  // namespace optilab
