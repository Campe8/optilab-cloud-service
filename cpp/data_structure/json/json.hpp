//
// Copyright OptiLab 2019. All rights reserved.
//
// JSON class to encapsulate JSON objects.
//

#pragma once

#include <cstdint>  // for int64_t
#include <memory> // for std::shared_ptr
#include <string>

#include "rapidjson/document.h"
#include "rapidjson/stringbuffer.h"

#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS JSONValue {
 public:
  using SPtr = std::shared_ptr<JSONValue>;
  using JsonObject = rapidjson::Value;

 public:
  JSONValue();

  template <typename Class>
  JSONValue(const Class& value)
  : pObject(value),
    pDocument(nullptr)
  {
  }

  explicit JSONValue(const std::string& value, rapidjson::Document* document);

  virtual ~JSONValue() = default;

  /// Returns the Json string representation of the given Json object
  static std::string jsonObjectToString(const JSONValue::JsonObject& jsonObj);

  /// Returns the sub-object "objName" of the given object with given name.
  /// @note throws std::runtime_error if the object with given name is not present
  static JsonObject& getContainedObject(JsonObject& obj, const std::string& objName);

  /// Set the document this value belongs to
  void setDocument(rapidjson::Document* document);

  /// Adds a JSON value with given key
  virtual void add(const std::string& value, const JSONValue::SPtr& val);

  /// Returns the internal object
  inline JsonObject& getObject() { return pObject; }

  /// Returns the Json string representation of this value
  std::string toString() const;

  /// Utility function: returns the allocator of this object
  inline rapidjson::Document::AllocatorType& getAllocator() { return pDocument->GetAllocator(); }

 private:
  /// Actual Json object storing the value encapsulated by this instance
  JsonObject pObject;

  /// Document this JSON value belongs to
  rapidjson::Document* pDocument;
};

class SYS_EXPORT_CLASS JSONValueArray : public JSONValue {
 public:
  using SPtr = std::shared_ptr<JSONValueArray>;

 public:
  JSONValueArray();

  void add(const std::string& value, const JSONValue::SPtr& val) override;

  void pushBack(const JSONValue::SPtr& val)
  {
    getObject().PushBack(val->getObject(), getAllocator());
  }
};

class SYS_EXPORT_CLASS JSON {
 public:
  using SPtr = std::shared_ptr<JSON>;

  /// General JSON object
  using JsonValue = rapidjson::Value;
  using JsonValueSPtr = std::shared_ptr<JsonValue>;

 public:
  /// Default constructor, creates an empty json object
  JSON();

  /// Creates a new JSON object from the given string in JSON format.
  /// @throw std::invalid_argument if the given string is not a valid JSON string
  JSON(const std::string& json);

  /// Creates and returns a generic value
  inline JSONValue::SPtr createValue()
  {
    auto ptr = std::make_shared<JSONValue>(rapidjson::kObjectType);
    ptr->setDocument(pDocument.get());
    return ptr;
  }

  template<typename Type>
  JSONValue::SPtr createValue(const Type& value)
  {
    auto ptr = std::make_shared<JSONValue>(value);
    ptr->setDocument(pDocument.get());
    return ptr;
  }

  /// Creates and returns an array value
  inline JSONValueArray::SPtr createArray()
  {
    auto ptr = std::make_shared<JSONValueArray>();
    ptr->setDocument(pDocument.get());
    return ptr;
  }

  /// Returns true if this JSON object has and object with given key.
  /// Returns false otherwise.
  inline bool hasObject(const std::string& key)
  {
    return pDocument->HasMember(key.c_str());
  }

  inline void add(const std::string& name, const JSONValue::SPtr& value)
  {
    pDocument->AddMember(JsonValue(name.c_str(), getAllocator()),
                         value->getObject(), getAllocator());
  }

  /// Advance usage: returns this object's document
  inline rapidjson::Document& getDocument() { return *pDocument; }

  /// Returns the object with given key
  JSONValue::JsonObject& getObject(const std::string& key);

  /// Serializes this JSON object into a string
  std::string toString();

 private:
  /// Document representing the JSON object
  std::shared_ptr<rapidjson::Document> pDocument;

  /// Utility function: returns the allocator for this root object
  inline rapidjson::Document::AllocatorType& getAllocator() { return pDocument->GetAllocator(); }
};

template<>
JSONValue::SPtr JSON::createValue<std::string>(const std::string& value);

}  // namespace optilab
