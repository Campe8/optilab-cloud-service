//
// Copyright OptiLab 2019. All rights reserved.
//
// Class holding information about the network configuration.
//

#pragma once

#include <string>
#include <unordered_map>
#include <vector>

#include "data_structure/json/json.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

struct MetaBBNetworkConfig {
  int connectTimeoutMsec{-1};
  int connectSynchNetworkWaitTimeMsec{2000};
  int ptpSend{-1};
  int ptpRecv{-1};
  int bcastSend{-1};
  int bcastRecv{-1};
};

class SYS_EXPORT_CLASS SystemNetworkConfig {
 public:

  /// Singleton class shared among all components and holding the values
  /// for the network configuration.
  /// @note this object is initialized with the default configuration file
  /// "NETWORK_CONFIG_FILE_PATH".
  /// For more information, see "config_constants.hpp"
  static SystemNetworkConfig& getInstance();

  ~SystemNetworkConfig() = default;

  /// Returns the list of ports of the given service component.
  /// For more information, see "config_constants.hpp"
  const std::vector<int>& getPortList(const std::string& serviceComponent) const;

  /// Returns the end-point used to accept websocket connections to the server
  inline const std::string& getFrontendServerEbSocketEndpoint() const { return pWebSocketEndpoint; }

  /// Returns the number of threads used by the front-end server to respond to
  /// websockets connections
  inline int getFrontendServerNumThreads() const { return pFrontEndServerNumThreads; }

  /// Returns the number of workers for the back-end service
  inline int getBackendServiceNumWorkers() const { return pNumBackendServiceWorkers; }

  /// Returns the MetaBB OptNet config
  inline MetaBBNetworkConfig& getMetaBBNetworkConfig() noexcept
  {
    return pMetaBBNetworkConfig;
  }

  /// Returns the const MetaBB OptNet config
  inline const MetaBBNetworkConfig& getMetaBBNetworkConfig() const noexcept
  {
    return pMetaBBNetworkConfig;
  }

 private:
  /// Private constructor as per singleton pattern
  SystemNetworkConfig(const std::string& configFilePath);

  /// Delete copy constructor and assignment operator as per singleton pattern
  SystemNetworkConfig(const SystemNetworkConfig&) = delete;
  SystemNetworkConfig& operator=(const SystemNetworkConfig&) = delete;

 private:
  using PortList = std::vector<int>;

 private:
  /// Map between service component names and list of ports
  std::unordered_map<std::string, PortList> pPortListMap;

  /// Websocket endpoint
  std::string pWebSocketEndpoint;

  /// Number of threads used by the server to accept websocket connections
  int pFrontEndServerNumThreads{1};

  /// Number of (sub) workers
  int pNumBackendServiceWorkers{1};

  /// MetaBB OptNet config
  MetaBBNetworkConfig pMetaBBNetworkConfig;

  /// Parses the input configuration file and initializes this object
  void parseNetworkConfig(const std::string& configPath);

  void addPortList(const std::string& serviceName, JSON& jsonConfig);
};

}  // end namespace optilab
