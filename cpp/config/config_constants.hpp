//
// Copyright OptiLab 2019. All rights reserved.
//
// Constants for config.
//

#pragma once

#include "system/system_export_defs.hpp"

namespace optilab {

/// General configuration constants
namespace configconsts {
extern const char BACKEND_SERVICE_BROKER[];
extern const char BACKEND_SERVICE_RUNNER[];
extern const char BACKEND_SERVICE_WORKER[] SYS_EXPORT_VAR;
extern const char BACKEND_SERVICE_WORKER_NUM_WORKERS[];
extern const char BCAST_RECV[] SYS_EXPORT_VAR;
extern const char BCAST_SEND[] SYS_EXPORT_VAR;
extern const char FRONT_END_BROKER[] SYS_EXPORT_VAR;
extern const char FRONT_END_CLIENT[];
extern const char FRONT_END_SERVER[] SYS_EXPORT_VAR;
extern const char FRONT_END_SERVER_NUM_THREADS[];
extern const char FRONT_END_SERVER_WEBSOCKET_ENDPOINT_NAME[];
extern const char FRONT_END_SERVICE[] SYS_EXPORT_VAR;
extern const char META_BB_CONFIG[] SYS_EXPORT_VAR;
extern const char META_BB_PORT_CONFIG[] SYS_EXPORT_VAR;
extern const char NETWORK_CONFIG_FILE_PATH[];
extern const char PORT_LIST[];
extern const char PTP_RECV[] SYS_EXPORT_VAR;
extern const char PTP_SEND[] SYS_EXPORT_VAR;
extern const char TIMEOUT_ON_CONNECTION[] SYS_EXPORT_VAR;
extern const char WAIT_TO_SYNCH_NETWORK_MSEC[] SYS_EXPORT_VAR;
}  // namespace configconsts

}  // namespace optilab
