#include "config/config_constants.hpp"

namespace optilab {

namespace configconsts {
const char BACKEND_SERVICE_BROKER[] = "backend_service_broker";
const char BACKEND_SERVICE_RUNNER[] = "backend_service_runner";
const char BACKEND_SERVICE_WORKER[] = "backend_service_worker";
const char BACKEND_SERVICE_WORKER_NUM_WORKERS[] = "num_workers";
const char BCAST_RECV[] = "bcast_recv";
const char BCAST_SEND[] = "bcast_send";
const char FRONT_END_BROKER[] = "front_end_broker";
const char FRONT_END_CLIENT[] = "front_end_client";
const char FRONT_END_SERVER[] = "front_end_server";
const char FRONT_END_SERVER_NUM_THREADS[] = "server_num_threads";
const char FRONT_END_SERVER_WEBSOCKET_ENDPOINT_NAME[] = "ws_end_point";
const char FRONT_END_SERVICE[] = "front_end_service";
const char NETWORK_CONFIG_FILE_PATH[] = "config/network_config.json";
const char META_BB_CONFIG[] = "meta_bb_config";
const char META_BB_PORT_CONFIG[] = "meta_bb_port_config";
const char PORT_LIST[] = "port_list";
const char PTP_RECV[] = "ptp_recv";
const char PTP_SEND[] = "ptp_send";
const char TIMEOUT_ON_CONNECTION[] = "timeout_on_connection";
const char WAIT_TO_SYNCH_NETWORK_MSEC[] = "wait_to_synch_network_msec";
}  // namespace configconsts

}  // namespace optilab
