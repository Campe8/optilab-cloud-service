#include "config/system_network_config.hpp"

#include <fstream>    // for std::ifstream
#include <stdexcept>  // for std::runtime_error
#include <streambuf>  // for std::istreambuf_iterator

#include "config/config_constants.hpp"

#include <spdlog/spdlog.h>

namespace optilab {

SystemNetworkConfig& SystemNetworkConfig::getInstance()
{
  static SystemNetworkConfig instance(configconsts::NETWORK_CONFIG_FILE_PATH);
  return instance;
}  // getInstance
  
SystemNetworkConfig::SystemNetworkConfig(const std::string& configFilePath)
{
  parseNetworkConfig(configFilePath);
}

const std::vector<int>& SystemNetworkConfig::getPortList(const std::string& serviceComponent) const
{
  if (pPortListMap.find(serviceComponent) == pPortListMap.end())
  {
    throw std::runtime_error("SystemNetworkConfig - service component not found: " +
                             serviceComponent);
  }
  return pPortListMap.at(serviceComponent);
}  // getPortList

void SystemNetworkConfig::parseNetworkConfig(const std::string& configPath)
{

  // Open config file and store the Json object into a string
  std::ifstream fileStream(configPath);
  if (!fileStream.is_open())
  {
    const std::string errMsg = "SystemNetworkConfig - cannot open file " + configPath;
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }
  std::string jsonString((std::istreambuf_iterator<char>(fileStream)),
                         std::istreambuf_iterator<char>());

  // Close the file
  fileStream.close();

  // Parse the input config and store the value
  JSON jsonConfig(jsonString);

  // Add the list of ports
  addPortList(configconsts::BACKEND_SERVICE_BROKER, jsonConfig);
  addPortList(configconsts::BACKEND_SERVICE_RUNNER, jsonConfig);
  addPortList(configconsts::BACKEND_SERVICE_WORKER, jsonConfig);
  addPortList(configconsts::FRONT_END_BROKER, jsonConfig);
  addPortList(configconsts::FRONT_END_CLIENT, jsonConfig);
  addPortList(configconsts::FRONT_END_SERVER, jsonConfig);
  addPortList(configconsts::FRONT_END_SERVICE, jsonConfig);

  // Add number of workers for the back-end service
  auto& backendWorkerConfig = jsonConfig.getObject(configconsts::BACKEND_SERVICE_WORKER);
  pNumBackendServiceWorkers = JSONValue::getContainedObject(
      backendWorkerConfig, configconsts::BACKEND_SERVICE_WORKER_NUM_WORKERS).GetInt();

  // Add information for the front-end server
  auto& serverConfig = jsonConfig.getObject(configconsts::FRONT_END_SERVER);
  pWebSocketEndpoint = JSONValue::getContainedObject(
      serverConfig, configconsts::FRONT_END_SERVER_WEBSOCKET_ENDPOINT_NAME).GetString();
  pFrontEndServerNumThreads = JSONValue::getContainedObject(
      serverConfig, configconsts::FRONT_END_SERVER_NUM_THREADS).GetInt();

  // Add information for the meta-bb optnet-based framework
  auto& metabbConfig = jsonConfig.getObject(configconsts::META_BB_CONFIG);
  pMetaBBNetworkConfig.connectTimeoutMsec = JSONValue::getContainedObject(
          metabbConfig, configconsts::TIMEOUT_ON_CONNECTION).GetInt();
  pMetaBBNetworkConfig.connectSynchNetworkWaitTimeMsec = JSONValue::getContainedObject(
          metabbConfig, configconsts::WAIT_TO_SYNCH_NETWORK_MSEC).GetInt();

  auto& metabbPortConfig = jsonConfig.getObject(configconsts::META_BB_PORT_CONFIG);
  pMetaBBNetworkConfig.ptpSend = JSONValue::getContainedObject(
          metabbPortConfig, configconsts::PTP_SEND).GetInt();
  pMetaBBNetworkConfig.ptpRecv = JSONValue::getContainedObject(
          metabbPortConfig, configconsts::PTP_RECV).GetInt();
  pMetaBBNetworkConfig.bcastSend = JSONValue::getContainedObject(
          metabbPortConfig, configconsts::BCAST_SEND).GetInt();
  pMetaBBNetworkConfig.bcastRecv = JSONValue::getContainedObject(
          metabbPortConfig, configconsts::BCAST_RECV).GetInt();
}  // parseNetworkConfig

void SystemNetworkConfig::addPortList(const std::string& serviceName, JSON& jsonConfig)
{
  if (!jsonConfig.hasObject(serviceName))
  {
    throw std::runtime_error("SystemNetworkConfig - missing field in json config: " + serviceName);
  }

  auto& config = jsonConfig.getObject(serviceName);
  if (!config.HasMember(configconsts::PORT_LIST))
  {
    throw std::runtime_error("SystemNetworkConfig - missing field in json config: " +
                             std::string(configconsts::PORT_LIST));
  }

  auto& portList = pPortListMap[serviceName];
  for (JSONValue::JsonObject& iter :
      JSONValue::getContainedObject(config, configconsts::PORT_LIST).GetArray())
  {
    portList.push_back(iter.GetInt());
  }
}  // addPortList

}  // namespace optilab
