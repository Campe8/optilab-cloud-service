//
// Copyright OptiLab 2020. All rights reserved.
//
// Base class for a evolutionary strategy instance.
//

#pragma once

#include <memory>  // for std::unique_ptr

#include "optilab_protobuf/evolutionary_model.pb.h"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace toolbox {

class SYS_EXPORT_CLASS EvolutionaryInstance {
 public:
  using UPtr = std::unique_ptr<EvolutionaryInstance>;
  using SPtr = std::shared_ptr<EvolutionaryInstance>;

 public:
  EvolutionaryInstance(const toolbox::EvolutionaryModelProto& evolutionaryModel)
 : pEvolutionaryModel(evolutionaryModel)
 {
 }

  inline const EvolutionaryModelProto& getEvolutionaryodel() const noexcept
  {
    return pEvolutionaryModel;
  }

 private:
  /// Protobuf containing the evolutionary model
  EvolutionaryModelProto pEvolutionaryModel;
};

}  // namespace toolbox
}  // namespace optilab
