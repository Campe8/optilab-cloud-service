#include "evolutionary_toolbox/ga_processor.hpp"

#include <cassert>
#include <exception>
#include <functional>  // for std::bind
#include <stdexcept>   // for std::invalid_argument

#include <spdlog/spdlog.h>

#include "data_structure/metrics/metrics_register.hpp"
#include "engine/engine_constants.hpp"
#include "ga_engine/ga_custom_environment.hpp"
#include "ga_engine/ga_std_environment.hpp"
#include "utilities/timer.hpp"

namespace optilab {
namespace toolbox {

GAProcessor::GAProcessor(const std::string& procName, evolengine::EvolResult::SPtr result)
: BaseProcessor(procName),
  pGAEnvironment(nullptr),
  pGAOptimizer(nullptr),
  pLatencyInitMsec(0),
  pEvolutionLatencyMsec(0),
  pOptimizerLatencyMsec(0)
{
  pResult = std::dynamic_pointer_cast<evolengine::GeneticAlgorithmResult>(result);
  if (!pResult)
  {
    throw std::runtime_error("GAProcessor - empty pointer to result");
  }
}

GAProcessor::~GAProcessor()
{
  try
  {
    cleanup();
  }
  catch(...)
  {
    spdlog::error("GAProcessor - exception in destructor");
  }
}

void GAProcessor::loadEnvironmentAndCreateSolver(EvolutionaryInstance::UPtr instance)
{
  if (instance == nullptr)
  {
    throw std::invalid_argument("GAProcessor - loadEnvironmentAndCreateSolver: "
        "empty instance");
  }

  // Create the GA environment according to its specified type
  const auto& model = instance->getEvolutionaryodel();
  if (!model.has_genetic_model())
  {
    throw std::invalid_argument("GAProcessor - loadEnvironmentAndCreateSolver: "
        "model is not genetic");
  }

  const auto& env = model.genetic_model();
  auto envType = GAEnvironment::getEnvironmentTypeFromModel(env);
  switch(envType)
  {
    case GAEnvironment::GAEnvironmentType::GA_ENV_STD:
      pGAEnvironment = std::make_shared<GAStdEnvironment>();
      break;
    case GAEnvironment::GAEnvironmentType::GA_ENV_CUSTOM:
      pGAEnvironment = std::make_shared<GACustomEnvironment>();
      break;
    default:
    {
      const std::string errMsg = "GAProcessor - loadEnvironmentAndCreateSolver: "
          "invalid GA environment type";
      spdlog::error(errMsg);
      throw std::runtime_error(errMsg);
    }
  }

  // Load the environment
  try
  {
    pGAEnvironment->loadEnvironment(env);
  }
  catch(const std::exception& ex)
  {
    const std::string errMsg = "GAProcessor - loadEnvironmentAndCreateSolver: "
        "error initializing the environment: " + std::string(ex.what());
    spdlog::error(errMsg);
    throw;
  }
  catch(...)
  {
    const std::string errMsg = "GAProcessor - loadEnvironmentAndCreateSolver: "
        "error initializing the environment";
    spdlog::error(errMsg);
    throw;
  }

  // Build the optimizer
  try
  {
    pGAOptimizer = std::make_shared<OpenGAOptimizer>(pGAEnvironment);
  }
  catch(const std::exception& ex)
  {
    const std::string errMsg = "GAProcessor - loadEnvironmentAndCreateSolver: "
        "error in building the optimizer: " + std::string(ex.what());
    spdlog::error(errMsg);
    throw;
  }
  catch(...)
  {
    const std::string errMsg = "GAProcessor - loadEnvironmentAndCreateSolver: "
        "error in building the optimizer";
    spdlog::error(errMsg);
    throw;
  }

  pResult->modelName = getLoadedEnvironmentName();
}  // loadEnvironmentAndCreateSolver

bool GAProcessor::interruptSolver()
{
  if (!pGAOptimizer)
  {
    return false;
  }

  pGAOptimizer->interruptOptimizationProcess();
  return true;
}  // interruptSolver

const std::string& GAProcessor::getLoadedEnvironmentName() const
{
  if (!pGAEnvironment)
  {
    throw std::runtime_error("GAProcessor - getLoadedEnvironmentName: no environment loaded");
  }
  return pGAEnvironment->getEnvironmentName();
}  // getLoadedEnvironmentName

void GAProcessor::cleanup()
{
  // Call the destructor of the environment.
  // @note this removes all the dynamic libraries built to run this environment
  pGAEnvironment.reset();
}  // cleanup

void GAProcessor::processWork(Work work)
{
  if (work->workType == evolengine::EvolWork::kRunOptimizer)
  {
    runSolver(work);
  }
  else
  {
    spdlog::warn("GAProcessor - processWork: unrecognized work type");
  }
}  // processWork

void GAProcessor::runSolver(Work& work)
{
  if (!pGAEnvironment)
  {
    const std::string errMsg = "GAProcessor - runSolver: no environment loaded";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  if (!pGAOptimizer)
  {
    const std::string errMsg = "GAProcessor - runSolver: no optimizer available";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  timer::Timer timer;

  runSolverImpl();

  const auto wallclockMsec = timer.getWallClockTimeMsec();
  work->metricsRegister->setMetric(jsonmetrics::OPTIMIZER_LATENCY_MSEC, wallclockMsec);
}  // runSolver

void GAProcessor::runSolverImpl()
{
  // Start the timer
  timer::Timer optimizerTimer;

  // Initialize the search
  pGAOptimizer->initSearch();

  // Calculate initialization latency
  pLatencyInitMsec = optimizerTimer.getWallClockTimeMsec();

  try
  {
    // Run the optimizer
    pGAOptimizer->runOptimizer();
  }
  catch(std::exception& e)
  {
    spdlog::error("GAProcessor - runSolverImpl: "
        "runOptimizer exception: " + std::string(e.what()));

    // Cleanup state
    cleanup();

    // Re-throw
    throw;
  }
  catch (...)
  {
    spdlog::error("GAProcessor - runSolverImpl: runOptimizer unknown exception");

    // Cleanup state
    cleanup();

    // Re-throw
    throw;
  }

  // Get the overall wall-clock time
  pOptimizerLatencyMsec = optimizerTimer.getWallClockTimeMsec();

  // Calculate the optimization process latency
  pEvolutionLatencyMsec = pOptimizerLatencyMsec - pLatencyInitMsec;

  // Cleanup the search
  pGAOptimizer->cleanupSearch();

  // Store the solution
  storeSolution();
}  // runSolverImpl

evolengine::EvolResult::OptimizerStatus GAProcessor::getOptimizerStatus() const
{
  if (!pGAOptimizer)
  {
    throw std::runtime_error("GAProcessor - getOptimizerStatus: no optimizer loaded");
  }
  switch(pGAOptimizer->getStatus())
  {
    case gaengine::GAStatus::TIMEOUT:
      return evolengine::EvolResult::OptimizerStatus::OS_TIME;
    case gaengine::GAStatus::MAX_GENERATIONS:
      return evolengine::EvolResult::OptimizerStatus::OS_LIMIT_GENERATIONS;
    case gaengine::GAStatus::STALL_BEST:
      return evolengine::EvolResult::OptimizerStatus::OS_STALL_BEST;
    case gaengine::GAStatus::ERROR:
      return evolengine::EvolResult::OptimizerStatus::OS_ERROR;
    case gaengine::GAStatus::UNKNOWN:
    default:
      return evolengine::EvolResult::OptimizerStatus::OS_UNKNOWN;
  }
}  // getOptimizerStatus

void GAProcessor::storeSolution()
{
  // Lock mutex on critical section
  LockGuard lock(pResultMutex);

  // Retrieve the solution from the environment
  pResult->status = getOptimizerStatus();
  pResult->chromosomeId = pGAEnvironment->getChromosomeId();
  pResult->solution.first = pGAEnvironment->getObjectiveValue();
  pResult->solution.second = pGAEnvironment->getBestChromosomes();
}  // storeSolution

}  // namespace toolbox
}  // namespace optilab
