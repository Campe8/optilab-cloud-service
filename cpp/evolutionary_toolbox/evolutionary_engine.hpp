//
// Copyright OptiLab 2020. All rights reserved.
//
// Base class for a evolutionary search strategy engine.
//

#pragma once

#include "engine/engine.hpp"

#include <memory>  // for std::shared_ptr
#include <string>

#include "evolutionary_toolbox/evolutionary_instance.hpp"
#include "evolutionary_toolbox/evolutionary_utilities.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace toolbox {

class SYS_EXPORT_CLASS EvolutionaryEngine {
 public:
  using SPtr = std::shared_ptr<EvolutionaryEngine>;

 public:
  virtual ~EvolutionaryEngine() = default;

  /// Registers the given instance/environment the evolution should run on.
  /// @note this method does not run on the environment.
  /// @throw std::runtime_error if this model is called while the engine is running
  virtual void registerInstance(EvolutionaryInstance::UPtr instance) = 0;

  /// Notifies the optimizer on a given EngineEvent
  virtual void notifyEngine(const evolengine::EvolEvent& event) = 0;

  /// Wait for the current jobs in the queue to finish.
  /// @note if "timeoutSec" is -1, waits for all jobs in the queue to complete
  virtual void engineWait(int timeoutMsec) = 0;

  /// Shuts down the engine
  virtual void turnDown() = 0;
};

}  // namespace toolbox
}  // namespace optilab
