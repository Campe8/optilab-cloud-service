//
// Copyright OptiLab 2020. All rights reserved.
//
// A GA optimizer is an asynchronous engine that
// holds a pipeline containing GA processors.
//

#pragma once

#include <memory>  // for std::shared_ptr
#include <string>

#include "data_structure/metrics/metrics_register.hpp"
#include "engine/async_engine.hpp"
#include "engine/processor_pipeline.hpp"
#include "evolutionary_toolbox/ga_processor.hpp"
#include "evolutionary_toolbox/evolutionary_utilities.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace toolbox {

class SYS_EXPORT_CLASS GAOptimizer : public AsyncEngine<evolengine::EvolWork::SPtr> {
 public:
  using SPtr = std::shared_ptr<GAOptimizer>;

 public:
  GAOptimizer(const std::string& engineName, evolengine::EvolResult::SPtr result,
              MetricsRegister::SPtr metricsRegister);

  /// Initializes this optimizer with the given model
  void loadEnvironment(EvolutionaryInstance::UPtr instance);

  /// Runs this optimizer on the loaded model
  void runOptimizer();

  /// Interrupts the solving processes on this optimizer.
  /// Returns true if the solver was successfully interrupted.
  /// Returns false otherwise
  bool interruptOptimizer();

 protected:
  ProcessorPipeline::SPtr buildPipeline(int pipeline = 0) override;

 private:
  using Work = evolengine::EvolWork;
  using BaseClass = AsyncEngine<Work::SPtr>;

 private:
  /// Pointer to the GA results
  evolengine::EvolResult::SPtr pResult;

  /// Pointer to the instance of the processor in the pipeline
  GAProcessor::SPtr pGAProcessor;

  /// Pointer to the metrics register given to each work task processed
  /// by each processor
  MetricsRegister::SPtr pMetricsRegister;
};

}  // namespace toolbox
}  // namespace optilab
