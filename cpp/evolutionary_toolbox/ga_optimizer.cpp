#include "evolutionary_toolbox/ga_optimizer.hpp"

#include <cassert>
#include <stdexcept>  // for std::invalid_argument
#include <utility>    // for std::move

#include <spdlog/spdlog.h>

namespace optilab {
namespace toolbox {

GAOptimizer::GAOptimizer(const std::string& engineName, evolengine::EvolResult::SPtr result,
                         MetricsRegister::SPtr metricsRegister)
: BaseClass(engineName),
  pResult(result),
  pGAProcessor(nullptr),
  pMetricsRegister(metricsRegister)
{
  // Initialize the engine,
  // e.g., the internal pipeline
  BaseClass::init();

  if (!pResult)
  {
    throw std::invalid_argument("GAOptimizer - empty pointer to result");
  }

  if (!pMetricsRegister)
  {
    throw std::invalid_argument("GAOptimizer - empty pointer to the metrics register");
  }
}

void GAOptimizer::loadEnvironment(EvolutionaryInstance::UPtr instance)
{
  // Load the environment into the optimizer
  assert(pGAProcessor);
  pGAProcessor->loadEnvironmentAndCreateSolver(std::move(instance));
}  // loadEnvironment

void GAOptimizer::runOptimizer()
{
  if (!isActive())
  {
    const std::string errMsg = "GAOptimizer - runOptimizer: optimizer is not active";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  Work::SPtr work = std::make_shared<Work>(evolengine::EvolWork::WorkType::kRunOptimizer,
                                           pMetricsRegister);
  pushTask(work);
}  // runOptimizer

bool GAOptimizer::interruptOptimizer()
{
  assert(pGAProcessor);
  return pGAProcessor->interruptSolver();
}  // interruptOptimizer

ProcessorPipeline::SPtr GAOptimizer::buildPipeline(int)
{
  ProcessorPipeline::SPtr pipeline = std::make_shared<ProcessorPipeline>();
  pGAProcessor = std::make_shared<GAProcessor>(getEngineName(), pResult);
  pipeline->pushBackProcessor(pGAProcessor);
  return pipeline;
}  // buildPipeline

}  // namespace toolbox
}  // namespace optilab
