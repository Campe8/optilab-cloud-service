//
// Copyright OptiLab 2020. All rights reserved.
//
// Utilities for GA optimizers.
//

#pragma once

#include <algorithm>      // for std::sort
#include <cassert>
#include <functional>     // for std::function
#include <cstddef>        // for std::size_t
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <utility>        // for std::pair
#include <vector>

#include "data_structure/metrics/metrics_register.hpp"
#include "optilab_protobuf/evolutionary_model.pb.h"
#include "utilities/random_generators.hpp"

namespace optilab {
namespace toolbox {

namespace evolengine {

enum class EvolutionaryProcessorType : int {
  /// Processor for genetic algorithms
  EPT_GENETIC_ALGORITHM = 0,

  /// Processor for particle swarm optimization.
  /// See also
  /// https://en.wikipedia.org/wiki/Particle_swarm_optimization
  EPT_PARTICLE_SWARM,

  /// Processor for ant colony.
  /// See also
  /// https://en.wikipedia.org/wiki/Ant_colony_optimization_algorithms
  EPT_ANT_COLONY,

  /// Processor for bee algorithm.
  /// See also
  /// https://en.wikipedia.org/wiki/Bees_algorithm
  EPT_BEE_ALGORITHM,

  /// Undefined evolutionary processor type.
  /// @note this is the default if the processor type
  /// is not specified
  EPT_UNDEF
};


struct SYS_EXPORT_STRUCT EvolWork {
using SPtr = std::shared_ptr<EvolWork>;

enum WorkType {
  /// Runs the optimizer on the loaded model
  kRunOptimizer,
  kWorkTypeUndef
};

EvolWork(WorkType wtype, MetricsRegister::SPtr metReg)
: workType(wtype), metricsRegister(metReg)
{
}

WorkType workType;
MetricsRegister::SPtr metricsRegister;
};

class SYS_EXPORT_CLASS EvolEvent {
 public:
  enum EventType {
    /// Starts the solving process
    kRunEngine,
    /// Kills any ongoing computation
    kInterruptEngine,
    /// Collects solutions
    kSolutions
  };

 public:
  explicit EvolEvent(EventType aType)
  : pEventType(aType)
  {
  }

  inline EventType getType() const noexcept
  {
    return pEventType;
  }

  inline void setNumSolutions(int numSolutions) noexcept
  {
    pNumSolutions = numSolutions;
  }

  inline int getNumSolutions() const noexcept
  {
    return pNumSolutions;
  }

 private:
  /// Type of this event
  EventType pEventType;

  /// Number of solutions for "kSolutions" events
  int pNumSolutions{0};
};

struct SYS_EXPORT_STRUCT EvolResult {
  /// Status of the optimizer
  enum OptimizerStatus {
    /// Timeout occurred
    OS_TIME,

    /// Limit in search due to max generations reached
    OS_LIMIT_GENERATIONS,

    /// Limit in search due to stall in best objective
    OS_STALL_BEST,

    /// Other error
    OS_ERROR,

    /// Unknown
    OS_UNKNOWN
  };

  using SPtr = std::shared_ptr<EvolResult>;

  /// Name of the model
  std::string modelName;

  /// Status of the solution
  OptimizerStatus status{OptimizerStatus::OS_UNKNOWN};

  virtual ~EvolResult() = default;
};

struct SYS_EXPORT_STRUCT GeneticAlgorithmResult : public EvolResult {
  using SPtr = std::shared_ptr<GeneticAlgorithmResult>;

  /// A solution is a pair of:
  /// 1 - objective value; and
  /// 2 - chromosomes values
  using Solution = std::pair<double, std::vector<double>>;
  Solution solution;

  std::string chromosomeId;

  void clear()
  {
    solution.first = 0.0;
    solution.second.clear();
  }
};

}  // namespace evolengine

namespace evolutils {
/// Builds a protobuf object representing the Genetic Algorithm solution given as parameter
SYS_EXPORT_FCN EvolutionarySolutionProto buildGASolutionProto(
        evolengine::GeneticAlgorithmResult::SPtr res);
}  // namespace evolutils

}  // namespace toolbox
}  // namespace optilab
