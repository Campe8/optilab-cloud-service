#include "evolutionary_toolbox/evolutionary_utilities.hpp"

#include <stdexcept>  // for std::runtime_error

namespace optilab {
namespace toolbox {

namespace evolutils {

EvolutionarySolutionProto buildGASolutionProto(evolengine::GeneticAlgorithmResult::SPtr res)
{
  if (!res)
  {
    throw std::runtime_error("buildGASolutionProto - empty result");
  }

  EvolutionarySolutionProto sol;
  switch (res->status)
  {
    case evolengine::EvolResult::OptimizerStatus::OS_TIME:
    case evolengine::EvolResult::OptimizerStatus::OS_LIMIT_GENERATIONS:
    case evolengine::EvolResult::OptimizerStatus::OS_STALL_BEST:
      sol.set_status(OptimizerSolutionStatusProto::OPT_SOLVER_SUCCESS);
      break;
    case evolengine::EvolResult::OptimizerStatus::OS_ERROR:
      sol.set_status(OptimizerSolutionStatusProto::OPT_SOLVER_FAIL);
      break;
    default:
      sol.set_status(OptimizerSolutionStatusProto::OPT_SOLVER_UNKNOWN_STATUS);
      break;
    }

  auto gaSol = sol.mutable_genetic_algorithm_solution();
  gaSol->set_objective_value(res->solution.first);
  for (auto val : res->solution.second)
  {
    gaSol->add_chromosome_value(val);
  }

  return sol;
}  // buildGASolutionProto

}  // namespace evolutils

}  // namespace toolbox
}  // namespace optilab
