//
// Copyright OptiLab 2020. All rights reserved.
//
// Processor for Genetic Algorithm strategies.
//

#pragma once

#include <cstdint>  // for uint64_t
#include <memory>   // for std::shared_ptr
#include <string>
#include <map>
#include <utility>  // for std::pair
#include <vector>

#include <boost/thread.hpp>

#include "engine/optimizer_processor.hpp"
#include "evolutionary_toolbox/evolutionary_instance.hpp"
#include "evolutionary_toolbox/evolutionary_utilities.hpp"
#include "ga_engine/ga_environment.hpp"
#include "ga_engine/openga_optimizer.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace toolbox {

class SYS_EXPORT_CLASS GAProcessor :
public OptimizerProcessor<evolengine::EvolWork::SPtr, evolengine::EvolWork::SPtr>{
public:
  using SPtr = std::shared_ptr<GAProcessor>;

public:
  GAProcessor(const std::string& procName, evolengine::EvolResult::SPtr result);

  ~GAProcessor();

  /// Loads the genetic algorithm instance
  void loadEnvironmentAndCreateSolver(EvolutionaryInstance::UPtr instance);

  /// Interrupts the current engine execution (if any).
  /// The engine remains in a non-executable state until another environment is loaded.
  /// Returns true on success, false otherwise
  bool interruptSolver();

protected:
  using Work = evolengine::EvolWork::SPtr;
  using BaseProcessor = OptimizerProcessor<Work, Work>;

  /// Lock type on the internal mutex
  using LockGuard = boost::lock_guard<boost::mutex>;

protected:
  void processWork(Work work) override;

  /// Returns the internal genetic algorithm optimizer
  inline OpenGAOptimizer::SPtr getOptimizer() const noexcept { return pGAOptimizer; }

  /// Returns the pointer to the environment
  inline GAEnvironment::SPtr getEnvironment() const noexcept { return pGAEnvironment; }

private:
  /// GA result
  evolengine::GeneticAlgorithmResult::SPtr pResult;

  /// Environment to optimize
  GAEnvironment::SPtr pGAEnvironment;

  /// Pointer to the OpenGA solver/optimizer used to process GA environments
  OpenGAOptimizer::SPtr pGAOptimizer;

  /// Mutex synch.ing on the state of the result
  boost::mutex pResultMutex;

  /// Variable keeping track of the latency due to the initialization process in msec
  uint64_t pLatencyInitMsec;

  /// Variable keeping track of the optimization process latency in msec
  uint64_t pEvolutionLatencyMsec;

  /// Variable keeping track of the overall latency due to the optimization process in msec
  uint64_t pOptimizerLatencyMsec;

  /// Returns the status of the internal optimizer
  evolengine::EvolResult::OptimizerStatus getOptimizerStatus() const;

  /// Returns the name of the loaded environment
  const std::string& getLoadedEnvironmentName() const;

  /// Triggers the execution of the solver
  /// @note this doesn't check whether an engine is currently running or not.
  /// It is responsibility of the caller to handle that logic
  void runSolver(Work& work);
  void runSolverImpl();

  /// Utility function: store current solution
  void storeSolution();

  /// Cleanup state
  void cleanup();
};

}  // namespace toolbox
}  // namespace optilab
