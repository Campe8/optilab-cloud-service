//
// Copyright OptiLab 2019. All rights reserved.
//
// Optimizer frontend.
// Creates the server that receives incoming connection on listening websockets,
// and runs optimizers.
//

#pragma once

#include <memory>  // for std::shared_ptr
#include <mutex>
#include <string>
#include <unordered_map>

#include <simpleweb/server_ws.hpp>

#include "optimizer_frontend/frontend_runner.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS OptimizerFrontend {
public:
  using SPtr = std::shared_ptr<OptimizerFrontend>;

public:
  /// Constructor: creates a new optimizer front-end that can start a server listening on
  /// websockets at specified port and end-point. The server will use the specified number
  /// of threads to satisfy incoming connections and will connect to the specified broker port.
  /// @note the caller can specify the broker host names which, if not specified,
  /// is "localhost" by default.
  /// @note throws std::invalid_argument on invalid input arguments (e.g., invalid port numbers,
  /// empty end-point name, invalid number of threads)
  OptimizerFrontend(const int websocketPort, const std::string& websocketEndpointName,
                    const int brokerPort, const std::string& brokerHostName = std::string());

  /// Runs the front-end server. Returns zero on success, non-zero otherwise
  int run();

private:
  using WsServer = SimpleWeb::SocketServer<SimpleWeb::WS>;
  using ServerPtr = std::shared_ptr<WsServer>;
  using Endpoint = WsServer::Endpoint;
  using ConnectionPtr = std::shared_ptr<WsServer::Connection>;

  /// Map of front-end runners
  using RunnerMap = std::unordered_map<ConnectionPtr, FrontendRunner::SPtr>;

  /// Lock type on the internal mutex
  using LockGuard = std::lock_guard<std::recursive_mutex>;

private:
  /// Websocket port the server will listen on
  const int pWebSocketPort;

  /// Name of endpoint for the websocket
  const std::string pWebSocketEndpointName;

  /// Port used by the server to connect to the broker
  const int pBrokerPort;

  /// Name of the host where the broker is running
  const std::string pBrokerHostName;

  /// Pointer to the websocket server
  ServerPtr pServer;

  /// Map of service runners
  RunnerMap pRunnerMap;

  /// Mutex synchronizing on the internal map
  std::recursive_mutex pMutex;

  /// Utility function: returns the reference to the server
  inline WsServer& getServer() { return *pServer; }

  /// Utility function: initializes the server
  void initializeServer();

  /// Sets up the endpoing methods
  void setupEndpoint(Endpoint& endpoint);

  /// Creates a new service runner and maps it to the given connection.
  /// Returns zero on success, non-zero otherwise.
  /// @note throws std::runtime_error If a service runner for the given connection exists
  int addServiceRunner(ConnectionPtr connection, int brokerPort);

  /// Removes the service runner paired to the given connection
  void removeServiceRunner(ConnectionPtr connection);

  /// Handles incoming request by routing it to the correspondent service runner.
  /// @note throws std::runtime_error if there is no service runner paired to the given connection
  void routeMsgToServiceRunner(ConnectionPtr connection, const std::string& msg);
};

}  // namespace optilab
