#include "optimizer_frontend/frontend_constants.hpp"

namespace optilab {

namespace optimizerfrontend {

const int DEFAULT_SERVICE_RUNNER_CONTEXT_NUM_THREADS = 1;
const char HTTP_REST_DELETE[] = "DELETE";
const char HTTP_REST_ENDPOINT_DELETE_OPTIMIZER[] = "^/optimizer/(.*)$";
const char HTTP_REST_ENDPOINT_GET_OPTIMIZER[] = "^/optimizer$";
const char HTTP_REST_ENDPOINT_POST_MODEL[] = "^/model/(.*)$";
const char HTTP_REST_GET[] = "GET";
const char HTTP_REST_POST[] = "POST";
const char HTTP_REST_PUT[] = "PUT";
const char LOCAL_HOST_NAME[] = "localhost";
const int MAX_SERVICE_PORT_NUMBER = 65535;
const char OPTIMIZER_ID[] = "optimizer";
const char PREFIX_TCP_NETWORK_ADDR[] = "tcp://";
const char SERVICE_RUNNER_LOGOFF_MESSAGE[] = "logoff";

}  // optimizerfrontend

}  // namespace optilab
