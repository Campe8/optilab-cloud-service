//
// Copyright OptiLab 2019. All rights reserved.
//
// Implementation (logic) of the front-end runner.
//

#pragma once

#include <atomic>
#include <condition_variable>
#include <memory>   // for std::shared_ptr
#include <mutex>
#include <string>
#include <thread>
#include <unordered_set>

#include "zeromq/zhelpers.hpp"

#include "optimizer_service/socket_connector.hpp"
#include "system/system_export_defs.hpp"

// Forward declarations
namespace optilab {
  class FrontendRunner;
}  // namespace optilab

namespace optilab {

class SYS_EXPORT_CLASS FrontendRunnerImpl {
public:
  using SocketSPtr = std::shared_ptr<zmq::socket_t>;
  using SPtr = std::shared_ptr<FrontendRunnerImpl>;


public:
  /// Constructor: creates a new front-end runner (implementation) connecting to the broker
  /// on the given port and address.
  /// @note if the addresses are empty, "localhost" will be used.
  /// @note throws std::invalid_argument if the runnerId is empty
  /// @note throws std::invalid_argument if the ports are less than 1 or greater than
  /// optimizerservice::MAX_SERVICE_PORT_NUMBER
  FrontendRunnerImpl(const std::string& runnerId,
                     const int frontendPort, const std::string& frontendAddr = std::string());

  ~FrontendRunnerImpl();

  /// Registers the front-end runner that owns this runner implementation instance.
  /// @note throws std::invalid_argument if the given pointer is nullptr
  void registerFrontendRunner(FrontendRunner* frontendRunner);

  /// Runs the client: it connects to the broker
  int run();

  /// Parses and routes the given message to the broker.
  /// Returns zero on success, non-zero othewise
  int parseAndRouteMessageToBroker(const std::string& msg);

private:
  using ContextPtr = std::shared_ptr<zmq::context_t>;
  using SocketPtr = std::shared_ptr<zmq::socket_t>;

  using UniqueLock = std::unique_lock<std::mutex>;

private:
  /// Identifier of this runner
  const std::string pRunnerId;

  /// Port the client connects to
  const int pFrontendPort;

  /// Address the client connects to
  const std::string pFrontendAddr;

  /// Flag indicating an error from the bac-kend
  std::atomic<bool> pErrorResponse{false};

  /// Flag used to check whether the log-off was successful of not
  std::atomic<bool> pLogoffSuccess{false};

  /// Pointer to the instance of the front-end runner that owns this runner implementation
  FrontendRunner* pFrontendRunner{nullptr};

  /// Context used by the ZMQ socket
  ContextPtr pSocketContext{nullptr};

  /// ZMQ socket used to communicate to/from the broker
  SocketPtr pSocket{nullptr};

  /// Thread running the front-end reading process
  std::shared_ptr<std::thread> pMessageReader;

  /// Set of ids of messages that got an OK response from the service
  std::unordered_set<int> pOKReplaySet;

  /// Mutex used to synchronize requests/replies messages
  std::mutex pRequestResponseStateMutex;

  /// Condition variable used to synchronize requests/replies messages
  std::condition_variable pRequestResponseStateConditionVariable;

  /// Prepares the runner and internal state to start sending requests to the service
  void startupRunner();

  /// Cleans up the client and internal state
  void cleanupRunner();

  /// Handles all incoming messages from the socket
  void handleIncomingMessages();

  /// Handles a reply message
  void handleReplyMessage(const OptiLabReplyMessage& repMsg);

  /// Builds the request for the given input arguments and message and route it to the broker.
  /// Returns zero on success, non-zero otherwise.
  /// @note the request can require multiple messages sent to the broker
  int buildRequestAndRouteToBroker(const std::string& optimizerId,
                                   const std::string& frameworkType, const std::string& classType,
                                   const std::string& packageType, const std::string& msg);

  /// Builds an optimizer request and routes it to the broker.
  /// Returns zero on success, non-zero otherwise
  int buildRequestAndRouteToBroker(const std::string& optimizerId, const std::string& classType,
                                   const std::string& packageType, const std::string& msg,
                                   optilab::FrameworkType frameworkType);

  /// Builds a connectors request and routes it to the broker.
  /// Returns zero on success, non-zero otherwise
  int buildConnectorsRequestAndRouteToBroker(const std::string& optimizerId,
                                             const std::string& classType, const std::string& msg);

  /// Sends the given request to the service using the internal socket.
  /// Returns zero on success, non-zero otherwise
  int sendRequestMessageToService(const optilab::OptilabRequestMessage& req);
};

}  // namespace optilab
