//
// Copyright OptiLab 2019. All rights reserved.
//
// Optimizer front-end runner
// Runs the front-end service connecting clients to brokers.
// @note there is a 1-to-1 mapping between clients and front-end runners.
//

#pragma once

#include <condition_variable>
#include <memory>  // for std::shared_ptr
#include <mutex>
#include <string>

#include <simpleweb/server_ws.hpp>

#include "optimizer_frontend/frontend_runner_impl.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS FrontendRunner {
public:
  using WsServer = SimpleWeb::SocketServer<SimpleWeb::WS>;
  using SPtr = std::shared_ptr<FrontendRunner>;

public:
  /// Constructor.
  /// @note the runner identifier should be unique for each front-end runner
  /// @note throws std::invalid_argument if the runner identifier is empty.
  /// @note throws std::invalid_argument if the given pointer is empty
  FrontendRunner(const std::string& frontendRunnerId,
                 std::shared_ptr<WsServer::Connection> connection, int brokerPort);

  /// Constructor for synchronous runner.
  /// This constructor makes the runner synchronize on the given mutex and notify all threads
  /// waiting on the given condition variable once "onMessage(...)" is triggered.
  /// @note the runner identifier should be unique for each front-end runner
  /// @note throws std::invalid_argument if the runner identifier is empty.
  /// @note throws std::invalid_argument if the given pointer is empty
  FrontendRunner(const std::string& frontendRunnerId, int brokerPort,
                 std::shared_ptr<std::mutex> syncMutex,
                 std::shared_ptr<std::condition_variable> syncCondVar);

  /// Initialize this runner.
  /// @note throws std::runtime_error if initialization fails
  void initRunner();

  /// Callback for routing incoming messages from the websocket
  void onMessage(const std::string& msg);

  /// Returns the output message on synchronous runner
  const std::string& getOutMessage() const { return pOutMessage; }

  /// Returns the metrics message on synchronous runner
  const std::string& getMetricsMessage() const { return pMetricsMessage; }

private:
  friend class FrontendRunnerImpl;
  using UniqueLock = std::unique_lock<std::mutex>;

private:
  const std::string pServiceRunnerId;

  /// Port on local-host where the broker is listening at
  int pBrokerPort;

  /// Message output for synchronous runner
  std::string pOutMessage;

  /// Metrics message output for synchronous runner
  std::string pMetricsMessage;

  /// Connection instance this runner is paired to
  std::shared_ptr<WsServer::Connection> pConnection;

  /// Output synchronization mutex
  std::shared_ptr<std::mutex> pMutex;

  /// Output synchronization condition variable
  std::shared_ptr<std::condition_variable> pConditionVariable;

  /// Pointer to the implementation of the front-end runner
  FrontendRunnerImpl::SPtr pRunnerImpl;

  /// Sends the given result message to websocket using the the internal connection
  void sendMessageToConnection(const std::string& msg);

  /// Sends the given metrics message to websocket using the the internal connection
  void sendMetricsToConnection(const std::string& msg);

  /// Sends the given message to websocket using the the internal connection
  void sendToConnectionImpl(const std::string& msg);
};

}  // namespace optilab
