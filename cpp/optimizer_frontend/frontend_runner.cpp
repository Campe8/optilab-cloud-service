#include "optimizer_frontend/frontend_runner.hpp"

#include <exception>
#include <stdexcept>  // for std::invalid_argument

#include <spdlog/spdlog.h>

namespace {

constexpr int kNoError = 0;

}  // namespace

namespace optilab {

FrontendRunner::FrontendRunner(const std::string& frontendRunnerId,
                               std::shared_ptr<WsServer::Connection> connection, int brokerPort)
: pServiceRunnerId(frontendRunnerId),
  pBrokerPort(brokerPort),
  pConnection(connection),
  pMutex(nullptr),
  pConditionVariable(nullptr)
{
  if (frontendRunnerId.empty())
  {
    throw std::invalid_argument("FrontendRunner - empty runner identifier");
  }

  if (!pConnection)
  {
    throw std::invalid_argument("FrontendRunner - empty connection");
  }

  pRunnerImpl = std::make_shared<FrontendRunnerImpl>(pServiceRunnerId, pBrokerPort);

  // Register this runner into the impl. instance
  pRunnerImpl->registerFrontendRunner(this);
}

FrontendRunner::FrontendRunner(const std::string& frontendRunnerId, int brokerPort,
                               std::shared_ptr<std::mutex> syncMutex,
                               std::shared_ptr<std::condition_variable> syncCondVar)
: pServiceRunnerId(frontendRunnerId),
  pBrokerPort(brokerPort),
  pConnection(nullptr),
  pMutex(syncMutex),
  pConditionVariable(syncCondVar)
{
  if (frontendRunnerId.empty())
  {
    throw std::invalid_argument("FrontendRunner - empty runner identifier");
  }

  pRunnerImpl = std::make_shared<FrontendRunnerImpl>(pServiceRunnerId, pBrokerPort);

  // Register this runner into the impl. instance
  pRunnerImpl->registerFrontendRunner(this);
}

void FrontendRunner::initRunner()
{
  // Start the runner impl.
  pRunnerImpl->run();
}  // initRunner

void FrontendRunner::onMessage(const std::string& msg)
{
  // Re-route the message to the runner implementation
  int errVal;
  try
  {
    errVal = pRunnerImpl->parseAndRouteMessageToBroker(msg);
  }
  catch (std::exception& ex)
  {
    const std::string errMsg = "FrontendRunner - "
        "error in routing the request to the back-end: " + std::string(ex.what());
    spdlog::error(errMsg);
    errVal = 1;
  }
  catch (...)
  {
    const std::string errMsg = "FrontendRunner - "
        "error in routing the request to the back-end: undefined error";
    spdlog::error(errMsg);
    errVal = 1;
  }

  if (errVal != kNoError)
  {
    const std::string errMsg = "FrontendRunner - error on processing the message";
    spdlog::error(errMsg + ": " + msg);

    // Communicate back the error to the client
    sendMessageToConnection(errMsg);
  }
}  // onMessage

void FrontendRunner::sendMessageToConnection(const std::string& msg)
{
  if (msg.empty())
  {
    spdlog::warn("FrontendRunner - sendMessageToConnection: empty message send to the client");
  }

  if (!pMutex)
  {
    // @note connection->send is an asynchronous function
    sendToConnectionImpl(msg);
  }
  else
  {
    UniqueLock ulock(*pMutex);

    // Store the message
    pOutMessage = msg;

    // Notify all blocked threads that an output message is ready
    pConditionVariable->notify_all();
  }
}  // sendMessageToConnection

void FrontendRunner::sendMetricsToConnection(const std::string& msg)
{
  if (msg.empty())
  {
    spdlog::warn("FrontendRunner - sendMessageToConnection: empty message send to the client");
  }

  if (!pMutex)
  {
    // @note connection->send is an asynchronous function
    sendToConnectionImpl(msg);
  }
  else
  {
    UniqueLock ulock(*pMutex);

    // Store the message
    pMetricsMessage = msg;

    // Notify all blocked threads that an output message is ready
    pConditionVariable->notify_all();
  }
}  // sendMetricsToConnection

void FrontendRunner::sendToConnectionImpl(const std::string& msg)
{
  // connection->send is an asynchronous function
  pConnection->send(msg, [](const SimpleWeb::error_code &ec)
  {
    if(ec)
    {
      std::stringstream ss;
      ss << "OptimizerFrontend - server: error sending message. " <<
          "Error: " << ec << ", error message: " << ec.message();
      spdlog::error(ss.str());
    }
  });
}  // sendToConnectionImpl

}  // namespace optilab
