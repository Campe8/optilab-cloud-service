#include "optimizer_frontend/optimizer_frontend.hpp"

#include <exception>
#include <stdexcept>  // for std::invalid_argument
#include <sstream>
#include <thread>

#include <spdlog/spdlog.h>

#include "optimizer_frontend/frontend_constants.hpp"
#include "utilities/random_generators.hpp"

namespace {

/// Returns the regex used by the server to map endpoints given the endpoint name
std::string getRegexEndpoint(const std::string& ep)
{
  std::string regex;
  regex = "^/";
  regex += ep;
  regex += "/?$";
  return regex;
}  // getRegexEndpoint

constexpr int kNoError = 0;
constexpr int kRandomIdLen = 10;
}  // namespace

namespace optilab {

OptimizerFrontend::OptimizerFrontend(const int websocketPort,
                                     const std::string& websocketEndpointName, const int brokerPort,
                                     const std::string& brokerHostName)
: pWebSocketPort(websocketPort),
  pWebSocketEndpointName(websocketEndpointName),
  pBrokerPort(brokerPort),
  pBrokerHostName(brokerHostName.empty() ?
      std::string(optimizerfrontend::LOCAL_HOST_NAME) : brokerHostName),
  pServer(nullptr)
{
  if (pWebSocketPort < 1 || pWebSocketPort > optimizerfrontend::MAX_SERVICE_PORT_NUMBER)
  {
    throw std::invalid_argument("OptimizerFrontend - invalid websocket port number: "
        + std::to_string(pWebSocketPort));
  }

  if (pWebSocketEndpointName.empty())
  {
    throw std::invalid_argument("OptimizerFrontend - empty websocket endpoint name");
  }

  if (pBrokerPort < 1 || pBrokerPort > optimizerfrontend::MAX_SERVICE_PORT_NUMBER)
  {
    throw std::invalid_argument("OptimizerFrontend - invalid broker port number: "
        + std::to_string(pBrokerPort));
  }

  initializeServer();
}

void OptimizerFrontend::initializeServer()
{
  spdlog::info("OptimizerFrontend - initialization");

  // Create the server
  pServer = std::shared_ptr<WsServer>(new WsServer());

  // Set the websocket listening port
  getServer().config.port = pWebSocketPort;

  // Create
  auto& serviceEP = getServer().endpoint[getRegexEndpoint(pWebSocketEndpointName)];

  // Setup the service end point
  setupEndpoint(serviceEP);
}  // initializeServer

int OptimizerFrontend::addServiceRunner(ConnectionPtr connection, int brokerPort)
{
  if (!connection)
  {
    const std::string errMsg = "OptimizerFrontend - addServiceRunner: connection is NULL";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }
  {
    // Critical section
    LockGuard lock(pMutex);

    if (pRunnerMap.find(connection) != pRunnerMap.end())
    {
      // Registering twice a service runner for the same connection throws an error
      std::stringstream ss;
      ss << "OptimizerFrontend - addServiceRunner: registering the same connection twice " <<
          connection;
      const std::string errMsg = ss.str();
      spdlog::error(errMsg);
      throw std::runtime_error(errMsg);
    }

    FrontendRunner::SPtr serviceRunner;
    try
    {
      // Create a new runner with a random identifier
      const auto randomId = utilsrandom::buildRandomAlphaNumString(kRandomIdLen);
      serviceRunner = std::make_shared<FrontendRunner>(randomId, connection, brokerPort);

      // Initialize the runner
      serviceRunner->initRunner();
    }
    catch (std::exception& ex)
    {
      std::stringstream ss;
      ss << "OptimizerFrontend - addServiceRunner: error creating and initializing "
          "the service runner " << ex.what() << " on connection " << connection;
      const std::string errMsg = ss.str();
      spdlog::error(errMsg);
      return kNoError + 1;
    }
    catch (...)
    {
      std::stringstream ss;
      ss << "OptimizerFrontend - addServiceRunner: error creating and initializing "
          "the service runner (undefined error) on connection " << connection;
      const std::string errMsg = ss.str();
      spdlog::error(errMsg);
      return kNoError + 1;
    }

    pRunnerMap[connection] = serviceRunner;

    std::stringstream ss;
    ss << "OptimizerFrontend - addServiceRunner: service runner created and paired "
        "with connection " << connection;
    const std::string msg = ss.str();
    spdlog::info(msg);

    return kNoError;
  }
}  // addServiceRunner

void OptimizerFrontend::removeServiceRunner(ConnectionPtr connection)
{
  if (!connection)
  {
    spdlog::warn("OptimizerFrontend - removeServiceRunner: connection is NULL. Returning.");
    return;
  }
  {
    // Critical section
    LockGuard lock(pMutex);

    if (pRunnerMap.find(connection) == pRunnerMap.end())
    {
      // Registering twice a service runner for the same connection throws an error
      std::stringstream ss;
      ss << "OptimizerFrontend - removeServiceRunner: connection not found " << connection;
      const std::string warnMsg = ss.str();
      spdlog::warn(warnMsg);
      return;
    }

    std::stringstream ss;
    ss << "OptimizerFrontend - removeServiceRunner: service runner removed "
        "on connection " << connection;
    const std::string msg = ss.str();

    pRunnerMap.erase(connection);

    spdlog::info(msg);
  }
}  // removeServiceRunner

void OptimizerFrontend::routeMsgToServiceRunner(ConnectionPtr connection, const std::string& msg)
{
  if (!connection)
  {
    const std::string errMsg = "OptimizerFrontend - routeMsgToServiceRunner: connection is NULL";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  if (msg.empty())
  {
    spdlog::warn("OptimizerFrontend - routeMsgToServiceRunner: empty message. Returning.");
    return;
  }

  FrontendRunner::SPtr serviceRunner;
  {
    // Critical section
    LockGuard lock(pMutex);

    auto it = pRunnerMap.find(connection);
    if (it == pRunnerMap.end())
    {
      // Registering twice a service runner for the same connection throws an error
      std::stringstream ss;
      ss << "OptimizerFrontend - routeMsgToServiceRunner: connection not found " << connection;
      const std::string errMsg = ss.str();
      spdlog::error(errMsg);
      throw std::runtime_error(errMsg);
    }

    serviceRunner = it->second;
  }

  // Route the message to the service runner
  serviceRunner->onMessage(msg);
}  // routeMsgToServiceRunner

void OptimizerFrontend::setupEndpoint(Endpoint& endpoint)
{
  // Open connection on end-point
  endpoint.on_open = [this](std::shared_ptr<WsServer::Connection> connection)
  {
    std::stringstream ss;
    ss << "OptimizerFrontend - server: opened connection " << connection.get();
    spdlog::info(ss.str());

    // Create a new service runner
    const int errVal = this->addServiceRunner(connection, this->pBrokerPort);
    if (errVal != kNoError)
    {
      std::stringstream ssClosure;
      ssClosure << "OptimizerFrontend - server: error while initializing the service runner "
          "for connection " << connection << ". Closing the connection.";
      spdlog::error(ss.str());
    }
  };

  // Close connection.
  // @note see RFC 6455 7.4.1. for status codes
  endpoint.on_close = [this](std::shared_ptr<WsServer::Connection> connection, int status,
      const std::string& reason)
  {
    std::stringstream ss;
    ss << "OptimizerFrontend - server: closed connection " << connection.get() <<
        " with status code " << status;
    if (!reason.empty())
    {
      ss << " reason: " << reason;
    }
    spdlog::info(ss.str());

    // Create a new service runner
    this->removeServiceRunner(connection);
  };

  // Handshake protocol.
  // @note can modify handshake response headers here if needed
  endpoint.on_handshake = [](std::shared_ptr<WsServer::Connection> /*connection*/,
      SimpleWeb::CaseInsensitiveMultimap& /*response_header*/)
  {
    // Upgrade to websocket
    return SimpleWeb::StatusCode::information_switching_protocols;
  };

  // Error handling and logging.
  // @note see http://www.boost.org/doc/libs/1_55_0/doc/html/boost_asio/reference.html
  // error Codes for error code meanings
  endpoint.on_error = [](std::shared_ptr<WsServer::Connection> connection,
      const SimpleWeb::error_code &ec)
  {
    std::stringstream ss;
    ss << "OptimizerFrontend - server: error in connection " << connection.get() << ". " <<
        "Error: " << ec << ", error message: " << ec.message();
    spdlog::error(ss.str());
  };

  // Handle incoming message
  endpoint.on_message = [this](std::shared_ptr<WsServer::Connection> connection,
      std::shared_ptr<WsServer::InMessage> in_message)
  {
    // Handle the incoming message
    this->routeMsgToServiceRunner(connection, in_message->string());
  };
}  // setupEndpoint

int OptimizerFrontend::run()
{
  // Start the server
  spdlog::info("OptimizerFrontend - start the server");
  pServer->start();
  return 0;
}  // run

}  // namespace optilab
