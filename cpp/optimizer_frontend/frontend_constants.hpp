//
// Copyright OptiLab 2019. All rights reserved.
//
// Constants for the optimizer frontend.
//

#pragma once

namespace optilab {

namespace optimizerfrontend {

extern const int DEFAULT_SERVICE_RUNNER_CONTEXT_NUM_THREADS;
extern const char HTTP_REST_DELETE[];
extern const char HTTP_REST_ENDPOINT_DELETE_OPTIMIZER[];
extern const char HTTP_REST_ENDPOINT_GET_OPTIMIZER[];
extern const char HTTP_REST_ENDPOINT_POST_MODEL[];
extern const char HTTP_REST_GET[];
extern const char HTTP_REST_POST[];
extern const char HTTP_REST_PUT[];
extern const char LOCAL_HOST_NAME[];
extern const int MAX_SERVICE_PORT_NUMBER;
extern const char OPTIMIZER_ID[];
extern const char PREFIX_TCP_NETWORK_ADDR[];
extern const char SERVICE_RUNNER_LOGOFF_MESSAGE[];

}  // optimizerfrontend

}  // namespace optilab
