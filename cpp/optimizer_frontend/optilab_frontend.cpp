//
// Copyright OptiLab 2019. All rights reserved.
//
// Entry point for the OptiLab client.
//

#include <getopt.h>

#include <exception>
#include <iostream>
#include <string>
#include <vector>

#include "config/config_constants.hpp"
#include "config/system_network_config.hpp"
#include "optimizer_frontend/optimizer_frontend.hpp"
#include "optimizer_frontend/optimizer_http_frontend.hpp"

namespace {

void printHelp(const std::string& programName) {
  std::cerr << "Usage: " << programName << " [options]"
      << std::endl
      << "options:" << std::endl
      << "  --rest|-r          Use REST APIs instead of websockets." << std::endl
      << "  --help|-h          Print this help message."
      << std::endl;
}  // printHelp

}  // namespace

int main(int argc, char* argv[]) {

  char optString[] = "hr";
  struct option longOptions[] =
  {
      { "rest", no_argument, NULL, 'r' },
      { "help", no_argument, NULL, 'h' },
      { 0, 0, 0, 0 }
  };

  // Parse options
  int opt;
  bool useRest = false;
  while (-1 != (opt = getopt_long(argc, argv, optString, longOptions, NULL)))
  {
    switch (opt)
    {
      case 'r':
        useRest = true;
        break;
      case 'h':
      default:
        printHelp(argv[0]);
        return 0;
    }
  }  // while

  // Get the port number to run the service on
  auto& config = optilab::SystemNetworkConfig::getInstance();
  auto& portList = config.getPortList(optilab::configconsts::FRONT_END_SERVER);
  if (portList.size() != 2)
  {
    std::cerr << "Invalid number of ports for the front-end server\n";
    return 1;
  }

  const int websocketConnectionPort = portList[0];
  const int brokerConnectionPort = portList[1];
  const std::string& serverWebSocketEndPoint = config.getFrontendServerEbSocketEndpoint();
  try
  {
    if (useRest)
    {
      // Create a new optimizer broker and run it
      optilab::OptimizerHttpFrontend optHttpFrontend(websocketConnectionPort, brokerConnectionPort);
      return optHttpFrontend.run();
    }
    else
    {
      // Create a new optimizer broker and run it
      optilab::OptimizerFrontend optFrontend(websocketConnectionPort, serverWebSocketEndPoint,
                                             brokerConnectionPort);
      return optFrontend.run();
    }
  }
  catch (const std::exception& e)
  {
    std::cerr << e.what() << std::endl;
    return 1;
  }
  catch (...)
  {
    std::cerr << "Undefined error" << std::endl;
    return 2;
  }

  return 0;
}
