#include "optimizer_frontend/frontend_runner_impl.hpp"

#include <cassert>
#include <exception>
#include <functional>  // for std::ref
#include <chrono>
#include <stdexcept>   // for std::invalid_argument

#include <spdlog/spdlog.h>

#include "data_structure/json/json.hpp"
#include "engine/engine_constants.hpp"
#include "model/model_constants.hpp"
#include "optimizer_frontend/frontend_constants.hpp"
#include "optimizer_frontend/frontend_runner.hpp"
#include "optimizer_frontend/frontend_utilities.hpp"
#include "optilab_protobuf/optilab.pb.h"
#include "optimizer_service/service_constants.hpp"

namespace {

#define CATCH_ERROR_AND_RETURN()         \
  catch (std::exception &ex)             \
  {                                      \
    const std::string errMsg = "FrontendRunnerImpl - buildORRequestAndRouteToBroker: " \
          "error while sending an optimizer request " + std::string(ex.what());        \
    spdlog::error(errMsg);               \
    return 1;                            \
  }                                      \
  catch (...)                            \
  {                                      \
    const std::string errMsg = "FrontendRunnerImpl - buildORRequestAndRouteToBroker: " \
        "undefined error while sending an optimizer request";     \
    spdlog::error(errMsg);               \
    return 1;                            \
  }


/// Returns the TCP address of the socket
std::string getSocketAddr(const int socketPort, const std::string& socketAddr)
{
  std::string addr = optilab::optimizerfrontend::PREFIX_TCP_NETWORK_ADDR;
  if (socketAddr.empty())
  {
    throw std::invalid_argument("Empty TCP address");

  }
  addr += socketAddr + ":";
  addr += std::to_string(socketPort);

  return addr;
}  // getSocketAddr

bool checkOKReply(const std::unordered_set<int>& idSet, int id)
{
  return idSet.find(id) != idSet.end();
}  // checkOKReply

constexpr int kCleanupWaitTimeSec = 2;
constexpr int kMessageId = 1000;
constexpr int kNoError = 0;
constexpr int kNumSolutions = 1;

}  // namespace

namespace optilab {

FrontendRunnerImpl::FrontendRunnerImpl(const std::string& runnerId, const int frontendPort,
                                       const std::string& frontendAddr)
: pRunnerId(runnerId),
  pFrontendPort(frontendPort),
  pFrontendAddr(frontendAddr.empty() ? optimizerfrontend::LOCAL_HOST_NAME : frontendAddr)
{
  if (pRunnerId.empty())
  {
    throw std::invalid_argument("FrontendRunnerImpl - "
        "empty service runner implementation identifier");
  }

  if (pFrontendPort < 1 || pFrontendPort > optimizerfrontend::MAX_SERVICE_PORT_NUMBER)
  {
    throw std::invalid_argument("FrontendRunnerImpl - invalid front-end port number");
  }
}

FrontendRunnerImpl::~FrontendRunnerImpl()
{
  // Perform cleanup
  cleanupRunner();

  // Cleanup sockets and context
  pSocket.reset();
  pSocketContext.reset();
}

void FrontendRunnerImpl::registerFrontendRunner(FrontendRunner* frontendRunner)
{
  if (!frontendRunner)
  {
    const std::string errMsg = "FrontendRunnerImpl - registerFrontendRunner: nullptr to "
        "front-end runner";
    spdlog::error(errMsg);
    throw std::invalid_argument(errMsg);
  }

  pFrontendRunner = frontendRunner;
}  // frontendRunner

int FrontendRunnerImpl::run()
{
  // Initialize the context for the client
  std::string clientConnectId;
  try
  {
    pSocketContext =
        std::make_shared<zmq::context_t>(
            optimizerfrontend::DEFAULT_SERVICE_RUNNER_CONTEXT_NUM_THREADS);
    pSocket = std::make_shared<zmq::socket_t>(*pSocketContext, ZMQ_DEALER);

    // Set client ID and connect
    clientConnectId = s_set_id(*pSocket);
    spdlog::info("FrontendRunnerImpl - create new client connection id " + clientConnectId);

    // Get the front-end/back-end addresses to bind to
    const std::string frontendAddr = getSocketAddr(pFrontendPort, pFrontendAddr);
    spdlog::info("FrontendRunnerImpl - client " + clientConnectId +
                 " connecting to address " + frontendAddr);
    pSocket->connect(frontendAddr.c_str());
  }
  catch (...)
  {
    throw;
  }

  // Perform startup
  startupRunner();

  return kNoError;
}  // run

void FrontendRunnerImpl::startupRunner()
{
  spdlog::info("FrontendRunnerImpl - start message reader");
  pMessageReader = std::make_shared<std::thread>(
      &FrontendRunnerImpl::handleIncomingMessages, this);
}  // startupRunner

void FrontendRunnerImpl::cleanupRunner()
{
  // Logoff from the broker
  spdlog::info("FrontendRunnerImpl - sending log off message");
  s_send(*pSocket, optimizerfrontend::SERVICE_RUNNER_LOGOFF_MESSAGE);

  // Wait some time to receive the log-off success message.
  // If not received, force log-off
  std::this_thread::sleep_for(std::chrono::seconds(kCleanupWaitTimeSec));

  if (!pLogoffSuccess)
  {
    // Force shutdown of the reader.
    // @note this detaches the thread which will throw since the (socket) context is destroyed.
    // The try-catch will terminate the thread
    spdlog::warn("Detach reader thread");
    pMessageReader->detach();
  }
  else
  {
    // Join the receiver thread
    pMessageReader->join();
  }
}  // cleanupClient

void FrontendRunnerImpl::handleIncomingMessages()
{
  // Add the reply message id to the set of OK replies.
  // @note this should happen only after being successfully logged into the service.
  // @note this unblocks the sender to send at least the first message
  {
    // Critical section
    UniqueLock lock(pRequestResponseStateMutex);
    pOKReplaySet.insert(kMessageId);
    pRequestResponseStateConditionVariable.notify_all();
  }

  bool runReceiverLoop = true;
  while (runReceiverLoop)
  {
    std::string reply;
    try
    {
      reply = s_recv(*pSocket);
    }
    catch (...)
    {
      spdlog::error("FrontendRunnerImpl - error reading from the socket");
      break;
    }

    OptiLabReplyMessage repMsg;
    repMsg.ParseFromString(reply);

    if (repMsg.replystatus() != optilab::ReplyStatus::OK)
    {
      spdlog::error("FrontendRunnerImpl - received error from a reply message");
      pErrorResponse = true;
      break;
    }

    {
      // Critical section
      UniqueLock lock(pRequestResponseStateMutex);
      pOKReplaySet.insert(kMessageId);
      pRequestResponseStateConditionVariable.notify_all();
    }

    const auto replyType = repMsg.type();
    switch (replyType)
    {
      case optilab::OptiLabReplyMessage::InformationalMessage:
      {
        // Do not perform any action
        break;
      }
      case optilab::OptiLabReplyMessage::ServiceLogoff:
      {
        ServiceLogoffRep logoffRep;
        repMsg.details().UnpackTo(&logoffRep);

        if (logoffRep.logoff() != ServiceLogoffRep::SUCCESS)
        {
          spdlog::error("FrontendRunnerImpl - failed log off");
        }

        pLogoffSuccess = true;
        runReceiverLoop = false;

        break;
      }
      default:
      {
        handleReplyMessage(repMsg);
        break;
      }
    }
  }  // while
}  // handleIncomingMessages

void FrontendRunnerImpl::handleReplyMessage(const OptiLabReplyMessage& repMsg)
{
  if (!pFrontendRunner)
  {
    const std::string errMsg = "FrontendRunnerImpl - front-end runner not register";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }
  const auto replyType = repMsg.type();
  switch (replyType)
  {
    case optilab::OptiLabReplyMessage::Solution:
    {
      OptimizerSolutionRep solRep;
      repMsg.details().UnpackTo(&solRep);

      const auto& jsonSolution = solRep.solution();
      pFrontendRunner->sendMessageToConnection(jsonSolution);
      break;
    }
    case optilab::OptiLabReplyMessage::Metrics:
    {
      OptimizerMetricsRep metRep;
      repMsg.details().UnpackTo(&metRep);

      const auto& jsonMetrics = metRep.metrics();
      pFrontendRunner->sendMetricsToConnection(jsonMetrics);
      break;
    }
    default:
    {
      std::string errMsg = "FrontendRunnerImpl - Unrecognized message reply type";
      spdlog::error(errMsg);
      throw std::runtime_error(errMsg);
    }
  }
}  // handleReplyMessage

int FrontendRunnerImpl::sendRequestMessageToService(const optilab::OptilabRequestMessage& req)
{
  assert(pSocket);

  // Error status
  int errorStatus = kNoError;

  // Serialize the message to send back to the client
  std::string reqMsg;
  try
  {
    reqMsg = req.SerializeAsString();
  }
  catch(...)
  {
    errorStatus = 1;
    return errorStatus;
  }

  // Before sending a request check if there is any error from some previous responses
  if (pErrorResponse)
  {
    spdlog::error("FrontendRunnerImpl - cannot send request to the client due to error in "
        " previous responses");
    errorStatus = 1;
    return errorStatus;
  }

  // Check if the runner received the OK response for the previous message, if so continue.
  // If not, put this thread on old until an OK response is received or a timeout is expired
  {
    UniqueLock lock(pRequestResponseStateMutex);
    if (!checkOKReply(pOKReplaySet, kMessageId))
    {
      pRequestResponseStateConditionVariable.wait(
          lock, [&]{ return checkOKReply(std::ref(pOKReplaySet), kMessageId); });
    }

    // Remove this message OK id
    pOKReplaySet.erase(kMessageId);
  }

  // Send the message back to the client
  s_send(*pSocket, reqMsg);

  return errorStatus;
}  // sendRequestMessageToService

int FrontendRunnerImpl::parseAndRouteMessageToBroker(const std::string& msg)
{
  int errorStatus = kNoError;

  // Parse the input as a Json object
  JSON::SPtr jsonMsg;
  try
  {
    jsonMsg = std::make_shared<JSON>(msg);
  }
  catch (...)
  {
    const std::string errMsg = "FrontendRunnerImpl - "
        "cannot parse the input message as a JSON message: " + msg;
    spdlog::error(errMsg);
    errorStatus = 1;
    return errorStatus;
  }

  // Use this runner id as optimizer id
  const std::string optimizerId = pRunnerId;

  // Get framework type
  if (!jsonMsg->hasObject(jsonmodel::FRAMEWORK))
  {
    const std::string& errMsg = "Model - Missing framework type specification in message";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }
  auto frameworkType = (jsonMsg->getObject(jsonmodel::FRAMEWORK)).GetString();


  // Get and set the framework the model should run in
  if (!jsonMsg->hasObject(jsonmodel::CLASS_TYPE))
  {
    const std::string& errMsg = "Model - Missing block type specification in message";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }
  auto classType = (jsonMsg->getObject(jsonmodel::CLASS_TYPE)).GetString();

  std::string packageType;
  if (jsonMsg->hasObject(jsonmodel::PACKAGE))
  {
    packageType = (jsonMsg->getObject(jsonmodel::PACKAGE)).GetString();
  }

  // Build the request to send to the broker and route it to the broker
  errorStatus = buildRequestAndRouteToBroker(optimizerId, frameworkType, classType,
                                             packageType, msg);

  return errorStatus;
}  // parseAndRouteMessageToBroker

int FrontendRunnerImpl::buildRequestAndRouteToBroker(const std::string& optimizerId,
                                                     const std::string& frameworkType,
                                                     const std::string& classType,
                                                     const std::string& packageType,
                                                     const std::string& msg)
{
  // The request is generally made as follows:
  // 1 - create an engine;
  // 2 - load the model;
  // 3 - run model;
  // 4 - collect solutions;
  // 5 - delete engine;
  // Between each request, send an engine wait request.
  int errorStatus = kNoError;
  if (frameworkType == engineconsts::ENGINE_FRAMEWORK_TYPE_OR)
  {
    errorStatus = buildRequestAndRouteToBroker(optimizerId, classType, packageType, msg,
                                               optilab::FrameworkType::OR);
  }
  else if (frameworkType == engineconsts::ENGINE_FRAMEWORK_TYPE_CONNECTORS)
  {
    errorStatus = buildConnectorsRequestAndRouteToBroker(optimizerId, classType, msg);
  }
  else if (frameworkType == engineconsts::ENGINE_FRAMEWORK_TYPE_EC)
  {
    errorStatus = buildRequestAndRouteToBroker(optimizerId, classType, packageType, msg,
                                               optilab::FrameworkType::EC);
  }
  else
  {
    const std::string errMsg = "FrontendRunnerImpl - framework not recognized: " + frameworkType;
    spdlog::error(errMsg);
    errorStatus = 1;
  }

  return errorStatus;
}  // buildRequestAndRouteToBroker

int FrontendRunnerImpl::buildRequestAndRouteToBroker(const std::string& optimizerId,
                                                     const std::string& classType,
                                                     const std::string& packageType,
                                                     const std::string& msg,
                                                     optilab::FrameworkType frameworkType)
{
  int errorStatus = kNoError;
  optilab::OptilabRequestMessage req;

  // Build a create engine request
  try
  {
    req = frontendserviceutils::buildCreateOptimizerRequest(optimizerId, classType, packageType,
                                                            frameworkType);
    errorStatus = sendRequestMessageToService(req);
    if (errorStatus != 0)
    {
      spdlog::error("Engine creation request failed");
      return errorStatus;
    }
  }
  CATCH_ERROR_AND_RETURN()

  // Wait for the engine to be built
  try
  {
    // @note wait on -1, i.e., until the back-end is done building the optimizer
    req = frontendserviceutils::buildOptimizerWaitRequest(optimizerId, packageType, -1,
                                                          frameworkType);
    errorStatus = sendRequestMessageToService(req);
    if (errorStatus != 0)
    {
      spdlog::error("Engine wait request failed");
      return errorStatus;
    }
  }
  CATCH_ERROR_AND_RETURN()

  // Load the model
  try
  {
    // @note wait on -1, i.e., until the back-end is done building the optimizer
    req = frontendserviceutils::buildLoadModelRequest(optimizerId, packageType, msg, frameworkType);
    errorStatus = sendRequestMessageToService(req);
    if (errorStatus != 0)
    {
      spdlog::error("Engine load model request failed");
      return errorStatus;
    }
  }
  CATCH_ERROR_AND_RETURN()

  // Wait for the model to be loaded
  try
  {
    // @note wait on -1, i.e., until the back-end is done building the optimizer
    req = frontendserviceutils::buildOptimizerWaitRequest(optimizerId, packageType, -1,
                                                          frameworkType);
    errorStatus = sendRequestMessageToService(req);
    if (errorStatus != 0)
    {
      spdlog::error("Engine wait request failed");
      return errorStatus;
    }
  }
  CATCH_ERROR_AND_RETURN()

  // Run the model
  try
  {
    // @note wait on -1, i.e., until the back-end is done building the optimizer
    req = frontendserviceutils::buildRunModelRequest(optimizerId, packageType, frameworkType);
    errorStatus = sendRequestMessageToService(req);
    if (errorStatus != 0)
    {
      spdlog::error("Engine run model request failed");
      return errorStatus;
    }
  }
  CATCH_ERROR_AND_RETURN()

  // Wait for the model to be model to be run
  try
  {
    // @note wait on -1, i.e., until the back-end is done building the optimizer
    req = frontendserviceutils::buildOptimizerWaitRequest(optimizerId, packageType, -1,
                                                          frameworkType);
    errorStatus = sendRequestMessageToService(req);
    if (errorStatus != 0)
    {
      spdlog::error("Engine wait request failed");
      return errorStatus;
    }
  }
  CATCH_ERROR_AND_RETURN()

  // Get the solutions
  try
  {
    // @note wait on -1, i.e., until the back-end is done building the optimizer
    req = frontendserviceutils::buildModelSolutionsRequest(optimizerId, packageType, kNumSolutions,
                                                           frameworkType);
    errorStatus = sendRequestMessageToService(req);
    if (errorStatus != 0)
    {
      spdlog::error("Engine run model request failed");
      return errorStatus;
    }
  }
  CATCH_ERROR_AND_RETURN()

  // Wait for the model to send the solutions
  try
  {
    // @note wait on -1, i.e., until the back-end is done building the optimizer
    req = frontendserviceutils::buildOptimizerWaitRequest(optimizerId, packageType, -1,
                                                          frameworkType);
    errorStatus = sendRequestMessageToService(req);
    if (errorStatus != 0)
    {
      spdlog::error("Engine wait request failed");
      return errorStatus;
    }
  }
  CATCH_ERROR_AND_RETURN()

  // Delete the optimizer
  try
  {
    // @note wait on -1, i.e., until the back-end is done building the optimizer
    req = frontendserviceutils::buildDeleteOptimizerRequest(optimizerId, packageType, frameworkType);
    errorStatus = sendRequestMessageToService(req);
    if (errorStatus != 0)
    {
      spdlog::error("Engine run model request failed");
      return errorStatus;
    }
  }
  CATCH_ERROR_AND_RETURN()

  return errorStatus;
}  // buildRequestAndRouteToBroker

int FrontendRunnerImpl::buildConnectorsRequestAndRouteToBroker(const std::string& optimizerId,
                                                               const std::string& classType,
                                                               const std::string& msg)
{
  (void)classType;

  int errorStatus = kNoError;
  optilab::OptilabRequestMessage req;

  // Build a create connector request
  try
  {
    req = frontendserviceutils::buildCreateConnectorRequest(optimizerId);
    errorStatus = sendRequestMessageToService(req);
    if (errorStatus != 0)
    {
      spdlog::error("Engine creation request failed");
      return errorStatus;
    }
  }
  CATCH_ERROR_AND_RETURN()

  // Wait for the engine to be built
  try
  {
    // @note wait on -1, i.e., until the back-end is done building the optimizer
    req = frontendserviceutils::buildConnectorWaitRequest(optimizerId);
    errorStatus = sendRequestMessageToService(req);
    if (errorStatus != 0)
    {
      spdlog::error("Engine wait request failed");
      return errorStatus;
    }
  }
  CATCH_ERROR_AND_RETURN()

  // Build a load connections request
  try
  {
    req = frontendserviceutils::buildLoadConnectionsRequest(optimizerId, msg);
    errorStatus = sendRequestMessageToService(req);
    if (errorStatus != 0)
    {
      spdlog::error("Engine load connection request failed");
      return errorStatus;
    }
  }
  CATCH_ERROR_AND_RETURN()

  // Wait for the connections to be loaded
  try
  {
    // @note wait on -1, i.e., until the back-end is done building the optimizer
    req = frontendserviceutils::buildConnectorWaitRequest(optimizerId);
    errorStatus = sendRequestMessageToService(req);
    if (errorStatus != 0)
    {
      spdlog::error("Engine wait request failed");
      return errorStatus;
    }
  }
  CATCH_ERROR_AND_RETURN()

  // Run the connections
  try
  {
    req = frontendserviceutils::buildRunConnectorRequest(optimizerId);
    errorStatus = sendRequestMessageToService(req);
    if (errorStatus != 0)
    {
      spdlog::error("Engine run connections request failed");
      return errorStatus;
    }
  }
  CATCH_ERROR_AND_RETURN()

  // Wait for the connections to run
  try
  {
    // @note wait on -1, i.e., until the back-end is done building the optimizer
    req = frontendserviceutils::buildConnectorWaitRequest(optimizerId);
    errorStatus = sendRequestMessageToService(req);
    if (errorStatus != 0)
    {
      spdlog::error("Engine wait request failed");
      return errorStatus;
    }
  }
  CATCH_ERROR_AND_RETURN()

  // Delete the connector
  try
  {
    req = frontendserviceutils::buildDeleteConnectorRequest(optimizerId);
    errorStatus = sendRequestMessageToService(req);
    if (errorStatus != 0)
    {
      spdlog::error("Engine delete connector request failed");
      return errorStatus;
    }
  }
  CATCH_ERROR_AND_RETURN()

  return errorStatus;
}  // buildConnectorsRequestAndRouteToBroker

}  // namespace optilab
