#include "optimizer_frontend/optimizer_http_frontend.hpp"

#include <exception>
#include <stdexcept>  // for std::invalid_argument
#include <sstream>
#include <thread>

#include <spdlog/spdlog.h>

#include "data_structure/json/json.hpp"
#include "optimizer_frontend/frontend_constants.hpp"
#include "utilities/random_generators.hpp"

namespace {

constexpr int kRandomIdLen = 10;
const char kMetricsJSON[] = "metrics";
}  // namespace

namespace optilab {

OptimizerHttpFrontend::OptimizerHttpFrontend(const int port, const int brokerPort,
                                             const std::string& brokerHostName,
                                             const std::string& httpServerAddress,
                                             const std::size_t numTreads)
: pHttpPort(port),
  pServerAddressName(httpServerAddress),
  pServerNumThreads(numTreads),
  pBrokerPort(brokerPort),
  pBrokerHostName(brokerHostName.empty() ?
      std::string(optimizerfrontend::LOCAL_HOST_NAME) : brokerHostName),
  pServer(nullptr),
  pSynchronizationResponseMutexPtr(std::make_shared<std::mutex>()),
  pConditionVariablePtr(std::make_shared<std::condition_variable>())
{
  if (pHttpPort < 1 || pHttpPort > optimizerfrontend::MAX_SERVICE_PORT_NUMBER)
  {
    throw std::invalid_argument("OptimizerHttpFrontend - invalid port number: "
        + std::to_string(pHttpPort));
  }

  if (pBrokerPort < 1 || pBrokerPort > optimizerfrontend::MAX_SERVICE_PORT_NUMBER)
  {
    throw std::invalid_argument("OptimizerFrontend - invalid broker port number: "
        + std::to_string(pBrokerPort));
  }

  initializeServer();
}

void OptimizerHttpFrontend::initializeServer()
{
  spdlog::info("OptimizerHttpFrontend - initialization");

  // Create the server
  pServer = std::shared_ptr<HttpServer>(new HttpServer());

  // Set the HTTP listening port and address
  getServer().config.port = pHttpPort;

  if(!pServerAddressName.empty())
  {
    getServer().config.address = pHttpPort;
  }

  // Set the number of threads for the server
  getServer().config.thread_pool_size = pServerNumThreads;

  // Setup the service end points
  setupEndpoint();
}  // initializeServer

void OptimizerHttpFrontend::onError(ServerRequestPtr request, const SimpleWeb::error_code& ec)
{
  // No-op
  (void) request;
  (void) ec;
}  // onError

void OptimizerHttpFrontend::onGetOptimizer(ServerResponsePtr response, ServerRequestPtr request)
{
  if (!request)
  {
    const std::string errMsg = "OptimizerHttpFrontend - onGetOptimizer: request is NULL";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  if (!response)
  {
    const std::string errMsg = "OptimizerHttpFrontend - onGetOptimizer: response is NULL";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  std::stringstream ss;
  ss << "Request from " << request->remote_endpoint_address() << ":"
      << request->remote_endpoint_port();
  spdlog::info(ss.str());

  // Create a new random id for the service runner to add
  const auto randomId = utilsrandom::buildRandomAlphaNumString(kRandomIdLen);
  {
    // Critical section
    LockGuard lock(pMutex);

    if (pRunnerMap.find(randomId) != pRunnerMap.end())
    {
      // Registering twice a service runner for the same id throws an error
      std::stringstream ss;
      ss << "OptimizerHttpFrontend - addServiceRunner: registering the same optimizer twice " <<
          randomId;
      const std::string errMsg = ss.str();
      spdlog::error(errMsg);
      throw std::runtime_error(errMsg);
    }

    FrontendRunner::SPtr serviceRunner;
    try
    {
      serviceRunner = std::make_shared<FrontendRunner>(randomId, pBrokerPort,
                                                       pSynchronizationResponseMutexPtr,
                                                       pConditionVariablePtr);

      // Initialize the runner
      serviceRunner->initRunner();
    }
    catch (std::exception& ex)
    {
      std::stringstream ss;
      ss << "OptimizerHttpFrontend - addServiceRunner: error creating and initializing "
          "the service runner " << ex.what() << " on id " << randomId;
      const std::string errMsg = ss.str();
      spdlog::error(errMsg);
      response->write(SimpleWeb::StatusCode::client_error_bad_request, errMsg);
      return;
    }
    catch (...)
    {
      std::stringstream ss;
      ss << "OptimizerHttpFrontend - addServiceRunner: error creating and initializing "
          "the service runner (undefined error) on id " << randomId;
      const std::string errMsg = ss.str();
      spdlog::error(errMsg);
      response->write(SimpleWeb::StatusCode::client_error_bad_request, errMsg);
      return;
    }

    pRunnerMap[randomId] = serviceRunner;

    std::stringstream ss;
    ss << "OptimizerHttpFrontend - addServiceRunner: service runner created and paired "
        "with id " << randomId;
    spdlog::info(ss.str());

    // Create the JSON answer with the id of the runner
    JSON::SPtr json = std::make_shared<JSON>();
    auto modelId = json->createValue(randomId);
    json->add(optimizerfrontend::OPTIMIZER_ID, modelId);

    // Return the json string representation
    response->write(json->toString());
  }
}  // onGetOptimizer

void OptimizerHttpFrontend::onDeleteOptimizer(ServerResponsePtr response, ServerRequestPtr request)
{
  if (!request)
  {
    const std::string errMsg = "OptimizerHttpFrontend - onDeleteOptimizer: request is NULL";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  if (!response)
  {
    const std::string errMsg = "OptimizerHttpFrontend - onDeleteOptimizer: response is NULL";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  // Get optimizer id to delete
  std::stringstream ssid;
  ssid << request->path_match[1];
  const auto optimizerId = ssid.str();
  {
    // Critical section
    LockGuard lock(pMutex);

    if (pRunnerMap.find(optimizerId) == pRunnerMap.end())
    {
      // Registering twice a service runner for the same connection throws an error
      std::stringstream ss;
      ss << "OptimizerHttpFrontend - onDeleteOptimizer: optimizer not found " << optimizerId;
      const std::string warnMsg = ss.str();
      spdlog::warn(warnMsg);
      response->write(SimpleWeb::StatusCode::client_error_bad_request, warnMsg);
      return;
    }

    std::stringstream ss;
    ss << "OptimizerHttpFrontend - onDeleteOptimizer: service runner removed "
        "on connection " << optimizerId;

    pRunnerMap.erase(optimizerId);
    spdlog::info(ss.str());

    // Create the JSON answer with the id of the runner
    JSON::SPtr json = std::make_shared<JSON>();
    auto modelId = json->createValue(optimizerId);
    json->add(optimizerfrontend::OPTIMIZER_ID, modelId);

    // Return the json string representation
    response->write(json->toString());
  }
}  // onDeleteOptimizer

void OptimizerHttpFrontend::onRunOptimizer(ServerResponsePtr response, ServerRequestPtr request)
{
  if (!request)
  {
    const std::string errMsg = "OptimizerHttpFrontend - onRunOptimizer: request is NULL";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  if (!response)
  {
    const std::string errMsg = "OptimizerHttpFrontend - onRunOptimizer: response is NULL";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  // Get optimizer id to run
  std::stringstream ssid;
  ssid << request->path_match[1];
  const auto optimizerId = ssid.str();

  // Get the content of the POST call,
  // i.e., the model to solve
  const std::string& model = (request->content).string();
  if (model.empty())
  {
    spdlog::warn("OptimizerHttpFrontend - onRunOptimizer: empty message. Returning.");
    return;
  }

  // Get the runner paired to the optimizer
  FrontendRunner::SPtr serviceRunner;
  {
    // Critical section
    LockGuard lock(pMutex);

    auto it = pRunnerMap.find(optimizerId);
    if (it == pRunnerMap.end())
    {
      // Registering twice a service runner for the same connection throws an error
      std::stringstream ss;
      ss << "OptimizerHttpFrontend - onRunOptimizer: optimizer not found " << optimizerId;
      const std::string errMsg = ss.str();
      spdlog::error(errMsg);
      response->write(ss);
      return;
    }

    serviceRunner = it->second;
  }

  // Route the message to the service runner
  serviceRunner->onMessage(model);

  std::string resp;
  std::string metrics;
  {
    // Critical section

    // Wait this thread to get the response
    UniqueLock ulock(*pSynchronizationResponseMutexPtr);
    while ((serviceRunner->getOutMessage()).empty() ||
           (serviceRunner->getMetricsMessage()).empty())
    {
      pConditionVariablePtr->wait(ulock);
    }

    // Once this thread is unblocked, read the message from the back-end
    resp = serviceRunner->getOutMessage();
    metrics = serviceRunner->getMetricsMessage();
  }

  // Send the response back to the client
  if (resp.empty() || metrics.empty())
  {
    const std::string errMsg = "OptimizerHttpFrontend - a problem occurred when processing "
        "the model. Return.";
    response->write(errMsg);
  }
  else
  {
    // Pack the response before sending
    packResponse(resp, metrics);

    // Send the response
    response->write(resp);
  }
}  // onRunOptimizer

void OptimizerHttpFrontend::setupEndpoint()
{
  // Set error action
  getServer().on_error =
      [this](ServerRequestPtr request, const SimpleWeb::error_code & ec)
      {
    this->onError(request, ec);
      };

  // Set the GET optimizer action, creates a new optimizer and returns its id
  getServer().resource[optimizerfrontend::HTTP_REST_ENDPOINT_GET_OPTIMIZER]
                       [optimizerfrontend::HTTP_REST_GET] =
                           [this](ServerResponsePtr response, ServerRequestPtr request)
                           {
    this->onGetOptimizer(response, request);
                           };

  // Set the GET optimizer action, creates a new optimizer and returns its id
  getServer().resource[optimizerfrontend::HTTP_REST_ENDPOINT_DELETE_OPTIMIZER]
                       [optimizerfrontend::HTTP_REST_DELETE] =
                           [this](ServerResponsePtr response, ServerRequestPtr request)
                           {
    this->onDeleteOptimizer(response, request);
                           };

  // Set the GET optimizer action, creates a new optimizer and returns its id
  getServer().resource[optimizerfrontend::HTTP_REST_ENDPOINT_POST_MODEL]
                       [optimizerfrontend::HTTP_REST_POST] =
                           [this](ServerResponsePtr response, ServerRequestPtr request)
                           {
    this->onRunOptimizer(response, request);
                           };
}  // setupEndpoint

int OptimizerHttpFrontend::run()
{
  // Start the server
  spdlog::info("OptimizerHttpFrontend - start the server");
  pServer->start();
  return 0;
}  // run

void OptimizerHttpFrontend::packResponse(std::string& resp, std::string& metrics)
{
  // Remove last '}'
  resp.pop_back();
  resp.push_back(',');
  resp.push_back('\n');
  resp += std::string(kMetricsJSON);
  resp += ":\n";
  resp += metrics;
  resp += "\n}";
}  // packResponse

}  // namespace optilab
