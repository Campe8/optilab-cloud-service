//
// Copyright OptiLab 2019. All rights reserved.
//
// Utilities class for front-end services.
//

#pragma once

#include <string>

#include "optilab_protobuf/optilab.pb.h"
#include "system/system_export_defs.hpp"

namespace optilab {

namespace frontendserviceutils {

/// Builds and returns the optimizer request to send to the broker w.r.t. the input arguments
SYS_EXPORT_FCN optilab::OptilabRequestMessage buildCreateOptimizerRequest(
    const std::string& optimizerId, const std::string& classType, const std::string& packageType,
    optilab::FrameworkType ftype);

/// Builds and returns an optimizer wait request message
SYS_EXPORT_FCN OptilabRequestMessage buildOptimizerWaitRequest(const std::string& optimizerId,
                                                               const std::string& packageType,
                                                               int waitSec,
                                                               optilab::FrameworkType ftype);

/// Builds and returns a load model request message
SYS_EXPORT_FCN OptilabRequestMessage buildLoadModelRequest(const std::string& optimizerId,
                                                           const std::string& packageType,
                                                           const std::string& jsonModel,
                                                           optilab::FrameworkType ftype);

/// Builds and returns a run model request message
SYS_EXPORT_FCN OptilabRequestMessage buildRunModelRequest(const std::string& optimizerId,
                                                          const std::string& packageType,
                                                          optilab::FrameworkType ftype);

/// Builds and returns a model solutions request message
SYS_EXPORT_FCN OptilabRequestMessage buildModelSolutionsRequest(const std::string& optimizerId,
                                                                const std::string& packageType,
                                                                int numSol,
                                                                optilab::FrameworkType ftype);

/// Builds and returns a delete optimizer request message
SYS_EXPORT_FCN OptilabRequestMessage buildDeleteOptimizerRequest(const std::string& optimizerId,
                                                                 const std::string& packageType,
                                                                 optilab::FrameworkType ftype);

/// Builds and returns a create a connector request message
SYS_EXPORT_FCN OptilabRequestMessage buildCreateConnectorRequest(const std::string& optimizerId);

/// Builds and returns a connector wait request message
SYS_EXPORT_FCN OptilabRequestMessage buildConnectorWaitRequest(const std::string& optimizerId,
                                                               int waitSec = -1);

/// Builds and returns a load connections request message
SYS_EXPORT_FCN OptilabRequestMessage buildLoadConnectionsRequest(const std::string& optimizerId,
                                                                 const std::string& jsonModel);

/// Builds and returns a run connector request message
SYS_EXPORT_FCN OptilabRequestMessage buildRunConnectorRequest(const std::string& optimizerId);

/// Builds and returns a delete connector request message
SYS_EXPORT_FCN OptilabRequestMessage buildDeleteConnectorRequest(const std::string& optimizerId);

}  // namespace frontendserviceutils

}  // namespace optilab
