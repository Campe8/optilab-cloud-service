#include "optimizer_frontend/frontend_utilities.hpp"

#include <memory>     // for std::make_shared
#include <stdexcept>  // for std::runtime_error

#include "engine/engine_constants.hpp"
#include "model/model_constants.hpp"

namespace {

optilab::PkgType getPackageTypeFromString(const std::string& pkgType)
{
  if (pkgType == optilab::model::PARAMETER_PKG_OR_TOOLS)
  {
    return optilab::PkgType::OR_TOOLS;
  }
  else if (pkgType == optilab::model::PARAMETER_PKG_GECODE)
  {
    return optilab::PkgType::GECODE;
  }
  else if (pkgType == optilab::model::PARAMETER_PKG_SCIP)
  {
    return optilab::PkgType::SCIP;
  }
  else if (pkgType == optilab::model::PARAMETER_PKG_OPEN_GA)
  {
    return optilab::PkgType::OPEN_GA;
  }
  else
  {
    throw std::runtime_error("buildCreateOROptimizerRequest - unrecognized package: " +
                             pkgType);
  }
}  // getPackageTypeFromString

optilab::CreateOptimizerReq::EngineType getEngineTypeFromString(const std::string& engineType)
{
  if (engineType == optilab::model::FRAMEWORK_CP)
  {
    return optilab::CreateOptimizerReq::EngineType::CreateOptimizerReq_EngineType_CP;
  }
  else if (engineType == optilab::model::FRAMEWORK_MIP)
  {
    return optilab::CreateOptimizerReq::EngineType::CreateOptimizerReq_EngineType_MIP;
  }
  else if (engineType == optilab::model::FRAMEWORK_GA)
  {
    return optilab::CreateOptimizerReq::EngineType::CreateOptimizerReq_EngineType_GA;
  }
  else if (engineType == optilab::model::FRAMEWORK_SCHEDULING)
  {
    return optilab::CreateOptimizerReq::EngineType::CreateOptimizerReq_EngineType_SCHEDULING;
  }
  else if (engineType == optilab::model::FRAMEWORK_VRP)
  {
    return optilab::CreateOptimizerReq::EngineType::CreateOptimizerReq_EngineType_VRP;
  }
  else
  {
    throw std::runtime_error("buildCreateOROptimizerRequest - unrecognized class type: " +
                             engineType);
  }
}  // getEngineTypeFromString

optilab::OptilabRequestMessage buildCreateOptimizerMessage(
    const std::string& engineId, optilab::PkgType pkgType,
    optilab::CreateOptimizerReq::EngineType engineType,
    optilab::FrameworkType ftype)
{
  optilab::CreateOptimizerReq optReq;
  optReq.set_enginetype(engineType);

  optilab::OptilabRequestMessage req;
  ::google::protobuf::Any* anyRequest = new ::google::protobuf::Any();
  anyRequest->PackFrom(optReq);
  req.set_allocated_details(anyRequest);

  req.set_type(optilab::OptilabRequestMessage::Type::OptilabRequestMessage_Type_EngineEvent);
  req.set_pkgtype(pkgType);
  req.set_requesttype(optilab::RequestType::POST);
  req.set_frameworktype(ftype);
  req.set_messageinfo(engineId);

  return req;
}  // buildCreateOptimizerMessage

}  // namespace

namespace optilab {

namespace frontendserviceutils {

optilab::OptilabRequestMessage buildCreateOptimizerRequest(const std::string& optimizerId,
                                                             const std::string& classType,
                                                             const std::string& packageType,
                                                             optilab::FrameworkType ftype)
{
  // Get package type
  optilab::PkgType pkgType = getPackageTypeFromString(packageType);

  // Get the engine type
  optilab::CreateOptimizerReq::EngineType engineType = getEngineTypeFromString(classType);

  // Build and return the create optimizer request
  return buildCreateOptimizerMessage(optimizerId, pkgType, engineType, ftype);
}  // buildCreateOptimizerRequest

OptilabRequestMessage buildOptimizerWaitRequest(const std::string& optimizerId,
                                                const std::string& packageType, int waitSec,
                                                optilab::FrameworkType ftype)
{
  OptimizerWaitReq optReq;
  optReq.set_waittimeout(waitSec);

  OptilabRequestMessage req;
  ::google::protobuf::Any* anyRequest = new ::google::protobuf::Any();
  anyRequest->PackFrom(optReq);
  req.set_allocated_details(anyRequest);

  req.set_type(OptilabRequestMessage::Type::OptilabRequestMessage_Type_EngineEvent);
  req.set_pkgtype(getPackageTypeFromString(packageType));
  req.set_requesttype(RequestType::POST);
  req.set_frameworktype(ftype);
  req.set_messageinfo(optimizerId);

  return req;
}  // buildOptimizerWaitRequest

OptilabRequestMessage buildLoadModelRequest(const std::string& optimizerId,
                                            const std::string& packageType,
                                            const std::string& jsonModel,
                                            optilab::FrameworkType ftype)
{
  RunModelReq optReq;
  optReq.set_jsonmodel(jsonModel);

  OptilabRequestMessage req;
  ::google::protobuf::Any* anyRequest = new ::google::protobuf::Any();
  anyRequest->PackFrom(optReq);
  req.set_allocated_details(anyRequest);

  req.set_type(OptilabRequestMessage::Type::OptilabRequestMessage_Type_EngineEvent);
  req.set_pkgtype(getPackageTypeFromString(packageType));
  req.set_requesttype(RequestType::POST);
  req.set_frameworktype(ftype);
  req.set_messageinfo(optimizerId);

  return req;
}  // buildLoadModelRequest

OptilabRequestMessage buildRunModelRequest(const std::string& optimizerId,
                                           const std::string& packageType,
                                           optilab::FrameworkType ftype)
{
  RunModelReq optReq;

  OptilabRequestMessage req;
  ::google::protobuf::Any* anyRequest = new ::google::protobuf::Any();
  anyRequest->PackFrom(optReq);
  req.set_allocated_details(anyRequest);

  req.set_type(OptilabRequestMessage::Type::OptilabRequestMessage_Type_EngineEvent);
  req.set_pkgtype(getPackageTypeFromString(packageType));
  req.set_requesttype(RequestType::PUT);
  req.set_frameworktype(ftype);
  req.set_messageinfo(optimizerId);

  return req;
}  // buildRunModelRequest

OptilabRequestMessage buildModelSolutionsRequest(const std::string& optimizerId,
                                                 const std::string& packageType, int numSol,
                                                 optilab::FrameworkType ftype)
{
  ModelSolutionsReq optReq;
  optReq.set_numsolutions(numSol);

  OptilabRequestMessage req;
  ::google::protobuf::Any* anyRequest = new ::google::protobuf::Any();
  anyRequest->PackFrom(optReq);
  req.set_allocated_details(anyRequest);

  req.set_type(OptilabRequestMessage::Type::OptilabRequestMessage_Type_EngineEvent);
  req.set_pkgtype(getPackageTypeFromString(packageType));
  req.set_requesttype(RequestType::GET);
  req.set_frameworktype(ftype);
  req.set_messageinfo(optimizerId);

  return req;
}  // buildModelSolutionsRequest

OptilabRequestMessage buildDeleteOptimizerRequest(const std::string& optimizerId,
                                                  const std::string& packageType,
                                                  optilab::FrameworkType ftype)
{
  CreateOptimizerReq optReq;

  OptilabRequestMessage req;
  ::google::protobuf::Any* anyRequest = new ::google::protobuf::Any();
  anyRequest->PackFrom(optReq);
  req.set_allocated_details(anyRequest);

  req.set_type(OptilabRequestMessage::Type::OptilabRequestMessage_Type_EngineEvent);
  req.set_pkgtype(getPackageTypeFromString(packageType));
  req.set_requesttype(RequestType::DELETE);
  req.set_frameworktype(ftype);
  req.set_messageinfo(optimizerId);

  return req;
}  // buildDeleteOptimizerRequest

OptilabRequestMessage buildCreateConnectorRequest(const std::string& optimizerId)
{
  optilab::CreateConnectorReq optReq;
  optReq.set_connectortype(
      optilab::CreateConnectorReq::ConnectorType::CreateConnectorReq_ConnectorType_COMBINATOR);

  OptilabRequestMessage req;
  ::google::protobuf::Any* anyRequest = new ::google::protobuf::Any();
  anyRequest->PackFrom(optReq);
  req.set_allocated_details(anyRequest);

  req.set_type(optilab::OptilabRequestMessage::Type::OptilabRequestMessage_Type_EngineEvent);
  req.set_requesttype(optilab::RequestType::POST);
  req.set_frameworktype(optilab::FrameworkType::CONNECTORS);
  req.set_messageinfo(optimizerId);

  return req;
}  // buildCreateConnectorRequest

OptilabRequestMessage buildCreateConnectorRequest(const std::string& optimizerId, int waitSec)
{
  OptimizerWaitReq optReq;
  optReq.set_waittimeout(waitSec);

  OptilabRequestMessage req;
  ::google::protobuf::Any* anyRequest = new ::google::protobuf::Any();
  anyRequest->PackFrom(optReq);
  req.set_allocated_details(anyRequest);

  req.set_type(OptilabRequestMessage::Type::OptilabRequestMessage_Type_EngineEvent);
  req.set_requesttype(RequestType::POST);
  req.set_frameworktype(optilab::FrameworkType::CONNECTORS);
  req.set_messageinfo(optimizerId);

  return req;
}  // buildCreateConnectorRequest

OptilabRequestMessage buildConnectorWaitRequest(const std::string& optimizerId, int waitSec)
{
  OptimizerWaitReq optReq;
  optReq.set_waittimeout(waitSec);

  OptilabRequestMessage req;
  ::google::protobuf::Any* anyRequest = new ::google::protobuf::Any();
  anyRequest->PackFrom(optReq);
  req.set_allocated_details(anyRequest);

  req.set_type(OptilabRequestMessage::Type::OptilabRequestMessage_Type_EngineEvent);
  req.set_requesttype(RequestType::POST);
  req.set_frameworktype(optilab::FrameworkType::CONNECTORS);
  req.set_messageinfo(optimizerId);

  return req;
}  // buildConnectorWaitRequest

OptilabRequestMessage buildLoadConnectionsRequest(const std::string& optimizerId,
                                                  const std::string& jsonModel)
{
  ExecuteConnectorReq optReq;
  optReq.set_connectionsdescriptor(jsonModel);

  OptilabRequestMessage req;
  ::google::protobuf::Any* anyRequest = new ::google::protobuf::Any();
  anyRequest->PackFrom(optReq);
  req.set_allocated_details(anyRequest);

  req.set_type(OptilabRequestMessage::Type::OptilabRequestMessage_Type_EngineEvent);
  req.set_requesttype(RequestType::POST);
  req.set_frameworktype(optilab::FrameworkType::CONNECTORS);
  req.set_messageinfo(optimizerId);

  return req;
}  // buildLoadConnectionsRequest

OptilabRequestMessage buildRunConnectorRequest(const std::string& optimizerId)
{
  ExecuteConnectorReq optReq;

  OptilabRequestMessage req;
  ::google::protobuf::Any* anyRequest = new ::google::protobuf::Any();
  anyRequest->PackFrom(optReq);
  req.set_allocated_details(anyRequest);

  req.set_type(OptilabRequestMessage::Type::OptilabRequestMessage_Type_EngineEvent);
  req.set_requesttype(RequestType::PUT);
  req.set_frameworktype(optilab::FrameworkType::CONNECTORS);
  req.set_messageinfo(optimizerId);

  return req;
}  // buildRunConnectorRequest

OptilabRequestMessage buildDeleteConnectorRequest(const std::string& optimizerId)
{
  CreateConnectorReq optReq;

  OptilabRequestMessage req;
  ::google::protobuf::Any* anyRequest = new ::google::protobuf::Any();
  anyRequest->PackFrom(optReq);
  req.set_allocated_details(anyRequest);

  req.set_type(OptilabRequestMessage::Type::OptilabRequestMessage_Type_EngineEvent);
  req.set_requesttype(RequestType::DELETE);
  req.set_frameworktype(optilab::FrameworkType::CONNECTORS);
  req.set_messageinfo(optimizerId);

  return req;
}  // buildDeleteConnectorRequest

}  // namespace frontendserviceutils

}  // namespace optilab
