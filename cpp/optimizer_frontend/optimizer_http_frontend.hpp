//
// Copyright OptiLab 2019. All rights reserved.
//
// Optimizer frontend.
// Creates the server that receives incoming connection on listening websockets,
// and runs optimizers.
//

#pragma once

#include <condition_variable>
#include <cstddef>     // for std::size_t
#include <functional>  // std::function
#include <memory>      // for std::shared_ptr
#include <mutex>
#include <string>
#include <unordered_map>

#include <simpleweb_rest/server_http.hpp>

#include "optimizer_frontend/frontend_runner.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS OptimizerHttpFrontend {
public:
  using SPtr = std::shared_ptr<OptimizerHttpFrontend>;

public:
  /// Constructor: creates a new http optimizer front-end that can start a server listening on
  /// the given port and responding to REST requests.
  /// @note the caller can specify the broker host names which, if not specified,
  /// is "localhost" by default.
  /// @note "httpServerAddress" is the server's address, if empty, any address will be used.
  /// IPv4 address in dotted decimal form or IPv6 address in hexadecimal notation.
  /// @note "numTreads" is the number of threads that the server will use when start() is called.
  /// @note throws std::invalid_argument on invalid input arguments (e.g., invalid port numbers,
  /// empty end-point name, invalid number of threads)
  OptimizerHttpFrontend(const int port, const int brokerPort,
                        const std::string& brokerHostName = std::string(),
                        const std::string& httpServerAddress = std::string(),
                        const std::size_t numTreads = 1);

  /// Runs the front-end server. Returns zero on success, non-zero otherwise
  int run();

private:
  using HttpServer = SimpleWeb::Server<SimpleWeb::HTTP>;
  using HttpServerPtr = std::shared_ptr<HttpServer>;
  using ServerResponsePtr = std::shared_ptr<HttpServer::Response>;
  using ServerRequestPtr = std::shared_ptr<HttpServer::Request>;

  /// Map of front-end runners
  using RunnerMap = std::unordered_map<std::string, FrontendRunner::SPtr>;

  /// Lock type on the internal mutex
  using LockGuard = std::lock_guard<std::recursive_mutex>;
  using UniqueLock = std::unique_lock<std::mutex>;

private:
  /// Port the server will listen on
  const int pHttpPort;

  /// Name of the server's address
  const std::string pServerAddressName;

  /// Number of threads used by the server
  const std::size_t pServerNumThreads;

  /// Port used by the server to connect to the broker
  const int pBrokerPort;

  /// Name of the host where the broker is running
  const std::string pBrokerHostName;

  /// Pointer to the HTTP server
  HttpServerPtr pServer;

  /// Map of service runners
  RunnerMap pRunnerMap;

  /// Mutex synchronizing on the internal map
  std::recursive_mutex pMutex;

  /// Mutex blocking a thread waiting for back-end replies
  std::shared_ptr<std::mutex> pSynchronizationResponseMutexPtr;

  /// Condition variable to wait on for back-end responses
  std::shared_ptr<std::condition_variable> pConditionVariablePtr;

  /// Utility function: returns the reference to the server
  inline HttpServer& getServer() { return *pServer; }

  /// Utility function: initializes the server
  void initializeServer();

  /// Sets up the endpoing methods
  void setupEndpoint();

  /// Wrap the two JSON strings into a single JSON object
  void packResponse(std::string& resp, std::string& metrics);

  /// Error action taken by the server.
  /// @note connection timeouts will also call this handle with ec set to
  /// SimpleWeb::errc::operation_canceled
  void onError(ServerRequestPtr request, const SimpleWeb::error_code& ec);

  /// Creates a new service runner for a client and returns its id
  void onGetOptimizer(ServerResponsePtr response, ServerRequestPtr request);

  /// Deletes an optimizer
  void onDeleteOptimizer(ServerResponsePtr response, ServerRequestPtr request);

  /// Run a optimizer
  void onRunOptimizer(ServerResponsePtr response, ServerRequestPtr request);
};

}  // namespace optilab
