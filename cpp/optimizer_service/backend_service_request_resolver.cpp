#include "optimizer_service/backend_service_request_resolver.hpp"

#include <cassert>
#include <stdexcept>  // for std::runtime_error

#include <spdlog/spdlog.h>

#include "optilab_protobuf/optilab.pb.h"

#include "optimizer_service/optimizer_service_utilities.hpp"
#include "optimizer_service/service_callback_handler.hpp"

namespace optilab {

OptimizerResponse::SPtr BackendServiceRequestResolver::resolveRequest(
    const OptimizerRequestInfo::SPtr& requestInfo, const FrameworkApi::SPtr& frameworkApi)
{
  if (!requestInfo)
  {
    throw std::runtime_error("BackendServiceRequestResolver - empty request info");
  }

  if (!frameworkApi)
  {
    throw std::runtime_error("BackendServiceRequestResolver - empty solver API map");
  }

  BackendServiceRequestInfo::SPtr reqInfo =
      std::dynamic_pointer_cast<BackendServiceRequestInfo>(requestInfo);
  if (!reqInfo)
  {
    throw std::runtime_error("BackendServiceRequestResolver -"
        "invalid cast to BackendServiceRequestInfo");
  }

  const auto RESTType = reqInfo->getRESTRequestType();
  const auto requestType = reqInfo->getRequestType();
  const auto addr = reqInfo->getRequest()->getOutAddress();
  if (RESTType == OptimizerRequestInfo::RESTRequestType::REST_UNDEF)
  {
    const auto addr = requestInfo->getRequest()->getOutAddress();
    auto resp = optilab::serviceutils::getErrorResponse("Invalid REST request type");
    resp->setResponseAddress(addr);
    return resp;
  }

  if (requestType == BackendServiceRequestInfo::RequestType::TYPE_UNDEF)
  {
    return serviceutils::getUndefinedRequestResponse(addr);
  }

  switch (requestType)
  {
    case BackendServiceRequestInfo::RequestType::SERVICE_ENGINE:
    {
      if (RESTType == OptimizerRequestInfo::RESTRequestType::POST)
      {
        spdlog::warn("BackendServiceRequestResolver - resolveRequest: invalid POST request");
        return resolveCreateBackendServiceRequest(reqInfo, frameworkApi);
      }
      else if (RESTType == OptimizerRequestInfo::RESTRequestType::DELETE)
      {
        return resolveDeleteBackendServiceRequest(reqInfo, frameworkApi);
      }
      else if (RESTType == OptimizerRequestInfo::RESTRequestType::GET)
      {
        return resolveCreateAndRunBackendServiceRequest(reqInfo, frameworkApi);
      }
      else if (RESTType == OptimizerRequestInfo::RESTRequestType::PUT)
      {
        spdlog::warn("BackendServiceRequestResolver - resolveRequest: invalid PUT request");
        return resolveRunBackendServiceRequest(reqInfo, frameworkApi);
      }
      else
      {
        return serviceutils::getUndefinedRequestResponse(addr);
      }
    }
    default:
      return serviceutils::getUndefinedRequestResponse(addr);
  }  // switch
}  // resolveRequest

OptimizerResponse::SPtr BackendServiceRequestResolver::resolveCreateBackendServiceRequest(
    const BackendServiceRequestInfo::SPtr& requestInfo, const FrameworkApi::SPtr& frameworkApi)
{
  // Get the address to send the replies to and the engine identifier to create
  const auto addr = requestInfo->getRequest()->getOutAddress();

  // Get the engine id
  const auto& engineId = requestInfo->getBackendServiceEngineId();

  // On creation request map the engine identifier to the address to send the callback replies to
  ServiceCallbackHandler::SPtr callbackHandler =
      std::dynamic_pointer_cast<ServiceCallbackHandler>(frameworkApi->getEngineHandler());
  if (!callbackHandler)
  {
    const std::string errMsg = "BackendServiceRequestResolver - empty/wrong callback handler";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }
  callbackHandler->mapEngineIdToAddress(engineId, addr);

  const auto reqErr = frameworkApi->createBackendServiceEngine(engineId,
                                                               requestInfo->getEngineClassType());

  if (reqErr == optilab::SolverError::kError)
  {
    // On error remove the mapping right-away
    callbackHandler->removeMappingBetweenEngineIdAndAddress(engineId);
  }

  return serviceutils::getSynchRequestResponse(reqErr, addr);
}  // resolveCreateBackendServiceRequest

OptimizerResponse::SPtr BackendServiceRequestResolver::resolveDeleteBackendServiceRequest(
    const BackendServiceRequestInfo::SPtr& requestInfo, const FrameworkApi::SPtr& frameworkApi)
{
  // Get the address of the requester and the engine identifier to delete
  const auto addr = requestInfo->getRequest()->getOutAddress();

  // Get the engine id
  const auto& engineId = requestInfo->getBackendServiceEngineId();

  // Force engine interrupt (if not already stopped)
  auto reqErr = frameworkApi->interruptBackendServiceEngine(engineId);
  if (reqErr == optilab::SolverError::kError)
  {
    // The interrupt can happen on an engine which has been previously deleted,
    // e.g., during "resolveCreateAndRunBackendServiceRequest(...)".
    // In this case just return
    const std::string errMsg = "BackendServiceRequestResolver - unsuccessful engine interrupt";
    spdlog::warn(errMsg);
  }

  // On deletion request remove the mapping between the engine identifier to the address
  // of the requester
  // @note delete is already performed by "resolveCreateAndRunBackendServiceRequest(...)"
  /*
  ServiceCallbackHandler::SPtr callbackHandler =
      std::dynamic_pointer_cast<ServiceCallbackHandler>(frameworkApi->getEngineHandler());
  if (!callbackHandler)
  {
    const std::string errMsg = "BackendServiceRequestResolver - empty/wrong callback handler";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  reqErr = frameworkApi->deleteBackendServiceEngine(engineId);
  if (reqErr == optilab::SolverError::kNoError)
  {
    // Remove the mapping only if there was no error in removing the engine.
    // If there was an error and the client tries to re-sent some requests,
    // the mapping should still be there
    callbackHandler->removeMappingBetweenEngineIdAndAddress(engineId);
  }
  */
  return serviceutils::getSynchRequestResponse(optilab::SolverError::kNoError, addr);
}  // resolveDeleteConnectorEngineRequest

OptimizerResponse::SPtr BackendServiceRequestResolver::resolveCreateAndRunBackendServiceRequest(
      const BackendServiceRequestInfo::SPtr& requestInfo, const FrameworkApi::SPtr& frameworkApi)
{
  // Create the backend response message
  OptimizerResponse::SPtr response;

  // 1 - Create the back-end service engine
  response = resolveCreateBackendServiceRequest(requestInfo, frameworkApi);
  if (response->isErrorResponse()) return response;

  // 2 - Run the back-end service engine
  response = resolveRunBackendServiceRequest(requestInfo, frameworkApi);
  if (response->isErrorResponse()) return response;

  // 3 - Delete the back-end service engine
  response = resolveDeleteBackendServiceRequest(requestInfo, frameworkApi);

  // Return the final response
  return response;
}  // resolveCreateAndRunBackendServiceRequest

OptimizerResponse::SPtr BackendServiceRequestResolver::resolveRunBackendServiceRequest(
      const BackendServiceRequestInfo::SPtr& requestInfo, const FrameworkApi::SPtr& frameworkApi)
{
  const auto addr = requestInfo->getRequest()->getOutAddress();

  // Get the engine id
  const auto& engineId = requestInfo->getBackendServiceEngineId();

  const auto reqErr = frameworkApi->runBackendServiceEngine(
      engineId, requestInfo->getServiceDescriptor());

  return serviceutils::getSynchRequestResponse(reqErr, addr);
}  // resolveRunConnectorRequest

}  // namespace optilab
