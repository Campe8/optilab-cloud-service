//
// Copyright OptiLab 2019. All rights reserved.
//
// Class wrapping a connection socket.
//

#pragma once

#include <memory>   // for std::shared_ptr
#include <mutex>
#include <string>
#include <utility>  // for std::pair

#include "zeromq/zhelpers.hpp"

#include "optimizer_service/optimizer_request.hpp"
#include "optimizer_service/optimizer_response.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

/**
 * Base class encapsulating writers/readers from a given socket.
 */
class SYS_EXPORT_CLASS BaseSocketConnector {
 public:
  using SPtr = std::shared_ptr<BaseSocketConnector>;
  using SocketSPtr = std::shared_ptr<zmq::socket_t>;

 public:
  virtual ~BaseSocketConnector() = default;

  /// Close sockets
  virtual void closeSockets() = 0;

  /// Blocking call, waits until it reads a new message from the socket.
  /// Returns the optimizer request containing the request, the address of the sender and
  /// the received message
  virtual OptimizerRequest::SPtr readMessage() = 0;

  /// Non blocking call, sends the given response to the socket directed to the address specified
  /// in the given optimizer response.
  /// @note throws std::runtime_error if response is empty
  virtual void sendMessage(const OptimizerResponse::SPtr& response) = 0;
};

/**
 * Class encapsulating writers/readers from a given socket.
 */
class SYS_EXPORT_CLASS SocketConnector : public BaseSocketConnector {
 public:
  using SPtr = std::shared_ptr<SocketConnector>;
  using SocketSPtr = std::shared_ptr<zmq::socket_t>;

 public:
  explicit SocketConnector(const SocketSPtr& socket);

  ~SocketConnector();

  /// Close the open socket
  void closeSockets() override;

  /// Blocking call, waits until it reads a new message from the socket.
  /// Returns the optimizer request containing the request, the address of the sender and
  /// the received message
  OptimizerRequest::SPtr readMessage() override;

  /// Non blocking call, sends the given response to the socket directed to the address specified
  /// in the given optimizer response.
  /// @note throws std::runtime_error if response is empty
  void sendMessage(const OptimizerResponse::SPtr& response) override;

  /// Non blocking call, sends the given raw string to the given address on the
  /// socket specified in the constructor of this class.
  /// @note this method is thread-safe
  void sendRawMessage(const std::string& rawMsg, const std::string& addr);

  /// Returns the actual (pointer to the) socket used by this connector
  const SocketSPtr getSocket() const { return pSocket; }

 private:
  using LockGuard = std::lock_guard<std::mutex>;

private:
  /// Pointer to the communication socket
  SocketSPtr pSocket;

  /// Mutex to sync. dispatcher threads on the input socket
  std::mutex pInputSocketMutex;

  /// Mutex to sync. dispatcher threads on the output socket
  std::mutex pOutputSocketMutex;
};

/**
 * Class encapsulating writers/readers from a given socket.
 * This is a "simple" version of the socket connector that does not communicate
 * with an intermediate broker, but "simply" runs send/recv on the given socket.
 * The simple connector uses the in/out sockets for in/out messages respectively
 */
class SYS_EXPORT_CLASS SimpleSocketConnector : public BaseSocketConnector {
 public:
  using SPtr = std::shared_ptr<SimpleSocketConnector>;
  using SocketSPtr = std::shared_ptr<zmq::socket_t>;

 public:
  SimpleSocketConnector(const SocketSPtr& inSocket, const SocketSPtr& outSocket);

  ~SimpleSocketConnector();

  /// Close open sockets
  void closeSockets() override;

  /// Blocking call, waits until it reads a new message from the socket.
  /// Returns the optimizer request containing the request, the address of the sender and
  /// the received message
  OptimizerRequest::SPtr readMessage() override;

  /// Non blocking call, sends the given response to the socket directed to the address specified
  /// in the given optimizer response.
  /// @note throws std::runtime_error if response is empty
  void sendMessage(const OptimizerResponse::SPtr& response) override;

  /// Blocking call, waits until it reads a new message from the socket.
  /// Returns the message as a standard string
  std::string readRawMessage();

  /// Non blocking call, sends the given string to the given address on the
  /// socket specified in the constructor of this class.
  /// @note this method is thread-safe
  void sendRawMessage(const std::string& msg);

  /// Returns the actual (pointer to the) input socket used by this connector
  const SocketSPtr getInSocket() const { return pInSocket; }

  /// Returns the actual (pointer to the) output socket used by this connector
  const SocketSPtr getOutSocket() const { return pOutSocket; }

 private:
  using LockGuard = std::lock_guard<std::mutex>;

private:
  /// Pointer to the input communication socket
  SocketSPtr pInSocket;

  /// Pointer to the output communication socket
  SocketSPtr pOutSocket;

  /// Mutex to sync. dispatcher threads on the input socket
  std::mutex pInputSocketMutex;

  /// Mutex to sync. dispatcher threads on the output socket
  std::mutex pOutputSocketMutex;
};

}  // namespace optilab
