#include "optimizer_service/solver_request_resolver.hpp"

#include <spdlog/spdlog.h>

#include <cassert>
#include <stdexcept>  // for std::runtime_error

#include "optilab_protobuf/optilab.pb.h"
#include "optilab_protobuf/optimizer_model.pb.h"
#include "optimizer_service/optimizer_service_utilities.hpp"
#include "optimizer_service/service_callback_handler.hpp"

namespace
{
#define ASSERT_EXTENSION(requestContent, extension, addr) \
  if (!(requestContent.Is<extension>()))                  \
  {                                                       \
    auto resp = optilab::serviceutils::getErrorResponse(  \
        "Invalid request extension type");                \
    resp->setResponseAddress(addr);                       \
    return resp;                                          \
  }

}  // namespace

namespace optilab
{
OptimizerResponse::SPtr SolverRequestResolver::resolveRequest(
    OptimizerRequestInfo::SPtr requestInfo, SolverApi::SPtr solverApi)
{
  if (!requestInfo)
  {
    throw std::runtime_error("SolverRequestResolver - empty request info");
  }

  if (!solverApi)
  {
    throw std::runtime_error("SolverRequestResolver - empty solver API map");
  }

  SolverRequestInfo::SPtr reqInfo =
      std::dynamic_pointer_cast<SolverRequestInfo>(requestInfo);
  if (!reqInfo)
  {
    throw std::runtime_error(
        "SolverRequestResolver - invalid cast to solver request info");
  }

  const auto RESTType = reqInfo->getRESTRequestType();
  const auto requestType = reqInfo->getRequestType();
  const auto addr = reqInfo->getRequest()->getOutAddress();
  if (RESTType == OptimizerRequestInfo::RESTRequestType::REST_UNDEF)
  {
    const auto addr = reqInfo->getRequest()->getOutAddress();
    auto resp =
        optilab::serviceutils::getErrorResponse("Invalid REST request type");
    resp->setResponseAddress(addr);
    return resp;
  }

  if (requestType == SolverRequestInfo::RequestType::TYPE_UNDEF)
  {
    return serviceutils::getUndefinedRequestResponse(addr);
  }

  switch (requestType)
  {
    case SolverRequestInfo::RequestType::ENGINE:
    {
      if (RESTType == OptimizerRequestInfo::RESTRequestType::POST)
      {
        return resolveCreateEngineRequest(reqInfo, solverApi);
      }
      else if (RESTType == OptimizerRequestInfo::RESTRequestType::DELETE)
      {
        return resolveDeleteEngineRequest(reqInfo, solverApi);
      }
      else
      {
        return serviceutils::getUndefinedRequestResponse(addr);
      }
    }
    case SolverRequestInfo::RequestType::MODEL:
    {
      if (RESTType == OptimizerRequestInfo::RESTRequestType::POST)
      {
        return resolveLoadModelRequest(reqInfo, solverApi);
      }
      else if (OptimizerRequestInfo::RESTRequestType::PUT)
      {
        return resolveRunModelRequest(reqInfo, solverApi);
      }
      else if (OptimizerRequestInfo::RESTRequestType::DELETE)
      {
        return resolveKillEngineRunRequest(reqInfo, solverApi);
      }
      else
      {
        return serviceutils::getUndefinedRequestResponse(addr);
      }
    }
    case SolverRequestInfo::RequestType::SOLUTION:
    {
      if (RESTType == OptimizerRequestInfo::RESTRequestType::GET)
      {
        return resolveCollectSolutionsRequest(reqInfo, solverApi);
      }
      else
      {
        return serviceutils::getUndefinedRequestResponse(addr);
      }
    }
    case SolverRequestInfo::RequestType::WAIT:
    {
      if (RESTType == OptimizerRequestInfo::RESTRequestType::POST)
      {
        return resolveEngineWaitRequest(reqInfo, solverApi);
      }
      else
      {
        return serviceutils::getUndefinedRequestResponse(addr);
      }
    }
    default:
      return serviceutils::getUndefinedRequestResponse(addr);
  }  // switch
}  // resolveRequest

OptimizerResponse::SPtr SolverRequestResolver::resolveCreateEngineRequest(
    const SolverRequestInfo::SPtr& requestInfo,
    const SolverApi::SPtr& optimizerApi)
{
  // Get the address to send the replies to and the engine identifier to create
  const auto addr = requestInfo->getRequest()->getOutAddress();
  const OptilabRequestMessage& reqMsg =
      requestInfo->getRequest()->getRequestContent();

  // Get the Id of the engine to create
  const std::string& engineId = reqMsg.messageinfo();

  // On creation request map the engine identifier to the address to send the
  // callback replies to
  ServiceCallbackHandler::SPtr callbackHandler =
      std::dynamic_pointer_cast<ServiceCallbackHandler>(
          optimizerApi->getEngineHandler());
  if (!callbackHandler)
  {
    const std::string errMsg =
        "OptimizerRequestResolver - empty/wrong callback handler";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }
  callbackHandler->mapEngineIdToAddress(engineId, addr);

  ASSERT_EXTENSION(reqMsg.details(), CreateOptimizerReq, addr);

  CreateOptimizerReq createOptimizerReq;
  reqMsg.details().UnpackTo(&createOptimizerReq);

  // Get the framework of the engine to create
  const auto engineType = createOptimizerReq.enginetype();

  // Send the request to the engine
  optilab::SolverError reqErr;
  switch (engineType)
  {
    case ::optilab::CreateOptimizerReq::EngineType::
        CreateOptimizerReq_EngineType_CP:
    {
      reqErr = optimizerApi->createEngine(engineId, EngineClassType::EC_CP);
      break;
    }
    case ::optilab::CreateOptimizerReq::EngineType::
        CreateOptimizerReq_EngineType_MIP:
    {
      reqErr = optimizerApi->createEngine(engineId, EngineClassType::EC_MIP);
      break;
    }
    case ::optilab::CreateOptimizerReq::EngineType::
        CreateOptimizerReq_EngineType_SCHEDULING:
    {
      reqErr =
          optimizerApi->createEngine(engineId, EngineClassType::EC_SCHEDULING);
      break;
    }
    case ::optilab::CreateOptimizerReq::EngineType::
        CreateOptimizerReq_EngineType_VRP:
    {
      reqErr = optimizerApi->createEngine(engineId, EngineClassType::EC_VRP);
      break;
    }
    default:
    {
      const std::string errMsg =
          "OptimizerRequestResolver - unavailable engine";
      spdlog::error(errMsg);
      throw std::runtime_error(errMsg);
    }
  }

  if (reqErr == optilab::SolverError::kError)
  {
    // On error remove the mapping right-away
    callbackHandler->removeMappingBetweenEngineIdAndAddress(engineId);
  }

  return serviceutils::getSynchRequestResponse(reqErr, addr);
}  // resolveCreateEngineRequest

OptimizerResponse::SPtr SolverRequestResolver::resolveDeleteEngineRequest(
    const SolverRequestInfo::SPtr& requestInfo,
    const SolverApi::SPtr& optimizerApi)
{
  // Get the address of the requester and the engine identifier to delete
  const auto addr = requestInfo->getRequest()->getOutAddress();
  const OptilabRequestMessage& reqMsg =
      requestInfo->getRequest()->getRequestContent();

  // Get the Id of the engine to create
  const std::string& engineId = reqMsg.messageinfo();

  // On deletion request remove the mapping between the engine identifier to the
  // address of the requester
  ServiceCallbackHandler::SPtr callbackHandler =
      std::dynamic_pointer_cast<ServiceCallbackHandler>(
          optimizerApi->getEngineHandler());
  if (!callbackHandler)
  {
    const std::string errMsg =
        "OptimizerRequestResolver - empty/wrong callback handler";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  const auto reqErr = optimizerApi->deleteEngine(engineId);
  if (reqErr == optilab::SolverError::kNoError)
  {
    // Remove the mapping only if there was no error in removing the engine.
    // If there was an error and the client tries to re-sent some requests,
    // the mapping should still be there
    callbackHandler->removeMappingBetweenEngineIdAndAddress(engineId);
  }

  return serviceutils::getSynchRequestResponse(reqErr, addr);
}  // resolveDeleteEngineRequest

OptimizerResponse::SPtr SolverRequestResolver::resolveLoadModelRequest(
    const SolverRequestInfo::SPtr& requestInfo,
    const SolverApi::SPtr& optimizerApi)
{
  const auto addr = requestInfo->getRequest()->getOutAddress();
  const OptilabRequestMessage& reqMsg =
      requestInfo->getRequest()->getRequestContent();
  const std::string& engineId = reqMsg.messageinfo();
  SolverError reqErr;
  if (reqMsg.details().Is<RunModelReq>())
  {
    ASSERT_EXTENSION(reqMsg.details(), RunModelReq, addr);
    RunModelReq modelReq;
    reqMsg.details().UnpackTo(&modelReq);
    reqErr = optimizerApi->loadModel(engineId, modelReq.jsonmodel());
  }
  else if (reqMsg.details().Is<OptimizerModel>())
  {
    ASSERT_EXTENSION(reqMsg.details(), OptimizerModel, addr);
    OptimizerModel modelReq;
    reqMsg.details().UnpackTo(&modelReq);
    reqErr = optimizerApi->loadModel(engineId, modelReq);
  }
  else
  {
    const std::string errMsg =
        "OptimizerRequestResolver - unrecognized model input format";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  return serviceutils::getSynchRequestResponse(reqErr, addr);
}  // resolveLoadModelRequest

OptimizerResponse::SPtr SolverRequestResolver::resolveRunModelRequest(
    const SolverRequestInfo::SPtr& requestInfo,
    const SolverApi::SPtr& optimizerApi)
{
  const auto addr = requestInfo->getRequest()->getOutAddress();
  const OptilabRequestMessage& reqMsg =
      requestInfo->getRequest()->getRequestContent();
  const std::string& engineId = reqMsg.messageinfo();

  const auto reqErr = optimizerApi->runModel(engineId);

  return serviceutils::getSynchRequestResponse(reqErr, addr);
}  // resolveRunModelRequest

OptimizerResponse::SPtr SolverRequestResolver::resolveKillEngineRunRequest(
    const SolverRequestInfo::SPtr& requestInfo,
    const SolverApi::SPtr& optimizerApi)
{
  const auto addr = requestInfo->getRequest()->getOutAddress();
  const OptilabRequestMessage& reqMsg =
      requestInfo->getRequest()->getRequestContent();
  const std::string& engineId = reqMsg.messageinfo();

  const auto reqErr = optimizerApi->forceTerminationModelRun(engineId);

  return serviceutils::getSynchRequestResponse(reqErr, addr);
}  // resolveRunModelRequest

OptimizerResponse::SPtr SolverRequestResolver::resolveCollectSolutionsRequest(
    const SolverRequestInfo::SPtr& requestInfo,
    const SolverApi::SPtr& optimizerApi)
{
  const auto addr = requestInfo->getRequest()->getOutAddress();
  const OptilabRequestMessage& reqMsg =
      requestInfo->getRequest()->getRequestContent();
  const std::string& engineId = reqMsg.messageinfo();

  ASSERT_EXTENSION(reqMsg.details(), ModelSolutionsReq, addr);
  ModelSolutionsReq solutionReq;
  reqMsg.details().UnpackTo(&solutionReq);

  const auto reqErr =
      optimizerApi->collectSolutions(engineId, solutionReq.numsolutions());

  return serviceutils::getSynchRequestResponse(reqErr, addr);
}  // resolveCollectSolutionsRequest

OptimizerResponse::SPtr SolverRequestResolver::resolveEngineWaitRequest(
    const SolverRequestInfo::SPtr& requestInfo,
    const SolverApi::SPtr& optimizerApi)
{
  const auto addr = requestInfo->getRequest()->getOutAddress();
  const OptilabRequestMessage& reqMsg =
      requestInfo->getRequest()->getRequestContent();
  const std::string& engineId = reqMsg.messageinfo();

  ASSERT_EXTENSION(reqMsg.details(), OptimizerWaitReq, addr);
  OptimizerWaitReq waitReq;
  reqMsg.details().UnpackTo(&waitReq);

  const auto reqErr = optimizerApi->engineWait(engineId, waitReq.waittimeout());

  return serviceutils::getSynchRequestResponse(reqErr, addr);
}  // resolveEngineWaitRequest

}  // namespace optilab
