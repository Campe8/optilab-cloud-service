#include "optimizer_service/backend_worker_runner.hpp"

#include <cassert>
#include <exception>
#include <stdexcept>   // for std::invalid_argument

#include <boost/bind.hpp>
#include <boost/chrono.hpp>
#include <boost/make_shared.hpp>
#include <spdlog/spdlog.h>

#include "optilab_protobuf/optilab.pb.h"
#include "optimizer_service/optimizer_service_utilities.hpp"
#include "optimizer_service/service_constants.hpp"

namespace {

const char kLocalHost[] = "127.0.0.1";
constexpr int kWaitForThreadsToExitMsec = 250;
const ::optilab::RequestType kShutdownRequestType = ::optilab::RequestType::UNKNOWN;

std::string getSocketAddr(const int socketPort, const std::string& socketAddr)
{
  std::string addr = optilab::serviceworker::WORKER_PREFIX_TCP_NETWORK_ADDR;
  if (socketAddr.empty())
  {
    throw std::invalid_argument("Empty TCP address");

  }
  addr += std::string(kLocalHost) /*socketAddr*/ + ":";
  addr += std::to_string(socketPort);

  return addr;
}  // getSocketAddr

}  // namespace

namespace optilab {

BackendWorkerRunner::BackendWorkerRunner(const int senderPort, const int sinkPort,
                                         const int controllerPort, const std::string& addrSender)
: pSenderPort(senderPort),
  pSinkPort(sinkPort),
  pControllerPort(controllerPort),
  pSenderAddr(addrSender.empty() ? optimizerservice::LOCAL_HOST_NAME : addrSender),
  pContext(nullptr),
  pWorkerPoolSenderSocket(nullptr),
  pSinkReceiverSocket(nullptr),
  pWorkerPoolControllerSocket(nullptr),
  pFrontendMessageReader(nullptr),
  pSinkMessageReader(nullptr)
{
  if (pSenderPort < 1 || pSenderPort > optimizerservice::MAX_SERVICE_PORT_NUMBER)
  {
    throw std::invalid_argument("BackendWorkerRunner - invalid sender port: " +
                                std::to_string(pSenderPort));
  }

  if (pSinkPort < 1 || pSinkPort > optimizerservice::MAX_SERVICE_PORT_NUMBER)
  {
    throw std::invalid_argument("BackendWorkerRunner - invalid sink port: " +
                                std::to_string(pSinkPort));
  }

  if (pControllerPort < 1 || pControllerPort > optimizerservice::MAX_SERVICE_PORT_NUMBER)
  {
    throw std::invalid_argument("BackendWorkerRunner - invalid controller port: " +
                                std::to_string(pControllerPort));
  }
}

BackendWorkerRunner::~BackendWorkerRunner()
{
  // Destroy the reader thread
  stopRunner();
  pContext.reset();
  pFrontendMessageReader.reset();
  pSinkMessageReader.reset();
  pWorkerPoolControllerSocket.reset();
}

void BackendWorkerRunner::sendRequestToWorker(const WorkerRequestMessage& request)
{
  optilab::OptilabRequestMessage reqMessage;
  ::optilab::RequestType reqType = request.deleteRequest ?
      ::optilab::RequestType::DELETE :
       ::optilab::RequestType::GET;
  reqMessage = serviceutils::buildBackendServiceRequest(request.messageId, request.engineId,
                                                        request.frameworkType,
                                                        request.classType, request.packageType,
                                                        request.payload, reqType);
  {
    // Critical section
    UniqueLock lock(pRequestResponseStateMutex);

    // Add the request to the queue of requests
    pRequestQueue.push_back(reqMessage);

    // If the request is a delete request, send also a shutdown message
    if (request.deleteRequest)
    {
      optilab::OptilabRequestMessage shutdownReqMessage =
          serviceutils::buildBackendServiceRequest(request.messageId, request.engineId,
                                                   request.frameworkType, request.classType,
                                                   request.packageType, request.payload,
                                                   kShutdownRequestType);
      pRequestQueue.push_back(shutdownReqMessage);
    }


    // Wake-up the thread waiting on incoming requests
    pRequestResponseStateConditionVariable.notify_all();
  }
}  // sendRequestToWorker

void BackendWorkerRunner::startRunner()
{
  // Skip the start runner if the worker runner is already running
  if (pWorkerRunnerRunning)
  {
    spdlog::warn("BackendWorkerRunner - worker runner already running");
    return;
  }

  // Initialize the context for the runner
  try
  {
    // Create a context and open a socket.
    // @note using ZMQ_PUSH instead of ZMQ_DEALER for the socket
    pContext = std::make_shared<zmq::context_t>(
        optimizerservice::DEFAULT_ZMQ_CONTEXT_NUM_THREADS);
    pWorkerPoolSenderSocket = std::make_shared<zmq::socket_t>(*pContext, ZMQ_PUSH);

    // Get the front-end/back-end addresses to bind to
    const std::string workerPoolAddr = getSocketAddr(pSenderPort, pSenderAddr);
    spdlog::info("BackendWorkerRunner - connecting to (worker pool) address " + workerPoolAddr);

    pWorkerPoolSenderSocket->bind(workerPoolAddr.c_str());

    // Wait for sockets startup
    boost::this_thread::sleep_for(boost::chrono::milliseconds(kWaitForThreadsToExitMsec));
  }
  catch (std::exception& ex)
  {
    const std::string errMsg = "BackendWorkerRunner - error during connection setup: " +
        std::string(ex.what());
    spdlog::error(errMsg);
    throw;
  }
  catch (...)
  {
    const std::string errMsg = "BackendWorkerRunner - error during connection setup";
    spdlog::error(errMsg);
    throw;
  }

  // Start the listener thread for incoming messages
  startSinkServiceListener();

  // Signal the sink the start of a batch of jobs
  setSinkReady();

  // Start the listener thread for incoming requests
  startServiceListener();

  // Runner is running
  spdlog::info("BackendWorkerRunner - runner started");
  pWorkerRunnerRunning = true;
}  // startRunner

void BackendWorkerRunner::setSinkReady()
{
  // The first message signals the start of the batch
  const std::string batchStartMsg = std::string(serviceworker::WORKER_MESSAGE_READY);
  spdlog::info("BackendWorkerRunner - setSinkReady: "
      "communicate to the sink the start of the batch: " + batchStartMsg);
  notifySink(batchStartMsg);
}  // setSinkReady

void BackendWorkerRunner::turnDownSocketAndContext()
{
  if (pContext)
  {
    // Set linger time to zero on all sockets
    int val = 0;
    zmq_setsockopt(pWorkerPoolSenderSocket.get(), ZMQ_LINGER, &val, sizeof(val));

    val = 0;
    zmq_setsockopt(pSinkReceiverSocket.get(), ZMQ_LINGER, &val, sizeof(val));

    val = 0;
    zmq_setsockopt(pWorkerPoolControllerSocket.get(), ZMQ_LINGER, &val, sizeof(val));

    // Close all sockets
    zmq_close(pWorkerPoolSenderSocket.get());
    zmq_close(pSinkReceiverSocket.get());
    zmq_close(pWorkerPoolControllerSocket.get());

    // Shutdown the context
    zmq_ctx_shutdown(pContext.get());
    zmq_ctx_term(pContext.get());
  }
}  // turnDownSocketAndContext

void BackendWorkerRunner::notifySink(const std::string& msg)
{
  try
  {
    // Sleep to allow sink thread to startup the connection
    boost::this_thread::sleep_for(boost::chrono::milliseconds(kWaitForThreadsToExitMsec));

    // Create the PUSH socket to notify the sink
    zmq::socket_t sinkReadySocket(*pContext, ZMQ_PUSH);

    // Get the address of the sink
    const std::string sinkddr = getSocketAddr(pSinkPort, optimizerservice::LOCAL_HOST_NAME);
    sinkReadySocket.connect(sinkddr);
    s_send(sinkReadySocket, msg);
  }
  catch (std::exception& ex)
  {
    const std::string errMsg = "BackendWorkerRunner - setSinkReady error: " +
        std::string(ex.what());
    spdlog::error(errMsg);
    throw;
  }
  catch (...)
  {
    const std::string errMsg = "BackendWorkerRunner - setSinkReady error";
    spdlog::error(errMsg);
    throw;
  }
}  // notifySink

void BackendWorkerRunner::stopRunner()
{
  // If the worker is not running, return asap
  if (!pWorkerRunnerRunning)
  {
    return;
  }

  // Set the running flag to false, the runner is not running anymore
  pWorkerRunnerRunning = false;

  // Turn down the pool of workers
  spdlog::info("BackendWorkerRunner - stop runner and turning off the pool of workers");
  if (pWorkerPoolControllerSocket)
  {
    s_send(*pWorkerPoolControllerSocket, serviceworker::WORKER_MESSAGE_TURN_DOWN);
  }

  // Set flag to terminate threads
  pTerminateReader = true;

  // Push a dummy message in the queue of messages to awake the possible waiting thread
  OptilabRequestMessage dummyMsg;
  {
    // Critical section
    UniqueLock lock(pRequestResponseStateMutex);

    // Send a dummy message to wakeup possible sleeping threads
    pRequestQueue.push_back(dummyMsg);
    pRequestResponseStateConditionVariable.notify_all();
  }

  // Wait for the shutdown
  boost::future_status waitStatusReader;
  {
    waitStatusReader =
        pMessageReaderFuture.wait_for(boost::chrono::milliseconds(kWaitForThreadsToExitMsec));
  }

  boost::future_status waitStatusSinkReader;
  {
    waitStatusSinkReader =
        pSinkReaderFuture.wait_for(boost::chrono::milliseconds(kWaitForThreadsToExitMsec));
  }

  if (waitStatusReader == boost::future_status::timeout)
  {
    pFrontendMessageReader->interrupt();
  }

  if (waitStatusSinkReader == boost::future_status::timeout)
  {
    pSinkMessageReader->interrupt();
  }

  // Turn down the context and unblock all reading threads.
  // The sink will terminate as well
  turnDownSocketAndContext();

  // Sleep before destroying the socket to allow detached threads to exit
  // if any of the threads are waiting on condition variables
  // boost::this_thread::sleep_for(boost::chrono::milliseconds(kWaitForThreadsToExitMsec));

  // Join the threads
  pSinkMessageReader->join();
  pFrontendMessageReader->join();

  if (!pMessageReaderFuture.has_exception())
  {
    pMessageReaderFuture.get();
  }

  if (!pSinkReaderFuture.has_exception())
  {
    pSinkReaderFuture.get();
  }

  // Delete the socket, if any thread is waiting on it, it will throw.
  // The exception should be controlled and the thread should return
  pWorkerPoolSenderSocket.reset();
  pSinkReceiverSocket.reset();
  pWorkerPoolControllerSocket.reset();
  pContext.reset();
}  // stopRunner

void BackendWorkerRunner::startSinkServiceListener()
{
  //  Prepare the socket for the sink
  std::string sinkddr;
  try
  {
    // Create and bind the sink socket
    pSinkReceiverSocket = std::make_shared<zmq::socket_t>(*pContext, ZMQ_PULL);

    // Get the address of the sink
    sinkddr = getSocketAddr(pSinkPort, optimizerservice::LOCAL_HOST_NAME);

    // Bind the sink to the address
    pSinkReceiverSocket->bind(sinkddr);

    // Create and bind the controller socket (part of the sink logic)
    pWorkerPoolControllerSocket = std::make_shared<zmq::socket_t>(*pContext, ZMQ_PUB);

    // Get the address of the sink
    sinkddr = getSocketAddr(pControllerPort, optimizerservice::LOCAL_HOST_NAME);

    // Bind the sink to the address
    pWorkerPoolControllerSocket->bind(sinkddr);
  }
  catch (std::exception& ex)
  {
    const std::string errMsg = "BackendWorkerRunner - "
        "startSinkServiceListener error during sink setup: " + std::string(ex.what());
    spdlog::error(errMsg);
    throw;
  }
  catch (...)
  {
    const std::string errMsg = "BackendWorkerRunner - "
        "startSinkServiceListener error during sink setup";
    spdlog::error(errMsg);
    throw;
  }

  spdlog::info("BackendWorkerRunner - start sink reader on address " + sinkddr);

  // Create the task for the run loop
  boost::packaged_task<void> task(boost::bind(
      &BackendWorkerRunner::handleMessagesFromWorkersPool, this));

  // Get the future/shared state from the packaged task
  pSinkReaderFuture = task.get_future();

  // Run the thread on the task,
  // i.e., on the "run(...) method
  pSinkMessageReader.reset(new boost::thread(boost::move(task)));
}  // startSinkServiceListener

void BackendWorkerRunner::startServiceListener()
{
  // @note the pool of threads must be up and running at this point
  spdlog::info("BackendWorkerRunner - start service listener");

  // Create the task for the run loop
  boost::packaged_task<void> task(boost::bind(&BackendWorkerRunner::handleIncomingRequests, this));

  // Get the future/shared state from the packaged task
  pMessageReaderFuture = task.get_future();

  // Run the thread on the task,
  // i.e., on the "run(...) method
  pFrontendMessageReader.reset(new boost::thread(boost::move(task)));
}  // startServiceListener

void BackendWorkerRunner::handleMessagesFromWorkersPool()
{
  //  Wait for start of batch
  std::string initMessage = s_recv(*pSinkReceiverSocket);
  spdlog::info("BackendWorkerRunner - received initialization batch message: " + initMessage);

  // Loop until there are no messages to read
  while (!pTerminateReader)
  {
    boost::this_thread::interruption_point();

    std::string reply;
    try
    {
      // Get the a message from the worker pool
      reply = s_recv(*pSinkReceiverSocket);
    }
    catch(zmq::error_t& e)
    {
      if (e.num() == ETERM)
      {
        spdlog::info("BackendWorkerRunner - sink: terminate socket, return");
        break;
      }
      break;
    }
    catch (std::exception& ex)
    {
      if (!pTerminateReader)
      {
        const std::string errMsg = "BackendWorkerRunner - sink: error reading "
            "from the socket: " + std::string(ex.what());
        spdlog::error(errMsg);
        break;
      }
    }
    catch (...)
    {
      if (!pTerminateReader)
      {
        const std::string errMsg = "BackendWorkerRunner - sink: error reading "
            "from the socket";
        spdlog::error(errMsg);
        break;
      }
    }

    // Check whether to break the loop or not after the recv within try-catch
    // since the socket may have been destroyed
    if (pTerminateReader)
    {
      return;
    }

    // If there is no callback, there is no action to be taken
    if (!pRunnerCallback)
    {
      continue;
    }

    // Otherwise parse the reply
    OptiLabReplyMessage repMsg;
    repMsg.ParseFromString(reply);

    boost::this_thread::interruption_point();
    if (repMsg.replystatus() != optilab::ReplyStatus::OK)
    {
      spdlog::error("BackendWorkerRunner - sink: received an error message "
          "from the pool of workers");
      pRunnerCallback->onError(-1);
    }
    else
    {
      // Skip informational messages
      const auto replyType = repMsg.type();
      if (replyType != optilab::OptiLabReplyMessage::InformationalMessage)
      {
        pRunnerCallback->onReply(-1, repMsg);
      }
    }
  }  // while
}  // handleMessagesFromWorkersPool

void BackendWorkerRunner::handleIncomingRequests()
{
  if (!pWorkerPoolSenderSocket)
  {
    throw std::runtime_error("BackendWorkerRunner - empty worker pool sender socket");
  }

  // Start receiving responses from the worker-pool
  while (!pTerminateReader)
  {
    boost::this_thread::interruption_point();

    // Check if there is a request to send to workers.
    // If so, continue. If not, put this thread on old until an OK response is received
    OptilabRequestMessage request;
    {
      // Critical section
      UniqueLock lock(pRequestResponseStateMutex);
      if (pRequestQueue.empty())
      {
        // Before waiting, check if this threads needs to be terminated.
        // If so, return asap
        if (pTerminateReader) return;

        // Wait until there is a request in the queue to be sent to the pool of workers
        pRequestResponseStateConditionVariable.wait(
            lock, boost::bind(&RequestQueue::size, &pRequestQueue));
      }

      // Check if this thread was woken up to terminate its execution
      if (pTerminateReader) return;

      // Remove one request
      request = pRequestQueue.front();
      pRequestQueue.pop_front();
    }

    // Send the request to the workers
    bool reqErr = false;
    try
    {
      // Send the message to the worker pool
      boost::this_thread::interruption_point();
      s_send(*pWorkerPoolSenderSocket, request.SerializeAsString());
    }
    catch(zmq::error_t& e)
    {
      if (e.num() == ETERM)
      {
        spdlog::info("BackendWorkerRunner - handleIncomingRequests: terminate socket, return");
        break;
      }
      break;
    }
    catch (std::exception& ex)
    {
      if (!pTerminateReader)
      {
        const std::string errMsg = "BackendWorkerRunner - handleIncomingRequests: "
            "error sending the request to the workers pool:" + std::string(ex.what());
        spdlog::error(errMsg);
        reqErr = true;
      }
    }
    catch(...)
    {
      if (!pTerminateReader)
      {
        const std::string errMsg = "BackendWorkerRunner - handleIncomingRequests: "
            "error sending the request to the workers pool";
        spdlog::error(errMsg);
        reqErr = true;
      }
    }

    // Check whether to break the loop or not after the recv within the try-catch
    // since the socket may have been destroyed
    if (pTerminateReader)
    {
      return;
    }

    // Notify the requester on error
    if (reqErr && pRunnerCallback)
    {
      pRunnerCallback->onError(request.messageid());
    }
    else if (!pRunnerCallback)
    {
      return;
    }
  }// while
}  // handleIncomingRequests

}  // namespace optilab
