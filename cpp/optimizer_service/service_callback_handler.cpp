#include "optimizer_service/service_callback_handler.hpp"

#include <stdexcept>  // for std::invalid_argument

#include <spdlog/spdlog.h>

#include "optilab_protobuf/optilab.pb.h"
#include "optimizer_service/optimizer_response.hpp"

namespace optilab {

ServiceCallbackHandler::ServiceCallbackHandler(BaseSocketConnector::SPtr connector)
: pSocketConnector(connector)
{
  if (!pSocketConnector)
  {
    throw std::invalid_argument("ServiceCallbackHandler - empty socket connector");
  }
}

void ServiceCallbackHandler::mapEngineIdToAddress(const std::string& engineId,
                                                  const std::string& addr)
{
  {
    // Critical section
    LockGuard lock(pEngineAddrMapMutex);
    pEngineAddressMap[engineId] = addr;
  }
}  // mapEngineIdToAddress

void ServiceCallbackHandler::removeMappingBetweenEngineIdAndAddress(const std::string& engineId)
{
  {
    // Critical section
    LockGuard lock(pEngineAddrMapMutex);
    pEngineAddressMap.erase(engineId);
  }
}  // removeMappingBetweenEngineIdAndAddress

void ServiceCallbackHandler::metricsCallback(const std::string& engineId,
                                             const std::string& jsonMetrics)
{
  // Get the address to send the solution to
  const auto addr = getAddressForEngineId(engineId);

  OptimizerMetricsRep metricsReply;
  metricsReply.set_metrics(jsonMetrics);

  ::google::protobuf::Any* metrics = new ::google::protobuf::Any();
  metrics->PackFrom(metricsReply);

  OptiLabReplyMessage rep;
  rep.set_allocated_details(metrics);

  rep.set_type(::optilab::OptiLabReplyMessage::Metrics);
  rep.set_replystatus(::optilab::ReplyStatus::OK);

  // Create an optimizer response and send it on the connector
  OptimizerResponse::SPtr optResponse = std::make_shared<OptimizerResponse>(addr, rep);
  pSocketConnector->sendMessage(optResponse);
}  // metricsCallback

void ServiceCallbackHandler::metadataCallback(const std::string& engineId,
                                             const std::string& jsonMetadata)
{
  // Get the address to send the solution to
  const auto addr = getAddressForEngineId(engineId);

  OptimizerMetadataRep metadataReply;
  metadataReply.set_metadata(jsonMetadata);

  ::google::protobuf::Any* metadata = new ::google::protobuf::Any();
  metadata->PackFrom(metadataReply);

  OptiLabReplyMessage rep;
  rep.set_allocated_details(metadata);

  rep.set_type(::optilab::OptiLabReplyMessage::Metadata);
  rep.set_replystatus(::optilab::ReplyStatus::OK);

  // Create an optimizer response and send it on the connector
  OptimizerResponse::SPtr optResponse = std::make_shared<OptimizerResponse>(addr, rep);
  pSocketConnector->sendMessage(optResponse);
}  // metadataCallback

void ServiceCallbackHandler::processCompletionCallback(const std::string& engineId)
{
  // Get the address to send the solution to
  const auto addr = getAddressForEngineId(engineId);

  OptiLabReplyMessage rep;

  rep.set_type(::optilab::OptiLabReplyMessage::EngineProcessCompleted);
  rep.set_replystatus(::optilab::ReplyStatus::OK);

  // Create an optimizer response and send it on the connector
  OptimizerResponse::SPtr optResponse = std::make_shared<OptimizerResponse>(addr, rep);
  pSocketConnector->sendMessage(optResponse);
}  // processCompletionCallback

void ServiceCallbackHandler::resultCallback(const std::string& engineId,
                                            const std::string& jsonResult)
{
  // Get the address to send the solution to
  const auto addr = getAddressForEngineId(engineId);

  OptimizerSolutionRep solReply;
  solReply.set_solution(jsonResult);
  solReply.set_semantic(OptimizerSolutionRep::UNKNOWN);

  ::google::protobuf::Any* solution = new ::google::protobuf::Any();
  solution->PackFrom(solReply);

  OptiLabReplyMessage rep;
  rep.set_allocated_details(solution);

  rep.set_type(::optilab::OptiLabReplyMessage::Solution);
  rep.set_replystatus(::optilab::ReplyStatus::OK);

  // Create an optimizer response and send it on the connector
  OptimizerResponse::SPtr optResponse = std::make_shared<OptimizerResponse>(addr, rep);
  pSocketConnector->sendMessage(optResponse);
}  // resultCallback

void ServiceCallbackHandler::resultCallback(const std::string& engineId,
                                            const ::google::protobuf::Message& protobufResult)
{
  // Get the address to send the solution to
  const auto addr = getAddressForEngineId(engineId);

  ::google::protobuf::Any* solution = new ::google::protobuf::Any();
  solution->PackFrom(protobufResult);

  OptiLabReplyMessage rep;
  rep.set_allocated_details(solution);

  rep.set_type(::optilab::OptiLabReplyMessage::Solution);
  rep.set_replystatus(::optilab::ReplyStatus::OK);

  // Create an optimizer response and send it on the connector
  OptimizerResponse::SPtr optResponse = std::make_shared<OptimizerResponse>(addr, rep);
  pSocketConnector->sendMessage(optResponse);
}  // resultCallback

std::string ServiceCallbackHandler::getAddressForEngineId(const std::string& engineId)
{
  {
    // Critical section
    LockGuard lock(pEngineAddrMapMutex);

    const auto& it = pEngineAddressMap.find(engineId);
    if (it == pEngineAddressMap.end())
    {
      const std::string errMsg = "ServiceCallbackHandler - address for engine id " + engineId
          + " not found";
      spdlog::info(errMsg);
      throw std::runtime_error(errMsg);
    }
    return it->second;
  }
}  // getAddressForEngineId

}  // namespace optilab
