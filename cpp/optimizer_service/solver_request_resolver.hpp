//
// Copyright OptiLab 2019. All rights reserved.
//
// Resolves a request to the proper solver API method.
//

#pragma once

#include <memory>  // for std::shared_ptr

#include "optimizer_api/solver_api.hpp"
#include "optimizer_service/optimizer_request_info.hpp"
#include "optimizer_service/optimizer_request_resolver.hpp"
#include "optimizer_service/optimizer_response.hpp"
#include "optimizer_service/solver_request_info.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

/*
 * The optimizer solver resolver, resolves the request encapsulated in the request info
 * by calling the appropriate method on the provided solver API.
 */
class SYS_EXPORT_CLASS SolverRequestResolver : public OptimizerRequestResolver {
 public:
  using SPtr = std::shared_ptr<SolverRequestResolver>;

 public:
  OptimizerResponse::SPtr resolveRequest(OptimizerRequestInfo::SPtr requestInfo,
                                         SolverApi::SPtr solverApi);

 private:
  /// Resolves and re-directs an engine creation request to the proper API method.
  /// Returns the response
  OptimizerResponse::SPtr resolveCreateEngineRequest(const SolverRequestInfo::SPtr& requestInfo,
                                                     const SolverApi::SPtr& solverApi);

  /// Resolves and re-directs an engine delete request to the proper API method.
  /// Returns the response
  OptimizerResponse::SPtr resolveDeleteEngineRequest(const SolverRequestInfo::SPtr& requestInfo,
                                                     const SolverApi::SPtr& solverApi);

  /// Resolves and re-directs a load model request to the proper API method.
  /// Returns the response
  OptimizerResponse::SPtr resolveLoadModelRequest(const SolverRequestInfo::SPtr& requestInfo,
                                                  const SolverApi::SPtr& solverApi);

  /// Resolves and re-directs a run model request to the proper API method.
  /// Returns the response
  OptimizerResponse::SPtr resolveRunModelRequest(const SolverRequestInfo::SPtr& requestInfo,
                                                 const SolverApi::SPtr& solverApi);

  /// Resolves and re-directs a kill engine request to the proper API method.
  /// Returns the response
  OptimizerResponse::SPtr resolveKillEngineRunRequest(const SolverRequestInfo::SPtr& requestInfo,
                                                      const SolverApi::SPtr& solverApi);

  /// Resolves and re-directs a collect solutions request to the proper API method.
  /// Returns the response
  OptimizerResponse::SPtr resolveCollectSolutionsRequest(const SolverRequestInfo::SPtr& requestInfo,
                                                         const SolverApi::SPtr& solverApi);

  /// Resolves and re-directs an engine wait request to the proper API method.
  /// Returns the response
  OptimizerResponse::SPtr resolveEngineWaitRequest(const SolverRequestInfo::SPtr& requestInfo,
                                                   const SolverApi::SPtr& solverApi);
};

}  // namespace optilab
