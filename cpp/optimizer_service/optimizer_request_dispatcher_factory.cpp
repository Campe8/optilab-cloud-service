#include "optimizer_service/optimizer_request_dispatcher_factory.hpp"

#include "zeromq/zhelpers.hpp"

namespace optilab
{
OptimizerRequestDispatcherFactory::OptimizerRequestDispatcherFactory(
    const FrameworkApi::FrameworkApiMapSPtr& frameworkApiMap)
    : pRequestInfoFactory(std::make_shared<OptimizerRequestInfoFactory>()),
      pFrameworkAPIMap(frameworkApiMap)
{
  if (!pFrameworkAPIMap)
  {
    throw std::invalid_argument("Empty framework API map");
  }
}

OptimizerRequestDispatcher::SPtr OptimizerRequestDispatcherFactory::build(
    const BaseSocketConnector::SPtr& connector,
    const OptimizerRequest::SPtr& request)
{
  // Build the request info w.r.t. the framework the optimizer request is
  // addressed to
  OptimizerRequestInfo::SPtr requestInfo =
      pRequestInfoFactory->buildRequestInfo(request);

  /// Build and return an optimizer dispatcher
  OptimizerRequestDispatcher::SPtr optimizerDispatcher =
      std::make_shared<OptimizerRequestDispatcher>(connector, requestInfo,
                                                   pFrameworkAPIMap);

  return optimizerDispatcher;
}  // build

}  // namespace optilab
