//
// Copyright OptiLab 2019. All rights reserved.
//
// Optimizer service.
// Creates a pool of workers and manages connections.
//

#pragma once

#include <memory>  // for std::shared_ptr
#include <string>
#include <vector>

#include "optimizer_service/tpool_service_worker.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS WorkerPoolService {
public:
  /// Type of connection
  enum ConnectionType
  {
    /// Standard TCP connection
    TCP = 0,

    /// IPC connection
    IPC
  };

  using SPtr = std::shared_ptr<WorkerPoolService>;

public:
  /// Constructor: creates a new worker pool service with "numWorkers" workers listening on the
  /// specified receiver port and sending responses to the specified sender port.
  /// @note the caller can specify a host names which, if not specified, is "localhost" by default.
  /// @note by default connection type is TCP on same host.
  /// @note the caller can specify the number of dispatcher threads each worker should have.
  /// If the number specified in input is zero (default), WORKER_DEFAULT_DISPATCHER_NUM_THREADS
  /// will be used. For more informations, see "service_constants.hpp".
  /// @note throws std::invalid_argument on invalid input arguments (e.g., invalid port numbers,
  /// or invalid number of workers)
  WorkerPoolService(const int receiverPort, const int senderPort, const int controllerPort,
                    const int numWorkers, const int numWorkerDispatcherThreads = 0,
                    ConnectionType connectionType = ConnectionType::TCP,
                    const std::string& hostName = std::string());

  /// Runs the service. Returns zero on success, non-zero otherwise.
  /// @note this is a blocking call
  int run();

private:
  /// Port the workers are listening from
  const int pReceiverPort;

  /// Port the workers are sending responses to
  const int pSenderPort;

  /// Port for life-time management of worker
  const int pControllerPort;

  /// Name of the host to use for connecting the workers
  const std::string pHostName;

  /// Number of workers to run the service on
  const int pNumWorkers;

  /// Number of threads for dispatcher on each worker
  const int pNumWorkerDispatcherThreads;

  /// Type of connection
  const ConnectionType pConnectionType;

  /// List of workers to run on this service
  std::vector<TPoolServiceWorker::SPtr> pWorkersList;

  /// Utility function: initializes the service
  void initializeService();

};

}  // namespace optilab
