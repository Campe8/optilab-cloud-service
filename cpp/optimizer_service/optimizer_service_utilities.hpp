//
// Copyright OptiLab 2019. All rights reserved.
//
// Utilities class for the optimizer service.
//

#pragma once

#include "optilab_protobuf/optilab.pb.h"

#include "engine/engine_constants.hpp"
#include "optimizer_api/framework_api.hpp"
#include "optimizer_service/optimizer_response.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

namespace serviceutils {

/// Sets the reply message as "generic" with given status
SYS_EXPORT_FCN void setGenericReplyMessage(::optilab::OptiLabReplyMessage& replyMsg,
                                           ::optilab::ReplyStatus status);

/// Returns an error response with given error message (default empty message)
SYS_EXPORT_FCN optilab::OptimizerResponse::SPtr getErrorResponse(const std::string& msg="");

/// Returns an okay response with given message (default empty message)
SYS_EXPORT_FCN optilab::OptimizerResponse::SPtr getOkResponse(const std::string& msg="");

/// Builds and returns a pointer to a new instance of an OptimizerAPI map.
/// @note this function builds an entry for each optimizer package type.
/// For more information, see the "FrameworkApi" class and derived classed
SYS_EXPORT_FCN optilab::FrameworkApi::FrameworkApiMapSPtr buildOpimizerAPIMap();

SYS_EXPORT_FCN optilab::OptimizerResponse::SPtr getUndefinedRequestResponse(
    const std::string& addr);

SYS_EXPORT_FCN optilab::OptimizerResponse::SPtr getErrorOnRequestResponse(const std::string& addr);

SYS_EXPORT_FCN optilab::OptimizerResponse::SPtr getOkOnRequestResponse(const std::string& addr);

SYS_EXPORT_FCN optilab::OptimizerResponse::SPtr getSynchRequestResponse(optilab::SolverError err,
                                                                        const std::string& addr);

SYS_EXPORT_FCN optilab::OptilabRequestMessage buildBackendServiceRequest(
    int messageId, const std::string& engineId, EngineFrameworkType frameworkType,
    EngineClassType classType, OptimizerPkg packageType, const std::string& payload,
    RequestType=GET);

}  // namespace serviceutils

}  // namespace optilab
