#include "optimizer_service/optimizer_request_info.hpp"

#include <stdexcept>  // for std::invalid_argument

#include <spdlog/spdlog.h>

#include "optilab_protobuf/optilab.pb.h"

namespace optilab {

OptimizerRequestInfo::OptimizerRequestInfo(const OptimizerRequest::SPtr& request)
: pRequest(request)
{
  if (!pRequest)
  {
    throw std::invalid_argument("OptimizerRequestInfo - empty request");
  }

  initializeRequestInfo(request);
}

void OptimizerRequestInfo::initializeRequestInfo(const OptimizerRequest::SPtr& request)
{
  // REST type
  if (request->getRequestType() == ::optilab::RequestType::POST)
  {
    pRESTType = RESTRequestType::POST;
  }
  else if (request->getRequestType() == ::optilab::RequestType::GET)
  {
    pRESTType = RESTRequestType::GET;
  }
  else if (request->getRequestType() == ::optilab::RequestType::DELETE)
  {
    pRESTType = RESTRequestType::DELETE;
  }
  else if (request->getRequestType() == ::optilab::RequestType::PUT)
  {
    pRESTType = RESTRequestType::PUT;
  }
  else
  {
    throw std::runtime_error("OptimizerRequestInfo - unrecognized REST request");
  }

  // Framework type
  const OptilabRequestMessage& req = request->getRequestContent();
  const auto frameworkType = req.frameworktype();
  switch (frameworkType)
  {
    case ::optilab::FrameworkType::OR:
      pFrameworkType = EngineFrameworkType::OR_FRAMEWORK;
      break;
    case ::optilab::FrameworkType::CONNECTORS:
      pFrameworkType = EngineFrameworkType::CONNECTORS_FRAMEWORK;
      break;
    case ::optilab::FrameworkType::BACKEND_SERVICE:
      pFrameworkType = EngineFrameworkType::BACKEND_SERVICE_FRAMEWORK;
      break;
    case ::optilab::FrameworkType::EVOLUTIONARY:
      pFrameworkType = EngineFrameworkType::EVOLUTIONARY_TOOLBOX;
      break;
    case ::optilab::FrameworkType::ROUTING:
      pFrameworkType = EngineFrameworkType::ROUTING_TOOLBOX;
      break;
    case ::optilab::FrameworkType::SCHEDULING:
      pFrameworkType = EngineFrameworkType::SCHEDULING_TOOLBOX;
      break;
    case ::optilab::FrameworkType::DATA_AND_PORTS:
      pFrameworkType = EngineFrameworkType::DATA_AND_PORTS_TOOLBOX;
      break;
    case ::optilab::FrameworkType::CONSTRAINT_PROGRAMMING:
      pFrameworkType = EngineFrameworkType::CP_TOOLBOX;
      break;
    case ::optilab::FrameworkType::OR_MP:
      pFrameworkType = EngineFrameworkType::OR_TOOLBOX;
      break;
    default:
    {
      const std::string errMsg = "OptimizerRequestInfo - unrecognized framework type";
      spdlog::error(errMsg);
      throw std::runtime_error(errMsg);
    }
  }

  // Optimizer package
  const auto packageType = req.pkgtype();
  switch (packageType)
  {
    case ::optilab::PkgType::OR_TOOLS:
      pOptimizerPackage = OptimizerPkg::OP_OR_TOOLS;
      break;
    case ::optilab::PkgType::SCIP:
      pOptimizerPackage = OptimizerPkg::OP_SCIP;
      break;
    case ::optilab::PkgType::GECODE:
      pOptimizerPackage = OptimizerPkg::OP_GECODE;
      break;
    case ::optilab::PkgType::OPEN_GA:
      pOptimizerPackage = OptimizerPkg::OP_OPEN_GA;
      break;
    default:
      pOptimizerPackage = OptimizerPkg::OP_UNDEF;
      break;
  }
}  // initializeRequestInfo

}  // namespace optilab
