//
// Copyright OptiLab 2019. All rights reserved.
//
// Entry point for the OptiLab service.
//

#include <getopt.h>

#include <exception>
#include <iostream>
#include <string>
#include <vector>

#include "config/config_constants.hpp"
#include "config/system_network_config.hpp"
#include "optimizer_service/optimizer_service.hpp"

extern int optind;
extern char* optarg;

namespace {

void printHelp(const std::string& programName) {
  std::cerr << "Usage: " << programName << " [options]"
      << std::endl
      << "options:" << std::endl
      << "  --help|-h          Print this help message.\n"
      << "  --num-workers|-w   Number of workers (default is 1)."
      << std::endl;
}  // printHelp

}  // namespace

int main(int argc, char* argv[]) {

  char optString[] = "hw:";
  struct option longOptions[] =
  {
      { "help", no_argument, NULL, 'h' },
      { "num-workers", required_argument, NULL, 'w' },
      { 0, 0, 0, 0 }
  };

  // Parse options
  int opt;
  int numWorkers = 1;
  while (-1 != (opt = getopt_long(argc, argv, optString, longOptions, NULL)))
  {
    switch (opt)
    {
      case 'w':
      {
        numWorkers = std::stoi(std::string(optarg));
        break;
      }
      case 'h':
      default:
        printHelp(argv[0]);
        return 0;
    }
  }  // while

  // Get the port number to run the service on
  auto& config = optilab::SystemNetworkConfig::getInstance();
  auto& portList = config.getPortList(optilab::configconsts::FRONT_END_SERVICE);
  if (portList.empty())
  {
    std::cerr << "Empty front-end service port list in configuration file\n";
    return 1;
  }

  int optilabServicePort = portList[0];

  try
  {
    // Create a new optimizer service and run it
    optilab::OptimizerService optService(optilabServicePort, numWorkers);
    return optService.run();
  }
  catch (const std::exception& e)
  {
    std::cerr << e.what() << std::endl;
    return 1;
  }
  catch (...)
  {
    std::cerr << "Undefined error" << std::endl;
    return 2;
  }

  return 0;
}
