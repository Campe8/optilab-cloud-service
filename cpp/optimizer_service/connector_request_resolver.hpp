//
// Copyright OptiLab 2019. All rights reserved.
//
// Resolves a request to the proper connector API method.
//

#pragma once

#include <memory>  // for std::shared_ptr

#include "optimizer_api/connector_api.hpp"
#include "optimizer_service/connector_request_info.hpp"
#include "optimizer_service/optimizer_request_info.hpp"
#include "optimizer_service/optimizer_request_resolver.hpp"
#include "optimizer_service/optimizer_response.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

/*
 * The optimizer solver resolver, resolves the request encapsulated in the request info
 * by calling the appropriate method on the provided solver API.
 */
class SYS_EXPORT_CLASS ConnectorRequestResolver : public OptimizerRequestResolver {
 public:
  using SPtr = std::shared_ptr<ConnectorRequestResolver>;

 public:
  OptimizerResponse::SPtr resolveRequest(const OptimizerRequestInfo::SPtr& requestInfo,
                                         const ConnectorApi::SPtr& connectorApi);

 private:
  /// Resolves and re-directs an connector engine creation request to the proper API method.
  /// Returns the response
  OptimizerResponse::SPtr resolveCreateConnectorRequest(
      const OptimizerRequestInfo::SPtr& requestInfo, const ConnectorApi::SPtr& connectorApi);

  /// Resolves and re-directs a connector engine delete request to the proper API method.
  /// Returns the response
  OptimizerResponse::SPtr resolveDeleteConnectorEngineRequest(
      const OptimizerRequestInfo::SPtr& requestInfo, const ConnectorApi::SPtr& connectorApi);

  /// Resolves and re-directs a load connections request to the proper API method.
  /// Returns the response
  OptimizerResponse::SPtr resolveLoadConnectionsRequest(
      const OptimizerRequestInfo::SPtr& requestInfo, const ConnectorApi::SPtr& connectorApi);

  /// Resolves and re-directs a run connector request to the proper API method.
  /// Returns the response
  OptimizerResponse::SPtr resolveRunConnectorRequest(
      const OptimizerRequestInfo::SPtr& requestInfo, const ConnectorApi::SPtr& connectorApi);

  /// Resolves and re-directs a kill connector engine request to the proper API method.
  /// Returns the response
  OptimizerResponse::SPtr resolveKillConnectorRequest(
      const OptimizerRequestInfo::SPtr& requestInfo, const ConnectorApi::SPtr& connectorApi);

  /// Resolves and re-directs an engine wait request to the proper API method.
  /// Returns the response
  OptimizerResponse::SPtr resolveEngineWaitRequest(const OptimizerRequestInfo::SPtr& requestInfo,
                                                   const ConnectorApi::SPtr& solverApi);
};

}  // namespace optilab
