//
// Copyright OptiLab 2019. All rights reserved.
//
// Class encapsulating information coming from a client request for connector frameworks.
//

#pragma once

#include <memory>  // for std::shared_ptr
#include <string>

#include "optimizer_api/framework_api.hpp"
#include "optimizer_service/optimizer_request_info.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

/*
 * Optimizer request information data structure class.
 * This class parses a raw request and hold all the information needed
 * to address the request by a connector engine.
 */
class SYS_EXPORT_CLASS ConnectorRequestInfo : public OptimizerRequestInfo {
 public:
  using SPtr = std::shared_ptr<ConnectorRequestInfo>;

  enum RequestType {
    CONNECTOR = 0,
    EXECUTE_CONNECTOR,
    WAIT,
    TYPE_UNDEF
  };

 public:
  /// Constructor: creates a new instance of request info.
  /// @note throws std::invalid_argument on empty request
  ConnectorRequestInfo(const OptimizerRequest::SPtr& request);

  inline RequestType getRequestType() const { return pRequestType; }

 private:
  /// Request type
  RequestType pRequestType{RequestType::TYPE_UNDEF};

  /// Utility function: initalizes the request information
  void initializeRequestInfo(const OptimizerRequest::SPtr& request);
};

}  // namespace optilab
