#include "optimizer_service/optimizer_response.hpp"

#include <stdexcept>  // for std::invalid_argument

namespace optilab {

OptimizerResponse::OptimizerResponse(const OptiLabReplyMessage& replyMessage)
: pReplyContent(replyMessage)
{
}

OptimizerResponse::OptimizerResponse(const std::string& addr,
                                     const OptiLabReplyMessage& replyMessage)
: pAddress(addr), pReplyContent(replyMessage)
{
}

void OptimizerResponse::setResponseAddress(const std::string& addr)
{
  if (addr.empty())
  {
    throw std::invalid_argument("OptimizerResponse: empty address");
  }
  pAddress = addr;
}  // setResponseAddress

bool OptimizerResponse::isErrorResponse() const
{
  return pReplyContent.replystatus() == ::optilab::ReplyStatus::ERROR;
}  // isErrorResponse

}  // namespace optilab
