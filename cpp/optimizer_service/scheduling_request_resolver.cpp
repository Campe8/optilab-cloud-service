#include "optimizer_service/scheduling_request_resolver.hpp"

#include <spdlog/spdlog.h>

#include <cassert>
#include <stdexcept>  // for std::runtime_error

#include "optilab_protobuf/optilab.pb.h"
#include "optilab_protobuf/optimizer_model.pb.h"
#include "optilab_protobuf/scheduling_model.pb.h"
#include "optimizer_service/optimizer_service_utilities.hpp"
#include "optimizer_service/service_callback_handler.hpp"
#include "scheduling_toolbox/scheduling_instance.hpp"
#include "scheduling_toolbox/scheduling_utilities.hpp"

namespace
{
#define ASSERT_EXTENSION(requestContent, extension, addr) \
  if (!(requestContent.Is<extension>()))                  \
  {                                                       \
    auto resp = optilab::serviceutils::getErrorResponse(  \
        "Invalid request extension type");                \
    resp->setResponseAddress(addr);                       \
    return resp;                                          \
  }

}  // namespace

namespace optilab {

OptimizerResponse::SPtr SchedulingRequestResolver::resolveRequest(
    OptimizerRequestInfo::SPtr requestInfo, SchedulingApi::SPtr solverApi)
{
  if (!requestInfo)
  {
    throw std::runtime_error("SchedulingRequestResolver - empty request info");
  }

  if (!solverApi)
  {
    throw std::runtime_error("SchedulingRequestResolver - empty solver API map");
  }

  SolverRequestInfo::SPtr reqInfo =
      std::dynamic_pointer_cast<SolverRequestInfo>(requestInfo);
  if (!reqInfo)
  {
    throw std::runtime_error(
        "SchedulingRequestResolver - invalid cast to solver request info");
  }

  const auto RESTType = reqInfo->getRESTRequestType();
  const auto requestType = reqInfo->getRequestType();
  const auto addr = reqInfo->getRequest()->getOutAddress();
  if (RESTType == OptimizerRequestInfo::RESTRequestType::REST_UNDEF)
  {
    const auto addr = reqInfo->getRequest()->getOutAddress();
    auto resp =
        optilab::serviceutils::getErrorResponse("Invalid REST request type");
    resp->setResponseAddress(addr);
    return resp;
  }

  if (requestType == SolverRequestInfo::RequestType::TYPE_UNDEF)
  {
    return serviceutils::getUndefinedRequestResponse(addr);
  }

  switch (requestType)
  {
    case SolverRequestInfo::RequestType::ENGINE:
    {
      if (RESTType == OptimizerRequestInfo::RESTRequestType::POST)
      {
        return resolveCreateEngineRequest(reqInfo, solverApi);
      }
      else if (RESTType == OptimizerRequestInfo::RESTRequestType::DELETE)
      {
        return resolveDeleteEngineRequest(reqInfo, solverApi);
      }
      else
      {
        return serviceutils::getUndefinedRequestResponse(addr);
      }
    }
    case SolverRequestInfo::RequestType::MODEL:
    {
      if (RESTType == OptimizerRequestInfo::RESTRequestType::POST)
      {
        return resolveLoadModelRequest(reqInfo, solverApi);
      }
      else if (OptimizerRequestInfo::RESTRequestType::PUT)
      {
        return resolveRunModelRequest(reqInfo, solverApi);
      }
      else if (OptimizerRequestInfo::RESTRequestType::DELETE)
      {
        return resolveKillEngineRunRequest(reqInfo, solverApi);
      }
      else
      {
        return serviceutils::getUndefinedRequestResponse(addr);
      }
    }
    case SolverRequestInfo::RequestType::SOLUTION:
    {
      if (RESTType == OptimizerRequestInfo::RESTRequestType::GET)
      {
        return resolveCollectSolutionsRequest(reqInfo, solverApi);
      }
      else
      {
        return serviceutils::getUndefinedRequestResponse(addr);
      }
    }
    case SolverRequestInfo::RequestType::WAIT:
    {
      if (RESTType == OptimizerRequestInfo::RESTRequestType::POST)
      {
        return resolveEngineWaitRequest(reqInfo, solverApi);
      }
      else
      {
        return serviceutils::getUndefinedRequestResponse(addr);
      }
    }
    default:
      return serviceutils::getUndefinedRequestResponse(addr);
  }  // switch
}  // resolveRequest

OptimizerResponse::SPtr SchedulingRequestResolver::resolveCreateEngineRequest(
    const SolverRequestInfo::SPtr& requestInfo, const SchedulingApi::SPtr& optimizerApi)
{
  // Get the address to send the replies to and the engine identifier to create
  const auto addr = requestInfo->getRequest()->getOutAddress();
  const OptilabRequestMessage& reqMsg = requestInfo->getRequest()->getRequestContent();

  // Get the Id of the engine to create
  const std::string& engineId = reqMsg.messageinfo();

  // On creation request map the engine identifier to the address to send the
  // callback replies to
  ServiceCallbackHandler::SPtr callbackHandler =
      std::dynamic_pointer_cast<ServiceCallbackHandler>(optimizerApi->getEngineHandler());
  if (!callbackHandler)
  {
    const std::string errMsg =
        "SchedulingRequestResolver - empty/wrong callback handler";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }
  callbackHandler->mapEngineIdToAddress(engineId, addr);

  ASSERT_EXTENSION(reqMsg.details(), toolbox::CreateSchedulingOptimizerReq, addr);

  toolbox::CreateSchedulingOptimizerReq createOptimizerReq;
  reqMsg.details().UnpackTo(&createOptimizerReq);

  // Get the framework of the engine to create
  const auto engineType = createOptimizerReq.scheduling_engine_type();

  // Send the request to the engine
  optilab::SolverError reqErr;
  switch (engineType)
  {
    case ::optilab::toolbox::CreateSchedulingOptimizerReq::SchedulingEngineType::
    CreateSchedulingOptimizerReq_SchedulingEngineType_EMPLOYEE_SCHEDULING:
    {
      reqErr = optimizerApi->createEngine(
              engineId,
              toolbox::schedulingengine::SchedulingProcessorType::SPT_EMPLOYEES_SCHEDULING);
      break;
    }
    default:
    {
      const std::string errMsg =
          "SchedulingRequestResolver - unavailable engine";
      spdlog::error(errMsg);
      throw std::runtime_error(errMsg);
    }
  }

  if (reqErr == optilab::SolverError::kError)
  {
    // On error remove the mapping right-away
    callbackHandler->removeMappingBetweenEngineIdAndAddress(engineId);
  }

  return serviceutils::getSynchRequestResponse(reqErr, addr);
}  // resolveCreateEngineRequest

OptimizerResponse::SPtr SchedulingRequestResolver::resolveDeleteEngineRequest(
    const SolverRequestInfo::SPtr& requestInfo, const SchedulingApi::SPtr& optimizerApi)
{
  // Get the address of the requester and the engine identifier to delete
  const auto addr = requestInfo->getRequest()->getOutAddress();
  const OptilabRequestMessage& reqMsg =
      requestInfo->getRequest()->getRequestContent();

  // Get the Id of the engine to create
  const std::string& engineId = reqMsg.messageinfo();

  // On deletion request remove the mapping between the engine identifier to the
  // address of the requester
  ServiceCallbackHandler::SPtr callbackHandler =
      std::dynamic_pointer_cast<ServiceCallbackHandler>(
          optimizerApi->getEngineHandler());
  if (!callbackHandler)
  {
    const std::string errMsg =
        "SchedulingRequestResolver - empty/wrong callback handler";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  const auto reqErr = optimizerApi->deleteEngine(engineId);
  if (reqErr == optilab::SolverError::kNoError)
  {
    // Remove the mapping only if there was no error in removing the engine.
    // If there was an error and the client tries to re-sent some requests,
    // the mapping should still be there
    callbackHandler->removeMappingBetweenEngineIdAndAddress(engineId);
  }

  return serviceutils::getSynchRequestResponse(reqErr, addr);
}  // resolveDeleteEngineRequest

OptimizerResponse::SPtr SchedulingRequestResolver::resolveLoadModelRequest(
    const SolverRequestInfo::SPtr& requestInfo, const SchedulingApi::SPtr& optimizerApi)
{
  const auto addr = requestInfo->getRequest()->getOutAddress();
  const OptilabRequestMessage& reqMsg =
      requestInfo->getRequest()->getRequestContent();
  const std::string& engineId = reqMsg.messageinfo();

  SolverError reqErr;
  if (reqMsg.details().Is<OptimizerModel>())
  {
    ASSERT_EXTENSION(reqMsg.details(), OptimizerModel, addr);

    OptimizerModel modelReq;
    reqMsg.details().UnpackTo(&modelReq);
    if (!modelReq.has_scheduling_model())
    {
      const std::string errMsg =
          "SchedulingRequestResolver - unrecognized scheduling instance input format "
          "from the OptimizerModel message";
      spdlog::error(errMsg);
      throw std::runtime_error(errMsg);
    }

    reqErr = optimizerApi->loadModelInstance(
        engineId, std::unique_ptr<toolbox::SchedulingInstance>(
            new toolbox::SchedulingInstance(modelReq.scheduling_model())));
  }
  else
  {
    const std::string errMsg =
        "SchedulingRequestResolver - unrecognized scheduling instance input format";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  return serviceutils::getSynchRequestResponse(reqErr, addr);
}  // resolveLoadModelRequest

OptimizerResponse::SPtr SchedulingRequestResolver::resolveRunModelRequest(
    const SolverRequestInfo::SPtr& requestInfo, const SchedulingApi::SPtr& optimizerApi)
{
  const auto addr = requestInfo->getRequest()->getOutAddress();
  const OptilabRequestMessage& reqMsg =
      requestInfo->getRequest()->getRequestContent();
  const std::string& engineId = reqMsg.messageinfo();

  const auto reqErr = optimizerApi->runEngine(engineId);

  return serviceutils::getSynchRequestResponse(reqErr, addr);
}  // resolveRunModelRequest

OptimizerResponse::SPtr SchedulingRequestResolver::resolveKillEngineRunRequest(
    const SolverRequestInfo::SPtr& requestInfo, const SchedulingApi::SPtr& optimizerApi)
{
  const auto addr = requestInfo->getRequest()->getOutAddress();
  const OptilabRequestMessage& reqMsg =
      requestInfo->getRequest()->getRequestContent();
  const std::string& engineId = reqMsg.messageinfo();

  const auto reqErr = optimizerApi->interruptEngine(engineId);

  return serviceutils::getSynchRequestResponse(reqErr, addr);
}  // resolveRunModelRequest

OptimizerResponse::SPtr SchedulingRequestResolver::resolveCollectSolutionsRequest(
    const SolverRequestInfo::SPtr& requestInfo, const SchedulingApi::SPtr& optimizerApi)
{
  const auto addr = requestInfo->getRequest()->getOutAddress();
  const OptilabRequestMessage& reqMsg =
      requestInfo->getRequest()->getRequestContent();
  const std::string& engineId = reqMsg.messageinfo();

  ASSERT_EXTENSION(reqMsg.details(), ModelSolutionsReq, addr);
  ModelSolutionsReq solutionReq;
  reqMsg.details().UnpackTo(&solutionReq);

  const auto reqErr =
      optimizerApi->collectSolutions(engineId, solutionReq.numsolutions());

  return serviceutils::getSynchRequestResponse(reqErr, addr);
}  // resolveCollectSolutionsRequest

OptimizerResponse::SPtr SchedulingRequestResolver::resolveEngineWaitRequest(
    const SolverRequestInfo::SPtr& requestInfo, const SchedulingApi::SPtr& optimizerApi)
{
  const auto addr = requestInfo->getRequest()->getOutAddress();
  const OptilabRequestMessage& reqMsg =
      requestInfo->getRequest()->getRequestContent();
  const std::string& engineId = reqMsg.messageinfo();

  ASSERT_EXTENSION(reqMsg.details(), OptimizerWaitReq, addr);
  OptimizerWaitReq waitReq;
  reqMsg.details().UnpackTo(&waitReq);

  const auto reqErr = optimizerApi->engineWait(engineId, waitReq.waittimeout());

  return serviceutils::getSynchRequestResponse(reqErr, addr);
}  // resolveEngineWaitRequest

}  // namespace optilab
