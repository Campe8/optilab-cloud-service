//
// Copyright OptiLab 2019. All rights reserved.
//
// Class encapsulating information coming from a client request for
// back-end services.
// @note in this case, clients are "workers" communicating with
// each other.
//

#pragma once

#include <memory>  // for std::shared_ptr
#include <string>

#include "engine/engine_constants.hpp"
#include "optimizer_api/framework_api.hpp"
#include "optimizer_service/optimizer_request_info.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

/*
 * Optimizer request information data structure class.
 * This class parses a raw request and hold all the information needed
 * to address the request by an optimizer.
 */
class SYS_EXPORT_CLASS BackendServiceRequestInfo : public OptimizerRequestInfo {
 public:
  using SPtr = std::shared_ptr<BackendServiceRequestInfo>;

  enum RequestType {
    SERVICE_ENGINE = 0,
    TYPE_UNDEF
  };

 public:
  /// Constructor: creates a new instance of request info.
  /// @note throws std::invalid_argument on empty request
  BackendServiceRequestInfo(const OptimizerRequest::SPtr& request);

  inline RequestType getRequestType() const { return pRequestType; }

  /// Returns the engine identifier to be used as backend service engine
  inline const std::string& getBackendServiceEngineId() const { return pEngineId; }

  inline EngineFrameworkType getEngineFrameworkType() const { return pEngineFrameworkType; }

  inline EngineClassType getEngineClassType() const { return pEngineClassType; }

  /// Returns the descriptor for the requested service
  inline const std::string getServiceDescriptor() const { return pServiceDescriptor; }

 private:
  /// Identifier for the back-end engine
  std::string pEngineId;

  /// Descriptor for the requested service
  std::string pServiceDescriptor;

  /// Type of framework of engine this request is addressed to
  EngineFrameworkType pEngineFrameworkType{EngineFrameworkType::UNDEF_FRAMEWORK};

  /// Type of the class of engine this request is addressed to
  EngineClassType pEngineClassType;

  /// Request type
  RequestType pRequestType{RequestType::TYPE_UNDEF};

  /// Utility function: initalizes the request information
  void initializeRequestInfo(const OptimizerRequest::SPtr& request);
};

}  // namespace optilab
