//
// Copyright OptiLab 2019. All rights reserved.
//
// Request dispatcher, dispatches client's requests to the proper
// optimizer API and API method.
//

#pragma once

#include <memory>   // for std::shared_ptr
#include <unordered_map>
#include <utility>  // for std::pair

#include "engine/engine_constants.hpp"
#include "optimizer_api/framework_api.hpp"
#include "optimizer_service/optimizer_request_info.hpp"
#include "optimizer_service/optimizer_request_resolver.hpp"
#include "optimizer_service/optimizer_response.hpp"
#include "optimizer_service/socket_connector.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

/*
 * The optimizer request dispatcher, dispatches the incoming request to
 * the proper optimizer API and the proper API method.
 * In other words, it dispatches the request to the correct engine that is in charge of
 * executing the given request.
 */
class SYS_EXPORT_CLASS OptimizerRequestDispatcher {
 public:
  using SPtr = std::shared_ptr<OptimizerRequestDispatcher>;

 public:
  OptimizerRequestDispatcher(const BaseSocketConnector::SPtr& connector,
                             const OptimizerRequestInfo::SPtr& requestInfo,
                             const FrameworkApi::FrameworkApiMapSPtr& frameworkApiMapPtr);

  void run();

 private:
  /// Output socket connector
  const BaseSocketConnector::SPtr pConnector;

  /// Request to be routed to an optimizer
  OptimizerRequestInfo::SPtr pRequestInfo;

  /// Pointer to the map of framework APIs
  FrameworkApi::FrameworkApiMapSPtr pFrameworkApiMapPtr;

  /// Map of resolvers used to resolve the incoming request.
  /// Each entry in the map is a pair <framework_type, resolver>
  std::unordered_map<int, OptimizerRequestResolver::SPtr> pRequestResolverMap;

  /// Utility function: verifies the validity of the request to dispatch.
  /// If the request is invalid, notifies the requester and throws an error
  void checkForRequestValidity();

  /// Sends back an error message to the requester.
  /// @note the error message can be specified with "errMessage"
  void sendErrorResponse(const BaseSocketConnector::SPtr& connector,
                         const std::string& errMessage);

  /// Utility function: returns the (framework) API to use in the router.
  /// @note throws std::runtime_error if no API is found w.r.t. the request info
  std::pair<EngineFrameworkType, FrameworkApi::SPtr> getFrameworkApi() const;

  /// Utility function: returns the (framework) API given the framework and package type.
  /// @note package type is not required for all framework. Some framework may have only one
  /// package type or no package type at all (e.g., CONNECTIONS_FRAMEWORK).
  /// In these cases, the only framework API present is returned
  FrameworkApi::SPtr getFrameworkApi(EngineFrameworkType frameworkType,
                                     OptimizerPkg optimizerPackage) const;

  /// Utility function: initializes the map of resolvers
  void initializeResolversMap();

  /// Dispatches the request to the right resolver according to the type of framework and the given
  /// api instances that can address requests directed to the given framework
  OptimizerResponse::SPtr dispatchRequestToResolver(EngineFrameworkType frameworkType,
                                                    FrameworkApi::SPtr frameworkApi);
};

}  // namespace optilab
