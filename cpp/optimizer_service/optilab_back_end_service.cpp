//
// Copyright OptiLab 2019. All rights reserved.
//
// Entry point for the OptiLab service.
//

#include <getopt.h>

#include <exception>
#include <iostream>
#include <string>
#include <vector>

#include "config/config_constants.hpp"
#include "config/system_network_config.hpp"
#include "optimizer_service/worker_pool_service.hpp"

#include "zeromq/zhelpers.hpp"

extern int optind;
extern char* optarg;

namespace {

void printHelp(const std::string& programName) {
  std::cerr << "Usage: " << programName << " [options]"
      << std::endl
      << "Creates a pool of workers of given size"
      << std::endl
      << "options:" << std::endl
      << "  --help|-h          Print this help message.\n"
      << "  --num-workers|-w   Number of workers (default is 2)."
      << std::endl;
}  // printHelp

}  // namespace

int main(int argc, char* argv[]) {
  char optString[] = "hw:";
  struct option longOptions[] =
  {
      { "help", no_argument, NULL, 'h' },
      { "num-workers", required_argument, NULL, 'w' },
      { 0, 0, 0, 0 }
  };

  // Parse options
  int opt;
  int numWorkers = 2;
  while (-1 != (opt = getopt_long(argc, argv, optString, longOptions, NULL)))
  {
    switch (opt)
    {
      case 'w':
      {
        numWorkers = std::stoi(std::string(optarg));
        break;
      }
      case 'h':
      default:
        printHelp(argv[0]);
        return 0;
    }
  }  // while

  // Get the port number to run the service on
  auto& config = optilab::SystemNetworkConfig::getInstance();
  auto& portList = config.getPortList(optilab::configconsts::BACKEND_SERVICE_WORKER);
  if (portList.size() < 3)
  {
    std::cerr << "Invalid number of ports for the back-end service "
        "in the configuration file\n";
    return 1;
  }

  int optilabServiceReceiverPort = portList[0];
  int optilabServiceSenderPort = portList[1];
  int optilabServiceControllerPort = portList[2];
  try
  {
    // Create a new optimizer service and run it
    optilab::WorkerPoolService optService(optilabServiceReceiverPort, optilabServiceSenderPort,
                                          optilabServiceControllerPort, numWorkers, 1);
    return optService.run();
  }
  catch (const std::exception& e)
  {
    std::cerr << e.what() << std::endl;
    return 1;
  }
  catch (...)
  {
    std::cerr << "Undefined error" << std::endl;
    return 2;
  }

  return 0;
}
