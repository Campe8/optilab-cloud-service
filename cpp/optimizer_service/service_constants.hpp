//
// Copyright OptiLab 2019. All rights reserved.
//
// Constants for the optimizer service.
//

#pragma once

namespace optilab {

namespace serviceworker {

extern const int WORKER_DEFAULT_DISPATCHER_NUM_THREADS;
extern const int WORKER_DEFAULT_IO_NUM_THREADS;
extern const char WORKER_MESSAGE_READY[];
extern const char WORKER_MESSAGE_TURN_DOWN[];
extern const char WORKER_PREFIX_TCP_NETWORK_ADDR[];
extern const char WORKER_PREFIX_IPC_NETWORK_ADDR[];
extern const char WORKER_PREFIX_IPC_NETWORK_ADDR_EXT[];

}  // namespace serviceworker

namespace optimizerservice {

extern const int DEFAULT_ZMQ_CONTEXT_NUM_THREADS;
extern const char LOCAL_HOST_NAME[];
extern const int MAX_SERVICE_PORT_NUMBER;
extern const char WORKER_ID_PREFIX[];

}  // optimizerservice

}  // namespace optilab
