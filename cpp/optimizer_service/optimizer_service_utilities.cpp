#include "optimizer_service/optimizer_service_utilities.hpp"

#include <memory>     // for std::make_shared
#include <stdexcept>  // for std::runtime_error

#include "optimizer_api/async_connector_api.hpp"
#include "optimizer_api/cp_api.hpp"
#include "optimizer_api/data_api.hpp"
#include "optimizer_api/evolutionary_api.hpp"
#include "optimizer_api/gecode_api.hpp"
#include "optimizer_api/or_tools_api.hpp"
#include "optimizer_api/or_api.hpp"
#include "optimizer_api/routing_api.hpp"
#include "optimizer_api/scheduling_api.hpp"
#include "optimizer_api/scip_api.hpp"

namespace optilab {

namespace serviceutils {

void setGenericReplyMessage(::optilab::OptiLabReplyMessage& replyMsg, ::optilab::ReplyStatus status)

{
  replyMsg.set_type(::optilab::OptiLabReplyMessage::InformationalMessage);
  replyMsg.set_replystatus(status);
}

optilab::OptimizerResponse::SPtr getErrorResponse(const std::string& msg)
{
  ::optilab::OptiLabReplyMessage replyMsg;
  setGenericReplyMessage(replyMsg, ::optilab::ReplyStatus::ERROR);
  replyMsg.set_messageinfo(msg);

  return std::make_shared<optilab::OptimizerResponse>("", replyMsg);
}  // getErrorResponse

optilab::OptimizerResponse::SPtr getOkResponse(const std::string& msg)
{
  ::optilab::OptiLabReplyMessage replyMsg;
  setGenericReplyMessage(replyMsg, ::optilab::ReplyStatus::OK);
  replyMsg.set_messageinfo(msg);

  return std::make_shared<optilab::OptimizerResponse>(msg, replyMsg);
}  // getErrorResponse

optilab::FrameworkApi::FrameworkApiMapSPtr buildOpimizerAPIMap()
{
  optilab::FrameworkApi::FrameworkApiMapSPtr apiMapPtr =
      std::make_shared<optilab::FrameworkApi::FrameworkApiMap>();

  // SOLVER API
  std::vector<FrameworkApi::SPtr> solverApiList;
  solverApiList.resize(static_cast<int>(OptimizerPkg::OP_OR_PACKAGES_END));
  solverApiList[static_cast<int>(OptimizerPkg::OP_OR_TOOLS)] =
      std::make_shared<optilab::ORToolsApi>();
  solverApiList[static_cast<int>(OptimizerPkg::OP_GECODE)] =
        std::make_shared<optilab::GecodeApi>();
  solverApiList[static_cast<int>(OptimizerPkg::OP_SCIP)] =
        std::make_shared<optilab::SCIPApi>();
  apiMapPtr->insert(
      {
    static_cast<int>(EngineFrameworkType::OR_FRAMEWORK),
    solverApiList
      }
  );

  // CONNECTOR API
  std::vector<FrameworkApi::SPtr> connectorApiList;
  connectorApiList.push_back(std::make_shared<optilab::AsyncConnectorApi>());
  apiMapPtr->insert(
      {
    static_cast<int>(EngineFrameworkType::CONNECTORS_FRAMEWORK),
    connectorApiList
      }
  );

  // EVOLUTIONARY API
  std::vector<FrameworkApi::SPtr> evolutionaryApiList;
  evolutionaryApiList.push_back(std::make_shared<optilab::EvolutionaryApi>());
  apiMapPtr->insert(
      {
      static_cast<int>(EngineFrameworkType::EVOLUTIONARY_TOOLBOX),
      evolutionaryApiList
      }
  );

  // DATA AND PORTS API
  std::vector<FrameworkApi::SPtr> dataAndPortsApiList;
  dataAndPortsApiList.push_back(std::make_shared<optilab::DataApi>());
  apiMapPtr->insert(
      {
      static_cast<int>(EngineFrameworkType::DATA_AND_PORTS_TOOLBOX),
      dataAndPortsApiList
      }
  );

  // CP API
  std::vector<FrameworkApi::SPtr> cpApiList;
  cpApiList.push_back(std::make_shared<optilab::CPApi>());
  apiMapPtr->insert(
      {
      static_cast<int>(EngineFrameworkType::CP_TOOLBOX),
              cpApiList
      }
  );

  // ROUTING API
  std::vector<FrameworkApi::SPtr> routingApiList;
  routingApiList.push_back(std::make_shared<optilab::RoutingApi>());
  apiMapPtr->insert(
      {
      static_cast<int>(EngineFrameworkType::ROUTING_TOOLBOX),
      routingApiList
      }
  );

  // SCHEDULING API
  std::vector<FrameworkApi::SPtr> schedulingApiList;
  schedulingApiList.push_back(std::make_shared<optilab::SchedulingApi>());
  apiMapPtr->insert(
      {
      static_cast<int>(EngineFrameworkType::SCHEDULING_TOOLBOX),
      schedulingApiList
      }
  );

  // OR API
  std::vector<FrameworkApi::SPtr> orApiList;
  orApiList.push_back(std::make_shared<optilab::ORApi>());
  apiMapPtr->insert(
      {
      static_cast<int>(EngineFrameworkType::OR_TOOLBOX),
      orApiList
      }
  );

  return apiMapPtr;
}  // buildOpimizerAPIMap

optilab::OptimizerResponse::SPtr getUndefinedRequestResponse(const std::string& addr)
{
  auto resp = optilab::serviceutils::getErrorResponse("Invalid request type");
  resp->setResponseAddress(addr);
  return resp;
}  // getUndefinedRequestResponse

optilab::OptimizerResponse::SPtr getErrorOnRequestResponse(const std::string& addr)
{
  auto resp = optilab::serviceutils::getErrorResponse("Error on handling the request");
  resp->setResponseAddress(addr);
  return resp;
}  // getErrorOnRequestResponse

optilab::OptimizerResponse::SPtr getOkOnRequestResponse(const std::string& addr)
{
  auto resp = optilab::serviceutils::getOkResponse();
  resp->setResponseAddress(addr);
  return resp;
}  // getOkOnRequestResponse

/// Returns the request response according to the given type of error
optilab::OptimizerResponse::SPtr getSynchRequestResponse(optilab::SolverError err,
                                                         const std::string& addr)
{
  if (err == optilab::SolverError::kError)
  {
    return getErrorOnRequestResponse(addr);
  }
  assert(err == optilab::SolverError::kNoError);

  return getOkOnRequestResponse(addr);
}  // getSynchRequestResponse

optilab::OptilabRequestMessage buildBackendServiceRequest(int messageId,
                                                          const std::string& engineId,
                                                          EngineFrameworkType frameworkType,
                                                          EngineClassType classType,
                                                          OptimizerPkg packageType,
                                                          const std::string& payload,
                                                          RequestType reqType)
{
  // Create the protobuf message for this request
  BackendServiceEngineReq backendServiceReq;

  backendServiceReq.set_servicedescriptor(payload);

  switch(frameworkType)
  {
    case EngineFrameworkType::OR_FRAMEWORK:
      backendServiceReq.set_engineframeworktype(optilab::FrameworkType::OR);
      break;
    case EngineFrameworkType::CONNECTORS_FRAMEWORK:
      backendServiceReq.set_engineframeworktype(optilab::FrameworkType::CONNECTORS);
      break;
    case EngineFrameworkType::BACKEND_SERVICE_FRAMEWORK:
      backendServiceReq.set_engineframeworktype(optilab::FrameworkType::BACKEND_SERVICE);
      break;
    case EngineFrameworkType::EC_FRAMEWORK:
      backendServiceReq.set_engineframeworktype(optilab::FrameworkType::EC);
      break;
    default:
      throw std::runtime_error("buildBackendServiceRequest - unrecognized framework type");
  }

  switch(classType)
  {
    case EngineClassType::EC_CP:
      backendServiceReq.set_serviceclasstype(
          BackendServiceEngineReq::ServiceClassType::
          BackendServiceEngineReq_ServiceClassType_SC_CP);
      break;
    case EngineClassType::EC_MIP:
      backendServiceReq.set_serviceclasstype(
          BackendServiceEngineReq::ServiceClassType::
          BackendServiceEngineReq_ServiceClassType_SC_MIP);
      break;
    case EngineClassType::EC_COMBINATOR:
      backendServiceReq.set_serviceclasstype(
          BackendServiceEngineReq::ServiceClassType::
          BackendServiceEngineReq_ServiceClassType_SC_COMBINATOR);
      break;
    case EngineClassType::EC_GA:
      backendServiceReq.set_serviceclasstype(
          BackendServiceEngineReq::ServiceClassType::
          BackendServiceEngineReq_ServiceClassType_SC_GA);
      break;
    case EngineClassType::EC_SCHEDULING:
      backendServiceReq.set_serviceclasstype(
          BackendServiceEngineReq::ServiceClassType::
          BackendServiceEngineReq_ServiceClassType_SC_SCHEDULING);
      break;
    case EngineClassType::EC_VRP:
      backendServiceReq.set_serviceclasstype(
          BackendServiceEngineReq::ServiceClassType::
          BackendServiceEngineReq_ServiceClassType_SC_VRP);
      break;
    default:
      throw std::runtime_error("buildBackendServiceRequest - unrecognized class type");
  }

  OptilabRequestMessage req;
  ::google::protobuf::Any* anyRequest = new ::google::protobuf::Any();
  anyRequest->PackFrom(backendServiceReq);
  req.set_allocated_details(anyRequest);

  req.set_messageinfo(engineId);
  req.set_messageid(messageId);
  req.set_type(OptilabRequestMessage::Type::OptilabRequestMessage_Type_EngineEvent);
  req.set_requesttype(reqType);
  req.set_frameworktype(optilab::FrameworkType::BACKEND_SERVICE);

  switch(packageType)
  {
    case OptimizerPkg::OP_OR_TOOLS:
      req.set_pkgtype(::optilab::PkgType::OR_TOOLS);
      break;
    case OptimizerPkg::OP_SCIP:
      req.set_pkgtype(::optilab::PkgType::SCIP);
      break;
    case OptimizerPkg::OP_GECODE:
      req.set_pkgtype(::optilab::PkgType::GECODE);
      break;
    case OptimizerPkg::OP_OPEN_GA:
      req.set_pkgtype(::optilab::PkgType::OPEN_GA);
      break;
    default:
      req.set_pkgtype(::optilab::PkgType::UNDEF_PKG_TYPE);
      break;
  }

  return req;
}  // buildBackendServiceRequest

}  // namespace serviceutils

}  // namespace optilab
