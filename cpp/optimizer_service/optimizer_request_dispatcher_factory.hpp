//
// Copyright OptiLab 2019. All rights reserved.
//
// Factory class for optimizer request dispatchers.
//

#pragma once

#include <memory>     // for std::shared_ptr
#include <stdexcept>  // for std::invalid_argument

#include "optimizer_api/framework_api.hpp"
#include "optimizer_service/optimizer_request.hpp"
#include "optimizer_service/optimizer_request_dispatcher.hpp"
#include "optimizer_service/optimizer_request_info_factory.hpp"
#include "optimizer_service/socket_connector.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS OptimizerRequestDispatcherFactory {
 public:
  using SPtr = std::shared_ptr<OptimizerRequestDispatcherFactory>;

 public:
  /// Constructor, creates a new request dispatcher factory.
  /// @note throws std::invalid_argument if the given pointer to the map is empty
  OptimizerRequestDispatcherFactory(const FrameworkApi::FrameworkApiMapSPtr& frameworkApiMap);

  OptimizerRequestDispatcher::SPtr build(const BaseSocketConnector::SPtr& connector,
                                         const OptimizerRequest::SPtr& request);

  /// Returns the pointer to the map of optimizer APIs used by this factory
  inline FrameworkApi::FrameworkApiMapSPtr getOptimizerApiMapPtr() const
  {
    return pFrameworkAPIMap;
  }

 private:
  /// Factory for request info objects
  OptimizerRequestInfoFactory::SPtr pRequestInfoFactory;

  /// Map of optimizer APIs
  FrameworkApi::FrameworkApiMapSPtr pFrameworkAPIMap;
};

}  // namespace optilab
