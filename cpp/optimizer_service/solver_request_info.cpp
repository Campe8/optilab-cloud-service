#include "optimizer_service/solver_request_info.hpp"

#include <spdlog/spdlog.h>

#include <stdexcept>  // for std::runtime_error

#include "optilab_protobuf/constraint_model.pb.h"
#include "optilab_protobuf/data_model.pb.h"
#include "optilab_protobuf/evolutionary_model.pb.h"
#include "optilab_protobuf/optilab.pb.h"
#include "optilab_protobuf/optimizer_model.pb.h"
#include "optilab_protobuf/routing_model.pb.h"
#include "optilab_protobuf/scheduling_model.pb.h"

namespace optilab
{
SolverRequestInfo::SolverRequestInfo(const OptimizerRequest::SPtr& request)
    : OptimizerRequestInfo(request)
{
  initializeRequestInfo(request);
}

void SolverRequestInfo::initializeRequestInfo(
    const OptimizerRequest::SPtr& request)
{
  // Request type
  const OptilabRequestMessage& reqMsg = request->getRequestContent();
  if (reqMsg.details().Is<CreateOptimizerReq>() ||
          reqMsg.details().Is<toolbox::CreateEvolutionaryOptimizerReq>() ||
          reqMsg.details().Is<toolbox::CreateSchedulingOptimizerReq>() ||
          reqMsg.details().Is<toolbox::CreateRoutingOptimizerReq>() ||
          reqMsg.details().Is<toolbox::CreateDataOptimizerReq>() ||
          reqMsg.details().Is<toolbox::CreateCPOptimizerReq>())
  {
    pRequestType = SolverRequestInfo::RequestType::ENGINE;
  }
  else if (reqMsg.details().Is<RunModelReq>() ||
           reqMsg.details().Is<OptimizerModel>())
  {
    pRequestType = SolverRequestInfo::RequestType::MODEL;
  }
  else if (reqMsg.details().Is<ModelSolutionsReq>())
  {
    pRequestType = SolverRequestInfo::RequestType::SOLUTION;
  }
  else if (reqMsg.details().Is<OptimizerWaitReq>())
  {
    pRequestType = SolverRequestInfo::RequestType::WAIT;
  }
  else
  {
    std::string errMsg =
        "SolverRequestInfo - unrecognized request type (unknown details)";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }
}  // initializeRequestInfo

}  // namespace optilab
