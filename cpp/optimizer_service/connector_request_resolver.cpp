#include "optimizer_service/connector_request_resolver.hpp"

#include <cassert>
#include <stdexcept>  // for std::runtime_error

#include <spdlog/spdlog.h>

#include "optilab_protobuf/optilab.pb.h"

#include "optimizer_service/optimizer_service_utilities.hpp"
#include "optimizer_service/service_callback_handler.hpp"

namespace {

#define ASSERT_EXTENSION(requestContent, extension, addr)                   \
    if (!(requestContent.Is<extension>()))                                  \
  {                                                                         \
    auto resp = optilab::serviceutils::getErrorResponse("Invalid request extension type"); \
    resp->setResponseAddress(addr);                                         \
    return resp;                                                            \
  }

}  // namespace

namespace optilab {

OptimizerResponse::SPtr ConnectorRequestResolver::resolveRequest(
    const OptimizerRequestInfo::SPtr& requestInfo, const ConnectorApi::SPtr& connectorApi)
{
  if (!requestInfo)
  {
    throw std::runtime_error("ConnectorRequestResolver - empty request info");
  }

  if (!connectorApi)
  {
    throw std::runtime_error("ConnectorRequestResolver - empty solver API map");
  }

  ConnectorRequestInfo::SPtr reqInfo = std::dynamic_pointer_cast<ConnectorRequestInfo>(requestInfo);
  if (!reqInfo)
  {
    throw std::runtime_error("ConnectorRequestResolver - invalid cast to ConnectorRequestInfo");
  }

  const auto RESTType = reqInfo->getRESTRequestType();
  const auto requestType = reqInfo->getRequestType();
  const auto addr = reqInfo->getRequest()->getOutAddress();
  if (RESTType == OptimizerRequestInfo::RESTRequestType::REST_UNDEF)
  {
    const auto addr = requestInfo->getRequest()->getOutAddress();
    auto resp = optilab::serviceutils::getErrorResponse("Invalid REST request type");
    resp->setResponseAddress(addr);
    return resp;
  }

  if (requestType == ConnectorRequestInfo::RequestType::TYPE_UNDEF)
  {
    return serviceutils::getUndefinedRequestResponse(addr);
  }

  switch (requestType)
  {
    case ConnectorRequestInfo::RequestType::CONNECTOR:
    {
      if (RESTType == OptimizerRequestInfo::RESTRequestType::POST)
      {
        return resolveCreateConnectorRequest(requestInfo, connectorApi);
      }
      else if (RESTType == OptimizerRequestInfo::RESTRequestType::DELETE)
      {
        return resolveDeleteConnectorEngineRequest(requestInfo, connectorApi);
      }
      else
      {
        return serviceutils::getUndefinedRequestResponse(addr);
      }
    }
    case ConnectorRequestInfo::RequestType::EXECUTE_CONNECTOR:
    {
      if (RESTType == OptimizerRequestInfo::RESTRequestType::POST)
      {
        return resolveLoadConnectionsRequest(requestInfo, connectorApi);
      }
      else if (OptimizerRequestInfo::RESTRequestType::PUT)
      {
        return resolveRunConnectorRequest(requestInfo, connectorApi);
      }
      else if (OptimizerRequestInfo::RESTRequestType::DELETE)
      {
        return resolveKillConnectorRequest(requestInfo, connectorApi);
      }
      else
      {
        return serviceutils::getUndefinedRequestResponse(addr);
      }
    }
    case ConnectorRequestInfo::RequestType::WAIT:
    {
      if (RESTType == OptimizerRequestInfo::RESTRequestType::POST)
      {
        return resolveEngineWaitRequest(reqInfo, connectorApi);
      }
      else
      {
        return serviceutils::getUndefinedRequestResponse(addr);
      }
    }
    default:
      return serviceutils::getUndefinedRequestResponse(addr);
  }  // switch
}  // resolveRequest

OptimizerResponse::SPtr ConnectorRequestResolver::resolveCreateConnectorRequest(
    const OptimizerRequestInfo::SPtr& requestInfo, const ConnectorApi::SPtr& optimizerApi)
{
  // Get the address to send the replies to and the engine identifier to create
  const auto addr = requestInfo->getRequest()->getOutAddress();
  const OptilabRequestMessage& reqMsg = requestInfo->getRequest()->getRequestContent();

  // Get the Id of the engine to create
  const std::string& engineId = reqMsg.messageinfo();

  // On creation request map the engine identifier to the address to send the callback replies to
  ServiceCallbackHandler::SPtr callbackHandler =
      std::dynamic_pointer_cast<ServiceCallbackHandler>(optimizerApi->getEngineHandler());
  if (!callbackHandler)
  {
    const std::string errMsg = "OptimizerRequestResolver - empty/wrong callback handler";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }
  callbackHandler->mapEngineIdToAddress(engineId, addr);

  ASSERT_EXTENSION(reqMsg.details(), CreateConnectorReq, addr);

  CreateConnectorReq createConnectorReq;
  reqMsg.details().UnpackTo(&createConnectorReq);

  // Get the framework of the engine to create
  const auto connectorType = createConnectorReq.connectortype();

  // Send the request to the engine
  optilab::SolverError reqErr;
  switch(connectorType)
  {
    case ::optilab::CreateConnectorReq::ConnectorType::CreateConnectorReq_ConnectorType_COMBINATOR:
    {
      reqErr = optimizerApi->createConnector(engineId, EngineClassType::EC_COMBINATOR);
      break;
    }
    default:
    {
      const std::string errMsg = "ConnectorRequestResolver - unavailable connector engine";
      spdlog::error(errMsg);
      throw std::runtime_error(errMsg);
    }
  }

  if (reqErr == optilab::SolverError::kError)
  {
    // On error remove the mapping right-away
    callbackHandler->removeMappingBetweenEngineIdAndAddress(engineId);
  }

  return serviceutils::getSynchRequestResponse(reqErr, addr);
}  // resolveCreateConnectorRequest

OptimizerResponse::SPtr ConnectorRequestResolver::resolveDeleteConnectorEngineRequest(
    const OptimizerRequestInfo::SPtr& requestInfo, const ConnectorApi::SPtr& optimizerApi)
{
  // Get the address of the requester and the engine identifier to delete
  const auto addr = requestInfo->getRequest()->getOutAddress();
  const OptilabRequestMessage& reqMsg = requestInfo->getRequest()->getRequestContent();

  // Get the Id of the engine to create
  const std::string& engineId = reqMsg.messageinfo();

  // On deletion request remove the mapping between the engine identifier to the address
  // of the requester
  ServiceCallbackHandler::SPtr callbackHandler =
      std::dynamic_pointer_cast<ServiceCallbackHandler>(optimizerApi->getEngineHandler());
  if (!callbackHandler)
  {
    const std::string errMsg = "ConnectorRequestResolver - empty/wrong callback handler";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  const auto reqErr = optimizerApi->deleteConnector(engineId);
  if (reqErr == optilab::SolverError::kNoError)
  {
    // Remove the mapping only if there was no error in removing the engine.
    // If there was an error and the client tries to re-sent some requests,
    // the mapping should still be there
    callbackHandler->removeMappingBetweenEngineIdAndAddress(engineId);
  }

  return serviceutils::getSynchRequestResponse(reqErr, addr);
}  // resolveDeleteConnectorEngineRequest

OptimizerResponse::SPtr ConnectorRequestResolver::resolveLoadConnectionsRequest(
    const OptimizerRequestInfo::SPtr& requestInfo, const ConnectorApi::SPtr& optimizerApi)
{
  const auto addr = requestInfo->getRequest()->getOutAddress();
  const OptilabRequestMessage& reqMsg = requestInfo->getRequest()->getRequestContent();
  const std::string& engineId = reqMsg.messageinfo();

  ASSERT_EXTENSION(reqMsg.details(), ExecuteConnectorReq, addr);
  ExecuteConnectorReq modelReq;
  reqMsg.details().UnpackTo(&modelReq);

  const auto reqErr = optimizerApi->loadConnections(engineId, modelReq.connectionsdescriptor());
  return serviceutils::getSynchRequestResponse(reqErr, addr);
}  // resolveLoadConnectionsRequest

OptimizerResponse::SPtr ConnectorRequestResolver::resolveRunConnectorRequest(
    const OptimizerRequestInfo::SPtr& requestInfo, const ConnectorApi::SPtr& optimizerApi)
{
  const auto addr = requestInfo->getRequest()->getOutAddress();
  const OptilabRequestMessage& reqMsg = requestInfo->getRequest()->getRequestContent();
  const std::string& engineId = reqMsg.messageinfo();

  auto reqErr = optimizerApi->runConnector(engineId);
  if (reqErr == optilab::SolverError::kError)
  {
    spdlog::error("ConnectorRequestResolver - resolveRunConnectorRequest: "
        "error while running the connector, return");
    return serviceutils::getSynchRequestResponse(reqErr, addr);
  }
  reqErr = optimizerApi->collectSolutions(engineId);
  return serviceutils::getSynchRequestResponse(reqErr, addr);
}  // resolveRunConnectorRequest

OptimizerResponse::SPtr ConnectorRequestResolver::resolveKillConnectorRequest(
    const OptimizerRequestInfo::SPtr& requestInfo, const ConnectorApi::SPtr& optimizerApi)
{
  const auto addr = requestInfo->getRequest()->getOutAddress();
  const OptilabRequestMessage& reqMsg = requestInfo->getRequest()->getRequestContent();
  const std::string& engineId = reqMsg.messageinfo();

  const auto reqErr = optimizerApi->forceConnectorTermination(engineId);
  return serviceutils::getSynchRequestResponse(reqErr, addr);
}  // resolveKillConnectorRequest

OptimizerResponse::SPtr ConnectorRequestResolver::resolveEngineWaitRequest(
    const OptimizerRequestInfo::SPtr& requestInfo, const ConnectorApi::SPtr& optimizerApi)
{
  const auto addr = requestInfo->getRequest()->getOutAddress();
  const OptilabRequestMessage& reqMsg = requestInfo->getRequest()->getRequestContent();
  const std::string& engineId = reqMsg.messageinfo();

  ASSERT_EXTENSION(reqMsg.details(), OptimizerWaitReq, addr);
  OptimizerWaitReq waitReq;
  reqMsg.details().UnpackTo(&waitReq);

  const auto reqErr = optimizerApi->engineWait(engineId, waitReq.waittimeout());
  return serviceutils::getSynchRequestResponse(reqErr, addr);
}  // resolveEngineWaitRequest

}  // namespace optilab
