//
// Copyright OptiLab 2019. All rights reserved.
//
// Backend worker runner.
// It is the "client" for the back-end (sub) worker.
// It posts jobs to the back-end broker and receives responses asynchronously.
//
//              +---------------------+           ===================================
//              | BackendWorkerRunner |             Runner (acts as a sink as well)
//       +----->+----------+----------+<-------+  ===================================
//       |                 |                   |
//       |       +---------+----------+        |
//       |       |         |          |        |
//       |   +---+----+   ...     +---+----+   |  =====================
//       |   | Worker |           | Worker |   |    Worker pool layer
//       |   +---+----+           +---+----+   |  =====================
//       |       |                    |        |
//       +-------+                    +--------+
//
// For more information about the above pattern, see
// http://zguide.zeromq.org/page:all#Divide-and-Conquer
//

#pragma once

#include <atomic>
#include <condition_variable>
#include <cstddef>  // for std::size_t
#include <deque>
#include <memory>   // for std::shared_ptr
#include <mutex>
#include <string>
#include <thread>

#include <boost/shared_ptr.hpp>
#include <boost/thread.hpp>

#include <zeromq/zhelpers.hpp>

#include "engine/engine_constants.hpp"
#include "optilab_protobuf/optilab.pb.h"
#include "optimizer_service/socket_connector.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

/// Class that provides a callback method which is called when the back-end
/// worker runner receives a new message from the (sub) worker
class SYS_EXPORT_CLASS WorkerRunnerResponseCallback {
 public:
  using SPtr = std::shared_ptr<WorkerRunnerResponseCallback>;

 public:
  virtual ~WorkerRunnerResponseCallback() = default;

  /// On reply callback, the id is the id of the request that produced the reply
  virtual void onReply(int id, const OptiLabReplyMessage& reply) = 0;

  /// On error callback, the id is the id of the request that produced the error
  virtual void onError(int id) = 0;
};

/// Request message for workers.
/// The request has:
/// - a unique identifier "id";
/// - the framework type of the engine it is addressed to;
/// - the class type the engine it is addressed to;
/// - the package the request should be handled with
/// - the payload.
struct WorkerRequestMessage {
  int messageId;
  std::string engineId;
  EngineFrameworkType frameworkType;
  EngineClassType classType;
  OptimizerPkg packageType;
  std::string payload;

  // Flag indicating whether or not this is a delete job request
  bool deleteRequest{false};
};

class SYS_EXPORT_CLASS BackendWorkerRunner {
public:
  using SPtr = std::shared_ptr<BackendWorkerRunner>;

public:
  /// Constructor: creates a new worker runner connecting to the back-end pull of workers.
  /// It needs the sender port and address, and the sink port and address.
  /// The sender port is the port used by this worker to send requests to (i.e., the
  /// pool of worker's port).
  /// The sink port is the port listening to the answers from the pool of workers
  /// port and address.
  /// The controller port is the broadcasting port used to control the pool of workers.
  /// This port won't take actual models but only "controlling" actions.
  /// @note if the addresses are empty, "localhost" will be used.
  /// @note throws std::invalid_argument if the ports are less than 1 or greater than
  /// optimizerservice::MAX_SERVICE_PORT_NUMBER
  BackendWorkerRunner(const int senderPort, const int sinkPort, const int controllerPort,
                      const std::string& addrSender = std::string());

  virtual ~BackendWorkerRunner();

  /// Initializes the runner and connects it to the back-end thread pool
  void startRunner();

  /// Stops the runner and turns off all the threads
  void stopRunner();

  /// Returns true if the worker runner is currently running, returns false otherwise
  inline bool isRunning() const noexcept { return pWorkerRunnerRunning; }

  /// Sets the callback method to be invoked when a back-end reply is received
  inline void setResponseCallback(WorkerRunnerResponseCallback::SPtr callback)
  {
    pRunnerCallback = callback;
  }

  /// Send a request to one of the back-end workers in the worker-pool.
  /// @note this method is asynchronous, i.e., it sends the request and returns asap
  void sendRequestToWorker(const WorkerRequestMessage& request);

private:
  using ContextSPtr = std::shared_ptr<zmq::context_t>;
  using SocketSPtr = std::shared_ptr<zmq::socket_t>;

  using UniqueLock = boost::unique_lock<boost::mutex>;

  // Queue of requests
  using RequestQueue = std::deque<OptilabRequestMessage>;

private:
  /// Port for worker-pool to connect to
  const int pSenderPort;

  /// Port for the sink receiving messages from the worker-pool
  const int pSinkPort;

  /// Port used to control turn-down of the worker pool
  const int pControllerPort;

  /// Address for worker-pool to connect to
  const std::string pSenderAddr;

  /// ZMQ context the runner uses for network communication
  ContextSPtr pContext;

  /// ZMQ socket the runner uses for network communication.
  /// This is the socket used to send messages to the worker pool
  SocketSPtr pWorkerPoolSenderSocket;

  /// ZMQ socket used as sink
  SocketSPtr pSinkReceiverSocket;

  /// ZMQ socket used as controller for the worker pool
  SocketSPtr pWorkerPoolControllerSocket;

  /// Thread and future running the front-end reading process
  boost::shared_ptr<boost::thread> pFrontendMessageReader;
  boost::unique_future<void> pMessageReaderFuture;

  /// Thread and future running the back-end sink process
  boost::shared_ptr<boost::thread> pSinkMessageReader;
  boost::unique_future<void> pSinkReaderFuture;

  /// Flag to indicate wether the reader thread should terminate or not
  std::atomic<bool> pTerminateReader{false};

  /// Flag indicating whether the runner is running or not
  bool pWorkerRunnerRunning{false};

  /// Callback method used by the runner
  WorkerRunnerResponseCallback::SPtr pRunnerCallback;

  /// Mutex used to synchronize requests/replies messages
  boost::mutex pRequestResponseStateMutex;

  /// Condition variable used to synchronize requests/replies messages
  boost::condition_variable pRequestResponseStateConditionVariable;

  /// Queue of requests
  RequestQueue pRequestQueue;

  /// Starts the thread listening for incoming messages from the pool of workers.
  /// This is the "sink" thread
  void startSinkServiceListener();

  /// Starts the thread listening for incoming requests to be sent to the pool of workers
  void startServiceListener();

  /// Handles all incoming messages from the pool of workers
  void handleMessagesFromWorkersPool();

  /// Handles all incoming requests to the runner
  void handleIncomingRequests();

  /// Signals the sink that a batch of jobs is about to be sent
  void setSinkReady();

  /// Terminates the context, unblocking all blocked threads on some socket reads.
  /// This also signals the sink to stop receiving data
  void turnDownSocketAndContext();

  /// Notifies the sink with the given message
  void notifySink(const std::string& msg);
};

}  // namespace optilab
