#include "optimizer_service/socket_connector.hpp"

#include <stdexcept>   // for std::runtime_error

#include <spdlog/spdlog.h>

#include "optilab_protobuf/optilab.pb.h"
#include "optimizer_service/service_constants.hpp"

namespace optilab {

SocketConnector::SocketConnector(const SocketSPtr& socket)
: pSocket(socket)
{
  if (!socket)
  {
    throw std::runtime_error("SocketConnector - empty socket");
  }
}

SocketConnector::~SocketConnector()
{
  closeSockets();
}

void SocketConnector::closeSockets()
{
  // Set linger time to zero and close the socket
  if (pSocket)
  {
    int val = 0;
    zmq_setsockopt(pSocket.get(), ZMQ_LINGER, &val, sizeof(val));
    zmq_close(pSocket.get());
    pSocket.reset();
  }
}  // closeSockets

OptimizerRequest::SPtr SocketConnector::readMessage()
{
  /*
   * The broker sends a message to the worker using a ZMQ_ROUTER socket.
   * The pattern is the following:
   *
   * 1 - s_sendmore(socket, worker_address);
   * 2 - s_sendmore(socket, client_address);
   * 3 - s_send(socket, request);
   *
   * The first message (1) is consumed by the ZMQ_ROUTER socket to address the message to
   * the proper worker. This means that the worker receives (a) client_address and; (b) the request.
   * According to the contract between broker and worker, the client_address (a) is the address
   * of the client expecting the worker's reply.
   *
   */
  std::string addr;
  std::string rawMsg;
  OptilabRequestMessage requestMsg;
  {
    LockGuard lock(pInputSocketMutex);

    try
    {
      // First frame contains the address of the sender, i.e., the "client_address"
      addr = s_recv(*pSocket);

      // Second frame contains the raw message, i.e., the "request"
      rawMsg = s_recv(*pSocket);
    }
    catch(...)
    {
      spdlog::error("SocketConnector - readMessage: error in reading from socket");
      throw;
    }

    if (rawMsg.empty())
    {
      const std::string errMsg = "SocketConnector - readMessage: "
          "received an empty message from the socket";
      spdlog::error(errMsg);
      throw std::runtime_error(errMsg);
    }
  }  // critical section

  try
  {
    requestMsg.ParseFromString(rawMsg);
  }
  catch (...)
  {
    return OptimizerRequest::SPtr();
  }

  return std::make_shared<OptimizerRequest>(addr, requestMsg);
}  // readMessage

void SocketConnector::sendRawMessage(const std::string& rawMsg, const std::string& addr)
{
  /*
   * The worker sends a message to the broker.
   * s_send(...)
   * sends the address of the sender as first frame of the message.
   * Therefore
   * s_send(socket, msg)
   * requires
   * s_recv(socket) -> to get the address of the sender
   * s_recv(socket) -> to get the message from the sender
   * This doesn't happen with s_sendmore(...) which doesn't add the address of the sender
   * to the socket envelope.
   *
   * Example:
   *
   * s_sendmore(socket, "msg_1");
   * s_sendmore(socket, "msg_2");
   * s_send(socket, "msg_3");
   *
   * Creates the following envelope:
   *
   * | Address sender |
   * +----------------+
   * | msg_1          |
   * +----------------+
   * | msg_2          |
   * +----------------+
   * | msg_3          |
   * +----------------+
   *
   * That is popped from the top by the broker:
   * s_recv(...) -> returns the address of the sender
   * s_recv(...) -> returns "msg_1"
   * s_recv(...) -> returns "msg_2"
   * s_recv(...) -> returns "msg_3"
   *
   */
  {
    // Critical section
    LockGuard lock(pOutputSocketMutex);

    s_sendmore(*pSocket, addr);
    s_send(*pSocket, rawMsg);
  }
}  // sendRawMessage

void SocketConnector::sendMessage(const OptimizerResponse::SPtr& response)
{
  if (!response)
  {
    spdlog::error("SocketConnector - empty message");
    throw std::runtime_error("Empty response");
  }

  if ((response->getResponseAddress()).empty())
  {
    spdlog::error("SocketConnector - empty response address");
    throw std::runtime_error("Empty receiver address");
  }

  {
    // Critical section (lock is done on the "sendRawMessage(...)" method

    // Serialize the message to send back to the client
    std::string replyMsg;
    try
    {
      const OptiLabReplyMessage& rep = response->getReplyContent();
      replyMsg = rep.SerializeAsString();
    }
    catch(...)
    {
      return;
    }

    // Send the message back to the broker.
    // The broker will receive:
    // 1 - Address of this worker;
    // 2 - the address of the client to respond to;
    // 3 - the actual reply.
    // Which would require 3 corresponding s_recv(...) calls from the broker side
    sendRawMessage(replyMsg, response->getResponseAddress());
  }
}  // sendMessage

SimpleSocketConnector::SimpleSocketConnector(const SocketSPtr& inSocket,
                                             const SocketSPtr& outSocket)
: pInSocket(inSocket),
  pOutSocket(outSocket)
{
  if (!pInSocket)
  {
    throw std::runtime_error("SimpleSocketConnector - empty input socket");
  }

  if (!pOutSocket)
  {
    throw std::runtime_error("SimpleSocketConnector - empty output socket");
  }
}

SimpleSocketConnector::~SimpleSocketConnector()
{
  closeSockets();
}

void SimpleSocketConnector::closeSockets()
{
  // Set linger time to zero on all sockets
  if (pInSocket)
  {
    int val = 0;
    zmq_setsockopt(pInSocket.get(), ZMQ_LINGER, &val, sizeof(val));
    zmq_close(pInSocket.get());
    pInSocket.reset();
  }

  // Close all sockets
  if (pOutSocket)
  {
    int val = 0;
    zmq_setsockopt(pOutSocket.get(), ZMQ_LINGER, &val, sizeof(val));
    zmq_close(pOutSocket.get());
    pOutSocket.reset();
  }
}  // closeSockets

OptimizerRequest::SPtr SimpleSocketConnector::readMessage()
{
  static const std::string localhostName = std::string(optimizerservice::LOCAL_HOST_NAME);
  OptilabRequestMessage requestMsg;
  auto msg = readRawMessage();

  try
  {
    requestMsg.ParseFromString(msg);
  }
  catch (...)
  {
    return OptimizerRequest::SPtr();
  }

  return std::make_shared<OptimizerRequest>(localhostName, requestMsg);
}  // readMessage

void SimpleSocketConnector::sendMessage(const OptimizerResponse::SPtr& response)
{
  if (!response)
  {
    spdlog::error("SimpleSocketConnector - empty message");
    throw std::runtime_error("Empty response");
  }

  if ((response->getResponseAddress()).empty())
  {
    spdlog::error("SimpleSocketConnector - empty response address");
    throw std::runtime_error("Empty receiver address");
  }

  {
    // Critical section (lock is done on the "sendRawMessage(...)" method

    // Serialize the message to send back to the socket
    std::string replyMsg;
    try
    {
      const OptiLabReplyMessage& rep = response->getReplyContent();
      replyMsg = rep.SerializeAsString();
    }
    catch(...)
    {
      return;
    }

    // Send the message back to the socket
    sendRawMessage(replyMsg);
  }
}  // sendMessage

std::string SimpleSocketConnector::readRawMessage()
{

  {
    // critical section
    LockGuard lock(pInputSocketMutex);
    return s_recv(*pInSocket);
  }
}  // readMessage

/// Non blocking call, sends the given string to the given address on the
/// socket specified in the constructor of this class.
/// @note this method is thread-safe
void SimpleSocketConnector::sendRawMessage(const std::string& msg)
{
  {
    // Critical section
    LockGuard lock(pOutputSocketMutex);
    s_send(*pOutSocket, msg);
  }
}  // sendMessage

}  // namespace optilab
