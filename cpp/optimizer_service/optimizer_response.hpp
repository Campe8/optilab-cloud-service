//
// Copyright OptiLab 2019. All rights reserved.
//
// Response to send back to the client.
//

#pragma once

#include <memory>  // for std::shared_ptr
#include <string>

#include "optilab_protobuf/optilab.pb.h"

#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS OptimizerResponse {
 public:
  using SPtr = std::shared_ptr<OptimizerResponse>;

 public:
  /// Constructs a new optimizer response.
  /// @note this response will need a receiver address to be set before sending it on the socket!
  explicit OptimizerResponse(const OptiLabReplyMessage& replyMessage);

  /// Constructs a new optimizer response which will be sent to the given address.
  /// @note throws std::invalid_argument if the address is empty
  OptimizerResponse(const std::string& addr, const OptiLabReplyMessage& replyMessage);

  /// Overrides the current response address.
  /// @note throws std::invalid_argument for empty addresses
  void setResponseAddress(const std::string& addr);

  /// Returns the address of the receiver of this response
  const std::string& getResponseAddress() const { return pAddress; }

  /// Returns the content of this reply
  inline const OptiLabReplyMessage& getReplyContent() const { return pReplyContent; }

  /// Checks the internal response content and returns true if it is an error response.
  /// Returns false otherwise
  bool isErrorResponse() const;

 private:
  /// Address of the client this reponse is directed to
  std::string pAddress;

  /// Content of the response
  OptiLabReplyMessage pReplyContent;
};

}  // namespace optilab
