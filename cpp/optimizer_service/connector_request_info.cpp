#include "optimizer_service/connector_request_info.hpp"

#include <stdexcept>  // for std::runtime_error

#include "optilab_protobuf/optilab.pb.h"

namespace optilab {

ConnectorRequestInfo::ConnectorRequestInfo(const OptimizerRequest::SPtr& request)
: OptimizerRequestInfo(request)
{
  initializeRequestInfo(request);
}

void ConnectorRequestInfo::initializeRequestInfo(const OptimizerRequest::SPtr& request)
{
  // Request type
  const OptilabRequestMessage& reqMsg = request->getRequestContent();
  if (reqMsg.details().Is<CreateConnectorReq>())
  {
    pRequestType = ConnectorRequestInfo::RequestType::CONNECTOR;
  }
  else if (reqMsg.details().Is<ExecuteConnectorReq>())
  {
    pRequestType = ConnectorRequestInfo::RequestType::EXECUTE_CONNECTOR;
  }
  else if (reqMsg.details().Is<OptimizerWaitReq>())
  {
    pRequestType = ConnectorRequestInfo::RequestType::WAIT;
  }
  else
  {
    throw std::runtime_error("ConnectorRequestInfo - unrecognized request type");
  }
}  // initializeRequestInfo

}  // namespace optilab
