#include "optimizer_service/service_worker_config.hpp"

#include "optimizer_service/service_constants.hpp"

namespace optilab {

ServiceWorkerConfig::ServiceWorkerConfig(const std::string& hostName, int port, bool useTCP)
: workerIOThreads(serviceworker::WORKER_DEFAULT_IO_NUM_THREADS),
  useTCPPort(useTCP),
  workerHostName(hostName),
  workerHostPort(port),
  workerNumDispatcherThreads(0)
{
}

std::string ServiceWorkerConfig::getSocketAddress() const
{
  std::string addr = useTCPPort ?
      serviceworker::WORKER_PREFIX_TCP_NETWORK_ADDR :
      serviceworker::WORKER_PREFIX_IPC_NETWORK_ADDR;

  addr += workerHostName;

  if (useTCPPort)
  {
    addr += ":";
    addr += std::to_string(workerHostPort);
  }
  else
  {
    addr += "_";
    addr += std::to_string(workerHostPort);
    addr += serviceworker::WORKER_PREFIX_IPC_NETWORK_ADDR_EXT;
  }

  return addr;
}  // getSocketAddress

TPoolServiceWorkerConfig::TPoolServiceWorkerConfig(const std::string& hostName, int inPort,
                                                   int outPort, int controllerPort, bool useTCP)
: workerIOThreads(serviceworker::WORKER_DEFAULT_IO_NUM_THREADS),
  useTCPPort(useTCP),
  workerHostName(hostName),
  workerInPort(inPort),
  workerOutPort(outPort),
  workerControllerPort(controllerPort),
  workerNumDispatcherThreads(0)
{
}

std::string TPoolServiceWorkerConfig::getSocketAddress(int port) const
{
  std::string addr = useTCPPort ?
      serviceworker::WORKER_PREFIX_TCP_NETWORK_ADDR :
      serviceworker::WORKER_PREFIX_IPC_NETWORK_ADDR;

  addr += workerHostName;

  if (useTCPPort)
  {
    addr += ":";
    addr += std::to_string(port);
  }
  else
  {
    addr += "_";
    addr += std::to_string(port);
    addr += serviceworker::WORKER_PREFIX_IPC_NETWORK_ADDR_EXT;
  }

  return addr;
}  // getSocketAddress

}  // namespace optilab
