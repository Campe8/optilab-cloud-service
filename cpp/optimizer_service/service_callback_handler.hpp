//
// Copyright OptiLab 2019-2020. All rights reserved.
//
// Callback handler for the service.
// On callback it sends results back to the broker.
//

#pragma once

#include <memory>
#include <mutex>
#include <string>

#include <sparsepp/spp.h>

#include <google/protobuf/message.h>

#include "engine/engine_callback_handler.hpp"
#include "optimizer_service/socket_connector.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS ServiceCallbackHandler : public EngineCallbackHandler {
 public:
  using SPtr = std::shared_ptr<ServiceCallbackHandler>;

 public:
  /// Constructor.
  /// @note throws std::invalid_argument on empty SocketConnector pointer
  ServiceCallbackHandler(BaseSocketConnector::SPtr connector);

  /// Callback for metrics
  void metricsCallback(const std::string& engineId, const std::string& jsonMetrics) override;

  /// Callback for meta-data
  void metadataCallback(const std::string& engineId, const std::string& jsonMetadata) override;

  /// Callback for optimizers to communicate process completion
  void processCompletionCallback(const std::string& engineId) override;

  /// Callback for optimizers results
  void resultCallback(const std::string& engineId, const std::string& jsonResult) override;

  /// Callback for optimizer results
  void resultCallback(const std::string& engineId,
                      const ::google::protobuf::Message& protobufResult) override;

  /// Maps the given engine id to the given address used by the internal socket connector.
  /// @note this method is thread-safe.
  void mapEngineIdToAddress(const std::string& engineId, const std::string& addr);

  /// Removes the mapping between the given engine id and given address if present.
  /// @note this method is thread-safe.
  void removeMappingBetweenEngineIdAndAddress(const std::string& engineId);

 private:
  using EngineAddrMap = spp::sparse_hash_map<std::string, std::string>;

  using LockGuard = std::lock_guard<std::mutex>;

 private:
  /// Socket connector used to send back responses to the broker
  BaseSocketConnector::SPtr pSocketConnector;

  /// Map storing the pairs <engine_id, address> used
  /// to re-direct the callbacks to the proper socket address
  EngineAddrMap pEngineAddressMap;

  /// Mutex used to protect concurrent accesses to the internal engine-address map
  std::mutex pEngineAddrMapMutex;

  /// Utility function: returns the address mapped to the given engine id.
  /// @note this method is thread-safe.
  /// @note throws std::runtime_error if the engine id is not found in the internal map
  std::string getAddressForEngineId(const std::string& engineId);
};

}  // namespace optilab
