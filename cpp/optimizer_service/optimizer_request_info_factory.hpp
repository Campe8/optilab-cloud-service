//
// Copyright OptiLab 2019. All rights reserved.
//
// Factory class for creating different types of
// requests info instances depending on the type
// of request.
//

#pragma once

#include <memory>  // for std::shared_ptr

#include "optimizer_service/optimizer_request_info.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS OptimizerRequestInfoFactory {
 public:
  using SPtr = std::shared_ptr<OptimizerRequestInfoFactory>;

 public:
  /// Creates a new request info pointer w.r.t. the given optimizer request framework.
  /// @note throws std::invalid_argument on empty request pointer
  OptimizerRequestInfo::SPtr buildRequestInfo(const OptimizerRequest::SPtr& request);
};

}  // namespace optilab
