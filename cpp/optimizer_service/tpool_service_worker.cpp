#include "optimizer_service/tpool_service_worker.hpp"

#include <cassert>
#include <chrono>
#include <functional>  // for std::bind
#include <stdexcept>   // for std::runtime_error
#include <thread>
#include <utility>     // for std::pair

#include <spdlog/spdlog.h>

#include "optimizer_service/optimizer_request.hpp"
#include "optimizer_service/service_constants.hpp"

namespace optilab {

TPoolServiceWorker::TPoolServiceWorker(
    const std::string& id, const TPoolServiceWorkerConfig& config,
    OptimizerRequestDispatcherFactory::SPtr optimizerDispatcherFactory)
: pWorkerId(id),
  pSWConfig(config),
  pShutDown(false),
  pWorkDispatcher(std::make_shared<ThreadPool>((config.workerNumDispatcherThreads == 0) ?
      serviceworker::WORKER_DEFAULT_DISPATCHER_NUM_THREADS : config.workerNumDispatcherThreads)),
  pContext(nullptr),
  pControllerSocket(nullptr),
  pConnector(nullptr),
  pOptimizerDispatcherFactory(optimizerDispatcherFactory)
{
  spdlog::info("TPoolServiceWorker - creating worker " + pWorkerId);
  if (!pOptimizerDispatcherFactory)
  {
    const std::string errMsg =
        "TPoolServiceWorker - empty pointer to the optimizer dispatcher factory";
    spdlog::error(errMsg);
    throw std::invalid_argument(errMsg);
  }
}

int TPoolServiceWorker::run()
{
  {
    // Critical section.
    // Check that the worker doesn't run twice
    LockGuard lock(pWorkerStateMutex);
    if (pState != INIT)
    {
      throw std::runtime_error(std::string("TPoolServiceWorker ") + pWorkerId +
                               " is being run more than once");
    }
  }

  try
  {
    // Initialize the context for the worker
    pContext = std::make_shared<zmq::context_t>(pSWConfig.workerIOThreads);
    auto inSocket = std::make_shared<zmq::socket_t>(*pContext, ZMQ_PULL);
    auto outSocket = std::make_shared<zmq::socket_t>(*pContext, ZMQ_PUSH);
    pControllerSocket = std::make_shared<zmq::socket_t>(*pContext, ZMQ_SUB);

    const std::string inAddr = pSWConfig.getSocketAddress(pSWConfig.workerInPort);
    const std::string outAddr = pSWConfig.getSocketAddress(pSWConfig.workerOutPort);
    const std::string controllerAddr = pSWConfig.getSocketAddress(pSWConfig.workerControllerPort);
    inSocket->connect(inAddr.c_str());
    outSocket->connect(outAddr.c_str());
    pControllerSocket->connect(controllerAddr.c_str());
    pControllerSocket->setsockopt(ZMQ_SUBSCRIBE, "", 0);

    spdlog::info("TPoolServiceWorker - worker " + pWorkerId +
                 " connecting to input address " + inAddr);
    spdlog::info("TPoolServiceWorker - worker " + pWorkerId +
                 " connecting to output address " + outAddr);
    spdlog::info("TPoolServiceWorker - worker " + pWorkerId +
                 " connecting to controller address " + controllerAddr);

    pConnector = std::make_shared<SimpleSocketConnector>(inSocket, outSocket);
  }
  catch (...)
  {
    LockGuard lock(pWorkerStateMutex);
    pState = TERMINATE;

    throw;
  }

  // Set state to LISTENING
  {
    LockGuard lock(pWorkerStateMutex);
    pState = LISTENING;
  }

  // Create the callback handler for callbacks responses to the broker
  pCallbackHandler = std::make_shared<ServiceCallbackHandler>(pConnector);

  // Set the callback handler on all APIs used by this worker
  // @note all the APIs use the same callback handler
  for (const auto& apiIter : *(pOptimizerDispatcherFactory->getOptimizerApiMapPtr()))
  {
    for (auto& apiPtr : apiIter.second)
    {
      apiPtr->setEngineHandler(pCallbackHandler);
    }
  }

  // Start controller thread
  std::thread controllerThread(&TPoolServiceWorker::checkWorkerLifeComm, this, pConnector);

  // Create a new socket connector and start listening for incoming messages
  spdlog::info("TPoolServiceWorker - worker " + pWorkerId + " listening on socket");
  const int errorStatus = listenOnSocket(pConnector);

  // Join the thread
  controllerThread.join();

  // Shutdown executor
  pWorkDispatcher->shutdown();

  return errorStatus;
}  // run

void TPoolServiceWorker::turnDown()
{
  {
    // Critical section
    LockGuard lock(pWorkerStateMutex);

    if (!pContext) return;

    // Cleanup socket and context
    spdlog::info("TPoolServiceWorker - worker " + pWorkerId + " cleanup");

    // Shutdown and terminate the context.
    // @note this will unblock all blocked threads
    if (pContext)
    {
      // Close all the open sockets in the conntector
      pConnector->closeSockets();

      // Shutdown the context
      zmq_ctx_shutdown(pContext.get());
      zmq_ctx_term(pContext.get());
    }

    // Context cleanup
    pContext.reset();
  }
}  // turnDown

void TPoolServiceWorker::checkWorkerLifeComm(SimpleSocketConnector::SPtr connector)
{
  if (!pControllerSocket)
  {
    throw std::runtime_error("TPoolServiceWorker - checkWorkerLifeComm: empty controller socket");
  }

  // Loop until the service is not down
  while (pContext)
  {
    try
    {
      std::string smessage = s_recv(*pControllerSocket);
      if (smessage == std::string(serviceworker::WORKER_MESSAGE_TURN_DOWN) ||
          (!pWorkDispatcher->isRunning()))
      {
        spdlog::info("TPoolServiceWorker - checkWorkerLifeComm: "
            "received turn down message  " + pWorkerId);
        break;
      }
    }
    catch (zmq::error_t& e)
    {
      if (e.num() == ETERM)
      {
        spdlog::info("TPoolServiceWorker - checkWorkerLifeComm: "
            "closed socket, return " + pWorkerId);
        break;
      }
      break;
    }
    catch (...)
    {
      spdlog::error("TPoolServiceWorker - checkWorkerLifeComm: "
          "undefined error on socket " + pWorkerId);
      break;
    }
  }
}  // checkWorkerLifeComm

void TPoolServiceWorker::handleWorkDispatcher(SimpleSocketConnector::SPtr connector,
                                              OptimizerRequest::SPtr req)
{
  if (!connector)
  {
    std::string errMsg = "TPoolServiceWorker - handleWorkDispatcher: "
        "empty connector on worker " + pWorkerId;
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  if (!req)
  {
    std::string errMsg = "TPoolServiceWorker - handleWorkDispatcher: "
        "empty request on worker " + pWorkerId;
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  if (pShutDown || isShutdownRequest(req))
  {
    pShutDown = true;
    return;
  }

  // Create the dispatcher optimizer and run it
  // spdlog::error("BUILD AND RUN REQUEST " + pWorkerId);
  OptimizerRequestDispatcher::SPtr optimizerDispatcher =
      pOptimizerDispatcherFactory->build(connector, req);

  // Run the dispatcher
  optimizerDispatcher->run();
}  // handleWorkDispatcher

bool TPoolServiceWorker::isShutdownRequest(OptimizerRequest::SPtr req)
{
  if (!req)
  {
    throw std::runtime_error("TPoolServiceWorker - isShutdownRequest empty request");
  }

  // Using UNKNOWN as shutdown message.
  // TODO use a proper type for shutdown requests
  return req->getRequestType() == RequestType::UNKNOWN;
}  // isShutdownRequest

int TPoolServiceWorker::listenOnSocket(SimpleSocketConnector::SPtr connector)
{
  assert(connector);

  // zero means no error
  int errorStatus = 0;
  while (!pShutDown)
  {
    // Create a new request to send to the optimizer dispatcher
    OptimizerRequest::SPtr request;
    {
      // Critical section: lock the mutex to read from the socket
      LockGuardRecursive lock(pDispatcherMutex);

      // The request is created by the socket connector upon reading from the socket
      try
      {
        // Read and save the message
        // @note this is a blocking call waiting until there is an incoming message
        request = connector->readMessage();
      }
      catch(zmq::error_t& e)
      {
        if (e.num() == ETERM)
        {
          spdlog::info("TPoolServiceWorker - listenOnSocket: closed socket, return: " + pWorkerId);
          break;
        }

        errorStatus = 1;
        break;
      }
      catch (...)
      {
        spdlog::error("TPoolServiceWorker - listenOnSocket: undefined error "
            "while reading from socket");
        errorStatus = 1;
        break;
      }

      // Release the mutex to allow other threads to read incoming messages
    }

    // Check if the previous request was a shutdown request, if so, return asap
    if (isShutdownRequest(request))
    {
      pShutDown = true;

      // Turn down the context (if not already down) and return
      turnDown();
      return errorStatus;
    }

    // Dispatch the handling of message to one of the threads in the thread pool
    pWorkDispatcher->execute(
              std::bind(&TPoolServiceWorker::handleWorkDispatcher, this, connector, request));

    if (pShutDown)
    {
      return errorStatus;
    }
  }

  return errorStatus;
}  // listenOnSocket

}  // namespace optilab
