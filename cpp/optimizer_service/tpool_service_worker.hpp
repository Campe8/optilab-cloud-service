//
// Copyright OptiLab 2019. All rights reserved.
//
// Worker part of a pool implementing back-end optimizer service.
// It connects to the specified sockets, receives requests, runs
// optimizers, and returns responses.
//

#pragma once

#include <atomic>
#include <memory>  // for std::shared_ptr
#include <mutex>
#include <string>

#include "zeromq/zhelpers.hpp"

#include "optimizer_service/optimizer_request_dispatcher_factory.hpp"
#include "optimizer_service/service_callback_handler.hpp"
#include "optimizer_service/service_worker_config.hpp"
#include "optimizer_service/socket_connector.hpp"
#include "data_structure/thread/thread_pool.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS TPoolServiceWorker {
public:
  /// The state this worker is in
  enum WorkerState
  {
    /// Service instantiated but not yet listening
    INIT = 0,

    /// Service listening and accepting incoming messages
    LISTENING,

    /// Service no longer accepting connections (cannot transition out of this state)
    TERMINATE
  };

  using SPtr = std::shared_ptr<TPoolServiceWorker>;

public:
  /// Constructor.
  /// @note throws std::invalid_argument on empty pointer to OptimizerRequestDispatcherFactory
  TPoolServiceWorker(const std::string& id, const TPoolServiceWorkerConfig& config,
                     OptimizerRequestDispatcherFactory::SPtr optimizerDispatcherFactory);

  /// Returns the id of this worker
  inline const std::string& getWorkerId() const { return pWorkerId; }

  /// Starts the service worker which will be listening for incoming messages on the port
  /// specified in the constructor's config.
  int run();

private:
  using LockGuard = std::lock_guard<std::mutex>;
  using LockGuardRecursive = std::lock_guard<std::recursive_mutex>;

private:
  /// Identifier of this worker
  const std::string pWorkerId;

  /// State this worker is in
  WorkerState pState{WorkerState::INIT};

  /// Configuration data structure for this worker
  TPoolServiceWorkerConfig pSWConfig;

  /// Mutex to protect the state of this worker if handled by parallel threads
  std::mutex pWorkerStateMutex;

  /// Mutex to sync. dispatcher threads on the input socket
  std::recursive_mutex pDispatcherMutex;

  /// Flag indicating whether or not to shut down the workers
  std::atomic<bool> pShutDown;

  /// This is the dispatcher thread pool, i.e., the thread poll dispatching requests
  /// from the client among the threads in the thread pool handling requests
  std::shared_ptr<ThreadPool> pWorkDispatcher;

  /// Context for the sockets
  std::shared_ptr<zmq::context_t> pContext;

  /// Controller socket for this worker life-time management
  std::shared_ptr<zmq::socket_t> pControllerSocket;

  /// Socket connector
  SimpleSocketConnector::SPtr pConnector;

  /// Factory to build optimizer dispatchers upon receiving a new request from an open socket
  OptimizerRequestDispatcherFactory::SPtr pOptimizerDispatcherFactory;

  /// Instance of the handler used to send json responses to the broker through callbacks
  ServiceCallbackHandler::SPtr pCallbackHandler;

  /// Start the loop to listen for incoming messages.
  /// @note returns zero on success, non-zero otherwise
  int listenOnSocket(SimpleSocketConnector::SPtr connector);

  /// Dispatcher function used to dispatch work to threads in the thread pool
  void handleWorkDispatcher(SimpleSocketConnector::SPtr connector, OptimizerRequest::SPtr req);

  /// Returns true if the input is a shotdown request
  bool isShutdownRequest(OptimizerRequest::SPtr req);

  /// Method check for asynch. life management worker communications
  void checkWorkerLifeComm(SimpleSocketConnector::SPtr connector);

  /// Turns down the pool of workers and deletes the context
  void turnDown();
};

}  // namespace optilab
