#include "optimizer_service/optimizer_request.hpp"

#include <stdexcept>  // for std::invalid_argument

namespace optilab {

OptimizerRequest::OptimizerRequest(const std::string& addr, const OptilabRequestMessage& request)
: pAddress(addr),
  pRequestContent(request)
{
  if (pAddress.empty())
  {
    throw std::invalid_argument("OptimizerRequest: empty address");
  }

  // Set REST type
  pRequestType = request.requesttype();
}

}  // namespace optilab
