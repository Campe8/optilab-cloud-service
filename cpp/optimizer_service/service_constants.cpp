#include "optimizer_service/service_constants.hpp"

namespace optilab {

namespace serviceworker {

const int WORKER_DEFAULT_DISPATCHER_NUM_THREADS = 3;
const int WORKER_DEFAULT_IO_NUM_THREADS = 1;
const char WORKER_MESSAGE_READY[] = "READY";
const char WORKER_MESSAGE_TURN_DOWN[]= "TURN_DOWN" ;
const char WORKER_PREFIX_TCP_NETWORK_ADDR[] = "tcp://";
const char WORKER_PREFIX_IPC_NETWORK_ADDR[] = "ipc://";
const char WORKER_PREFIX_IPC_NETWORK_ADDR_EXT[] = ".ipc";

}  // namespace serviceworker

namespace optimizerservice {

const int DEFAULT_ZMQ_CONTEXT_NUM_THREADS = 1;
const char LOCAL_HOST_NAME[] = "localhost";
const int MAX_SERVICE_PORT_NUMBER = 65535;
const char WORKER_ID_PREFIX[] = "worker_";

}  // optimizerservice

}  // namespace optilab
