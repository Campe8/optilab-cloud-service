#include "optimizer_service/optimizer_request_dispatcher.hpp"

#include <spdlog/spdlog.h>

#include <cassert>
#include <exception>
#include <stdexcept>  // for std::runtime_error
#include <string>

#include "optimizer_api/connector_api.hpp"
#include "optimizer_api/cp_api.hpp"
#include "optimizer_api/data_api.hpp"
#include "optimizer_api/evolutionary_optimizer_api.hpp"
#include "optimizer_api/routing_api.hpp"
#include "optimizer_api/solver_api.hpp"
#include "optimizer_service/backend_service_request_info.hpp"
#include "optimizer_service/backend_service_request_resolver.hpp"
#include "optimizer_service/cp_request_resolver.hpp"
#include "optimizer_service/data_request_resolver.hpp"
#include "optimizer_service/connector_request_resolver.hpp"
#include "optimizer_service/evolutionary_request_resolver.hpp"
#include "optimizer_service/optimizer_service_utilities.hpp"
#include "optimizer_service/or_request_resolver.hpp"
#include "optimizer_service/routing_request_resolver.hpp"
#include "optimizer_service/scheduling_request_resolver.hpp"
#include "optimizer_service/service_constants.hpp"
#include "optimizer_service/solver_request_resolver.hpp"
#include "zeromq/zhelpers.hpp"

namespace optilab
{
OptimizerRequestDispatcher::OptimizerRequestDispatcher(
    const BaseSocketConnector::SPtr& connector,
    const OptimizerRequestInfo::SPtr& requestInfo,
    const FrameworkApi::FrameworkApiMapSPtr& frameworkApiMapPtr)
    : pConnector(connector),
      pRequestInfo(requestInfo),
      pFrameworkApiMapPtr(frameworkApiMapPtr)
{
  if (!pConnector)
  {
    throw std::runtime_error(
        "OptimizerRequestDispatcher: empty socket connector");
  }

  if (!pRequestInfo)
  {
    throw std::runtime_error("OptimizerRequestDispatcher: empty request info");
  }

  if (!pFrameworkApiMapPtr)
  {
    throw std::runtime_error(
        "OptimizerRequestDispatcher: empty framework API map");
  }

  // Initialize the map or request resolvers
  initializeResolversMap();
}

void OptimizerRequestDispatcher::initializeResolversMap()
{
  pRequestResolverMap[static_cast<int>(EngineFrameworkType::OR_FRAMEWORK)] =
      std::make_shared<SolverRequestResolver>();
  pRequestResolverMap[static_cast<int>(
      EngineFrameworkType::CONNECTORS_FRAMEWORK)] =
      std::make_shared<ConnectorRequestResolver>();
  pRequestResolverMap[static_cast<int>(
      EngineFrameworkType::BACKEND_SERVICE_FRAMEWORK)] =
      std::make_shared<BackendServiceRequestResolver>();
  pRequestResolverMap[static_cast<int>(EngineFrameworkType::EVOLUTIONARY_TOOLBOX)] =
      std::make_shared<EvolutionaryRequestResolver>();
  pRequestResolverMap[static_cast<int>(EngineFrameworkType::ROUTING_TOOLBOX)] =
      std::make_shared<RoutingRequestResolver>();
  pRequestResolverMap[static_cast<int>(EngineFrameworkType::SCHEDULING_TOOLBOX)] =
      std::make_shared<SchedulingRequestResolver>();
  pRequestResolverMap[static_cast<int>(EngineFrameworkType::DATA_AND_PORTS_TOOLBOX)] =
      std::make_shared<DataRequestResolver>();
  pRequestResolverMap[static_cast<int>(EngineFrameworkType::CP_TOOLBOX)] =
      std::make_shared<CPRequestResolver>();
  pRequestResolverMap[static_cast<int>(EngineFrameworkType::OR_TOOLBOX)] =
      std::make_shared<ORRequestResolver>();
}  // initializeResolversMap

void OptimizerRequestDispatcher::checkForRequestValidity()
{
  assert(pRequestInfo);
}  // checkForRequestValidity

FrameworkApi::SPtr OptimizerRequestDispatcher::getFrameworkApi(
    EngineFrameworkType frameworkType, OptimizerPkg optimizerPackage) const
{
  const int frameworkTypeIdx = static_cast<int>(frameworkType);
  if (frameworkType == EngineFrameworkType::CONNECTORS_FRAMEWORK)
  {
    return (pFrameworkApiMapPtr->at(frameworkTypeIdx)).front();
  }
  else if (frameworkType == EngineFrameworkType::EVOLUTIONARY_TOOLBOX)
  {
    return (pFrameworkApiMapPtr->at(frameworkTypeIdx)).front();
  }
  else if (frameworkType == EngineFrameworkType::ROUTING_TOOLBOX)
  {
    return (pFrameworkApiMapPtr->at(frameworkTypeIdx)).front();
  }
  else if (frameworkType == EngineFrameworkType::SCHEDULING_TOOLBOX)
  {
    return (pFrameworkApiMapPtr->at(frameworkTypeIdx)).front();
  }
  else if (frameworkType == EngineFrameworkType::DATA_AND_PORTS_TOOLBOX)
  {
    return (pFrameworkApiMapPtr->at(frameworkTypeIdx)).front();
  }
  else if (frameworkType == EngineFrameworkType::OR_TOOLBOX)
  {
    return (pFrameworkApiMapPtr->at(frameworkTypeIdx)).front();
  }
  else if (frameworkType == EngineFrameworkType::CP_TOOLBOX)
  {
    return (pFrameworkApiMapPtr->at(frameworkTypeIdx)).front();
  }
  else if (frameworkType == EngineFrameworkType::OR_FRAMEWORK)
  {
    const int packageTypeIdx = static_cast<int>(optimizerPackage);
    const auto apiList = pFrameworkApiMapPtr->at(frameworkTypeIdx);
    if (packageTypeIdx >= 0 && packageTypeIdx < apiList.size())
    {
      return apiList.at(packageTypeIdx);
    }
    else
    {
      const std::string errMsg =
          "OptimizerRequestDispatcher - "
          "getFrameworkApi: invalid package type. "
          "Package type " +
          std::to_string(packageTypeIdx);
      spdlog::error(errMsg);
      throw std::runtime_error(errMsg);
    }
  }
  else
  {
    const std::string errMsg =
        "OptimizerRequestDispatcher - "
        "getFrameworkApi: framework type not available. "
        "Framework type " +
        std::to_string(frameworkTypeIdx);
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }
}  // getFrameworkApi

std::pair<EngineFrameworkType, FrameworkApi::SPtr>
OptimizerRequestDispatcher::getFrameworkApi() const
{
  int frameworkType = static_cast<int>(pRequestInfo->getFrameworkType());
  if (pRequestInfo->getFrameworkType() ==
      EngineFrameworkType::BACKEND_SERVICE_FRAMEWORK)
  {
    // Special case for back-end framework.
    // For back-end framework requests, cast to the proper request info and get
    // the framework the request is actually addressed to
    auto ptr =
        std::dynamic_pointer_cast<BackendServiceRequestInfo>(pRequestInfo);
    if (!ptr)
    {
      const std::string errMsg =
          "OptimizerRequestDispatcher - "
          "wrong cast to BackendServiceRequestInfo";
      spdlog::error(errMsg);
      throw std::runtime_error(errMsg);
    }
    frameworkType = static_cast<int>(ptr->getEngineFrameworkType());

    return {pRequestInfo->getFrameworkType(),
            getFrameworkApi(ptr->getEngineFrameworkType(),
                            ptr->getOptimizerPackage())};
  }
  else
  {
    return {pRequestInfo->getFrameworkType(),
            getFrameworkApi(pRequestInfo->getFrameworkType(),
                            pRequestInfo->getOptimizerPackage())};
  }
}  // getOptimizerApi

void OptimizerRequestDispatcher::sendErrorResponse(
    const BaseSocketConnector::SPtr& connector, const std::string& errMessage)
{
  if (!connector)
  {
    throw std::runtime_error("OptimizerRequestDispatcher: empty connector");
  }

  auto resp = optilab::serviceutils::getErrorResponse(errMessage);
  resp->setResponseAddress(pRequestInfo->getRequest()->getOutAddress());
  connector->sendMessage(resp);
}  // sendErrorResponse

OptimizerResponse::SPtr OptimizerRequestDispatcher::dispatchRequestToResolver(
    EngineFrameworkType frameworkType, FrameworkApi::SPtr frameworkApi)
{
  assert(frameworkApi);
  const auto resolverIt =
      pRequestResolverMap.find(static_cast<int>(frameworkType));

  if (resolverIt == pRequestResolverMap.end())
  {
    const std::string errMsg =
        "OptimizerRequestDispatcher - resolver not found during "
        "dispatching the request for the framework type " +
        std::to_string(frameworkType);
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  switch (frameworkType)
  {
    case EngineFrameworkType::OR_FRAMEWORK:
    {
      auto api = std::dynamic_pointer_cast<SolverApi>(frameworkApi);
      SolverRequestResolver::SPtr resolver =
          std::dynamic_pointer_cast<SolverRequestResolver>(resolverIt->second);
      if (!api || !resolver)
      {
        throw std::runtime_error(
            "OptimizerRequestDispatcher - "
            "invalid cast to solver api/resolver");
      }
      return resolver->resolveRequest(pRequestInfo, api);
    }
    case EngineFrameworkType::CONNECTORS_FRAMEWORK:
    {
      auto api = std::dynamic_pointer_cast<ConnectorApi>(frameworkApi);
      ConnectorRequestResolver::SPtr resolver =
          std::dynamic_pointer_cast<ConnectorRequestResolver>(
              resolverIt->second);
      if (!api || !resolver)
      {
        throw std::runtime_error(
            "OptimizerRequestDispatcher - "
            "invalid cast to connector api/resolver");
      }
      return resolver->resolveRequest(pRequestInfo, api);
    }
    case EngineFrameworkType::BACKEND_SERVICE_FRAMEWORK:
    {
      BackendServiceRequestResolver::SPtr resolver =
          std::dynamic_pointer_cast<BackendServiceRequestResolver>(
              resolverIt->second);
      if (!resolver)
      {
        throw std::runtime_error(
            "OptimizerRequestDispatcher - "
            "invalid cast to connector api/resolver");
      }
      return resolver->resolveRequest(pRequestInfo, frameworkApi);
    }
    case EngineFrameworkType::DATA_AND_PORTS_TOOLBOX:
    {
      auto api =
          std::dynamic_pointer_cast<DataApi>(frameworkApi);
      DataRequestResolver::SPtr resolver =
          std::dynamic_pointer_cast<DataRequestResolver>(
              resolverIt->second);
      if (!resolver)
      {
        std::string errMsg = "OptimizerRequestDispatcher - "
                "invalid cast to data api/resolver";
        spdlog::error(errMsg);
        throw std::runtime_error(errMsg);
      }
      return resolver->resolveRequest(pRequestInfo, api);
    }
    case EngineFrameworkType::CP_TOOLBOX:
    {
      auto api = std::dynamic_pointer_cast<CPApi>(frameworkApi);
      CPRequestResolver::SPtr resolver = std::dynamic_pointer_cast<CPRequestResolver>(
              resolverIt->second);
      if (!resolver)
      {
        std::string errMsg = "OptimizerRequestDispatcher - "
                "invalid cast to CP api/resolver";
        spdlog::error(errMsg);
        throw std::runtime_error(errMsg);
      }
      return resolver->resolveRequest(pRequestInfo, api);
    }
    case EngineFrameworkType::EVOLUTIONARY_TOOLBOX:
    {
      auto api =
          std::dynamic_pointer_cast<EvolutionaryApi>(frameworkApi);
      EvolutionaryRequestResolver::SPtr resolver =
          std::dynamic_pointer_cast<EvolutionaryRequestResolver>(
              resolverIt->second);
      if (!resolver)
      {
        std::string errMsg = "OptimizerRequestDispatcher - "
                "invalid cast to evolutionary api/resolver";
        spdlog::error(errMsg);
        throw std::runtime_error(errMsg);
      }
      return resolver->resolveRequest(pRequestInfo, api);
    }
    case EngineFrameworkType::ROUTING_TOOLBOX:
    {
      auto api =
          std::dynamic_pointer_cast<RoutingApi>(frameworkApi);
      RoutingRequestResolver::SPtr resolver =
          std::dynamic_pointer_cast<RoutingRequestResolver>(resolverIt->second);
      if (!resolver)
      {
        std::string errMsg = "OptimizerRequestDispatcher - "
                "invalid cast to routing api/resolver";
        spdlog::error(errMsg);
        throw std::runtime_error(errMsg);
      }
      return resolver->resolveRequest(pRequestInfo, api);
    }
    case EngineFrameworkType::SCHEDULING_TOOLBOX:
    {
      auto api =
          std::dynamic_pointer_cast<SchedulingApi>(frameworkApi);
      SchedulingRequestResolver::SPtr resolver =
          std::dynamic_pointer_cast<SchedulingRequestResolver>(resolverIt->second);
      if (!resolver)
      {
        std::string errMsg = "OptimizerRequestDispatcher - "
                "invalid cast to scheduling api/resolver";
        spdlog::error(errMsg);
        throw std::runtime_error(errMsg);
      }
      return resolver->resolveRequest(pRequestInfo, api);
    }
    case EngineFrameworkType::OR_TOOLBOX:
    {
      auto api =
          std::dynamic_pointer_cast<ORApi>(frameworkApi);
      ORRequestResolver::SPtr resolver =
              std::dynamic_pointer_cast<ORRequestResolver>(resolverIt->second);
      if (!resolver)
      {
        std::string errMsg = "OptimizerRequestDispatcher - "
                "invalid cast to OR api/resolver";
        spdlog::error(errMsg);
        throw std::runtime_error(errMsg);
      }
      return resolver->resolveRequest(pRequestInfo, api);
    }
    default:
    {
      const std::string errMsg =
          "OptimizerRequestDispatcher - framework type not available";
      spdlog::error(errMsg);
      throw std::runtime_error(errMsg);
    }
  }
}  // dispatchRequestToResolver

void OptimizerRequestDispatcher::run()
{
  // Check that the request is a valid request
  checkForRequestValidity();

  try
  {
    // Get the right API to use on the router to route the request
    const auto frameworkPair = getFrameworkApi();

    // Route the request to the proper API and get the response.
    // Send back the response on the socket
    auto response =
        dispatchRequestToResolver(frameworkPair.first, frameworkPair.second);
    if (!response)
    {
      throw std::runtime_error("Empty optimizer response");
    }

    // Send the response about the resolved request
    pConnector->sendMessage(response);
  }
  catch (std::exception& e)
  {
    sendErrorResponse(pConnector, e.what());
    throw;
  }
  catch (...)
  {
    const std::string errMsg = "Undefined error on request dispatcher";
    sendErrorResponse(pConnector, errMsg);
    throw;
  }
}  // run

}  // namespace optilab
