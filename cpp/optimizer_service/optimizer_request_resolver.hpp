//
// Copyright OptiLab 2019. All rights reserved.
//
// Resolves a request to the proper API method.
//

#pragma once

#include <memory>  // for std::shared_ptr

namespace optilab {

/*
 * The optimizer request resolver, resolves the request encapsulated in the request info
 * by calling the appropriate method on the provided optimizer API.
 */
class SYS_EXPORT_CLASS OptimizerRequestResolver {
 public:
  using SPtr = std::shared_ptr<OptimizerRequestResolver>;

 public:
  virtual ~OptimizerRequestResolver() = default;

};

}  // namespace optilab
