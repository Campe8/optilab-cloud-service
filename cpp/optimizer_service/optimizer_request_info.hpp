//
// Copyright OptiLab 2019. All rights reserved.
//
// Class encapsulating information coming from a client request.
//

#pragma once

#include <memory>  // for std::shared_ptr
#include <string>

#include "engine/engine_constants.hpp"
#include "optimizer_service/optimizer_request.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

/*
 * Optimizer request information data structure class.
 * This class parses a raw request and hold all the information needed
 * to address the request by an optimizer.
 */
class SYS_EXPORT_CLASS OptimizerRequestInfo {
 public:
  using SPtr = std::shared_ptr<OptimizerRequestInfo>;

  enum RESTRequestType {
    POST = 0,
    GET,
    DELETE,
    PUT,
    REST_UNDEF
  };

 public:
  /// Constructor: creates a new instance of request info.
  /// @note throws std::invalid_argument on empty request
  OptimizerRequestInfo(const OptimizerRequest::SPtr& request);

  virtual ~OptimizerRequestInfo() = default;

  inline RESTRequestType getRESTRequestType() const { return pRESTType; }

  /// Returns the request parsed and hold by this optimizer request info instance
  inline const OptimizerRequest::SPtr& getRequest() const { return pRequest; }

  /// Returns the type of framework this request is directed to
  inline EngineFrameworkType getFrameworkType() const
  {
    return pFrameworkType;
  }

  /// Returns the optimizer package that needs to be used to address this request
  inline OptimizerPkg getOptimizerPackage() const { return pOptimizerPackage; }

 private:
  /// REST type of request
  RESTRequestType pRESTType{RESTRequestType::REST_UNDEF};

  /// Pointer to the actual request instance
  OptimizerRequest::SPtr pRequest;

  /// API framework type this request is directed to
  EngineFrameworkType pFrameworkType{EngineFrameworkType::UNDEF_FRAMEWORK};

  /// Optimizer package used to address this request
  OptimizerPkg pOptimizerPackage{OptimizerPkg::OP_UNDEF};

  /// Utility function: initalizes the request information
  void initializeRequestInfo(const OptimizerRequest::SPtr& request);
};

}  // namespace optilab
