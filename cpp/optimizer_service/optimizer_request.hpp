//
// Copyright OptiLab 2019. All rights reserved.
//
// Class encapsulating a client rrquest.
//

#pragma once

#include <memory>  // for std::shared_ptr
#include <string>

#include "optilab_protobuf/optilab.pb.h"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS OptimizerRequest {
 public:
  using SPtr = std::shared_ptr<OptimizerRequest>;

 public:
  /// Constructor, creates a new optimizer request.
  /// The request encapsulate the request message as received (and de-serialized) from the client,
  /// and the address of the sender (i.e., the client).
  /// @note throws std::invalid_argument if "addr" is empty
  OptimizerRequest(const std::string& addr, const OptilabRequestMessage& request);

  /// Returns the address of the client creating this request
  inline const std::string& getOutAddress() const { return pAddress; }

  /// Returns the REST type for this request
  inline RequestType getRequestType() const { return pRequestType; }

  /// Returns the content of this request
  inline const OptilabRequestMessage& getRequestContent() const { return pRequestContent; }

 private:
  /// Address of the client generating this request
  const std::string pAddress;

  /// REST type of this request
  RequestType pRequestType;

  /// Content of the request as received from the socket
  OptilabRequestMessage pRequestContent;
};

}  // namespace optilab
