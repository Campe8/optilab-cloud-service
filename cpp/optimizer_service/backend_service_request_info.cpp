#include "optimizer_service/backend_service_request_info.hpp"

#include <stdexcept>  // for std::runtime_error

#include "optilab_protobuf/optilab.pb.h"

namespace optilab {

BackendServiceRequestInfo::BackendServiceRequestInfo(const OptimizerRequest::SPtr& request)
: OptimizerRequestInfo(request)
{
  initializeRequestInfo(request);
}

void BackendServiceRequestInfo::initializeRequestInfo(const OptimizerRequest::SPtr& request)
{
  // Request type
  const OptilabRequestMessage& reqMsg = request->getRequestContent();
  if (reqMsg.details().Is<BackendServiceEngineReq>())
  {
    pRequestType = BackendServiceRequestInfo::RequestType::SERVICE_ENGINE;
  }
  else
  {
    throw std::runtime_error("BackendServiceRequestInfo - unrecognized request type");
  }

  // Store engine id
  pEngineId = reqMsg.messageinfo();

  BackendServiceEngineReq backendReq;
  reqMsg.details().UnpackTo(&backendReq);

  const auto frameworkType = backendReq.engineframeworktype();
  switch (frameworkType)
  {
    case ::optilab::FrameworkType::OR:
      pEngineFrameworkType = EngineFrameworkType::OR_FRAMEWORK;
      break;
    case ::optilab::FrameworkType::CONNECTORS:
      pEngineFrameworkType = EngineFrameworkType::CONNECTORS_FRAMEWORK;
      break;
    case ::optilab::FrameworkType::BACKEND_SERVICE:
      pEngineFrameworkType = EngineFrameworkType::BACKEND_SERVICE_FRAMEWORK;
      break;
    default:
    {
      const std::string errMsg = "BackendServiceRequestInfo - unrecognized framework type";
      throw std::runtime_error(errMsg);
    }
  }

  const auto classType = backendReq.serviceclasstype();
  switch (classType)
  {
    case BackendServiceEngineReq::ServiceClassType::BackendServiceEngineReq_ServiceClassType_SC_CP:
      pEngineClassType = EngineClassType::EC_CP;
      break;
    case BackendServiceEngineReq::ServiceClassType::BackendServiceEngineReq_ServiceClassType_SC_MIP:
      pEngineClassType = EngineClassType::EC_MIP;
      break;
    case BackendServiceEngineReq::ServiceClassType::
    BackendServiceEngineReq_ServiceClassType_SC_SCHEDULING:
      pEngineClassType = EngineClassType::EC_SCHEDULING;
      break;
    case BackendServiceEngineReq::ServiceClassType::BackendServiceEngineReq_ServiceClassType_SC_VRP:
      pEngineClassType = EngineClassType::EC_VRP;
      break;
    case BackendServiceEngineReq::ServiceClassType::
    BackendServiceEngineReq_ServiceClassType_SC_COMBINATOR:
      pEngineClassType = EngineClassType::EC_COMBINATOR;
      break;
    default:
      throw std::runtime_error("BackendServiceRequestInfo - "
          "unrecognized back-end service class type");
  }

  // Get service descriptor
  pServiceDescriptor = backendReq.servicedescriptor();
}  // initializeRequestInfo

}  // namespace optilab
