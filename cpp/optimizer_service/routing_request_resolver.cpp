#include "optimizer_service/routing_request_resolver.hpp"

#include <spdlog/spdlog.h>

#include <cassert>
#include <stdexcept>  // for std::runtime_error

#include "optilab_protobuf/optilab.pb.h"
#include "optilab_protobuf/optimizer_model.pb.h"
#include "optilab_protobuf/routing_model.pb.h"
#include "optimizer_service/optimizer_service_utilities.hpp"
#include "optimizer_service/service_callback_handler.hpp"
#include "routing_toolbox/routing_utilities.hpp"

namespace
{
#define ASSERT_EXTENSION(requestContent, extension, addr) \
  if (!(requestContent.Is<extension>()))                  \
  {                                                       \
    auto resp = optilab::serviceutils::getErrorResponse(  \
        "Invalid request extension type");                \
    resp->setResponseAddress(addr);                       \
    return resp;                                          \
  }

}  // namespace

namespace optilab {

OptimizerResponse::SPtr RoutingRequestResolver::resolveRequest(
    OptimizerRequestInfo::SPtr requestInfo, RoutingApi::SPtr routingApi)
{
  if (!requestInfo)
  {
    throw std::runtime_error("RoutingRequestResolver - empty request info");
  }

  if (!routingApi)
  {
    throw std::runtime_error("RoutingRequestResolver - empty solver API map");
  }

  SolverRequestInfo::SPtr reqInfo =
      std::dynamic_pointer_cast<SolverRequestInfo>(requestInfo);
  if (!reqInfo)
  {
    throw std::runtime_error(
        "RoutingRequestResolver - invalid cast to solver request info");
  }

  const auto RESTType = reqInfo->getRESTRequestType();
  const auto requestType = reqInfo->getRequestType();
  const auto addr = reqInfo->getRequest()->getOutAddress();
  if (RESTType == OptimizerRequestInfo::RESTRequestType::REST_UNDEF)
  {
    const auto addr = reqInfo->getRequest()->getOutAddress();
    auto resp =
        optilab::serviceutils::getErrorResponse("Invalid REST request type");
    resp->setResponseAddress(addr);
    return resp;
  }

  if (requestType == SolverRequestInfo::RequestType::TYPE_UNDEF)
  {
    return serviceutils::getUndefinedRequestResponse(addr);
  }

  switch (requestType)
  {
    case SolverRequestInfo::RequestType::ENGINE:
    {
      if (RESTType == OptimizerRequestInfo::RESTRequestType::POST)
      {
        return resolveCreateEngineRequest(reqInfo, routingApi);
      }
      else if (RESTType == OptimizerRequestInfo::RESTRequestType::DELETE)
      {
        return resolveDeleteEngineRequest(reqInfo, routingApi);
      }
      else
      {
        return serviceutils::getUndefinedRequestResponse(addr);
      }
    }
    case SolverRequestInfo::RequestType::MODEL:
    {
      if (RESTType == OptimizerRequestInfo::RESTRequestType::POST)
      {
        return resolveLoadModelRequest(reqInfo, routingApi);
      }
      else if (OptimizerRequestInfo::RESTRequestType::PUT)
      {
        return resolveRunModelRequest(reqInfo, routingApi);
      }
      else if (OptimizerRequestInfo::RESTRequestType::DELETE)
      {
        return resolveKillEngineRunRequest(reqInfo, routingApi);
      }
      else
      {
        return serviceutils::getUndefinedRequestResponse(addr);
      }
    }
    case SolverRequestInfo::RequestType::SOLUTION:
    {
      if (RESTType == OptimizerRequestInfo::RESTRequestType::GET)
      {
        return resolveCollectSolutionsRequest(reqInfo, routingApi);
      }
      else
      {
        return serviceutils::getUndefinedRequestResponse(addr);
      }
    }
    case SolverRequestInfo::RequestType::WAIT:
    {
      if (RESTType == OptimizerRequestInfo::RESTRequestType::POST)
      {
        return resolveEngineWaitRequest(reqInfo, routingApi);
      }
      else
      {
        return serviceutils::getUndefinedRequestResponse(addr);
      }
    }
    default:
      return serviceutils::getUndefinedRequestResponse(addr);
  }  // switch
}  // resolveRequest

OptimizerResponse::SPtr RoutingRequestResolver::resolveCreateEngineRequest(
    const SolverRequestInfo::SPtr& requestInfo, const RoutingApi::SPtr& optimizerApi)
{
  // Get the address to send the replies to and the engine identifier to create
  const auto addr = requestInfo->getRequest()->getOutAddress();
  const OptilabRequestMessage& reqMsg = requestInfo->getRequest()->getRequestContent();

  // Get the Id of the engine to create
  const std::string& engineId = reqMsg.messageinfo();

  // On creation request map the engine identifier to the address to send the
  // callback replies to
  ServiceCallbackHandler::SPtr callbackHandler =
      std::dynamic_pointer_cast<ServiceCallbackHandler>(optimizerApi->getEngineHandler());
  if (!callbackHandler)
  {
    const std::string errMsg =
        "RoutingRequestResolver - empty/wrong callback handler";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }
  callbackHandler->mapEngineIdToAddress(engineId, addr);

  ASSERT_EXTENSION(reqMsg.details(), toolbox::CreateRoutingOptimizerReq, addr);

  toolbox::CreateRoutingOptimizerReq createOptimizerReq;
  reqMsg.details().UnpackTo(&createOptimizerReq);

  // Get the framework of the engine to create
  const auto engineType = createOptimizerReq.routing_engine_type();

  // Send the request to the engine
  optilab::SolverError reqErr;
  switch (engineType)
  {
    case ::optilab::toolbox::CreateRoutingOptimizerReq::RoutingEngineType::
    CreateRoutingOptimizerReq_RoutingEngineType_ACTOR_VRP:
    {
      reqErr = optimizerApi->createEngine(
          engineId, toolbox::routingengine::RoutingProcessorType::RPT_ACTOR_VRP);
      break;
    }
    case ::optilab::toolbox::CreateRoutingOptimizerReq::RoutingEngineType::
    CreateRoutingOptimizerReq_RoutingEngineType_ACTOR_TSP:
    {
      reqErr = optimizerApi->createEngine(
          engineId, toolbox::routingengine::RoutingProcessorType::RPT_ACTOR_VRP);
      break;
    }
    case ::optilab::toolbox::CreateRoutingOptimizerReq::RoutingEngineType::
    CreateRoutingOptimizerReq_RoutingEngineType_CP_TSP:
    {
      reqErr = optimizerApi->createEngine(
          engineId, toolbox::routingengine::RoutingProcessorType::RPT_CP_TSP);
      break;
    }
    case ::optilab::toolbox::CreateRoutingOptimizerReq::RoutingEngineType::
    CreateRoutingOptimizerReq_RoutingEngineType_CP_VRP:
    {
      reqErr = optimizerApi->createEngine(
          engineId, toolbox::routingengine::RoutingProcessorType::RPT_CP_VRP);
      break;
    }
    default:
    {
      const std::string errMsg =
          "RoutingRequestResolver - unavailable engine";
      spdlog::error(errMsg);
      throw std::runtime_error(errMsg);
    }
  }

  if (reqErr == optilab::SolverError::kError)
  {
    // On error remove the mapping right-away
    callbackHandler->removeMappingBetweenEngineIdAndAddress(engineId);
  }

  return serviceutils::getSynchRequestResponse(reqErr, addr);
}  // resolveCreateEngineRequest

OptimizerResponse::SPtr RoutingRequestResolver::resolveDeleteEngineRequest(
    const SolverRequestInfo::SPtr& requestInfo, const RoutingApi::SPtr& optimizerApi)
{
  // Get the address of the requester and the engine identifier to delete
  const auto addr = requestInfo->getRequest()->getOutAddress();
  const OptilabRequestMessage& reqMsg =
      requestInfo->getRequest()->getRequestContent();

  // Get the Id of the engine to create
  const std::string& engineId = reqMsg.messageinfo();

  // On deletion request remove the mapping between the engine identifier to the
  // address of the requester
  ServiceCallbackHandler::SPtr callbackHandler =
      std::dynamic_pointer_cast<ServiceCallbackHandler>(
          optimizerApi->getEngineHandler());
  if (!callbackHandler)
  {
    const std::string errMsg =
        "RoutingRequestResolver - empty/wrong callback handler";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  const auto reqErr = optimizerApi->deleteEngine(engineId);
  if (reqErr == optilab::SolverError::kNoError)
  {
    // Remove the mapping only if there was no error in removing the engine.
    // If there was an error and the client tries to re-sent some requests,
    // the mapping should still be there
    callbackHandler->removeMappingBetweenEngineIdAndAddress(engineId);
  }

  return serviceutils::getSynchRequestResponse(reqErr, addr);
}  // resolveDeleteEngineRequest

OptimizerResponse::SPtr RoutingRequestResolver::resolveLoadModelRequest(
    const SolverRequestInfo::SPtr& requestInfo, const RoutingApi::SPtr& optimizerApi)
{
  const auto addr = requestInfo->getRequest()->getOutAddress();
  const OptilabRequestMessage& reqMsg =
      requestInfo->getRequest()->getRequestContent();
  const std::string& engineId = reqMsg.messageinfo();

  SolverError reqErr;
  if (reqMsg.details().Is<OptimizerModel>())
  {
    ASSERT_EXTENSION(reqMsg.details(), OptimizerModel, addr);

    OptimizerModel modelReq;
    reqMsg.details().UnpackTo(&modelReq);
    if (!modelReq.has_routing_model())
    {
      const std::string errMsg =
          "RoutingRequestResolver - unrecognized routing instance input format "
          "from the OptimizerModel message";
      spdlog::error(errMsg);
      throw std::runtime_error(errMsg);
    }

    reqErr = optimizerApi->loadModelInstance(
          engineId, std::unique_ptr<toolbox::RoutingInstance>(
              new toolbox::RoutingInstance(modelReq.routing_model())));
  }
  else
  {
    const std::string errMsg =
        "RoutingRequestResolver - unrecognized routing instance input format";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  return serviceutils::getSynchRequestResponse(reqErr, addr);
}  // resolveLoadModelRequest

OptimizerResponse::SPtr RoutingRequestResolver::resolveRunModelRequest(
    const SolverRequestInfo::SPtr& requestInfo, const RoutingApi::SPtr& optimizerApi)
{
  const auto addr = requestInfo->getRequest()->getOutAddress();
  const OptilabRequestMessage& reqMsg =
      requestInfo->getRequest()->getRequestContent();
  const std::string& engineId = reqMsg.messageinfo();

  const auto reqErr = optimizerApi->runEngine(engineId);

  return serviceutils::getSynchRequestResponse(reqErr, addr);
}  // resolveRunModelRequest

OptimizerResponse::SPtr RoutingRequestResolver::resolveKillEngineRunRequest(
    const SolverRequestInfo::SPtr& requestInfo, const RoutingApi::SPtr& optimizerApi)
{
  const auto addr = requestInfo->getRequest()->getOutAddress();
  const OptilabRequestMessage& reqMsg =
      requestInfo->getRequest()->getRequestContent();
  const std::string& engineId = reqMsg.messageinfo();

  const auto reqErr = optimizerApi->interruptEngine(engineId);

  return serviceutils::getSynchRequestResponse(reqErr, addr);
}  // resolveRunModelRequest

OptimizerResponse::SPtr RoutingRequestResolver::resolveCollectSolutionsRequest(
    const SolverRequestInfo::SPtr& requestInfo, const RoutingApi::SPtr& optimizerApi)
{
  const auto addr = requestInfo->getRequest()->getOutAddress();
  const OptilabRequestMessage& reqMsg =
      requestInfo->getRequest()->getRequestContent();
  const std::string& engineId = reqMsg.messageinfo();

  ASSERT_EXTENSION(reqMsg.details(), ModelSolutionsReq, addr);
  ModelSolutionsReq solutionReq;
  reqMsg.details().UnpackTo(&solutionReq);

  const auto reqErr =
      optimizerApi->collectSolutions(engineId, solutionReq.numsolutions());

  return serviceutils::getSynchRequestResponse(reqErr, addr);
}  // resolveCollectSolutionsRequest

OptimizerResponse::SPtr RoutingRequestResolver::resolveEngineWaitRequest(
    const SolverRequestInfo::SPtr& requestInfo, const RoutingApi::SPtr& optimizerApi)
{
  const auto addr = requestInfo->getRequest()->getOutAddress();
  const OptilabRequestMessage& reqMsg =
      requestInfo->getRequest()->getRequestContent();
  const std::string& engineId = reqMsg.messageinfo();

  ASSERT_EXTENSION(reqMsg.details(), OptimizerWaitReq, addr);
  OptimizerWaitReq waitReq;
  reqMsg.details().UnpackTo(&waitReq);

  const auto reqErr = optimizerApi->engineWait(engineId, waitReq.waittimeout());

  return serviceutils::getSynchRequestResponse(reqErr, addr);
}  // resolveEngineWaitRequest

}  // namespace optilab
