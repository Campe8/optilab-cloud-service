//
// Copyright OptiLab 2019. All rights reserved.
//
// Resolves a request to the proper connector API method.
//

#pragma once

#include <memory>  // for std::shared_ptr

#include "optimizer_api/framework_api.hpp"
#include "optimizer_service/backend_service_request_info.hpp"
#include "optimizer_service/optimizer_request_info.hpp"
#include "optimizer_service/optimizer_request_resolver.hpp"
#include "optimizer_service/optimizer_response.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS BackendServiceRequestResolver : public OptimizerRequestResolver {
 public:
  using SPtr = std::shared_ptr<BackendServiceRequestResolver>;

 public:
  OptimizerResponse::SPtr resolveRequest(const OptimizerRequestInfo::SPtr& requestInfo,
                                         const FrameworkApi::SPtr& frameworkApi);

 private:
  OptimizerResponse::SPtr resolveCreateBackendServiceRequest(
      const BackendServiceRequestInfo::SPtr& requestInfo, const FrameworkApi::SPtr& frameworkApi);

  OptimizerResponse::SPtr resolveDeleteBackendServiceRequest(
      const BackendServiceRequestInfo::SPtr& requestInfo, const FrameworkApi::SPtr& frameworkApi);

  OptimizerResponse::SPtr resolveCreateAndRunBackendServiceRequest(
      const BackendServiceRequestInfo::SPtr& requestInfo, const FrameworkApi::SPtr& frameworkApi);

  OptimizerResponse::SPtr resolveRunBackendServiceRequest(
      const BackendServiceRequestInfo::SPtr& requestInfo, const FrameworkApi::SPtr& frameworkApi);
};

}  // namespace optilab
