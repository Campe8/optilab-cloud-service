//
// Copyright OptiLab 2019. All rights reserved.
//
// Configuration class for the service worker.
//

#pragma once

#include <cstddef>  // for std::size_t
#include <string>

#include "system/system_export_defs.hpp"

namespace optilab {

struct SYS_EXPORT_STRUCT ServiceWorkerConfig {
  /// Construct a worker configuration data structure.
  /// @note "hostName" is the name of the host to run the worker will run on.
  /// @note "port" is the port number the worker is listening on.
  /// @note "useTCP" flag indicates if the worker should listen on a TCP port or an IPC port.
  /// By default the worker listens on a TCP port
  ServiceWorkerConfig(const std::string& hostName, int port, bool useTCP=true);

  /// Number of threads to be used for IO on the worker.
  /// @note the number of threads should be one I/O thread per gigabyte of data
  /// in or out per second
  int workerIOThreads;

  /// Indicates if the communication happens on a TCP socket or an IPC socket
  bool useTCPPort;

  std::string workerHostName;
  int workerHostPort;

  /// Number of dispatcher threads in the worker
  std::size_t workerNumDispatcherThreads;

  /// Returns the address the worker should connect to in order to send/recv messages
  std::string getSocketAddress() const;
};

struct SYS_EXPORT_STRUCT TPoolServiceWorkerConfig {
  /// Construct a worker configuration data structure.
  /// @note "hostName" is the name of the host to run the worker will run on.
  /// @note "inPort" is the port number the worker is listening on.
  /// @note "outPort" is the port number the worker is responding to.
  /// @note "controllerPort" is the port number use for worker's life-time management.
  /// @note "useTCP" flag indicates if the worker should listen on a TCP port or an IPC port.
  /// By default the worker listens on a TCP port
  TPoolServiceWorkerConfig(const std::string& hostName, int inPort, int outPort, int controllerPort,
                           bool useTCP=true);

  /// Number of threads to be used for IO on the worker.
  /// @note the number of threads should be one I/O thread per gigabyte of data
  /// in or out per second
  int workerIOThreads;

  /// Indicates if the communication happens on a TCP socket or an IPC socket
  bool useTCPPort;

  std::string workerHostName;
  int workerInPort;
  int workerOutPort;
  int workerControllerPort;

  /// Number of dispatcher threads in the worker
  std::size_t workerNumDispatcherThreads;

  /// Returns the address the worker should connect to in order to send/recv messages
  std::string getSocketAddress(int port) const;
};


}  // namespace optilab
