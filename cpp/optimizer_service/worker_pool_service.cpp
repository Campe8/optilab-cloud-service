#include "optimizer_service/worker_pool_service.hpp"

#include <atomic>
#include <cassert>
#include <stdexcept>  // for std::invalid_argument
#include <thread>

#include <spdlog/spdlog.h>

#include "engine/engine_callback_handler.hpp"
#include "optimizer_api/framework_api.hpp"
#include "optimizer_service/optimizer_request_dispatcher.hpp"
#include "optimizer_service/optimizer_request_dispatcher_factory.hpp"
#include "optimizer_service/service_constants.hpp"
#include "optimizer_service/optimizer_service_utilities.hpp"
#include "or_tools_engine/or_tools_callback_handler.hpp"
#include "utilities/random_generators.hpp"

namespace {

constexpr int kWorkerIdLength = 5;

std::atomic<int> workerError(0);

/// Returns a unique string id to be used as service worker id
std::string getWorkerId(int id, const int len=kWorkerIdLength)
{
  std::string workerId = optilab::optimizerservice::WORKER_ID_PREFIX;
  workerId += optilab::utilsrandom::buildRandomAlphaNumString(len);
  workerId += std::string("_") + std::to_string(id);
  return workerId;
}  // getWorkerId

/// Build an instance of a service worker which will connect and listen for requests
/// on the provided port
optilab::TPoolServiceWorker::SPtr buildServiceWorker(
    const std::string& workerId, int receiverPort, int senderPort, int controllerPort,
    optilab::WorkerPoolService::ConnectionType connType,
    const std::string& hostName, const int numWorkerDispatcherThreads)
{

  // Create worker configuration
  bool tcpConnection = (connType == optilab::WorkerPoolService::ConnectionType::TCP);
  if (!tcpConnection)
  {
    // Paranoid
    assert(connType == optilab::WorkerPoolService::ConnectionType::IPC);
  }

  optilab::TPoolServiceWorkerConfig workerConfig(hostName, receiverPort, senderPort,
                                                 controllerPort);
  workerConfig.workerNumDispatcherThreads = numWorkerDispatcherThreads;

  // Build a new API map to be handled by this worker
  optilab::FrameworkApi::FrameworkApiMapSPtr APIMapPtr =
      optilab::serviceutils::buildOpimizerAPIMap();

  // Create a new dispatcher factory
  optilab::OptimizerRequestDispatcherFactory::SPtr dispatcherFactory =
      std::make_shared<optilab::OptimizerRequestDispatcherFactory>(APIMapPtr);

  // Create service worker
  optilab::TPoolServiceWorker::SPtr serviceWorker =
      std::make_shared<optilab::TPoolServiceWorker>(workerId, workerConfig, dispatcherFactory);

  return serviceWorker;
}  // buildServiceWorker

/// Runs the given service worker
void runWorker(optilab::TPoolServiceWorker::SPtr worker)
{
  if (!worker)
  {
    throw std::invalid_argument("Empty worker");
  }

  const auto err = worker->run();

  workerError += err;
} // runWorker

}  // namespace

namespace optilab {

WorkerPoolService::WorkerPoolService(const int receiverPort, const int senderPort,
                                     const int controllerPort, const int numWorkers,
                                     const int numWorkerDispatcherThreads,
                                     ConnectionType connectionType, const std::string& hostName)
: pReceiverPort(receiverPort),
  pSenderPort(senderPort),
  pControllerPort(controllerPort),
  pHostName(hostName),
  pNumWorkers(numWorkers),
  pNumWorkerDispatcherThreads(numWorkerDispatcherThreads),
  pConnectionType(connectionType)
{
  if (pReceiverPort < 1 || pReceiverPort > optimizerservice::MAX_SERVICE_PORT_NUMBER)
  {
    throw std::invalid_argument("WorkerPoolService - invalid receiver port number: " +
                                std::to_string(pReceiverPort));
  }

  if (pSenderPort < 1 || pSenderPort > optimizerservice::MAX_SERVICE_PORT_NUMBER)
  {
    throw std::invalid_argument("WorkerPoolService - invalid sender port number: " +
                                std::to_string(pSenderPort));
  }

  if (numWorkers < 1)
  {
    throw std::invalid_argument("Invalid number of workers");
  }

  initializeService();
}

void WorkerPoolService::initializeService()
{
  spdlog::info("WorkerPoolService - initialization");
  spdlog::info("WorkerPoolService - number of workers: " + std::to_string(pNumWorkers));

  // Create a worker for each given port in the port list
  for (int portIdx = 0; portIdx < pNumWorkers; ++portIdx)
  {
    std::string hostName = pHostName.empty() ? optimizerservice::LOCAL_HOST_NAME : pHostName;

    const std::string workerId = getWorkerId(portIdx);
    pWorkersList.push_back(buildServiceWorker(workerId, pReceiverPort, pSenderPort, pControllerPort,
                                              pConnectionType, hostName,
                                              pNumWorkerDispatcherThreads));
  }
}  // initializeService

int WorkerPoolService::run()
{
  if (pWorkersList.empty())
  {
    return 1;
  }

  // If there is only one worker (default), run it on the current thread.
  // Otherwise spawn a new thread for each worker
  if (pWorkersList.size() == 1)
  {
    spdlog::info("WorkerPoolService - service run");
    return pWorkersList[0]->run();
  }
  else
  {
    std::vector<std::thread> workerThreads;
    for (int thrIdx = 0; thrIdx < static_cast<int>(pWorkersList.size()); ++thrIdx)
    {
      workerThreads.push_back(std::thread(runWorker, pWorkersList[thrIdx]));
    }

    // Synchronize all worker threads
    for (auto& th : workerThreads)
    {
      if (th.joinable()) th.join();
    }

    // Return the error value given by the workers
    return workerError.load();
  }
}  // run

}  // namespace optilab
