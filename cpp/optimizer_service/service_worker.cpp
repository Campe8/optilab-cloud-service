#include "optimizer_service/service_worker.hpp"

#include <execinfo.h>
#include <signal.h>
#include <spdlog/spdlog.h>
#include <unistd.h>

#include <cassert>
#include <functional>  // for std::bind
#include <stdexcept>   // for std::runtime_error
#include <utility>     // for std::pair

#include "optimizer_service/optimizer_request.hpp"
#include "optimizer_service/service_constants.hpp"
#include "zeromq/zhelpers.hpp"

static int s_prog_interrupted = 0;

namespace
{
constexpr int kMaxStackTrace = 1000;

void s_signal_handler_reg(int signal_value)
{
  // Set interrupt flag
  s_prog_interrupted = 1;

  void* stack_addr[kMaxStackTrace];
  int trace_size = backtrace(stack_addr, kMaxStackTrace);

  // At least two entries are needed to be useful:
  // entry 0: signal handler;
  // entry 1: active function when signal was raised
  if (trace_size < 2)
  {
    return;
  }

  // Cannot use std::cerr here
  backtrace_symbols_fd(stack_addr, trace_size, STDERR_FILENO);
  write(STDERR_FILENO, "\n", 1);
}  // s_signal_handler

void s_catch_signals_reg(void)
{
  struct sigaction action;
  action.sa_handler = s_signal_handler_reg;
  action.sa_flags = 0;
  sigemptyset(&action.sa_mask);
  sigaction(SIGINT, &action, NULL);
  sigaction(SIGTERM, &action, NULL);
  sigaction(SIGSEGV, &action, NULL);
  sigaction(SIGBUS, &action, NULL);
  sigaction(SIGILL, &action, NULL);
}  // s_catch_signals

}  // namespace

namespace optilab
{
ServiceWorker::ServiceWorker(
    const std::string& id, const ServiceWorkerConfig& config,
    OptimizerRequestDispatcherFactory::SPtr optimizerDispatcherFactory)
    : pWorkerId(id),
      pSWConfig(config),
      pShutDown(false),
      pWorkDispatcher((config.workerNumDispatcherThreads == 0)
                          ? serviceworker::WORKER_DEFAULT_DISPATCHER_NUM_THREADS
                          : config.workerNumDispatcherThreads),
      pOptimizerDispatcherFactory(optimizerDispatcherFactory)
{
  spdlog::info("ServiceWorker - creating worker " + pWorkerId);
  if (!pOptimizerDispatcherFactory)
  {
    const std::string errMsg =
        "ServiceWorker - empty pointer to the optimizer dispatcher factory";
    spdlog::error(errMsg);
    throw std::invalid_argument(errMsg);
  }
}

int ServiceWorker::run()
{
  {
    // Critical section.
    // Check that the worker doesn't run twice
    LockGuard lock(pWorkerStateMutex);
    if (pState != INIT)
    {
      throw std::runtime_error(std::string("Worker ") + pWorkerId +
                               " is being run more than once");
    }
  }

  // Set catch signal handlers to handle ctrl-c situations
  // s_catch_signals_reg();

  // Initialize the context for the worker
  std::string workerConnectId;
  std::shared_ptr<zmq::context_t> context;

  // RAII on socket
  SocketConnector::SPtr connector;
  try
  {
    context = std::make_shared<zmq::context_t>(pSWConfig.workerIOThreads);
    auto socket = std::make_shared<zmq::socket_t>(*context, ZMQ_DEALER);

    // Set worker ID and connect
    workerConnectId = s_set_id(*socket);
    spdlog::info("ServiceWorker - worker " + pWorkerId +
                 " mapped to connection id " + workerConnectId);

    spdlog::info("ServiceWorker - worker " + workerConnectId +
                 " connecting to address " + pSWConfig.getSocketAddress());
    socket->connect(pSWConfig.getSocketAddress().c_str());

    // Create the connector and inform the broker that the worker is in a ready
    // state
    spdlog::info("ServiceWorker - worker " + workerConnectId +
                 " send ready message " +
                 std::string(serviceworker::WORKER_MESSAGE_READY));

    // Use this worker ID as address.
    // This is used by the broker to map worker addresses to worker IDs
    connector = std::make_shared<SocketConnector>(socket);
    connector->sendRawMessage(serviceworker::WORKER_MESSAGE_READY, pWorkerId);
  }
  catch (...)
  {
    LockGuard lock(pWorkerStateMutex);
    pState = TERMINATE;

    throw;
  }

  // Set state to LISTENING
  {
    LockGuard lock(pWorkerStateMutex);
    pState = LISTENING;
  }

  // Create the callback handler for callbacks responses to the broker
  pCallbackHandler = std::make_shared<ServiceCallbackHandler>(connector);

  // Set the callback handler on all APIs used by this worker
  // @note all the APIs use the same callback handler
  for (const auto& apiIter :
       *(pOptimizerDispatcherFactory->getOptimizerApiMapPtr()))
  {
    for (auto& apiPtr : apiIter.second)
    {
      apiPtr->setEngineHandler(pCallbackHandler);
    }
  }

  // Create a new socket connector and start listening for incoming messages
  spdlog::info("ServiceWorker - worker " + workerConnectId +
               " listening on socket");
  const int errorStatus = listenOnSocket(connector);

  if (s_prog_interrupted)
  {
    spdlog::error("Interrupt signal - shut down and exit");
  }

  // Cleanup socket and context
  spdlog::info("ServiceWorker - worker " + workerConnectId + " cleanup");

  // Turn down sockets and context
  connector->closeSockets();

  // Shutdown the context
  zmq_ctx_shutdown(context.get());
  zmq_ctx_term(context.get());

  context.reset();

  spdlog::info("ServiceWorker - run: shutdown dispatcher");

  // Shutdown executor
  pWorkDispatcher.shutdown();

  return errorStatus;
}  // run

void ServiceWorker::handleWorkDispatcher(SocketConnector::SPtr connector,
                                         OptimizerRequest::SPtr request)
{
  if (!connector)
  {
    const std::string errMsg =
        "ServiceWorker - handleWorkDispatcher: "
        "empty connection " +
        pWorkerId;
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  if (!request)
  {
    const std::string errMsg =
        "ServiceWorker - handleWorkDispatcher: "
        "empty request " +
        pWorkerId;
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  // Create the dispatcher optimizer and run it
  OptimizerRequestDispatcher::SPtr optimizerDispatcher =
      pOptimizerDispatcherFactory->build(connector, request);

  // Run the dispatcher
  optimizerDispatcher->run();

  // Cleanup resources
  optimizerDispatcher.reset();
}  // handleWorkDispatcher

int ServiceWorker::listenOnSocket(SocketConnector::SPtr connector)
{
  assert(connector);

  // Keep track of errors on reading from the socket.
  // @note zero means no error
  int errorStatus = 0;

  // Loop on incoming requests
  while (true)
  {
    // Create a new request to send to the optimizer dispatcher.
    OptimizerRequest::SPtr request;
    {
      // Critical section: lock the mutex to read from the socket
      LockGuardRecursive lock(pDispatcherMutex);

      // The request is created by the socket connector upon reading from the
      // socket
      try
      {
        // Read request from socket.
        // @note this is a blocking call waiting for an incoming message
        request = connector->readMessage();
      }
      catch (zmq::error_t& e)
      {
        if (e.num() == ETERM)
        {
          spdlog::info(
              "ServiceWorker - listenOnSocket: terminate socket, return " +
              pWorkerId);
          break;
        }

        errorStatus = 1;
        return errorStatus;
      }
      catch (...)
      {
        spdlog::error(
            "ServiceWorker - listenOnSocket: undefined error, return " +
            pWorkerId);

        errorStatus = 1;
        return errorStatus;
      }

      // Release the mutex to allow other threads in the thread pool
      // to read incoming messages
    }

    // TODO check if the request is a "shutdown" request and, if so, set the
    // shutdown flag to terminate this worker

    // Return asap on empty request
    if (!request || pShutDown)
    {
      return errorStatus;
    }

    // Dispatch request to  thread pool and loop back
    // to listen to next incoming request
    try
    {
      // Dispatch request to be handled by the thread pool
      pWorkDispatcher.execute(std::bind(&ServiceWorker::handleWorkDispatcher,
                                        this, connector, request));

      if (pShutDown)
      {
        spdlog::error("ServiceWorker - listenOnSocket: done " + pWorkerId);
        break;
      }
    }
    catch (...)
    {
      errorStatus = 1;
      break;
    }
  }

  return errorStatus;
}  // listenOnSocket

}  // namespace optilab
