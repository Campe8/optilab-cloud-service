#include "optimizer_service/optimizer_request_info_factory.hpp"

#include <spdlog/spdlog.h>

#include <stdexcept>  // for std::invalid_argument
#include <string>

#include "optilab_protobuf/optilab.pb.h"
#include "optimizer_service/backend_service_request_info.hpp"
#include "optimizer_service/connector_request_info.hpp"
#include "optimizer_service/solver_request_info.hpp"

namespace optilab
{
OptimizerRequestInfo::SPtr OptimizerRequestInfoFactory::buildRequestInfo(
    const OptimizerRequest::SPtr& request)
{
  if (!request)
  {
    const std::string errMsg =
        "OptimizerRequestInfoFactory - empty optimizer request";
    spdlog::error(errMsg);
    throw std::invalid_argument(errMsg);
  }

  const OptilabRequestMessage& req = request->getRequestContent();
  const auto frameworkType = req.frameworktype();
  switch (frameworkType)
  {
    case ::optilab::FrameworkType::OR:
    {
      spdlog::error("OptimizerRequestInfoFactory - buildRequestInfo: "
              "deprecated framework type OR");
      return std::make_shared<SolverRequestInfo>(request);
    }
    case ::optilab::FrameworkType::EC:
    {
      spdlog::error("OptimizerRequestInfoFactory - buildRequestInfo: "
              "deprecated framework type EC");
      return std::make_shared<SolverRequestInfo>(request);
    }
    case ::optilab::FrameworkType::ROUTING:
    case ::optilab::FrameworkType::SCHEDULING:
    case ::optilab::FrameworkType::EVOLUTIONARY:
    case ::optilab::FrameworkType::DATA_AND_PORTS:
    case ::optilab::FrameworkType::OR_MP:
    case ::optilab::FrameworkType::CONSTRAINT_PROGRAMMING:
      return std::make_shared<SolverRequestInfo>(request);
    case ::optilab::FrameworkType::CONNECTORS:
    {
      spdlog::error("OptimizerRequestInfoFactory - buildRequestInfo: "
              "deprecated framework type CONNECTORS");
      return std::make_shared<ConnectorRequestInfo>(request);
    }
    case ::optilab::FrameworkType::BACKEND_SERVICE:
    {
      spdlog::error("OptimizerRequestInfoFactory - buildRequestInfo: "
              "deprecated framework type BACKEND_SERVICE");
      return std::make_shared<BackendServiceRequestInfo>(request);
    }
    default:
    {
      const std::string errMsg = "OptimizerRequestInfoFactory - invalid request framework";
      spdlog::error(errMsg);
      throw std::invalid_argument(errMsg);
    }
  }

}  // buildRequestInfo

}  // namespace optilab
