//
// Copyright OptiLab 2020. All rights reserved.
//
// Processor for MP models.
//

#pragma once

#include <memory>   // for std::shared_ptr
#include <string>
#include <map>
#include <utility>  // for std::pair
#include <vector>

#include "engine/optimizer_processor.hpp"
#include "optilab_protobuf/linear_model.pb.h"
#include "or_toolbox/or_processor.hpp"
#include "or_toolbox/or_utilities.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace toolbox {

class SYS_EXPORT_CLASS MockORProcessor : ORProcessor {
public:
  using SPtr = std::shared_ptr<MockORProcessor>;

public:
  explicit MockORProcessor();

  void loadModelAndCreateSolver(const std::string& solverId, const std::string& modelPath);

  void runProcessor();
};

}  // namespace toolbox
}  // namespace optilab
