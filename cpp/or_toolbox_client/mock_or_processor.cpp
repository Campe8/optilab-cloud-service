#include "or_toolbox_client/mock_or_processor.hpp"

#include "or_toolbox/or_utilities.hpp"

#include <chrono>

namespace optilab {
namespace toolbox {

MockORProcessor::MockORProcessor()
: ORProcessor(std::make_shared<orengine::MIPResult>())
{
}

void MockORProcessor::loadModelAndCreateSolver(const std::string& solverId,
                                               const std::string& modelPath)
{
  using namespace std::chrono;

  LinearModelSpecProto model;
  model.set_model_id(solverId);
  model.set_model_format_type(
          LinearModelSpecProto_ModelFormatType::LinearModelSpecProto_ModelFormatType_CSV);
  model.set_model_path(modelPath);
  model.set_class_type(LinearModelSpecProto_ClassType::LinearModelSpecProto_ClassType_MIP);
  model.set_package_type(LinearModelSpecProto_PackageType::LinearModelSpecProto_PackageType_SCIP);

  // Load the proto model

  std::cout << "Load model..." << std::endl;
  auto tic = high_resolution_clock::now();

  ORProcessor::loadModelAndCreateSolver(model);

  auto toc = high_resolution_clock::now();
  auto solvingSec = static_cast<uint64_t>(std::chrono::duration_cast<std::chrono::seconds>(toc - tic).count());
  std::cout << "Loaded model in " << solvingSec << " sec." << std::endl;
  std::cout << "Number of variables: " << getSolver()->getNumVariables() << std::endl;
  std::cout << "Number of constraints: " << getSolver()->getNumConstraints() << std::endl;
}  // loadModelAndCreateSolver

void MockORProcessor::runProcessor()
{
  using namespace std::chrono;

  std::cout << "Start solving..." << std::endl;
  auto tic = high_resolution_clock::now();

  MetricsRegister::SPtr mreg = std::make_shared<MetricsRegister>();
  orengine::ORWork::SPtr work = std::make_shared<orengine::ORWork>(
          orengine::ORWork::WorkType::kRunOptimizer, mreg);
  processWork(work);
  ORProcessor::waitForTaskToComplete();

  auto toc = high_resolution_clock::now();
  auto solvingSec = static_cast<uint64_t>(std::chrono::duration_cast<std::chrono::seconds>(toc - tic).count());
  std::cout << "Solved model in " << solvingSec << " sec." << std::endl;
  std::cout << "Objective value " << getSolver()->getObjective().getValue()
          << " best bound " << getSolver()->getObjective().getBestBound() << std::endl;
}  // runProcessor

}  // namespace toolbox
}  // namespace optilab
