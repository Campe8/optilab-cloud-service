#include "data_toolbox_client/client_python_processor.hpp"

#include <chrono>
#include <stdexcept>  // for std::invalid_argument
#include <utility>    // For std::pair

#include <spdlog/spdlog.h>

#include "data_structure/metrics/metrics_register.hpp"
#include "data_toolbox/data_instance.hpp"
#include "data_toolbox/data_utilities.hpp"
#include "optilab_protobuf/data_model.pb.h"
#include "utilities/timer.hpp"

namespace {

std::pair<std::string, std::string> splitArgument(const std::string& arg)
{
  auto found = arg.find_first_of("-");
  if (found == std::string::npos)
  {
    throw std::runtime_error("Invalid input argument");
  }
  auto type = arg.substr(0, found);
  auto val = arg.substr(found+1);
  return {type, val};
}  // splitArgument

void setFcnArgument(::optilab::toolbox::PythonFcnTypedValue* arg, const std::string& inArg)
{
  auto typeVal = splitArgument(inArg);

  if (typeVal.first == "double")
  {
    arg->set_double_arg(std::stod(typeVal.second));
  }
  else if (typeVal.first == "float")
  {
    arg->set_float_arg(std::stof(typeVal.second));
  }
  else if (typeVal.first == "int")
  {
    arg->set_int_arg(std::stoi(typeVal.second));
  }
  else if (typeVal.first == "uint")
  {
    arg->set_uint_arg(std::stoul(typeVal.second));
  }
  else if (typeVal.first == "bool")
  {
    arg->set_bool_arg((typeVal.second == "true"));
  }
  else if (typeVal.first == "string")
  {
    arg->set_string_arg(typeVal.second);
  }
  else if (typeVal.first == "bytes")
  {
    arg->set_bytes_arg(typeVal.second);
  }
  else
  {
    throw std::runtime_error("Invalid input argument");
  }
}  // setFcnArgument

}  // namespace

namespace optilab {
namespace toolbox {

ClientPythonProcessor::ClientPythonProcessor(const std::string& procName)
: PythonProcessor(procName, std::make_shared<dataengine::PythonFcnResult>())
{
}

void ClientPythonProcessor::loadCustomModel(
        const std::string& modelName,
        const std::string& entryPointFcnName,
        const std::string& entryPointFcnModule,
        const std::string& pathToEnvironment,
        const std::vector<PythonArg>& inputArgs,
        const std::vector<PythonArg>& outputArgs
        )
{
  DataModelProto model;
  model.set_model_id(modelName);
  auto pmodel = model.mutable_python_model();
  pmodel->set_entry_fcn_name(entryPointFcnName);
  pmodel->set_entry_module_name(entryPointFcnModule);
  pmodel->add_python_file_path(pathToEnvironment);

  for (const auto& in : inputArgs)
  {
    auto input_args = pmodel->add_input_args();
    for (const auto inArg : in)
    {
      auto arg = input_args->add_argument();
      setFcnArgument(arg, inArg);
    }
  }

  for (const auto& out : outputArgs)
  {
    auto output_args = pmodel->add_output_args();
    for (const auto outArg : out)
    {
      auto arg = output_args->add_argument();
      setFcnArgument(arg, outArg);
    }
  }

  // Load the model and create a new Python solver
  PythonProcessor::loadEnvironmentAndCreateSolver(DataInstance::UPtr(new DataInstance(model)));
}  // loadCustomModel

void ClientPythonProcessor::runProcessor() {
  using namespace std::chrono;

  std::cout << "Start solving..." << std::endl;
  auto tic = high_resolution_clock::now();

  MetricsRegister::SPtr mreg = std::make_shared<MetricsRegister>();

  dataengine::PythonFcnDataWork::SPtr work = std::make_shared<dataengine::PythonFcnDataWork>(
          dataengine::DataWork::WorkType::kRunOptimizer, mreg);
  processWork(work);
  PythonProcessor::waitForTaskToComplete();

  auto toc = high_resolution_clock::now();
  auto solvingSec = static_cast<uint64_t>(std::chrono::duration_cast<std::chrono::seconds>(
          toc - tic).count());
  std::cout << "Optimized environment in " << solvingSec << " sec." << std::endl;

  std::cout << "Done solving" << std::endl;
}  // runProcessor

void ClientPythonProcessor::printResult()
{
  auto result = getResult();
  const auto& solution = result->solutionProto.python_fcn_solution();
  for (int idx = 0; idx < solution.output_size(); ++idx)
  {
    std::cout << "Output n." << idx + 1 << ":\n";
    const auto& sol = solution.output(idx);

    for (int subIdx = 0; subIdx < sol.argument_size(); ++subIdx)
    {
      const auto& val = sol.argument(subIdx);
      switch (val.argType_case())
      {
        case ::optilab::toolbox::PythonFcnTypedValue::ArgTypeCase::kDoubleArg:
          std::cout << val.double_arg() << ", ";
          break;
        case ::optilab::toolbox::PythonFcnTypedValue::ArgTypeCase::kFloatArg:
           std::cout << val.float_arg() << ", ";
           break;
        case ::optilab::toolbox::PythonFcnTypedValue::ArgTypeCase::kIntArg:
           std::cout << val.int_arg() << ", ";
           break;
        case ::optilab::toolbox::PythonFcnTypedValue::ArgTypeCase::kUintArg:
           std::cout << val.uint_arg() << ", ";
           break;
        case ::optilab::toolbox::PythonFcnTypedValue::ArgTypeCase::kBoolArg:
           std::cout << val.bool_arg() << ", ";
           break;
        case ::optilab::toolbox::PythonFcnTypedValue::ArgTypeCase::kStringArg:
           std::cout << val.string_arg() << ", ";
           break;
        case ::optilab::toolbox::PythonFcnTypedValue::ArgTypeCase::kBytesArg:
           std::cout << val.bytes_arg() << ", ";
           break;
      }
    }
  }
  std::cout << std::endl;
}  // printResult

}  // namespace toolbox
}  // namespace optilab
