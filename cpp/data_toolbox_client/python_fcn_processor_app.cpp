//
// Copyright OptiLab 2020. All rights reserved.
//
// Entry point for the OptiLab Python function
// toolbox application.
//

#include <getopt.h>
#include <signal.h>

#include <cstdint>  // for uint32_t
#include <iostream>
#include <limits>   // for std::numeric_limits
#include <memory>   // for std::shared_ptr
#include <string>
#include <vector>

#include "data_toolbox_client/client_python_processor.hpp"

extern int optind;

namespace {

void printHelp(const std::string& programName) {
  std::cerr << "Usage: " << programName << " [options]"
      << std::endl
      << "options:" << std::endl
      << "  --file|-f           Load the given python file." << std::endl
      << "  --help|-h           Print this help message."
      << std::endl;
}  // printHelp

void runPythonFunction(const std::string& fcnName, const std::string& fcnFile,
                       const std::string& fcnPath)
{
  using namespace optilab;
  using namespace toolbox;

  // Prepare a model
  std::cout << "Python processor - create processor..." << std::endl;

  auto proc = std::make_shared<ClientPythonProcessor>("PythonProcessor");
  std::cout << "...done" << std::endl;

  // Create the model
  std::cout << "Python processor - load model..." << std::endl;
  proc->loadCustomModel(
          "PythonFcnModel",
          fcnName,
          fcnFile,
          fcnPath,
          {{"int-10"}, {"int-20"}},
          {{"int-10"}});

  std::cout << "...done" << std::endl;

  // Run the Python function
  std::cout << "Python processor - run python..." << std::endl;
  proc->runProcessor();
  std::cout << "...done" << std::endl;

  std::cout << "Python processor - print results..." << std::endl;
  proc->printResult();
  std::cout << "...done" << std::endl;

  // Delete processor instance
  proc.reset();
}  // runPythonFunction

}  // namespace

int main(int argc, char* argv[]) {

  char optString[] = "p:h";
  struct option longOptions[] =
  {
      { "path", no_argument, NULL, 'p' },
      { "help", no_argument, NULL, 'h' },
      { 0, 0, 0, 0 }
  };

  // Parse options
  int opt;
  std::string filePath;
  while (-1 != (opt = getopt_long(argc, argv, optString, longOptions, NULL)))
  {
    switch (opt)
    {
      case 'p':
      {
        filePath = std::string(optarg);
        break;
      }
      case 'h':
      default:
        printHelp(argv[0]);
        return 0;
    }
  }  // while

  if (filePath.empty())
  {
    std::cout << "Please provide the file path to the Python function." << std::endl;
    return 1;
  }

  std::cout << "=== Running Python processor ===" << std::endl;
  std::cout << "Running on file: " << filePath << std::endl;
  try
  {
    runPythonFunction("myFcn", "fcn_test", filePath);
  }
  catch(...)
  {
    std::cerr << "Error while running Python processor" << std::endl;
    return 1;
  }
  std::cout << "=== Done running Python processor ===" << std::endl;

  return 0;
}
