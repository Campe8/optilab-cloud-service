//
// Copyright OptiLab 2020. All rights reserved.
//
// Test processor for Python strategies.
//

#pragma once

#include <cstdint>  // for uint32_t
#include <memory>   // for std::shared_ptr
#include <string>
#include <vector>

#include "data_toolbox/python_processor.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace toolbox {

class SYS_EXPORT_CLASS ClientPythonProcessor : public PythonProcessor {
public:
  using SPtr = std::shared_ptr<ClientPythonProcessor>;
  using PythonArg = std::vector<std::string>;
public:
  ClientPythonProcessor(const std::string& procName);
  ~ClientPythonProcessor() override = default;

  void loadCustomModel(
          const std::string& modelName,
          const std::string& entryPointFcnName,
          const std::string& entryPointFcnModule,
          const std::string& pathToEnvironment,
          const std::vector<PythonArg>& inputArgs,
          const std::vector<PythonArg>& outputArgs
          );

  void runProcessor();

  void printResult();
};

}  // namespace toolbox
}  // namespace optilab
