#include "optimizer_network/socket_connector.hpp"

#include <cassert>
#include <cstdlib>     // for rand
#include <stdexcept>   // for std::invalid_argument

#include <google/protobuf/util/message_differencer.h>
#include <spdlog/spdlog.h>

#include "optilab_protobuf/linear_model.pb.h"
#include "optilab_protobuf/meta_bb_packet.pb.h"
#include "optilab_protobuf/network_packet.pb.h"
#include "optimizer_network/network_constants.hpp"
#include "or_toolbox/meta_net_initiator.hpp"

namespace {

std::string readMsgFromSocket(const optilab::optnet::BaseSocketConnector::SocketSPtr& socket)
{
  assert(socket);

  zmq::message_t message;
  socket->recv(message);
  return std::string(static_cast<char*>(message.data()), message.size());
}  // readMsgFromSocket

}  // namespace

namespace optilab {
namespace  optnet {

BaseNetworkPacket::UPtr SocketPacketDeserializer::deserializePacket(int packetType,
                                                                    const std::string& bytes)
{
  const auto ptype = intToNetworkPacketType(packetType);
  switch (ptype)
  {
    case BaseNetworkPacket::BaseNetworkPacketType::BNPT_NETWORK_PACKET:
    {
      return deserializeNetworkPacket(bytes);
    }
    case BaseNetworkPacket::BaseNetworkPacketType::BNPT_MP_LINEAR_MODEL:
    {
      LinearModelSpecProto protoPacket;
      protoPacket.ParseFromString(bytes);
      NetworkPacket<LinearModelSpecProto>* packet =
              new NetworkPacket<LinearModelSpecProto>({protoPacket});
      packet->packetType = BaseNetworkPacket::BaseNetworkPacketType::BNPT_MP_LINEAR_MODEL;
      return NetworkPacket<LinearModelSpecProto>::UPtr(packet);
    }
    case BaseNetworkPacket::BaseNetworkPacketType::BNPT_MP_LINEAR_MODEL_SOLUTION:
    {
      LinearModelSolutionProto protoPacket;
      protoPacket.ParseFromString(bytes);
      NetworkPacket<LinearModelSolutionProto>* packet =
              new NetworkPacket<LinearModelSolutionProto>({protoPacket});
      packet->packetType = BaseNetworkPacket::BaseNetworkPacketType::BNPT_MP_LINEAR_MODEL_SOLUTION;
      return NetworkPacket<LinearModelSolutionProto>::UPtr(packet);
    }
    case BaseNetworkPacket::BaseNetworkPacketType::BNPT_META_BB_INITIATOR:
    {
      metabb::InitiatorInitSystemMessageProtoPacket protoPacket;
      protoPacket.ParseFromString(bytes);
      NetworkPacket<metabb::InitiatorInitSystemMessageProtoPacket>* packet =
              new NetworkPacket<metabb::InitiatorInitSystemMessageProtoPacket>({protoPacket});
      packet->packetType = BaseNetworkPacket::BaseNetworkPacketType::BNPT_META_BB_INITIATOR;
      return NetworkPacket<metabb::InitiatorInitSystemMessageProtoPacket>::UPtr(packet);
    }
    case BaseNetworkPacket::BaseNetworkPacketType::BNPT_META_BB_PACKET:
    case BaseNetworkPacket::BaseNetworkPacketType::BNPT_META_BB_SOLVER_PARAMSET:
    case BaseNetworkPacket::BaseNetworkPacketType::BNPT_META_BB_DESCRIPTOR_PARAMSET:
    case BaseNetworkPacket::BaseNetworkPacketType::BNPT_UNSPECIFIED_PACKET:
    default:
    {
      // Let the receiver properly deserialize the packet.
      // Create a network packet of std::string type and store
      // the serialized original packet within it.
      // In other words, create a network packet holding a std::string.
      // Copy the serialized object in the string and let clients deserialize it later
      // considering the packet type and tag, if any
      NetworkPacket<std::string>* packet = new NetworkPacket<std::string>({bytes});
      packet->packetType = intToNetworkPacketType(packetType);
      return NetworkPacket<std::string>::UPtr(packet);
    }
  }
}  // deserializePacket

SocketConnector::SocketConnector(BaseSocketConnector::SocketSPtr socket)
: pSocket(socket)
{
  if (!socket)
  {
    throw std::invalid_argument("SocketConnector - empty pointer to socket");
  }

  // Instantiate the packet deserializer
  pPacketDeserializer = std::unique_ptr<SocketPacketDeserializer>(new SocketPacketDeserializer());
}

SocketConnector::~SocketConnector()
{
  closeSocket();
}

void SocketConnector::closeSocket()
{
  // Set linger time to zero and close the socket
  if (pSocket)
  {
    // Set linger to zero to discard all pending messages
    int val = 0;
    pSocket->setsockopt(ZMQ_LINGER, &val, sizeof(val));

    // Close the socket
    pSocket->close();

    // Reset the pointer to prevent re-closing the socket
    pSocket.reset();
  }
}  // closeSocket

std::pair<std::string, std::string> SocketConnector::receiveRawMessage()
{
  {
    LockGuard lock(pInputSocketMutex);
    try {
      // First frame contains the address of the sender, i.e., the "sender_address"
      const std::string addr = readMsgFromSocket(pSocket);
      // const std::string addr = s_recv(*pSocket);

      // Second frame contains the raw message, i.e., the serialized "payload"
      const std::string rawMsg = readMsgFromSocket(pSocket);
      // const std::string rawMsg = s_recv(*pSocket);

      // Return the pair <address, raw_message>
      return {addr, rawMsg};
    }
    catch(zmq::error_t& e)
    {
      if (e.num() == ETERM)
      {
        spdlog::info("SocketConnector - receiveRawMessage: terminate socket, return");
      }
      throw e;
    }
    catch(...)
    {
      const std::string errMsg = "SocketConnector - receiveRawMessage: "
          "undefined error in reading from socket";
      spdlog::error(errMsg);
      throw std::runtime_error(errMsg);
    }
  }  // critical section
}  // receiveRawMessage

std::pair<std::string, std::string> SocketConnector::receiveRawMessageOrReturnOnTimeout(
    const int timeoutMsec)
{
  {
    LockGuard lock(pInputSocketMutex);

    /*
     * Since the socket is already connected, it is not possible to reset
     * the timeout value on receive.
     * Instead, it is possible to use a poll with a specified timeout to read from the
     * given socket.
     */
    zmq::pollitem_t pollItems[] = {{ *pSocket, 0, ZMQ_POLLIN, 0 }};
    try
    {
      zmq::poll(&pollItems[0], 1, timeoutMsec);
      if (pollItems[0].revents & ZMQ_POLLIN)
      {
        // First frame contains the address of the sender, i.e., the "sender_address"
        const std::string addr = readMsgFromSocket(pSocket);
        // const std::string addr = s_recv(*pSocket);

        // Second frame contains the raw message, i.e., the serialized "payload"
        const std::string rawMsg = readMsgFromSocket(pSocket);
        // const std::string rawMsg = s_recv(*pSocket);

        // Return the pair <address, raw_message>
        return {addr, rawMsg};
      }
    }
    catch(zmq::error_t& e)
    {
      if (e.num() == ETERM)
      {
        // Reset the old timeout values before returning
        spdlog::info("SocketConnector - receiveRawMessageOrReturnOnTimeout: "
            "terminate socket, return");
      }
      throw e;
    }
    catch(...)
    {
      const std::string errMsg = "SocketConnector - receiveRawMessage: "
          "undefined error in reading from socket";
      spdlog::error(errMsg);
      throw std::runtime_error(errMsg);
    }
  }  // critical section

  return {"", ""};
}  //receiveRawMessageOrReturnOnTimeout

Packet::UPtr SocketConnector::receiveMessage()
{
  /*
   * The hub sends a message to a node using a ZMQ_ROUTER socket.
   * The pattern is the following:
   *
   * 1 - s_sendmore(socket, receiver_address);
   * 2 - s_sendmore(socket, sender_address);
   * 3 - s_send(socket, request);
   *
   * The first message (1) is consumed by the ZMQ_ROUTER socket to address the message to
   * the proper receiver. This means that the receiver node receives:
   * (a) client_address and; (b) the request.
   * According to the contract between hub and receiver node,
   * the sender_address (a) is the address of the sender node expecting the receiver
   * node's reply.
   *
   */

  // First frame contains the address of the sender, i.e., the "sender_address".
  // Second frame contains the raw message, i.e., the serialized "payload"
  const auto addrMsgPair = receiveRawMessage();

  // Get the protobuf deserialized packet
  PointToPointProtoPacket protoPacket;
  try
  {
    // Deserialize the protobuf message
    protoPacket.ParseFromString(addrMsgPair.second);
  }
  catch (...)
  {
    const std::string errMsg = "SocketConnector - receiveMessage: "
        "cannot parse the point to point protobuf packet for undefined error";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  try
  {
    Packet* rawPacketPtr = new Packet(pPacketDeserializer->deserializePacket(
            protoPacket.payloadtype(), protoPacket.payload()), addrMsgPair.first);
    assert(rawPacketPtr);

    // Set the packet tag
    const int packetTagVal = protoPacket.packettag();
    if (packetTagVal != PacketTag::getInvalidPacketTag())
    {
      rawPacketPtr->packetTag = PacketTag::UPtr(new PacketTag(packetTagVal));
    }

    return Packet::UPtr(rawPacketPtr);
  }
  catch (...)
  {
    return Packet::UPtr();
  }
}  // receiveMessage

void SocketConnector::sendRawMessage(const std::string& rawMsg, const std::string& addr)
{
  /*
   * The node sends a message to the hub.
   * s_send(...)
   * sends the address of the sender node as first frame of the message.
   * Therefore
   * s_send(socket, msg)
   * requires
   * s_recv(socket) -> to get the address of the sender node
   * s_recv(socket) -> to get the message from the sender node
   * This doesn't happen with s_sendmore(...) which doesn't add the address of the sender
   * to the socket envelope.
   *
   * Example:
   *
   * s_sendmore(socket, "msg_1");
   * s_send(socket, "msg_2");
   *
   * Creates the following envelope:
   *
   * | Address sender |
   * +----------------+
   * | msg_1          |
   * +----------------+
   * | msg_2          |
   * +----------------+
   *
   * That is popped from the top by the hub:
   * s_recv(...) -> returns the address of the sender node
   * s_recv(...) -> returns "msg_1"
   * s_recv(...) -> returns "msg_2"
   *
   */
  {
    // Critical section
    LockGuard lock(pOutputSocketMutex);

    zmq::message_t messageAddr(addr.data(), addr.size());
    auto res = pSocket->send(messageAddr, zmq::send_flags::sndmore);
    if (!res || (*res != addr.size()))
    {
      if (!res)
      {
        spdlog::error("SocketConnector - sendRawMessage: error on sendmore. "
                "Non-blocking mode was requested and the message cannot be sent at the moment. "
                "The message cannot be queued on the socket.");
      }
      spdlog::error("SocketConnector - sendRawMessage: error on sendmore. "
              "String to send " + addr + " of size " + std::to_string(addr.size()) +
              " actually sent " + std::to_string(*res));

      throw std::runtime_error("SocketConnector - sendRawMessage: error on sendmore.");
    }

    zmq::message_t message(rawMsg.data(), rawMsg.size());
    res = pSocket->send(message, zmq::send_flags::none);
    if (!res || (*res != rawMsg.size()))
    {
      if (!res)
      {
        spdlog::error("SocketConnector - sendRawMessage: error on send. "
                "Non-blocking mode was requested and the message cannot be sent at the moment. "
                "The message cannot be queued on the socket.");
      }
      spdlog::error("SocketConnector - sendRawMessage: error on send. "
              "String to send " + rawMsg + " of size " + std::to_string(rawMsg.size()) +
              " actually sent " + std::to_string(*res));

      throw std::runtime_error("SocketConnector - sendRawMessage: error on send.");
    }
    // s_sendmore(*pSocket, addr);
    // s_send(*pSocket, rawMsg);
  }
}  // sendRawMessage

int SocketConnector::sendRawMessageOrReturnOnTimeout(const std::string& rawMsg,
                                                     const std::string& addr,
                                                     const int timeoutMsec) noexcept
{
  int timeoutOldVal = 0;
  std::size_t timeoutOldLen = sizeof(timeoutOldVal);
  {
    // Critical section
    LockGuard lock(pOutputSocketMutex);

    // Set the new value
    int timeout = timeoutMsec > 0 ? timeoutMsec : timeoutOldVal;
    //pSocket->setsockopt(ZMQ_SNDTIMEO, &timeout, sizeof(timeout));

    try {
      zmq::message_t messageAddr(addr.data(), addr.size());
      auto res = pSocket->send(messageAddr, zmq::send_flags::sndmore);
      if (!res || (*res != addr.size()))
      {
        if (!res)
        {
          spdlog::error("SocketConnector - sendRawMessageOrReturnOnTimeout: error on sendmore. "
                  "Non-blocking mode was requested and the message cannot be sent at the moment. "
                  "The message cannot be queued on the socket.");
          return 1;
        }
        spdlog::error("SocketConnector - sendRawMessageOrReturnOnTimeout: error on sendmore. "
                "String to send " + addr + " of size " + std::to_string(addr.size()) +
                " actually sent " + std::to_string(*res));
        return 1;
      }

      zmq::message_t message(rawMsg.data(), rawMsg.size());
      res = pSocket->send(message, zmq::send_flags::none);
      if (!res || (*res != rawMsg.size()))
      {
        if (!res)
        {
          spdlog::error("SocketConnector - sendRawMessageOrReturnOnTimeout: error on send. "
                  "Non-blocking mode was requested and the message cannot be sent at the moment. "
                  "The message cannot be queued on the socket.");
        }
        spdlog::error("SocketConnector - sendRawMessageOrReturnOnTimeout: error on send. "
                "String to send " + addr + " of size " + std::to_string(addr.size()) +
                " actually sent " + std::to_string(*res));
        return 1;
      }

      /*
      if(!s_sendmore(*pSocket, addr))
      {
        //pSocket->setsockopt(ZMQ_SNDTIMEO, &timeoutOldVal, sizeof(timeoutOldVal));
        return 1;
      }

      if(!s_send(*pSocket, rawMsg))
      {
        //pSocket->setsockopt(ZMQ_SNDTIMEO, &timeoutOldVal, sizeof(timeoutOldVal));
        return 1;
      }
      */
    }
    catch(...)
    {
      spdlog::error("SocketConnector - sendRawMessageOrReturnOnTimeout: "
              "undefined exception on send");

      // Reset the old timeout value
      // pSocket->setsockopt(ZMQ_SNDTIMEO, &timeoutOldVal, sizeof(timeoutOldVal));
      return 1;
    }

    // Reset the old timeout value
    // pSocket->setsockopt(ZMQ_SNDTIMEO, &timeoutOldVal, sizeof(timeoutOldVal));
  }

  return 0;
}  // sendRawMessageOrReturnOnTimeout

int SocketConnector::sendHubRegistrationMessage(bool isRoot, int timeoutMsec, int rank) noexcept
{
  // Get a random number as packet identifier
  const int regPacketId = rand() % 1000 + 1;
  try
  {
    // Send the message back to the hub.
    // The hub will receive:
    // 1 - Address of this sender;
    // 2 - the address of the receiver;
    // 3 - the actual reply.
    // Which would require 3 corresponding s_recv(...) calls from the broker side.
    // @note the message to send back to the node's receiver is a serialized message
    PointToPointProtoPacket protoPacket;
    protoPacket.set_packettype(
        ::optilab::optnet::PointToPointProtoPacket_Type::
         PointToPointProtoPacket_Type_HubRegistration);
    protoPacket.set_packetid(regPacketId);

    // Set an empty payload and empty address
    protoPacket.set_payload(std::to_string(rank));
    protoPacket.set_sourceroot(isRoot);
    if (sendRawMessageOrReturnOnTimeout(protoPacket.SerializeAsString(), "", timeoutMsec))
    {
      return -1;
    }
  }
  catch(...)
  {
    return -1;
  }

  return regPacketId;
}  // sendHubRegistrationMessage

int SocketConnector::recvHubRegistrationMessage(int registrationId, int timeoutMsec) noexcept
{
  // Wait to receive the answer from the hub
   const auto addrMsgPair = receiveRawMessageOrReturnOnTimeout(timeoutMsec);

   if (addrMsgPair.first.empty() || addrMsgPair.second.empty())
   {
     spdlog::error("SocketConnector - recvHubRegistrationMessage: "
         "invalid registration to the hub, return");
     return networkconst::INVALID_NETWORK_NODE_ID;
   }

   PointToPointProtoPacket protoPacket;
   try
   {
     // Deserialize the protobuf message
     protoPacket.ParseFromString(addrMsgPair.second);
   }
   catch (...)
   {
     const std::string errMsg = "SocketConnector - recvHubRegistrationMessage: "
         "cannot parse the point to point protobuf packet for undefined error";
     spdlog::error(errMsg);
     return networkconst::INVALID_NETWORK_NODE_ID;
   }

   if (registrationId > 0 && protoPacket.packetid() != registrationId)
   {
     const std::string errMsg = "SocketConnector - recvHubRegistrationMessage: "
         "received a different registration packet id " +
         std::to_string(protoPacket.packetid()) + " instead of " + std::to_string(registrationId);
     spdlog::error(errMsg);
     return networkconst::INVALID_NETWORK_NODE_ID;
   }

   // The payload contains the string version of the unique network node identifier
   return std::stoi(protoPacket.payload());
}  // recvHubRegistrationMessage

void SocketConnector::sendNetworkSynchronizationMessage(bool isRoot)
{
  try
  {
    PointToPointProtoPacket protoPacket;
    protoPacket.set_packettype(
        ::optilab::optnet::PointToPointProtoPacket_Type::
         PointToPointProtoPacket_Type_NetworkSynchronization);
    protoPacket.set_packetid(0);

    // Set an empty payload and empty address
    protoPacket.set_payload("");
    protoPacket.set_sourceroot(isRoot);
    sendRawMessage(protoPacket.SerializeAsString(), "");
  }
  catch(...)
  {
    const std::string errMsg = "SocketConnector - sendNetworkSynchronizationMessage: "
        "undefined error";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }
}  // sendNetworkSynchronizationMessage

void SocketConnector::sendMessage(Packet::UPtr packet)
{
  if (!packet)
  {
    const std::string errMsg = "SocketConnector - sendMessage: empty message";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  if (packet->networkAddr.empty())
  {
    const std::string errMsg = "SocketConnector - sendMessage: empty address";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  {
    // Critical section (lock is done on the "sendRawMessage(...)" method

    try
    {
      // Send the message back to the hub.
      // The hub will receive:
      // 1 - Address of this sender;
      // 2 - the address of the receiver;
      // 3 - the actual reply.
      // Which would require 3 corresponding s_recv(...) calls from the broker side.
      // @note the message to send back to the node's receiver is a serialized message
      PointToPointProtoPacket protoPacket;
      protoPacket.set_packettype(
          ::optilab::optnet::PointToPointProtoPacket_Type::
           PointToPointProtoPacket_Type_PointToPointPayload);
      protoPacket.set_packetid(static_cast<int64_t>(packet->packetId));

      const auto spayload = packet->networkPacket->serialize();
      protoPacket.set_payloadtype(static_cast<int>(spayload.first));
      protoPacket.set_payload(spayload.second);
      protoPacket.set_sourceroot(packet->rootSource);

      // Check and set packet tag
      if (packet->packetTag)
      {
        protoPacket.set_packettag(packet->packetTag->getTag());
      }
      else
      {
        protoPacket.set_packettag(PacketTag::getInvalidPacketTag());
      }

      sendRawMessage(protoPacket.SerializeAsString(), packet->networkAddr);
    }
    catch(...)
    {
      const std::string errMsg = "SocketConnector - sendMessage: undefined error";
      spdlog::error(errMsg);
      throw std::runtime_error(errMsg);
    }
  }
}  // sendMessage

BroadcastSocketConnector::BroadcastSocketConnector(SocketSPtr socket)
: pSocket(socket)
{
  if (!socket)
  {
    throw std::invalid_argument("BroadcastSocketConnector - empty pointer to socket");
  }

  // Instantiate the packet deserializer
  pPacketDeserializer = std::unique_ptr<SocketPacketDeserializer>(new SocketPacketDeserializer());
}

BroadcastSocketConnector::~BroadcastSocketConnector()
{
  closeSocket();
}

void BroadcastSocketConnector::closeSocket()
{
  // Set linger time to zero and close the socket
  if (pSocket)
  {
    // Set linger to zero to discard all pending messages
    int val = 0;
    pSocket->setsockopt(ZMQ_LINGER, &val, sizeof(val));

    // Close the socket
    pSocket->close();

    // Reset the pointer to prevent re-closing the socket
    pSocket.reset();
  }
}  // closeSocket

Packet::UPtr BroadcastSocketConnector::receiveMessage()
{
  /*
   * The hub broadcasts a message to a node using a ZMQ_PUB socket.
   * The receiver socket must be a ZMQ_SUB socket acting as a subscriber.
   */
  BroadcastProtoPacket protoPacket;
  try
  {
    LockGuard lock(pInputSocketMutex);

    // Deserialize the protobuf message.
    // The received frame is the broadcasted message.
    // @note read messages as per SUBSCRIBER sockets
    zmq::message_t message;
    pSocket->recv(message);
    protoPacket.ParseFromString(std::string(static_cast<char*>(message.data()), message.size()));
    // protoPacket.ParseFromString(s_recv(*pSocket));
  }
  catch(zmq::error_t& e)
  {
    if (e.num() == ETERM)
    {
      spdlog::info("BroadcastSocketConnector - receiveMessage: terminate socket, return");
      // BroadcastPacket* packet = new BroadcastPacket();
      // packet->networkAbortMessage = true;
      // return BroadcastPacket::UPtr(packet);
      throw;
    }
    throw e;
  }
  catch (...)
  {
    const std::string errMsg = "BroadcastSocketConnector - receiveMessage: "
        "cannot parse the broadcast protobuf packet for undefined error";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  BroadcastPacket* packet = new BroadcastPacket();

  // Set the source address of this broadcasted packet
  packet->networkAddr = protoPacket.sourceaddr();
  try
  {
    switch(protoPacket.packettype())
    {
      case BroadcastProtoPacket_Type::BroadcastProtoPacket_Type_NetworkSynchronization:
      {
        packet->networkSychMessage = true;
        packet->networkAbortMessage = false;

        auto& nmap = *(protoPacket.mutable_networkmap());
        for (const auto& iter : nmap)
        {
          auto& entry = packet->networkNodeMap[iter.first];
          entry.first = iter.second.sendaddr();
          entry.second = iter.second.recvaddr();
        }

        return BroadcastPacket::UPtr(packet);
      }
      case BroadcastProtoPacket_Type::BroadcastProtoPacket_Type_NetworkAbort:
      {
        packet->networkSychMessage = false;
        packet->networkAbortMessage = true;
        return BroadcastPacket::UPtr(packet);
      }
      default:
      {
        if (protoPacket.packettype() !=
            BroadcastProtoPacket_Type::BroadcastProtoPacket_Type_BroadcastPayload)
        {
          std::string errMsg = "BroadcastSocketConnector - receiveMessage: "
                  "invalid broadcast message type " + std::to_string(protoPacket.packettype());
          spdlog::error(errMsg);
          return Packet::UPtr();
        }

        packet->networkSychMessage = false;
        packet->networkAbortMessage = false;

        packet->networkPacket = pPacketDeserializer->deserializePacket(
                protoPacket.payloadtype(), protoPacket.payload());
        assert(packet->networkPacket);

        return BroadcastPacket::UPtr(packet);
      }
    }
  }
  catch (...)
  {
    std::string errMsg = "BroadcastSocketConnector - receiveMessage: "
            "unknown error while trying to read the broadcast packet type";
    spdlog::error(errMsg);
    return Packet::UPtr();
  }
}  // receiveMessage

void BroadcastSocketConnector::sendMessage(Packet::UPtr packet)
{
  if (!packet)
  {
    const std::string errMsg = "BroadcastSocketConnector - sendMessage: empty message";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  {
    // Critical section
    LockGuard lock(pOutputSocketMutex);
    try
    {
      BroadcastProtoPacket protoPacket;
      protoPacket.set_packettype(
          ::optilab::optnet::BroadcastProtoPacket_Type::BroadcastProtoPacket_Type_BroadcastPayload);

      const auto spayload = packet->networkPacket->serialize();
      protoPacket.set_payloadtype(static_cast<int>(spayload.first));
      protoPacket.set_payload(spayload.second);

      // Set the source address of this broadcasted packet
      protoPacket.set_sourceaddr(packet->networkAddr);

      // Send the message back to the hub.
      // The hub will receive:
      // 1 - Address of this sender;
      // 2 - the address of the receiver;
      // 3 - the actual reply.
      // Which would require 3 corresponding s_recv(...) calls from the broker side.
      // @note the message to send back to the node's receiver is a serialized message
      zmq::message_t messageAddr(packet->networkAddr.data(), packet->networkAddr.size());
      auto res = pSocket->send(messageAddr, zmq::send_flags::sndmore);
      if (!res || (*res != packet->networkAddr.size()))
      {
        spdlog::error("BroadcastSocketConnector - sendMessage: error on sendmore");
      }

      const auto rawMsg = protoPacket.SerializeAsString();
      zmq::message_t message(rawMsg.data(), rawMsg.size());
      res = pSocket->send(message, zmq::send_flags::none);
      if (!res || (*res != rawMsg.size()))
      {
        spdlog::error("BroadcastSocketConnector - sendMessage: error on send");
      }
      // s_sendmore(*pSocket, packet->networkAddr);
      // s_send(*pSocket, protoPacket.SerializeAsString());

      // The above is used in place of a simple send message, this is due
      // to the fact that the sender is sending the message to the hub not as a publisher
      // but as a standard DEALER socket.
      // In turn, the hub will act as a PUBLISHER socket
      // s_send(*pSocket, packet->networkPacket->serialize());
    }
    catch(...)
    {
      const std::string errMsg = "BroadcastSocketConnector - sendMessage: undefined error";
      spdlog::error(errMsg);
      throw std::runtime_error(errMsg);
    }
  }
}  // sendMessage

void BroadcastSocketConnector::sendAbortMessage(Packet::UPtr packet)
{
  if (!packet)
  {
    const std::string errMsg = "BroadcastSocketConnector - sendAbortMessage: empty message";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  {
    // Critical section
    LockGuard lock(pOutputSocketMutex);

    try
    {
      BroadcastProtoPacket protoPacket;
      protoPacket.set_packettype(
          ::optilab::optnet::BroadcastProtoPacket_Type::BroadcastProtoPacket_Type_NetworkAbort);

      const auto spayload = packet->networkPacket->serialize();
      protoPacket.set_payloadtype(static_cast<int>(spayload.first));
      protoPacket.set_payload(spayload.second);

      // Set the source address of this broadcasted packet
      protoPacket.set_sourceaddr(packet->networkAddr);

      // Send the message back to the hub.
      // The hub will receive:
      // 1 - Address of this sender;
      // 2 - the address of the receiver;
      // 3 - the actual reply.
      // Which would require 3 corresponding s_recv(...) calls from the broker side.
      // @note the message to send back to the node's receiver is a serialized message
      zmq::message_t messageAddr(packet->networkAddr.data(), packet->networkAddr.size());
      auto res = pSocket->send(messageAddr, zmq::send_flags::sndmore);
      if (!res || (*res != packet->networkAddr.size()))
      {
        spdlog::error("BroadcastSocketConnector - sendAbortMessage: error on sendmore");
      }

      const auto rawMsg = protoPacket.SerializeAsString();
      zmq::message_t message(rawMsg.data(), rawMsg.size());
      res = pSocket->send(message, zmq::send_flags::none);
      if (!res || (*res != rawMsg.size()))
      {
        spdlog::error("BroadcastSocketConnector - sendAbortMessage: error on send");
      }
      // s_sendmore(*pSocket, packet->networkAddr);
      // s_send(*pSocket, protoPacket.SerializeAsString());

      // The above is used in place of a simple send message, this is due
      // to the fact that the sender is sending the message to the hub not as a publisher
      // but as a standard DEALER socket.
      // In turn, the hub will act as a PUBLISHER socket
      // s_send(*pSocket, packet->networkPacket->serialize());
    }
    catch(...)
    {
      const std::string errMsg = "BroadcastSocketConnector - sendAbortMessage: undefined error";
      spdlog::error(errMsg);
      throw std::runtime_error(errMsg);
    }
  }
}  // sendAbortMessage


}  // namespace optnet
}  // namespace optilab
