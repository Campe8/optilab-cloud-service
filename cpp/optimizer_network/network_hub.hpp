//
// Copyright OptiLab 2019. All rights reserved.
//
// Base class for the central network hub.
// The hub is the center of the network (in a star configuration) and acts
// as a broker.
//

#pragma once

#include <atomic>
#include <memory>   // for std::shared_ptr
#include <string>

#include <boost/chrono.hpp>
#include <boost/scoped_ptr.hpp>
#include <boost/thread.hpp>
#include <sparsepp/spp.h>
#include <zeromq/zhelpers.hpp>

#include "optilab_protobuf/network_packet.pb.h"
#include "optimizer_network/network_base_hub.hpp"
#include "optimizer_network/network_constants.hpp"
#include "optimizer_network/socket_connector.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace  optnet {

class SYS_EXPORT_CLASS NetworkHub : public NetworkBaseHub {
 public:
  using SPtr = std::shared_ptr<NetworkHub>;

 public:
  /// Constructor.
  /// @note throws std::invalid_argument on invalid TCP ports,
  /// i.e., less than or equal MIN_NETWORK_PORT_NUMBER or
  /// greater than MAX_NETWORK_PORT_NUMBER
  NetworkHub(const int inPort, const int outPort, const int broadcastInPort,
             const int broadcastOutPort);

  ~NetworkHub();

  /// Returns true if this network hub is running, false otherwise
  inline bool isRunning() const noexcept { return pRunning; }

  /// Turns down this hub
  void turnDown();

  /// Runs the hub,
  /// i.e., creates the sockets and listen for incoming messages.
  /// @note in order for a network node to send a point-to-point message using this hub,
  /// the node must register itself to this hub
  void run() override;

  /// Synchronizes the network, i.e., sends to all connected nodes the table
  /// mapping unique identifiers with addresses of registered nodes as a broadcasting message
  void synchNetwork() override;

 private:
  struct NetworkNodeInfo {
    using Ptr = std::shared_ptr<NetworkNodeInfo>;

    NetworkNodeInfo();

    int NodeId{networkconst::INVALID_NETWORK_NODE_ID};

    /// Input address the hub receives messages from a node.
    /// This address describes the "sender" network node
    std::string InNodeAddr;

    /// Output address the hub sends messages to a node.
    /// This address describes the "receiver" network node
    std::string OutNodeAddr;

    static std::atomic<int> NodeCtr;
  };

  /// Map of pairs <node address, node information>
  using NodeMap = spp::sparse_hash_map<std::string, NetworkNodeInfo::Ptr>;

  /// Map of pairs <node rank, node information>
  using NodeReverseMap = spp::sparse_hash_map<int, NetworkNodeInfo::Ptr>;

  /// Set of registered network addresses
  using NodeAddressSet = spp::sparse_hash_set<std::string>;

  /// Lock type on the internal mutex
  using LockGuard = boost::lock_guard<boost::mutex>;

 private:
  /// Flag indicating whether this network hub is running or not
  bool pRunning;

  /// Flag indicating whether or not listening threads have been initialized
  bool pInitThreads;

  /// Context for the hub
  std::shared_ptr<zmq::context_t> pContext;

  /// Sockets pointers used for cleanup
  SocketConnector::SocketSPtr pPToPInSocket;
  SocketConnector::SocketSPtr pPToPOutSocket;
  SocketConnector::SocketSPtr pBcastInSocket;
  SocketConnector::SocketSPtr pBcastOutSocket;

  /// Connector used for incoming communication
  BaseSocketConnector::SPtr pInputConnector;

  /// Connector used for outgoing communication
  BaseSocketConnector::SPtr pOutputConnector;

  /// Connector used for input messages for broadcasting
  BaseSocketConnector::SPtr pInBroadcastConnector;

  /// Connector used for output messages for broadcasting
  BaseSocketConnector::SPtr pOutBroadcastConnector;

  /// Future/shared state for communication listener
  mutable boost::unique_future<void> pCommListenerFuture;

  /// Future/shared state for broadcast listener
  mutable boost::unique_future<void> pBroadcastListenerFuture;

  /// Thread executing communication work
  boost::scoped_ptr<boost::thread> pCommThread;

  /// Thread executing broadcasting work
  boost::scoped_ptr<boost::thread> pBroadcastThread;

  /// Keep track of exceptions in the asynch. thread
  mutable bool pExceptionInAsynchCommThread;

  /// Keep track of exceptions in the asynch. broadcasting thread
  mutable bool pExceptionInAsynchBroadcastThread;

  /// Mutex synchronizing the access on the broacasting connector
  mutable boost::mutex pBroadcastMutex;

  /// Mutex synchronizing the access to the sockets for cleanup
  mutable boost::mutex pSocketMutex;

  /// Map of registered nodes
  NodeMap pNetworkNodeMap;

  /// Reverse map to get node information provided the rank of a node
  NodeReverseMap pNetworkNodeRevMap;

  /// Set of all registered network addresses
  NodeAddressSet pNetworkAddressSet;

  /// Timeout for turn down
  const boost::chrono::milliseconds pTurnDownTimeout;

  /// Turns down the asynchronous threads
  void turnDownAsyncThread();

  /// Closes and resets any open socket and context
  void turndownSocketsAndContext();

  /// Starts the listening process
  void listenOnSockets();

  /// Start the service working on communications
  void commRun();

  /// Start the service working on broadcast communications
  void broadcastRun();

  /// Checks for throws on future
  void checkForFutureException() const;

  /// Handles node activity on front-end and back-end receivers.
  /// Returns zero on success, non-zero otherwise
  int handleNodeActivity(const BaseSocketConnector::SPtr& connector);

  /// Handles a node activity.
  /// @note "payload" is the payload as received from the sender network node,
  /// i.e., the serialized network packet
  int handleNode(const BaseSocketConnector::SPtr& connector,
                 const std::string& nodeAddr,
                 const std::string& recipientAddr,
                 const std::string& payload);

  /// Registers the given network address in the set of registered addresses
  inline void registerNodeAddress(const std::string& addr) noexcept
  {
    pNetworkAddressSet.insert(addr);
  }

  /// Returns true if the given address is from a node which is not yet
  /// registered by this hub. Returns false otherwise
  inline bool isNewNode(const std::string& nodeAddr) const noexcept
  {
    return pNetworkAddressSet.find(nodeAddr) == pNetworkAddressSet.end();
  }

  /// Handle registration of a new node
  int handleNodeRegistrationRequest(const BaseSocketConnector::SPtr& connector,
                                    PointToPointProtoPacket& ptpPacket,
                                    const std::string& nodeAddr);

  /// Handle network synchronization request
  int handleNetworkSynchronizationRequest();

  /// Handle point to point requests
  int handlePointToPointCommunicationRequest(const std::string& payload,
                                             const std::string& nodeSender,
                                             const std::string& nodeReceiver);

  /// Registers a new network node in this hub
  int registerNode(PointToPointProtoPacket& ptpPacket, const std::string& nodeAddr);

  /// Handles node input broadcast activity.
  /// Returns zero on success, non-zero otherwise
  int handleBroadcastNodeActivity();

  /// Handles a broadcast node activity.
  /// @note "payload" is the payload as received from the sender network node,
  /// i.e., the serialized network packet.
  /// This payload is broadcasted to all nodes in the network
  int handleBroadcastNode(const std::string& payload);
};

}  // namespace optnet
}  // namespace optilab
