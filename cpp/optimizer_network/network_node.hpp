//
// Copyright OptiLab 2019. All rights reserved.
//
// Class representing a node in the network.
//

#pragma once

#include <memory>  // for std::shared_ptr
#include <string>

#include <boost/thread.hpp>
#include <sparsepp/spp.h>
#include <zeromq/zhelpers.hpp>

#include "optimizer_network/network_base_communicator.hpp"
#include "optimizer_network/packet.hpp"
#include "optimizer_network/socket_connector.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace  optnet {

class SYS_EXPORT_CLASS NetworkNode {
 public:
  using SPtr = std::shared_ptr<NetworkNode>;

 public:
  /// Constructor:
  /// - sendPort: port for outgoing point-to-point messages to another node
  /// - recvPort: port for incoming point-to-point communication to this node
  /// - broadcastSendPort: port to use to broadcast messages to other nodes
  /// - broadcastRecvPort: port using to receive broadcasted messages from other nodes
  /// - inAddr: network input address (default "localhost")
  /// - outAddr: network output address (default "localhost")
  /// - inAddr: network input broadcasting address (default "localhost")
  /// - inAddr: network outout broadcasting address (default "localhost")
  /// - isRoot: flag indicating whether or not this is the root node of the network
  NetworkNode(const int sendPort,
              const int recvPort,
              const int broadcastSendPort,
              const int broadcastRecvPort,
              const std::string& inAddr = std::string(),
              const std::string& outAddr = std::string(),
              const std::string& broadcastInAddr = std::string(),
              const std::string& broadcastOutAddr = std::string(),
              bool isRoot = false);

  ~NetworkNode();

  /// Initializes the sockets and connectors of this network node.
  /// @note if a connection timeout in msec is specified (greater than zero), the network
  /// node waits up to the specified timeout attempting to connect.
  /// Returns zero on success, non-zero otherwise
  int init(const int connectionTimeoutMsec=0) noexcept;

  /// Registers this node to the central hub.
  /// @note if "isRoot" is true, registers this node as the root of the network.
  /// @note there must be ONLY ONE root per network/hub.
  /// Upon registration, a new unique network identifier is assigned to this network node.
  /// @note throws std::runtime_error if this node is not previously initialized.
  /// See also "init(...)".
  /// Returns zero on success, non-zero otherwise
  int registerNodeToHub();

  /// Returns the rank of this node.
  /// @note throws std::runtime_error if this node has not being initialized
  int getRank() const;

  /// Returns the size of the network
  /// @note throws std::runtime_error if this node has not being initialized
  int getNetworkSize() const;

  /// Returns true if this node is the root node.
  /// Returns false otherwise
  inline bool isRoot() const noexcept { return pIsRoot; }

  /// Returns the network communicator hold by this node
  inline NetworkBaseCommunicator::SPtr getNetworkCommunicator() const noexcept
  {
    return pNetworkCommunicator;
  }

  /// Utility function: broadcast a packet from this node to all other nodes
  /// in the network.
  /// See also NetworkCommunicator for more information about the broadcast method.
  /// @note throws std::runtime_error if this node has not being initialized
  /// Returns zero on success, non-zero otherwise
  int broadcast(Packet::UPtr& packet, int root);

  /// Utility function: sends the given packet to the given destination.
  /// @note throws std::runtime_error if this node has not being initialized
  /// Returns zero on success, non-zero otherwise
  int send(Packet::UPtr packet, const int dest);

  /// Receive and returns a packet from the specified source.
  /// Returns an empty packet on failure.
  /// @note throws std::runtime_error if this node has not being initialized
  /// @note if a tag is given, this method blocks until a packet with specified
  /// tag is received.
  /// @note if the source is ANY_NETWORK_NODE_ID, it receives from any node in the network.
  /// @note this is a blocking call.
  /// @note the network address of the packet is automatically set by this method
  Packet::UPtr receive(const int source, PacketTag::UPtr tag=nullptr);

 private:
  /// Ports and addresses
  std::string pSendPointToPointAddr;
  std::string pRecvPointToPointAddr;
  std::string pSendBroadcastAddr;
  std::string pRecvBroadcastAddr;

  /// Flag indicating whether or not this is the root node of the network
  bool pIsRoot;

  /// Network communicator
  NetworkBaseCommunicator::SPtr pNetworkCommunicator;
};

}  // namespace optnet
}  // namespace optilab
