#include "optimizer_network/network_hub.hpp"

#include <cassert>
#include <stdexcept>  // for std::runtime_error
#include <sstream>

#include <boost/bind.hpp>
#include <spdlog/spdlog.h>

#include "optimizer_network/network_utilities.hpp"

namespace {
constexpr int kDefaultTurnDownTimeOutMsec = 5000;
// constexpr int kNoPollTimeout = -1;

// Wait 2 sec. on polling, then repeat.
// This allows the threads to check if the hub is shutting down before polling again
constexpr int kPollTimeoutMsec = 2000;

std::string readFromConnector(const optilab::optnet::BaseSocketConnector::SPtr& connector)
{
  if (!connector)
  {
    throw std::invalid_argument("readFromConnector - empty connector");
  }

  zmq::message_t message;
  connector->getSocket()->recv(message);
  return std::string(static_cast<char*>(message.data()), message.size());
}  // readFromConnector

/// Sends the given payload to the given network node address on the socket specified
/// by the given connector
void sendMessageToNetworkNode(const optilab::optnet::BaseSocketConnector::SPtr& connector,
                              const std::string& payload, const std::string& senderAddr,
                              const std::string& receiverAddr)
{
  if (!connector)
  {
    throw std::invalid_argument("sendRequestToNetworkNode - empty connector");
  }

  // Sending a message to a network node is done as follows:
  // 1 - Send the receiver address.
  //     This is consumed by the ZMQ_ROUTER socket to address the payload to the proper
  //     network node
  zmq::message_t recvAddr(receiverAddr.data(), receiverAddr.size());
  auto res = connector->getSocket()->send(recvAddr, zmq::send_flags::sndmore);
  if (!res || (*res != receiverAddr.size()))
  {
    if (!res)
    {
      spdlog::error("NetworkHub - sendMessageToNetworkNode: error on sendmore (receiverAddr). "
              "Non-blocking mode was requested and the message cannot be sent at the moment. "
              "The message cannot be queued on the socket.");
    }
    spdlog::error("NetworkHub - sendMessageToNetworkNode: error on sendmore (receiverAddr). "
            "String to send " + receiverAddr + " of size " + std::to_string(receiverAddr.size()) +
            " actually sent " + std::to_string(*res));

    throw std::runtime_error("NetworkHub - sendMessageToNetworkNode: error on sendmore.");
  }
  // s_sendmore(*(connector->getSocket()), receiverAddr);

  // 2 - Send the sender address to the receiver node
  zmq::message_t sendAddr(senderAddr.data(), senderAddr.size());
  res = connector->getSocket()->send(sendAddr, zmq::send_flags::sndmore);
  if (!res || (*res != senderAddr.size()))
  {
    if (!res)
    {
      spdlog::error("NetworkHub - sendMessageToNetworkNode: error on sendmore (senderAddr). "
              "Non-blocking mode was requested and the message cannot be sent at the moment. "
              "The message cannot be queued on the socket.");
    }
    spdlog::error("NetworkHub - sendMessageToNetworkNode: error on sendmore (senderAddr). "
            "String to send " + senderAddr + " of size " + std::to_string(senderAddr.size()) +
            " actually sent " + std::to_string(*res));

    throw std::runtime_error("NetworkHub - sendMessageToNetworkNode: error on sendmore.");
  }
  // s_sendmore(*(connector->getSocket()), senderAddr);

  // 3 - Send the actual payload with "s_send(...)"
  zmq::message_t message(payload.data(), payload.size());
  res = connector->getSocket()->send(message, zmq::send_flags::none);
  if (!res || (*res != payload.size()))
  {
    if (!res)
    {
      spdlog::error("SocketConnector - sendMessageToNetworkNode: error on send (payload). "
              "Non-blocking mode was requested and the message cannot be sent at the moment. "
              "The message cannot be queued on the socket.");
      }
      spdlog::error("SocketConnector - sendMessageToNetworkNode: error on send (payload). "
              "String to send " + payload + " of size " + std::to_string(payload.size()) +
              " actually sent " + std::to_string(*res));

      throw std::runtime_error("SocketConnector - sendMessageToNetworkNode: error on send.");
  }
  // s_send(*(connector->getSocket()), payload);
}  // sendRequestToNetworkNode

void broadcastMessageToNetworkNode(const optilab::optnet::BaseSocketConnector::SPtr& connector,
                                   const std::string& payload)
{
  if (!connector)
  {
    throw std::invalid_argument("broadcastMessageToNetworkNode - empty connector");
  }

  zmq::message_t message(payload.data(), payload.size());
  auto res = connector->getSocket()->send(message, zmq::send_flags::none);
  if (!res || (*res != payload.size()))
  {
    if (!res)
    {
      spdlog::error("SocketConnector - broadcastMessageToNetworkNode: error on send (payload). "
              "Non-blocking mode was requested and the message cannot be sent at the moment. "
              "The message cannot be queued on the socket.");
    }
    spdlog::error("SocketConnector - broadcastMessageToNetworkNode: error on send (payload). "
            "String to send " + payload + " of size " + std::to_string(payload.size()) +
            " actually sent " + std::to_string(*res));

      throw std::runtime_error("SocketConnector - broadcastMessageToNetworkNode: error on send.");
  }
  // s_send(*(connector->getSocket()), payload);
}  // broadcastMessageToNetworkNode

}  // namespace

namespace optilab {
namespace  optnet {

// Initialize static counters
std::atomic<int> NetworkHub::NetworkNodeInfo::NetworkNodeInfo::NodeCtr(
    networkconst::ROOT_NETWORK_NODE_ID + 1);

NetworkHub::NetworkNodeInfo::NetworkNodeInfo()
: NodeId(NodeCtr++)
{
}

NetworkHub::NetworkHub(const int inPort, const int outPort, const int broadcastInPort,
                       const int broadcastOutPort)
: NetworkBaseHub(inPort, outPort, broadcastInPort, broadcastOutPort),
  pRunning(false),
  pInitThreads(false),
  pExceptionInAsynchCommThread(false),
  pExceptionInAsynchBroadcastThread(false),
  pTurnDownTimeout(boost::chrono::milliseconds(kDefaultTurnDownTimeOutMsec))
{
}

NetworkHub::~NetworkHub()
{
  turnDown();
}

void NetworkHub::turndownSocketsAndContext()
{
  // Critical section, block on socket cleanup
  LockGuard lock(pSocketMutex);

  if (pInputConnector) pInputConnector->closeSocket();
  if (pOutputConnector) pOutputConnector->closeSocket();
  if (pInBroadcastConnector) pInBroadcastConnector->closeSocket();
  if (pOutBroadcastConnector) pOutBroadcastConnector->closeSocket();
  if (pContext)
  {
    pContext->close();
    pContext.reset();
  }
}  // turndownSocketsAndContext

void NetworkHub::run()
{
  if (pRunning)
  {
    const std::string errMsg = "NetworkHub - run: network hub already running";
    spdlog::error(errMsg);
    return;
  }

  try
  {
    // Create the context for the sockets
    pContext = std::make_shared<zmq::context_t>(networkconst::CONTEXT_DEFAULT_NUM_THREADS);

    // Create the 4 sockets for in/out point to point and broadcast packets
    pPToPInSocket = std::make_shared<zmq::socket_t>(*pContext, ZMQ_ROUTER);
    pPToPOutSocket = std::make_shared<zmq::socket_t>(*pContext, ZMQ_ROUTER);
    pBcastInSocket = std::make_shared<zmq::socket_t>(*pContext, ZMQ_ROUTER);
    pBcastOutSocket = std::make_shared<zmq::socket_t>(*pContext, ZMQ_PUB);

    // Get the addresses of the sockets
    const std::string frontendAddr = getNetworkInSocketAddr();
    const std::string backendAddr = getNetworkOutSocketAddr();
    const std::string broadcastInAddr = getNetworkInBroadcastSocketAddr();
    const std::string broadcastOutAddr = getNetworkOutBroadcastSocketAddr();

    // Bind the hub to the input address
    spdlog::info("NetworkHub - run: binding to input address " + frontendAddr);
    pPToPInSocket->bind(frontendAddr.c_str());

    // Bind the hub to the output address
    spdlog::info("NetworkHub - run: binding to output address " + backendAddr);
    pPToPOutSocket->bind(backendAddr.c_str());

    // Bind the hub to the input broadcast address
    spdlog::info("NetworkHub - run: binding to input broadcast address " + broadcastInAddr);
    pBcastInSocket->bind(broadcastInAddr.c_str());

    // Bind the hub to the output broadcast address
    spdlog::info("NetworkHub - run: binding to output broadcast address " + broadcastOutAddr);
    pBcastOutSocket->bind(broadcastOutAddr.c_str());
  }
  catch (...)
  {
    pRunning = false;
    throw;
  }

  // Set the startup flag as running
  pRunning = true;

  // Create a new socket connector and start listening for incoming messages
  spdlog::info("NetworkHub - run: binding on sockets");
  pInputConnector = std::make_shared<SocketConnector>(pPToPInSocket);
  pOutputConnector = std::make_shared<SocketConnector>(pPToPOutSocket);
  pInBroadcastConnector = std::make_shared<SocketConnector>(pBcastInSocket);
  pOutBroadcastConnector = std::make_shared<BroadcastSocketConnector>(pBcastOutSocket);

  // Start listening for incoming messages
  spdlog::info("NetworkHub - run: listen on sockets");
  listenOnSockets();
}  // run

void NetworkHub::turnDown()
{
  // Turn down sockets and context
  turndownSocketsAndContext();
  if (pInitThreads)
  {
    turnDownAsyncThread();
    try
    {
      // Get the value from the future
      if (!pExceptionInAsynchCommThread)
      {
        if (!pCommListenerFuture.has_exception())
        {
          pCommListenerFuture.get();
        }
      }

      // Get the value from the future
      if (!pExceptionInAsynchBroadcastThread)
      {
        if (!pBroadcastListenerFuture.has_exception())
        {
          pBroadcastListenerFuture.get();
        }
      }
    }
    catch(const boost::future_error& err)
    {
      std::string errMsg = err.what();
      auto code = err.code();
      errMsg += ": " + code.message();
      spdlog::error(errMsg);
    }
    catch (const std::exception& ex)
    {
      spdlog::error("NetworkHub - turnDown: error " + std::string(ex.what()));
    }
    catch (...)
    {
      spdlog::error("NetworkHub - turnDown: unknown exception");
    }

    pCommThread.reset();
    pBroadcastThread.reset();
  }

  // Switch running and threads flags to false
  pInitThreads = false;
  pRunning = false;
}  // turnDown

void NetworkHub::listenOnSockets()
{
  assert(pInputConnector);
  assert(pOutputConnector);
  assert(pInBroadcastConnector);
  assert(pOutBroadcastConnector);

  // Start the threads listening on communication sockets
  boost::packaged_task<void> commTask(boost::bind(&NetworkHub::commRun, this));
  pCommListenerFuture = commTask.get_future();

  // Run the thread on the commTask
  pCommThread.reset(new boost::thread(boost::move(commTask)));

  // Check if the future throws
  checkForFutureException();

  // Start the threads listening on broadcast sockets
  boost::packaged_task<void> broadcastTask(boost::bind(&NetworkHub::broadcastRun, this));
  pBroadcastListenerFuture = broadcastTask.get_future();

  // Run the thread on the broadcastTask
  pBroadcastThread.reset(new boost::thread(boost::move(broadcastTask)));

  // Set thread initialization flag
  pInitThreads = true;

  // Check if the future throws
  checkForFutureException();
}  // listenOnSockets

void NetworkHub::commRun()
{
  // Logic of the while-loop:
  // - Poll output always, input only if 1+ nodes are ready
  // - If node replies, queue nodes as ready and forward reply
  //   to input nodes if necessary
  // - If input node requests, pop next output node and send request to it
  spdlog::info("NetworkHub - commRun: start input and output poll");
  while (pRunning)
  {
    // Set an interruption point a the beginning of every new loop iteration
    boost::this_thread::interruption_point();
    {
      // Check sockets before start polling
      LockGuard lock(pSocketMutex);
      if (!pOutputConnector->getSocket() || !pInputConnector->getSocket()) return;
    }

    // Initialize poll set and start polling for incoming requests
    zmq::pollitem_t pollItems[] = {
        //  Always poll for node activity on output connections
        { *(pOutputConnector->getSocket()), 0, ZMQ_POLLIN, 0 },
        //  Poll input only if we have available output nodes
        { *(pInputConnector->getSocket()), 0, ZMQ_POLLIN, 0 }
    };
    zmq::poll(&pollItems[0], 2, kPollTimeoutMsec);

    // Set another interruption point after polling for incoming messages
    boost::this_thread::interruption_point();

    // Handle worker activity on back-end
    if (pollItems[0].revents & ZMQ_POLLIN)
    {
      const int err = handleNodeActivity(pOutputConnector);
      if (err)
      {
        spdlog::error("NetworkHub - commRun: error on handling output activity");
        break;
      }
    }

    // Handle client activity on front-end
    if (pollItems[1].revents & ZMQ_POLLIN)
    {
      const int err = handleNodeActivity(pInputConnector);
      if (err)
      {
        spdlog::error("NetworkHub - commRun: error on handling input activity");
        break;
      }
    }
  }  // while
}  // commRun

int NetworkHub::handleNodeActivity(const BaseSocketConnector::SPtr& connector)
{
  assert(connector);
  boost::this_thread::interruption_point();

  // According to the broker-client contract,
  // each message sent from a node is an envelop with two frames:
  // 1 - the address of the sender
  // 2 - the address of the receiver
  // 3 - the payload (i.e., the raw message)
  int err = networkconst::ERR_GENERIC_ERROR;

  // Check if this thread has to return
  if (!pRunning) return err;
  try
  {
    // critical section
    const std::string senderAddr = readFromConnector(connector);
    const std::string recipientAddr = readFromConnector(connector);
    const std::string payload = readFromConnector(connector);
    err = handleNode(connector, senderAddr, recipientAddr, payload);
  }
  catch (...)
  {
    spdlog::error("NetworkHub - handleNodeActivity: error on receiving a message "
        "from a network node");
    err = networkconst::ERR_GENERIC_ERROR;
  }

  return err;
}  // handleNodeActivity

int NetworkHub::handleNode(const BaseSocketConnector::SPtr& connector,
                           const std::string& nodeAddr,
                           const std::string& recipientAddr,
                           const std::string& payload)
{
  // Set interruption point before proceeding further
  boost::this_thread::interruption_point();

  int err = networkconst::ERR_NO_ERROR;

  // Deserialize the point-to-point payload
  PointToPointProtoPacket protoPayload;
  protoPayload.ParseFromString(payload);

  // Check if the node is a new node.
  // If so, register it and pair it with a given worker
  switch(protoPayload.packettype())
  {
    case ::optilab::optnet::PointToPointProtoPacket_Type::
    PointToPointProtoPacket_Type_HubRegistration:
    {
      err = handleNodeRegistrationRequest(connector, protoPayload, nodeAddr);
      break;
    }
    case ::optilab::optnet::PointToPointProtoPacket_Type::
    PointToPointProtoPacket_Type_HubUnregistration:
    {
      spdlog::error("NetworkHub - handleNode: network node un-registration not supported yet");
      break;
    }
    case ::optilab::optnet::PointToPointProtoPacket_Type::
    PointToPointProtoPacket_Type_NetworkSynchronization:
    {
      err = handleNetworkSynchronizationRequest();
      break;
    }
    default:
    {
      if (protoPayload.packettype() != ::optilab::optnet::PointToPointProtoPacket_Type::
          PointToPointProtoPacket_Type_PointToPointPayload)
      {
        const std::string errMsg = "NetworkHub - handleNode: invalid point to point "
            "message type";
        spdlog::error(errMsg);
        throw std::runtime_error(errMsg);
      }
      err = handlePointToPointCommunicationRequest(payload, nodeAddr, recipientAddr);
      break;
    }
  }

  return err;
}  // handleFrontendNode

int NetworkHub::handlePointToPointCommunicationRequest(const std::string& payload,
                                                       const std::string& nodeSender,
                                                       const std::string& nodeReceiver)
{
  int err = networkconst::ERR_NO_ERROR;

  // Check if the recipient node is registered
  if (isNewNode(nodeReceiver))
  {
    spdlog::error("NetworkHub - handleNode: no known recipient address " +
                  utils::bytesToString(nodeReceiver));
    return networkconst::ERR_GENERIC_ERROR;
  }

  // Check if this thread has to return
  if (!pRunning) return err;

  try
  {
    sendMessageToNetworkNode(pOutputConnector, payload, nodeSender, nodeReceiver);
  }
  catch (...)
  {
    spdlog::error("NetworkHub - handleNode: cannot send the message from sender node " +
                  nodeSender + " to receiver node " + nodeReceiver);
    // Set the error status
    err = networkconst::ERR_GENERIC_ERROR;
  }

  return err;
}  // handlePointToPointCommunicationRequest

int NetworkHub::handleNodeRegistrationRequest(const BaseSocketConnector::SPtr& connector,
                                              PointToPointProtoPacket& ptpPacket,
                                              const std::string& nodeAddr)
{
  int err = networkconst::ERR_NO_ERROR;
  if (!isNewNode(nodeAddr))
  {
    spdlog::warn("NetworkHub - handleNode: network node already registered, return " +
                 utils::bytesToString(nodeAddr));

    // Set invalid id into the protobuf network packet
    ptpPacket.set_payload(std::to_string(networkconst::INVALID_NETWORK_NODE_ID));

    // Send the reply to the network node about successful registration
    sendMessageToNetworkNode(connector, ptpPacket.SerializeAsString(), nodeAddr,
                             nodeAddr);
    return err;
  }

  err = registerNode(ptpPacket, nodeAddr);
  if (err)
  {
    spdlog::error("NetworkHub - handleNode: error in registering a new node " +
                  utils::bytesToString(nodeAddr));
    ptpPacket.set_payload(std::to_string(networkconst::INVALID_NETWORK_NODE_ID));
  }

  // Send the reply to the network node about successful registration.
  // @note the connector here is needed since the HUB must return the response to the same
  // socket that requested registration
  sendMessageToNetworkNode(connector, ptpPacket.SerializeAsString(), nodeAddr, nodeAddr);

  // Register the node and return
  return err;
}  // handleNodeRegistrationRequest

int NetworkHub::handleNetworkSynchronizationRequest()
{
  // Synchronize the network and return
  synchNetwork();
  return networkconst::ERR_NO_ERROR;
}  // handleNetworkSynchronizationRequest

int NetworkHub::registerNode(PointToPointProtoPacket& ptpPacket, const std::string& nodeAddr)
{
  if (nodeAddr.empty())
  {
    spdlog::error("NetworkHub - registerNode: empty node address, return");
    return networkconst::ERR_GENERIC_ERROR;
  }

  NetworkNodeInfo::Ptr nodeInfo;
  const int rank = std::stoi(ptpPacket.payload());
  if (rank < 0)
  {
    // This is the first time registering a socket for the given requesting node.
    // Setup a new identifier and return
    nodeInfo = std::make_shared<NetworkNodeInfo>();
    if (ptpPacket.sourceroot())
    {
      nodeInfo->NodeId = networkconst::ROOT_NETWORK_NODE_ID;
    }

    // Set the first address as input address
    nodeInfo->InNodeAddr = nodeAddr;

    // Add the node information into the network map and the reverse map
    pNetworkNodeMap[nodeAddr] = nodeInfo;
    pNetworkNodeRevMap[nodeInfo->NodeId] = nodeInfo;

    // Register the address in the hub
    registerNodeAddress(nodeAddr);
  }
  else
  {
    // This is another socket to be registered for a node who previously registered a socket.
    // Retrieve the rank of the node previously used since this is the same network node asking
    // to register another of its ports.
    if (pNetworkNodeRevMap.find(rank) == pNetworkNodeRevMap.end())
    {
      spdlog::error("NetworkHub - registerNode: "
          "cannot find rank in the reverse network map, return");
      return networkconst::ERR_GENERIC_ERROR;
    }
    nodeInfo = pNetworkNodeRevMap[rank];

    // Set the new address for the new socket.
    // This is the address the sender has to use to send to the network recipient
    nodeInfo->OutNodeAddr = nodeAddr;

    // Register the address in the hub
    registerNodeAddress(nodeAddr);
  }
  const std::string nodeUniqueIdStr = std::to_string(nodeInfo->NodeId);
  spdlog::info("NetworkHub - register network node with rank " +
               nodeUniqueIdStr + " and unique identifier " + utils::bytesToString(nodeAddr));

  // Update the network protobuf packet
  ptpPacket.set_payload(nodeUniqueIdStr);

  return networkconst::ERR_NO_ERROR;
}  // registerNode

void NetworkHub::broadcastRun()
{
  spdlog::info("NetworkHub - broadcastRun: start broadcast listener");

  while (pRunning)
  {
    // Set an interruption point a the beginning of every new loop iteration
    boost::this_thread::interruption_point();
    {
      // Check sockets before start polling
      LockGuard lock(pSocketMutex);
      if (!pInBroadcastConnector->getSocket()) return;
    }

    // Initialize poll set
    zmq::pollitem_t pollItems[] = {
        //  Always poll for node activity on input broadcast connections
        { *(pInBroadcastConnector->getSocket()), 0, ZMQ_POLLIN, 0 }
    };

    // Blocking call on polling for incoming messages
    int numStruct = zmq::poll(&pollItems[0], 1, kPollTimeoutMsec);
    // if (numStruct == 0) continue;

    // Set an interruption point after receiving a message from the socket
    boost::this_thread::interruption_point();

    //  Handle worker activity on back-end
    if (pollItems[0].revents & ZMQ_POLLIN)
    {
      const int err = handleBroadcastNodeActivity();
      if (err)
      {
        spdlog::error("NetworkHub - broadcastRun: error on handling input activity");
        break;
      }
    }
  }  // while
}  // broadcastRun

int NetworkHub::handleBroadcastNodeActivity()
{
  assert(pInBroadcastConnector);
  boost::this_thread::interruption_point();

  // According to the broker-client contract,
  // each message sent from a node is an envelop with two frames:
  // 1 - the address of the sender
  // 2 - the address of the receiver
  // 3 - the payload (i.e., the raw message)
  int err = networkconst::ERR_GENERIC_ERROR;

  // Check if this thread has to return
  if (!pRunning) return err;
  try
  {
    // critical section

    // Discard address of the sender
    readFromConnector(pInBroadcastConnector);

    // Discard address of receiver (all receivers)
    readFromConnector(pInBroadcastConnector);

    // Get the payload and broadcast it
    err = handleBroadcastNode(readFromConnector(pInBroadcastConnector));
  }
  catch (...)
  {
    spdlog::error("NetworkHub - handleNodeActivity: error on receiving a message "
        "from a network node");
    err = networkconst::ERR_GENERIC_ERROR;
  }

  return err;
}  // handleBroadcastNodeActivity

int NetworkHub::handleBroadcastNode(const std::string& payload)
{
  // Set interruption point before proceeding further
  boost::this_thread::interruption_point();

  int err = networkconst::ERR_NO_ERROR;

  // Check if this thread has to return
  if (!pRunning) return err;

  try
  {
    // Critical section
    LockGuard lock(pBroadcastMutex);
    broadcastMessageToNetworkNode(pOutBroadcastConnector, payload);
  }
  catch (...)
  {
    spdlog::error("NetworkHub - handleBroadcastNode: cannot broadcast message");

    // Set the error status
    err = networkconst::ERR_GENERIC_ERROR;
  }

  return err;
}  // handleFrontendNode

void NetworkHub::synchNetwork()
{
  // Check if this thread has to return
  if (!pRunning) return;

  {
    // Critical section
    LockGuard lock(pBroadcastMutex);

    BroadcastProtoPacket bpacket;
    bpacket.set_packettype(::optilab::optnet::BroadcastProtoPacket_Type::
                           BroadcastProtoPacket_Type_NetworkSynchronization);

    // Add the map network map to the packet
    auto& nmap = *(bpacket.mutable_networkmap());
    for (const auto& iter : pNetworkNodeRevMap)
    {
      nmap[iter.first].set_sendaddr(iter.second->InNodeAddr);
      nmap[iter.first].set_recvaddr(iter.second->OutNodeAddr);
    }
    broadcastMessageToNetworkNode(pOutBroadcastConnector, bpacket.SerializeAsString());
  }
}  // synchNetwork

void NetworkHub::checkForFutureException() const
{
  if (pCommListenerFuture.has_exception())
  {
    // Set exception flag
    pExceptionInAsynchCommThread = true;

    // Re-throw
    pCommListenerFuture.get();
  }

  if (pBroadcastListenerFuture.has_exception())
  {
    // Set exception flag
    pExceptionInAsynchBroadcastThread = true;

    // Re-throw
    pBroadcastListenerFuture.get();
  }
}  // checkForFutureException

void NetworkHub::turnDownAsyncThread()
{
  // Set running flag to termination
  pRunning = false;

  // Wait for the shutdown
  boost::future_status waitCommStatus;
  boost::future_status waitBroadcastStatus;
  {
    waitCommStatus = pCommListenerFuture.wait_for(pTurnDownTimeout);
    waitBroadcastStatus = pBroadcastListenerFuture.wait_for(pTurnDownTimeout);
  }

  if (waitCommStatus == boost::future_status::timeout)
  {
    pCommThread->interrupt();
  }

  if (waitBroadcastStatus == boost::future_status::timeout)
  {
    pBroadcastThread->interrupt();
  }

  boost::this_thread::disable_interruption di;
  static const uint32_t WAIT_PERIOD = 2;
  bool joined = false;
  uint32_t waitTime = 0;
  while (!joined)
  {
    joined = pCommThread->try_join_for(boost::chrono::seconds(WAIT_PERIOD));
    joined = joined && pBroadcastThread->try_join_for(boost::chrono::seconds(WAIT_PERIOD));
    if (!joined)
    {
      waitTime += WAIT_PERIOD;
    }

    if (waitTime > 2 * WAIT_PERIOD)
    {
      pCommThread.reset();
      pBroadcastThread.reset();
      return;
    }
  }
}  // turnDownAsyncThread

}  // namespace optnet
}  // namespace optilab
