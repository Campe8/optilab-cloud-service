//
// Copyright OptiLab 2019. All rights reserved.
//
// Constant for the optimizer network framework.
//

#pragma once

#include "system/system_export_defs.hpp"

namespace optilab {
namespace  optnet {

namespace networkconst {
extern int ANY_NETWORK_NODE_ID SYS_EXPORT_VAR;
extern int CONTEXT_DEFAULT_NUM_THREADS SYS_EXPORT_VAR;
extern int ERR_GENERIC_ERROR SYS_EXPORT_VAR;
extern int ERR_NO_ERROR SYS_EXPORT_VAR;
extern int INVALID_NETWORK_NODE_ID SYS_EXPORT_VAR;
extern const char LOCAL_HOST_NAME[] SYS_EXPORT_VAR;
extern int MAX_NETWORK_PORT_NUMBER SYS_EXPORT_VAR;
extern int MIN_NETWORK_PORT_NUMBER SYS_EXPORT_VAR;
extern int ROOT_NETWORK_NODE_ID SYS_EXPORT_VAR;
}  // namespave networkconst

}  // namespace optnet
}  // namespace optilab
