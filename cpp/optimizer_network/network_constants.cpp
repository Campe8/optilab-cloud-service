#include "optimizer_network/network_constants.hpp"

namespace optilab {
namespace  optnet {

namespace networkconst {
int ANY_NETWORK_NODE_ID = -1;
int CONTEXT_DEFAULT_NUM_THREADS = 1;
int ERR_GENERIC_ERROR = 1;
int ERR_NO_ERROR = 0;
int INVALID_NETWORK_NODE_ID = -1;
const char LOCAL_HOST_NAME[] = "localhost";
int MAX_NETWORK_PORT_NUMBER = 65535;
int MIN_NETWORK_PORT_NUMBER = 1;
int ROOT_NETWORK_NODE_ID = 0;
}  // namespave networkconst

}  // namespace optnet
}  // namespace optilab
