//
// Copyright OptiLab 2019-2020. All rights reserved.
//
// Base class for the network communicator.
// Similar to MPI frameworks, a communicator encapsulates
// all communication among a set of processes.
// In the context of optimizer network, a communicator is implemented
// as a wrapper around socket connectors for point to point and
// and broadcast communication.
//

#pragma once

#include <memory>  // for std::shared_ptr

#include "optimizer_network/packet.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace  optnet {

class SYS_EXPORT_CLASS NetworkBaseCommunicator {
 public:
  using SPtr = std::shared_ptr<NetworkBaseCommunicator>;

 public:
  virtual ~NetworkBaseCommunicator() = default;

  /// Turn down this communicator, stop threads and return
  virtual void turnDown() = 0;

  /// Initializes this communicator.
  /// Returns zero on success, non-zero otherwise.
  /// @note if "initTimeoutMsec" is specified and greater than zero, this method will wait up
  /// to "initTimeoutMsec" msec. before returning if the hub is not able to respond successfully.
  /// A negative timeout value (default) makes the network communicator wait indefinitely
  virtual int init(int initTimeoutMsec = -1) = 0;

  /// Returns the rank of the process running on this communication channel.
  /// The rank is the unique network identifier associated with this process when
  /// registering into the network hub
  virtual int getRank() const noexcept = 0;

  /// Returns the number of processes attached to this communicator.
  /// Better, returns the number of processes/sockets attached to the hub
  virtual int getSize() const noexcept = 0;

  /// Terminates this communicator.
  /// This method makes a "best attempt" to abort all tasks running
  /// on this communicator
  virtual void abort() = 0;

  /// Broadcasts the given packet from the process with rank "root"
  /// to all other processes attached to this communicator.
  /// If the communicator calling this method IS the root, broadcast the packet
  /// to all other nodes. If the communicator calling this method IS NOT the root,
  /// waits to receive the packet from the the root.
  /// In other words, broadcast works like the following:
  /// if rank == root:
  ///   send packet to rank 0, 1, ..., root - 1, root + 1, ..., n
  /// else:
  ///   receive(packet, root);
  /// endif
  /// @note DO NOT use receive to read broadcast messages, use "broadcast(...)".
  /// This is similar to MPI framework. See also
  /// https://stackoverflow.com/questions/7864075/using-mpi-bcast-for-mpi-communication.
  /// @note if root is not rank, i.e., if the caller reads a broadcasted message,
  /// the received packet will override the given packet, if any.
  /// @note if root is not rank, this is a blocking call.
  /// Returns zero on success, non-zero otherwise
  virtual int broadcast(Packet::UPtr& packet, const int root) noexcept = 0;

  /// Sends the given packet to the given destination.
  /// Returns zero on success, non-zero otherwise.
  /// @note the packet may contain a packet tag.
  /// @note this is a non-blocking call
  virtual int send(Packet::UPtr packet, const int dest) noexcept = 0;

  /// Receives a packet from the specified source and sets its content into the given
  /// packet instance.
  /// Returns zero on success, non-zero otherwise.
  /// @note if the given packet contains a tag, this method blocks until a packet with specified
  /// tag is received.
  /// @note if the source is ANY_NETWORK_NODE_ID, it receives from any node in the network.
  /// @note this is a blocking call
  virtual int receive(Packet::UPtr& packet, const int source) noexcept = 0;

  /// Non blocking version of "receive(...)".
  /// Receives a packet from the specified source matching the tag if specified.
  /// Sets its content into the given packet instance.
  /// If "err" is specified, sets the flag indicating error (if any).
  /// Returns zero on success, i.e., there was a match and the packet has been set.
  /// Returns non-zero otherwise, i.r., there was an error, there was no match, or there
  /// was no packet to be received.
  /// @note if the source is ANY_NETWORK_NODE_ID, it receives from any node in the network.
  /// @note this is a non blocking call.
  virtual int ireceive(Packet::UPtr& packet, const int source, bool* err=nullptr) noexcept = 0;

  /// Probes the input socket and wait for any incoming message from
  /// any other network node.
  /// @note this is a blocking call
  virtual int probe(Packet::UPtr& packet) noexcept = 0;

  /// Non blocking version of "probe(...)".
  /// Probes the input socket and wait for any incoming message from
  /// any other network node.
  /// If "err" is specified, sets the flag indicating error (if any).
  /// @note this is a non blocking call
  virtual int iprobe(Packet::UPtr& packet, bool* err=nullptr) noexcept = 0;

  /// Waits to receive any tag from the given source.
  /// When a message is received, if "replaceTag" is true, it sets it into the packet tag.
  /// If "replaceTag" is false, the tag is not replaced (e.g., this method can be used
  /// as a Boolean expression in a while-loop).
  /// If the previous tag and the received tag are the same, it returns zero.
  /// Returns non-zero otherwise
  virtual int waitSpecTagFromSpecSource(Packet::UPtr& packet,
                                        const int source, bool replaceTag=true) noexcept = 0;
};

}  // namespace optnet
}  // namespace optilab
