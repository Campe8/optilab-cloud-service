#include "optimizer_network/network_utilities.hpp"

namespace optilab {
namespace  optnet {

namespace utils {

/// Converts the input set of chars into a string decimal representation
std::string bytesToString(const std::string& in, char separator)
{
  std::stringstream ss;
  for (int idx = 0; idx < static_cast<int>(in.size()); ++idx)
  {
    ss << static_cast<int>(in[idx]) << separator;
  }
  ss <<  static_cast<int>(in.back());
  return ss.str();
}  // bytesToString

std::shared_ptr<zmq::socket_t> buildPointToPointSocket(zmq::context_t& ctx)
{
  auto socket = std::make_shared<zmq::socket_t>(ctx, ZMQ_DEALER);
  return socket;
}  // buildPointToPointSocket

std::shared_ptr<zmq::socket_t> buildSenderBroadcastSocket(zmq::context_t& ctx)
{
  auto socket = std::make_shared<zmq::socket_t>(ctx, ZMQ_DEALER);
  return socket;
}  // buildSenderBroadcastSocket

std::shared_ptr<zmq::socket_t> buildReceivingBroadcastSocket(zmq::context_t& ctx)
{
  auto socket = std::make_shared<zmq::socket_t>(ctx, ZMQ_SUB);
  return socket;
}  // buildReceivingBroadcastSocket

}  // namesoace utils
}  // namespace optnet
}  // namespace optilab
