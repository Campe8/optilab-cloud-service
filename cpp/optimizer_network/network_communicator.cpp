#include "optimizer_network/network_communicator.hpp"

#include <cassert>
#include <stdexcept>  // for std::invalid_argument
#include <thread>

#include <boost/chrono.hpp>
#include <boost/bind.hpp>
#include <spdlog/spdlog.h>

#include "optimizer_network/network_constants.hpp"
#include "optimizer_network/network_packet.hpp"
#include "optimizer_network/network_utilities.hpp"

namespace {
constexpr int kDefaultTurnDownTimeOutMsec = 1500;

/// Close a socket given a connector or returns if nullptr
void closeSocket(optilab::optnet::BaseSocketConnector* conn)
{
  if (conn)
  {
    conn->closeSocket();
  }
}  // closeSocket

/// Close a context of returns if nullptr
void closeContext(zmq::context_t* ctx)
{
  if (ctx)
  {
    ctx->close();
  }
}  // closeContext

void closeSocketAndContext(optilab::optnet::BaseSocketConnector* conn1,
                           optilab::optnet::BaseSocketConnector* conn2,
                           optilab::optnet::BaseSocketConnector* conn3,
                           optilab::optnet::BaseSocketConnector* conn4,
                           zmq::context_t* ctx)
{
  closeSocket(conn1);
  closeSocket(conn2);
  closeSocket(conn3);
  closeSocket(conn4);
  closeContext(ctx);
}  // closeSocketAndContext

}  // namespace

namespace optilab {
namespace  optnet {

NetworkCommunicator::NetworkCommunicator(BaseSocketConnector::UPtr ptpSend,
                                         BaseSocketConnector::UPtr ptpRecv,
                                         BaseSocketConnector::UPtr bcastSend,
                                         BaseSocketConnector::UPtr bcastRecv,
                                         std::unique_ptr<zmq::context_t> ctx,
                                         bool isRoot,
                                         bool synchNetworkOnInitialization)
: pRank(networkconst::INVALID_NETWORK_NODE_ID),
  pRootCommunicator(isRoot),
  pSynchOnInit(synchNetworkOnInitialization),
  pExceptionInAsynchThread(false),
  pRunning(false),
  pInitThreads(false),
  pTurnDown(false),
  pTurnDownTimeout(boost::chrono::milliseconds(kDefaultTurnDownTimeOutMsec)),
  pCtx(std::move(ctx))
{
  if (!pCtx)
  {
    closeSocketAndContext(ptpSend.get(), ptpRecv.get(), bcastSend.get(), bcastRecv.get(), nullptr);
    throw std::invalid_argument("NetworkCommunicator - empty context");
  }

  {
    if (!ptpSend)
    {
      closeSocketAndContext(ptpSend.get(), ptpRecv.get(), bcastSend.get(), bcastRecv.get(),
                            pCtx.get());
      throw std::invalid_argument("NetworkCommunicator - empty point to point sender connector");
    }

    // Store the raw pointer to clear memory if cast fails
    auto* rawPtr = ptpSend.get();

    // dynamic_cast does not throw a bad_cast on casting pointer, but it returns nullptr
    auto connCast = dynamic_cast<SocketConnector*>(ptpSend.release());

    // Check if cast has failed
    if (!connCast)
    {
      rawPtr->closeSocket();
      delete rawPtr;

      closeSocketAndContext(nullptr, ptpRecv.get(), bcastSend.get(), bcastRecv.get(),
                            pCtx.get());
      throw std::runtime_error("NetworkCommunicator: "
          "cannot cast BaseSocketConnector to SocketConnector");
    }
    pPToPSendConnector.reset(connCast);
  }

  {
    if (!ptpRecv)
    {
      closeSocketAndContext(pPToPSendConnector.get(), ptpRecv.get(), bcastSend.get(),
                            bcastRecv.get(), pCtx.get());
      throw std::invalid_argument("NetworkCommunicator - empty point to point receiver connector");
    }

    // Store the raw pointer to clear memory if cast fails
    auto* rawPtr = ptpRecv.get();

    // dynamic_cast does not throw a bad_cast on casting pointer, but it returns nullptr
    auto connCast = dynamic_cast<SocketConnector*>(ptpRecv.release());

    // Check if cast has failed
    if (!connCast)
    {
      rawPtr->closeSocket();
      delete rawPtr;

      closeSocketAndContext(pPToPSendConnector.get(), nullptr, bcastSend.get(), bcastRecv.get(),
                            pCtx.get());
      throw std::runtime_error("NetworkCommunicator: "
          "cannot cast BaseSocketConnector to SocketConnector");
    }

    pPToPRecvConnector.reset(connCast);
  }

  {
    if (!bcastSend)
    {
      closeSocketAndContext(pPToPSendConnector.get(), pPToPRecvConnector.get(), bcastSend.get(),
                            bcastRecv.get(), pCtx.get());
      throw std::invalid_argument("NetworkCommunicator - empty broadcast sender connector");
    }

    // Store the raw pointer to clear memory if cast fails
    auto* rawPtr = bcastSend.get();

    // dynamic_cast does not throw a bad_cast on casting pointer, but it returns nullptr
    auto connCast = dynamic_cast<BroadcastSocketConnector*>(bcastSend.release());

    // Check if cast has failed
    if (!connCast)
    {
      rawPtr->closeSocket();
      delete rawPtr;

      closeSocketAndContext(pPToPSendConnector.get(), pPToPRecvConnector.get(), nullptr,
                            bcastRecv.get(), pCtx.get());
      throw std::runtime_error("NetworkCommunicator: "
          "cannot cast BaseSocketConnector to BroadcastSocketConnector");
    }

    pBroadcastSendConnector.reset(connCast);
  }

  {
    if (!bcastRecv)
    {
      closeSocketAndContext(pPToPSendConnector.get(), pPToPRecvConnector.get(),
                            pBroadcastSendConnector.get(), bcastRecv.get(), pCtx.get());
      throw std::invalid_argument("NetworkCommunicator - empty point to point receiver connector");
    }

    // Store the raw pointer to clear memory if cast fails
    auto* rawPtr = bcastRecv.get();

    // dynamic_cast does not throw a bad_cast on casting pointer, but it returns nullptr
    auto connCast = dynamic_cast<BroadcastSocketConnector*>(bcastRecv.release());

    // Check if cast has failed
    if (!connCast)
    {
      rawPtr->closeSocket();
      delete rawPtr;

      closeSocketAndContext(pPToPSendConnector.get(), pPToPRecvConnector.get(),
                            pBroadcastSendConnector.get(), nullptr, pCtx.get());
      throw std::runtime_error("NetworkCommunicator: "
          "cannot cast BaseSocketConnector to SocketConnector");
    }

    pBroadcastRecvConnector.reset(connCast);
  }
}

NetworkCommunicator::~NetworkCommunicator()
{
  // Close sockets and turn down the context
  turnDown();

  // Turn down parallel threads
  if (pInitThreads)
  {
    //pPtoPThread->interrupt();
    //pBcastThread->interrupt();

    // Wait for the shutdown
    boost::future_status waitCommStatus;
    boost::future_status waitBroadcastStatus;
    {
      waitCommStatus = pPtoPFuture.wait_for(pTurnDownTimeout);
      waitBroadcastStatus = pBcastFuture.wait_for(pTurnDownTimeout);
    }

    if (waitCommStatus == boost::future_status::timeout)
    {
      pPtoPThread->interrupt();
    }

    if (waitBroadcastStatus == boost::future_status::timeout)
    {
      pBcastThread->interrupt();
    }
    boost::this_thread::disable_interruption di;
    static const uint32_t WAIT_PERIOD = 2;
    bool joined = false;
    uint32_t waitTime = 0;
    while (!joined)
    {
      joined = pPtoPThread->try_join_for(boost::chrono::seconds(WAIT_PERIOD));
      joined = joined && pBcastThread->try_join_for(boost::chrono::seconds(WAIT_PERIOD));
      if (!joined)
      {
        waitTime += WAIT_PERIOD;
      }

      if (waitTime > 2 * WAIT_PERIOD)
      {
        pPtoPThread.reset();
        pBcastThread.reset();
        return;
      }
    }
  }
}  // NetworkCommunicator

void NetworkCommunicator::turnDown()
{
  pRunning = false;

  {
    // Critical section
    LockGuard lock(pTurnDownMutex);

    // Return if turn down has been performed already
    if (pTurnDown) return;

    // Interrupt threads
    if (pPtoPThread) pPtoPThread->interrupt();
    if (pBcastThread) pBcastThread->interrupt();

    // Close sockets
    if (pPToPSendConnector)
    {
      pPToPSendConnector->closeSocket();
    }

    if (pPToPRecvConnector)
    {
      pPToPRecvConnector->closeSocket();
    }

    if (pBroadcastSendConnector)
    {
      pBroadcastSendConnector->closeSocket();
    }

    if (pBroadcastRecvConnector)
    {
      pBroadcastRecvConnector->closeSocket();
    }

    // Close context and reset the pointer.
    // This will unblock any thread waiting on recv
    if (pCtx)
    {
      pCtx->close();
      pCtx.reset();
    }

    // Set turn down flag to avoid double turn-downs
    pTurnDown = true;
  }

  // Sleep to allow turn down of threads
  boost::this_thread::sleep_for(boost::chrono::milliseconds(500));
}  // turnDown

int NetworkCommunicator::init(int initTimeoutMsec)
{
  // The idea is that this node sends a registration message to the HUB.
  // The HUB replies with a registration confirmation and the unique network id for this node.
  // @note the node MUST send a registration message as a "back-end" node
  int err = networkconst::ERR_NO_ERROR;

  // Return asap if already running
  if (pRunning) return err;

  /*
   * Send a request to the hub for network registration.
   * Both IN/OUT ports need to be registered to the hub and they must be mapped to the same
   * network node identifier.
   * In order to do this, the following steps are required:
   * 1) register IN socket to the hub, and receive the network node rank,
   *    i.e., its network unique identifier.
   *    @note the HUB must know that this is a registration for an IN socket.
   * 2) register OUT socket to the hub, and receive the network node rank.
   *    The network node rank MUST be the same as the one received for the IN socket since both
   *    IN and OUT sockets belong to the same network node.
   */
  int packetId =
      pPToPSendConnector->sendHubRegistrationMessage(pRootCommunicator, initTimeoutMsec);
  if (packetId < 0)
  {
    spdlog::error("NetworkCommunicator - init: "
        "failure in registering the communicator to the hub, return");
    return networkconst::ERR_GENERIC_ERROR;
  }

  // Wait to receive the registration for the IN socket
  pRank = pPToPSendConnector->recvHubRegistrationMessage(packetId, initTimeoutMsec);
  if (pRank == networkconst::INVALID_NETWORK_NODE_ID)
  {
    spdlog::error("NetworkCommunicator - init: invalid send socket network registration, return");
    return networkconst::ERR_GENERIC_ERROR;
  }

  if (pRootCommunicator && pRank != networkconst::ROOT_NETWORK_NODE_ID)
  {
    const std::string errMsg = "NetworkCommunicator - init: "
        "received an invalid network node identifier for the root node " +
        std::to_string(pRank);
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  // Now send the request registration for the OUT socket.
  // @note communicate to the hub that this network node has already registered one of its sockets
  // by providing the previously received rank
  packetId =
      pPToPRecvConnector->sendHubRegistrationMessage(pRootCommunicator, initTimeoutMsec, pRank);

  if (packetId < 0)
  {
    spdlog::error("NetworkCommunicator - init: "
        "failure in registering the communicator to the hub, return");
    return networkconst::ERR_GENERIC_ERROR;
  }

  // Wait to receive the registration for the OUT socket
  int secondRank =
      pPToPRecvConnector->recvHubRegistrationMessage(packetId, initTimeoutMsec);
  if (secondRank == networkconst::INVALID_NETWORK_NODE_ID)
  {
    spdlog::error("NetworkCommunicator - init: invalid recv socket network registration, return");
    return networkconst::ERR_GENERIC_ERROR;
  }

  if (secondRank != pRank)
  {
    const std::string errMsg = "NetworkCommunicator - init: "
        "received an invalid network node identifier " + std::to_string(secondRank) +
        " for the root node " + std::to_string(pRank);
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  if (pSynchOnInit)
  {
    pPToPSendConnector->sendNetworkSynchronizationMessage(pRootCommunicator);
  }

  // Set this network communicator as running
  pRunning = true;

  // Start the threads reading from sockets
  initReadingThreads();

  return err;
}  //init

int NetworkCommunicator::getRank() const noexcept
{
  return pRank;
}  // getRank

int NetworkCommunicator::getSize() const noexcept
{
  {
    // Critical section on accessing the network's map
    LockGuard lock(pNetworkMapMutex);
    return static_cast<int>(pNetworkNodeMap.size());
  }
}  // getRank

void NetworkCommunicator::abort()
{
  // Create a "dummy" packet containing only the address of this communicator and
  // broadcast the abort message
  Packet::UPtr packet(new Packet());
  packet->rootSource = pRootCommunicator;

  if (pNetworkNodeMap.find(getRank()) != pNetworkNodeMap.end())
  {
    packet->networkAddr = pNetworkNodeMap[getRank()].second;
  }
  packet->networkPacket = NetworkPacket<int>::UPtr(new NetworkPacket<int>(0));
  pBroadcastSendConnector->sendAbortMessage(std::move(packet));

  // Sleep to allow abort message to be sent
  boost::this_thread::sleep_for(boost::chrono::milliseconds(500));
}  // abort

void NetworkCommunicator::initReadingThreads()
{
  // Create the task for the run loop
  boost::packaged_task<void> taskPToP(
      boost::bind(&NetworkCommunicator::readFromPToPConnector, this));
  boost::packaged_task<void> taskBcast(
      boost::bind(&NetworkCommunicator::readFromBcastConnector, this));

  // Get the future/shared state from the packaged task
  pPtoPFuture = taskPToP.get_future();
  pBcastFuture = taskBcast.get_future();

  // Run the thread on the task,
  // i.e., on the "run(...) method
  pPtoPThread.reset(new boost::thread(boost::move(taskPToP)));
  pBcastThread.reset(new boost::thread(boost::move(taskBcast)));

  // Set thread initialization flag
  pInitThreads = true;

  // Check if the future throws
  checkForFutureException();
}  // initReadingThreads

void NetworkCommunicator::checkForFutureException() const
{
  if (pPtoPFuture.has_exception())
  {
    // Set exception flag
    pExceptionInAsynchThread = true;

    // Re-throw
    pPtoPFuture.get();
  }

  if (pBcastFuture.has_exception())
  {
    // Set exception flag
    pExceptionInAsynchThread = true;

    // Re-throw
    pBcastFuture.get();
  }
}  // checkForFutureException

void NetworkCommunicator::readFromPToPConnector()
{
  spdlog::info("NetworkCommunicator - readFromPToPConnector: start PToP reading");
  while (pRunning)
  {
    // Set an interruption point a the beginning of every new loop iteration
    boost::this_thread::interruption_point();

    // Blocking call to read from the socket
    Packet::UPtr packet;
    try
    {
      packet = pPToPRecvConnector->receiveMessage();
    }
    catch(zmq::error_t& e)
    {
      if (e.num() == ETERM)
      {
        spdlog::warn("NetworkCommunicator - readFromPToPConnector: closing socket, return");
        return;
      }
      throw;
    }
    catch (...)
    {
      spdlog::error("NetworkCommunicator - readFromPToPConnector: error reading from socket");
      throw;
    }

    // Once there is a packet, store it in the queue
    assert(packet);
    {
      // Critical section (lock on the queue of packets)
      UniqueLock lock(pPacketQueueMutex);
      pPacketQueue.push_back(std::move(packet));

      // Notify any thread waiting on packets in the queue
      pPacketQueueConditionVar.notify_one();
    }
  }  // while
}  // readFromPToPConnector

void NetworkCommunicator::readFromBcastConnector()
{
  spdlog::info("NetworkCommunicator - readFromBcastConnector: start broadcast reading");
  while (pRunning)
  {
    // Set an interruption point a the beginning of every new loop iteration
    boost::this_thread::interruption_point();

    // Blocking call to read from the socket
    Packet::UPtr packet;
    try
    {
      packet = pBroadcastRecvConnector->receiveMessage();
    }
    catch(zmq::error_t& e)
    {
      if (e.num() == ETERM)
      {
        spdlog::warn("NetworkCommunicator - readFromBcastConnector: "
            "closing socket, return");
        return;
      }
      throw;
    }
    catch (...)
    {
      spdlog::error("NetworkCommunicator - readFromBcastConnector: "
          "error reading from socket");
      throw;
    }

    // Once there is a packet, store it in the queue
    assert(packet && packet->isBroadcastPacket());

    // Convert to broadcast packet
    auto bcastPacket = static_cast<BroadcastPacket*>(packet.get());
    if (bcastPacket->isNetworkSynchMessage())
    {
      {
        // Critical section
        // Update the network node map
          LockGuard lock(pNetworkMapMutex);
          pNetworkNodeMap = bcastPacket->networkNodeMap;

          // Set the network address of this node
          if (pNetworkNodeMap.find(pRank) == pNetworkNodeMap.end())
          {
            const std::string errMsg = "NetworkCommunicator - readFromPToPConnector: "
                "received a network synchronization message. Network rank not found in the "
                "network map for rank " + std::to_string(pRank);
            spdlog::error(errMsg);
            throw std::runtime_error(errMsg);
          }

          // Update the address of this communicator using the
          // network node receiving address
          pNetworkBroadcastAddr = pNetworkNodeMap[pRank].first;
      }  // critical section
    }
    else if (bcastPacket->isNetworkAbortMessage())
    {
      // Abort the computation and return
      turnDown();
      return;
    }
    else
    {
      // Skip auto-broadcasted message.
      // @note for network synchronization and aborts, the packet is never skipped
      if (pNetworkBroadcastAddr == packet->networkAddr)
      {
        continue;
      }

      {
        // Critical section (lock on the queue of broadcasted packets) and push it back
        // into the queue
        UniqueLock lock(pBcastPacketQueueMutex);
        pBcastPacketQueue.push_back(std::move(packet));

        // Notify any thread waiting on packets in the queue
        pBcastPacketQueueConditionVar.notify_one();
      }  // critical section
    }
  }  // while
}  // readFromBcastConnector

int NetworkCommunicator::broadcast(Packet::UPtr& packet, const int root) noexcept
{
  int err = networkconst::ERR_NO_ERROR;
  if (!packet && (getRank() == root))
  {
    spdlog::error("NetworkCommunicator - broadcast: empty network packet to broadcast, return");
    return networkconst::ERR_GENERIC_ERROR;
  }

  if (getRank() == root)
  {
    // The root node broadcasting the message
    try
    {
      {
        // Critical section
        LockGuard lock(pNetworkMapMutex);

        // Set the address of the sender
        auto it = pNetworkNodeMap.find(getRank());
        if (it == pNetworkNodeMap.end())
        {
          const std::string errMsg = "NetworkCommunicator - broadcast: "
              "Network rank not found in the network map for rank " + std::to_string(root);
          spdlog::error(errMsg);
          return networkconst::ERR_GENERIC_ERROR;
        }

        // Set the sender address,
        // i.e., the node network address identifier
        packet->networkAddr = it->second.first;
      }  // critical section

      // Broadcast the message
      pBroadcastSendConnector->sendMessage(std::move(packet));
    }
    catch (...)
    {
      spdlog::error("NetworkCommunicator - broadcast: error while broadcasting a packet, return");
      err = networkconst::ERR_GENERIC_ERROR;
    }
  }
  else
  {
    // Read from broadcast
    if (packet != nullptr)
    {
      spdlog::warn("NetworkCommunicator - broadcast: overwriting a non-empty packet");
    }
    packet = receiveBcast();
  }

  return err;
}  // broadcast

int NetworkCommunicator::send(Packet::UPtr packet, const int dest) noexcept
{
  int err = networkconst::ERR_NO_ERROR;
  if (!packet)
  {
    spdlog::error("NetworkCommunicator - send: empty network packet, return");
    return networkconst::ERR_GENERIC_ERROR;
  }

  try
  {
    {
      // Critical section
      LockGuard lock(pNetworkMapMutex);

      // Set the address of the sender
      auto it = pNetworkNodeMap.find(dest);
      if (it == pNetworkNodeMap.end())
      {
        const std::string errMsg = "NetworkCommunicator - send: "
            "Network rank not found in the network map for rank " + std::to_string(dest);
        spdlog::error(errMsg);
        return networkconst::ERR_GENERIC_ERROR;
      }

      // @note use the network node receiving address
      packet->networkAddr = (it->second).second;
    }

    // Send the point to point message
    pPToPSendConnector->sendMessage(std::move(packet));
  }
  catch (...)
  {
    spdlog::error("NetworkCommunicator - send: error while sending a packet, return");
    err = networkconst::ERR_GENERIC_ERROR;
  }

  return err;
}  // send

int NetworkCommunicator::receive(Packet::UPtr& packet, const int source) noexcept
{
  int err = networkconst::ERR_NO_ERROR;

  // Loop until there is a meaningful packet
  Packet::UPtr tempPacket;
  while (true)
  {
    {
      // Critical section
      UniqueLock lock(pPacketQueueMutex);

      // Wait until there are some packets the queue
      pPacketQueueConditionVar.wait(lock, boost::bind(&PacketQueue::size, &pPacketQueue));

      // If this thread got unblocked and the communicator is not running anymore,
      // return asap
      if (!pRunning)
      {
        return err;
      }

      tempPacket = std::move(pPacketQueue.front());
      pPacketQueue.pop_front();

      // Exit critical section, unblocking the queue.
      // The reading threads will be able to add more packets to the packet queue
    }

    auto recvAddr = tempPacket->networkAddr;
    for (const auto& it : pNetworkNodeMap)
    {
      if (it.second.first == recvAddr)
      {
        break;
      }
    }

    // Check if the source need to match the sender of the packet
    if (source != networkconst::ANY_NETWORK_NODE_ID)
    {
      std::string addr;
      {
        // Critical section
        LockGuard lock(pNetworkMapMutex);

        // Set the address of the sender
        auto it = pNetworkNodeMap.find(source);
        if (it == pNetworkNodeMap.end())
        {
          const std::string errMsg = "NetworkCommunicator - receive: "
              "Network rank not found in the network map for rank " + std::to_string(source);
          spdlog::error(errMsg);
          return networkconst::ERR_GENERIC_ERROR;
        }

        // @note use the network node identifier address to match
        // to the sender address
        addr = (it->second).first;
      }

      // @note the received packet contains the network address of the sender,
      // i.e., its network node identifier address
      if (addr != tempPacket->networkAddr)
      {
        // The source is not matching, loop over to the next message
        spdlog::warn("NetworkCommunicator - receive: source not matching expected " +
                     utils::bytesToString(addr) + " (rank " + std::to_string(source) +
                     ") received " + utils::bytesToString(tempPacket->networkAddr) +
                     std::string(", discard packet"));
        continue;
      }
    }

    if (!packet)
    {
      packet = std::move(tempPacket);
      return err;
    }

    // Here, either the receive is from any source or the source is matching.
    // Check if there is a tag to match
    if (packet->packetTag)
    {
      // If there is no tag or the tags are different, continue
      if (!tempPacket->packetTag ||
          packet->packetTag->getTag() != tempPacket->packetTag->getTag())
      {
        spdlog::warn("NetworkCommunicator - receive: tag not matching expected " +
                     std::to_string(packet->packetTag->getTag()) + " received " +
                     std::to_string(tempPacket->packetTag->getTag()) +
                     std::string(", discard packet"));
        continue;
      }
    }

    // Both source and tag are matching, copy the packet and return
    packet = std::move(tempPacket);
    return err;
  }  // while
}  // receive

Packet::UPtr NetworkCommunicator::receiveBcast() noexcept
{
  // Loop until there is a meaningful packet
  Packet::UPtr bcastPacket;
  while (true)
  {
    {
      // Critical section
      UniqueLock lock(pBcastPacketQueueMutex);

      // Wait until there are some packets the queue
      pBcastPacketQueueConditionVar.wait(lock, boost::bind(&PacketQueue::size, &pBcastPacketQueue));

      // If this thread got unblocked and the communicator is not running anymore,
      // return asap
      if (!pRunning) { return Packet::UPtr(); }

      bcastPacket = std::move(pBcastPacketQueue.front());
      pBcastPacketQueue.pop_front();

      // Exit critical section, unblocking the queue.
      // The reading threads will be able to add more packets to the packet queue
    }

    // Return the packet
    return std::move(bcastPacket);
  }  // while
}  // receiveBcast

int NetworkCommunicator::ireceive(Packet::UPtr& packet, const int source, bool* err) noexcept
{
  const int resPacketSet = 1;
  const int resPacketNotSet = 0;

  // Loop until there is a meaningful packet
  Packet::UPtr tempPacket;
  {
    // Critical section
    UniqueLock lock(pPacketQueueMutex);

    // Check if there is any packet in the queue
    if (pPacketQueue.empty())
    {
      // If not, don't wait, return asap
      if (err)
      {
        // There was no error
        *err = false;
      }
      return resPacketNotSet;
    }

    // If this thread got unblocked and the communicator is not running anymore,
    // return asap
    if (!pRunning)
    {
      if (err)
      {
        // There was no error
        *err = false;
      }

      return resPacketNotSet;
    }

    tempPacket = std::move(pPacketQueue.front());
    pPacketQueue.pop_front();

    // Exit critical section, unblocking the queue.
    // The reading threads will be able to add more packets to the packet queue
  }

  // Check if the source need to match the sender of the packet
  if (source != networkconst::ANY_NETWORK_NODE_ID)
  {
    std::string addr;
    {
      // Critical section
      LockGuard lock(pNetworkMapMutex);

      // Set the address of the sender
      auto it = pNetworkNodeMap.find(source);
      if (it == pNetworkNodeMap.end())
      {
        const std::string errMsg = "NetworkCommunicator - ireceive: "
                "Network rank not found in the network map for rank " + std::to_string(source);
        spdlog::error(errMsg);
        if (err)
        {
          // There was an error
          *err = true;
        }

        return resPacketNotSet;
      }

      // @note use the network node identifier address to match
      // to the sender address
      addr = (it->second).first;
    }

    // @note the received packet contains the network address of the sender,
    // i.e., its network node identifier address
    if (addr != tempPacket->networkAddr)
    {
      // The source is not matching, loop over to the next message
      spdlog::warn("NetworkCommunicator - ireceive: source not matching expected " +
                   utils::bytesToString(addr) + " (rank " + std::to_string(source) +
                   ") received " + utils::bytesToString(tempPacket->networkAddr) +
                   std::string(", discard packet"));
      if (err)
      {
        // There was no error
        *err = false;
      }
      return resPacketNotSet;
    }
  }  // not networkconst::ANY_NETWORK_NODE_ID

  if (!packet)
  {
    packet = std::move(tempPacket);
    if (err)
    {
      // There was no error
      *err = false;
    }
    return resPacketSet;
  }

  // Here, either the receive is from any source or the source is matching.
  // Check if there is a tag to match
  if (packet->packetTag)
  {
    // If there is no tag or the tags are different, continue
    if (!tempPacket->packetTag ||
            packet->packetTag->getTag() != tempPacket->packetTag->getTag())
    {
      spdlog::warn("NetworkCommunicator - ireceive: tag not matching expected " +
                   std::to_string(packet->packetTag->getTag()) + " received " +
                   std::to_string(tempPacket->packetTag->getTag()) +
                   std::string(", discard packet"));
      if (err)
      {
        // There was no error
        *err = false;
      }
      return resPacketNotSet;
    }
  }

  // Both source and tag are matching, copy the packet and return
  packet = std::move(tempPacket);
  if (err)
  {
    // There was no error
    *err = false;
  }
  return resPacketSet;
}  // ireceive

int NetworkCommunicator::probe(Packet::UPtr& packet) noexcept
{
  return receive(packet, networkconst::ANY_NETWORK_NODE_ID);
}  // probe

int NetworkCommunicator::iprobe(Packet::UPtr& packet, bool* err) noexcept
{
  return ireceive(packet, networkconst::ANY_NETWORK_NODE_ID, err);
}  // iprobe

int NetworkCommunicator::waitSpecTagFromSpecSource(Packet::UPtr& packet, const int source,
                                                   bool replaceTag) noexcept
{
  if (!packet)
  {
    return networkconst::ERR_GENERIC_ERROR;
  }

  // Store the packet tag and replace it with "any tag"
  auto oldTag = std::move(packet->packetTag);
  packet->packetTag = nullptr;

  // Wait to receive any message from the specified source
  auto err = receive(packet, source);
  if (err)
  {
    return err;
  }

  // Get the new tag
  auto newTag = (packet->packetTag).get();

  if (!oldTag && !newTag)
  {
    return networkconst::ERR_NO_ERROR;
  }

  if ((!oldTag && newTag) || (oldTag && !newTag))
  {
    if (oldTag && !replaceTag)
    {
      packet->packetTag = std::move(oldTag);
    }
    return networkconst::ERR_GENERIC_ERROR;
  }

  // Both tags available
  const auto resTag = (oldTag->getTag() == newTag->getTag()) ? 0 : 1;
  if (!replaceTag)
  {
    packet->packetTag = std::move(oldTag);
  }

  return resTag;
}  // waitSpecTagFromSpecSource

int NetworkCommunicator::convertNetworkAddrToRank(const std::string& netAddr) const noexcept
{
  for (const auto& it : pNetworkNodeMap)
  {
    if (netAddr == it.second.first || netAddr == it.second.second)
    {
      return it.first;
    }
  }
  return networkconst::INVALID_NETWORK_NODE_ID;
}  // convertNetworkAddrToRank

}  // namespace optnet
}  // namespace optilab
