//
// Copyright OptiLab 2019. All rights reserved.
//
// Utilities for network communication.
//

#pragma once

#include <algorithm>  // for std::equal
#include <initializer_list>
#include <memory>     // for std::shared_ptr
#include <vector>

#include <boost/any.hpp>
#include <boost/logic/tribool.hpp>
#include <zeromq/zhelpers.hpp>

#include "optimizer_network/network_constants.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace  optnet {

/// Communication Tag used for sending and receiving specific messages
/// within the communication framework
class CommTag {
public:
  using SPtr = std::shared_ptr<CommTag>;
};

namespace utils {
/// Convert the give string of bytes into numbers separated by char and returns the
/// correspondent string
SYS_EXPORT_FCN std::string bytesToString(const std::string& in, char separator = '.');

/// Builds and returns a socket for point to point communication.
/// @note communications run on a hub
SYS_EXPORT_FCN std::shared_ptr<zmq::socket_t> buildPointToPointSocket(zmq::context_t& ctx);

/// Builds and returns a socket for sending broadcast messages.
/// @note communications run on a hub
SYS_EXPORT_FCN std::shared_ptr<zmq::socket_t> buildSenderBroadcastSocket(zmq::context_t& ctx);

/// Builds and returns a socket for receiving broadcast messages.
/// @note communications run on a hub.
/// @note remember to SUBSCRIBE the socket after binding it to an address.
/// For example:
/// socket->setsockopt(ZMQ_SUBSCRIBE, "", 0);
SYS_EXPORT_FCN std::shared_ptr<zmq::socket_t> buildReceivingBroadcastSocket(zmq::context_t& ctx);
}  // namesoace utils
}  // namespace optnet
}  // namespace optilab
