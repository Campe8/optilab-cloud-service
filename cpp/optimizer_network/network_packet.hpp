//
// Copyright OptiLab 2019. All rights reserved.
//
// Network packet transmitted on the optimizer network.
//

#pragma once

#include <algorithm>  // for std::equal
#include <cstdint>    // for int32
#include <memory>     // for std::shared_ptr
#include <stdexcept>  // for std::runtime_error
#include <string>
#include <typeinfo>   // for typeid
#include <utility>    // for std::move
#include <vector>

#include <boost/any.hpp>
#include <boost/logic/tribool.hpp>

#include "optimizer_network/network_constants.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace  optnet {

/**
 * Base struct for any payload sent along the network of workers
 * @note Payload sub-structs need to implement at least the equality/disequality operators
 */
struct SYS_EXPORT_STRUCT BaseNetworkPacket {
  enum class BaseNetworkPacketType {
    /// Base network packet.
    /// It can have sub-types as double, float, int32, etc.
    /// For a complete description of types, see "NetworkProtoPacket"
    /// under network_packet.proto
    BNPT_NETWORK_PACKET = 0,

    /// Packet for MP linear models
    BNPT_MP_LINEAR_MODEL,

    /// Packet for MP linear model solutions
    BNPT_MP_LINEAR_MODEL_SOLUTION,

    /// Packet for meta_bb framework.
    /// This packet type can be, for example,
    /// a NodeProtoPacket, CalculationStateProtoPacket, etc.
    BNPT_META_BB_PACKET,

    /// Packet for meta_bb framework paramset specific.
    /// This message holds SolverProtoParamSet messages
    BNPT_META_BB_SOLVER_PARAMSET,

    /// Packet for meta_bb framework paramset specific.
    /// This message holds ProtoParamSetDescriptor messages
    BNPT_META_BB_DESCRIPTOR_PARAMSET,

    /// Packet for meta_bb initiator message.
    /// This is the message sent upon initialization to all solvers
    BNPT_META_BB_INITIATOR,

    /// Unspecified network packet
    BNPT_UNSPECIFIED_PACKET  // Must be last
  };

  using UPtr = std::unique_ptr<BaseNetworkPacket>;
  using SPtr = std::shared_ptr<BaseNetworkPacket>;

  virtual ~BaseNetworkPacket() = default;

  virtual bool operator==(const BaseNetworkPacket& other) const noexcept = 0;
  virtual bool operator!=(const BaseNetworkPacket& other) const noexcept = 0;

  /// Type of this base packet
  BaseNetworkPacketType packetType;

  /// Serialization method.
  /// Serialize this Network packet as a string.
  /// Returns a pair:
  /// - first: an integer describing the type of packet (used for de-serialization);
  /// - second: an stl string representing the serialized packet.
  /// @note the string is used as a convenient data structure holding
  /// bytes of data. It doesn't need to have any semantic or syntactic meaning
  virtual std::pair<BaseNetworkPacketType, std::string> serialize() const noexcept = 0;
};

/**
 * Template payload to allow the optimizer network to exchange any type of data
 */
template<class Type>
struct SYS_EXPORT_STRUCT NetworkPacket : public BaseNetworkPacket {

  using UPtr = std::unique_ptr<NetworkPacket<Type>>;
  using SPtr = std::shared_ptr<NetworkPacket<Type>>;

  NetworkPacket() = default;

  /// Constructor:
  /// - payloadData: pointer to the data.
  /// @note the size of the data is assumed to be 1
  explicit NetworkPacket(const Type* payloadData)
  : NetworkPacket(payloadData, 1) {}

  /// Constructor:
  /// - payloadData: pointer to the data
  /// - payloadSize: number of elements of type "Type" pointed by "payloadSize".
  /// @note this constructor performs a in internal copy of the input data
  NetworkPacket(const Type* payloadData, int payloadSize)
  : data(payloadData, payloadData + payloadSize) {}

  /// Move constructor
  NetworkPacket(NetworkPacket<Type>&& other) noexcept = default;

  /// Copy constructor.
  /// @note the copy constructor is deleted.
  /// This is done to avoid expensive copies, use the move constructor instead
  NetworkPacket(const NetworkPacket<Type>& other) = delete;

  /// Constructor for scalar data
  explicit NetworkPacket(const Type& payloadData) : data{{payloadData}} {}

  /// Move constructor for scalar data
  explicit NetworkPacket(Type&& payloadData) : data{{std::move(payloadData)}} {}

  /// Copy constructor for vectors of data
  explicit NetworkPacket(const std::vector<Type>& payloadData) : data{payloadData} {}

  /// Move constructor for vectors of data
  explicit NetworkPacket(std::vector<Type>&& payloadData) : data{std::move(payloadData)} {}

  ~NetworkPacket() override = default;

  /// Assignment operator.
  /// @note the assignment is deleted.
  /// This is done to avoid expensive copies, use the move assignment operator instead
  NetworkPacket<Type>& operator=(const NetworkPacket<Type>& other) = delete;

  /// Move assignment operator
  NetworkPacket<Type>& operator=(NetworkPacket<Type>&& other) = default;

  inline bool operator==(const NetworkPacket<Type>& other) const noexcept
  {
    if (this->size() != other.size())
    {
      return false;
    }

    return this->data == other.data;
  }

  inline bool operator==(const BaseNetworkPacket& other) const noexcept override
  {
    try
    {
      return operator==(dynamic_cast<const NetworkPacket<Type>&>(other));
    }
    catch (...)
    {
      return false;
    }
  }

  inline bool operator!=(const NetworkPacket<Type>& other) const noexcept
  {
    return !(operator==(other));
  }

  inline bool operator!=(const BaseNetworkPacket& other) const noexcept override
  {
    return !(operator==(other));
  }

  Type& operator[](typename std::vector<Type>::size_type idx) { return data[idx]; }
  const Type& operator[](typename std::vector<Type>::size_type idx) const { return data[idx]; }

  /// Default overriding.
  /// Template method specializations should define this method properly
  std::pair<BaseNetworkPacketType, std::string> serialize() const noexcept override
  {
    return {BaseNetworkPacketType::BNPT_UNSPECIFIED_PACKET, ""};
  }

  /// Returns the size of the data,
  /// i.e., the number of element hold by this network Payload
  inline typename std::vector<Type>::size_type size() const noexcept { return data.size(); }

  /// Data structure holding the actual data
  std::vector<Type> data;
};

template<> std::pair<BaseNetworkPacket::BaseNetworkPacketType, std::string>
NetworkPacket<double>::serialize() const noexcept;
template<> std::pair<BaseNetworkPacket::BaseNetworkPacketType, std::string>
NetworkPacket<float>::serialize() const noexcept;
template<> std::pair<BaseNetworkPacket::BaseNetworkPacketType, std::string>
NetworkPacket<int32_t>::serialize() const noexcept;
template<> std::pair<BaseNetworkPacket::BaseNetworkPacketType, std::string>
NetworkPacket<int64_t>::serialize() const noexcept;
template<> std::pair<BaseNetworkPacket::BaseNetworkPacketType, std::string>
NetworkPacket<uint32_t>::serialize() const noexcept;
template<> std::pair<BaseNetworkPacket::BaseNetworkPacketType, std::string>
NetworkPacket<uint64_t>::serialize() const noexcept;
template<> std::pair<BaseNetworkPacket::BaseNetworkPacketType, std::string>
NetworkPacket<bool>::serialize() const noexcept;
template<> std::pair<BaseNetworkPacket::BaseNetworkPacketType, std::string>
NetworkPacket<std::string>::serialize() const noexcept;

/// Converts from network packet type to int
SYS_EXPORT_FCN int networkPacketTypeToInt(BaseNetworkPacket::BaseNetworkPacketType type);

/// Converts from int to network packet tyep
SYS_EXPORT_FCN BaseNetworkPacket::BaseNetworkPacketType intToNetworkPacketType(int type);

/// Utility function:
/// de-serializes the given string of bytes and returns the (unique) pointer to the
/// base network packet encoded by the de-serialized string of bytes.
/// @note this should work in tandem with the "serialize(...)" method of the network packet
SYS_EXPORT_FCN BaseNetworkPacket::UPtr deserializeNetworkPacket(const std::string& bytes);

/// Utility function:
/// check if the given packet has the given template type.
/// Returns true on success, false otherwise
template <class Type>
bool canCastBaseToNetworkPacket(const BaseNetworkPacket::UPtr& packet)
{
  // If nullptr, return asap
  if (!packet)
  {
    return false;
  }

  // dynamic_cast does not throw a bad_cast on casting pointer, but it returns nullptr
  auto networkPacketPtr = dynamic_cast<NetworkPacket<Type>*>(packet.get());
  return (networkPacketPtr != nullptr);
}

/// Utility function:
/// Casts the give base network packet to the proper instance of network packet
template <class Type>
typename NetworkPacket<Type>::UPtr castBaseToNetworkPacket(BaseNetworkPacket::UPtr packet)
{
  // If nullptr, return asap
  if (!packet)
  {
    return nullptr;
  }

  // Store the raw pointer to clear memory if cast fails
  auto* rawPtr = packet.get();

  // dynamic_cast does not throw a bad_cast on casting pointer, but it returns nullptr
  auto networkPacketPtr = dynamic_cast<NetworkPacket<Type>*>(packet.release());

  // Check if cast has failed
  if (!networkPacketPtr)
  {
    delete rawPtr;
    throw std::runtime_error("castBaseToNetworkPacket: cannot cast BaseNetworkPacket to "
        "NetworkPacket<" + std::string(typeid(Type).name()) + ">");
  }

  // Returns the casted network packet
  typename NetworkPacket<Type>::UPtr networkPacket;
  networkPacket.reset(networkPacketPtr);
  return networkPacket;
}

}  // namespace optnet
}  // namespace optilab
