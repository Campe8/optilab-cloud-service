//
// Copyright OptiLab 2019. All rights reserved.
//
// Base class for the central network hub.
// The hub is the center of the network (in a star configuration) and acts
// as a broker.
//

#pragma once

#include <memory>  // for std::shared_ptr
#include <string>

#include "system/system_export_defs.hpp"

namespace optilab {
namespace  optnet {

class SYS_EXPORT_CLASS NetworkBaseHub {
 public:
  using SPtr = std::shared_ptr<NetworkBaseHub>;

 public:
  /// Constructor:
  /// - inPort: input port for point-to-point communication
  /// - outPort: output port for point-to-point communication
  /// - broadcastInPort: input port for broadcasting messages
  /// - broadcastOutPort: output port for broadcasting messages
  /// - inAddr: network input address (default "localhost")
  /// - outAddr: network output address (default "localhost")
  /// - inAddr: network input broadcasting address (default "localhost")
  /// - inAddr: network outout broadcasting address (default "localhost")
  NetworkBaseHub(const int inPort,
                 const int outPort,
                 const int broadcastInPort,
                 const int broadcastOutPort,
                 const std::string& inAddr = std::string(),
                 const std::string& outAddr = std::string(),
                 const std::string& broadcastInAddr = std::string(),
                 const std::string& broadcastOutAddr = std::string());

  virtual ~NetworkBaseHub() = default;

  /// Runs this hub.
  /// It creates context and connections and starts listening on
  /// the input/output/broadcast ports
  virtual void run() = 0;

  /// Synchronizes the network.
  /// This method is used to keep in sync. all network nodes connected to this hub
  virtual void synchNetwork() = 0;

 protected:
  std::string getNetworkInSocketAddr();
  std::string getNetworkOutSocketAddr();
  std::string getNetworkInBroadcastSocketAddr();
  std::string getNetworkOutBroadcastSocketAddr();

 private:
  /// Ports
  int pInPort;
  int pOutPort;
  int pInBroadcastPort;
  int pOutBroadcastPort;

  /// Addresses
  std::string pInAddr;
  std::string pOutAddr;
  std::string pInBroadcastAddr;
  std::string pOutBroadcastAddr;
};

}  // namespace optnet
}  // namespace optilab
