#include "optimizer_network/network_base_hub.hpp"

#include <stdexcept>  // for std::invalid_argument

#include "optimizer_network/network_constants.hpp"

namespace {

const char TCP_NETWORK_PREFIX[] = "tcp://";

std::string getSocketAddr(const int socketPort, const std::string& socketAddr)
{
  std::string addr(TCP_NETWORK_PREFIX);
  if (!socketAddr.empty())
  {
    addr += socketAddr + ":";
  }
  else
  {
    addr += "*:";
  }
  addr += std::to_string(socketPort);

  return addr;
}  // getSocketAddr

}  // namespace

namespace optilab {
namespace  optnet {

NetworkBaseHub::NetworkBaseHub(const int inPort, const int outPort, const int broadcastInPort,
                               const int broadcastOutPort, const std::string& inAddr,
                               const std::string& outAddr, const std::string& broadcastInAddr,
                               const std::string& broadcastOutAddr)
: pInPort(inPort),
  pOutPort(outPort),
  pInBroadcastPort(broadcastInPort),
  pOutBroadcastPort(broadcastOutPort),
  pInAddr(inAddr),
  pOutAddr(outAddr),
  pInBroadcastAddr(broadcastInAddr),
  pOutBroadcastAddr(broadcastOutAddr)
{
  if (pInPort < networkconst::MIN_NETWORK_PORT_NUMBER ||
      pInPort > networkconst::MAX_NETWORK_PORT_NUMBER)
  {
    throw std::invalid_argument("NetworkBaseHub - invalid input port number: " +
                                std::to_string(pInPort));
  }

  if (pOutPort < networkconst::MIN_NETWORK_PORT_NUMBER ||
      pOutPort > networkconst::MAX_NETWORK_PORT_NUMBER)
  {
    throw std::invalid_argument("NetworkBaseHub - invalid output port number: " +
                                std::to_string(pOutPort));
  }

  if (pInBroadcastPort < networkconst::MIN_NETWORK_PORT_NUMBER ||
      pInBroadcastPort > networkconst::MAX_NETWORK_PORT_NUMBER)
  {
    throw std::invalid_argument("NetworkBaseHub - invalid input broadcast port number: " +
                                std::to_string(pInBroadcastPort));
  }

  if (pOutBroadcastPort < networkconst::MIN_NETWORK_PORT_NUMBER ||
      pOutBroadcastPort > networkconst::MAX_NETWORK_PORT_NUMBER)
  {
    throw std::invalid_argument("NetworkBaseHub - invalid output broadcast port number: " +
                                std::to_string(pOutBroadcastPort));
  }
}

std::string NetworkBaseHub::getNetworkInSocketAddr()
{
  return getSocketAddr(pInPort, pInAddr);
}  // getNetworkInSocketAddr

std::string NetworkBaseHub::getNetworkOutSocketAddr()
{
  return getSocketAddr(pOutPort, pOutAddr);
}  // getNetworkOutSocketAddr

std::string NetworkBaseHub::getNetworkInBroadcastSocketAddr()
{
  return getSocketAddr(pInBroadcastPort, pInBroadcastAddr);
}  // getNetworkInBroadcastSocketAddr

std::string NetworkBaseHub::getNetworkOutBroadcastSocketAddr()
{
  return getSocketAddr(pOutBroadcastPort, pOutBroadcastAddr);
}  // getNetworkOutBroadcastSocketAddr

}  // namespace optnet
}  // namespace optilab

