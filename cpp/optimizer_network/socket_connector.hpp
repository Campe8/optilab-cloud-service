//
// Copyright OptiLab 2019. All rights reserved.
//
// Class wrapping a connection socket.
//

#pragma once

#include <memory>   // for std::shared_ptr
#include <string>
#include <utility>  // for std::move

#include <boost/thread.hpp>
#include <sparsepp/spp.h>
#include <zeromq/zhelpers.hpp>

#include "optimizer_network/network_packet.hpp"
#include "optimizer_network/packet.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace  optnet {

/**
 * Struct functor used to deserialize non-POD/std::string network packets
 * by using a user-defined deserialization method.
 */
struct SYS_EXPORT_STRUCT PacketDeserializer {
  using UPtr = std::unique_ptr<PacketDeserializer>;
  using SPtr = std::shared_ptr<PacketDeserializer>;

  virtual ~PacketDeserializer() = default;

  /// Deserializes the given string of bytes into the proper
  /// base network packet
  virtual BaseNetworkPacket::UPtr deserializePacket(int packetType, const std::string& bytes) = 0;
};

/**
 * Implementation of the packet deserializer for packets read from
 * socket connectors.
 * This deserializer performs deserialization on the following packet types:
 * - BNPT_NETWORK_PACKET
 * - BNPT_MP_LINEAR_MODEL
 * - BNPT_MP_LINEAR_MODEL_SOLUTION
 * - BNPT_META_BB_INITIATOR
 * The following packet are not deserialized into their objects.
 * Instead, they are deserialized into a BaseNetworkPacket<std::string>
 * containing the original serialized bytes and the packet type information:
 * - BNPT_META_BB_PACKET
 * - BNPT_META_BB_SOLVER_PARAMSET
 * - BNPT_META_BB_DESCRIPTOR_PARAMSET
 * - BNPT_UNSPECIFIED_PACKET
 * - All other types not listed here
 */
struct SYS_EXPORT_STRUCT SocketPacketDeserializer : public PacketDeserializer
{
  ~SocketPacketDeserializer() override = default;
  BaseNetworkPacket::UPtr deserializePacket(int packetType, const std::string& bytes) override;
};

/**
 * Base class encapsulating writers/readers from a given socket.
 */
class SYS_EXPORT_CLASS BaseSocketConnector {
 public:
  using UPtr = std::unique_ptr<BaseSocketConnector>;
  using SPtr = std::shared_ptr<BaseSocketConnector>;
  using SocketSPtr = std::shared_ptr<zmq::socket_t>;

 public:
  virtual ~BaseSocketConnector() = default;

  /// Closes the socket
  virtual void closeSocket() = 0;

  /// Returns the packet received from the socket.
  /// The returned packet contains the address of the sender.
  /// @note this is (usually) a blocking call
  virtual Packet::UPtr receiveMessage() = 0;

  /// Sends the given packet to the destination specified by the address in the packet
  /// over the network using this socket.
  /// @note this is (usually) an asynchronous call
  virtual void sendMessage(Packet::UPtr packet) = 0;

  /// Returns the socket hold by this connection
  virtual SocketSPtr getSocket() const noexcept = 0;
};

/**
 * Class encapsulating writers/readers from a socket.
 */
class SYS_EXPORT_CLASS SocketConnector : public BaseSocketConnector {
 public:
  using UPtr = std::unique_ptr<SocketConnector>;
  using SPtr = std::shared_ptr<SocketConnector>;

 public:
  explicit SocketConnector(BaseSocketConnector::SocketSPtr socket);

  ~SocketConnector();

  /// Close the open socket
  void closeSocket() override;

  /// Blocking call, waits until it reads a new message from the socket
  Packet::UPtr receiveMessage() override;

  /// Sends a registration message to the hub.
  /// On success, it returns a random number greater than zero, representing the packet
  /// identifier sent to the hub. This number can be used to match the receive hub
  /// registration message
  /// Returns a negative number on failure.
  /// @note "isRoot" is used to register the root node to the hub.
  /// @note "timeoutMsec" is a timeout in msec. (greater than zero)
  /// to wait upon sending the registration message.
  /// @note "rank" is the rank of the node requesting registration.
  /// A negative value indicates that this is the first time a network node is registering
  /// one of its sockets (either IN or OUT sockets).
  /// When the rank is positive, the hub will match the registered rank and map the socket
  /// address to the same node identifier/rank on its side.
  /// In other words, a rank greater than or equal to zero means that the network node has already
  /// registered one of its ports and the rank corresponds to the received rank
  /// on that registration.
  /// If the message cannot be sent (e.g., hub is down), this method discards the message
  /// and returns an error
  int sendHubRegistrationMessage(bool isRoot, int timeoutMsec, int rank = -1) noexcept;

  /// Waits to receive the network identifier used by the hub to uniquely
  /// identify this network node.
  /// If a registration id number greater than zero is provided, this function will check the
  /// id number with the received hub packet identification number and return failure if the
  /// two numbers don't match.
  /// @note this is a blocking call until the given timeout in msec. (greater than zero) expires.
  /// @note this method should be called only once and in tandem with
  /// the method "sendHubRegistrationMessage(...)"
  /// @note this method never throws but returns "INVALID_NETWORK_NODE_ID" on error
  int recvHubRegistrationMessage(int registrationId, int timeoutMsec) noexcept;

  /// Sends a message to the hub requesting to perform network synchronization
  void sendNetworkSynchronizationMessage(bool isRoot);

  /// Non blocking call, sends the given response to the socket directed
  /// to the address specified by the given packet
  void sendMessage(Packet::UPtr packet) override;

  /// Returns the actual (pointer to the) socket used by this connector
  BaseSocketConnector::SocketSPtr getSocket() const noexcept override { return pSocket; }

 private:
  using LockGuard = boost::lock_guard<boost::mutex>;

private:
  /// Pointer to the communication socket
  BaseSocketConnector::SocketSPtr pSocket;

  /// Mutex to sync. dispatcher threads on the input socket
  boost::mutex pInputSocketMutex;

  /// Mutex to sync. dispatcher threads on the output socket
  boost::mutex pOutputSocketMutex;

  /// Deserializer for packets
  PacketDeserializer::UPtr pPacketDeserializer;

  /// Sends the given message to the given address
  void sendRawMessage(const std::string& rawMsg, const std::string& addr);

  /// Sends the given message to the given address and return after the given timeout has expired.
  /// Returns zero on success, non-zero otherwise.
  /// @note timeout is specified in msec.
  /// @note if the timeout expires and the socket is not able to send the message,
  /// the message is lost
  int sendRawMessageOrReturnOnTimeout(const std::string& rawMsg, const std::string& addr,
                                      const int timeoutMsec) noexcept;

  /// Blocks until it receives a raw message.
  /// @note this is a blocking call
  std::pair<std::string, std::string> receiveRawMessage();

  /// Waits to receive a message until the given timeout in msec.
  /// Returns an empty pair if there is no received messaged on timeout.
  /// @note this is a blocking call
  std::pair<std::string, std::string> receiveRawMessageOrReturnOnTimeout(const int timeoutMsec);
};

/**
 * Class encapsulating writers/readers from a broadcasting socket.
 */
class SYS_EXPORT_CLASS BroadcastSocketConnector : public BaseSocketConnector {
 public:
  using UPtr = std::unique_ptr<BroadcastSocketConnector>;
  using SPtr = std::shared_ptr<BroadcastSocketConnector>;

 public:
  explicit BroadcastSocketConnector(SocketSPtr socket);

  ~BroadcastSocketConnector();

  /// Broadcasts abort message to all nodes in the network
  void sendAbortMessage(Packet::UPtr packet);

  /// Closes the socket
  void closeSocket() override;

  /// Returns the packet read from the broadcasting socket
  Packet::UPtr receiveMessage() override;

  /// Broadcast the given packet to all nodes in the network subscribed to
  /// broadcasting publisher.
  /// @note the address in the packet is not used
  void sendMessage(Packet::UPtr packet) override;

  /// Returns the actual (pointer to the) socket used by this connector
  BaseSocketConnector::SocketSPtr getSocket() const noexcept override { return pSocket; }

 private:
  using LockGuard = boost::lock_guard<boost::mutex>;

private:
  /// Pointer to the communication socket
  BaseSocketConnector::SocketSPtr pSocket;

  /// Mutex to sync. dispatcher threads on the input socket
  boost::mutex pInputSocketMutex;

  /// Mutex to sync. dispatcher threads on the output socket
  boost::mutex pOutputSocketMutex;

  /// Deserializer for packets
  PacketDeserializer::UPtr pPacketDeserializer;
};

}  // namespace optnet
}  // namespace optilab
