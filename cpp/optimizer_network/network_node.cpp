#include "optimizer_network/network_node.hpp"

#include <stdexcept>  // for std::invalid_argument
#include <utility>    // for std::move

#include <spdlog/spdlog.h>

#include "optimizer_network/network_communicator.hpp"
#include "optimizer_network/network_constants.hpp"
#include "optimizer_network/network_utilities.hpp"

namespace {

const char TCP_NETWORK_PREFIX[] = "tcp://";

std::string getSocketAddr(const int socketPort, const std::string& socketAddr)
{
  std::string addr(TCP_NETWORK_PREFIX);
  if (!socketAddr.empty())
  {
    addr += socketAddr + ":";
  }
  else
  {
    addr += "*:";
  }
  addr += std::to_string(socketPort);

  return addr;
}  // getSocketAddr

}  // namespace

namespace optilab {
namespace  optnet {

NetworkNode::NetworkNode(const int sendPort,
                         const int recvPort,
                         const int broadcastSendPort,
                         const int broadcastRecvPort,
                         const std::string& inAddr,
                         const std::string& outAddr,
                         const std::string& broadcastInAddr,
                         const std::string& broadcastOutAddr,
                         bool isRoot)
: pIsRoot(isRoot)
{
  if (sendPort < networkconst::MIN_NETWORK_PORT_NUMBER ||
      sendPort > networkconst::MAX_NETWORK_PORT_NUMBER)
  {
    throw std::invalid_argument("NetworkBaseHub - invalid send port number: " +
                                std::to_string(sendPort));
  }

  if (recvPort < networkconst::MIN_NETWORK_PORT_NUMBER ||
      recvPort > networkconst::MAX_NETWORK_PORT_NUMBER)
  {
    throw std::invalid_argument("NetworkBaseHub - invalid recv port number: " +
                                std::to_string(recvPort));
  }

  if (broadcastSendPort < networkconst::MIN_NETWORK_PORT_NUMBER ||
      broadcastSendPort > networkconst::MAX_NETWORK_PORT_NUMBER)
  {
    throw std::invalid_argument("NetworkBaseHub - invalid input broadcast port number: " +
                                std::to_string(broadcastSendPort));
  }

  if (broadcastRecvPort < networkconst::MIN_NETWORK_PORT_NUMBER ||
      broadcastRecvPort > networkconst::MAX_NETWORK_PORT_NUMBER)
  {
    throw std::invalid_argument("NetworkBaseHub - invalid output broadcast port number: " +
                                std::to_string(broadcastRecvPort));
  }

  pSendPointToPointAddr =
      getSocketAddr(sendPort,
                    (inAddr.empty() ? std::string(networkconst::LOCAL_HOST_NAME) : inAddr));
  pRecvPointToPointAddr =
      getSocketAddr(recvPort,
                    (outAddr.empty() ? std::string(networkconst::LOCAL_HOST_NAME) : outAddr));
  pSendBroadcastAddr =
      getSocketAddr(broadcastSendPort,
                    (broadcastInAddr.empty() ?
                        std::string(networkconst::LOCAL_HOST_NAME) : broadcastInAddr));
  pRecvBroadcastAddr =
      getSocketAddr(broadcastRecvPort,
                    (broadcastOutAddr.empty() ?
                        std::string(networkconst::LOCAL_HOST_NAME) : broadcastOutAddr));
}

NetworkNode::~NetworkNode()
{
  if (pNetworkCommunicator)
  {
    try
    {
      pNetworkCommunicator->turnDown();
    }
    catch(...)
    {
      spdlog::error("NetworkNode - undefined exception in destructor");
    }
  }
}

int NetworkNode::getRank() const
{
  if (!pNetworkCommunicator)
  {
    throw std::runtime_error("NetworkNode - getRank: uninitialized node");
  }
  return pNetworkCommunicator->getRank();
}  // getRank

int NetworkNode::getNetworkSize() const
{
  if (!pNetworkCommunicator)
  {
    throw std::runtime_error("NetworkNode - getNetworkSize: uninitialized node");
  }
  return pNetworkCommunicator->getSize();
}  // getNetworkSize

int NetworkNode::init(const int connectionTimeoutMsec) noexcept
{
  // Re-open sockets and context
  zmq::context_t* context;
  SocketConnector::SocketSPtr pToPSendSocket;
  SocketConnector::SocketSPtr pToPRecvSocket;
  SocketConnector::SocketSPtr bcastSendSocket;
  SocketConnector::SocketSPtr bcastRecvSocket;
  try
  {
    context = new zmq::context_t(networkconst::CONTEXT_DEFAULT_NUM_THREADS);
    pToPSendSocket = utils::buildPointToPointSocket(*context);
    pToPRecvSocket = utils::buildPointToPointSocket(*context);
    bcastSendSocket = utils::buildSenderBroadcastSocket(*context);
    bcastRecvSocket = utils::buildReceivingBroadcastSocket(*context);
  }
  catch(...)
  {
    spdlog::info("NetworkNode - init: error initializing the context and sockets, return");
    return networkconst::ERR_GENERIC_ERROR;
  }

  // Set connection timeout if specified
  if (connectionTimeoutMsec > 0)
  {
    int timeout = connectionTimeoutMsec;
    pToPSendSocket->setsockopt(ZMQ_CONNECT_TIMEOUT, &timeout, sizeof(timeout));

    timeout = connectionTimeoutMsec;
    pToPRecvSocket->setsockopt(ZMQ_CONNECT_TIMEOUT, &timeout, sizeof(timeout));

    timeout = connectionTimeoutMsec;
    bcastSendSocket->setsockopt(ZMQ_CONNECT_TIMEOUT, &timeout, sizeof(timeout));

    timeout = connectionTimeoutMsec;
    bcastRecvSocket->setsockopt(ZMQ_CONNECT_TIMEOUT, &timeout, sizeof(timeout));
  }

  try
  {
    // Bind the hub to the input address
    spdlog::debug("NetworkNode - init: connecting to send address " + pSendPointToPointAddr);
    pToPSendSocket->connect(pSendPointToPointAddr.c_str());

    // Bind the hub to the output address
    spdlog::debug("NetworkNode - init: connecting to recv address " + pRecvPointToPointAddr);
    pToPRecvSocket->connect(pRecvPointToPointAddr.c_str());

    // Bind the hub to the input broadcast address
    spdlog::debug("NetworkNode - init: connecting to send broadcast address " + pSendBroadcastAddr);
    bcastSendSocket->connect(pSendBroadcastAddr.c_str());

    // Bind the hub to the output broadcast address
    spdlog::debug("NetworkNode - init: connecting to recv broadcast address " + pRecvBroadcastAddr);
    bcastRecvSocket->connect(pRecvBroadcastAddr.c_str());

    // Subscribe to all messages
    bcastRecvSocket->setsockopt(ZMQ_SUBSCRIBE, "", 0);
  }
  catch (...)
  {
    spdlog::info("NetworkNode - init: undefined error on socket connection");
    return networkconst::ERR_GENERIC_ERROR;
  }

  // Instantiate the network communicator.
  // @note the root synchronizes on initialization
  const bool synchNetworkOnInit = pIsRoot;
  pNetworkCommunicator =
      std::make_shared<NetworkCommunicator>(
          BaseSocketConnector::UPtr(new SocketConnector(pToPSendSocket)),
          BaseSocketConnector::UPtr(new SocketConnector(pToPRecvSocket)),
          BaseSocketConnector::UPtr(new BroadcastSocketConnector(bcastSendSocket)),
          BaseSocketConnector::UPtr(new BroadcastSocketConnector(bcastRecvSocket)),
          std::unique_ptr<zmq::context_t>(context), pIsRoot, synchNetworkOnInit);
  return networkconst::ERR_NO_ERROR;
}  // init

int NetworkNode::registerNodeToHub()
{
  if (!pNetworkCommunicator)
  {
    throw std::runtime_error("NetworkNode - registerNodeToHub: no network communicator");
  }

  return pNetworkCommunicator->init();
}  // registerNodeToHub

int NetworkNode::broadcast(Packet::UPtr& packet, int root)
{
  if (!pNetworkCommunicator)
  {
    throw std::runtime_error("NetworkNode - broadcast: uninitialized node");
  }
  return pNetworkCommunicator->broadcast(packet, root);
}  // broadcast

int NetworkNode::send(Packet::UPtr packet, const int dest)
{
  if (!pNetworkCommunicator)
  {
    throw std::runtime_error("NetworkNode - send: uninitialized node");
  }
  return pNetworkCommunicator->send(std::move(packet), dest);
}  // send

Packet::UPtr NetworkNode::receive(const int source, PacketTag::UPtr tag)
{
  if (!pNetworkCommunicator)
  {
    throw std::runtime_error("NetworkNode - receive: uninitialized node");
  }

  Packet::UPtr recvPacket(new Packet());
  if (tag)
  {
    // Set the tag if any
    recvPacket->packetTag = std::move(tag);
  }

  // Receive the packet and check for errors
  if (pNetworkCommunicator->receive(recvPacket, source) != 0)
  {
    // Return empty packet on error
    return Packet::UPtr(nullptr);
  }

  return recvPacket;
}  // receive

}  // namespace optnet
}  // namespace optilab
