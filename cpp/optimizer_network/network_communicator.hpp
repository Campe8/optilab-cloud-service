//
// Copyright OptiLab 2019-2020. All rights reserved.
//
// Implementation class for the network communicator.
// Similar to MPI frameworks, a communicator encapsulates
// all communication among a set of processes.
// In the context of optimizer network, a communicator is implemented
// as a wrapper around socket connectors for point to point and
// and broadcast communication.
//

#pragma once

#include <deque>
#include <memory>   // for std::shared_ptr
#include <string>
#include <utility>  // for std::pair

#include <boost/atomic.hpp>
#include <boost/chrono.hpp>
#include <boost/scoped_ptr.hpp>
#include <boost/thread.hpp>

#include <sparsepp/spp.h>

#include "optimizer_network/network_base_communicator.hpp"
#include "optimizer_network/packet.hpp"
#include "optimizer_network/socket_connector.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace  optnet {

class SYS_EXPORT_CLASS NetworkCommunicator : public NetworkBaseCommunicator {
 public:
  using SPtr = std::shared_ptr<NetworkCommunicator>;

 public:
  /**
   * Default constructor.
   * Used for testing only.
   * DO NOT USE.
   */
  NetworkCommunicator() {};

  /// Constructor:
  /// - ptpSend: point to point sender connector
  /// - ptpRecv: point to point receiver connector
  /// - bcastSend: broadcast sender connector
  /// - bcastRecv: broadcast receiver connector
  /// - ctx: the context owning the sockets used to create the input connectors
  /// - isRoot: flag indicating whether or not this is the root communicator
  /// - synchNetworkOnInitialization: send a network synchronization request to the hub
  ///   when initializing this communicator
  /// @note throws std::invalid_argument if any of the given connectors is empty
  /// @note the network communicator takes ownership of the given context and sockets and
  /// manages their life time
  NetworkCommunicator(BaseSocketConnector::UPtr ptpSend,
                      BaseSocketConnector::UPtr ptpRecv,
                      BaseSocketConnector::UPtr bcastSend,
                      BaseSocketConnector::UPtr bcastRecv,
                      std::unique_ptr<zmq::context_t> ctx,
                      bool isRoot=false,
                      bool synchNetworkOnInitialization=false);

  ~NetworkCommunicator() override;

  /// Turn down this communicator, stop threads and return
  void turnDown() override;

  /// Initializes this communicator.
  /// This will register this communicator to the hub and receive the network configuration.
  /// Returns zero on success, non-zero otherwise.
  /// @note this is a blocking call waiting for the hub to send the network configuration.
  /// @note if "initTimeoutMsec" is specified and greater than zero, this method will wait up
  /// to "initTimeoutMsec" msec. before returning if the hub is not able to respond successfully.
  /// A negative timeout value (default) makes the network communicator wait indefinitely
  int init(int initTimeoutMsec = -1) override;

  /// Returns the rank of the process running on this communication channel.
  /// The rank is the unique network identifier associated with this process when
  /// registering into the network hub
  int getRank() const noexcept override;

  /// Returns the number of processes attached to this communicator.
  /// Better, returns the number of processes/sockets attached to the hub
  int getSize() const noexcept override;

  /// Terminates this communicator.
  /// This method makes a "best attempt" to abort all tasks running
  /// on this communicator
  void abort() override;

  /// Broadcasts the given packet from the process with rank "root"
  /// to all other processes attached to this communicator.
  /// Returns zero on success, non-zero otherwise
  int broadcast(Packet::UPtr& packet, const int root) noexcept override;

  /// Sends the given packet to the given destination.
  /// Returns zero on success, non-zero otherwise.
  /// @note the packet may contain a packet tag.
  /// @note this is a non-blocking call.
  /// @note the network address of the packet is automatically set by this method
  int send(Packet::UPtr packet, const int dest) noexcept override;

  /// Receive a packet from the specified source and sets its content into the given
  /// packet instance.
  /// Returns zero on success, non-zero otherwise.
  /// @note if the given packet contains a tag, this method blocks until a packet with specified
  /// tag is received.
  /// @note if the source is ANY_NETWORK_NODE_ID, it receives from any node in the network.
  /// @note this is a blocking call.
  /// @note the network address of the packet is automatically set by this method
  int receive(Packet::UPtr& packet, const int source) noexcept override;

  /// Non blocking version of "receive(...)".
  /// Receives a packet from the specified source matching the tag if specified.
  /// Sets its content into the given packet instance.
  /// If "err" is specified, sets the flag indicating error (if any).
  /// Returns non-zero on success, i.e., THERE IS a match and the packet has been set.
  /// Returns zero otherwise, i.r., there was an error, there was no match, or there
  /// was no packet to be received.
  /// @note if the source is ANY_NETWORK_NODE_ID, it receives from any node in the network.
  /// @note this is a non blocking call.
  int ireceive(Packet::UPtr& packet, const int source, bool* err=nullptr) noexcept override;

  /// Probes the input socket and wait for any incoming message from
  /// any other network node
  int probe(Packet::UPtr& packet) noexcept override;

  /// Non blocking version of "probe(...)".
  /// Probes the input socket and wait for any incoming message from
  /// any other network node.
  /// Returns non-zero on success, i.e., THERE IS a packet. Zero otherwise.
  /// If "err" is specified, sets the flag indicating error (if any).
  /// @note this is a non blocking call
  int iprobe(Packet::UPtr& packet, bool* err=nullptr) noexcept override;

  /// Waits to receive any tag from the given source.
  /// When a message is received, it sets it into the packet tag.
  /// Returns zero if the previous tag and the received tag are the same.
  /// Returns non-zero otherwise
  int waitSpecTagFromSpecSource(Packet::UPtr& packet, const int source, bool replaceTag=true)
  noexcept override;

  /// Converts and returns the given network (socket) address to the rank of the node
  /// owning the socket.
  /// @note returns invalid socket is the network address is not registered
  /// in the network
  int convertNetworkAddrToRank(const std::string& netAddr) const noexcept;

 private:
  /// Lock type on the internal mutex
  using LockGuard = boost::lock_guard<boost::mutex>;
  using UniqueLock = boost::unique_lock<boost::mutex>;

  /// Map of pairs <network_node_rank, <network_node_addr_id, network_node_recv_address>>.
  /// Each node has a rank, a sender address and a receiving address.
  /// These addresses are mapped to the PToP send and recv sockets.
  /// In particular, each network node does the following (in order):
  /// 1) registers its node rank to the hub;
  /// 2) registers its PToP Send connector socket to the hub;
  /// 3) registers its PToP Recv connector socket to the hub.
  /// These addresses are used to send and receive respectively.
  /// These addresses are send as part of the network packet information.
  /// In particular, on a "send" operation:
  /// - PToP: the address should be the receiver's address;
  /// - Broadcast: the address should be the sender's address.
  /// On a "receive" operation:
  /// - PToP: the address is the sender's address;
  /// - Broadcast: the address is the sender's address.
  /// For example, sending PtoP from rank 1 to rank 2:
  /// - node with rank 1 sends a packet using the recv_address (map.second.second);
  /// - node with rank 2 receives a packet and check that the address
  //    matches its sender's network address
  /// For broadcast packets all addresses are based on the first sender's address.
  using NetworkNodeMap = spp::sparse_hash_map<int, std::pair<std::string, std::string>>;

  /// Queue of received packets from the network
  using PacketQueue = std::deque<Packet::UPtr>;

 private:
  /// Rank (unique network identifier) of this communicator
  int pRank{-1};

  /// The network address to be used for broadcast communications for this communicator.
  /// This variable is set at network synchronization.
  /// The communicator receives the network map, it gets the entry
  /// corresponding to this node, and takes the address of the
  /// PToPRecvConnector's socket.
  /// In other words, the "pNetworkAddr" is the recv address of this node.
  /// This is only used for broadcast-related messages
  std::string pNetworkBroadcastAddr;

  /// Flag indicating whether or not this is the root communicator of the network
  bool pRootCommunicator{true};

  /// Flag indicating whether or not the network should be synchronized
  /// on initialization of this communicator
  bool pSynchOnInit{true};

  /// Exception flag set by asycnhronous reading threads
  mutable bool pExceptionInAsynchThread{false};

  /// Flag indicating whether this network communicator is running or not
  boost::atomic<bool> pRunning{false};

  /// Flag indicating whether or not threads have been initialized
  boost::atomic<bool> pInitThreads{false};

  /// Flag indicating whether or not turn-down has been done already
  boost::atomic<bool> pTurnDown{false};

  /// Thread used for point to point reads
  boost::scoped_ptr<boost::thread> pPtoPThread;

  /// Future/shared state PtoP thread
  mutable boost::unique_future<void> pPtoPFuture;

  /// Thread used for broadcast reads
  boost::scoped_ptr<boost::thread> pBcastThread;

  /// Future/shared state broadcast thread
  mutable boost::unique_future<void> pBcastFuture;

  /// Queue of received packets
  PacketQueue pPacketQueue;

  /// Queue of received bcast packets
  PacketQueue pBcastPacketQueue;

  /// Mutex synch.ing the access to the queue
  mutable boost::mutex pPacketQueueMutex;

  /// Mutex synch.ing the access to the bcast queue
  mutable boost::mutex pBcastPacketQueueMutex;

  /// Mutex synch.ing the access to the queue
  mutable boost::mutex pNetworkMapMutex;

  /// Mutex used to synchronize aborts and turn-downs
  boost::mutex pTurnDownMutex;

  /// Condition variable to synchronize read on the internal packet queue
  mutable boost::condition_variable pPacketQueueConditionVar;

  /// Condition variable to synchronize read on the internal bcast packet queue
  mutable boost::condition_variable pBcastPacketQueueConditionVar;

  /// Timeout for turn down
  const boost::chrono::milliseconds pTurnDownTimeout{
    boost::chrono::milliseconds(1000)};

  /// Network context owning the sockets on the connectors of this class
  std::unique_ptr<zmq::context_t> pCtx;

  /// Connector used for sending point to point messages
  SocketConnector::UPtr pPToPSendConnector;

  /// Connector used for receiving point to point messages
  SocketConnector::UPtr pPToPRecvConnector;

  /// Connector used for broadcasting messages
  BroadcastSocketConnector::UPtr pBroadcastSendConnector;

  /// Connector used for receiving broqdcasted messages
  BroadcastSocketConnector::UPtr pBroadcastRecvConnector;

  /// Map of network nodes
  NetworkNodeMap pNetworkNodeMap;

  /// Starts the threads reading on PtoP and Broadcast connectors
  void initReadingThreads();

  /// Reads from point to point connector
  void readFromPToPConnector();

  /// Reads from broadcast connector
  void readFromBcastConnector();

  /// Check for exceptions from future
  void checkForFutureException() const;

  /// Similar to receive but for broadcasted messages.
  /// Returns the bcasted packet.
  /// @note returns nullptr on error
  Packet::UPtr receiveBcast() noexcept;
};

}  // namespace optnet
}  // namespace optilab
