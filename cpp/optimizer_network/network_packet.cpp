#include "optimizer_network/network_packet.hpp"

#include "optilab_protobuf/network_packet.pb.h"

#include <spdlog/spdlog.h>

namespace optilab {
namespace  optnet {

int networkPacketTypeToInt(BaseNetworkPacket::BaseNetworkPacketType type)
{
  return static_cast<int>(type);
}  // networkPacketTypeToInt

BaseNetworkPacket::BaseNetworkPacketType intToNetworkPacketType(int type)
{
  if (type >=
  networkPacketTypeToInt(BaseNetworkPacket::BaseNetworkPacketType::BNPT_UNSPECIFIED_PACKET))
  {
    return BaseNetworkPacket::BaseNetworkPacketType::BNPT_UNSPECIFIED_PACKET;
  }
  return static_cast<BaseNetworkPacket::BaseNetworkPacketType>(type);
}  // intToNetworkPacketType

BaseNetworkPacket::UPtr deserializeNetworkPacket(const std::string& bytes)
{
  NetworkProtoPacket protoPacket;
  protoPacket.ParseFromString(bytes);

  switch(protoPacket.payloadtype())
  {
    case NetworkProtoPacket_Type::NetworkProtoPacket_Type_NPT_DOUBLE:
    {
      std::vector<double> data(protoPacket.payload_size());
      for (int dataIdx = 0; dataIdx < protoPacket.payload_size(); ++dataIdx)
      {
        const auto& payload = protoPacket.payload(dataIdx);
        data[dataIdx] = payload.p_double();
      }
      NetworkPacket<double>* packet = new NetworkPacket<double>(std::move(data));
      packet->packetType = BaseNetworkPacket::BaseNetworkPacketType::BNPT_NETWORK_PACKET;
      return NetworkPacket<double>::UPtr(packet);
    }
    case NetworkProtoPacket_Type::NetworkProtoPacket_Type_NPT_FLOAT:
    {
      std::vector<float> data(protoPacket.payload_size());
      for (int dataIdx = 0; dataIdx < protoPacket.payload_size(); ++dataIdx)
      {
        const auto& payload = protoPacket.payload(dataIdx);
        data[dataIdx] = payload.p_float();
      }
      NetworkPacket<float>* packet = new NetworkPacket<float>(std::move(data));
      packet->packetType = BaseNetworkPacket::BaseNetworkPacketType::BNPT_NETWORK_PACKET;
      return NetworkPacket<float>::UPtr(packet);
    }
    case NetworkProtoPacket_Type::NetworkProtoPacket_Type_NPT_INT_32:
    {
      std::vector<int32_t> data(protoPacket.payload_size());
      for (int dataIdx = 0; dataIdx < protoPacket.payload_size(); ++dataIdx)
      {
        const auto& payload = protoPacket.payload(dataIdx);
        data[dataIdx] = payload.p_int32();
      }
      NetworkPacket<int32_t>* packet = new NetworkPacket<int32_t>(std::move(data));
      packet->packetType = BaseNetworkPacket::BaseNetworkPacketType::BNPT_NETWORK_PACKET;
      return NetworkPacket<int32_t>::UPtr(packet);
    }
    case NetworkProtoPacket_Type::NetworkProtoPacket_Type_NPT_INT_64:
    {
      std::vector<int64_t> data(protoPacket.payload_size());
      for (int dataIdx = 0; dataIdx < protoPacket.payload_size(); ++dataIdx)
      {
        const auto& payload = protoPacket.payload(dataIdx);
        data[dataIdx] = payload.p_int64();
      }
      NetworkPacket<int64_t>* packet = new NetworkPacket<int64_t>(std::move(data));
      packet->packetType = BaseNetworkPacket::BaseNetworkPacketType::BNPT_NETWORK_PACKET;
      return NetworkPacket<int64_t>::UPtr(packet);
    }
    case NetworkProtoPacket_Type::NetworkProtoPacket_Type_NPT_UINT_32:
    {
      std::vector<uint32_t> data(protoPacket.payload_size());
      for (int dataIdx = 0; dataIdx < protoPacket.payload_size(); ++dataIdx)
      {
        const auto& payload = protoPacket.payload(dataIdx);
        data[dataIdx] = payload.p_uint32();
      }
      NetworkPacket<uint32_t>* packet = new NetworkPacket<uint32_t>(std::move(data));
      packet->packetType = BaseNetworkPacket::BaseNetworkPacketType::BNPT_NETWORK_PACKET;
      return NetworkPacket<uint32_t>::UPtr(packet);
    }
    case NetworkProtoPacket_Type::NetworkProtoPacket_Type_NPT_UINT_64:
    {
      std::vector<uint64_t> data(protoPacket.payload_size());
      for (int dataIdx = 0; dataIdx < protoPacket.payload_size(); ++dataIdx)
      {
        const auto& payload = protoPacket.payload(dataIdx);
        data[dataIdx] = payload.p_uint64();
      }
      NetworkPacket<uint64_t>* packet = new NetworkPacket<uint64_t>(std::move(data));
      packet->packetType = BaseNetworkPacket::BaseNetworkPacketType::BNPT_NETWORK_PACKET;
      return NetworkPacket<uint64_t>::UPtr(packet);
    }
    case NetworkProtoPacket_Type::NetworkProtoPacket_Type_NPT_BOOL:
    {
      std::vector<bool> data(protoPacket.payload_size());
      for (int dataIdx = 0; dataIdx < protoPacket.payload_size(); ++dataIdx)
      {
        const auto& payload = protoPacket.payload(dataIdx);
        data[dataIdx] = payload.p_bool();
      }
      NetworkPacket<bool>* packet = new NetworkPacket<bool>(std::move(data));
      packet->packetType = BaseNetworkPacket::BaseNetworkPacketType::BNPT_NETWORK_PACKET;
      return NetworkPacket<bool>::UPtr(packet);
    }
    default:
    {
      if (protoPacket.payloadtype() != NetworkProtoPacket_Type::NetworkProtoPacket_Type_NPT_STRING)
      {
        throw std::runtime_error("BaseNetworkPacket - deserializePacket: undefined payload type "
            + std::to_string(protoPacket.payloadtype()));
      }

      std::vector<std::string> data(protoPacket.payload_size());
      for (int dataIdx = 0; dataIdx < protoPacket.payload_size(); ++dataIdx)
      {
        const auto& payload = protoPacket.payload(dataIdx);
        data[dataIdx] = payload.p_string();
      }
      NetworkPacket<std::string>* packet = new NetworkPacket<std::string>(std::move(data));
      packet->packetType = BaseNetworkPacket::BaseNetworkPacketType::BNPT_NETWORK_PACKET;
      return NetworkPacket<std::string>::UPtr(packet);
    }
  }

  return BaseNetworkPacket::UPtr();
}  // deserializeNetworkPacket

template<>
std::pair<BaseNetworkPacket::BaseNetworkPacketType, std::string>
NetworkPacket<double>::serialize() const noexcept
{
  NetworkProtoPacket protoPacket;
  protoPacket.set_payloadtype(NetworkProtoPacket_Type::NetworkProtoPacket_Type_NPT_DOUBLE);
  for (auto d : data)
  {
    auto payload = protoPacket.add_payload();
    payload->set_p_double(d);
  }

  return {BaseNetworkPacket::BaseNetworkPacketType::BNPT_NETWORK_PACKET,
    protoPacket.SerializeAsString()};
}  // serialize

template<>
std::pair<BaseNetworkPacket::BaseNetworkPacketType, std::string>
NetworkPacket<float>::serialize() const noexcept
{
  NetworkProtoPacket protoPacket;
  protoPacket.set_payloadtype(NetworkProtoPacket_Type::NetworkProtoPacket_Type_NPT_FLOAT);
  for (auto d : data)
  {
    auto payload = protoPacket.add_payload();
    payload->set_p_float(d);
  }

  return {BaseNetworkPacket::BaseNetworkPacketType::BNPT_NETWORK_PACKET,
    protoPacket.SerializeAsString()};
}  // serialize

template<>
std::pair<BaseNetworkPacket::BaseNetworkPacketType, std::string>
NetworkPacket<int32_t>::serialize() const noexcept
{
  NetworkProtoPacket protoPacket;
  protoPacket.set_payloadtype(NetworkProtoPacket_Type::NetworkProtoPacket_Type_NPT_INT_32);
  for (auto d : data)
  {
    auto payload = protoPacket.add_payload();
    payload->set_p_int32(d);
  }

  return {BaseNetworkPacket::BaseNetworkPacketType::BNPT_NETWORK_PACKET,
    protoPacket.SerializeAsString()};
}  // serialize

template<>
std::pair<BaseNetworkPacket::BaseNetworkPacketType, std::string>
NetworkPacket<int64_t>::serialize() const noexcept
{
  NetworkProtoPacket protoPacket;
  protoPacket.set_payloadtype(NetworkProtoPacket_Type::NetworkProtoPacket_Type_NPT_INT_64);
  for (auto d : data)
  {
    auto payload = protoPacket.add_payload();
    payload->set_p_int64(d);
  }

  return {BaseNetworkPacket::BaseNetworkPacketType::BNPT_NETWORK_PACKET,
    protoPacket.SerializeAsString()};
}  // serialize

template<>
std::pair<BaseNetworkPacket::BaseNetworkPacketType, std::string>
NetworkPacket<uint32_t>::serialize() const noexcept
{
  NetworkProtoPacket protoPacket;
  protoPacket.set_payloadtype(NetworkProtoPacket_Type::NetworkProtoPacket_Type_NPT_UINT_32);
  for (auto d : data)
  {
    auto payload = protoPacket.add_payload();
    payload->set_p_uint32(d);
  }

  return {BaseNetworkPacket::BaseNetworkPacketType::BNPT_NETWORK_PACKET,
    protoPacket.SerializeAsString()};
}  // serialize

template<>
std::pair<BaseNetworkPacket::BaseNetworkPacketType, std::string>
NetworkPacket<uint64_t>::serialize() const noexcept
{
  NetworkProtoPacket protoPacket;
  protoPacket.set_payloadtype(NetworkProtoPacket_Type::NetworkProtoPacket_Type_NPT_UINT_64);
  for (auto d : data)
  {
    auto payload = protoPacket.add_payload();
    payload->set_p_uint64(d);
  }

  return {BaseNetworkPacket::BaseNetworkPacketType::BNPT_NETWORK_PACKET,
    protoPacket.SerializeAsString()};
}  // serialize

template<>
std::pair<BaseNetworkPacket::BaseNetworkPacketType, std::string>
NetworkPacket<bool>::serialize() const noexcept
{
  NetworkProtoPacket protoPacket;
  protoPacket.set_payloadtype(NetworkProtoPacket_Type::NetworkProtoPacket_Type_NPT_BOOL);
  for (auto d : data)
  {
    auto payload = protoPacket.add_payload();
    payload->set_p_bool(d);
  }

  return {BaseNetworkPacket::BaseNetworkPacketType::BNPT_NETWORK_PACKET,
    protoPacket.SerializeAsString()};
}  // serialize

template<>
std::pair<BaseNetworkPacket::BaseNetworkPacketType, std::string>
NetworkPacket<std::string>::serialize() const noexcept
{
  NetworkProtoPacket protoPacket;
  protoPacket.set_payloadtype(NetworkProtoPacket_Type::NetworkProtoPacket_Type_NPT_STRING);
  for (const auto& d : data)
  {
    auto payload = protoPacket.add_payload();
    payload->set_p_string(d);
  }

  return {BaseNetworkPacket::BaseNetworkPacketType::BNPT_NETWORK_PACKET,
    protoPacket.SerializeAsString()};
}  // serialize

}  // namespace optnet
}  // namespace optilab

