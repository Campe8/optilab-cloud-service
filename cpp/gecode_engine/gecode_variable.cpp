#include "gecode_engine/gecode_variable.hpp"

#include <cassert>
#include <stdexcept>  // for std::invalid_argument
#include <utility>  // for std::pair

#include <gecode/int.hh>

#ifdef GECODE_HAS_SET_VARS
#include <gecode/set.hh>
#endif

#ifdef GECODE_HAS_FLOAT_VARS
#include <gecode/float.hh>
#endif

namespace optilab {

GecodeVariable::GecodeVariable(const std::string& name, GecodeVarType varType,
                               const std::vector<std::size_t>& indexList,
                               const OptilabSpace::SPtr& space)
: CPVariable(name),
  pVarType(varType),
  pSpace(space),
  pVariableIndexList(indexList)
{
  if (!pSpace)
  {
    throw std::invalid_argument("GecodeVariable - empty pointer to OptilabSpace");
  }
}

GecodeVariable::GecodeVariable(const std::string& name, const CPDomain& domain,
                               const OptilabSpace::SPtr& space)
: CPVariable(name, domain),
  pVarType(getVarTypeFromDomain(getDomain())),
  pSpace(space)
{
  if (!pSpace)
  {
    throw std::invalid_argument("GecodeVariable - empty pointer to OptilabSpace");
  }

  // Build the variable and store its index into the space
  pVariableIndexList.push_back(buildVariableAndAddItToSpace(getDomain()));
}

GecodeVariable::GecodeVariable(const std::string& name, const CPDomain& domain, int len,
                               const OptilabSpace::SPtr& space)
: CPVariable(name, domain, len),
  pVarType(getVarTypeFromDomain(getDomain())),
  pSpace(space)
{
  if (!pSpace)
  {
    throw std::invalid_argument("GecodeVariable - empty pointer to OptilabSpace");
  }

  for (int vIdx = 0; vIdx < len; ++vIdx)
  {
    pVariableIndexList.push_back(buildVariableAndAddItToSpace(getDomain()));
  }
}

GecodeVariable::GecodeVariable(const std::string& name, const CPDomain& domain,
                               const std::vector<int>& dims, const OptilabSpace::SPtr& space)
: CPVariable(name, domain, dims),
  pVarType(getVarTypeFromDomain(getDomain())),
  pSpace(space)
{
  if (!pSpace)
  {
    throw std::invalid_argument("GecodeVariable - empty pointer to OptilabSpace");
  }

  if (getNumDim() < 2)
  {
    throw std::runtime_error("GecodeVariable - invalid number of dimensions for a matrix variable");
  }

  int numvars = 1;
  for (auto d : getVarDims())
  {
    numvars *= d;
  }

  for (int vIdx = 0; vIdx < numvars; ++vIdx)
  {
    pVariableIndexList.push_back(buildVariableAndAddItToSpace(getDomain()));
  }
}

CPVariable::SPtr GecodeVariable::getSubVariable(const std::vector<int>& subscr)
{
  if (isScalar())
  {
    throw std::runtime_error("GecodeVariable - cannot get a sub-variable from a scalar variable");
  }

  // Get the flattened index
  const auto idx = getFlattenedIndex(subscr);

  // The flattened index corresponds to the index in the internal list of indices
  if (idx >= pVariableIndexList.size())
  {
    throw std::runtime_error("GecodeVariable - invalid indexing in array/matrix");
  }
  const auto varIdx = pVariableIndexList[idx];

  // Create a new CPVariable with "varIdx" as unique index
  const auto flatIdxName = createNameForIndexedVariable(subscr);
  std::vector<std::size_t> varIdxList;
  varIdxList.push_back(varIdx);
  GecodeVariable::SPtr subVar = std::shared_ptr<GecodeVariable>(
      new GecodeVariable(flatIdxName, pVarType, varIdxList, pSpace));

  // Create and set the domain of the variable
  CPDomain domainCopy = getDomain();

  // Change the boundaries in the domain w.r.t. the current variable boundaries
  if (pVarType == GecodeVarType::GVT_BOOL)
  {
    const auto bvar = pSpace->getBoolVar(varIdx);
    domainCopy.intValues.clear();
    domainCopy.intValues.push_back(bvar.min());
    domainCopy.intValues.push_back(bvar.max());
  }
  else if (pVarType == GecodeVarType::GVT_INT)
  {
    const auto ivar = pSpace->getIntVar(varIdx);
    domainCopy.intValues.clear();
    domainCopy.intValues.push_back(ivar.min());
    domainCopy.intValues.push_back(ivar.max());
  }
#ifdef GECODE_HAS_SET_VARS
  else if (pVarType == GecodeVarType::GVT_SET)
  {
    // No-op
  }
#endif
#ifdef GECODE_HAS_FLOAT_VARS
  else if (pVarType == GecodeVarType::GVT_FLOAT)
  {
    const auto fvar = pSpace->getFloatVar(varIdx);
    domainCopy.doubleValues.clear();
    domainCopy.doubleValues.push_back(fvar.min());
    domainCopy.doubleValues.push_back(fvar.max());
  }
#endif

  // Reset the domain within the newly created variable
  subVar->overrideDomain(domainCopy);

  // Set output of the sub-variable the same as this variable
  subVar->setOutput(isOutput());

  // Return the variable
  return subVar;
}  // getSubVariable

GecodeVariable::GecodeVarType GecodeVariable::getVarTypeFromDomain(const CPDomain& domain)
{
  if (domain.isBool) return GecodeVarType::GVT_BOOL;
  if (domain.isSet) return GecodeVarType::GVT_SET;
  if (!domain.intValues.empty()) return GecodeVarType::GVT_INT;
  if (!domain.doubleValues.empty()) return GecodeVarType::GVT_FLOAT;
  throw std::runtime_error("GecodeVariable - unrecognized variable type");
}  // getVarTypeFromDomain

std::size_t GecodeVariable::buildVariableAndAddItToSpace(const CPDomain& domain)
{
  std::size_t varIdx;
  const auto varType = getVarTypeFromDomain(domain);
  if (varType == GecodeVarType::GVT_BOOL)
  {
    if (domain.intValues.size() != 2)
    {
      throw std::runtime_error("GecodeVariable - empty Boolean domain");
    }

    Gecode::BoolVar boolVar(*pSpace, domain.intValues[0] == 0 ? 0 : 1,
        domain.intValues[1] == 0 ? 0 : 1);
    varIdx = pSpace->addBoolVar(boolVar);
  }
  else if (varType == GecodeVarType::GVT_INT)
  {
    if (domain.intValues.empty())
    {
      throw std::runtime_error("GecodeVariable - empty integer domain");
    }

    // Set the values of the lb/ub w.r.t. the min/max allowed values
    int lb = (domain.intValues.front() <= Gecode::Int::Limits::min) ?
        Gecode::Int::Limits::min :
        domain.intValues.front();

    int ub = (domain.intValues.back() >= Gecode::Int::Limits::max) ?
        Gecode::Int::Limits::max :
        domain.intValues.back();

    if (domain.isInterval)
    {
      Gecode::IntVar intVar(*pSpace, lb, ub);
      varIdx = pSpace->addIntVar(intVar);
    }
    else if (domain.isListOfRanges)
    {
      if ((domain.intValues.size() % 2) != 0)
      {
        throw std::runtime_error("GecodeVariable - wrong number of pairs for "
            "list of ranges domain");
      }

      const int numRanges = static_cast<int>(domain.intValues.size()) / 2;

      int ranges[numRanges][2];
      for (int idx = 0; idx < numRanges; ++idx)
      {

        ranges[idx][0] = static_cast<int>(
            std::max<int64_t>(static_cast<int64_t>(Gecode::Int::Limits::min),
                              domain.intValues[2 * idx]));
        ranges[idx][1] = static_cast<int>(
            std::min<int64_t>(static_cast<int64_t>(Gecode::Int::Limits::max),
                              domain.intValues[(2 * idx) + 1]));
      }

      auto intSet = Gecode::IntSet(ranges, numRanges);
      Gecode::IntVar intVar(*pSpace, intSet);
      varIdx = pSpace->addIntVar(intVar);
    }
    else if (domain.isListOfValues)
    {
      const int listLen = static_cast<int>(domain.intValues.size());

      int list[listLen];
      for (int idx = 0; idx < listLen; ++idx)
      {
        int valRound = static_cast<int>(std::max<int64_t>(
            static_cast<int64_t>(Gecode::Int::Limits::min), domain.intValues[idx]));
        valRound = static_cast<int>(std::min(valRound, Gecode::Int::Limits::max));
        list[idx] = valRound;
      }
      auto intSet = Gecode::IntSet(list, listLen);
      Gecode::IntVar intVar(*pSpace, intSet);
      varIdx = pSpace->addIntVar(intVar);
    }
    else
    {
      throw std::runtime_error("GecodeVariable - Integer variable type not legal");
    }
  }
#ifdef GECODE_HAS_SET_VARS
  else if (varType == GecodeVarType::GVT_SET)
  {
    const int listLen = static_cast<int>(domain.intValues.size());

    int list[listLen];
    for (int idx = 0; idx < listLen; ++idx)
    {
      int valRound = static_cast<int>(std::max<int64_t>(
          static_cast<int64_t>(Gecode::Int::Limits::min), domain.intValues[idx]));
      valRound = static_cast<int>(std::min(valRound, Gecode::Int::Limits::max));
      list[idx] = valRound;
    }
    auto intSet = Gecode::IntSet(list, listLen);
    Gecode::SetVar setVar(*pSpace, Gecode::IntSet::empty, intSet);
    varIdx = pSpace->addSetVar(setVar);
  }
#endif
#ifdef GECODE_HAS_FLOAT_VARS
  else if (varType == GecodeVarType::GVT_FLOAT)
  {
    if (domain.doubleValues.empty())
    {
      throw std::runtime_error("GecodeVariable - empty float domain");
    }

    // Set the values of the lb/ub w.r.t. the min/max allowed values
    double lb = (domain.doubleValues.front() <= Gecode::Float::Limits::min) ?
        Gecode::Float::Limits::min :
        domain.intValues.front();

    double ub = (domain.doubleValues.front() >= Gecode::Float::Limits::max) ?
        Gecode::Float::Limits::max :
        domain.intValues.front();

    Gecode::FloatVar floatVar(*pSpace, lb, ub);
    varIdx = pSpace->addFloatVar(floatVar);
  }
#endif
  else
  {
    throw std::runtime_error("GecodeVariable - domain type not recognized");
  }
  return varIdx;
}  // buildVariableAndAddItToSpace

}  // namespace optilab
