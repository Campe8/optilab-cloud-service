//
// Copyright OptiLab 2019. All rights reserved.
//
// Class encapsulating an Gecode constraint.
//

#pragma once

#include <memory> // for std::shared_ptr

#include "gecode_engine/optilab_space.hpp"
#include "solver/cp_constraint.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS GecodeConstraint : public CPConstraint {
 public:
  using SPtr = std::shared_ptr<GecodeConstraint>;

 public:
  /// Creates a Gecode constraint.
  /// @note throws std::invalid_argument if the pointer to the Optilab space is nullptr
  GecodeConstraint(const std::string& name, const std::vector<CPArgument>& args,
                   PropagationType propType, OptilabSpace::SPtr space)
 : CPConstraint(name, args, propType),
   pSpacePtr(space)
 {
    if (!pSpacePtr)
    {
      throw std::invalid_argument("GecodeConstraint - empty pointer to space");
    }
 }

  inline OptilabSpace* getSpace() const { return pSpacePtr.get(); }

 private:
  /// Space this constraint is declared into
  OptilabSpace::SPtr pSpacePtr;
};

}  // namespace optilab
