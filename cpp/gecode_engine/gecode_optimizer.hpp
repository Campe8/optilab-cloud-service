//
// Copyright OptiLab 2019. All rights reserved.
//
// A Gecode optimizer is an asynchronous engine that
// holds a pipeline containing Gecode processors.
//

#pragma once

#include <memory>  // for std::shared_ptr
#include <string>

#include "data_structure/metrics/metrics_register.hpp"
#include "engine/async_engine.hpp"
#include "engine/processor_pipeline.hpp"
#include "gecode_engine/gecode_processor.hpp"
#include "gecode_engine/gecode_utilities.hpp"
#include "model/model.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS GecodeOptimizer : public AsyncEngine<gecodeengine::GecodeWork::SPtr> {
 public:
  using SPtr = std::shared_ptr<GecodeOptimizer>;

 public:
  GecodeOptimizer(const std::string& engineName, gecodeengine::GecodeResult::SPtr result,
                  MetricsRegister::SPtr metricsRegister);

  /// Initializes this optimizer with the given model.
  /// i.e., loads the model into the internal state and creates
  /// the optimizer processor that will solve it.
  /// @note this is a blocking call
  void loadModel(const std::string& jsonModel);

  /// Runs this optimizer on the loaded model
  void runOptimizer();

  /// Interrupts the solving processes on this optimizer.
  /// Returns true if the solver was successfully interrupted.
  /// Returns false otherwise
  bool interruptOptimizer();

 protected:
  ProcessorPipeline::SPtr buildPipeline(int pipeline = 0) override;

 private:
  using Work = gecodeengine::GecodeWork;
  using BaseClass = AsyncEngine<Work::SPtr>;

 private:
  /// Model solved by the optimizer
  Model::SPtr pOptimizerModel;

  /// Pointer to the instance of the processor in the pipeline
  GecodeProcessor::SPtr pGecodeProcessor;

  /// Pointer to the result instance
  gecodeengine::GecodeResult::SPtr pResult;

  /// Pointer to the metrics register given to each work task processed
  /// by each processor
  MetricsRegister::SPtr pMetricsRegister;

  /// Loads the model from JSON to Model instance
  void loadModelImpl(const std::string& jsonModel);
};

}  // namespace optilab
