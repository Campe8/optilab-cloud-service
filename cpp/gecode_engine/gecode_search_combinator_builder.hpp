//
// Copyright OptiLab 2019. All rights reserved.
//
// Search combinator builder for Gecode CP solvers.
//

#pragma once

#include <cstddef>  // for std::size_t
#include <memory>   // for std::shared_ptr
#include <string>
#include <vector>

#include "gecode_engine/optilab_space.hpp"
#include "solver/cp_variable.hpp"
#include "solver/search_combinator_builder.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

/**
 * Internal state of the Gecode combinator builder.
 * The state can be reset at run time actually changing the behavior of the
 * combinator builder.
 */
struct SYS_EXPORT_STRUCT GecodeCombinatorBuilderState {
  enum CombinatorSelectionClass {
    CSC_BOOL = 0,
    CSC_INT,
    CSC_SET,
    CSC_FLOAT
  };

  CombinatorSelectionClass selectionClass{CombinatorSelectionClass::CSC_INT};
  std::size_t randomSeed{0};
  double decay{0.5};
};

class SYS_EXPORT_CLASS GecodeSearchCombinatorBuilder : public SearchCombinatorBuilder {
 public:
  using SPtr = std::shared_ptr<GecodeSearchCombinatorBuilder>;

 public:
  /// Constructor.
  /// @note the give solver is used to build all the combinator of this class.
  /// When one of the methods has one or more combinators as input, they must built using this
  /// combinator builder instance.
  /// @note throws std::invalid_argument if the given solver pointer is nullptr
  GecodeSearchCombinatorBuilder(OptilabSpace::SPtr space);

  ~GecodeSearchCombinatorBuilder() = default;

  /// Sets the state of the builder w.r.t. the search combinators it has to build
  inline void setCombinatorBuilderState(const GecodeCombinatorBuilderState& state)
  {
    pBuilderState = state;
  }

  /// Returns the const state of the builder
  inline const GecodeCombinatorBuilderState& getCombinatorBuilderState() const
  {
    return pBuilderState;
  }

  /// Returns the const state of the builder
  inline GecodeCombinatorBuilderState& getCombinatorBuilderState()
  {
    return pBuilderState;
  }

  std::string getDefaultVariableSelectionStrategy() const override;

  std::string getDefaultValueSelectionStrategy() const override;

  SearchCombinator::SPtr buildBaseSearchCombinator(
      const std::vector<CPVariable::SPtr>& variablesList, const std::string& varSelectionStrategy,
      const std::string& valSelectionStrategy) override;

  SearchCombinator::SPtr buildAndCombinator(
      const std::vector<SearchCombinator::SPtr>& combList) override;

  SearchCombinator::SPtr buildOrCombinator(
      const std::vector<SearchCombinator::SPtr>& combList) override;

 private:
  /// ORTools solver instance used to build combinators
  OptilabSpace::SPtr pSpace;

  /// State of the builder
  GecodeCombinatorBuilderState pBuilderState;
};

}  // namespace optilab
