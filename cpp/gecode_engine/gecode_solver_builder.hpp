//
// Copyright OptiLab 2019. All rights reserved.
//
// CPSolver builder for Gecode package.
//

#pragma once

#include <memory>   // for std::shared_ptr

#include "gecode_engine/gecode_solver.hpp"
#include "solver/cp_model.hpp"
#include "solver/cp_search.hpp"
#include "solver/cp_solver.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS GecodeSolverBuilder {
 public:
  using SPtr = std::shared_ptr<GecodeSolverBuilder>;

 public:
  /// Constructor.
  /// @note throws std::invalid_argument if the input argument is an empty pointer
  GecodeSolverBuilder(CPModel::SPtr cpModel);

  virtual ~GecodeSolverBuilder() = default;

  /// Builds a (Gecode) CPSolver instance running according to the model and
  /// search strategy provided by the builder
  CPSolver::SPtr build();

 private:
  /// Model to build the solver upon
  CPModel::SPtr pModel;

  /// Sets global search strategy on solver
  void setGlobalSearchOnSolver(const GecodeSolver::SPtr& solver,
                               const CPSearch::SPtr& searchStrategy);
};

}  // namespace optilab
