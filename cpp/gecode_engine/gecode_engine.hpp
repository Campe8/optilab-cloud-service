//
// Copyright OptiLab 2019. All rights reserved.
//
// Engine class for Gecode CP frameworks.
//

#pragma once

#include <atomic>
#include <cstdint>  // uint64_t
#include <memory>   // for std::shared_ptr
#include <string>

#include "data_structure/metrics/metrics_register.hpp"
#include "engine/engine.hpp"
#include "engine/engine_callback_handler.hpp"
#include "gecode_engine/gecode_optimizer.hpp"
#include "gecode_engine/gecode_utilities.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS GecodeEngine {
 public:
  using SPtr = std::shared_ptr<GecodeEngine>;

 public:
  GecodeEngine(const std::string& engineId, const EngineCallbackHandler::SPtr& handler);

  ~GecodeEngine();

  /// Registers the given model.
  /// @note this method does not run the model.
  /// @throw std::runtime_error if this model is called while the engine is running
  void registerModel(const std::string& model);

  /// Notifies the engine on a given EngineEvent
  void notifyEngine(const gecodeengine::GecodeEvent& event);

  /// Wait for the current jobs in the queue to finish.
  /// @note if "timeoutMsec" is -1, waits for all jobs in the queue to complete
  void engineWait(int timeoutMsec);

  /// Shuts down the engine
  void turnDown();

 private:
  /// Engine identifier
  std::string pEngineId;

  /// Handler for callbacks to send back results to caller methods
  EngineCallbackHandler::SPtr pCallbackHandler;

  /// Pointer to the result instance collecting result from the optimizer
  gecodeengine::GecodeCPResult::SPtr pCPResult;

  /// Latency in msec. to create this engine
  std::atomic<uint64_t> pEngineCreationTimeMsec{0};

  /// Latency in msec. caused by loading a model into the optimizer
  std::atomic<uint64_t> pLoadModelLatencyMsec{0};

  /// Latency in msec. caused by running the optimizer
  std::atomic<uint64_t> pOptimizerLatencyMsec{0};

  /// Register for metrics
  MetricsRegister::SPtr pMetricsRegister;

  /// Flag indicating whether this engine already started running
  std::atomic<bool> pActiveEngine{false};

  /// Asynchronous optimizer
  GecodeOptimizer::SPtr pOptimizer;

  // Builds the instance of a Gecode optimizer with a CP processor
  void buildOptimizer();

  /// Runs the registered model
  void processRunModelEvent();

  /// Sends back using the handler all the solutions collected so far
  void processCollectSolutionsEvent(int numSolutions);

  /// Kills the current computation, if any, and return
  void processInterruptEngineEvent();

  /// Collects metrics in the internal metrics register
  void collectMetrics();
};

}  // namespace optilab
