#include "gecode_engine/gecode_search_combinator.hpp"

#include <stdexcept>  // for std::invalid_argument

#include <spdlog/spdlog.h>

namespace optilab {

GecodeSearchCombinator::GecodeSearchCombinator(OptilabSpace::SPtr space)
: pSpace(space)
{
  if (!pSpace)
  {
    throw std::invalid_argument("GecodeSearchCombinator - empty OptiLabSpace pointer");
  }
}


GecodeBaseSearchCombinator::GecodeBaseSearchCombinator(
    OptilabSpace::SPtr space, const Gecode::IntVarArgs& varArgs,
    Gecode::TieBreak<Gecode::IntVarBranch> varBranch, Gecode::IntValBranch valBranch)
: GecodeSearchCombinator(space),
  pCombinatorClass(CombinatorClass::CLASS_INT),
  pIntVarArgs(varArgs),
  pIntVarBranch(varBranch),
  pIntValBranch(valBranch)
{
}

GecodeBaseSearchCombinator::GecodeBaseSearchCombinator(
    OptilabSpace::SPtr space, const Gecode::BoolVarArgs& varArgs,
    Gecode::TieBreak<Gecode::BoolVarBranch> varBranch, Gecode::BoolValBranch valBranch)
: GecodeSearchCombinator(space),
  pCombinatorClass(CombinatorClass::CLASS_BOOL),
  pBoolVarArgs(varArgs),
  pBoolVarBranch(varBranch),
  pBoolValBranch(valBranch)
{
}

#ifdef GECODE_HAS_SET_VARS
GecodeBaseSearchCombinator::GecodeBaseSearchCombinator(OptilabSpace::SPtr space,
                                                       const Gecode::SetVarArgs& varArgs,
                                                       Gecode::SetVarBranch varBranch,
                                                       Gecode::SetValBranch valBranch)
: GecodeSearchCombinator(space),
  pCombinatorClass(CombinatorClass::CLASS_SET),
  pSetVarArgs(varArgs),
  pSetVarBranch(varBranch),
  pSetValBranch(valBranch)
{
}
#endif

#ifdef GECODE_HAS_FLOAT_VARS
GecodeBaseSearchCombinator::GecodeBaseSearchCombinator(
    OptilabSpace::SPtr space, const Gecode::FloatVarArgs& varArgs,
    Gecode::TieBreak<Gecode::FloatVarBranch> varBranch, Gecode::FloatValBranch valBranch)
: GecodeSearchCombinator(space),
  pCombinatorClass(CombinatorClass::CLASS_FLOAT),
  pFloatVarArgs(varArgs),
  pFloatVarBranch(varBranch),
  pFloatValBranch(valBranch)
{
}
#endif

void GecodeBaseSearchCombinator::post()
{
  if (!getSpace())
  {
    const std::string errMsg = "GecodeBaseSearchCombinator - post: empty space";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  switch (pCombinatorClass)
  {
    case CombinatorClass::CLASS_INT:
      Gecode::branch(*getSpace(), pIntVarArgs, pIntVarBranch, pIntValBranch);
      break;
    case CombinatorClass::CLASS_BOOL:
      Gecode::branch(*getSpace(), pBoolVarArgs, pBoolVarBranch, pBoolValBranch);
      break;
#ifdef GECODE_HAS_SET_VARS
    case CombinatorClass::CLASS_SET:
      Gecode::branch(*getSpace(), pSetVarArgs, pSetVarBranch, pSetValBranch);
      break;
#endif
#ifdef GECODE_HAS_FLOAT_VARS
    case CombinatorClass::CLASS_FLOAT:
      Gecode::branch(*getSpace(), pFloatVarArgs, pFloatVarBranch, pFloatValBranch);
      break;
#endif
    default:
      throw std::runtime_error("GecodeBaseSearchCombinator - post: invalid combinator class");
  }
}  // post

GecodeCompositeSearchCombinator::GecodeCompositeSearchCombinator(
    OptilabSpace::SPtr space, const std::vector<GecodeSearchCombinator::SPtr>& combinatorList)
: GecodeSearchCombinator(space),
  pCombinatorList(combinatorList)
{
  for (const auto& ptr : pCombinatorList)
  {
    if (!ptr)
    {
      throw std::invalid_argument("GecodeCompositeSearchCombinator - empty pointer to a "
          "search combinator");
    }
  }
}

void GecodeCompositeSearchCombinator::post()
{
  // Recursively post the combinators in the list
  for (const auto& ptr : pCombinatorList)
  {
    ptr->post();
  }
}  // post

}  // namespace optilab
