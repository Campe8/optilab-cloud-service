#include "gecode_engine/gecode_utilities.hpp"

#include <stdexcept>  // for std::runtime_error
#include <vector>

#include "engine/engine_constants.hpp"
#include "engine/engine_utils.hpp"
#include "model/model_constants.hpp"
#include "model/model_object_intension_constraint.hpp"
#include "utilities/strings.hpp"
#include "utilities/variables.hpp"

namespace {

optilab::JSONValueArray::SPtr createSolutionDomain(
    const optilab::JSON::SPtr& json, const std::vector<std::vector<std::string>>& assign)
{
  auto domainJson = json->createArray();
  for (const auto& valuesList : assign)
  {
    auto valuesListJson = json->createArray();
    for (const auto& val : valuesList)
    {
      auto valJson = json->createValue(val);
      valuesListJson->pushBack(valJson);
    }
    domainJson->pushBack(valuesListJson);
  }

  return domainJson;
}  // createSolutionDomain

optilab::JSONValue::SPtr createSolution(
    const optilab::JSON::SPtr& json,
    const optilab::gecodeengine::GecodeCPResult::Solution& solution)
{
  auto solutionJson = json->createValue();
  if (!solution.first.empty())
  {
    auto solutionDescriptorJson = json->createValue(solution.first);
    solutionJson->add(optilab::jsonsolution::SOLUTION_DESCRIPTOR, solutionDescriptorJson);
  }

  auto solutionAssignJson = json->createArray();
  for (const auto& varMapIter : solution.second)
  {
    auto varAssign = json->createValue();

    // Set variable id
    auto varId = json->createValue(varMapIter.first);
    varAssign->add(optilab::jsonsolution::VAR_IDENTIFIER, varId);

    // Create domain bounds
    auto domainJson =
        createSolutionDomain(json, varMapIter.second.second);
    varAssign->add(optilab::jsonsolution::VAR_DOMAIN_LIST, domainJson);

    // Push the current variable assignment in the solution list of variable assignments
    solutionAssignJson->pushBack(varAssign);
  }

  solutionJson->add(optilab::jsonsolution::SOLUTION_ASSIGNMENT_LIST, solutionAssignJson);
  return solutionJson;
}  // createSolution

optilab::JSONValueArray::SPtr createSolutionArray(
    const optilab::JSON::SPtr& json,
    const std::vector<optilab::gecodeengine::GecodeCPResult::Solution>& solutionCollection,
    int numSolutions)
{
  auto solutionListJson = json->createArray();
  for (int solIdx = 0; solIdx < numSolutions; ++solIdx)
  {
    solutionListJson->pushBack(createSolution(json, solutionCollection[solIdx]));
  }

  return solutionListJson;
}  // createSolutionArray

optilab::CPConstraint::PropagationType getConstraintPropagationTypeFromString(
    const std::string& propType)
{
  if (propType == optilab::jsonmodel::CONSTRAINT_PROPAGATION_TYPE_DEFAULT)
  {
    return optilab::CPConstraint::PropagationType::PT_DEFAULT;
  }
  else if (propType == optilab::jsonmodel::CONSTRAINT_PROPAGATION_TYPE_BOUNDS)
  {
    return optilab::CPConstraint::PropagationType::PT_BOUNDS;
  }
  else if (propType == optilab::jsonmodel::CONSTRAINT_PROPAGATION_TYPE_DOMAIN)
  {
    return optilab::CPConstraint::PropagationType::PT_DOMAIN;
  }
  else
  {
    throw std::runtime_error("Constraint propagation type not recognized: " + propType);
  }
}  // getConstraintPropagationTypeFromString

optilab::CPArgument getConstraintArgumentFromArgList(const std::vector<std::string>& args,
                                                     optilab::CPModel* model)
{
  if (args.empty())
  {
    throw std::runtime_error("Empty list of arguments");
  }

  // Create the argument according to the type of the (first) element in the vector
  // with the exception that one single double type cast all the other elements to double
  bool isDouble = false;
  bool isInt = false;
  bool isBool = false;
  for (const auto& arg : args)
  {
    if (optilab::utilsstrings::isDouble(arg))
    {
      isDouble = true;
    }
    if (optilab::utilsstrings::isInt(arg))
    {
      isInt = true;
    }
    if (optilab::utilsstrings::isBool(arg))
    {
      isBool = true;
    }
  }

  // Create the argument
  optilab::CPArgument cpArg;

  // At least one string has the following syntax: <num>.<num>
  if (isDouble && !isInt)
  {
    std::vector<double> doubleArgs;
    for (const auto& arg : args)
    {
      doubleArgs.push_back(optilab::utilsstrings::stringToDouble(arg));
    }
    cpArg = optilab::CPArgument::buildDoubleValueArrayArg(doubleArgs);
  }
  else if ((isInt || isBool) && !isDouble)
  {
    // All arguments are int (or Boolean)
    std::vector<int64_t> intArgs;
    for (const auto& arg : args)
    {
      intArgs.push_back(optilab::utilsstrings::stringToInt(arg));
    }
    cpArg = optilab::CPArgument::buildIntValueArrayArg(intArgs);
  }
  else
  {
    // All arguments are variables
    std::vector<optilab::CPVariable::SPtr> varArgs;
    const auto& varMap = model->getVariablesMap();
    for (const auto& arg : args)
    {
      // Var id can be either:
      // - id, for example "x"
      // - subscript, for example "x[1, 2]"
      auto idAndIdx = optilab::utilsvariables::extractVariableIdAndIdxFromString(arg);
      auto it = varMap.find(idAndIdx.first);
      if (it == varMap.end())
      {
        throw std::runtime_error("Constraint argument (variable) not found in the model: " +
                                 idAndIdx.first);
      }

      auto var = it->second;
      if (idAndIdx.second.empty())
      {
        varArgs.push_back(var);
      }
      else
      {
        // Get the variable at the specified index
        if (var->isScalar())
        {
          throw std::runtime_error("Constraint argument: trying to use "
              "the subscript operator on a scalar variable");
        }
        varArgs.push_back(var->getSubVariable(idAndIdx.second));
      }
      cpArg = optilab::CPArgument::buildVarRefArrayArg(varArgs);
    }
  }

  // Return the argument
  return cpArg;
}  // getConstraintArgumentFromArgList

std::vector<optilab::CPArgument> getConstraintArgumentsFromIntensionConstraint(
    optilab::ModelObjectIntensionConstraint* intCon, optilab::CPModel* model)
{
  std::vector<optilab::CPArgument> argList;

  for (const auto& argStrList : intCon->getArgumentsList())
  {
    argList.push_back(getConstraintArgumentFromArgList(argStrList, model));
  }

  return argList;
}  // getConstraintArgumentsFromIntensionConstraint

optilab::GecodeConstraint::SPtr getIntensionCPCon(
    optilab::ModelObjectIntensionConstraint* intCon, const optilab::OptilabSpace::SPtr& space,
    optilab::CPModel* model)
{
  auto conTypeName = intCon->getType() + "_" + intCon->getName();
  auto propType = getConstraintPropagationTypeFromString(intCon->getPropagationType());
  auto argumentsList = getConstraintArgumentsFromIntensionConstraint(intCon, model);


  return std::make_shared<optilab::GecodeConstraint>(conTypeName, argumentsList, propType,
                                                     space);
}  // getIntensionCPCon

}  // namespace

namespace optilab {

namespace gecodesutils {

JSON::SPtr buildSolutionJson(const gecodeengine::GecodeCPResult::SPtr& res, int numSolutions)
{
  if (!res)
  {
    throw std::runtime_error("buildSolutionJson - empty solution");
  }

  JSON::SPtr json = std::make_shared<JSON>();

  // Check for the number of solutions to send back to the caller
  const auto& modelName = res->modelName;
  const auto numResSol = res->getNumSolutions();
  if (numResSol == 0)
  {
    // Handle no solutions found
    if (res->isSatModel)
    {
      // No solutions but satisfiable model (e.g., a model without branching variables)
      json = engineutils::createCPSatSolutionJson(modelName);
    }
    else if (!res->isSatModel)
    {
      // No solutions and an unsatisfiable model
      json = engineutils::createCPUnsatSolutionJson(modelName);
    }
    else
    {
      // The engine did not run
      throw std::runtime_error("The engine did not run");
    }
    return json;
  }

  if (numResSol < numSolutions)
  {
    numSolutions = static_cast<int>(numResSol);
  }

  if (numResSol < numSolutions)
  {
    numSolutions = static_cast<int>(numResSol);
  }

  auto modelId = json->createValue(modelName);
  auto numSol = json->createValue(numSolutions);
  auto status = json->createValue(std::string(jsonsolution::STATUS_SATISFIED));
  json->add(jsonsolution::MODEL_ID, modelId);
  json->add(jsonsolution::NUM_SOLUTIONS, numSol);
  json->add(jsonsolution::MODEL_STATUS, status);

  // Set the list of solutions
  const auto& solutionArray = createSolutionArray(json, res->solutionList, numSolutions);
  json->add(jsonsolution::SOLUTION_LIST, solutionArray);

  return json;
}  // buildSolutionJson

optilab::GecodeVariable::SPtr getCPVar(const optilab::ModelObjectVariable::SPtr& varPtr,
                                       const OptilabSpace::SPtr& space)
{
  // Get the name of the variable
  const std::string& varName = varPtr->getVariableName();

  // Get the domain from the model object variable
  auto domain = utilsvariables::buildCPDomainFromObjectVariable(varPtr);

  // Create the variable
  if (varPtr->isScalar())
  {
    return std::make_shared<GecodeVariable>(varName, domain, space);
  }
  else
  {
    const int numDim = static_cast<int>(varPtr->numDimensions());
    if (numDim == 1)
    {
      return std::make_shared<GecodeVariable>(varName, domain, varPtr->getDimension(0), space);
    }
    else
    {
      std::vector<int> dims;
      for (int dim = 0; dim < numDim; ++dim)
      {
        dims.push_back(varPtr->getDimension(dim));
      }
      return std::make_shared<GecodeVariable>(varName, domain, dims, space);
    }
  }
}  // getCPVar

Gecode::TieBreak<Gecode::IntVarBranch> getIntVarBranchFromString(const std::string& varBranch,
                                                                 Gecode::Rnd& rnd, double decay)
{
  if (varBranch == gecodeconstants::INT_VAR_ACTION_MAX)
    return Gecode::TieBreak<Gecode::IntVarBranch>(Gecode::INT_VAR_ACTION_MAX(decay));
  else if (varBranch == gecodeconstants::INT_VAR_ACTION_MIN)
    return Gecode::TieBreak<Gecode::IntVarBranch>(Gecode::INT_VAR_ACTION_MIN(decay));
  else if (varBranch == gecodeconstants::INT_VAR_ACTION_SIZE_MAX)
    return Gecode::TieBreak<Gecode::IntVarBranch>(Gecode::INT_VAR_ACTION_SIZE_MAX(decay));
  else if (varBranch == gecodeconstants::INT_VAR_ACTION_SIZE_MIN)
    return Gecode::TieBreak<Gecode::IntVarBranch>(Gecode::INT_VAR_ACTION_SIZE_MIN(decay));
  else if (varBranch == gecodeconstants::INT_VAR_AFC_MAX)
    return Gecode::TieBreak<Gecode::IntVarBranch>(Gecode::INT_VAR_AFC_MAX(decay));
  else if (varBranch == gecodeconstants::INT_VAR_AFC_MIN)
    return Gecode::TieBreak<Gecode::IntVarBranch>(Gecode::INT_VAR_AFC_MIN(decay));
  else if (varBranch == gecodeconstants::INT_VAR_AFC_SIZE_MAX)
    return Gecode::TieBreak<Gecode::IntVarBranch>(Gecode::INT_VAR_AFC_SIZE_MAX(decay));
  else if (varBranch == gecodeconstants::INT_VAR_AFC_SIZE_MIN)
    return Gecode::TieBreak<Gecode::IntVarBranch>(Gecode::INT_VAR_AFC_SIZE_MIN(decay));
  else if (varBranch == gecodeconstants::INT_VAR_DEGREE_MAX)
    return Gecode::TieBreak<Gecode::IntVarBranch>(Gecode::INT_VAR_DEGREE_MAX());
  else if (varBranch == gecodeconstants::INT_VAR_DEGREE_MIN)
    return Gecode::TieBreak<Gecode::IntVarBranch>(Gecode::INT_VAR_DEGREE_MIN());
  else if (varBranch == gecodeconstants::INT_VAR_DEGREE_SIZE_MAX)
    return Gecode::TieBreak<Gecode::IntVarBranch>(Gecode::INT_VAR_DEGREE_SIZE_MAX());
  else if (varBranch == gecodeconstants::INT_VAR_DEGREE_SIZE_MIN)
    return Gecode::TieBreak<Gecode::IntVarBranch>(Gecode::INT_VAR_DEGREE_SIZE_MIN());
  else if (varBranch == gecodeconstants::INT_VAR_MAX_MAX)
    return Gecode::TieBreak<Gecode::IntVarBranch>(Gecode::INT_VAR_MAX_MAX());
  else if (varBranch == gecodeconstants::INT_VAR_MAX_MIN)
    return Gecode::TieBreak<Gecode::IntVarBranch>(Gecode::INT_VAR_MAX_MIN());
  else if (varBranch == gecodeconstants::INT_VAR_MIN_MAX)
    return Gecode::TieBreak<Gecode::IntVarBranch>(Gecode::INT_VAR_MIN_MAX());
  else if (varBranch == gecodeconstants::INT_VAR_MIN_MIN)
    return Gecode::TieBreak<Gecode::IntVarBranch>(Gecode::INT_VAR_MIN_MIN());
  else if (varBranch == gecodeconstants::INT_VAR_NONE)
    return Gecode::TieBreak<Gecode::IntVarBranch>(Gecode::INT_VAR_NONE());
  else if (varBranch == gecodeconstants::INT_VAR_REGRET_MAX_MAX)
    return Gecode::TieBreak<Gecode::IntVarBranch>(Gecode::INT_VAR_REGRET_MAX_MAX());
  else if (varBranch == gecodeconstants::INT_VAR_REGRET_MAX_MIN)
    return Gecode::TieBreak<Gecode::IntVarBranch>(Gecode::INT_VAR_REGRET_MAX_MIN());
  else if (varBranch == gecodeconstants::INT_VAR_REGRET_MIN_MAX)
    return Gecode::TieBreak<Gecode::IntVarBranch>(Gecode::INT_VAR_REGRET_MIN_MAX());
  else if (varBranch == gecodeconstants::INT_VAR_REGRET_MIN_MIN)
    return Gecode::TieBreak<Gecode::IntVarBranch>(Gecode::INT_VAR_REGRET_MIN_MIN());
  else if (varBranch == gecodeconstants::INT_VAR_RND)
    return Gecode::TieBreak<Gecode::IntVarBranch>(Gecode::INT_VAR_RND(rnd));
  else if (varBranch == gecodeconstants::INT_VAR_SIZE_MAX)
    return Gecode::TieBreak<Gecode::IntVarBranch>(Gecode::INT_VAR_SIZE_MAX());
  else if (varBranch == gecodeconstants::INT_VAR_SIZE_MIN)
    return Gecode::TieBreak<Gecode::IntVarBranch>(Gecode::INT_VAR_SIZE_MIN());
  throw std::runtime_error("getIntVarBranchFromString - int variable branching strategy "
      "not supported: " + varBranch);
}  // getIntVarBranchFromString

Gecode::IntValBranch getIntValBranchFromString(const std::string& valBranch, Gecode::Rnd& rnd)
{
  if (valBranch == gecodeconstants::INT_VAL_MAX)
    return Gecode::INT_VAL_MAX();
  else if (valBranch == gecodeconstants::INT_VAL_MED)
    return Gecode::INT_VAL_MED();
  else if (valBranch == gecodeconstants::INT_VAL_MIN)
    return Gecode::INT_VAL_MIN();
  else if (valBranch == gecodeconstants::INT_VAL_RANGE_MAX)
    return Gecode::INT_VAL_RANGE_MAX();
  else if (valBranch == gecodeconstants::INT_VAL_RANGE_MIN)
    return Gecode::INT_VAL_RANGE_MIN();
  else if (valBranch == gecodeconstants::INT_VAL_RND)
    return Gecode::INT_VAL_RND(rnd);
  else if (valBranch == gecodeconstants::INT_VAL_SPLIT_MAX)
    return Gecode::INT_VAL_SPLIT_MAX();
  else if (valBranch == gecodeconstants::INT_VAL_SPLIT_MIN)
    return Gecode::INT_VAL_SPLIT_MIN();
  else if (valBranch == gecodeconstants::INT_VALUES_MAX)
    return Gecode::INT_VALUES_MAX();
  else if (valBranch == gecodeconstants::INT_VALUES_MIN)
    return Gecode::INT_VALUES_MIN();
  throw std::runtime_error("getIntValBranchFromString - int value branching strategy "
      "not supported: " + valBranch);
}  // getIntValBranchFromString

Gecode::TieBreak<Gecode::BoolVarBranch> getBoolVarBranchFromString(const std::string& varBranch,
                                                                   Gecode::Rnd& rnd, double decay)
{
  if (varBranch == gecodeconstants::BOOL_VAR_ACTION_MAX)
    return Gecode::TieBreak<Gecode::BoolVarBranch>(Gecode::BOOL_VAR_ACTION_MAX(decay));
  else if (varBranch == gecodeconstants::BOOL_VAR_ACTION_MIN)
    return Gecode::TieBreak<Gecode::BoolVarBranch>(Gecode::BOOL_VAR_ACTION_MIN(decay));
  else if (varBranch == gecodeconstants::BOOL_VAR_AFC_MAX)
    return Gecode::TieBreak<Gecode::BoolVarBranch>(Gecode::BOOL_VAR_AFC_MAX(decay));
  else if (varBranch == gecodeconstants::BOOL_VAR_AFC_MIN)
    return Gecode::TieBreak<Gecode::BoolVarBranch>(Gecode::BOOL_VAR_AFC_MIN(decay));
  else if (varBranch == gecodeconstants::BOOL_VAR_DEGREE_MAX)
    return Gecode::TieBreak<Gecode::BoolVarBranch>(Gecode::BOOL_VAR_DEGREE_MAX());
  else if (varBranch == gecodeconstants::BOOL_VAR_DEGREE_MIN)
    return Gecode::TieBreak<Gecode::BoolVarBranch>(Gecode::BOOL_VAR_DEGREE_MIN());
  else if (varBranch == gecodeconstants::BOOL_VAR_NONE)
    return Gecode::TieBreak<Gecode::BoolVarBranch>(Gecode::BOOL_VAR_NONE());
  else if (varBranch == gecodeconstants::BOOL_VAR_RND)
    return Gecode::TieBreak<Gecode::BoolVarBranch>(Gecode::BOOL_VAR_RND(rnd));
  throw std::runtime_error("getBoolVarBranchFromString - bool variable branching strategy "
      "not supported: " + varBranch);
}  // getBoolVarBranchFromString

Gecode::BoolValBranch getBoolValBranchFromString(const std::string& valBranch, Gecode::Rnd& rnd)
{
  if (valBranch == gecodeconstants::BOOL_VAL_MAX)
    return Gecode::BOOL_VAL_MAX();
  else if (valBranch == gecodeconstants::BOOL_VAL_MIN)
    return Gecode::BOOL_VAL_MIN();
  else if (valBranch == gecodeconstants::BOOL_VAL_RND)
    return Gecode::BOOL_VAL_RND(rnd);
  throw std::runtime_error("getBoolValBranchFromString - bool value branching strategy "
      "not supported: " + valBranch);
}  // getIntValBranchFromString

#ifdef GECODE_HAS_SET_VARS
Gecode::SetVarBranch getSetVarBranchFromString(const std::string& varBranch, Gecode::Rnd& rnd,
                                               double decay)
{
  if (varBranch == gecodeconstants::SET_VAR_ACTION_MAX)
    return Gecode::SET_VAR_ACTION_MAX(decay);
  else if (varBranch == gecodeconstants::SET_VAR_ACTION_MIN)
    return Gecode::SET_VAR_ACTION_MIN(decay);
  else if (varBranch == gecodeconstants::SET_VAR_ACTION_SIZE_MAX)
    return Gecode::SET_VAR_ACTION_SIZE_MAX(decay);
  else if (varBranch == gecodeconstants::SET_VAR_ACTION_SIZE_MIN)
    return Gecode::SET_VAR_ACTION_SIZE_MIN(decay);
  else if (varBranch == gecodeconstants::SET_VAR_AFC_MAX)
    return Gecode::SET_VAR_AFC_MAX(decay);
  else if (varBranch == gecodeconstants::SET_VAR_AFC_MIN)
    return Gecode::SET_VAR_AFC_MIN(decay);
  else if (varBranch == gecodeconstants::SET_VAR_AFC_SIZE_MAX)
    return Gecode::SET_VAR_AFC_SIZE_MAX(decay);
  else if (varBranch == gecodeconstants::SET_VAR_AFC_SIZE_MIN)
    return Gecode::SET_VAR_AFC_SIZE_MIN(decay);
  else if (varBranch == gecodeconstants::SET_VAR_DEGREE_MAX)
    return Gecode::SET_VAR_DEGREE_MAX();
  else if (varBranch == gecodeconstants::SET_VAR_DEGREE_MIN)
    return Gecode::SET_VAR_DEGREE_MIN();
  else if (varBranch == gecodeconstants::SET_VAR_DEGREE_SIZE_MAX)
    return Gecode::SET_VAR_DEGREE_SIZE_MAX();
  else if (varBranch == gecodeconstants::SET_VAR_DEGREE_SIZE_MIN)
    return Gecode::SET_VAR_DEGREE_SIZE_MIN();
  else if (varBranch == gecodeconstants::SET_VAR_MAX_MAX)
    return Gecode::SET_VAR_MAX_MAX();
  else if (varBranch == gecodeconstants::SET_VAR_MAX_MIN)
    return Gecode::SET_VAR_MAX_MIN();
  else if (varBranch == gecodeconstants::SET_VAR_MIN_MAX)
    return Gecode::SET_VAR_MIN_MAX();
  else if (varBranch == gecodeconstants::SET_VAR_MIN_MIN)
    return Gecode::SET_VAR_MIN_MIN();
  else if (varBranch == gecodeconstants::SET_VAR_NONE)
    return Gecode::SET_VAR_NONE();
  else if (varBranch == gecodeconstants::SET_VAR_RND)
    return Gecode::SET_VAR_RND(rnd);
  else if (varBranch == gecodeconstants::SET_VAR_SIZE_MAX)
    return Gecode::SET_VAR_SIZE_MAX();
  else if (varBranch == gecodeconstants::SET_VAR_SIZE_MIN)
    return Gecode::SET_VAR_SIZE_MIN();
  throw std::runtime_error("getSetVarBranchFromString - set value branching strategy "
      "not supported: " + varBranch);
}  // getSetVarBranchFromString

Gecode::SetValBranch getSetValBranchFromString(const std::string& valBranch, Gecode::Rnd& rnd)
{
  if (valBranch == gecodeconstants::SET_VAL_MAX_EXC)
    return Gecode::SET_VAL_MAX_EXC();
  else if (valBranch == gecodeconstants::SET_VAL_MAX_INC)
    return Gecode::SET_VAL_MAX_INC();
  else if (valBranch == gecodeconstants::SET_VAL_MED_EXC)
    return Gecode::SET_VAL_MED_EXC();
  else if (valBranch == gecodeconstants::SET_VAL_MED_INC)
    return Gecode::SET_VAL_MED_INC();
  else if (valBranch == gecodeconstants::SET_VAL_MIN_EXC)
    return Gecode::SET_VAL_MIN_EXC();
  else if (valBranch == gecodeconstants::SET_VAL_MIN_INC)
    return Gecode::SET_VAL_MIN_INC();
  else if (valBranch == gecodeconstants::SET_VAL_RND_EXC)
    return Gecode::SET_VAL_RND_EXC(rnd);
  else if (valBranch == gecodeconstants::SET_VAL_RND_INC)
    return Gecode::SET_VAL_RND_INC(rnd);
  throw std::runtime_error("getSetValBranchFromString - set value branching strategy "
      "not supported: " + valBranch);
}  // getSetValBranchFromString
#endif

#ifdef GECODE_HAS_FLOAT_VARS
Gecode::TieBreak<Gecode::FloatVarBranch> getFloatVarBranchFromString(const std::string& varBranch,
                                                                     Gecode::Rnd& rnd, double decay)
{
  if (varBranch == gecodeconstants::FLOAT_VAR_ACTION_MAX)
    return Gecode::TieBreak<Gecode::FloatVarBranch>(Gecode::FLOAT_VAR_ACTION_MAX(decay));
  else if (varBranch == gecodeconstants::FLOAT_VAR_ACTION_MIN)
    return Gecode::TieBreak<Gecode::FloatVarBranch>(Gecode::FLOAT_VAR_ACTION_MIN(decay));
  else if (varBranch == gecodeconstants::FLOAT_VAR_ACTION_SIZE_MAX)
    return Gecode::TieBreak<Gecode::FloatVarBranch>(Gecode::FLOAT_VAR_ACTION_SIZE_MAX(decay));
  else if (varBranch == gecodeconstants::FLOAT_VAR_ACTION_SIZE_MIN)
    return Gecode::TieBreak<Gecode::FloatVarBranch>(Gecode::FLOAT_VAR_ACTION_SIZE_MIN(decay));
  else if (varBranch == gecodeconstants::FLOAT_VAR_AFC_MAX)
    return Gecode::TieBreak<Gecode::FloatVarBranch>(Gecode::FLOAT_VAR_AFC_MAX(decay));
  else if (varBranch == gecodeconstants::FLOAT_VAR_AFC_MIN)
    return Gecode::TieBreak<Gecode::FloatVarBranch>(Gecode::FLOAT_VAR_AFC_MIN(decay));
  else if (varBranch == gecodeconstants::FLOAT_VAR_AFC_SIZE_MAX)
    return Gecode::TieBreak<Gecode::FloatVarBranch>(Gecode::FLOAT_VAR_AFC_SIZE_MAX(decay));
  else if (varBranch == gecodeconstants::FLOAT_VAR_AFC_SIZE_MIN)
    return Gecode::TieBreak<Gecode::FloatVarBranch>(Gecode::FLOAT_VAR_AFC_SIZE_MIN(decay));
  else if (varBranch == gecodeconstants::FLOAT_VAR_DEGREE_MAX)
    return Gecode::TieBreak<Gecode::FloatVarBranch>(Gecode::FLOAT_VAR_DEGREE_MAX());
  else if (varBranch == gecodeconstants::FLOAT_VAR_DEGREE_MIN)
    return Gecode::TieBreak<Gecode::FloatVarBranch>(Gecode::FLOAT_VAR_DEGREE_MIN());
  else if (varBranch == gecodeconstants::FLOAT_VAR_DEGREE_SIZE_MAX)
    return Gecode::TieBreak<Gecode::FloatVarBranch>(Gecode::FLOAT_VAR_DEGREE_SIZE_MAX());
  else if (varBranch == gecodeconstants::FLOAT_VAR_DEGREE_SIZE_MIN)
    return Gecode::TieBreak<Gecode::FloatVarBranch>(Gecode::FLOAT_VAR_DEGREE_SIZE_MIN());
  else if (varBranch == gecodeconstants::FLOAT_VAR_MAX_MAX)
    return Gecode::TieBreak<Gecode::FloatVarBranch>(Gecode::FLOAT_VAR_MAX_MAX());
  else if (varBranch == gecodeconstants::FLOAT_VAR_MAX_MIN)
    return Gecode::TieBreak<Gecode::FloatVarBranch>(Gecode::FLOAT_VAR_MAX_MIN());
  else if (varBranch == gecodeconstants::FLOAT_VAR_MIN_MAX)
    return Gecode::TieBreak<Gecode::FloatVarBranch>(Gecode::FLOAT_VAR_MIN_MAX());
  else if (varBranch == gecodeconstants::FLOAT_VAR_MIN_MIN)
    return Gecode::TieBreak<Gecode::FloatVarBranch>(Gecode::FLOAT_VAR_MIN_MIN());
  else if (varBranch == gecodeconstants::FLOAT_VAR_NONE)
    return Gecode::TieBreak<Gecode::FloatVarBranch>(Gecode::FLOAT_VAR_NONE());
  else if (varBranch == gecodeconstants::FLOAT_VAR_RND)
    return Gecode::TieBreak<Gecode::FloatVarBranch>(Gecode::FLOAT_VAR_RND(rnd));
  else if (varBranch == gecodeconstants::FLOAT_VAR_SIZE_MAX)
    return Gecode::TieBreak<Gecode::FloatVarBranch>(Gecode::FLOAT_VAR_SIZE_MAX());
  else if (varBranch == gecodeconstants::FLOAT_VAR_SIZE_MIN)
    return Gecode::TieBreak<Gecode::FloatVarBranch>(Gecode::FLOAT_VAR_SIZE_MIN());
  throw std::runtime_error("getFloatVarBranchFromString - float variable branching strategy "
      "not supported: " + varBranch);
}  // getIntVarBranchFromString

Gecode::FloatValBranch getFloatValBranchFromString(const std::string& valBranch, Gecode::Rnd& rnd)
{
  if (valBranch == gecodeconstants::FLOAT_VAL_SPLIT_MAX)
    return Gecode::FLOAT_VAL_SPLIT_MAX();
  else if (valBranch == gecodeconstants::FLOAT_VAL_SPLIT_MIN)
    return Gecode::FLOAT_VAL_SPLIT_MIN();
  else if (valBranch == gecodeconstants::FLOAT_VAL_SPLIT_RND)
    return Gecode::FLOAT_VAL_SPLIT_RND(rnd);
  throw std::runtime_error("getFloatValBranchFromString - float value branching strategy "
      "not supported: " + valBranch);
}  // getIntValBranchFromString
#endif

optilab::GecodeConstraint::SPtr getCPCon(const optilab::ModelObjectConstraint::SPtr& conPtr,
                                         const OptilabSpace::SPtr& space, CPModel* model)
{
  auto conSemantic = conPtr->getSemantic();
  if (conSemantic == optilab::ModelObjectConstraint::ConstraintSemantic::CS_INTENSION)
  {
    auto intConst = static_cast<optilab::ModelObjectIntensionConstraint*>(conPtr.get());
    return getIntensionCPCon(intConst, space, model);
  }
  throw std::runtime_error("Gecode constraint semantic not supported");
}  // getCPCon


}  // namespace gecodesutils

}  // namespace optilab
