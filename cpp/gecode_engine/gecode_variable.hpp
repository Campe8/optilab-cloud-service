//
// Copyright OptiLab 2019. All rights reserved.
//
// Class encapsulating a Gecode CP variable
//

#pragma once

#include <cstddef>  // for std::size_t
#include <memory>   // for std::shared_ptr
#include <vector>

#include "gecode_engine/optilab_space.hpp"
#include "solver/cp_variable.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_STRUCT GecodeVariable : public CPVariable {
 public:
  /// Type (domain) of this variable
  enum GecodeVarType {
    GVT_INT = 0,
    GVT_BOOL,
    GVT_SET,
    GVT_FLOAT,

    /// Undefined variable types are used when the type of the variable
    /// if not explicitely set in the model, e.g., for FlatZinc models
    GVT_UNDEF
  };

  using SPtr = std::shared_ptr<GecodeVariable>;

 public:
  /// Creates a scalar variable with given name and domain.
  /// @note throws std::invalid_argument if the pointer to the OptilabSpace is empty
  GecodeVariable(const std::string& name, const CPDomain& domain, const OptilabSpace::SPtr& space);

  /// Creates an array of variables of given size and with given name and domain.
  /// @note throws std::invalid_argument if the pointer to the OptilabSpace is empty
  GecodeVariable(const std::string& name, const CPDomain& domain, int len,
                 const OptilabSpace::SPtr& space);

  /// Creates an n-dimensional matrix of variables of given dimensions, with given name and domain
  /// @note dims size must be at least two.
  /// @note throws std::invalid_argument if the pointer to the OptilabSpace is empty
  GecodeVariable(const std::string& name, const CPDomain& domain, const std::vector<int>& dims,
                 const OptilabSpace::SPtr& space);

  /// Returns the type of this variable
  inline GecodeVarType getVarType() const { return pVarType; }

  /// Utility function: returns true if the type of the variable is Boolean
  inline bool isVarBool() const { return pVarType == GecodeVarType::GVT_BOOL; }
  /// Utility function: returns true if the type of the variable is integer
  inline bool isVarInt() const { return pVarType == GecodeVarType::GVT_INT; }
  /// Utility function: returns true if the type of the variable is float
  inline bool isVarFloat() const { return pVarType == GecodeVarType::GVT_FLOAT; }
  /// Utility function: returns true if the type of the variable is set
  inline bool isVarSet() const { return pVarType == GecodeVarType::GVT_SET; }

  CPVariable::SPtr getSubVariable(const std::vector<int>& subscr) override;

  /// Returns the list of indexing in the space where the corresponding Gecode variables
  /// are stored
  const std::vector<std::size_t>& getVarSpaceIdxList() const { return pVariableIndexList; }

 protected:
  /// Default constructor
  GecodeVariable(const std::string& name, GecodeVarType varType,
                 const std::vector<std::size_t>& indexList, const OptilabSpace::SPtr& space);

 private:
  /// Type of this variable
  GecodeVarType pVarType;

  /// Pointer to the Gecode/OptiLab used to create this instance of variable
  OptilabSpace::SPtr pSpace;

  /// List of the indices in the space for the variables encapsulated by this GecodeVariable
  std::vector<std::size_t> pVariableIndexList;

  /// Utility function: returns the type of this variable given the domain
  GecodeVarType getVarTypeFromDomain(const CPDomain& domain);

  /// Utility function: builds a variable with given domain and adds it to the space
  /// of this GecodeVariable.
  /// Returns the index of the variable in the space
  std::size_t buildVariableAndAddItToSpace(const CPDomain& domain);
};

}  // namespace optilab
