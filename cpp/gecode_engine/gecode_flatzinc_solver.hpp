//
// Copyright OptiLab 2019. All rights reserved.
//
// CPSolver for solving FlatZinc models within the Gecode package.
//

#pragma once

#include <memory>   // for std::shared_ptr
#include <ostream>
#include <string>
#include <unordered_map>
#include <utility>  // for std::pair
#include <vector>

#include <gecode/flatzinc.hh>
#include <gecode/search.hh>

#include "gecode_engine/gecode_cp_model.hpp"
#include "gecode_engine/gecode_search_combinator.hpp"
#include "gecode_engine/gecode_solver_impl.hpp"
#include "solver/cp_solver.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS GecodeFlatZincSolver : public CPSolver {
 public:
  /// A FlatZinc solution is a pair <a, b>, where:
  /// - a is the full FlatZinc solution as printed by the Gecode engine FlatZinc space;
  /// - b is the map of
  ///   key = variable name;
  ///   value = vector of values (one value for scalar variables)
  using VarSolution = std::vector<std::string>;
  using Solution = std::pair<std::string, std::unordered_map<std::string, VarSolution>>;

  using SPtr = std::shared_ptr<GecodeFlatZincSolver>;

 public:
  /// Constructor.
  /// @note throws std::invalid_argument on empty argument pointer
  explicit GecodeFlatZincSolver(const GecodeCPModel::SPtr& cpModel);

  virtual ~GecodeFlatZincSolver() = default;

  /// Initializes the solver for the search
  void initSearch() override;

  /// Performs constraint propagation only.
  /// Returns true if the current variable assignment satisfies all constraints.
  /// Returns false otherwise
  bool constraintPropagation() override;

  /// Runs the solver until a solution (next solution) is found.
  /// Returns true when there is a "next" solution, returns false otherwise.
  /// This method is supposed to be called within a loop, for example:
  /// while (nextSolution())
  /// {
  ///    use solution;
  /// }
  /// @note this method DESTROYS the current solution (if any) and replaces it with next solution.
  /// To get the pointer to the current solution, use the method "getCurrentSolution(...)"
  bool nextSolution() override;

  /// Stops the (ongoing) search process and return
  void interruptSearchProcess() override;

  /// Returns the pointer to the copy of the space holding the current solution
  inline const Solution& getSolution() const { return pSolution; }

 private:
  /// Flag indicating whether or not the solver has already run on the model given
  /// as input argument to the constructor
  bool pRunComplete{false};

  /// Solution reported as a string
  Solution pSolution;

  /// Stream used to collect error messages
  std::ostringstream pErrStream;

  /// Space of the model
  std::shared_ptr<Gecode::FlatZinc::FlatZincSpace> pSpace;

  /// FlatZinc space environment
  std::shared_ptr<FlatzincModelEnvironment> pFznEnvPtr;

  /// Sets the state of the solver
  void setSolverState(const std::string& sol);

  /// Parses and stores the input string as FlatZinc solution
  void parseAndStoreSolution(std::string& sol);
};

}  // namespace optilab
