#include "gecode_engine/gecode_cp_model.hpp"

#include <cassert>
#include <fstream>
#include <iostream>
#include <stdexcept>  // for std::invalid_argument
#include <sstream>

#include <spdlog/spdlog.h>

#include "model/model_object_variable.hpp"
#include "gecode_engine/gecode_constraint_posters.hpp"
#include "gecode_engine/gecode_utilities.hpp"
#include "utilities/file_sys.hpp"

namespace optilab {

OptilabModelEnvironment::OptilabModelEnvironment(OptilabSpace::SPtr space)
: pSpace(space)
{
  if (!pSpace)
  {
    throw std::invalid_argument("OptilabModelEnvironment - empty space");
  }
}

bool OptilabModelEnvironment::isa(const GecodeCPModelEnvironment* env)
{
  return (env != nullptr) &&
      (env->modelInputFormat() == SolverModel::ModelInputFormat::INPUT_FORMAT_OPTILAB);
}  // isa

OptilabModelEnvironment* OptilabModelEnvironment::cast(GecodeCPModelEnvironment* env)
{
  if (!isa(env)) return nullptr;
  return static_cast<OptilabModelEnvironment*>(env);
}  // cast

const OptilabModelEnvironment* OptilabModelEnvironment::cast(const GecodeCPModelEnvironment* env)
{
  if (!isa(env)) return nullptr;
  return static_cast<const OptilabModelEnvironment*>(env);
}  // cast

SolverModel::ModelInputFormat OptilabModelEnvironment::modelInputFormat() const noexcept
{
  return SolverModel::ModelInputFormat::INPUT_FORMAT_OPTILAB;
}  // modelInputFormat

FlatzincModelEnvironment::FlatzincModelEnvironment(const std::string& modelName,
                                                   const std::string& fznModel)
: pFlatZincPrinterPtr(std::make_shared<Gecode::FlatZinc::Printer>()),
  pFznOptionsPtr(std::make_shared<Gecode::FlatZinc::FlatZincOptions>(modelName.c_str()))
{
  if (fznModel.empty())
  {
    throw std::invalid_argument("FlatzincModelEnvironment - empty FlatZinc model");
  }

  std::stringstream sourceModelStream(fznModel);
  std::ostringstream outErrStream;

  pSpace = FlatZincSpaceSPtr(
      Gecode::FlatZinc::parse(sourceModelStream, *pFlatZincPrinterPtr, outErrStream));

  if (!pSpace)
  {
    const std::string errMsg = "FlatzincModelEnvironment - "
        "error in parsing the model: " + outErrStream.str();
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }
}

FlatzincModelEnvironment::FlatzincModelEnvironment(const std::string& modelName,
                                                   std::istream& fznModel)
: pFlatZincPrinterPtr(std::make_shared<Gecode::FlatZinc::Printer>()),
  pFznOptionsPtr(std::make_shared<Gecode::FlatZinc::FlatZincOptions>(modelName.c_str()))
{
  std::ostringstream outErrStream;
  pSpace = FlatZincSpaceSPtr(
      Gecode::FlatZinc::parse(fznModel, *pFlatZincPrinterPtr, outErrStream));

  if (!pSpace)
  {
    const std::string errMsg = "FlatzincModelEnvironment - "
        "error in parsing the model: " + outErrStream.str();
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }
}

bool FlatzincModelEnvironment::isa(const GecodeCPModelEnvironment* env)
{
  return (env != nullptr) &&
      (env->modelInputFormat() == SolverModel::ModelInputFormat::INPUT_FORMAT_FLATZINC);
}  // isa

FlatzincModelEnvironment* FlatzincModelEnvironment::cast(GecodeCPModelEnvironment* env)
{
  if (!isa(env)) return nullptr;
  return static_cast<FlatzincModelEnvironment*>(env);
}  // cast

const FlatzincModelEnvironment* FlatzincModelEnvironment::cast(const GecodeCPModelEnvironment* env)
{
  if (!isa(env)) return nullptr;
  return static_cast<const FlatzincModelEnvironment*>(env);
}  // cast

SolverModel::ModelInputFormat FlatzincModelEnvironment::modelInputFormat() const noexcept
{
  return SolverModel::ModelInputFormat::INPUT_FORMAT_FLATZINC;
}  // modelInputFormat

GecodeCPModel::GecodeCPModel(const Model::SPtr& model)
: CPModel(model)
{
  const auto modelFormat = getModelInputFormat();
  switch(modelFormat)
  {
    case SolverModel::ModelInputFormat::INPUT_FORMAT_OPTILAB:
    {
      OptilabSpace* ospace = new OptilabSpace();
      ospace->setSolveType(OptilabSpace::SolveType::SOLVE_TYPE_SAT);
      pSpaceEnvironment = std::make_shared<OptilabModelEnvironment>(OptilabSpace::SPtr(ospace));

    }
      break;
    case SolverModel::ModelInputFormat::INPUT_FORMAT_FLATZINC:
    {
      if (model->useModelFileDescriptor())
      {
        // Check if the file exists
        const std::string pathToFile =
            utilsfilesys::getPathToLocation({model->getModelDescriptor()});
        if (!utilsfilesys::doesFileExists(pathToFile))
        {
          const std::string errMsg = "GecodeCPModel - cannot find file: " + pathToFile;
          spdlog::error(errMsg);
          throw std::runtime_error(errMsg);
        }

        // Load the file
        std::ifstream fznFile(pathToFile);
        if (!fznFile.is_open())
        {
          const std::string errMsg = "GecodeCPModel - cannot load file: " + pathToFile;
          spdlog::error(errMsg);
          throw std::runtime_error(errMsg);
        }
        pSpaceEnvironment = std::make_shared<FlatzincModelEnvironment>(model->getModelName(),
                                                                       fznFile);
        fznFile.close();
      }
      else
      {
        pSpaceEnvironment = std::make_shared<FlatzincModelEnvironment>(model->getModelName(),
                                                                       model->getModelDescriptor());
      }
    }
      break;
    default:
      throw std::runtime_error("GecodeCPModel - unrecognized model input format");
  }
}

CPVariable::SPtr GecodeCPModel::createVariable(const ModelObjectVariable::SPtr& variable)
{
  if (!variable)
  {
    throw std::runtime_error("GecodeCPModel - empty pointer to ModelObjectVariable");
  }

  if (!OptilabModelEnvironment::isa(pSpaceEnvironment.get()))
  {
    throw std::runtime_error("GecodeCPModel - createVariable: invalid model environment");
  }

  return gecodesutils::getCPVar(
      variable, OptilabModelEnvironment::cast(pSpaceEnvironment.get())->getSpace());
}  // createVariable

CPConstraint::SPtr GecodeCPModel::createConstraint(
    const ModelObjectConstraint::SPtr& objConstraint) {
  if (!objConstraint)
  {
    throw std::runtime_error("GecodeCPModel - empty pointer to ModelObjectConstraint");
  }

  return gecodesutils::getCPCon(objConstraint,
                                OptilabModelEnvironment::cast(pSpaceEnvironment.get())->getSpace(), this);
}  // createConstraint

void GecodeCPModel::registerConstraints() {
  registerConstraint("int_eq", gecodeconstraintposters::p_int_eq);
  registerConstraint("int_ne", gecodeconstraintposters::p_int_ne);
  registerConstraint("int_ge", gecodeconstraintposters::p_int_ge);
  registerConstraint("int_gt", gecodeconstraintposters::p_int_gt);
  registerConstraint("int_le", gecodeconstraintposters::p_int_le);
  registerConstraint("int_lt", gecodeconstraintposters::p_int_lt);
  registerConstraint("int_eq_reif", gecodeconstraintposters::p_int_eq_reif);
  registerConstraint("int_ne_reif", gecodeconstraintposters::p_int_ne_reif);
  registerConstraint("int_ge_reif", gecodeconstraintposters::p_int_ge_reif);
  registerConstraint("int_gt_reif", gecodeconstraintposters::p_int_gt_reif);
  registerConstraint("int_le_reif", gecodeconstraintposters::p_int_le_reif);
  registerConstraint("int_lt_reif", gecodeconstraintposters::p_int_lt_reif);
  registerConstraint("int_eq_imp", gecodeconstraintposters::p_int_eq_imp);
  registerConstraint("int_ne_imp", gecodeconstraintposters::p_int_ne_imp);
  registerConstraint("int_ge_imp", gecodeconstraintposters::p_int_ge_imp);
  registerConstraint("int_gt_imp", gecodeconstraintposters::p_int_gt_imp);
  registerConstraint("int_le_imp", gecodeconstraintposters::p_int_le_imp);
  registerConstraint("int_lt_imp", gecodeconstraintposters::p_int_lt_imp);
  registerConstraint("int_lin_eq", gecodeconstraintposters::p_int_lin_eq);
  registerConstraint("int_lin_eq_reif", gecodeconstraintposters::p_int_lin_eq_reif);
  registerConstraint("int_lin_eq_imp", gecodeconstraintposters::p_int_lin_eq_imp);
  registerConstraint("int_lin_ne", gecodeconstraintposters::p_int_lin_ne);
  registerConstraint("int_lin_ne_reif", gecodeconstraintposters::p_int_lin_ne_reif);
  registerConstraint("int_lin_ne_imp", gecodeconstraintposters::p_int_lin_ne_imp);
  registerConstraint("int_lin_le", gecodeconstraintposters::p_int_lin_le);
  registerConstraint("int_lin_le_reif", gecodeconstraintposters::p_int_lin_le_reif);
  registerConstraint("int_lin_le_imp", gecodeconstraintposters::p_int_lin_le_imp);
  registerConstraint("int_lin_lt", gecodeconstraintposters::p_int_lin_lt);
  registerConstraint("int_lin_lt_reif", gecodeconstraintposters::p_int_lin_lt_reif);
  registerConstraint("int_lin_lt_imp", gecodeconstraintposters::p_int_lin_lt_imp);
  registerConstraint("int_lin_ge", gecodeconstraintposters::p_int_lin_ge);
  registerConstraint("int_lin_ge_reif", gecodeconstraintposters::p_int_lin_ge_reif);
  registerConstraint("int_lin_ge_imp", gecodeconstraintposters::p_int_lin_ge_imp);
  registerConstraint("int_lin_gt", gecodeconstraintposters::p_int_lin_gt);
  registerConstraint("int_lin_gt_reif", gecodeconstraintposters::p_int_lin_gt_reif);
  registerConstraint("int_lin_gt_imp", gecodeconstraintposters::p_int_lin_gt_imp);
  registerConstraint("int_plus", gecodeconstraintposters::p_int_plus);
  registerConstraint("int_minus", gecodeconstraintposters::p_int_minus);
  registerConstraint("int_times", gecodeconstraintposters::p_int_times);
  registerConstraint("int_div", gecodeconstraintposters::p_int_div);
  registerConstraint("int_mod", gecodeconstraintposters::p_int_mod);
  registerConstraint("int_min", gecodeconstraintposters::p_int_min);
  registerConstraint("int_max", gecodeconstraintposters::p_int_max);
  registerConstraint("int_abs", gecodeconstraintposters::p_abs);
  registerConstraint("int_negate", gecodeconstraintposters::p_int_negate);
  registerConstraint("bool_eq", gecodeconstraintposters::p_bool_eq);
  registerConstraint("bool_eq_reif", gecodeconstraintposters::p_bool_eq_reif);
  registerConstraint("bool_eq_imp", gecodeconstraintposters::p_bool_eq_imp);
  registerConstraint("bool_ne", gecodeconstraintposters::p_bool_ne);
  registerConstraint("bool_ne_reif", gecodeconstraintposters::p_bool_ne_reif);
  registerConstraint("bool_ne_imp", gecodeconstraintposters::p_bool_ne_imp);
  registerConstraint("bool_ge", gecodeconstraintposters::p_bool_ge);
  registerConstraint("bool_ge_reif", gecodeconstraintposters::p_bool_ge_reif);
  registerConstraint("bool_ge_imp", gecodeconstraintposters::p_bool_ge_imp);
  registerConstraint("bool_le", gecodeconstraintposters::p_bool_le);
  registerConstraint("bool_le_reif", gecodeconstraintposters::p_bool_le_reif);
  registerConstraint("bool_le_imp", gecodeconstraintposters::p_bool_le_imp);
  registerConstraint("bool_gt", gecodeconstraintposters::p_bool_gt);
  registerConstraint("bool_gt_reif", gecodeconstraintposters::p_bool_gt_reif);
  registerConstraint("bool_gt_imp", gecodeconstraintposters::p_bool_gt_imp);
  registerConstraint("bool_lt", gecodeconstraintposters::p_bool_lt);
  registerConstraint("bool_lt_reif", gecodeconstraintposters::p_bool_lt_reif);
  registerConstraint("bool_lt_imp", gecodeconstraintposters::p_bool_lt_imp);
  registerConstraint("bool_or", gecodeconstraintposters::p_bool_or);
  registerConstraint("bool_or_imp", gecodeconstraintposters::p_bool_or_imp);
  registerConstraint("bool_and", gecodeconstraintposters::p_bool_and);
  registerConstraint("bool_and_imp", gecodeconstraintposters::p_bool_and_imp);
  registerConstraint("bool_xor", gecodeconstraintposters::p_bool_xor);
  registerConstraint("bool_xor_imp", gecodeconstraintposters::p_bool_xor_imp);
  registerConstraint("array_bool_and", gecodeconstraintposters::p_array_bool_and);
  registerConstraint("array_bool_and_imp", gecodeconstraintposters::p_array_bool_and_imp);
  registerConstraint("array_bool_or", gecodeconstraintposters::p_array_bool_or);
  registerConstraint("array_bool_or_imp", gecodeconstraintposters::p_array_bool_or_imp);
  registerConstraint("array_bool_xor", gecodeconstraintposters::p_array_bool_xor);
  registerConstraint("array_bool_xor_imp", gecodeconstraintposters::p_array_bool_xor_imp);
  registerConstraint("bool_clause", gecodeconstraintposters::p_array_bool_clause);
  registerConstraint("bool_clause_reif", gecodeconstraintposters::p_array_bool_clause_reif);
  registerConstraint("bool_clause_imp", gecodeconstraintposters::p_array_bool_clause_imp);
  registerConstraint("bool_left_imp", gecodeconstraintposters::p_bool_l_imp);
  registerConstraint("bool_right_imp", gecodeconstraintposters::p_bool_r_imp);
  registerConstraint("bool_not", gecodeconstraintposters::p_bool_not);
  registerConstraint("array_int_element", gecodeconstraintposters::p_array_int_element);
  registerConstraint("array_var_int_element", gecodeconstraintposters::p_array_int_element);
  registerConstraint("array_bool_element", gecodeconstraintposters::p_array_bool_element);
  registerConstraint("array_var_bool_element", gecodeconstraintposters::p_array_bool_element);
  registerConstraint("bool2int", gecodeconstraintposters::p_bool2int);
}  // registerConstraint

}  // namespace optilab
