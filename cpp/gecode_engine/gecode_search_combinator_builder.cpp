#include "gecode_engine/gecode_search_combinator_builder.hpp"

#include <stdexcept>  // for std::invalid_argument

#include <gecode/int.hh>

#ifdef GECODE_HAS_SET_VARS
#include <gecode/set.hh>
#endif

#ifdef GECODE_HAS_FLOAT_VARS
#include <gecode/float.hh>
#endif

#include "model/model_constants.hpp"
#include "gecode_engine/gecode_variable.hpp"
#include "gecode_engine/gecode_search_combinator.hpp"
#include "gecode_engine/gecode_utilities.hpp"

namespace {

/// Returns the list of all indices of all Gecode variables corresponding to
/// the given list of variable
std::vector<std::size_t> getIndexListFromVarArray(
    const std::vector<optilab::CPVariable::SPtr>& varList)
{
  std::vector<std::size_t> idxList;
  for (const auto& ptr : varList)
  {
    auto gecodeVarPtr = std::dynamic_pointer_cast<optilab::GecodeVariable>(ptr);
    if (!gecodeVarPtr)
    {
      throw std::runtime_error("GecodeSearchCombinatorBuilder - buildBaseSearchCombinator "
          "invalid cast to GecodeVariable");
    }

    for (auto varIdx : gecodeVarPtr->getVarSpaceIdxList())
    {
      idxList.push_back(varIdx);
    }
  }

  return idxList;
}  // getIndexListFromVarArray

}  // namespace

namespace optilab {

GecodeSearchCombinatorBuilder::GecodeSearchCombinatorBuilder(OptilabSpace::SPtr space)
: pSpace(space)
{
  if (!pSpace)
  {
    throw std::invalid_argument("GecodeSearchCombinatorBuilder - pointer to space is empty");
  }
}

std::string GecodeSearchCombinatorBuilder::getDefaultVariableSelectionStrategy() const
{
  if (pBuilderState.selectionClass ==
      GecodeCombinatorBuilderState::CombinatorSelectionClass::CSC_INT)
  {
    return gecodeconstants::INT_VAR_AFC_SIZE_MAX;
  }
  else if (pBuilderState.selectionClass ==
      GecodeCombinatorBuilderState::CombinatorSelectionClass::CSC_BOOL)
  {
    return gecodeconstants::BOOL_VAR_AFC_MAX;
  }
  else if (pBuilderState.selectionClass ==
        GecodeCombinatorBuilderState::CombinatorSelectionClass::CSC_SET)
  {
    return gecodeconstants::SET_VAR_AFC_SIZE_MAX;
  }
  else if (pBuilderState.selectionClass ==
        GecodeCombinatorBuilderState::CombinatorSelectionClass::CSC_FLOAT)
  {
    return gecodeconstants::FLOAT_VAR_SIZE_MIN;
  }

  return "";
}  // getDefaultVariableSelectionStrategy

std::string GecodeSearchCombinatorBuilder::getDefaultValueSelectionStrategy() const
{
  if (pBuilderState.selectionClass ==
      GecodeCombinatorBuilderState::CombinatorSelectionClass::CSC_INT)
  {
    return gecodeconstants::INT_VAL_MIN;
  }
  else if (pBuilderState.selectionClass ==
      GecodeCombinatorBuilderState::CombinatorSelectionClass::CSC_BOOL)
  {
    return gecodeconstants::BOOL_VAL_MIN;
  }
  else if (pBuilderState.selectionClass ==
        GecodeCombinatorBuilderState::CombinatorSelectionClass::CSC_SET)
  {
    return gecodeconstants::SET_VAL_MIN_INC;
  }
  else if (pBuilderState.selectionClass ==
        GecodeCombinatorBuilderState::CombinatorSelectionClass::CSC_FLOAT)
  {
    return gecodeconstants::FLOAT_VAL_SPLIT_MIN;
  }

  return "";
}  // getDefaultValueSelectionStrategy

SearchCombinator::SPtr GecodeSearchCombinatorBuilder::buildBaseSearchCombinator(
    const std::vector<CPVariable::SPtr>& variablesList, const std::string& varSelectionStrategy,
    const std::string& valSelectionStrategy)
{
  Gecode::Rnd rnd(pBuilderState.randomSeed);
  auto idxList = getIndexListFromVarArray(variablesList);
  if (pBuilderState.selectionClass ==
      GecodeCombinatorBuilderState::CombinatorSelectionClass::CSC_INT)
  {
    Gecode::IntVarArgs vars(static_cast<int>(idxList.size()));

    int idx = 0;
    for (auto iter : idxList)
    {
      vars[idx++] = pSpace->getIntVar(iter);
    }

    const auto varBranch = gecodesutils::getIntVarBranchFromString(varSelectionStrategy,
                                                                   rnd, pBuilderState.decay);
    const auto valBranch = gecodesutils::getIntValBranchFromString(valSelectionStrategy, rnd);
    return std::make_shared<GecodeBaseSearchCombinator>(pSpace, vars, varBranch, valBranch);
  }
  else if (pBuilderState.selectionClass ==
      GecodeCombinatorBuilderState::CombinatorSelectionClass::CSC_BOOL)
  {
    Gecode::BoolVarArgs vars(static_cast<int>(idxList.size()));

    int idx = 0;
    for (auto iter : idxList)
    {
      vars[idx++] = pSpace->getBoolVar(iter);
    }

    const auto varBranch = gecodesutils::getBoolVarBranchFromString(varSelectionStrategy,
                                                                    rnd, pBuilderState.decay);
    const auto valBranch = gecodesutils::getBoolValBranchFromString(valSelectionStrategy, rnd);
    return std::make_shared<GecodeBaseSearchCombinator>(pSpace, vars, varBranch, valBranch);
  }
#ifdef GECODE_HAS_SET_VARS
  else if (pBuilderState.selectionClass ==
        GecodeCombinatorBuilderState::CombinatorSelectionClass::CSC_SET)
  {
    Gecode::SetVarArgs vars(static_cast<int>(idxList.size()));

    int idx = 0;
    for (auto iter : idxList)
    {
      vars[idx++] = pSpace->getSetVar(iter);
    }

    const auto varBranch = gecodesutils::getSetVarBranchFromString(varSelectionStrategy,
                                                                   rnd, pBuilderState.decay);
    const auto valBranch = gecodesutils::getSetValBranchFromString(valSelectionStrategy, rnd);
    return std::make_shared<GecodeBaseSearchCombinator>(pSpace, vars, varBranch, valBranch);
  }
#endif
#ifdef GECODE_HAS_FLOAT_VARS
  else if (pBuilderState.selectionClass ==
        GecodeCombinatorBuilderState::CombinatorSelectionClass::CSC_FLOAT)
  {
    Gecode::FloatVarArgs vars(static_cast<int>(idxList.size()));

    int idx = 0;
    for (auto iter : idxList)
    {
      vars[idx++] = pSpace->getFloatVar(iter);
    }

    const auto varBranch = gecodesutils::getFloatVarBranchFromString(varSelectionStrategy,
                                                                     rnd, pBuilderState.decay);
    const auto valBranch = gecodesutils::getFloatValBranchFromString(valSelectionStrategy, rnd);
    return std::make_shared<GecodeBaseSearchCombinator>(pSpace, vars, varBranch, valBranch);
  }
#endif
  throw std::runtime_error("GecodeSearchCombinatorBuilder - unrecognized selection class");
}  // buildBaseSearchCombinator


SearchCombinator::SPtr GecodeSearchCombinatorBuilder::buildAndCombinator(
    const std::vector<SearchCombinator::SPtr>& combList)
{
  if (combList.empty())
  {
    throw std::runtime_error("GecodeSearchCombinatorBuilder - buildAndCombinator: "
        "empty list of combinators");
  }

  std::vector<GecodeSearchCombinator::SPtr> listOfCombinators;
  listOfCombinators.reserve(combList.size());

  for (const auto& combPtr : combList)
  {
    GecodeSearchCombinator::SPtr gecodeComb =
        std::dynamic_pointer_cast<GecodeSearchCombinator>(combPtr);
    if (!gecodeComb)
    {
      throw std::runtime_error("GecodeSearchCombinatorBuilder - buildAndCombinator: "
          "invalid pointer cast to Gecode combinator");
    }

    listOfCombinators.push_back(gecodeComb);
  }
  return std::make_shared<GecodeCompositeSearchCombinator>(pSpace, listOfCombinators);
}  // buildAndCombinator

SearchCombinator::SPtr GecodeSearchCombinatorBuilder::buildOrCombinator(
    const std::vector<SearchCombinator::SPtr>& combList)
{
  if (combList.empty())
  {
    throw std::runtime_error("GecodeSearchCombinatorBuilder - buildOrCombinator: "
        "empty list of combinators");
  }

  std::vector<GecodeSearchCombinator::SPtr> listOfCombinators;
  listOfCombinators.reserve(combList.size());

  for (const auto& combPtr : combList)
  {
    GecodeSearchCombinator::SPtr gecodeComb =
        std::dynamic_pointer_cast<GecodeSearchCombinator>(combPtr);
    if (!gecodeComb)
    {
      throw std::runtime_error("GecodeSearchCombinatorBuilder - buildOrCombinator: "
          "invalid pointer cast to Gecode combinator");
    }

    listOfCombinators.push_back(gecodeComb);
  }
  throw std::runtime_error("GecodeSearchCombinatorBuilder - OR combinator not supported");
}  // buildOrCombinator

}  // namespace optilab
