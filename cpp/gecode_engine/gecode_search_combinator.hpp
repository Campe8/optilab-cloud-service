//
// Copyright OptiLab 2019. All rights reserved.
//
// Search combinator for Gecode CP solvers.
// @note a search combinator, as implemented for the Gecode library,
// encapsulates a brancher object.
// When a new brancher object is created, it is posted into the Gecode Space and
// becomes immediately active.
// This means that, in order to combine combinators, the brancher objects
// must not be posted right aways. Instead, the combinators should contain
// enough information to create branchers and post them only when required,
// after they are combined.
// For more information about branchers, see
// https://www.gecode.org/doc-latest/MPG.pdf
// ch.8.
//

#pragma once

#include <memory>   // for std::shared_ptr
#include <vector>

#include <gecode/int.hh>

#ifdef GECODE_HAS_SET_VARS
#include <gecode/set.hh>
#endif

#ifdef GECODE_HAS_FLOAT_VARS
#include <gecode/float.hh>
#endif

#include "gecode_engine/optilab_space.hpp"
#include "solver/cp_variable.hpp"
#include "solver/search_combinator.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS GecodeSearchCombinator : public SearchCombinator {
 public:
  using SPtr = std::shared_ptr<GecodeSearchCombinator>;

 public:
  /// Constructor.
  /// @note throws std::invalid_argument if the given pointer to the space is empty
  GecodeSearchCombinator(OptilabSpace::SPtr space);

  virtual ~GecodeSearchCombinator() = default;

  /// Returns true if this is a composite search combinator.
  /// Returns false otherwise
  virtual bool isComposite() const noexcept = 0;

  /// Posts this search combinator on the search space.
  /// When posted, this combinator becomes active,
  /// i.e., the search engine will branch w.r.t. this search combinator
  virtual void post() = 0;

 protected:
  /// Returns the space hold by this search combinator
  inline OptilabSpace::SPtr getSpace() const { return pSpace; }

 private:
  /// Space that creates this search combinator
  OptilabSpace::SPtr pSpace;
};

class SYS_EXPORT_CLASS GecodeBaseSearchCombinator : public GecodeSearchCombinator {
 public:
  using SPtr = std::shared_ptr<GecodeBaseSearchCombinator>;

 public:
  /// Constructor for INT base search combinator.
  /// @note throws std::invalid_argument if the given pointer to the space is empty
  GecodeBaseSearchCombinator(OptilabSpace::SPtr space, const Gecode::IntVarArgs& varArgs,
                             Gecode::TieBreak<Gecode::IntVarBranch> varBranch,
                             Gecode::IntValBranch valBranch);

  /// Constructor for BOOL base search combinator.
  /// @note throws std::invalid_argument if the given pointer to the space is empty
  GecodeBaseSearchCombinator(OptilabSpace::SPtr space, const Gecode::BoolVarArgs& varArgs,
                             Gecode::TieBreak<Gecode::BoolVarBranch> varBranch,
                             Gecode::BoolValBranch valBranch);

#ifdef GECODE_HAS_SET_VARS
  /// Constructor for SET base search combinator.
  /// @note throws std::invalid_argument if the given pointer to the space is empty
  GecodeBaseSearchCombinator(OptilabSpace::SPtr space, const Gecode::SetVarArgs& varArgs,
                             Gecode::SetVarBranch varBranch, Gecode::SetValBranch valBranch);
#endif

#ifdef GECODE_HAS_FLOAT_VARS
  /// Constructor for SET base search combinator.
  /// @note throws std::invalid_argument if the given pointer to the space is empty
  GecodeBaseSearchCombinator(OptilabSpace::SPtr space, const Gecode::FloatVarArgs& varArgs,
                             Gecode::TieBreak<Gecode::FloatVarBranch> varBranch,
                             Gecode::FloatValBranch valBranch);
#endif

  ~GecodeBaseSearchCombinator() = default;

  /// Returns true if this is a composite search combinator.
  /// Returns false otherwise
  bool isComposite() const noexcept override { return false; }

  /// Posts this search combinator on the search space.
  /// When posted, this combinator becomes active,
  /// i.e., the search engine will branch w.r.t. this search combinator
  void post() override;

 private:
  /// Class of this base combinator
  enum CombinatorClass {
    CLASS_INT = 0,
    CLASS_BOOL,
    CLASS_SET,
    CLASS_FLOAT
  };

 private:
  /// Class of this combinator
  CombinatorClass pCombinatorClass;

  /// Var args
  Gecode::IntVarArgs pIntVarArgs;
  Gecode::BoolVarArgs pBoolVarArgs;
#ifdef GECODE_HAS_SET_VARS
  Gecode::SetVarArgs pSetVarArgs;
#endif
#ifdef GECODE_HAS_FLOAT_VARS
  Gecode::FloatVarArgs pFloatVarArgs;
#endif

  /// Var choice
  Gecode::TieBreak<Gecode::IntVarBranch> pIntVarBranch;
  Gecode::TieBreak<Gecode::BoolVarBranch> pBoolVarBranch;
#ifdef GECODE_HAS_SET_VARS
  Gecode::SetVarBranch pSetVarBranch;
#endif
#ifdef GECODE_HAS_FLOAT_VARS
  Gecode::TieBreak<Gecode::FloatVarBranch> pFloatVarBranch;
#endif

  /// Val choice
  Gecode::IntValBranch pIntValBranch;
  Gecode::BoolValBranch pBoolValBranch;
#ifdef GECODE_HAS_SET_VARS
  Gecode::SetValBranch pSetValBranch;
#endif
#ifdef GECODE_HAS_FLOAT_VARS
  Gecode::FloatValBranch pFloatValBranch;
#endif
};

class SYS_EXPORT_CLASS GecodeCompositeSearchCombinator : public GecodeSearchCombinator {
 public:
  using SPtr = std::shared_ptr<GecodeCompositeSearchCombinator>;

 public:
  /// Constructor for composite search combinator.
  /// @note throws std::invalid_argument if the given pointer to the space is empty or any of
  /// the combinators in the list is empty
  GecodeCompositeSearchCombinator(OptilabSpace::SPtr space,
                                  const std::vector<GecodeSearchCombinator::SPtr>& combinatorList);

  virtual ~GecodeCompositeSearchCombinator() = default;

  /// Returns true if this is a composite search combinator.
  /// Returns false otherwise
  bool isComposite() const noexcept override { return true; }

  /// Posts all the search combinator hold by this composite object
  /// When posted, this combinator becomes active,
  /// i.e., the search engine will branch w.r.t. this search combinator
  void post() override;

 private:
  std::vector<GecodeSearchCombinator::SPtr> pCombinatorList;
};

}  // namespace optilab
