//
// Copyright OptiLab 2019. All rights reserved.
//
// Processor for Gecode CP engines.
//

#pragma once

#include "engine/optimizer_processor.hpp"

#include <cstdint>  // for uint64_t
#include <memory>   // for std::shared_ptr
#include <string>
#include <unordered_map>
#include <utility>  // for std::pair
#include <vector>

#include <boost/logic/tribool.hpp>
#include <boost/thread.hpp>

#include "model/model.hpp"
#include "gecode_engine/gecode_cp_model.hpp"
#include "gecode_engine/gecode_utilities.hpp"
#include "solver/cp_solver.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS GecodeProcessor :
public OptimizerProcessor<gecodeengine::GecodeWork::SPtr, gecodeengine::GecodeWork::SPtr> {
public:
  using SPtr = std::shared_ptr<GecodeProcessor>;

public:
  explicit GecodeProcessor(gecodeengine::GecodeResult::SPtr result);

  /// Loads the model and creates a solver to run on it.
  /// @note throw std::invalid_argument on empty model
  virtual void loadModelAndCreateSolver(const Model::SPtr& model);

  /// Interrupts the internal solving process (if running).
  /// The solver remains in a non-executable state until another model is loaded.
  /// Returns true on success, false otherwise
  virtual bool interruptSolver();

protected:
  using Work = gecodeengine::GecodeWork::SPtr;
  using BaseProcessor = OptimizerProcessor<Work, Work>;

protected:
  void processWork(Work work) override;

private:
  /// Lock type on the internal mutex
  using LockGuard = boost::lock_guard<boost::mutex>;

private:
  /// Model to solve
  GecodeCPModel::SPtr pCPModel{nullptr};

  /// Gecode CPSolver used to solve instances on this engine processor
  CPSolver::SPtr pCPSolver{nullptr};

  /// Mutex synch.ing the state of the result
  boost::mutex pResultMutex;

  /// Pointer to the downcast CP result instance
  gecodeengine::GecodeCPResult::SPtr pCPResult{nullptr};

  /// Boolean flag indicating whether or not the model is a satisfiable model
  boost::logic::tribool pSatModel{boost::logic::indeterminate};

  SolverModel::ModelInputFormat pModelInputFormat{
    SolverModel::ModelInputFormat::INPUT_FORMAT_OPTILAB};

  /// Builds an OptiLab solver
  void buildOptiLabSolver();

  /// Builds a FlatZinc solver
  void buildFlatZincSolver();

  /// Interrupts the current engine execution (if any).
  /// The engine remains in a non-executable state until another model is loaded
  void interruptEngine();

  /// Triggers the execution of the internal solver.
  /// @note this doesn't check whether a solver is currently running or not.
  /// It is responsibility of the caller to handle that logic
  void runSolver(Work& work);

  /// Runs constraint propagation only and returns whether it failed or not
  bool runConstraintPropagationOnly();

  /// Runs the solver to collect the give number of solution
  void runSolver(int numSol);

  /// Utility function: set the model run by the engine as unsatisfiable model
  void storeUnsatModel();

  /// Utility function: set the model run by the engine as satisfiable model
  void storeSatModel();

  /// Utility function: store current solution
  void storeSolution();

  /// Stores an OptiLab solution
  void storeOptilabSolution();

  /// Stores an OptiLab solution
  void storeFlatZincSolution();
};

}  // namespace optilab
