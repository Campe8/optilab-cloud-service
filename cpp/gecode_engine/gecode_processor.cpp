#include "gecode_engine/gecode_processor.hpp"

#include <cassert>
#include <exception>
#include <functional>  // for std::bind
#include <stdexcept>   // for std::runtime_error

#include <gecode/int.hh>

#ifdef GECODE_HAS_SET_VARS
#include <gecode/set.hh>
#endif

#ifdef GECODE_HAS_FLOAT_VARS
#include <gecode/float.hh>
#endif

#include <spdlog/spdlog.h>

#include "data_structure/metrics/metrics_register.hpp"
#include "engine/engine_constants.hpp"
#include "gecode_engine/gecode_flatzinc_solver.hpp"
#include "gecode_engine/gecode_solver.hpp"
#include "gecode_engine/gecode_solver_builder.hpp"
#include "gecode_engine/optilab_space.hpp"
#include "utilities/timer.hpp"

namespace optilab {

GecodeProcessor::GecodeProcessor(gecodeengine::GecodeResult::SPtr result)
: BaseProcessor("GecodeProcessor")
{
  pCPResult = std::dynamic_pointer_cast<gecodeengine::GecodeCPResult>(result);
  if (!pCPResult)
  {
    throw std::runtime_error("GecodeProcessor - empty pointer to result");
  }
}

void GecodeProcessor::loadModelAndCreateSolver(const Model::SPtr& model)
{
  if (!model)
  {
    throw std::runtime_error("GecodeProcessor - loadModelAndCreateSolver: empty model");
  }

  // Create a new Gecode CP model from the given input model
  pCPModel = std::make_shared<GecodeCPModel>(model);

  // Parse and initialize the model
  try
  {
    pCPModel->initializeModel();
  }
  catch(const std::exception& ex)
  {
    const std::string errMsg = "GecodeProcessor - loadModelAndCreateSolver: "
        "error parsing and initializing the model " + std::string(ex.what());
    spdlog::error(errMsg);
    throw;
  }
  catch(...)
  {
    const std::string errMsg = "GecodeProcessor - loadModelAndCreateSolver: "
        "error parsing and initializing the model";
    spdlog::error(errMsg);
    throw;
  }

  // Store the model input format.
  // @note the input format determines what Gecode solver will be used
  pModelInputFormat = pCPModel->getModelInputFormat();
  if (pModelInputFormat == SolverModel::ModelInputFormat::INPUT_FORMAT_OPTILAB)
  {
    buildOptiLabSolver();
  }
  else if (pModelInputFormat == SolverModel::ModelInputFormat::INPUT_FORMAT_FLATZINC)
  {
    buildFlatZincSolver();
  }
  else
  {
    const std::string errMsg = "GecodeProcessor - loadModelAndCreateSolver: "
        "invalid model input format";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }
}  // loadModelAndCreateSolver

void GecodeProcessor::buildOptiLabSolver()
{
  // Build a CP Solver builder
  GecodeSolverBuilder::SPtr solverBuilder;
  try
  {
    solverBuilder = std::make_shared<GecodeSolverBuilder>(pCPModel);
  }
  catch(const std::exception& ex)
  {
    const std::string errMsg = "GecodeProcessor - buildOptiLabSolver: "
        "cannot build the solver builder " + std::string(ex.what());
    spdlog::error(errMsg);
    throw;
  }
  catch(...)
  {
    const std::string errMsg = "GecodeProcessor - buildOptiLabSolver: "
            "cannot build the solver builder";
    spdlog::error(errMsg);
    throw;
  }

  // Build the solver
  try
  {
    pCPSolver = solverBuilder->build();
  }
  catch(const std::exception& ex)
  {
    const std::string errMsg = "GecodeProcessor - buildOptiLabSolver: "
        "cannot build the solver " + std::string(ex.what());
    spdlog::error(errMsg);
    throw;
  }
  catch(...)
  {
    const std::string errMsg = "GecodeProcessor - buildOptiLabSolver: "
            "cannot build the solver";
    spdlog::error(errMsg);
    throw;
  }
}  // buildOptiLabSolver

void GecodeProcessor::buildFlatZincSolver()
{
  // Build the solver
  try
  {
    pCPSolver = std::make_shared<GecodeFlatZincSolver>(pCPModel);
  }
  catch(const std::exception& ex)
  {
    const std::string errMsg = "GecodeProcessor - buildFlatZincSolver: "
        "cannot build the solver " + std::string(ex.what());
    spdlog::error(errMsg);
    throw;
  }
  catch(...)
  {
    const std::string errMsg = "GecodeProcessor - buildFlatZincSolver: "
            "cannot build the solver";
    spdlog::error(errMsg);
    throw;
  }
}  // buildFlatZincSolver

bool GecodeProcessor::interruptSolver()
{
  if (!pCPSolver)
  {
    return false;
  }

  pCPSolver->interruptSearchProcess();
  return true;
}  // interruptSolver

void GecodeProcessor::processWork(Work work)
{
  if (work->workType == gecodeengine::GecodeWork::kRunOptimizer)
  {
    runSolver(work);
  }
  else
  {
    spdlog::warn("GecodeProcessor - processWork: unrecognized work type");
  }
}  // processWork

void GecodeProcessor::runSolver(Work& work)
{
  if (!pCPModel)
  {
    const std::string errMsg = "GecodeProcessor - runSolver: no model loaded";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  if (!pCPSolver)
  {
    const std::string errMsg = "GecodeProcessor - runSolver: no solver available";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  timer::Timer timer;

  auto numSol = pCPModel->getNumberOfSolutionsToFind();
  if (pModelInputFormat == SolverModel::ModelInputFormat::INPUT_FORMAT_OPTILAB &&
      (pCPModel->isPropagationOnlyModel() || numSol == 0))
  {
    const bool satModel = runConstraintPropagationOnly();
    if (!satModel)
    {
      storeUnsatModel();
    }
    else
    {
      storeSatModel();
    }
  }
  else
  {
    // For non propagation modes and FlatZinc models
    if (pModelInputFormat == SolverModel::ModelInputFormat::INPUT_FORMAT_FLATZINC)
    {
      numSol = -1;
    }
    runSolver(numSol);
  }

  const auto wallclockMsec = timer.getWallClockTimeMsec();
  work->metricsRegister->setMetric(jsonmetrics::OPTIMIZER_LATENCY_MSEC, wallclockMsec);
}  // runSolver

bool GecodeProcessor::runConstraintPropagationOnly()
{
  // Initialize the search
  pCPSolver->initSearch();

  // Run the solver for constraint propagation only
  const bool satModel = pCPSolver->constraintPropagation();

  // Cleanup the search
  pCPSolver->cleanupSearch();

  // Return the result of the propagation
  return satModel;
}  // runConstraintPropagationOnly

void GecodeProcessor::runSolver(int numSol)
{
  assert(numSol != 0);

  if (!pCPSolver)
  {
    const std::string errMsg = "GecodeProcessor - runSolver: "
        "empty pointer to the solver instance";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  // Initialize the search
  pCPSolver->initSearch();

  // Check for all solutions
  bool findAllSolutions = numSol < 0;

  int solCtr = 0;
  while(((solCtr < numSol) || findAllSolutions) && pCPSolver->nextSolution())
  {
    storeSolution();
    solCtr++;
  }

  // Check and set this model status w.r.t. the solver status
  const auto status = pCPSolver->getStatus();
  if (status == CPSolver::SolverStatus::SS_SAT || status == CPSolver::SolverStatus::SS_OPT)
  {
    // Keep track of the fact that the model is satisfiable
    storeSatModel();
  }
  else
  {
    storeUnsatModel();
  }

  // Cleanup the search
  pCPSolver->cleanupSearch();
}  // runSolver

void GecodeProcessor::storeUnsatModel()
{
  pSatModel = false;

  LockGuard lock(pResultMutex);
  pCPResult->isSatModel = pSatModel;
}  // storeUnsatModel

void GecodeProcessor::storeSatModel()
{
  pSatModel = true;

  LockGuard lock(pResultMutex);
  pCPResult->isSatModel = pSatModel;
}  // storeSatModel


void GecodeProcessor::storeSolution()
{
  if (pModelInputFormat == SolverModel::ModelInputFormat::INPUT_FORMAT_OPTILAB)
  {
    storeOptilabSolution();
  }
  else if (pModelInputFormat == SolverModel::ModelInputFormat::INPUT_FORMAT_FLATZINC)
  {
    storeFlatZincSolution();
  }
  else
  {
    const std::string errMsg = "GecodeProcessor - storeSolution: "
        "invalid solver instance";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }
}  // storeSolution

void GecodeProcessor::storeFlatZincSolution()
{
  // Critical section
  LockGuard lock(pResultMutex);

  auto flatzincSolver = std::dynamic_pointer_cast<GecodeFlatZincSolver>(pCPSolver);
  if (!flatzincSolver)
  {
    const std::string errMsg = "GecodeProcessor - storeFlatZincSolution: "
        "invalid GecodeFlatZincSolver instance";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  // Get the FlatZinc solution
  const auto fznSol = flatzincSolver->getSolution();

  // Build the solution pair.
  // @note the first element of the solution pair is the FlatZinc verbatim solution
  gecodeengine::GecodeCPResult::Solution solutionPair;
  solutionPair.first = fznSol.first;

  // Add the variable assignments in the map
  auto& solutionMap = solutionPair.second;
  for (const auto& it : fznSol.second)
  {
    auto& domainAssign = solutionMap[it.first];

    // Type of the variable in FlatZinc model is not defined and it must be deduced from
    // the client side
    domainAssign.first = GecodeVariable::GecodeVarType::GVT_UNDEF;
    for (const auto& vals : it.second)
    {
      std::vector<std::string> assign;
      assign.push_back(vals);
      domainAssign.second.push_back(assign);
    }
  }

  // Store the solution
  pCPResult->solutionList.push_back(solutionPair);
}  // storeFlatZincSolution

void GecodeProcessor::storeOptilabSolution()
{
  // Critical section
  LockGuard lock(pResultMutex);

  // Keep track of the fact that the model is satisfiable
  storeSatModel();

  // Retrieve the solution from the Gecode solution
  auto gecodeSolver = std::dynamic_pointer_cast<GecodeSolver>(pCPSolver);
  if (!gecodeSolver)
  {
    const std::string errMsg = "GecodeProcessor - storeOptilabSolution: "
        "invalid GecodeSolver instance";;
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  gecodeengine::GecodeCPResult::Solution solutionPair;
  solutionPair.first = "";
  auto& solution = solutionPair.second;
  OptilabSpace::SPtr gecodeSolution;
  gecodeSolution = gecodeSolver->getSolution();
  if (!gecodeSolution)
  {
    const std::string errMsg = "GecodeProcessor - storeOptilabSolution: "
        "empty pointer to Gecode solution";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  for (const auto& varPair : pCPModel->getVariablesMap())
  {
    const auto& varName = varPair.first;
    if (solution.find(varName) != solution.end())
    {
      throw std::runtime_error(std::string("GecodeProcessor - storeOptilabSolution: "
          "variable assignment ") + std::string("already registered for variable ") + varName);
    }

    // Skip non-output variables
    if (!varPair.second->isOutput()) continue;

    GecodeVariable::SPtr var = std::dynamic_pointer_cast<GecodeVariable>(varPair.second);
    if (!var)
    {
      const std::string errMsg = "GecodeProcessor - storeOptilabSolution: "
          "invalid Gecode variable";
      spdlog::error(errMsg);
      throw std::runtime_error(errMsg);
    }

    const auto varIdxList = var->getVarSpaceIdxList();
    auto& solList = solution[varName];
    solList.second.reserve(varIdxList.size());

    const auto varType = var->getVarType();
    for (auto idx : varIdxList)
    {
      if (varType == GecodeVariable::GecodeVarType::GVT_INT)
      {
        solList.first = GecodeVariable::GecodeVarType::GVT_INT;

        std::vector<std::string> vals;
        vals.push_back(std::to_string((gecodeSolution->getIntVar(idx)).min()));
        vals.push_back(std::to_string((gecodeSolution->getIntVar(idx)).max()));
        solList.second.push_back(vals);
      }
      else if (varType == GecodeVariable::GecodeVarType::GVT_BOOL)
      {
        solList.first = GecodeVariable::GecodeVarType::GVT_BOOL;

        std::vector<std::string> vals;
        vals.push_back(std::to_string((gecodeSolution->getBoolVar(idx)).min()));
        vals.push_back(std::to_string((gecodeSolution->getBoolVar(idx)).max()));
        solList.second.push_back(vals);
      }
#ifdef GECODE_HAS_SET_VARS
      else if (varType == GecodeVariable::GecodeVarType::GVT_SET)
      {
        solList.first = GecodeVariable::GecodeVarType::GVT_SET;

        std::vector<std::string> vals;
        Gecode::SetVarGlbRanges svr(gecodeSolution->getSetVar(idx));
        if (!svr()) continue;

        ++svr;
        if (svr())
        {
          Gecode::SetVarGlbValues svv(gecodeSolution->getSetVar(idx));
          vals.push_back(std::to_string(svv.val()));
          ++svv;
          for (; svv(); ++svv)
          {
            vals.push_back(std::to_string(svv.val()));
          }
        }
        else
        {
          vals.push_back(std::to_string(svr.min()));
          vals.push_back(std::to_string(svr.max()));
        }
        solList.second.push_back(vals);
      }
#endif
#ifdef GECODE_HAS_FLOAT_VARS
      else if (varType == GecodeVariable::GecodeVarType::GVT_FLOAT)
      {
        solList.first = GecodeVariable::GecodeVarType::GVT_FLOAT;

        std::vector<std::string> vals;

        Gecode::FloatVal vv = (gecodeSolution->getFloatVar(idx)).val();
        vals.push_back(std::to_string(vv.min()));
        vals.push_back(std::to_string(vv.max()));
        solList.second.push_back(vals);
      }
#endif
    }
  }

  // Store the solution
  pCPResult->solutionList.push_back(solutionPair);
}  // storeOptilabSolution

}  // namespace optilab
