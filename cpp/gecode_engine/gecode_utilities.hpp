//
// Copyright OptiLab 2019. All rights reserved.
//
// Utilities for Gecode engine.
//

#pragma once

#include <cstddef>  // for std::size_t
#include <string>
#include <vector>
#include <unordered_map>
#include <utility>  // for std::pair

#include <boost/logic/tribool.hpp>

#include <gecode/int.hh>

#ifdef GECODE_HAS_SET_VARS
#include <gecode/set.hh>
#endif

#ifdef GECODE_HAS_FLOAT_VARS
#include <gecode/float.hh>
#endif

#include "data_structure/json/json.hpp"
#include "data_structure/metrics/metrics_register.hpp"
#include "model/model_object_variable.hpp"
#include "gecode_engine/gecode_variable.hpp"
#include "gecode_engine/gecode_constraint.hpp"
#include "gecode_engine/optilab_space.hpp"
#include "solver/cp_model.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

namespace gecodeengine {

struct SYS_EXPORT_STRUCT GecodeWork {
using SPtr = std::shared_ptr<GecodeWork>;

enum WorkType {
  /// Runs the optimizer on the loaded model
  kRunOptimizer,
  kWorkTypeUndef
};

GecodeWork(WorkType wtype, MetricsRegister::SPtr metReg)
: workType(wtype), metricsRegister(metReg)
{
}

WorkType workType;
MetricsRegister::SPtr metricsRegister;
};

class SYS_EXPORT_CLASS GecodeEvent {
 public:
  enum EventType {
    /// Starts the solving process
    kRunEngine,
    /// Kills any ongoing computation
    kInterruptEngine,
    /// Collects solutions
    kSolutions
  };

 public:
  explicit GecodeEvent(EventType aType)
  : pEventType(aType)
  {
  }

  inline EventType getType() const noexcept
  {
    return pEventType;
  }

  inline void setNumSolutions(int numSolutions) noexcept
  {
    pNumSolutions = numSolutions;
  }

  inline int getNumSolutions() const noexcept
  {
    return pNumSolutions;
  }

 private:
  /// Type of this event
  EventType pEventType;

  /// Number of solutions for "kSolutions" events
  int pNumSolutions{0};
};

struct SYS_EXPORT_STRUCT GecodeResult {
  using SPtr = std::shared_ptr<GecodeResult>;

  virtual ~GecodeResult() = default;

  /// Name of the model producing this result
  std::string modelName;
};

struct SYS_EXPORT_STRUCT GecodeCPResult : public GecodeResult {
  using SPtr = std::shared_ptr<GecodeCPResult>;

  /// Collection of solutions.
  /// @note an assignment is a list of list of values of a variable which may be scalar or
  /// a multi-dimensional variable. For each dimension, there is a vector of values instead of
  /// a pair of bounds. This is done to register SetVariables, which domain can have multiple
  /// elements instead of two bounds
  using VarAssign = std::pair<GecodeVariable::GecodeVarType, std::vector<std::vector<std::string>>>;

  /// A solution is a pair <string, map> where the string can be empty and represents the "verbatim"
  /// solution found by Gecode on model formats different than OptiLab (e.g. FlatZinc).
  /// The map is the actual map of variable assignments
  using Solution = std::pair<std::string, std::unordered_map<std::string, VarAssign>>;

  /// List of solutions
  std::vector<Solution> solutionList;

  /// Flag indicating whether or not this model is satisfiable
  boost::logic::tribool isSatModel{boost::logic::indeterminate};

  inline std::size_t getNumSolutions() const noexcept { return solutionList.size(); }

  /// Clear the results
  void clear()
  {
    solutionList.clear();
  }
};

}  // gecodeengine

namespace gecodesutils {

/// Builds a Gecode variable
SYS_EXPORT_FCN optilab::GecodeVariable::SPtr getCPVar(
    const optilab::ModelObjectVariable::SPtr& varPtr, const OptilabSpace::SPtr& space);

SYS_EXPORT_FCN optilab::GecodeConstraint::SPtr getCPCon(
    const optilab::ModelObjectConstraint::SPtr& conPtr, const OptilabSpace::SPtr& space,
    CPModel* model);

SYS_EXPORT_FCN Gecode::TieBreak<Gecode::IntVarBranch> getIntVarBranchFromString(
    const std::string& varBranch, Gecode::Rnd& rnd, double decay);
SYS_EXPORT_FCN Gecode::IntValBranch getIntValBranchFromString(const std::string& valBranch,
                                                              Gecode::Rnd& rnd);

SYS_EXPORT_FCN Gecode::TieBreak<Gecode::BoolVarBranch> getBoolVarBranchFromString(
    const std::string& varBranch, Gecode::Rnd& rnd, double decay);
SYS_EXPORT_FCN Gecode::BoolValBranch getBoolValBranchFromString(const std::string& valBranch,
                                                                Gecode::Rnd& rnd);

#ifdef GECODE_HAS_SET_VARS
SYS_EXPORT_FCN Gecode::SetVarBranch getSetVarBranchFromString(const std::string& varBranch,
                                                              Gecode::Rnd& rnd, double decay);
SYS_EXPORT_FCN Gecode::SetValBranch getSetValBranchFromString(const std::string& valBranch,
                                                              Gecode::Rnd& rnd);
#endif

#ifdef GECODE_HAS_FLOAT_VARS
SYS_EXPORT_FCN Gecode::TieBreak<Gecode::FloatVarBranch> getFloatVarBranchFromString(
    const std::string& varBranch, Gecode::Rnd& rnd, double decay);
SYS_EXPORT_FCN Gecode::FloatValBranch getFloatValBranchFromString(const std::string& valBranch,
                                                                  Gecode::Rnd& rnd);
#endif

/// Builds a JSON object representing the set of CP solutions given as parameter
SYS_EXPORT_FCN JSON::SPtr buildSolutionJson(const gecodeengine::GecodeCPResult::SPtr& res,
                                            int numSolutions);
}  // namespace gecodesutils

}  // namespace optilab
