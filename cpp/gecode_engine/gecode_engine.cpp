// Base class
#include "gecode_engine/gecode_engine.hpp"

#include <algorithm>  // for std::max
#include <memory>     // for std::make_shared
#include <sstream>
#include <stdexcept>  // for std::runtime_error
#include <vector>

#include <boost/logic/tribool.hpp>

#include "data_structure/json/json.hpp"
#include "gecode_engine/gecode_utilities.hpp"
#include "engine/engine_constants.hpp"
#include "engine/engine_utils.hpp"
#include "utilities/timer.hpp"

namespace {
/// Waiting value for an idle processor
constexpr int kWaitOnIdleProcessor { -1 };
}  // namespace

namespace optilab {

GecodeEngine::GecodeEngine(const std::string& engineId, const EngineCallbackHandler::SPtr& handler)
    : pEngineId(engineId),
      pCallbackHandler(handler)

{
  if (engineId.empty())
  {
    throw std::invalid_argument("GecodeEngine - empty engine identifier");
  }

  if (!handler)
  {
    throw std::invalid_argument("GecodeEngine - empty engine callback handler");
  }

  timer::Timer timer;

  pCPResult = std::make_shared<gecodeengine::GecodeCPResult>();

  pMetricsRegister = std::make_shared<MetricsRegister>();

  // Builds the optimizer running Gecode CP models
  buildOptimizer();
  assert(pOptimizer);

  // Start  the asynchronous thread in the optimizer
  pOptimizer->startUp();

  pActiveEngine = true;

  pEngineCreationTimeMsec = timer.getWallClockTimeMsec();
}

GecodeEngine::~GecodeEngine()
{
  try {
    turnDown();
  }
  catch(...)
  {
    // Do not throw in destructor
    spdlog::warn("GecodeEngine - thrown in destructor");
  }
}

void GecodeEngine::turnDown()
{
  if (pActiveEngine)
  {
    pActiveEngine = false;
    pOptimizer->tearDown();
    pOptimizer.reset();
  }
}  // turnDown

void GecodeEngine::buildOptimizer()
{
  pOptimizer = std::make_shared<GecodeOptimizer>(pEngineId, pCPResult, pMetricsRegister);
}  // buildOptimizer

void GecodeEngine::registerModel(const std::string& model)
{
  if (model.empty())
  {
    std::ostringstream ss;
    ss << "GecodeEngine - registerModel: "
        "registering an empty model for engine " << pEngineId;
    throw std::runtime_error(ss.str());
  }

  if (!pActiveEngine)
  {
    const std::string errMsg = "GecodeEngine - registerModel: "
        "trying to register a model on an inactive engine " + pEngineId;
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  // Start timer for loading the model
  timer::Timer timer;

  // Load the model into the optimizer
  try
  {
    pOptimizer->loadModel(model);
  }
  catch(std::exception& ex)
  {
    const std::string errMsg = "GecodeEngine - registerModel: "
        "error while loading the model on engine " + pEngineId +
        " " + std::string(ex.what());
    spdlog::error(errMsg);
    throw;
  }
  catch(...)
  {
    const std::string errMsg = "GecodeEngine - registerModel: "
        "undefined error while loading the model on engine " + pEngineId;
    spdlog::error(errMsg);
    throw;
  }

  pLoadModelLatencyMsec = timer.getWallClockTimeMsec();

  // Reset solution
  pCPResult->clear();
}  // registerModel

void GecodeEngine::engineWait(int timeoutMsec)
{
  if (pActiveEngine)
  {
    pOptimizer->waitOnTaskCompletion(timeoutMsec);
  }
}  // engineWait

void GecodeEngine::notifyEngine(const gecodeengine::GecodeEvent& event)
{
  switch (event.getType()) {
    case gecodeengine::GecodeEvent::EventType::kRunEngine:
    {
      processRunModelEvent();
      break;
    }
    case gecodeengine::GecodeEvent::EventType::kSolutions:
    {
      processCollectSolutionsEvent(event.getNumSolutions());
      break;
    }
    default:
    {
      assert(event.getType() == gecodeengine::GecodeEvent::EventType::kInterruptEngine);
      processInterruptEngineEvent();
      break;
    }
  }
}  // notifyEngine

void GecodeEngine::processRunModelEvent()
{
  // Return if the engine is not active
  if (!pActiveEngine)
  {
    spdlog::warn("GecodeEngine - processRunModelEvent: "
        "inactive engine, returning " + pEngineId);
    return;
  }

  timer::Timer timer;

  // Wait on previous tasks to complete.
  // For example, it waits on model registration
  engineWait(kWaitOnIdleProcessor);

  // Trigger the engine to run and return
  pOptimizer->runOptimizer();

  // Latency for running the optimizer
  pOptimizerLatencyMsec = timer.getWallClockTimeMsec();
}  // processRunModelEvent

void GecodeEngine::processInterruptEngineEvent()
{
  if (!pActiveEngine)
  {
    spdlog::warn("GecodeEngine - processInterruptEngineEvent: "
        "inactive engine, returning " + pEngineId);
    return;
  }

  pOptimizer->interruptOptimizer();
}  // processInterruptEngineEvent

void GecodeEngine::processCollectSolutionsEvent(int numSolutions)
{
  // @note it is assumed that the processor
  // is not currently adding result into the solution
  auto jsonSolution = gecodesutils::buildSolutionJson(pCPResult, numSolutions);

  // Callback method to send results back to the client
  pCallbackHandler->resultCallback(pEngineId, jsonSolution->toString());

  // Callback to send the metrics back to the client
  collectMetrics();
  auto jsonMetrics = engineutils::createMetricsJson(pMetricsRegister);

  // Callback method to send metrics back to the client
  pCallbackHandler->metricsCallback(pEngineId, jsonMetrics->toString());

  // Completed run
  pCallbackHandler->processCompletionCallback(pEngineId);
}  // processRunModelEvent

void GecodeEngine::collectMetrics()
{
  assert(pMetricsRegister);
  pMetricsRegister->setMetric(jsonmetrics::ENGINE_CREATION_TIME_MSEC, pEngineCreationTimeMsec);
  pMetricsRegister->setMetric(jsonmetrics::LOAD_MODEL_LATENCY_MSEC, pLoadModelLatencyMsec);
}  // collectMetrics

}  // namespace optilab
