#include "gecode_engine/gecode_solver.hpp"

#include <functional>  // for std::function
#include <stdexcept>   // for std::invalid_argument
#include <string>

#include <spdlog/spdlog.h>

namespace optilab {

GecodeSolver::GecodeSolver(const GecodeCPModel::SPtr& cpModel)
{
  if (!cpModel)
  {
    throw std::invalid_argument("GecodeSolver - empty pointer to a CP model");
  }

  // Get the model environment which, in the Gecode package, corresponds to a space
  auto modelEnvironment = cpModel->getModelEnvironment();
  auto optilabModelEnvironment = OptilabModelEnvironment::cast(modelEnvironment.get());
  if (!optilabModelEnvironment)
  {
    std::string errMsg = "GecodeSolver - invalid model environment";
    spdlog::error(errMsg);
    throw std::invalid_argument(errMsg);
  }

  pSpace = optilabModelEnvironment->getSpace();
  if (!pSpace)
  {
    std::string errMsg = "GecodeSolver - empty OptiLab space";
    spdlog::error(errMsg);
    throw std::invalid_argument(errMsg);
  }
}

void GecodeSolver::initSearch()
{
  const auto solveType = pSpace->getSolveType();
  switch(solveType)
  {
    case OptilabSpace::SolveType::SOLVE_TYPE_SAT:
      break;
    case OptilabSpace::SolveType::SOLVE_TYPE_MIN:
    case OptilabSpace::SolveType::SOLVE_TYPE_MAX:
    default:
      throw std::runtime_error("GecodeSolver - initSearch: invalid solve type");
  }

  // Post the branchers
  postBranchers();

  // Create the stop object to stop the search
  // Create the monitor used to kill the engine given the kill event
  std::function<bool()> killEngineLimit =
      std::bind(&GecodeSolver::checkWhetherSolverShouldStop, this);
  pStopObject = std::make_shared<InterruptStop>(killEngineLimit);
  pSolverOptions.stop = pStopObject.get();

  // Create the solver
  GecodeSolverImpl* solver;
  if (pSpace->getSolveType() == OptilabSpace::SolveType::SOLVE_TYPE_SAT)
  {
    solver = new MetaSolverImpl<Gecode::DFS, Gecode::Driver::EngineToMeta>(
        pSpace.get(), pSolverOptions);
  }
  else
  {
    solver = new MetaSolverImpl<Gecode::BAB, Gecode::Driver::EngineToMeta>(
        pSpace.get(), pSolverOptions);
  }

  pSolver = std::shared_ptr<GecodeSolverImpl>(solver);
}  // initSearch

void GecodeSolver::interruptSearchProcess()
{
  pSolverActive = false;
}  // interruptSearchProcess

bool GecodeSolver::checkWhetherSolverShouldStop()
{
  // The solver should stop when it is NOT active
  return !pSolverActive;
}  // checkWhetherSolverShouldStop

void GecodeSolver::postBranchers()
{
  if (!pCombinator)
  {
    const std::string errMsg = "GecodeSolver - empty combinator pointer";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  /// Post the combinator(s)
  pCombinator->post();
}  // postBranchers

bool GecodeSolver::nextSolution()
{
  // Call the solver's (Gecode engine's) "next(...)" method
  // to return a solution
  pSolution = std::shared_ptr<OptilabSpace>(pSolver->next());

  bool hasNextSolution = false;
  if (pSolution)
  {
    setSolverStatus(SolverStatus::SS_SAT);
    hasNextSolution = true;
  }
  else if (pSolver->stopped())
  {
    setSolverStatus(SolverStatus::SS_UNKNOWN);
  }
  else
  {
    setSolverStatus(SolverStatus::SS_UNSAT);
  }
  return hasNextSolution;
}  // nextSolution

bool GecodeSolver::constraintPropagation()
{
  // Perform constraint propagation only
  Gecode::SpaceStatus status = pSpace->status();

  bool propStatus = false;
  if (status == Gecode::SpaceStatus::SS_SOLVED ||
      status == Gecode::SpaceStatus::SS_BRANCH)
  {
    setSolverStatus(SolverStatus::SS_SAT);
    propStatus = true;
  }
  else
  {
    setSolverStatus(SolverStatus::SS_UNSAT);
  }

  return propStatus;
}  // constraintPropagation

}  // namespace optilab
