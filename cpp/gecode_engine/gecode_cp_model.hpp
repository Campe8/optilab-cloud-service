//
// Copyright OptiLab 2019. All rights reserved.
//
// Class encapsulating a Gecode CP model.
//

#pragma once

#include <istream>
#include <memory>   // for std::shared_ptr

#include <gecode/flatzinc.hh>

#include "gecode_engine/optilab_space.hpp"
#include "model/model.hpp"
#include "model/model_object_variable.hpp"
#include "solver/cp_model.hpp"
#include "solver/cp_variable.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

/**
 * Environment class for Gecode CPModels
 */
class SYS_EXPORT_CLASS GecodeCPModelEnvironment {
 public:
  using SPtr = std::shared_ptr<GecodeCPModelEnvironment>;

 public:
  virtual ~GecodeCPModelEnvironment() = default;
  virtual SolverModel::ModelInputFormat modelInputFormat() const noexcept = 0;
};

class SYS_EXPORT_CLASS OptilabModelEnvironment : public GecodeCPModelEnvironment {
 public:
  /// Constructor.
  /// @note throws std::invalid_argument on empty pointer to space
  OptilabModelEnvironment(OptilabSpace::SPtr space);

  static bool isa(const GecodeCPModelEnvironment* env);
  static OptilabModelEnvironment* cast(GecodeCPModelEnvironment* env);
  static const OptilabModelEnvironment* cast(const GecodeCPModelEnvironment* env);

  SolverModel::ModelInputFormat modelInputFormat() const noexcept override;
  inline OptilabSpace::SPtr getSpace() const { return pSpace; }

 private:
  /// The OptiLab Gecode space where the solver lives.
  /// For more information, see:
  /// https://www.gecode.org/doc-latest/MPG.pdf
  OptilabSpace::SPtr pSpace;
};

class SYS_EXPORT_CLASS FlatzincModelEnvironment : public GecodeCPModelEnvironment {
 public:
  using FlatZincSpaceSPtr = std::shared_ptr<Gecode::FlatZinc::FlatZincSpace>;

 public:
  /// Constructor: creates a new FlatZinc environment (with internal space) by parsing
  /// the given FlatZinc model represented as a string.
  /// @note throws std::invalid_argument if the given string is empty.
  /// @note throws std::runtime_error if the parsing of the model or the creation
  /// of the space was not successful
  FlatzincModelEnvironment(const std::string& modelName, const std::string& fznModel);
  FlatzincModelEnvironment(const std::string& modelName, std::istream& fznModel);

  static bool isa(const GecodeCPModelEnvironment* env);
  static FlatzincModelEnvironment* cast(GecodeCPModelEnvironment* env);
  static const FlatzincModelEnvironment* cast(const GecodeCPModelEnvironment* env);

  SolverModel::ModelInputFormat modelInputFormat() const noexcept override;
  inline FlatZincSpaceSPtr getSpace() const { return pSpace; }

  inline std::shared_ptr<Gecode::FlatZinc::Printer> getEnvFznPrinter() const
  {
    return pFlatZincPrinterPtr;
  }

  inline std::shared_ptr<Gecode::FlatZinc::FlatZincOptions> getEnvFznOptions() const
  {
    return pFznOptionsPtr;
  }

 private:
  /// The FlatZinc Gecode space where the solver lives.
  /// For more information, see:
  /// https://www.gecode.org/doc-latest/MPG.pdf
  FlatZincSpaceSPtr pSpace;

  /// Pointer to the printer for the FlatZinc space
  std::shared_ptr<Gecode::FlatZinc::Printer> pFlatZincPrinterPtr;

  /// Pointer to the FlatZinc model options
  std::shared_ptr<Gecode::FlatZinc::FlatZincOptions> pFznOptionsPtr;
};

class SYS_EXPORT_CLASS GecodeCPModel : public CPModel {
 public:
  using SPtr = std::shared_ptr<GecodeCPModel>;

 public:
  /// Constructor creates a new CP model run by Gecode CP engine.
  /// The model is build on the given input model.
  /// @note throws std::invalid_argument on empty input arguments.
  explicit GecodeCPModel(const Model::SPtr& model);

  /// Returns the "environment" this model is in,
  /// i.e., the "state" of the model where all model components belong to
  inline GecodeCPModelEnvironment::SPtr getModelEnvironment() const { return pSpaceEnvironment; }

 protected:
  CPVariable::SPtr createVariable(const ModelObjectVariable::SPtr& variable) override;

  CPConstraint::SPtr createConstraint(const ModelObjectConstraint::SPtr& objConstraint) override;

  void registerConstraints() override;

 private:
  /// The environment of this model is represented by the Gecode space
  /// where the solver and Gecode engine lives.
  /// For more information, see:
  /// https://www.gecode.org/doc-latest/MPG.pdf
  GecodeCPModelEnvironment::SPtr pSpaceEnvironment;
};

}  // namespace optilab
