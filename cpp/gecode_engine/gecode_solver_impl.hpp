//
// Copyright OptiLab 2019. All rights reserved.
//
// CPSolver implementation for Gecode package.
//

#pragma once

#include <functional>  // for std::function
#include <memory>      // for std::shared_ptr

#include <gecode/search.hh>

#include "gecode_engine/optilab_space.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS GecodeSolverImpl {
 public:
  using SPtr = std::shared_ptr<GecodeSolverImpl>;

 public:
  virtual ~GecodeSolverImpl() = default;
  virtual OptilabSpace* next(void) = 0;
  virtual bool stopped(void) = 0;
  virtual Gecode::Search::Statistics statisitcs(void) = 0;
};

template<template<class> class Solver,
template<class, template<class> class> class Meta>
class SYS_EXPORT_CLASS MetaSolverImpl : public GecodeSolverImpl {
 public:
  MetaSolverImpl(OptilabSpace* space, Gecode::Search::Options& options)
 : pSolver(space, options)
 {
 }
  OptilabSpace* next(void) override { return pSolver.next(); }
  bool stopped(void) override { return pSolver.stopped(); }
  Gecode::Search::Statistics statisitcs(void) override { return pSolver.statistics(); }

 private:
  Meta<OptilabSpace, Solver> pSolver;
};

/**
 * Stop-object based on external interrupt function
 */
class SYS_EXPORT_CLASS InterruptStop : public Gecode::Search::Stop {
 public:
  using SPtr = std::shared_ptr<InterruptStop>;

public:
  InterruptStop(std::function<bool()> fcn) : pStopFcn(fcn) {}

  /// Returns true if the function given in the constructor returns true
  bool stop(const Gecode::Search::Statistics&, const Gecode::Search::Options&) override
  {
    return pStopFcn();
  }

private:
  std::function<bool()> pStopFcn;
};


}  // namespace optilab
