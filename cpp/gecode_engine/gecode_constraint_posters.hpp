//
// Copyright OptiLab 2019. All rights reserved.
//
// Utilities for Gecode engine.
//

#pragma once

#include <gecode/int.hh>

#ifdef GECODE_HAS_SET_VARS
#include <gecode/set.hh>
#endif

#ifdef GECODE_HAS_FLOAT_VARS
#include <gecode/float.hh>
#endif

#include "solver/cp_constraint.hpp"
#include "gecode_engine/optilab_space.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

/**
 * Utilities namespace for constraint posters.
 */
namespace gecodeutils {

/// Convert arg (array of integers) to IntArgs
Gecode::IntArgs arg2intargs(const CPArgument& arg, int offset = 0);
/// Convert arg (array of Booleans) to IntArgs
Gecode::IntArgs arg2boolargs(const CPArgument& arg, int offset = 0);
/// Convert arg to IntVarArgs
Gecode::IntVarArgs arg2intvarargs(OptilabSpace& space, const CPArgument& arg, int offset = 0);
/// Convert arg to BoolVarArgs
Gecode::BoolVarArgs arg2boolvarargs(OptilabSpace& space, const CPArgument& arg, int offset = 0,
                                    int siv=-1);
/// Convert arg to BoolVar
Gecode::BoolVar arg2boolvar(OptilabSpace& space, const CPArgument& arg);
/// Convert arg to IntVar
Gecode::IntVar arg2intvar(OptilabSpace& space, const CPArgument& arg);

Gecode::IntPropLevel ann2icl(CPConstraint::PropagationType propType);

}  // gecodeutils

namespace gecodeconstraintposters {

SYS_EXPORT_FCN void p_int_eq(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_int_ne(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_int_ge(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_int_gt(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_int_le(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_int_lt(CPConstraint::SPtr con);

SYS_EXPORT_FCN void p_int_eq_reif(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_int_ne_reif(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_int_ge_reif(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_int_gt_reif(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_int_le_reif(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_int_lt_reif(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_int_eq_imp(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_int_ne_imp(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_int_ge_imp(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_int_gt_imp(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_int_le_imp(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_int_lt_imp(CPConstraint::SPtr con);

SYS_EXPORT_FCN void p_int_lin_eq(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_int_lin_eq_reif(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_int_lin_eq_imp(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_int_lin_ne(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_int_lin_ne_reif(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_int_lin_ne_imp(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_int_lin_le(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_int_lin_le_reif(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_int_lin_le_imp(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_int_lin_lt(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_int_lin_lt_reif(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_int_lin_lt_imp(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_int_lin_ge(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_int_lin_ge_reif(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_int_lin_ge_imp(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_int_lin_gt(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_int_lin_gt_reif(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_int_lin_gt_imp(CPConstraint::SPtr con);

SYS_EXPORT_FCN void p_bool_lin_eq(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_bool_lin_eq_reif(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_bool_lin_eq_imp(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_bool_lin_ne(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_bool_lin_ne_reif(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_bool_lin_ne_imp(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_bool_lin_le(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_bool_lin_le_reif(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_bool_lin_le_imp(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_bool_lin_lt(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_bool_lin_lt_reif(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_bool_lin_lt_imp(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_bool_lin_ge(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_bool_lin_ge_reif(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_bool_lin_ge_imp(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_bool_lin_gt(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_bool_lin_gt_reif(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_bool_lin_gt_imp(CPConstraint::SPtr con);

SYS_EXPORT_FCN void p_int_plus(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_int_minus(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_int_times(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_int_div(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_int_mod(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_int_min(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_int_max(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_abs(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_int_negate(CPConstraint::SPtr con);

SYS_EXPORT_FCN void p_bool_eq(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_bool_eq_reif(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_bool_eq_imp(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_bool_ne(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_bool_ne_reif(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_bool_ne_imp(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_bool_ge(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_bool_ge_reif(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_bool_ge_imp(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_bool_le(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_bool_le_reif(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_bool_le_imp(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_bool_gt(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_bool_gt_reif(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_bool_gt_imp(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_bool_lt(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_bool_lt_reif(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_bool_lt_imp(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_bool_or(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_bool_or_imp(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_bool_and(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_bool_and_imp(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_array_bool_and(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_array_bool_and_imp(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_array_bool_or(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_array_bool_or_imp(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_array_bool_xor(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_array_bool_xor_imp(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_array_bool_clause(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_array_bool_clause_reif(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_array_bool_clause_imp(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_bool_xor(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_bool_xor_imp(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_bool_l_imp(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_bool_r_imp(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_bool_not(CPConstraint::SPtr con);

SYS_EXPORT_FCN void p_bool_eq(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_bool_eq_reif(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_bool_eq_imp(CPConstraint::SPtr con);

SYS_EXPORT_FCN void p_array_int_element(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_array_bool_element(CPConstraint::SPtr con);

SYS_EXPORT_FCN void p_bool2int(CPConstraint::SPtr con);

}  // namespace gecodeconstraintposters

}  // namespace optilab
