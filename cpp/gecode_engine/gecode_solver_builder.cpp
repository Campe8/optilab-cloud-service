#include "gecode_engine/gecode_solver_builder.hpp"

#include <cassert>
#include <stdexcept>  // for std::invalid_argument

#include <spdlog/spdlog.h>

#include "gecode_engine/gecode_cp_model.hpp"
#include "gecode_engine/gecode_search_combinator.hpp"
#include "gecode_engine/gecode_search_combinator_builder.hpp"
#include "solver/cp_global_search.hpp"
#include "solver/cp_search_builder.hpp"

namespace optilab {

GecodeSolverBuilder::GecodeSolverBuilder(CPModel::SPtr cpModel)
: pModel(cpModel)
{
  if (!pModel)
  {
    throw std::invalid_argument("GecodeSolverBuilder - empty CPModel pointer");
  }
}

CPSolver::SPtr GecodeSolverBuilder::build()
{
  // Get the ORToolsCPModel used to instantiate the solver
  GecodeCPModel::SPtr gecodeCPModel = std::dynamic_pointer_cast<GecodeCPModel>(pModel);
  if (!gecodeCPModel)
  {
    throw std::runtime_error("GecodeSolverBuilder - CPModel is not a Gecode CPModel");
  }

  // Create a new instance of a solver
  GecodeSolver::SPtr solver = std::make_shared<GecodeSolver>(gecodeCPModel);

  // Create a builder for search strategies
  auto modelEnvironment = gecodeCPModel->getModelEnvironment();
  if (!OptilabModelEnvironment::isa(modelEnvironment.get()))
  {
    const std::string errMsg = "GecodeSolverBuilder - combinator builder cannot be built on "
        "non OptiLab spaces";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  GecodeSearchCombinatorBuilder::SPtr combinatorBuilder =
      std::make_shared<GecodeSearchCombinatorBuilder>(
          OptilabModelEnvironment::cast(modelEnvironment.get())->getSpace());
  CPSearchBuilder::SPtr searchBuilder = std::make_shared<CPSearchBuilder>(pModel,
                                                                          combinatorBuilder);

  // Build the search strategy
  CPSearch::SPtr searchStrategy = searchBuilder->build();

  // Set the search strategy into the solver
  const auto searchType = searchStrategy->getSearchType();
  switch (searchType)
  {
    case CPSearch::SearchType::GLOBAL_SEARCH:
    {
      setGlobalSearchOnSolver(solver, searchStrategy);
      break;
    }
    default:
    {
      assert(searchType == CPSearch::SearchType::LOCAL_SEARCH);
      throw std::runtime_error("GecodeSolverBuilder - local search strategy not supported");
    }
  }

  // Return the CP solver
  return solver;
}  // build

void GecodeSolverBuilder::setGlobalSearchOnSolver(const GecodeSolver::SPtr& solver,
                                                  const CPSearch::SPtr& searchStrategy)
{
  assert(CPGlobalSearchStrategy::isa(searchStrategy.get()));
  auto globalSearch = CPGlobalSearchStrategy::cast(searchStrategy.get());

  // Get the search combinator describing this search heuristic
  auto heuristic =
      std::dynamic_pointer_cast<GecodeSearchCombinator>(globalSearch->getSearchHeuristic());
  if (!heuristic)
  {
    throw std::runtime_error("GecodeSolverBuilder - search heuristic combinator is not "
        "a Gecode search combinator");
  }

  // Set the search heuristic to be used by the solver to explore the search space
  solver->setSearchCombinator(heuristic);
}  // setGlobalSearchOnSolver

}  // namespace optilab
