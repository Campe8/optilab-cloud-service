//
// Copyright OptiLab 2019. All rights reserved.
//
// CPSolver for Gecode package.
//

#pragma once

#include <memory>   // for std::shared_ptr

#include <gecode/search.hh>

#include "gecode_engine/gecode_cp_model.hpp"
#include "gecode_engine/gecode_search_combinator.hpp"
#include "gecode_engine/gecode_solver_impl.hpp"
#include "gecode_engine/optilab_space.hpp"
#include "solver/cp_solver.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS GecodeSolver : public CPSolver {
 public:
  using SPtr = std::shared_ptr<GecodeSolver>;

 public:
  /// Constructor.
  /// @note throws std::invalid_argument on empty argument pointer
  explicit GecodeSolver(const GecodeCPModel::SPtr& cpModel);

  virtual ~GecodeSolver() = default;

  /// Initializes the solver for the search
  void initSearch() override;

  /// Performs constraint propagation only.
  /// Returns true if the current variable assignment satisfies all constraints.
  /// Returns false otherwise
  bool constraintPropagation() override;

  /// Runs the solver until a solution (next solution) is found.
  /// Returns true when there is a "next" solution, returns false otherwise.
  /// This method is supposed to be called within a loop, for example:
  /// while (nextSolution())
  /// {
  ///    use solution;
  /// }
  /// @note this method DESTROYS the current solution (if any) and replaces it with next solution.
  /// To get the pointer to the current solution, use the method "getCurrentSolution(...)"
  bool nextSolution() override;

  /// Stops the (ongoing) search process and return
  void interruptSearchProcess() override;

  /// Sets the search combinator defining the search tree explored by this solver
  void setSearchCombinator(const GecodeSearchCombinator::SPtr& combinator)
  {
    pCombinator = combinator;
  }

  /// Returns the pointer to the copy of the space holding the current solution
  inline OptilabSpace::SPtr getSolution() { return pSolution; }

 private:
  /// Space of the model
  OptilabSpace::SPtr pSpace;

  /// Solution clone of the OptilabSpace
  OptilabSpace::SPtr pSolution{nullptr};

  /// Pointer to the actual implementation of the solver
  GecodeSolverImpl::SPtr pSolver{nullptr};

  /// Options to pass to the solver
  Gecode::Search::Options pSolverOptions;

  /// Combinator used by this solver while exploring the search tree
  GecodeSearchCombinator::SPtr pCombinator{nullptr};

  /// Flag indicating whether the solver should be running or not
  bool pSolverActive{true};

  /// Interrupt stop object
  InterruptStop::SPtr pStopObject{nullptr};

  /// Posts the branchers defining the search tree
  void postBranchers();

  /// Returns true if the solver should stop running. Returns false otherwise
  bool checkWhetherSolverShouldStop();
};

}  // namespace optilab
