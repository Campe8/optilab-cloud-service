#include "gecode_engine/gecode_constraint_posters.hpp"

#include <string>

#include "gecode_engine/gecode_constraint.hpp"
#include "gecode_engine/gecode_variable.hpp"

#define CON_CAST(c) castSmartPointer<optilab::CPConstraint::SPtr, optilab::GecodeConstraint>(c)
#define VAR_CAST(v) castSmartPointer<optilab::CPVariable::SPtr, optilab::GecodeVariable>(v)

namespace {

template<class From, class To>
To* castSmartPointer(const From& val)
{
  auto dynCast = std::dynamic_pointer_cast<To>(val);
  if (!dynCast)
  {
    throw std::runtime_error("Invalid dynamic cast to ORTools type");
  }

  return dynCast.get();
}  // castSmartPointer

inline bool valueWithinBounds(int64_t b)
{
  return b >= Gecode::Int::Limits::min && b <= Gecode::Int::Limits::max;
}  // valueWithinBounds

void p_int_CMP(optilab::GecodeConstraint* ct, Gecode::IntRelType irt)
{
  const auto& lhsArg = ct->getArgumentList()[0];
  const auto& rhsArg = ct->getArgumentList()[1];
  auto lhs = VAR_CAST(lhsArg.getVariable());
  auto rhs = VAR_CAST(rhsArg.getVariable());
  if (lhs->isVarInt()) {
    if (rhs->isVarInt()) {
      auto& space = *(ct->getSpace());
      rel(space, optilab::gecodeutils::arg2intvar(space, lhsArg), irt,
          optilab::gecodeutils::arg2intvar(space, rhsArg),
          optilab::gecodeutils::ann2icl(ct->getPropagationType()));
    } else {
      auto& space = *(ct->getSpace());
      rel(space, optilab::gecodeutils::arg2intvar(space, lhsArg), irt,
          rhsArg.getIntValue(), optilab::gecodeutils::ann2icl(ct->getPropagationType()));
    }
  } else {
    auto& space = *(ct->getSpace());
    rel(space, optilab::gecodeutils::arg2intvar(space, rhsArg), Gecode::swap(irt),
        lhsArg.getIntValue(), optilab::gecodeutils::ann2icl(ct->getPropagationType()));
  }
}  // p_int_CMP

void p_int_CMP_reif(optilab::GecodeConstraint* ct, Gecode::IntRelType irt,
                    Gecode::ReifyMode rm)
{
  const auto& lhsArg = ct->getArgumentList()[0];
  const auto& rhsArg = ct->getArgumentList()[1];
  const auto& reifArg = ct->getArgumentList()[2];
  if (rm == Gecode::ReifyMode::RM_EQV && !reifArg.isIntArgVal()) {
    if (reifArg.getIntValue() != 0) {
      p_int_CMP(ct, irt);
    } else {
      p_int_CMP(ct, neg(irt));
    }
    return;
  }

  auto& space = *(ct->getSpace());
  if (lhsArg.isArgVar()) {
    if (rhsArg.isArgVar()) {
      rel(space, optilab::gecodeutils::arg2intvar(space, lhsArg), irt,
          optilab::gecodeutils::arg2intvar(space, rhsArg),
          Gecode::Reify(optilab::gecodeutils::arg2boolvar(space, reifArg), rm),
          optilab::gecodeutils::ann2icl(ct->getPropagationType()));
    } else {
      rel(space, optilab::gecodeutils::arg2intvar(space, lhsArg), irt,
          rhsArg.getIntValue(),
          Gecode::Reify(optilab::gecodeutils::arg2boolvar(space, reifArg), rm),
          optilab::gecodeutils::ann2icl(ct->getPropagationType()));
    }
  } else {
    rel(space, optilab::gecodeutils::arg2intvar(space, rhsArg), swap(irt),
        lhsArg.getIntValue(),
        Gecode::Reify(optilab::gecodeutils::arg2boolvar(space, reifArg)),
        optilab::gecodeutils::ann2icl(ct->getPropagationType()));
  }
}  // p_int_CMP_reif

void p_int_lin_CMP(optilab::GecodeConstraint* ct, Gecode::IntRelType irt)
{
  const auto& lhsArg = ct->getArgumentList()[0];
  Gecode::IntArgs ia = optilab::gecodeutils::arg2intargs(lhsArg);

  const auto& varsArg = ct->getArgumentList()[1];
  auto& space = *(ct->getSpace());
  Gecode::IntVarArgs iv = optilab::gecodeutils::arg2intvarargs(space, varsArg);

  const auto& constArg = ct->getArgumentList()[2];
  Gecode::linear(space, ia, iv, irt, constArg.getIntValue(),
                 optilab::gecodeutils::ann2icl(ct->getPropagationType()));
}  // p_int_lin_CMP

void p_int_lin_CMP_reif(optilab::GecodeConstraint* ct, Gecode::IntRelType irt,
                        Gecode::ReifyMode rm)
{
  const auto& reifArg = ct->getArgumentList()[3];
  if (rm == Gecode::ReifyMode::RM_EQV && reifArg.isIntArgVal()) {
    if (reifArg.getIntValue() != 0) {
      p_int_lin_CMP(ct, irt);
    } else {
      p_int_lin_CMP(ct, neg(irt));
    }
    return;
  }

  const auto& lhsArg = ct->getArgumentList()[0];
  Gecode::IntArgs ia = optilab::gecodeutils::arg2intargs(lhsArg);

  const auto& varsArg = ct->getArgumentList()[1];
  auto& space = *(ct->getSpace());
  Gecode::IntVarArgs iv = optilab::gecodeutils::arg2intvarargs(space, varsArg);

  const auto& constArg = ct->getArgumentList()[2];
  Gecode::linear(space, ia, iv, irt, constArg.getIntValue(),
                 Gecode::Reify(optilab::gecodeutils::arg2boolvar(space, reifArg), rm),
                 optilab::gecodeutils::ann2icl(ct->getPropagationType()));
}  // p_int_lin_CMP_reif

void p_bool_lin_CMP(optilab::GecodeConstraint* ct, Gecode::IntRelType irt)
{
  const auto& lhsArg = ct->getArgumentList()[0];
  Gecode::IntArgs ia = optilab::gecodeutils::arg2intargs(lhsArg);

  const auto& varsArg = ct->getArgumentList()[1];
  auto& space = *(ct->getSpace());
  Gecode::BoolVarArgs iv = optilab::gecodeutils::arg2boolvarargs(space, varsArg);

  const auto& constArg = ct->getArgumentList()[2];
  if (constArg.isIntArgVal())
  {
    linear(space, ia, iv, irt, constArg.getIntValue(),
           optilab::gecodeutils::ann2icl(ct->getPropagationType()));
  }
  else
  {
    auto var = VAR_CAST(constArg.getVariable());
    auto constVar = space.getIntVar(var->getVarSpaceIdxList()[0]);
    Gecode::linear(space, ia, iv, irt, constVar,
                   optilab::gecodeutils::ann2icl(ct->getPropagationType()));
  }

}  // p_bool_lin_CMP

void p_bool_lin_CMP_reif(optilab::GecodeConstraint* ct, Gecode::IntRelType irt,
                         Gecode::ReifyMode rm)
{
  const auto& reifArg = ct->getArgumentList()[3];
  if (rm == Gecode::ReifyMode::RM_EQV && reifArg.isIntArgVal()) {
    if (reifArg.getIntValue() != 0) {
      p_bool_lin_CMP(ct, irt);
    } else {
      p_bool_lin_CMP(ct, neg(irt));
    }
    return;
  }

  const auto& lhsArg = ct->getArgumentList()[0];
  Gecode::IntArgs ia = optilab::gecodeutils::arg2intargs(lhsArg);

  const auto& varsArg = ct->getArgumentList()[1];
  auto& space = *(ct->getSpace());
  Gecode::IntVarArgs iv = optilab::gecodeutils::arg2intvarargs(space, varsArg);

  const auto& constArg = ct->getArgumentList()[2];
  if (constArg.isIntArgVal())
  {
    linear(space, ia, iv, irt, constArg.getIntValue(),
           Gecode::Reify(optilab::gecodeutils::arg2boolvar(space, reifArg), rm),
           optilab::gecodeutils::ann2icl(ct->getPropagationType()));
  }
  else
  {
    auto var = VAR_CAST(constArg.getVariable());
    auto constVar = space.getIntVar(var->getVarSpaceIdxList()[0]);
    Gecode::linear(space, ia, iv, irt, constVar,
                   Gecode::Reify(optilab::gecodeutils::arg2boolvar(space, reifArg), rm),
                   optilab::gecodeutils::ann2icl(ct->getPropagationType()));
  }
}  // p_bool_lin_CMP_reif

void p_bool_CMP(optilab::GecodeConstraint* ct, Gecode::IntRelType irt)
{
  const auto& lhsArg = ct->getArgumentList()[0];
  const auto& rhsArg = ct->getArgumentList()[1];
  auto& space = *(ct->getSpace());

  Gecode::rel(space, optilab::gecodeutils::arg2boolvar(space, lhsArg), irt,
              optilab::gecodeutils::arg2boolvar(space, rhsArg),
              optilab::gecodeutils::ann2icl(ct->getPropagationType()));
}  // p_bool_CMP

void p_bool_CMP_reif(optilab::GecodeConstraint* ct, Gecode::IntRelType irt,
                     Gecode::ReifyMode rm)
{
  const auto& lhsArg = ct->getArgumentList()[0];
  const auto& rhsArg = ct->getArgumentList()[1];
  const auto& reifArg = ct->getArgumentList()[2];
  auto& space = *(ct->getSpace());

  Gecode::rel(space, optilab::gecodeutils::arg2boolvar(space, lhsArg), irt,
              optilab::gecodeutils::arg2boolvar(space, rhsArg),
              Gecode::Reify(optilab::gecodeutils::arg2boolvar(space, reifArg), rm),
              optilab::gecodeutils::ann2icl(ct->getPropagationType()));
}  // p_bool_CMP_reif

}  // namespace

namespace optilab {

namespace gecodeutils {

Gecode::IntArgs arg2intargs(const CPArgument& arg, int offset)
{
  if (arg.valIntList.empty())
  {
    throw std::runtime_error("arg2intargs - invalid argument: not a list of integers");
  }
  const int size = static_cast<int>(arg.valIntList.size());
  Gecode::IntArgs ia(size + offset);
  for (int i=offset; i--;)
      ia[i] = 0;
  for (int i=size; i--;) {
      ia[i+offset] = arg.valIntList[i];
  }
  return ia;
}  // arg2intargs

Gecode::IntArgs arg2boolargs(const CPArgument& arg, int offset)
{
  if (arg.valIntList.empty())
  {
    throw std::runtime_error("arg2intargs - invalid argument: not a list of integers/Booleans");
  }
  const int size = static_cast<int>(arg.valIntList.size());
  Gecode::IntArgs ia(size + offset);
  for (int i=offset; i--;)
      ia[i] = 0;
  for (int i=size; i--;) {
      ia[i+offset] = arg.valIntList[i];
  }
  return ia;
}  // arg2boolargs

Gecode::IntVar arg2intvar(OptilabSpace& space, const CPArgument& arg)
{
  Gecode::IntVar x0;
  if (arg.hasOneIntValue())
  {
    if (arg.valIntList.size() != 1)
    {
      throw std::runtime_error("arg2intvar - cannot extract a variable from an int array "
          "argument: invalid size " + std::to_string(arg.valIntList.size()));
    }
    x0 = Gecode::IntVar(space, arg.getIntValue(), arg.getIntValue());
  }
  else if (arg.hasOneVar())
  {
    auto var = VAR_CAST(arg.getVariable());
    const int size = static_cast<int>(var->getVarSpaceIdxList().size());
    if (size != 1)
    {
      throw std::runtime_error("arg2intvar - cannot extract a variable from the argument: "
          "invalid size " + std::to_string(size));
    }

    if (!var->isVarInt())
    {
      throw std::runtime_error("arg2intvar - variable is not of integer type");
    }

    x0 = space.getIntVar(var->getVarSpaceIdxList()[0]);
  }
  else
  {
    throw std::runtime_error("arg2intvar - variable is not of integer type");
  }

  return x0;
} // arg2intvar

Gecode::BoolVar arg2boolvar(OptilabSpace& space, const CPArgument& arg)
{
  Gecode::BoolVar x0;
  if (arg.hasOneIntValue())
  {
    if (arg.valIntList.size() != 1)
    {
      throw std::runtime_error("arg2boolvar - cannot extract a variable from an int array "
          "argument: invalid size " + std::to_string(arg.valIntList.size()));
    }

    x0 = Gecode::BoolVar(space, arg.getIntValue(), arg.getIntValue());
  }
  else if (arg.hasOneVar())
  {
    auto var = VAR_CAST(arg.getVariable());
    const int size = static_cast<int>(var->getVarSpaceIdxList().size());
    if (size != 1)
    {
      throw std::runtime_error("arg2boolvar - cannot extract a variable from the argument: "
          "invalid size " + std::to_string(size));
    }

    if (!var->isVarInt())
    {
      throw std::runtime_error("arg2boolvar - variable is not of integer type");
    }

    x0 = space.getBoolVar(var->getVarSpaceIdxList()[0]);
  }
  else
  {
    throw std::runtime_error("arg2boolvar - variable is not of integer type");
  }

  return x0;
} // arg2boolvar

Gecode::IntVarArgs arg2intvarargs(OptilabSpace& space, const CPArgument& arg, int offset)
{
  if (arg.varList.empty() && arg.valIntList.empty())
  {
    Gecode::IntVarArgs emptyIa(0);
    return emptyIa;
  }

  int size;
  if (arg.isArgVar())
  {
    size = static_cast<int>(arg.varList.size());
  }
  else
  {
    if (!arg.isIntArgVal())
    {
      throw std::runtime_error("arg2intvarargs - cannot convert the argument into varargs");
    }
    size = static_cast<int>(arg.valIntList.size());
  }

  Gecode::IntVarArgs ia(size + offset);
  for (int i=offset; i--;)
    ia[i] = Gecode::IntVar(space, 0, 0);
  if (arg.isArgVar())
  {
    for (int i=size; i--;)
    {
      auto var = VAR_CAST(arg.varList[i]);
      const int size = static_cast<int>(var->getVarSpaceIdxList().size());
      if (size != 1)
      {
        throw std::runtime_error("arg2intvarargs - cannot extract a variable from the argument: "
            "invalid size " + std::to_string(size));
      }

      if (!var->isVarInt())
      {
        throw std::runtime_error("arg2intvar - variable is not of integer type");
      }

      ia[i+offset] = space.getIntVar(var->getVarSpaceIdxList()[0]);
    }
  }
  else
  {
    for (int i=size; i--;)
    {
      auto val = arg.valIntList[i];
      if(valueWithinBounds(val)) {
        Gecode::IntVar iv(space, val, val);
        ia[i+offset] = iv;
      } else {
        throw std::runtime_error("arg2intvarargs - value outside 32-bit int: " +
                                 std::to_string(val));
      }
    }
  }

  return ia;
}  // arg2intvarargs

Gecode::BoolVarArgs arg2boolvarargs(OptilabSpace& space, const CPArgument& arg, int offset,
                                    int siv)
{
  if (arg.varList.empty() && arg.valIntList.empty())
  {
    Gecode::BoolVarArgs emptyIa(0);
    return emptyIa;
  }

  int size;
  if (arg.isArgVar())
  {
    size = static_cast<int>(arg.varList.size());
  }
  else
  {
    if (!arg.isIntArgVal())
    {
      throw std::runtime_error("arg2intvarargs - cannot convert the argument into varargs");
    }
    size = static_cast<int>(arg.valIntList.size());
  }

  Gecode::BoolVarArgs ia(size + offset - (siv == -1 ? 0 : 1));
  for (int i=offset; i--;)
    ia[i] = Gecode::BoolVar(space, 0, 0);
  for (int i=0; i < size; i++)
  {
    if (i==siv)
        continue;
    if (arg.isArgVar())
    {
      auto var = VAR_CAST(arg.varList[i]);
      const int size = static_cast<int>(var->getVarSpaceIdxList().size());
      if (size != 1)
      {
        throw std::runtime_error("arg2intvarargs - cannot extract a variable from the argument: "
            "invalid size " + std::to_string(size));
      }

      if (!var->isVarInt())
      {
        throw std::runtime_error("arg2intvar - variable is not of integer type");
      }
      ia[offset++] = space.getBoolVar(var->getVarSpaceIdxList()[0]);
    }
    else
    {
      auto val = arg.valIntList[i];
      if(valueWithinBounds(val)) {
        Gecode::BoolVar iv(space, val, val);
        ia[i+offset] = iv;
      } else {
        throw std::runtime_error("arg2intvarargs - value outside 32-bit int: " +
                                 std::to_string(val));
      }
    }
  }

  return ia;
}  // arg2boolvarargs

Gecode::IntPropLevel ann2icl(CPConstraint::PropagationType propType)
{
  switch(propType) {
    case CPConstraint::PropagationType::PT_BOUNDS:
      return Gecode::IntPropLevel::IPL_BND;
    case CPConstraint::PropagationType::PT_DOMAIN:
      return Gecode::IntPropLevel::IPL_DOM;
    default:
      return Gecode::IntPropLevel::IPL_DEF;
  }
}  // ann2icl

}  // namespace gecodeutils

namespace gecodeconstraintposters {

void p_int_eq(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_int_CMP(c, Gecode::IntRelType::IRT_EQ);
}  // p_int_eq

void p_int_ne(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_int_CMP(c, Gecode::IntRelType::IRT_NQ);
}  // p_int_ne

void p_int_ge(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_int_CMP(c, Gecode::IntRelType::IRT_GQ);
}  // p_int_ge

void p_int_gt(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_int_CMP(c, Gecode::IntRelType::IRT_GR);
}  // p_int_gt

void p_int_le(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_int_CMP(c, Gecode::IntRelType::IRT_LQ);
}  // p_int_le

void p_int_lt(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_int_CMP(c, Gecode::IntRelType::IRT_LE);
}  // p_int_lt

void p_int_eq_reif(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_int_CMP_reif(c, Gecode::IntRelType::IRT_EQ, Gecode::ReifyMode::RM_EQV);
}

void p_int_ne_reif(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_int_CMP_reif(c, Gecode::IntRelType::IRT_NQ, Gecode::ReifyMode::RM_EQV);
}

void p_int_ge_reif(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_int_CMP_reif(c, Gecode::IntRelType::IRT_GQ, Gecode::ReifyMode::RM_EQV);
}

void p_int_gt_reif(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_int_CMP_reif(c, Gecode::IntRelType::IRT_GR, Gecode::ReifyMode::RM_EQV);
}

void p_int_le_reif(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_int_CMP_reif(c, Gecode::IntRelType::IRT_LQ, Gecode::ReifyMode::RM_EQV);
}

void p_int_lt_reif(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_int_CMP_reif(c, Gecode::IntRelType::IRT_LE, Gecode::ReifyMode::RM_EQV);
}

void p_int_eq_imp(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_int_CMP_reif(c, Gecode::IntRelType::IRT_EQ, Gecode::ReifyMode::RM_IMP);
}

void p_int_ne_imp(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_int_CMP_reif(c, Gecode::IntRelType::IRT_NQ, Gecode::ReifyMode::RM_IMP);
}

void p_int_ge_imp(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_int_CMP_reif(c, Gecode::IntRelType::IRT_GQ, Gecode::ReifyMode::RM_IMP);
}

void p_int_gt_imp(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_int_CMP_reif(c, Gecode::IntRelType::IRT_GR, Gecode::ReifyMode::RM_IMP);
}

void p_int_le_imp(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_int_CMP_reif(c, Gecode::IntRelType::IRT_LQ, Gecode::ReifyMode::RM_IMP);
}

void p_int_lt_imp(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_int_CMP_reif(c, Gecode::IntRelType::IRT_LE, Gecode::ReifyMode::RM_IMP);
}

void p_int_lin_eq(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_int_lin_CMP(c, Gecode::IntRelType::IRT_EQ);
}

void p_int_lin_eq_reif(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_int_lin_CMP_reif(c, Gecode::IntRelType::IRT_EQ, Gecode::ReifyMode::RM_EQV);
}

void p_int_lin_eq_imp(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_int_lin_CMP_reif(c, Gecode::IntRelType::IRT_EQ, Gecode::ReifyMode::RM_IMP);
}

void p_int_lin_ne(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_int_lin_CMP(c, Gecode::IntRelType::IRT_NQ);
}

void p_int_lin_ne_reif(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_int_lin_CMP_reif(c, Gecode::IntRelType::IRT_NQ, Gecode::ReifyMode::RM_EQV);
}

void p_int_lin_ne_imp(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_int_lin_CMP_reif(c, Gecode::IntRelType::IRT_NQ, Gecode::ReifyMode::RM_IMP);
}

void p_int_lin_le(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_int_lin_CMP(c, Gecode::IntRelType::IRT_LQ);
}

void p_int_lin_le_reif(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_int_lin_CMP_reif(c, Gecode::IntRelType::IRT_LQ, Gecode::ReifyMode::RM_EQV);
}

void p_int_lin_le_imp(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_int_lin_CMP_reif(c, Gecode::IntRelType::IRT_LQ, Gecode::ReifyMode::RM_IMP);
}

void p_int_lin_lt(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_int_lin_CMP(c, Gecode::IntRelType::IRT_LE);
}

void p_int_lin_lt_reif(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_int_lin_CMP_reif(c, Gecode::IntRelType::IRT_LE, Gecode::ReifyMode::RM_EQV);
}

void p_int_lin_lt_imp(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_int_lin_CMP_reif(c, Gecode::IntRelType::IRT_LE, Gecode::ReifyMode::RM_IMP);
}

void p_int_lin_ge(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_int_lin_CMP(c, Gecode::IntRelType::IRT_GQ);
}

void p_int_lin_ge_reif(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_int_lin_CMP_reif(c, Gecode::IntRelType::IRT_GQ, Gecode::ReifyMode::RM_EQV);
}

void p_int_lin_ge_imp(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_int_lin_CMP_reif(c, Gecode::IntRelType::IRT_GQ, Gecode::ReifyMode::RM_IMP);
}

void p_int_lin_gt(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_int_lin_CMP(c, Gecode::IntRelType::IRT_GR);
}

void p_int_lin_gt_reif(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_int_lin_CMP_reif(c, Gecode::IntRelType::IRT_GR, Gecode::ReifyMode::RM_EQV);
}

void p_int_lin_gt_imp(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_int_lin_CMP_reif(c, Gecode::IntRelType::IRT_GR, Gecode::ReifyMode::RM_IMP);
}

void p_bool_lin_eq(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_bool_lin_CMP(c, Gecode::IntRelType::IRT_EQ);
}

void p_bool_lin_eq_reif(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_bool_lin_CMP_reif(c, Gecode::IntRelType::IRT_EQ, Gecode::ReifyMode::RM_EQV);
}

void p_bool_lin_eq_imp(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_bool_lin_CMP_reif(c, Gecode::IntRelType::IRT_EQ, Gecode::ReifyMode::RM_IMP);
}

void p_bool_lin_ne(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_bool_lin_CMP(c, Gecode::IntRelType::IRT_NQ);
}

void p_bool_lin_nq_reif(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_bool_lin_CMP_reif(c, Gecode::IntRelType::IRT_NQ, Gecode::ReifyMode::RM_EQV);
}

void p_bool_lin_nq_imp(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_bool_lin_CMP_reif(c, Gecode::IntRelType::IRT_NQ, Gecode::ReifyMode::RM_IMP);
}

void p_bool_lin_le(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_bool_lin_CMP(c, Gecode::IntRelType::IRT_LQ);
}

void p_bool_lin_le_reif(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_bool_lin_CMP_reif(c, Gecode::IntRelType::IRT_LQ, Gecode::ReifyMode::RM_EQV);
}

void p_bool_lin_le_imp(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_bool_lin_CMP_reif(c, Gecode::IntRelType::IRT_LQ, Gecode::ReifyMode::RM_IMP);
}

void p_bool_lin_lt(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_bool_lin_CMP(c, Gecode::IntRelType::IRT_LE);
}

void p_bool_lin_lt_reif(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_bool_lin_CMP_reif(c, Gecode::IntRelType::IRT_LE, Gecode::ReifyMode::RM_EQV);
}

void p_bool_lin_lt_imp(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_bool_lin_CMP_reif(c, Gecode::IntRelType::IRT_LE, Gecode::ReifyMode::RM_IMP);
}

void p_bool_lin_ge(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_bool_lin_CMP(c, Gecode::IntRelType::IRT_GQ);
}

void p_bool_lin_ge_reif(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_bool_lin_CMP_reif(c, Gecode::IntRelType::IRT_GQ, Gecode::ReifyMode::RM_EQV);
}

void p_bool_lin_ge_imp(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_bool_lin_CMP_reif(c, Gecode::IntRelType::IRT_GQ, Gecode::ReifyMode::RM_IMP);
}

void p_bool_lin_gt(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_bool_lin_CMP(c, Gecode::IntRelType::IRT_GR);
}

void p_bool_lin_gt_reif(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_bool_lin_CMP_reif(c, Gecode::IntRelType::IRT_GR, Gecode::ReifyMode::RM_EQV);
}

void p_bool_lin_gt_imp(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_bool_lin_CMP_reif(c, Gecode::IntRelType::IRT_GR, Gecode::ReifyMode::RM_IMP);
}

void p_int_plus(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  const auto& arg_0 = c->getArgumentList()[0];
  const auto& arg_1 = c->getArgumentList()[1];
  const auto& arg_2 = c->getArgumentList()[2];

  auto& space = *(c->getSpace());
  if (!arg_0.isArgVar())
  {
    Gecode::rel(space,
                arg_0.getIntValue() +
                optilab::gecodeutils::arg2intvar(space, arg_1)
    == optilab::gecodeutils::arg2intvar(space, arg_2),
    optilab::gecodeutils::ann2icl(c->getPropagationType()));
  }
  else if (!arg_1.isArgVar())
  {
    Gecode::rel(space,
                optilab::gecodeutils::arg2intvar(space, arg_0) +
                arg_1.getIntValue()
    == optilab::gecodeutils::arg2intvar(space, arg_2),
    optilab::gecodeutils::ann2icl(c->getPropagationType()));
  }
  else if (!arg_2.isArgVar())
  {
    Gecode::rel(space,
                optilab::gecodeutils::arg2intvar(space, arg_0) +
                optilab::gecodeutils::arg2intvar(space, arg_1)
    == arg_2.getIntValue(),
    optilab::gecodeutils::ann2icl(c->getPropagationType()));
  }
  else
  {
    Gecode::rel(space,
                optilab::gecodeutils::arg2intvar(space, arg_0) +
                optilab::gecodeutils::arg2intvar(space, arg_1)
    == optilab::gecodeutils::arg2intvar(space, arg_2),
    optilab::gecodeutils::ann2icl(c->getPropagationType()));
  }
}

void p_int_minus(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  const auto& arg_0 = c->getArgumentList()[0];
  const auto& arg_1 = c->getArgumentList()[1];
  const auto& arg_2 = c->getArgumentList()[2];

  auto& space = *(c->getSpace());
  if (!arg_0.isArgVar())
  {
    Gecode::rel(space,
                arg_0.getIntValue() -
                optilab::gecodeutils::arg2intvar(space, arg_1)
    == optilab::gecodeutils::arg2intvar(space, arg_2),
    optilab::gecodeutils::ann2icl(c->getPropagationType()));
  }
  else if (!arg_1.isArgVar())
  {
    Gecode::rel(space,
                optilab::gecodeutils::arg2intvar(space, arg_0) -
                arg_1.getIntValue()
    == optilab::gecodeutils::arg2intvar(space, arg_2),
    optilab::gecodeutils::ann2icl(c->getPropagationType()));
  }
  else if (!arg_2.isArgVar())
  {
    Gecode::rel(space,
                optilab::gecodeutils::arg2intvar(space, arg_0) -
                optilab::gecodeutils::arg2intvar(space, arg_1)
    == arg_2.getIntValue(),
    optilab::gecodeutils::ann2icl(c->getPropagationType()));
  }
  else
  {
    Gecode::rel(space,
                optilab::gecodeutils::arg2intvar(space, arg_0) -
                optilab::gecodeutils::arg2intvar(space, arg_1)
    == optilab::gecodeutils::arg2intvar(space, arg_2),
    optilab::gecodeutils::ann2icl(c->getPropagationType()));
  }
}

void p_int_times(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  const auto& arg_0 = c->getArgumentList()[0];
  const auto& arg_1 = c->getArgumentList()[1];
  const auto& arg_2 = c->getArgumentList()[2];

  auto& space = *(c->getSpace());
  Gecode::IntVar x0 = optilab::gecodeutils::arg2intvar(space, arg_0);
  Gecode::IntVar x1 = optilab::gecodeutils::arg2intvar(space, arg_1);
  Gecode::IntVar x2 = optilab::gecodeutils::arg2intvar(space, arg_2);

  Gecode::mult(space, x0, x1, x2, optilab::gecodeutils::ann2icl(c->getPropagationType()));
}

void p_int_div(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  const auto& arg_0 = c->getArgumentList()[0];
  const auto& arg_1 = c->getArgumentList()[1];
  const auto& arg_2 = c->getArgumentList()[2];

  auto& space = *(c->getSpace());
  Gecode::IntVar x0 = optilab::gecodeutils::arg2intvar(space, arg_0);
  Gecode::IntVar x1 = optilab::gecodeutils::arg2intvar(space, arg_1);
  Gecode::IntVar x2 = optilab::gecodeutils::arg2intvar(space, arg_2);

  Gecode::div(space, x0, x1, x2, optilab::gecodeutils::ann2icl(c->getPropagationType()));
}

void p_int_mod(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  const auto& arg_0 = c->getArgumentList()[0];
  const auto& arg_1 = c->getArgumentList()[1];
  const auto& arg_2 = c->getArgumentList()[2];

  auto& space = *(c->getSpace());
  Gecode::IntVar x0 = optilab::gecodeutils::arg2intvar(space, arg_0);
  Gecode::IntVar x1 = optilab::gecodeutils::arg2intvar(space, arg_1);
  Gecode::IntVar x2 = optilab::gecodeutils::arg2intvar(space, arg_2);

  Gecode::mod(space, x0, x1, x2, optilab::gecodeutils::ann2icl(c->getPropagationType()));
}

void p_int_min(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  const auto& arg_0 = c->getArgumentList()[0];
  const auto& arg_1 = c->getArgumentList()[1];
  const auto& arg_2 = c->getArgumentList()[2];

  auto& space = *(c->getSpace());
  Gecode::IntVar x0 = optilab::gecodeutils::arg2intvar(space, arg_0);
  Gecode::IntVar x1 = optilab::gecodeutils::arg2intvar(space, arg_1);
  Gecode::IntVar x2 = optilab::gecodeutils::arg2intvar(space, arg_2);

  Gecode::min(space, x0, x1, x2, optilab::gecodeutils::ann2icl(c->getPropagationType()));
}

void p_int_max(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  const auto& arg_0 = c->getArgumentList()[0];
  const auto& arg_1 = c->getArgumentList()[1];
  const auto& arg_2 = c->getArgumentList()[2];

  auto& space = *(c->getSpace());
  Gecode::IntVar x0 = optilab::gecodeutils::arg2intvar(space, arg_0);
  Gecode::IntVar x1 = optilab::gecodeutils::arg2intvar(space, arg_1);
  Gecode::IntVar x2 = optilab::gecodeutils::arg2intvar(space, arg_2);

  Gecode::max(space, x0, x1, x2, optilab::gecodeutils::ann2icl(c->getPropagationType()));
}

void p_abs(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  const auto& arg_0 = c->getArgumentList()[0];
  const auto& arg_1 = c->getArgumentList()[1];

  auto& space = *(c->getSpace());
  Gecode::IntVar x0 = optilab::gecodeutils::arg2intvar(space, arg_0);
  Gecode::IntVar x1 = optilab::gecodeutils::arg2intvar(space, arg_1);

  Gecode::abs(space, x0, x1, optilab::gecodeutils::ann2icl(c->getPropagationType()));
}

void p_int_negate(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  const auto& arg_0 = c->getArgumentList()[0];
  const auto& arg_1 = c->getArgumentList()[1];

  auto& space = *(c->getSpace());
  Gecode::IntVar x0 = optilab::gecodeutils::arg2intvar(space, arg_0);
  Gecode::IntVar x1 = optilab::gecodeutils::arg2intvar(space, arg_1);

  Gecode::rel(space, x0 == -x1, optilab::gecodeutils::ann2icl(c->getPropagationType()));
}

void p_bool_eq(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_bool_CMP(c, Gecode::IntRelType::IRT_EQ);
}

void p_bool_eq_reif(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_bool_CMP_reif(c, Gecode::IntRelType::IRT_EQ, Gecode::ReifyMode::RM_EQV);
}

void p_bool_eq_imp(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_bool_CMP_reif(c, Gecode::IntRelType::IRT_EQ, Gecode::ReifyMode::RM_IMP);
}

void p_bool_ne(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_bool_CMP(c, Gecode::IntRelType::IRT_NQ);
}

void p_bool_ne_reif(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_bool_CMP_reif(c, Gecode::IntRelType::IRT_NQ, Gecode::ReifyMode::RM_EQV);
}

void p_bool_ne_imp(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_bool_CMP_reif(c, Gecode::IntRelType::IRT_NQ, Gecode::ReifyMode::RM_IMP);
}

void p_bool_ge(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_bool_CMP(c, Gecode::IntRelType::IRT_GQ);
}

void p_bool_ge_reif(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_bool_CMP_reif(c, Gecode::IntRelType::IRT_GQ, Gecode::ReifyMode::RM_EQV);
}

void p_bool_ge_imp(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_bool_CMP_reif(c, Gecode::IntRelType::IRT_GQ, Gecode::ReifyMode::RM_IMP);
}

void p_bool_le(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_bool_CMP(c, Gecode::IntRelType::IRT_LQ);
}

void p_bool_le_reif(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_bool_CMP_reif(c, Gecode::IntRelType::IRT_LQ, Gecode::ReifyMode::RM_EQV);
}

void p_bool_le_imp(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_bool_CMP_reif(c, Gecode::IntRelType::IRT_LQ, Gecode::ReifyMode::RM_IMP);
}

void p_bool_gt(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_bool_CMP(c, Gecode::IntRelType::IRT_GR);
}

void p_bool_gt_reif(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_bool_CMP_reif(c, Gecode::IntRelType::IRT_GR, Gecode::ReifyMode::RM_EQV);
}

void p_bool_gt_imp(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_bool_CMP_reif(c, Gecode::IntRelType::IRT_GR, Gecode::ReifyMode::RM_IMP);
}

void p_bool_lt(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_bool_CMP(c, Gecode::IntRelType::IRT_LE);
}

void p_bool_lt_reif(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_bool_CMP_reif(c, Gecode::IntRelType::IRT_LE, Gecode::ReifyMode::RM_EQV);
}

void p_bool_lt_imp(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  p_bool_CMP_reif(c, Gecode::IntRelType::IRT_LE, Gecode::ReifyMode::RM_IMP);
}

#define BOOL_OP(op) \
    Gecode::BoolVar b0 = optilab::gecodeutils::arg2boolvar(space, arg_0); \
    Gecode::BoolVar b1 = optilab::gecodeutils::arg2boolvar(space, arg_1); \
    if (!arg_2.isArgVar()) { \
      Gecode::rel(space, b0, op, b1, (arg_2.getIntValue() != 0) ? true : false, \
          optilab::gecodeutils::ann2icl(c->getPropagationType())); \
    } else { \
      Gecode::rel(space, b0, op, b1, optilab::gecodeutils::arg2boolvar(space, arg_2), \
                  optilab::gecodeutils::ann2icl(c->getPropagationType())); \
    }


#define BOOL_ARRAY_OP(op) \
    Gecode::BoolVarArgs bv = optilab::gecodeutils::arg2boolvarargs(space, arg_0); \
    if (!arg_1.isArgVar()) { \
      Gecode::rel(space, op, bv, (arg_1.getIntValue() != 0) ? true : false, \
          optilab::gecodeutils::ann2icl(c->getPropagationType())); \
    } else { \
      Gecode::rel(space, op, bv, optilab::gecodeutils::arg2boolvar(space, arg_1), \
                  optilab::gecodeutils::ann2icl(c->getPropagationType())); \
    }

void p_bool_or(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  auto& space = *(c->getSpace());
  const auto& arg_0 = c->getArgumentList()[0];
  const auto& arg_1 = c->getArgumentList()[1];
  const auto& arg_2 = c->getArgumentList()[2];

  BOOL_OP(Gecode::BoolOpType::BOT_OR);
}

void p_bool_or_imp(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  auto& space = *(c->getSpace());
  const auto& arg_0 = c->getArgumentList()[0];
  const auto& arg_1 = c->getArgumentList()[1];
  const auto& arg_2 = c->getArgumentList()[2];

  Gecode::BoolVar b0 = optilab::gecodeutils::arg2boolvar(space, arg_0);
  Gecode::BoolVar b1 = optilab::gecodeutils::arg2boolvar(space, arg_1);
  Gecode::BoolVar b2 = optilab::gecodeutils::arg2boolvar(space, arg_2);

  Gecode::clause(space, Gecode::BoolOpType::BOT_OR, Gecode::BoolVarArgs()<<b0<<b1,
                 Gecode::BoolVarArgs()<<b2, 1,
                 optilab::gecodeutils::ann2icl(c->getPropagationType()));
}

void p_bool_and(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  auto& space = *(c->getSpace());
  const auto& arg_0 = c->getArgumentList()[0];
  const auto& arg_1 = c->getArgumentList()[1];
  const auto& arg_2 = c->getArgumentList()[2];

  BOOL_OP(Gecode::BoolOpType::BOT_AND);
}

void p_bool_and_imp(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  auto& space = *(c->getSpace());
  const auto& arg_0 = c->getArgumentList()[0];
  const auto& arg_1 = c->getArgumentList()[1];
  const auto& arg_2 = c->getArgumentList()[2];

  Gecode::BoolVar b0 = optilab::gecodeutils::arg2boolvar(space, arg_0);
  Gecode::BoolVar b1 = optilab::gecodeutils::arg2boolvar(space, arg_1);
  Gecode::BoolVar b2 = optilab::gecodeutils::arg2boolvar(space, arg_2);

  Gecode::rel(space, b2, Gecode::BoolOpType::BOT_IMP, b0, 1,
              optilab::gecodeutils::ann2icl(c->getPropagationType()));
  Gecode::rel(space, b2, Gecode::BoolOpType::BOT_IMP, b1, 1,
              optilab::gecodeutils::ann2icl(c->getPropagationType()));
}  // namespace gecodeconstraintposters

void p_array_bool_and(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  auto& space = *(c->getSpace());
  const auto& arg_0 = c->getArgumentList()[0];
  const auto& arg_1 = c->getArgumentList()[1];
  BOOL_ARRAY_OP(Gecode::BoolOpType::BOT_AND);
}

void p_array_bool_and_imp(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  auto& space = *(c->getSpace());
  const auto& arg_0 = c->getArgumentList()[0];
  const auto& arg_1 = c->getArgumentList()[1];
  Gecode::BoolVarArgs bv = optilab::gecodeutils::arg2boolvarargs(space, arg_0);
  Gecode::BoolVar b1 = optilab::gecodeutils::arg2boolvar(space, arg_1);
  for (unsigned int i=bv.size(); i--;)
    Gecode::rel(space, b1, Gecode::BoolOpType::BOT_IMP, bv[i], 1,
                optilab::gecodeutils::ann2icl(c->getPropagationType()));
}

void p_array_bool_or(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  auto& space = *(c->getSpace());
  const auto& arg_0 = c->getArgumentList()[0];
  const auto& arg_1 = c->getArgumentList()[1];
  BOOL_ARRAY_OP(Gecode::BoolOpType::BOT_OR);
}

void p_array_bool_or_imp(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  auto& space = *(c->getSpace());
  const auto& arg_0 = c->getArgumentList()[0];
  const auto& arg_1 = c->getArgumentList()[1];

  Gecode::BoolVarArgs bv = optilab::gecodeutils::arg2boolvarargs(space, arg_0);
  Gecode::BoolVar b1 = optilab::gecodeutils::arg2boolvar(space, arg_1);

  Gecode::clause(space, Gecode::BoolOpType::BOT_OR, bv, Gecode::BoolVarArgs()<<b1, 1,
                 optilab::gecodeutils::ann2icl(c->getPropagationType()));
}

void p_array_bool_xor(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  auto& space = *(c->getSpace());
  const auto& arg_0 = c->getArgumentList()[0];
  const auto& arg_1 = c->getArgumentList()[1];
  BOOL_ARRAY_OP(Gecode::BoolOpType::BOT_XOR);
}

void p_array_bool_xor_imp(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  auto& space = *(c->getSpace());
  const auto& arg_0 = c->getArgumentList()[0];
  const auto& arg_1 = c->getArgumentList()[1];

  Gecode::BoolVarArgs bv = optilab::gecodeutils::arg2boolvarargs(space, arg_0);
  Gecode::BoolVar tmp(space, 0, 1);

  Gecode::rel(space, Gecode::BoolOpType::BOT_XOR, bv, tmp,
              optilab::gecodeutils::ann2icl(c->getPropagationType()));
  Gecode::rel(space, optilab::gecodeutils::arg2boolvar(space, arg_1), Gecode::BoolOpType::BOT_IMP,
              tmp, 1);
}

void p_array_bool_clause(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  auto& space = *(c->getSpace());
  const auto& arg_0 = c->getArgumentList()[0];
  const auto& arg_1 = c->getArgumentList()[1];

  Gecode::BoolVarArgs bvp = optilab::gecodeutils::arg2boolvarargs(space, arg_0);
  Gecode::BoolVarArgs bvn = optilab::gecodeutils::arg2boolvarargs(space, arg_1);

  Gecode::clause(space, Gecode::BoolOpType::BOT_OR, bvp, bvn, 1,
                 optilab::gecodeutils::ann2icl(c->getPropagationType()));
}

void p_array_bool_clause_reif(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  auto& space = *(c->getSpace());
  const auto& arg_0 = c->getArgumentList()[0];
  const auto& arg_1 = c->getArgumentList()[1];
  const auto& arg_2 = c->getArgumentList()[2];

  Gecode::BoolVarArgs bvp = optilab::gecodeutils::arg2boolvarargs(space, arg_0);
  Gecode::BoolVarArgs bvn = optilab::gecodeutils::arg2boolvarargs(space, arg_1);
  Gecode::BoolVar b0 = optilab::gecodeutils::arg2boolvar(space, arg_2);

  Gecode::clause(space, Gecode::BoolOpType::BOT_OR, bvp, bvn, b0,
                 optilab::gecodeutils::ann2icl(c->getPropagationType()));
}

void p_array_bool_clause_imp(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  auto& space = *(c->getSpace());
  const auto& arg_0 = c->getArgumentList()[0];
  const auto& arg_1 = c->getArgumentList()[1];
  const auto& arg_2 = c->getArgumentList()[2];

  Gecode::BoolVarArgs bvp = optilab::gecodeutils::arg2boolvarargs(space, arg_0);
  Gecode::BoolVarArgs bvn = optilab::gecodeutils::arg2boolvarargs(space, arg_1);
  Gecode::BoolVar b0 = optilab::gecodeutils::arg2boolvar(space, arg_2);

  Gecode::clause(space, Gecode::BoolOpType::BOT_OR, bvp, bvn, b0,
                 optilab::gecodeutils::ann2icl(c->getPropagationType()));
}

void p_bool_xor(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  auto& space = *(c->getSpace());
  const auto& arg_0 = c->getArgumentList()[0];
  const auto& arg_1 = c->getArgumentList()[1];
  const auto& arg_2 = c->getArgumentList()[2];
  BOOL_OP(Gecode::BoolOpType::BOT_XOR);
}

void p_bool_xor_imp(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  auto& space = *(c->getSpace());
  const auto& arg_0 = c->getArgumentList()[0];
  const auto& arg_1 = c->getArgumentList()[1];
  const auto& arg_2 = c->getArgumentList()[2];

  Gecode::BoolVar b0 = optilab::gecodeutils::arg2boolvar(space, arg_0);
  Gecode::BoolVar b1 = optilab::gecodeutils::arg2boolvar(space, arg_1);
  Gecode::BoolVar b2 = optilab::gecodeutils::arg2boolvar(space, arg_2);

  Gecode::clause(space, Gecode::BoolOpType::BOT_OR, Gecode::BoolVarArgs()<<b0<<b1,
                 Gecode::BoolVarArgs()<<b2, 1,
                 optilab::gecodeutils::ann2icl(c->getPropagationType()));
  Gecode::clause(space, Gecode::BoolOpType::BOT_OR, Gecode::BoolVarArgs(),
                 Gecode::BoolVarArgs()<<b0<<b1<<b2, 1,
                 optilab::gecodeutils::ann2icl(c->getPropagationType()));
}

void p_bool_l_imp(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  auto& space = *(c->getSpace());
  const auto& arg_0 = c->getArgumentList()[0];
  const auto& arg_1 = c->getArgumentList()[1];
  const auto& arg_2 = c->getArgumentList()[2];

  Gecode::BoolVar b0 = optilab::gecodeutils::arg2boolvar(space, arg_0);
  Gecode::BoolVar b1 = optilab::gecodeutils::arg2boolvar(space, arg_1);
  if (!arg_2.isArgVar())
  {
    Gecode::rel(space, b1, Gecode::BoolOpType::BOT_IMP, b0,
                (arg_2.getIntValue() != 0) ? true : false,
                optilab::gecodeutils::ann2icl(c->getPropagationType()));
  }
  else
  {
    Gecode::rel(space, b1, Gecode::BoolOpType::BOT_IMP, b0,
                optilab::gecodeutils::arg2boolvar(space, arg_2),
                optilab::gecodeutils::ann2icl(c->getPropagationType()));
  }
}

void p_bool_r_imp(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  auto& space = *(c->getSpace());
  const auto& arg_0 = c->getArgumentList()[0];
  const auto& arg_1 = c->getArgumentList()[1];
  const auto& arg_2 = c->getArgumentList()[1];
  BOOL_OP(Gecode::BoolOpType::BOT_IMP);
}

void p_bool_not(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  auto& space = *(c->getSpace());
  const auto& arg_0 = c->getArgumentList()[0];
  const auto& arg_1 = c->getArgumentList()[1];

  Gecode::BoolVar x0 = optilab::gecodeutils::arg2boolvar(space, arg_0);
  Gecode::BoolVar x1 = optilab::gecodeutils::arg2boolvar(space, arg_1);

  Gecode::rel(space, x0, Gecode::BoolOpType::BOT_XOR, x1, 1,
              optilab::gecodeutils::ann2icl(c->getPropagationType()));
}

void p_array_int_element(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  auto& space = *(c->getSpace());
  const auto& arg_0 = c->getArgumentList()[0];
  const auto& arg_1 = c->getArgumentList()[1];
  const auto& arg_2 = c->getArgumentList()[2];

  Gecode::IntVar selector = optilab::gecodeutils::arg2intvar(space, arg_0);
  Gecode::rel(space, selector > 0);

  if (!arg_1.isArgVar())
  {
    Gecode::IntVarArgs iv = optilab::gecodeutils::arg2intvarargs(space, arg_1);
    Gecode::element(space, iv, selector, optilab::gecodeutils::arg2intvar(space, arg_2),
                    optilab::gecodeutils::ann2icl(c->getPropagationType()));
  }
  else
  {
    Gecode::IntArgs ia = optilab::gecodeutils::arg2intargs(arg_1);
    Gecode::element(space, ia, selector, optilab::gecodeutils::arg2intvar(space, arg_2),
                    optilab::gecodeutils::ann2icl(c->getPropagationType()));
  }
}

void p_array_bool_element(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  auto& space = *(c->getSpace());
  const auto& arg_0 = c->getArgumentList()[0];
  const auto& arg_1 = c->getArgumentList()[1];
  const auto& arg_2 = c->getArgumentList()[2];

  Gecode::IntVar selector = optilab::gecodeutils::arg2intvar(space, arg_0);
  Gecode::rel(space, selector > 0);

  if (!arg_1.isArgVar())
  {
    Gecode::BoolVarArgs iv = optilab::gecodeutils::arg2boolvarargs(space, arg_1);
    Gecode::element(space, iv, selector, optilab::gecodeutils::arg2boolvar(space, arg_2),
                    optilab::gecodeutils::ann2icl(c->getPropagationType()));
  }
  else
  {
    Gecode::IntArgs ia = optilab::gecodeutils::arg2boolargs(arg_1);
    Gecode::element(space, ia, selector, optilab::gecodeutils::arg2boolvar(space, arg_2),
                    optilab::gecodeutils::ann2icl(c->getPropagationType()));
  }
}

void p_bool2int(CPConstraint::SPtr con)
{
  auto c = CON_CAST(con);
  auto& space = *(c->getSpace());
  const auto& arg_0 = c->getArgumentList()[0];
  const auto& arg_1 = c->getArgumentList()[1];

  Gecode::BoolVar x0 = optilab::gecodeutils::arg2boolvar(space, arg_0);
  Gecode::IntVar x1 = optilab::gecodeutils::arg2intvar(space, arg_1);
  Gecode::channel(space, x0, x1, optilab::gecodeutils::ann2icl(c->getPropagationType()));
}

}  // namespace optilab
} // namespace optilab
