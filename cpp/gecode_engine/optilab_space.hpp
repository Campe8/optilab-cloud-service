//
// Copyright OptiLab 2019. All rights reserved.
//
// OptiLab Gecode derived space.
// For more information, see
// https://www.gecode.org/doc-latest/MPG.pdf.
//
// @note this class is very similar to the FznSpace
// created for FlatZinc models.
// For example, see "fzn_space.hh".
//

#pragma once

#include <cstddef>  // for std::size_t
#include <memory>   // for std::shared_ptr
#include <vector>

#include <gecode/kernel.hh>
#include <gecode/int.hh>
#include <gecode/driver.hh>

#ifdef GECODE_HAS_SET_VARS
#include <gecode/set.hh>
#endif

#ifdef GECODE_HAS_FLOAT_VARS
#include <gecode/float.hh>
#endif

#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS OptilabSpace : public Gecode::Space {
 public:
  /// Solving type
  enum SolveType {
    /// Satisfiability
    SOLVE_TYPE_SAT = 0,

    /// Minimization
    SOLVE_TYPE_MIN,

    /// Maximization
    SOLVE_TYPE_MAX
  };

  using SPtr = std::shared_ptr<OptilabSpace>;

 public:
  /// Constructor creates a new Gecode OptiLab space
  OptilabSpace();

  /// Copy constructor
  OptilabSpace(OptilabSpace&);

  virtual ~OptilabSpace() = default;

  void setSolveType(SolveType solveType) { pSolveType = solveType; }
  inline SolveType getSolveType() const { return pSolveType; }

  std::size_t addBoolVar(const Gecode::BoolVar& var);
  std::size_t addIntVar(const Gecode::IntVar& var);

  /// Returns the Boolean variable at given index.
  /// @note throws std::out_of_bound on invalid index
  const Gecode::BoolVar& getBoolVar(std::size_t idx) const;

  /// Returns the Integer variable at given index.
  /// @note throws std::out_of_bound on invalid index
  const Gecode::IntVar& getIntVar(std::size_t idx) const;

#ifdef GECODE_HAS_SET_VARS
  std::size_t addSetVar(const Gecode::SetVar& var);

  /// Returns the Set variable at given index.
  /// @note throws std::out_of_bound on invalid index
  const Gecode::SetVar& getSetVar(std::size_t idx) const;
#endif

#ifdef GECODE_HAS_FLOAT_VARS
  std::size_t addFloatVar(const Gecode::FloatVar& var);

  /// Returns the Float variable at given index.
  /// @note throws std::out_of_bound on invalid index
  const Gecode::FloatVar& getFloatVar(std::size_t idx) const;
#endif

 protected:
  /// The integer variables
  std::vector<Gecode::IntVar> pIntVarList;

  /// The Boolean variables
  std::vector<Gecode::BoolVar> pBoolVarList;

#ifdef GECODE_HAS_SET_VARS
  /// The set variables
  std::vector<Gecode::SetVar> pSetVarList;
#endif

#ifdef GECODE_HAS_FLOAT_VARS
  /// The float variables
  std::vector<Gecode::FloatVar> pFloatVarList;
#endif

  /// Indicates if the objective variable is integer (float otherwise)
  bool pOptVarIsInt{true};

  /// Index of the variable to optimize
  int pOptVarIdx{-1};

  /// solve type (SAT, MIN or MAX)
  SolveType  pSolveType;

  /// Implements optimization
  virtual void constrain(const Space& s);

  /// Copy function
  virtual Gecode::Space* copy(void);
};

}  // namespace optilab
