#include "gecode_engine/gecode_optimizer.hpp"

#include <cassert>
#include <stdexcept>  // for std::invalid_argument

#include <spdlog/spdlog.h>

namespace optilab {

GecodeOptimizer::GecodeOptimizer(const std::string& engineName,
                                 gecodeengine::GecodeResult::SPtr result,
                                 MetricsRegister::SPtr metricsRegister)
: BaseClass(engineName),
  pOptimizerModel(nullptr),
  pGecodeProcessor(nullptr),
  pResult(result),
  pMetricsRegister(metricsRegister)
{
  if (!pResult)
  {
    throw std::runtime_error("GecodeOptimizer - empty result");
  }

  if (!pMetricsRegister)
  {
    throw std::invalid_argument("GecodeOptimizer - empty pointer to the metrics register");
  }

  // Initialize the engine,
  // e.g., the internal pipeline
  BaseClass::init();
}

void GecodeOptimizer::loadModel(const std::string& jsonModel)
{
  // Load the model into the optimizer
  assert(pGecodeProcessor);

  // Load the model from the given JSON
  loadModelImpl(jsonModel);
  assert(pOptimizerModel);
  pGecodeProcessor->loadModelAndCreateSolver(pOptimizerModel);
}  // loadModel

void GecodeOptimizer::loadModelImpl(const std::string& jsonModel)
{
  if (jsonModel.empty())
  {
    throw std::invalid_argument("GecodeOptimizer - loadModelImpl: empty JSON model");
  }

  pOptimizerModel = std::make_shared<Model>();
  pOptimizerModel->loadModelFromJson(jsonModel);
}  // loadModelImpl

void GecodeOptimizer::runOptimizer()
{
  if (!isActive())
  {
    const std::string errMsg = "GecodeOptimizer - runOptimizer: optimizer is not active";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  Work::SPtr work = std::make_shared<Work>(gecodeengine::GecodeWork::WorkType::kRunOptimizer,
                                           pMetricsRegister);
  pushTask(work);
}  // runOptimizer

bool GecodeOptimizer::interruptOptimizer()
{
  assert(pGecodeProcessor);
  return pGecodeProcessor->interruptSolver();
}  // interruptOptimizer

ProcessorPipeline::SPtr GecodeOptimizer::buildPipeline(int)
{
  ProcessorPipeline::SPtr pipeline = std::make_shared<ProcessorPipeline>();
  pGecodeProcessor = std::make_shared<GecodeProcessor>(pResult);
  pipeline->pushBackProcessor(pGecodeProcessor);
  return pipeline;
}  // buildPipeline

}  // namespace optilab
