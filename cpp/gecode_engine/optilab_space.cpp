#include "gecode_engine/optilab_space.hpp"

#include <stdexcept>  // for std::out_of_range
#include <string>

namespace optilab {

OptilabSpace::OptilabSpace()
: pSolveType(SolveType::SOLVE_TYPE_SAT)
{
}

OptilabSpace::OptilabSpace(OptilabSpace& other)
: Space(other)
{
  // Integer variables
  pIntVarList.resize(other.pIntVarList.size());
  for (int idx = 0; idx < static_cast<int>(pIntVarList.size()); ++idx)
  {
    pIntVarList[idx].update(*this, other.pIntVarList[idx]);
  }

  // Boolean variables
  pBoolVarList.resize(other.pBoolVarList.size());
  for (int idx = 0; idx < static_cast<int>(pBoolVarList.size()); ++idx)
  {
    pBoolVarList[idx].update(*this, other.pBoolVarList[idx]);
  }

#ifdef GECODE_HAS_SET_VARS
  // Set variables
  pSetVarList.resize(other.pSetVarList.size());
  for (int idx = 0; idx < static_cast<int>(pSetVarList.size()); ++idx)
  {
    pSetVarList[idx].update(*this, other.pSetVarList[idx]);
  }
#endif

#ifdef GECODE_HAS_FLOAT_VARS
  // Float variables
  pFloatVarList.resize(other.pFloatVarList.size());
  for (int idx = 0; idx < static_cast<int>(pFloatVarList.size()); ++idx)
  {
    pFloatVarList[idx].update(*this, other.pFloatVarList[idx]);
  }
#endif

  pOptVarIsInt = other.pOptVarIsInt;
  pOptVarIdx = other.pOptVarIdx;
  pSolveType = other.pSolveType;
}

Gecode::Space* OptilabSpace::copy(void)
{
  return new OptilabSpace(*this);
}  // copy

std::size_t OptilabSpace::addBoolVar(const Gecode::BoolVar& var)
{
  pBoolVarList.push_back(var);
  return pBoolVarList.size() - 1;
}  // addBoolVar

const Gecode::BoolVar& OptilabSpace::getBoolVar(std::size_t idx) const
{
  if (idx >= pBoolVarList.size())
  {
    throw std::out_of_range("OptilabSpace - (Bool) var index out of range: " +
                            std::to_string(idx) + " on " + std::to_string(pBoolVarList.size()));
  }
  return pBoolVarList.at(idx);
}  // getBoolVar

std::size_t OptilabSpace::addIntVar(const Gecode::IntVar& var)
{
  pIntVarList.push_back(var);
  return pIntVarList.size() - 1;
}  // addIntVar

const Gecode::IntVar& OptilabSpace::getIntVar(std::size_t idx) const
{
  if (idx >= pIntVarList.size())
  {
    throw std::out_of_range("OptilabSpace - (Int) var index out of range: " +
                            std::to_string(idx) + " on " + std::to_string(pIntVarList.size()));
  }
  return pIntVarList.at(idx);
}  // getIntVar

#ifdef GECODE_HAS_SET_VARS
std::size_t OptilabSpace::addSetVar(const Gecode::SetVar& var)
{
  pSetVarList.push_back(var);
  return pSetVarList.size() - 1;
}  // addSetVar

const Gecode::SetVar& OptilabSpace::getSetVar(std::size_t idx) const
{
  if (idx >= pSetVarList.size())
  {
    throw std::out_of_range("OptilabSpace - (Set) var index out of range: " +
                            std::to_string(idx) + " on " + std::to_string(pSetVarList.size()));
  }
  return pSetVarList.at(idx);
}  // getSetVar
#endif

#ifdef GECODE_HAS_FLOAT_VARS
std::size_t OptilabSpace::addFloatVar(const Gecode::FloatVar& var)
{
  pFloatVarList.push_back(var);
  return pFloatVarList.size() - 1;
}  // addFloatVar

const Gecode::FloatVar& OptilabSpace::getFloatVar(std::size_t idx) const
{
  if (idx >= pFloatVarList.size())
  {
    throw std::out_of_range("OptilabSpace - (Set) var index out of range: " +
                            std::to_string(idx) + " on " + std::to_string(pFloatVarList.size()));
  }
  return pFloatVarList.at(idx);
}  // getFloatVar
#endif

void OptilabSpace::constrain(const Gecode::Space& space)
{
    if (pOptVarIsInt)
    {
      if (pSolveType == SolveType::SOLVE_TYPE_MIN)
      {
        Gecode::rel(*this, pIntVarList[pOptVarIdx], Gecode::IRT_LE,
                    static_cast<const OptilabSpace*>(&space)->pIntVarList[pOptVarIdx].val());
      }
      else if (pSolveType == SolveType::SOLVE_TYPE_MAX)
      {
        Gecode::rel(*this, pIntVarList[pOptVarIdx], Gecode::IRT_GR,
                    static_cast<const OptilabSpace*>(&space)->pIntVarList[pOptVarIdx].val());
      }
    }
    else
    {
#ifdef GECODE_HAS_FLOAT_VARS
      if (pSolveType == SolveType::SOLVE_TYPE_MIN)
      {
        Gecode::rel(*this, pFloatVarList[pOptVarIdx], Gecode::FRT_LE,
                    static_cast<const OptilabSpace*>(&space)->pFloatVarList[pOptVarIdx].val());
      }
      else if (pSolveType == SolveType::SOLVE_TYPE_MAX)
      {
        Gecode::rel(*this, pFloatVarList[pOptVarIdx], Gecode::FRT_GR,
                    static_cast<const OptilabSpace*>(&space)->pFloatVarList[pOptVarIdx].val());
      }
#endif
    }
  }  // constrain

}  // namespace optilab
