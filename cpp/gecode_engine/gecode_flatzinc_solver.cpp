#include "gecode_engine/gecode_flatzinc_solver.hpp"

#include <functional>  // for std::function
#include <stdexcept>   // for std::invalid_argument

#include <spdlog/spdlog.h>

#include "utilities/strings.hpp"

namespace {

const char kArrayAssignmentPrefix[] = "array";
const char kSolutionSeparator[] = "----------";
const char kUnsatisfiableSolution[] = "=====UNSATISFIABLE=====";
const char kUnknownSolution[] = "=====UNKNOWN=====";

/// Returns true if the given string starts with "array",
/// returns false otherwise
inline bool isArrayAssignment(const std::string& str)
{
  return str.find(kArrayAssignmentPrefix) == 0;
}  // isArrayAssignment

/// From a FlatZinc array assignment, returns the array.
/// For example, from
/// "array1d(1..4, [3, 1, 4, 2])"
/// returns
/// "3, 1, 4, 2"
std::string getArrayFromAssignment(const std::string& str)
{
  auto it1 = str.find_first_of('[');
  auto it2 = str.find_last_of(']');
  if (it1 == std::string::npos || it2 == std::string::npos)
  {
    throw std::runtime_error("getArrayFromAssignment - "
        "the string does not contain an array: " + str);
  }

  return str.substr(it1 + 1, it2 - it1 - 1);
}  // getArrayFromAssignment

}  // namespace

namespace optilab {

GecodeFlatZincSolver::GecodeFlatZincSolver(const GecodeCPModel::SPtr& cpModel)
{
  if (!cpModel)
  {
    throw std::invalid_argument("GecodeFlatZincSolver - empty pointer to a CP model");
  }

  // Get the model environment which, in the Gecode package, corresponds to a space
  auto modelEnvironment = cpModel->getModelEnvironment();
  auto flatzincModelEnvironment = FlatzincModelEnvironment::cast(modelEnvironment.get());
  if (!flatzincModelEnvironment)
  {
    std::string errMsg = "GecodeFlatZincSolver - invalid model environment";
    spdlog::error(errMsg);
    throw std::invalid_argument(errMsg);
  }

  pSpace = flatzincModelEnvironment->getSpace();
  if (!pSpace)
  {
    std::string errMsg = "GecodeSolver - empty FlatZinc space";
    spdlog::error(errMsg);
    throw std::invalid_argument(errMsg);
  }

  // Store the pointer to environment
  pFznEnvPtr = std::dynamic_pointer_cast<FlatzincModelEnvironment>(modelEnvironment);
}

void GecodeFlatZincSolver::initSearch()
{
  try
  {
    pSpace->createBranchers(
        *(pFznEnvPtr->getEnvFznPrinter()), pSpace->solveAnnotations(),
        *(pFznEnvPtr->getEnvFznOptions()), false, pErrStream);

    // Shrink the variable arrays to only the output variables
    pSpace->shrinkArrays(*(pFznEnvPtr->getEnvFznPrinter()));
  }
  catch (Gecode::FlatZinc::Error& err)
  {
    std::string errMsg = "GecodeFlatZincSolver - initSearch: " + err.toString();
    spdlog::error(errMsg);
    throw std::invalid_argument(errMsg);
  }
  catch(...)
  {
    std::string errMsg = "GecodeFlatZincSolver - initSearch undefined error: " + pErrStream.str();
    spdlog::error(errMsg);
    throw std::invalid_argument(errMsg);
  }
}  // initSearch

void GecodeFlatZincSolver::interruptSearchProcess()
{
  // No-op
}  // interruptSearchProcess

bool GecodeFlatZincSolver::nextSolution()
{
  // Don't run more than once on the same model
  if (pRunComplete)
  {
    // There is no next solution to find
    return false;
  }

  Gecode::Support::Timer timeTotal;
  timeTotal.start();

  std::ostringstream solutionStream;
  try
  {
    pSpace->run(solutionStream,
                *(pFznEnvPtr->getEnvFznPrinter()),
                *(pFznEnvPtr->getEnvFznOptions()),
                timeTotal);
  }
  catch (Gecode::FlatZinc::Error& err)
  {
    pRunComplete = true;

    std::string errMsg = "GecodeFlatZincSolver - nextSolution: " + err.toString();
    spdlog::error(errMsg);
    throw std::invalid_argument(errMsg);
  }
  catch(...)
  {
    pRunComplete = true;

    std::string errMsg = "GecodeFlatZincSolver - "
        "nextSolution, undefined error: " + pErrStream.str();
    spdlog::error(errMsg);
    throw std::invalid_argument(errMsg);
  }

  // The run is now complete, set the flag to avoid further runs
  pRunComplete = true;

  // Get the solution found by Gecode-FlatZinc as a string
  std::string solution = solutionStream.str();

  // Set the state of the solver based on the solution found
  setSolverState(solution);

  // Parse and create the solution solution
  parseAndStoreSolution(solution);

  // Return true, a solution is found (if satisfiable)
  return getStatus() == SolverStatus::SS_SAT || SolverStatus::SS_OPT;
}  // nextSolution

bool GecodeFlatZincSolver::constraintPropagation()
{
  // No-op
  return true;
}  // constraintPropagation

void GecodeFlatZincSolver::setSolverState(const std::string& sol)
{
  if (sol.find(kUnsatisfiableSolution) != std::string::npos)
  {
    setSolverStatus(SolverStatus::SS_UNSAT);
  }
  else if (sol.find(kUnknownSolution) != std::string::npos)
  {
    setSolverStatus(SolverStatus::SS_UNKNOWN);
  }
  else
  {
    setSolverStatus(SolverStatus::SS_SAT);
  }
}  // setSolverState

void GecodeFlatZincSolver::parseAndStoreSolution(std::string& sol)
{
  // First element in the pair is the verbatim solution as produced by Gecode
  pSolution.first = sol;

  // Return asap if no solution was found
  if (getStatus() == SolverStatus::SS_UNSAT || getStatus() == SolverStatus::SS_UNKNOWN)
  {
    // @note the original verbatim FlatZinc solution has been saved already,
    // meaning that the FlatZinc solution will be always present, regardless the status
    // of the solver
    return;
  }

  // Store the first solution found
  auto it = pSolution.first.find(kSolutionSeparator);
  if (it != std::string::npos)
  {
    sol = pSolution.first.substr(0, it);
  }

  // Tokenize the string on the new line char.
  // Each token represents a variable assignment
  auto listVarsAssign = utilsstrings::tokenizeStringOnSymbol(sol, '\n');
  for (const auto& varAssign : listVarsAssign)
  {
    // Get the assignment
    auto assign = utilsstrings::tokenizeStringOnSymbol(varAssign, '=');
    if (assign.size() != 2)
    {
      const std::string errMsg = "GecodeFlatZincSolver - cannot parse solution: " + varAssign;
      spdlog::error(errMsg);
      throw std::runtime_error(errMsg);
    }

    // Trim both sides of the assignment
    utilsstrings::trim(assign[0]);
    utilsstrings::trim(assign[1]);

    // Left side is the variable name.
    // Create an entry in the map
    if (pSolution.second.find(assign[0]) != pSolution.second.end())
    {
      const std::string errMsg = "GecodeFlatZincSolver - "
          "variable being assigned twice: " + assign[0];
      spdlog::error(errMsg);
      throw std::runtime_error(errMsg);
    }

    // Store the domain of the variable.
    // @note the domain can be something like the following:
    // -1;
    // array1d(1..4, [3, 1, 4, 2]);
    auto& varDom = pSolution.second[assign[0]];

    // Remove the semicolon at the end of the string
    assign[1].pop_back();

    // Check if the assignment is an array or a single value
    if (isArrayAssignment(assign[1]))
    {
      varDom = utilsstrings::splitArguments(getArrayFromAssignment(assign[1]));
    }
    else
    {
      varDom.push_back(assign[1]);
    }
  }
}  // parseAndStoreSolution

}  // namespace optilab
