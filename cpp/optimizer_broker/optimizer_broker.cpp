#include "optimizer_broker/optimizer_broker.hpp"

#include <cassert>
#include <stdexcept>   // for std::invalid_argument

#include <spdlog/spdlog.h>
#include "zeromq/zhelpers.hpp"

#include "optimizer_broker/broker_constants.hpp"
#include "optimizer_broker/broker_utilities.hpp"
#include "optimizer_client/client_constants.hpp"
#include "optimizer_service/service_constants.hpp"

namespace {

constexpr int kNoPollTimeout = -1;

std::string getSocketAddr(const int socketPort, const std::string& socketAddr)
{
  std::string addr = optilab::optimizerbroker::BROKER_PREFIX_TCP_NETWORK_ADDR;
  if (!socketAddr.empty())
  {
    addr += socketAddr + ":";
  }
  else
  {
    addr += "*:";
  }
  addr += std::to_string(socketPort);

  return addr;
}  // getSocketAddr

}  // namespace

namespace optilab {

OptimizerBroker::OptimizerBroker(const int frontendPort, const int backendPort,
                                 const std::string& frontendAddr, const std::string& backendAddr)
: pStartup(false),
  pFrontendPort(frontendPort),
  pBackendPort(backendPort),
  pFrontendAddr(frontendAddr),
  pBackendAddr(backendAddr),
  pFrontendConnector(nullptr),
  pBackendConnector(nullptr)
{
  if (pFrontendPort < 1 || pFrontendPort > optimizerservice::MAX_SERVICE_PORT_NUMBER)
  {
    throw std::invalid_argument("OptimizerBroker - invalid frontend port number");
  }

  if (pBackendPort < 1 || pBackendPort > optimizerservice::MAX_SERVICE_PORT_NUMBER)
  {
    throw std::invalid_argument("OptimizerBroker - invalid backend port number");
  }
}

int OptimizerBroker::run()
{
  if (pStartup)
  {
    const std::string errMsg = "OptimizerBroker - broker already running";
    spdlog::error(errMsg);
    return 1;
  }

  // Create the context and the sockets to connect to
  std::shared_ptr<zmq::context_t> context;
  std::shared_ptr<zmq::socket_t> frontendSocket;
  std::shared_ptr<zmq::socket_t> backendSocket;
  try
  {
    context = std::make_shared<zmq::context_t>(optimizerbroker::BROKER_CONTEXT_NUM_THREADS);
    frontendSocket = std::make_shared<zmq::socket_t>(*context, ZMQ_ROUTER);
    backendSocket = std::make_shared<zmq::socket_t>(*context, ZMQ_ROUTER);

    // Get the front-end/back-end addresses to bind to
    const std::string frontendAddr = getSocketAddr(pFrontendPort, pFrontendAddr);
    const std::string backendAddr = getSocketAddr(pBackendPort, pBackendAddr);

    // Bind the broker to the front-end/back-end addresses
    spdlog::info("OptimizerBroker - binding to front-end address " + frontendAddr);
    frontendSocket->bind(frontendAddr.c_str());

    spdlog::info("OptimizerBroker - binding to back-end address " + backendAddr);
    backendSocket->bind(backendAddr.c_str());
  }
  catch (...)
  {
    pStartup = false;
    throw;
  }

  // Set the startup flag as running
  pStartup = true;

  // Create a new socket connector and start listening for incoming messages
  spdlog::info("OptimizerBroker - binding on sockets");
  pFrontendConnector = std::make_shared<SocketConnector>(frontendSocket);
  pBackendConnector = std::make_shared<SocketConnector>(backendSocket);

  const int errorStatus = listenOnSockets();

  // Cleanup sockets and context
  spdlog::info("OptimizerBroker - cleanup");
  pFrontendConnector.reset();
  pBackendConnector.reset();
  context.reset();

  return errorStatus;
}  // run

int OptimizerBroker::listenOnSockets()
{
  if (!getFrontendConnector() || !getBackendConnector())
  {
    throw std::runtime_error("OptimizerBroker - empty connectors");
  }

  int errorStatus = optimizerbroker::BROKER_ERROR_STATUS_NO_ERROR;

  // Logic of the while-loop:
  // - Poll back-end always, front-end only if 1+ workers are ready
  // - If worker replies, queue worker as ready and forward reply
  //   to client if necessary
  // - If client requests, pop next worker and send request to it
  spdlog::info("OptimizerBroker - start front-end and back-end poll");
  while (true)
  {
    // Initialize poll set
    zmq::pollitem_t pollItems[] = {
        //  Always poll for worker activity on backend
        { *(getBackendConnector()->getSocket()), 0, ZMQ_POLLIN, 0 },
        //  Poll front-end only if we have available workers
        { *(getFrontendConnector()->getSocket()), 0, ZMQ_POLLIN, 0 }
    };

    if (workersReady())
    {
      zmq::poll(&pollItems[0], 2, kNoPollTimeout);
    }
    else
    {
      zmq::poll(&pollItems[0], 1, kNoPollTimeout);
    }

    //  Handle worker activity on back-end
    if (pollItems[0].revents & ZMQ_POLLIN)
    {
      errorStatus = handleWorkerActivity();
      if (errorStatus)
      {
        spdlog::error("OptimizerBroker - error on handling worker activity");
        break;
      }
    }

    //  Handle client activity on front-end
    if (pollItems[1].revents & ZMQ_POLLIN)
    {
      errorStatus = handleClientActivity();
      if (errorStatus)
      {
        spdlog::error("OptimizerBroker - error on handling client activity");
        break;
      }
    }
  }  // while

  return errorStatus;
}  // listenOnSockets

int OptimizerBroker::handleWorkerActivity()
{
  // According to the broker-worker contract,
  // each message sent from the worker is an envelop with three frames:
  // 1 - the address of the sender
  // 2 - the address of the receiver
  // 3 - the payload (i.e., the raw message)
  std::string workerAddr;
  std::string recipientAddr;
  std::string payload;
  try
  {
    // critical section

    // First frame contains the address of the worker
    workerAddr = brokerutils::readFromConnector(getBackendConnector());

    // Second frame contains the address of the client/receiver
    recipientAddr = brokerutils::readFromConnector(getBackendConnector());

    // Third frame contains the raw message
    payload = brokerutils::readFromConnector(getBackendConnector());
  }
  catch (...)
  {
    spdlog::error("OptimizerBroker - error on receiving address from back-end worker");
    return 1;
  }

  return handleWorker(workerAddr, recipientAddr, payload);
}  // handleWorkerActivity

int OptimizerBroker::handleClientActivity()
{
  // According to the broker-client contract,
  // each message sent from the client is an envelop with two frames:
  // 1 - the address of the sender
  // 2 - the payload (i.e., the raw message)
  std::string clientAddr;
  std::string payload;
  try
  {
    // critical section

    // First frame contains the client's address
    clientAddr = brokerutils::readFromConnector(getFrontendConnector());

    // Second frame contains the payload
    payload = brokerutils::readFromConnector(getFrontendConnector());
  }
  catch (...)
  {
    spdlog::error("OptimizerBroker - error on receiving address from front-end client");
    return 1;
  }

  return handleClient(clientAddr, payload);
}  // handleClientActivity

}  // namespace optilab
