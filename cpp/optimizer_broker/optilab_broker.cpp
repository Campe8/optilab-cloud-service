//
// Copyright OptiLab 2019. All rights reserved.
//
// Entry point for the OptiLab broker.
//

#include <getopt.h>

#include <exception>
#include <iostream>
#include <string>
#include <vector>

#include "config/config_constants.hpp"
#include "config/system_network_config.hpp"
#include "optimizer_broker/service_broker.hpp"

extern int optind;

namespace {

void printHelp(const std::string& programName) {
  std::cerr << "Usage: " << programName << " [options]"
      << std::endl
      << "options:" << std::endl
      << "  --help|-h           Print this help message."
      << std::endl;
}  // printHelp

}  // namespace

int main(int argc, char* argv[]) {

  char optString[] = "h:";
  struct option longOptions[] =
  {
      { "help", no_argument, NULL, 'h' },
      { 0, 0, 0, 0 }
  };

  // Parse options
  int opt;
  bool jsonConfigPath = false;
  while (-1 != (opt = getopt_long(argc, argv, optString, longOptions, NULL)))
  {
    switch (opt)
    {
      case 'h':
      default:
        printHelp(argv[0]);
        return 0;
    }
  }  // while

  // Get the port number to run the service on
  auto& config = optilab::SystemNetworkConfig::getInstance();
  auto& portList = config.getPortList(optilab::configconsts::FRONT_END_BROKER);
  if (portList.size() < 2)
  {
    std::cerr << "Invalid number of port for the front-end broker port list "
        "in configuration file\n";
    return 1;
  }

  int optilabFrontEndPort = portList[0];
  int optilabBackEndPort = portList[1];
  try
  {
    // Create a new optimizer broker and run it
    optilab::ServiceBroker optBroker(optilabFrontEndPort, optilabBackEndPort);
    return optBroker.run();
  }
  catch (const std::exception& e)
  {
    std::cerr << e.what() << std::endl;
    return 1;
  }
  catch (...)
  {
    std::cerr << "Undefined error" << std::endl;
    return 2;
  }

  return 0;
}
