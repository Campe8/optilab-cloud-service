#include "optimizer_broker/broker_utilities.hpp"

#include <stdexcept>  // for std::invalid_argument

namespace optilab {

namespace brokerutils {

std::string readFromConnector(const optilab::SocketConnector::SPtr& connector)
{
  if (!connector)
  {
    throw std::invalid_argument("readFromConnector - empty connector");
  }

  return s_recv(*(connector->getSocket()));
}  // readFromConnector

/// Sends the given payload to the given client address on the socket specified
/// by the given connector
void sendPayloadToClient(const optilab::SocketConnector::SPtr& connector,
                         const std::string& payload, const std::string& clientAddr)
{
  if (!connector)
  {
    throw std::invalid_argument("readFromConnector - empty connector");
  }

  // Sending a message to the client is done as follows:
  // 1 - Send the client address with "s_sendmore(...)"
  //     This is consumed by the ZMQ_ROUTER socket to address the payload to the proper client
  s_sendmore(*(connector->getSocket()), clientAddr);

  // 2 - Send the actual payload with "s_send(...)"
  s_send(*(connector->getSocket()), payload);
}  // sendPayloadToClient

/// Sends the given payload to the given worker address on the socket specified
/// by the given connector
void sendRequestToWorker(const optilab::SocketConnector::SPtr& connector,
                         const std::string& payload, const std::string& clientAddr,
                         const std::string& workerAddr)
{
  if (!connector)
  {
    throw std::invalid_argument("readFromConnector - empty connector");
  }

  // Sending a message to the worker is done as follows:
  // 1 - Send the worker address to send the request to with "s_sendmore(...)"
  //     This is consumed by the ZMQ_ROUTER socket to address the payload to the proper worker
  s_sendmore(*(connector->getSocket()), workerAddr);

  // 2 - Send the client address to the worker to let him know who
  //     to reply to with "s_sendmore(...)"
  s_sendmore(*(connector->getSocket()), clientAddr);

  // 3 - Send the actual payload with "s_send(...)"
  s_send(*(connector->getSocket()), payload);
}  // sendRequestToWorker

OptiLabReplyMessage buildLoginSuccessMessage()
{
  ServiceLoginRep loginRep;
  loginRep.set_login(ServiceLoginRep::SUCCESS);

  OptiLabReplyMessage rep;
  ::google::protobuf::Any* anyRequest = new ::google::protobuf::Any();
  anyRequest->PackFrom(loginRep);
  rep.set_allocated_details(anyRequest);

  rep.set_type(::optilab::OptiLabReplyMessage::ServiceLogin);
  rep.set_replystatus(::optilab::ReplyStatus::OK);

  return rep;
}  // buildLoginSuccessMessage

OptiLabReplyMessage buildLogoffSuccessMessage()
{
  ServiceLogoffRep logoffRep;
  logoffRep.set_logoff(ServiceLogoffRep::SUCCESS);

  OptiLabReplyMessage rep;
  ::google::protobuf::Any* anyRequest = new ::google::protobuf::Any();
  anyRequest->PackFrom(logoffRep);
  rep.set_allocated_details(anyRequest);

  rep.set_type(::optilab::OptiLabReplyMessage::ServiceLogoff);
  rep.set_replystatus(::optilab::ReplyStatus::OK);

  return rep;
}  // buildLogoffSuccessMessage

}  // namespace brokerutils

}  // namespace optilab
