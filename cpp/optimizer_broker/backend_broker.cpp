#include "optimizer_broker/backend_broker.hpp"

#include <cassert>
#include <exception>
#include <stdexcept>   // for std::invalid_argument

#include <spdlog/spdlog.h>
#include "zeromq/zhelpers.hpp"

#include "optimizer_broker/broker_constants.hpp"
#include "optimizer_broker/broker_utilities.hpp"
#include "optimizer_client/client_constants.hpp"
#include "optimizer_service/service_constants.hpp"

namespace optilab {

BackendBroker::BackendBroker(const int frontendPort, const int backendPort,
                             const std::string& frontendAddr, const std::string& backendAddr)
: OptimizerBroker(frontendPort, backendPort, frontendAddr, backendAddr)
{
}

bool BackendBroker::workersReady() const
{
  return !pReadyWorkers.empty();
}  // workersReady

int BackendBroker::handleWorker(const std::string& workerAddr, const std::string& recipientAddr,
                                const std::string& payload)
{
  int errorStatus = optimizerbroker::BROKER_ERROR_STATUS_NO_ERROR;

  // Check if this is a "READY" (registration) message,
  // i.e., if the message is coming from a new worker trying to register itself to the broker
  if (payload == std::string(serviceworker::WORKER_MESSAGE_READY))
  {
    // If so, register the worker and return.
    // @note only for READY messages, the recipientAddr contains the worker's unique ID
    // and it is used by the broker for internal mapping
    return registerWorker(recipientAddr, workerAddr);
  }

  // Send the reply to the recipient
  try
  {
    // critical section

    // The worker is available again
    assert(pReverseWorkerMap.find(workerAddr) != pReverseWorkerMap.end());
    pReadyWorkers.push_back(pReverseWorkerMap[workerAddr]);

    // Send the message to the client
    brokerutils::sendPayloadToClient(getFrontendConnector(), payload, recipientAddr);
  }
  catch (std::exception& ex)
  {
    const std::string errMsg = "BackendBroker - error on receiving payload from back-end worker "
        "and forwarding to the front-end client: " + std::string(ex.what());
    spdlog::error(errMsg);
    return 1;
  }
  catch (...)
  {
    const std::string errMsg = "BackendBroker - error on receiving payload from back-end worker "
            "and forwarding to the front-end client";
        spdlog::error(errMsg);
    return 1;
  }

  // Check if there is a new job in the queue of jobs
  if (pJobQueue.empty())
  {
    // If not, return asap
    return errorStatus;
  }

  // If so, pick a new worker and give it the job to execute
  // There is at least one available worker (pushed in the queue above)
  const auto workerId = pReadyWorkers.front();
  pReadyWorkers.pop_front();

  // Send the request to the worker
  spdlog::info("BackendBroker - processing request from client " + pJobQueue.front().first +
               " with worker " + workerId);
  try
  {
    brokerutils::sendRequestToWorker(getBackendConnector(), payload, pJobQueue.front().first,
                                     pWorkerMap[workerId]);

    // Remove the task from the queue
    pJobQueue.pop_front();

  }
  catch (...)
  {
    spdlog::error("BackendBroker - cannot send the request to the worker " + workerId);

    // Re-insert the worker in the set of available workers
    pReadyWorkers.push_back(workerId);

    // Set the error status
    errorStatus = 1;
  }

  return errorStatus;
}  // handleWorker

int BackendBroker::handleClient(const std::string& clientAddr, const std::string& payload)
{
  int errorStatus = optimizerbroker::BROKER_ERROR_STATUS_NO_ERROR;

  // Check if there is an available worker to send the job to.
  // If not, store the job into a queue and return
  if (workersReady())
  {
    // If so, pick a new worker and give it the job to execute
    const auto workerId = pReadyWorkers.front();
    pReadyWorkers.pop_front();

    // Send the request to the worker
    spdlog::info("BackendBroker - processing request from client " + clientAddr +
                 " with worker " + workerId);
    try
    {
      brokerutils::sendRequestToWorker(getBackendConnector(), payload, clientAddr,
                                       pWorkerMap[workerId]);
    }
    catch (...)
    {
      spdlog::error("BackendBroker - cannot send the request to the worker " + workerId);

      // Re-insert the worker in the set of available workers
      pReadyWorkers.push_back(workerId);

      // Set the error status
      errorStatus = 1;
    }
  }
  else
  {
    // Store the job into the queue and return.
    // The job will be served and given to workers in FIFO mode
    pJobQueue.push_back({clientAddr, payload});
  }

  // Return asap to the caller and, when ready, process the job asynchronously
  return errorStatus;
}  // handleClient

int BackendBroker::registerWorker(const std::string& workerId, const std::string& workerAddr)
{
  int errorStatus = optimizerbroker::BROKER_ERROR_STATUS_NO_ERROR;

  if (workerId.empty())
  {
    throw std::runtime_error("BackendBroker - registering a worker with an empty id");
  }

  if (pWorkerMap.find(workerId) != pWorkerMap.end())
  {
    throw std::runtime_error("BackendBroker - "
        "registering a worker with an address already in use");
  }

  // Map the worker and register it in the priority list
  spdlog::info("BackendBroker - register worker " + workerId + " with address " + workerAddr);
  pWorkerMap[workerId] = workerAddr;

  // Register the worker in the reverse map
  pReverseWorkerMap[workerAddr] = workerId;

  // Set this worker as ready
  pReadyWorkers.push_back(workerId);

  return errorStatus;
}  // registerWorker

}  // namespace optilab
