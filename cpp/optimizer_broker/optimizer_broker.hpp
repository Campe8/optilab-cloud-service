//
// Copyright OptiLab 2019. All rights reserved.
//
// Optimizer broker.
// It is the component between clients and workers.
// Manages the connections and messages from the front-end to the
// back-end and vice-versa.
//

#pragma once

#include <atomic>
#include <memory>   // for std::shared_ptr
#include <string>

#include "optimizer_service/socket_connector.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS OptimizerBroker {
public:
  using SPtr = std::shared_ptr<OptimizerBroker>;

public:
  /// Constructor: creates a new broker connecting the front-end to the back-end with the given
  /// two ports and given addresses.
  /// @note if the addresses are empty, "localhost" will be used.
  /// @note throws std::invalid_argument if the ports are less than 1 or greater than
  /// optimizerservice::MAX_SERVICE_PORT_NUMBER
  OptimizerBroker(const int frontendPort, const int backendPort,
                  const std::string& frontendAddr = std::string(),
                  const std::string& backendAddr = std::string());

  virtual ~OptimizerBroker() = default;

  /// Runs the broker: it connects frontend clients to backend services.
  /// Returns zero on success, non-zero otherwise
  int run();

protected:
  /// Returns the connector used for front-end communication
  inline const SocketConnector::SPtr& getFrontendConnector() const { return pFrontendConnector; }

  /// Returns the connector used for back-end communication
  inline const SocketConnector::SPtr& getBackendConnector() const { return pBackendConnector; }

  /// Returns true if there is at least one worker that is ready to accept requests
  /// from clients. Returns false otherwise
  virtual bool workersReady() const = 0;

  /// Handles client activity (i.e., this is the implementation method).
  /// @note "payload" is the payload as received from the client
  virtual int handleClient(const std::string& clientAddr, const std::string& payload) = 0;

  /// Handles worker activity (i.e., this is the implementation method).
  /// @note "payload" is the payload as received from the worker
  virtual int handleWorker(const std::string& workerAddr, const std::string& recipientAddr,
                           const std::string& payload) = 0;

private:
  /// Startup flag indicating whether or not the broker is running
  std::atomic<bool> pStartup;

  /// Port to connect to for the front-end service
  const int pFrontendPort;

  /// Port to connect to for the back-end service
  const int pBackendPort;

  /// Address for front-end connection
  const std::string pFrontendAddr;

  /// Address for back-end connection
  const std::string pBackendAddr;

  /// Connector used for front-end communication
  SocketConnector::SPtr pFrontendConnector;

  /// Connector used for back-end communication
  SocketConnector::SPtr pBackendConnector;

  /// Start the loop to listen for incoming/outgoing messages.
  /// @note returns zero on success, non-zero otherwise
  int listenOnSockets();

  /// Handles worker activity on back-end.
  /// Returns zero on success, non-zero otherwise
  int handleWorkerActivity();

  /// Handles client activity on front-end.
  /// Returns zero on success, non-zero otherwise
  int handleClientActivity();
};

}  // namespace optilab
