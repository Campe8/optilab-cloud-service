//
// Copyright OptiLab 2019. All rights reserved.
//
// Constants for the optimizer broker.
//

#pragma once

namespace optilab {

namespace optimizerbroker {

extern const int BROKER_CONTEXT_NUM_THREADS;
extern const int BROKER_ERROR_STATUS_NO_ERROR;
extern const char BROKER_PREFIX_TCP_NETWORK_ADDR[];

}  // optimizerbroker

}  // namespace optilab
