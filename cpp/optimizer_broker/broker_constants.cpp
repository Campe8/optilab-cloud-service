#include "optimizer_broker/broker_constants.hpp"

namespace optilab {

namespace optimizerbroker {

const int BROKER_CONTEXT_NUM_THREADS = 1;
const int BROKER_ERROR_STATUS_NO_ERROR = 0;
const char BROKER_PREFIX_TCP_NETWORK_ADDR[] = "tcp://";

}  // optimizerbroker

}  // namespace optilab
