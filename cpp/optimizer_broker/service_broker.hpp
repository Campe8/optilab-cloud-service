//
// Copyright OptiLab 2019. All rights reserved.
//
// Service broker.
// It is the component between front-end cloud clients and workers.
// Manages the connections and messages from the front-end to the
// back-end and vice-versa.
//

#pragma once

#include <cstddef>  // for std::size_t
#include <memory>   // for std::shared_ptr
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include "optimizer_broker/optimizer_broker.hpp"
#include "optimizer_service/socket_connector.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS ServiceBroker : public OptimizerBroker {
public:
  using SPtr = std::shared_ptr<ServiceBroker>;

public:
  /// Constructor: creates a new broker connecting the front-end to the back-end with the given
  /// two ports and given addresses.
  /// @note if the addresses are empty, "localhost" will be used.
  /// @note throws std::invalid_argument if the ports are less than 1 or greater than
  /// optimizerservice::MAX_SERVICE_PORT_NUMBER
  ServiceBroker(const int frontendPort, const int backendPort,
                const std::string& frontendAddr = std::string(),
                const std::string& backendAddr = std::string());

  virtual ~ServiceBroker() = default;

private:
  struct WorkerInfo {
    using Ptr = std::shared_ptr<WorkerInfo>;

    WorkerInfo(const std::string& workerId, const std::string& workerAddr)
    : WorkerId(workerId),
      WorkerAddr(workerAddr),
      WorkerCtr(0)
    {
    }

    const std::string WorkerId;
    const std::string WorkerAddr;
    std::size_t WorkerCtr;
  };

  /// Map of pairs <worker id, worker information>
  using WorkerMap = std::unordered_map<std::string, WorkerInfo::Ptr>;

  /// Client-worker map stores pair of addresses
  using ClientWorkerMap = std::unordered_map<std::string, std::string>;

  /// Worker priority list.
  /// This list is implemented as a simple vector e finding the "Least Used Worker"
  /// is done by simply scanning the list in O(n) time.
  /// @note the number of worker shouldn't be big
  using WorkerPriorityList = std::vector<WorkerInfo::Ptr>;

protected:

  /// Returns true if there is at least one worker that is ready to accept requests
  /// from clients. Returns false otherwise
  bool workersReady() const override;

  /// Handles client activity (i.e., this is the implementation method).
  /// @note "payload" is the payload as received from the client
  int handleClient(const std::string& clientAddr, const std::string& payload) override;

  /// Handles worker activity (i.e., this is the implementation method).
  /// @note "payload" is the payload as received from the worker
  int handleWorker(const std::string& workerAddr, const std::string& recipientAddr,
                   const std::string& payload) override;

private:
  /// Set of available, ready workers
  std::unordered_set<std::string> pReadyWorkers;

  /// Map pairing worker ids with their corresponding addresses
  WorkerMap pWorkerMap;

  /// Reverse lookup map worker address to worker id
  std::unordered_map<std::string, std::string> pReverseWorkerMap;

  /// Map pairing clients to workers.
  /// @note this map is required since workers are stateful
  ClientWorkerMap pClientWorkerMap;

  /// Priority list for workers
  WorkerPriorityList pWorkerPriorityList;

  /// Returns true if the given id corresponds to a worker which is not yet
  /// registered by this broker. Returns false otherwise
  inline bool isNewWorker(const std::string& workerId) const
  {
    return pWorkerMap.find(workerId) == pWorkerMap.end();
  }

  /// Returns true if the given address corresponds to a client which has never been seen before.
  /// Returns false otherwise
  inline bool isNewClient(const std::string& clientAddr) const
  {
    return pClientWorkerMap.find(clientAddr) == pClientWorkerMap.end();
  }

  /// Registers a new worker pairing it with the provided worker address.
  /// Returns zero on success, non-zero otherwise.
  /// @note throws std::runtime_error if the worker id is an empty string
  int registerWorker(const std::string& workerAddr, const std::string& workerId);

  /// Registers a new client pairing it with a given worker.
  /// Returns zero on success, non-zero otherwise
  int registerClient(const std::string& clientAddr);

  /// Performs login of the client with given address.
  /// @note this should check client's credentials
  int loginClient(const SocketConnector::SPtr& frontendConnector, const std::string& clientAddr);

  /// Performs logoff of the client with given address
  int logoffClient(const SocketConnector::SPtr& frontendConnector, const std::string& clientAddr);

  /// Unregisters the client with given address.
  /// Always return success
  int unregisterClient(const std::string& clientAddr);

  /// Returns the address of the "least used worker"
  const std::string& getWorkerForClient() const;
};

}  // namespace optilab
