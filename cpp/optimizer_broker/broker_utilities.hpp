//
// Copyright OptiLab 2019. All rights reserved.
//
// Utilities class for the broker.
//

#pragma once

#include <string>

#include "optilab_protobuf/optilab.pb.h"
#include "optimizer_service/socket_connector.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

namespace brokerutils {

/// Sends the given payload to the given worker address on the socket specified
/// by the given connector.
/// @note throws std::invalid_argument on empty connector pointer
SYS_EXPORT_FCN void sendRequestToWorker(const optilab::SocketConnector::SPtr& connector,
                                        const std::string& payload, const std::string& clientAddr,
                                        const std::string& workerAddr);

/// Sends the given payload to the given client address on the socket specified
/// by the given connector.
/// @note throws std::invalid_argument on empty connector pointer
SYS_EXPORT_FCN void sendPayloadToClient(const optilab::SocketConnector::SPtr& connector,
                                        const std::string& payload, const std::string& clientAddr);

/// Reads from the given connector and returns the incoming message.
/// @note this is a blocking call.
/// @note throws std::invalid_argument on empty connector pointer
SYS_EXPORT_FCN std::string readFromConnector(const optilab::SocketConnector::SPtr& connector);

/// Builds and returns a login success message
SYS_EXPORT_FCN OptiLabReplyMessage buildLoginSuccessMessage();

/// Builds and returns a logoff success message
SYS_EXPORT_FCN OptiLabReplyMessage buildLogoffSuccessMessage();

}  // namespace brokerutils

}  // namespace optilab
