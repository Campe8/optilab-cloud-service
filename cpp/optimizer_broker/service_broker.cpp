#include "optimizer_broker/service_broker.hpp"

#include <cassert>
#include <stdexcept>   // for std::invalid_argument

#include <spdlog/spdlog.h>
#include "zeromq/zhelpers.hpp"

#include "optimizer_broker/broker_constants.hpp"
#include "optimizer_broker/broker_utilities.hpp"
#include "optimizer_client/client_constants.hpp"
#include "optimizer_service/service_constants.hpp"

namespace optilab {

ServiceBroker::ServiceBroker(const int frontendPort, const int backendPort,
                             const std::string& frontendAddr, const std::string& backendAddr)
: OptimizerBroker(frontendPort, backendPort, frontendAddr, backendAddr)
{
}

bool ServiceBroker::workersReady() const
{
  return !pReadyWorkers.empty();
}  // workersReady

int ServiceBroker::handleWorker(const std::string& workerAddr, const std::string& recipientAddr,
                                const std::string& payload)
{
  int errorStatus = optimizerbroker::BROKER_ERROR_STATUS_NO_ERROR;

  // Check if this is a "READY" (registration) message,
  // i.e., if the message is coming from a new worker trying to register itself to the broker
  if (payload == std::string(serviceworker::WORKER_MESSAGE_READY))
  {
    // If so, register the worker and return.
    // @note only for READY messages, the recipient_addr contains the worker's unique ID
    // and it is used by the broker for internal mapping
    return registerWorker(workerAddr, recipientAddr);
  }

  // Send the reply to the recipient
  try
  {
    // critical section

    // The worker is available again
    assert(pReverseWorkerMap.find(workerAddr) != pReverseWorkerMap.end());
    pReadyWorkers.insert(pReverseWorkerMap[workerAddr]);

    // Send the message to the client
    brokerutils::sendPayloadToClient(getFrontendConnector(), payload, recipientAddr);
  }
  catch (...)
  {
    spdlog::error("OptimizerBroker - error on receiving payload from back-end worker "
        "and forwarding to the front-end client");
    errorStatus = 1;
  }

  return errorStatus;
}  // handleWorker

int ServiceBroker::handleClient(const std::string& clientAddr, const std::string& payload)
{
  int errorStatus = optimizerbroker::BROKER_ERROR_STATUS_NO_ERROR;

  // Check if the client is a new client.
  // If so, register it and pair it with a given worker
  if (isNewClient(clientAddr))
  {
    errorStatus = registerClient(clientAddr);
    if (errorStatus)
    {
      spdlog::error("OptimizerBroker - error in registering the client " + clientAddr);
      return errorStatus;
    }
  }

  // Check for login/logoff
  if (payload == std::string(optimizerclient::CLIENT_LOGIN_MESSAGE))
  {
    // Login client
    errorStatus = loginClient(getFrontendConnector(), clientAddr);
    return errorStatus;
  }
  else if (payload == std::string(optimizerclient::CLIENT_LOGOFF_MESSAGE))
  {
    spdlog::info("OptimizerBroker -  client " + clientAddr + " requested log off");
    errorStatus = unregisterClient(clientAddr);
    errorStatus = logoffClient(getFrontendConnector(), clientAddr);

    return errorStatus;
  }

  // Get the worker paired to this client
  const auto& workerId = pClientWorkerMap.at(clientAddr);

  // Remove the worker from the list of available workers
  pReadyWorkers.erase(workerId);

  // Increase the counter of the work done by the selected worker
  assert(pWorkerMap.find(workerId) != pWorkerMap.end());
  pWorkerMap[workerId]->WorkerCtr++;

  // Send the request to the worker
  spdlog::info("OptimizerBroker - client " + clientAddr + " send request to worker " + workerId);
  try
  {
    brokerutils::sendRequestToWorker(getBackendConnector(), payload, clientAddr,
                                     pWorkerMap[workerId]->WorkerAddr);
  }
  catch (...)
  {
    spdlog::error("OptimizerBroker - cannot send the request from client " + clientAddr +
                  " to the worker " + workerId);

    // Re-insert the worker in the set of available workers
    pReadyWorkers.insert(workerId);

    // Set the error status
    errorStatus = 1;
  }

  return errorStatus;
}  // handleClient

int ServiceBroker::registerWorker(const std::string& workerAddr, const std::string& workerId)
{
  int errorStatus = optimizerbroker::BROKER_ERROR_STATUS_NO_ERROR;

  if (workerId.empty())
  {
    throw std::runtime_error("OptimizerBroker - registering a worker with an empty id");
  }

  if (pReverseWorkerMap.find(workerAddr) != pReverseWorkerMap.end())
  {
    throw std::runtime_error("OptimizerBroker - "
        "registering a worker with an address already in use");
  }

  if (!isNewWorker(workerId))
  {
    spdlog::warn("OptimizerBroker - worker " + workerId + " is being registered more than once");
    return 1;
  }

  // Map the worker and register it in the priority list
  spdlog::info("OptimizerBroker - register worker " + workerId + " with address " + workerAddr);

  WorkerInfo::Ptr workerInfo = std::make_shared<WorkerInfo>(workerId, workerAddr);
  pWorkerMap[workerId] = workerInfo;
  pWorkerPriorityList.push_back(workerInfo);

  // Register the worker in the reverse map
  pReverseWorkerMap[workerAddr] = workerId;

  // Set this worker as ready
  pReadyWorkers.insert(workerId);

  return errorStatus;
}  // registerWorker

int ServiceBroker::loginClient(const SocketConnector::SPtr& frontendConnector,
                               const std::string& clientAddr)
{
  if (!frontendConnector)
  {
    std::string errMsg = "OptimizerBroker - loginClient: empty front-end connector";
    spdlog::error(errMsg);
    throw std::invalid_argument(errMsg);
  }

  int errorStatus = optimizerbroker::BROKER_ERROR_STATUS_NO_ERROR;

  spdlog::info("OptimizerBroker -  client " + clientAddr +
               " requested login...");
  spdlog::info("OptimizerBroker - ...login successful");

  // Check for login - simulate login success
  auto loginReplyMsg = brokerutils::buildLoginSuccessMessage();
  std::string payload = loginReplyMsg.SerializeAsString();

  // Send login message to client
  brokerutils::sendPayloadToClient(frontendConnector, payload, clientAddr);

  return errorStatus;
}  // loginClient

int ServiceBroker::logoffClient(const SocketConnector::SPtr& frontendConnector,
                                const std::string& clientAddr)
{
  if (!frontendConnector)
  {
    std::string errMsg = "OptimizerBroker - logoffClient: empty front-end connector";
    spdlog::error(errMsg);
    throw std::invalid_argument(errMsg);
  }

  int errorStatus = optimizerbroker::BROKER_ERROR_STATUS_NO_ERROR;

  // Check for login - simulate login success
  auto logoffReplyMsg = brokerutils::buildLogoffSuccessMessage();
  std::string payload = logoffReplyMsg.SerializeAsString();

  // Send login message to client
  brokerutils::sendPayloadToClient(frontendConnector, payload, clientAddr);

  return errorStatus;
}  // loginClient

int ServiceBroker::registerClient(const std::string& clientAddr)
{
  int errorStatus = optimizerbroker::BROKER_ERROR_STATUS_NO_ERROR;
  spdlog::info("OptimizerBroker - registering new client " + clientAddr);

  // Find available worker
  const auto& workerId = getWorkerForClient();
  spdlog::info("OptimizerBroker - pairing client " + clientAddr + " with worker " + workerId);

  // Store the pairing in the client-worker map
  assert(isNewClient(clientAddr));
  assert(pWorkerMap.find(workerId) != pWorkerMap.end());
  pClientWorkerMap[clientAddr] = workerId;

  return errorStatus;
}  // registerClient

int ServiceBroker::unregisterClient(const std::string& clientAddr)
{
  int errorStatus = optimizerbroker::BROKER_ERROR_STATUS_NO_ERROR;
  pClientWorkerMap.erase(clientAddr);

  return errorStatus;
}  // unregisterClient

const std::string& ServiceBroker::getWorkerForClient() const
{
  if (pWorkerPriorityList.empty())
  {
    const std::string errMsg =
        "OptimizerBroker - no registered workers to respond to client's requests";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  int winngingWorker = 0;
  std::size_t workCount = pWorkerPriorityList[0]->WorkerCtr;
  for (int workerIdx = 1; workerIdx < static_cast<int>(pWorkerPriorityList.size()); ++workerIdx)
  {
    if (pWorkerPriorityList[workerIdx]->WorkerCtr < workCount)
    {
      workCount = pWorkerPriorityList[workerIdx]->WorkerCtr;
      winngingWorker = workerIdx;
    }
  }

  return pWorkerPriorityList[winngingWorker]->WorkerId;
}  // getWorkerForClient

}  // namespace optilab
