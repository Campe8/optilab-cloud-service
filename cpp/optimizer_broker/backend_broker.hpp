//
// Copyright OptiLab 2019. All rights reserved.
//
// Backend broker.
// It is the component between back-end workers and other (sub) workers.
//

#pragma once

#include <memory>   // for std::shared_ptr
#include <string>
#include <deque>
#include <utility>  // for std;:pair

#include "optimizer_broker/optimizer_broker.hpp"
#include "optimizer_service/socket_connector.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS BackendBroker : public OptimizerBroker {
public:
  using SPtr = std::shared_ptr<BackendBroker>;

public:
  /// Constructor: creates a new broker connecting the front-end to the back-end with the given
  /// two ports and given addresses.
  /// @note if the addresses are empty, "localhost" will be used.
  /// @note throws std::invalid_argument if the ports are less than 1 or greater than
  /// optimizerservice::MAX_SERVICE_PORT_NUMBER
  BackendBroker(const int frontendPort, const int backendPort,
                const std::string& frontendAddr = std::string(),
                const std::string& backendAddr = std::string());

  virtual ~BackendBroker() = default;

private:
  /// Job information as a pair <address_requester, payload>
  using JobInfo = std::pair<std::string, std::string>;

  /// Queue of jobs
  using JobQueue = std::deque<JobInfo>;

  /// Map of pairs <worker id, worker address>
  using WorkerMap = std::unordered_map<std::string, std::string>;

protected:

  /// Returns true if there is at least one worker that is ready to accept requests
  /// from clients. Returns false otherwise
  bool workersReady() const override;

  /// Handles client activity (i.e., this is the implementation method).
  /// @note "payload" is the payload as received from the client
  int handleClient(const std::string& clientAddr, const std::string& payload) override;

  /// Handles worker activity (i.e., this is the implementation method).
  /// @note "payload" is the payload as received from the worker
  int handleWorker(const std::string& workerAddr, const std::string& recipientAddr,
                   const std::string& payload) override;

private:
  /// Startup flag indicating whether or not the broker is running
  JobQueue pJobQueue;

  /// Queue of available, ready workers
  std::deque<std::string> pReadyWorkers;

  /// Map of registered workers
  WorkerMap pWorkerMap;

  /// Reverse lookup map worker address to worker id
  std::unordered_map<std::string, std::string> pReverseWorkerMap;

  /// Registers a new worker pairing it with the provided worker address.
  /// Returns zero on success, non-zero otherwise.
  /// @note workers are registered and they are persistent.
  /// On the other end, clients come and go.
  /// @note throws std::runtime_error if the worker id is an empty string
  int registerWorker(const std::string& workerId, const std::string& workerAddr);
};

}  // namespace optilab
