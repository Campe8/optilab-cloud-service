//
// Copyright OptiLab 2019. All rights reserved.
//
// This is an asynchronous thread processor.
// This thread processor runs each work unit asynchronously
// from the thread that posted the unit of work.
// All the unit of tasks are pushed into a queue and this
// processor uses a parallel thread to process the units.
// Units of work are not "strictly processed in this processor.
// Instead, they are published/notified to whatever observer is
// observing this publisher.
// The notification will call the "execute(...)" method of
// next processor/observer.
// If next processor is not another asynch. processor,
// then this thread that notified next processor, will also
// execute the actual work.
// Basically, when this asynch. thread calls "notifyOnWork(...)",
// it actually calls the "execute(...)" method of the observer,
// which, in turn, executes the unit of work.
//

#pragma once

#include <algorithm>
#include <cassert>
#include <deque>
#include <memory>  // for std::shared_ptr
#include <string>

#include <boost/bind.hpp>
#include <boost/bind/apply.hpp>
#include <boost/chrono.hpp>
#include <boost/function.hpp>
#include <boost/optional.hpp>
#include <boost/scoped_ptr.hpp>
#include <boost/thread.hpp>

#include <spdlog/spdlog.h>

#include "engine/optimizer_processor.hpp"

namespace {
constexpr int kDefaultTurnDownTimeOutMsec = 5000;
}

namespace optilab {

template <class WorkType>
class AsyncThreadProcessor : public OptimizerProcessor<WorkType, WorkType> {
 public:
  using SPtr = std::shared_ptr<AsyncThreadProcessor<WorkType>>;

 public:
  explicit AsyncThreadProcessor(const std::string& procName = "AsyncThreadProcessor")
  : OptimizerProcessor<WorkType, WorkType>(procName),
    pNumWorkUnitsInQueue(0),
    pNumWorkBatchesInProgress(0),
    pTerminateProcessor(false),
    pTurnDownTimeout(boost::chrono::milliseconds(kDefaultTurnDownTimeOutMsec)),
    pExceptionInAsynchThread(false)
  {
  }

  ~AsyncThreadProcessor()
  {
    if (pWorkerThread && pWorkerThread->joinable())
    {
      // Try to interrupt the asynch. thread.
      // @note interrupt works on boost threads
      pWorkerThread->interrupt();

      try
      {
        boost::this_thread::disable_interruption di;

        // Try to join before destroying the thread in order to avoid
        // calls to "terminate(...)"
        pWorkerThread->try_join_for(boost::chrono::milliseconds(1));
      }
      catch (...)
      {
      }
    }
  }

  /// Overrides "init(...)" method of base class "OptimizerBaseProcessor"
  void init() override
  {
    // Critical section
    LockGuard lock(pProcessorMutex);

    // Create the task for the run loop
    boost::packaged_task<void> task(boost::bind(&ThisProc::run, this));

    // Get the future/shared state from the packaged task
    pWorkerFuture = task.get_future();

    // Run the thread on the task,
    // i.e., on the "run(...) method
    pWorkerThread.reset(new boost::thread(boost::move(task)));

    // Check if the future throws
    checkForFutureException();

    // Add a new task right away,
    // i.e., notify observers on action "init".
    // This will make the asynch. thread use the notification method of the
    // base class "OptimizerPublisher" to notify all attached "OptimizerObserver"(s)
    // and run their "init(...)" method
    addWorkToQueue(
        WorkUnit(boost::bind(&ThisProc::notifyOnAction, this,
                             OptimizerPublisher<WorkType>::NotificationAction::NA_INIT)));
  }

  /// Process work method from the base class "OptimizerObserver"
  void processWork(WorkType workUnit) override
  {
    // Critical section (lock on the queue of work)
    LockGuard lock(pProcessorMutex);

    // Check for future/shared state with parallel thread exceptions.
    // If any, throw
    checkForFutureException();

    // Wrap the given work unit into a WorkUnit object and push that object in the queue
    // of work to process.
    // In turn, the WorkUnit object will call the method "doWork" of the
    // base class "OptimizerProcessor", having the following work-flow:
    // 1) OptimizerProcessor::doWork       ->
    // 2) OptimizerPublisher::notifyOnWork ->
    // 3) OptimizerObserver::execute       ->
    // 4) OptimizerObserver::processWork
    addWorkToQueue(WorkUnit(boost::bind(&ThisProc::doWork, this, workUnit)));
  }

  /// Override the flush method of the base "OptimizerObserver" class
  /// since the flush must be performed by the asynch. thread and not this thread
  void flush() override
  {
    // Critical section (lock on the queue)
    LockGuard lock(pProcessorMutex);

    // Check for future exception
    checkForFutureException();

    // Send a flush event from the publisher to the observers
    addWorkToQueue(
        WorkUnit(boost::bind(&ThisProc::notifyOnAction, this,
                             OptimizerPublisher<WorkType>::NotificationAction::NA_FLUSH)));
  }

  /// Override the turn-down method of the base class "OptimizerProcessor"
  /// in order to turn down the asynch. thread
  void turnDown() override
  {
    bool terminateProcessor = false;
    {
      // Critical section (lock on the terminate processor flag)
      LockGuard lock(pProcessorMutex);
      assert(pWorkerFuture.valid());

      // Set the terminate process variable and
      // release the mutex asap for other operations on the queue to complete
      terminateProcessor = pTerminateProcessor;
    }

    if (terminateProcessor)
    {
      // Already in terminate mode.
      // This means that the asynch. thread is no longer running.
      // The call to the "join(...)" method must be performed anyways to prevent
      // a call to "terminate(...)" when destroying the thread object
      pWorkerThread->join();

      // Use this thread to notify observers on TURN_DOWN action since the
      // async. thread is no longer running/available but there may be
      // still work units in the queue
      this->notifyOnAction(OptimizerPublisher<WorkType>::NotificationAction::NA_TURN_DOWN);
    }
    else
    {
      // The asynch. worker is active and runnning and must be turned down
      turnDownAsyncThread();
    }

    // Critical section.
    // @note here the asynch. thread is not running)
    LockGuard lock(pProcessorMutex);
    try
    {
      // Get the value from the future
      if (!pExceptionInAsynchThread)
      {
        pWorkerFuture.get();
      }
    }
    catch (const std::exception& ex)
    {
      spdlog::error("AsyncThreadProcessor - turnDown: error " + std::string(ex.what()));
    }
    catch (...)
    {
      spdlog::error("AsyncThreadProcessor - turnDown: unknown exception");
    }

    // Reset the pointer to the asynch. thread
    pWorkerThread.reset();
  }

  /// Override the "numProcessingTask(...)" method of the base class "OptimizerBaseProcessor"
  int numProcessingTask() const override
  {
    // Critical section
    LockGuard lock(pProcessorMutex);

    // Check for future exception
    checkForFutureException();

    // The number of processing tasks is the number of work units in the queue
    // plus the number of work batches currently in progress
    return numTaskInTheQueue() + pNumWorkBatchesInProgress;
  }

  /// Override the "waitForTaskToComplete(...)" method of the base class "OptimizerBaseProcessor"
  void waitForTaskToComplete() const override
  {
    // Critical section
    boost::unique_lock<boost::mutex> lock(pProcessorMutex);

    // Check the number of work units in the queue.
    // If the number is greater than zero, wait on the corresponding condition variable
    // until that number goes down to zero
    pNumWorkUnitsZeroConditionVar.wait(lock, boost::bind(&ThisProc::isWorkQueueEmpty, this));

    // Check for future exception
    checkForFutureException();
  }

 private:
  /// Utility typedef
  using ThisProc = AsyncThreadProcessor<WorkType>;

  /// Lock type on the internal mutex
  using LockGuard = boost::lock_guard<boost::mutex>;

  /**
   * Wrapper class around a unit of work or type WorkType
   */
  class WorkUnit {
   public:
    WorkUnit() : pWorkUnitFcnCaller(noop), pWorkUnit(boost::none) {}

    template <class WorkFcnCallerType>
    explicit WorkUnit(WorkFcnCallerType fcn)
    : pWorkUnitFcnCaller(fcn),
      pWorkUnit(boost::none)
    {
      assert(pWorkUnitFcnCaller);
    }

    template <class WorkFcnCallerType>
    explicit WorkUnit(WorkFcnCallerType fcn, const WorkType& workUnit)
    : pWorkUnitFcnCaller(fcn),
      pWorkUnit(workUnit)
    {
      assert(pWorkUnitFcnCaller);
    }

    void operator()() const
    {
      // Call the work function.
      // @note here it is possible to do some pre/post processing
      // tasks before/after calling the actual function
      pWorkUnitFcnCaller();
    }

    /// Returns the actual work unit hold by this class
    inline boost::optional<WorkType> getWorkUnit() const { return (pWorkUnit); }

   private:
    boost::function<void()> pWorkUnitFcnCaller;
    boost::optional<WorkType> pWorkUnit;

    static void noop() {}
  };

  /// Work queue type
  using WorkQueue = std::deque<WorkUnit>;

 private:
  /// Actual (de)queue of work units
  WorkQueue pWorkQueue;

  /// Mutex synch.ing the state of this processor
  mutable boost::mutex pProcessorMutex;

  /// Number of work units in the queue ready to be processed
  int pNumWorkUnitsInQueue;

  /// Number of work batches currently being processed by the asynch. thread.
  /// @note the asynch. thread processes batches of work units
  int pNumWorkBatchesInProgress;

  /// Flag indicating whether the processor has to terminate or not
  bool pTerminateProcessor;

  /// Condition variable to synchronize read/writes on the
  /// internal queue of WorkUnits
  mutable boost::condition_variable pWorkQueueConditionVar;

  /// Condition variable triggered when the number of work units to process
  /// goes down to zero
  mutable boost::condition_variable pNumWorkUnitsZeroConditionVar;

  // Timeout for turn down
  const boost::chrono::milliseconds pTurnDownTimeout;

  /// Keep track of exceptions in the asynch. thread
  mutable bool pExceptionInAsynchThread;

  /// Thread executing work
  boost::scoped_ptr<boost::thread> pWorkerThread;

  /// Future/shared state for the worker
  mutable boost::unique_future<void> pWorkerFuture;

  /// Run function performed by the asynch. thread
  void run()
  {
    try
    {
      runAsynchWorkLoop();
    }
    catch (boost::thread_interrupted& ex)
    {
      spdlog::error("AsyncThreadProcessor - run: asynchronous thread has been interrupted");
      LockGuard lock(pProcessorMutex);
      handleWorkerThreadException();
    }
    catch (...)
    {
      LockGuard lock(pProcessorMutex);
      handleWorkerThreadException();
      throw;
    }
  }

  void handleWorkerThreadException()
  {
    // If the shutdown flag has been set, yet work remains on the queue,
    // the remaining work includes publishSignalShutdown(). We publish
    // this signal now: this is particularly important when there are
    // threading processors downstream--otherwise they do not shut down
    if (pTerminateProcessor && !pWorkQueue.empty())
    {
      this->notifyOnAction(OptimizerPublisher<WorkType>::NotificationAction::NA_TURN_DOWN);
    }

    pTerminateProcessor = true;
    pWorkQueue.clear();
    pNumWorkBatchesInProgress = 0;
    pNumWorkUnitsZeroConditionVar.notify_all();
  }

  /// Asynch. loop for reader thread
  void runAsynchWorkLoop()
  {
    WorkQueue tempWorkQueue;
    while (true)
    {
      {
        // Loop forever trying to read and perform some work.
        // @note critical section since operating on the queue of work
        boost::unique_lock<boost::mutex> lock(pProcessorMutex);
        pNumWorkBatchesInProgress = 0;
        if (pWorkQueue.empty())
        {
          // The queue of work units is empty.
          // Notify whoever is waiting for the work units to be all processed that there are no
          // no work units anymore in the queue
          pNumWorkUnitsZeroConditionVar.notify_all();

          // Check whether or not it is time to terminate the processor and hence this loop
          if (pTerminateProcessor) { return; }
        }

        // Wait until there is some work to do in the queue
        pWorkQueueConditionVar.wait(lock, boost::bind(&WorkQueue::size, &pWorkQueue));

        // Update the work in progress
        pNumWorkBatchesInProgress = numTaskInTheQueue();

        // Swap the queues
        assert(tempWorkQueue.empty());
        tempWorkQueue.swap(pWorkQueue);

        // Set to zero the number of work units in the queue
        pNumWorkUnitsInQueue = 0;

        // Exit critical section
      }

      // Execute the work units in the queue
      std::for_each(tempWorkQueue.begin(), tempWorkQueue.end(), boost::apply<void>());
      tempWorkQueue.clear();
    }
  }

  /// Returns the number of tasks in the queue.
  /// @note this is not the number of tasks units but the
  /// number of batches of tasks submitted to the queue
  int numTaskInTheQueue() const
  {
    if (pWorkQueue.empty())
    {
      return 0;
    }

    // One batch in the queue.
    // @note this is valid since the asynch. thread process
    // the full queue in one run.
    // In other words, the asynch. thread processes batches of work units
    return 1;
  }

  void turnDownAsyncThread()
  {
    {
      // Critical section
      LockGuard lock(pProcessorMutex);

      // Publish a turn-down signal to all observers
      pWorkQueue.push_back(
          WorkUnit(boost::bind(&ThisProc::notifyOnAction, this,
                               OptimizerPublisher<WorkType>::NotificationAction::NA_TURN_DOWN)));

      pTerminateProcessor = true;
    }

    // Notify the asynch. reader thread about the turn-down notification
    pWorkQueueConditionVar.notify_one();

    // Wait for the shutdown
    boost::future_status waitStatus;
    {
      waitStatus = pWorkerFuture.wait_for(pTurnDownTimeout);
    }

    if (waitStatus == boost::future_status::timeout)
    {
      pWorkerThread->interrupt();
    }

    boost::this_thread::disable_interruption di;
    static const uint32_t WAIT_PERIOD = 5;
    bool joined = false;
    uint32_t waitTime = 0;
    while (!joined)
    {
      joined = pWorkerThread->try_join_for(boost::chrono::minutes(WAIT_PERIOD));
      if (!joined)
      {
        waitTime += WAIT_PERIOD;
      }
    }
  }

  void checkForFutureException() const
  {
    if (pWorkerFuture.has_exception())
    {
      // Set exception flag
      pExceptionInAsynchThread = true;

      // Re-throw
      pWorkerFuture.get();
    }
  }

  bool isWorkQueueEmpty() const
  {
    // The work queue is empty if:
    // 1) the processor is terminated; or
    // 2) the work queue is empty and there is no work in progress
    return pTerminateProcessor || (pWorkQueue.empty() && pNumWorkBatchesInProgress == 0);
  }

  /// Adds the given work into the queue of work units to be executed
  /// by the asynchronous thread
  void addWorkToQueue(const WorkUnit& workUnit)
  {
    if (!pTerminateProcessor)
    {
      pWorkQueue.push_back(workUnit);
      pWorkQueueConditionVar.notify_one();
    }
  }

};

}  // namespace optilab
