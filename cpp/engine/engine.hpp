//
// Copyright OptiLab 2019. All rights reserved.
//
// Base class for an engine.
// An (optimizer) engine is the OptiLab unit that executes optimization or, more generally,
// solves an OptiLab/AI problem.
// For example, an engine can solve CP problems, MIP problems, etc.
// An engine contains a pipeline of processors.
// Each tasks gets executed sequentially by each processor in the pipeline:
//
// Pipeline:
//            +------+------+-------+
// Task_in -> |  P1  |  P2  |  ...  | -> Task_out
//            +------+------+-------+
//
// where each P_i is a different processor.
// An engine can contain one or more processing pipelines.
// By default, an engine contains only one pipeline.
// To use an engine, a client must do what follows:
// 1) construct the engine
// 2) start-up the engine, see "startUp(...)"
// 3) use the engine
// 4) tear-down the engine, see tearDow(...)"
// 5) destruct the engine
//

#pragma once

#include <memory>   // for std::shared_ptr
#include <string>
#include <vector>

#include "engine/processor_pipeline.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS Engine {
 public:
  using SPtr = std::shared_ptr<Engine>;

 public:
  Engine() = default;

  /// Creates an engine with given name and specified number of pipelines.
  /// @note throw std::invalid_argument if the number of pipelines is less than 1
  Engine(const std::string& engineName, int numPipelines = 1);

  virtual ~Engine() = default;

  /// Returns the name of this engine
  const std::string& getEngineName() const noexcept
  {
    return pEngineName;
  }

  /// Returns the number of pipelines in this engine
  const int getNumPipelines() const noexcept { return pNumPipelines; }

  /// Starts up the engine
  void startUp();

  /// Tear down this engine and releases resources
  virtual void tearDown() {}

  /// Returns the specified processor pipeline.
  /// @note throws if the number is invalid
  ProcessorPipeline::SPtr getProcessorPipeline(int pipeline = 0) const;

  /// Returns true if this engine is active, false otherwise
  virtual bool isActive() const = 0;

  /// Returns the number of tasks in the specified processor pipeline
  virtual int numTasksInPipeline(int pipeline = 0) const = 0;

  /// Blocks the calling thread for "timeoutMsec" seconds or, if -1, until
  /// the specified pipeline is done processing all the tasks in its queue of tasks
  virtual void waitOnTaskCompletion(int timeoutMsec, int pipeline = 0) const = 0;

 protected:
  /// Number of pipelines in this engine
  int pNumPipelines{-1};

  /// Pipeline of processors, running tasks in FIFO order
  std::vector<ProcessorPipeline::SPtr> pProcessorPipelineList;

  /// Initializes this engine,
  /// e.g., builds the processor pipeline
  virtual void init();

  /// Actual start-up
  virtual void performStartup() = 0;

  /// Builds and returns each pipeline to be used by this engine.
  /// This method must return a valid pipeline instance for each index in
  /// [0, getNumPipelines() - 1]
  virtual ProcessorPipeline::SPtr buildPipeline(int pipeline = 0) = 0;

 private:
  /// Name of this engine
  const std::string pEngineName;
};

}  // namespace optilab
