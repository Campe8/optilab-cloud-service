//
// Copyright OptiLab 2019. All rights reserved.
//
// Utilities for engine
//

#pragma once

#include <memory>  // for std::shared_ptr
#include <string>

#include "data_structure/json/json.hpp"
#include "data_structure/metrics/metrics_register.hpp"
#include "engine/engine_constants.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

namespace engineutils {

/// Creates a JSON object representing a CP model that is satisfiable
SYS_EXPORT_FCN JSON::SPtr createCPSatSolutionJson(const std::string& modelName);

/// Creates a JSON object representing a CP model that is unsatisfiable
SYS_EXPORT_FCN JSON::SPtr createCPUnsatSolutionJson(const std::string& modelName);

/// Creates a JSON object containing the metrics from the given metrics register
SYS_EXPORT_FCN JSON::SPtr createMetricsJson(MetricsRegister::SPtr metricsReg);

/// Returns the EngineFrameworkType given string
SYS_EXPORT_FCN EngineFrameworkType getEngineFrameworkTypeFromString(
    const std::string& frameworkType);

/// Returns the Optimizer package type given string
SYS_EXPORT_FCN OptimizerPkg getEnginePackageTypeFromString(const std::string& packageType);

/// Returns the EngineClassType given string
SYS_EXPORT_FCN EngineClassType getEngineClassTypeFromString(const std::string& classType);

}  // namespave engineutils

}  // namespace optilab
