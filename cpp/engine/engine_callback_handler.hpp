//
// Copyright OptiLab 2019-2020. All rights reserved.
//
// Callback handler used to pass messages from the engine
// to the caller.
//

#pragma once

#include <memory>  // for std::shared_ptr

#include <google/protobuf/message.h>

#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS EngineCallbackHandler {
 public:
  using SPtr = std::shared_ptr<EngineCallbackHandler>;

 public:
  virtual ~EngineCallbackHandler() = default;

  /// Callback for metrics
  virtual void metricsCallback(const std::string& engineId, const std::string& jsonMetrics) = 0;

  /// Callback for meta-data
  virtual void metadataCallback(const std::string& engineId, const std::string& jsonMetadata) = 0;

  /// Callback for optimizers to communicate process completion
  virtual void processCompletionCallback(const std::string& engineId) = 0;

  /// Callback for optimizers results
  virtual void resultCallback(const std::string& engineId, const std::string& jsonResult) = 0;

  /// Callback for optimizer results sent as protocol buffer messages.
  /// @note as of 1/21/20 not all back-end engines support protocol buffers as result message
  virtual void resultCallback(const std::string& engineId,
                              const ::google::protobuf::Message& protobufResult) {}
};

}  // namespace optilab
