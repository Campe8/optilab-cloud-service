#include "engine/engine_constants.hpp"

#include <limits>

namespace optilab {

namespace engineconsts {
const char ENGINE_CLASS_TYPE_COMBINATOR[] = "Combinator";
const char ENGINE_CLASS_TYPE_CP[] = "CP";
const char ENGINE_CLASS_TYPE_GA[] = "GA";
const char ENGINE_CLASS_TYPE_MIP[] = "MIP";
const char ENGINE_CLASS_TYPE_SCHEDULING[] = "SCHEDULING";
const char ENGINE_CLASS_TYPE_VRP[] = "VRP";
const char ENGINE_ERROR_MSG[] = "engine_error";
const char ENGINE_FRAMEWORK_TYPE_CONNECTORS[] = "Connectors";
const char ENGINE_FRAMEWORK_TYPE_EC[] = "EC";
const char ENGINE_FRAMEWORK_TYPE_OR[] = "OR";
const char ENGINE_PACKAGE_TYPE_GECODE[] = "GECODE";
const char ENGINE_PACKAGE_TYPE_OPEN_GA[] = "OPEN-GA";
const char ENGINE_PACKAGE_TYPE_OR_TOOLS[] = "OR-TOOLS";
const char ENGINE_PACKAGE_TYPE_SCIP[] = "SCIP";
}  // namespace engineconsts

namespace jsonsolution {
const char MODEL_ID[] = "modelId";
const char MODEL_STATUS[] = "modelStatus";
const char NUM_SOLUTIONS[] = "numSolutions";
const char OBJECTIVE_VALUE_LIST[] = "objectiveValueList";
const char SOLUTION_ASSIGNMENT_LIST[] = "solutionAssignmentList";
const char SOLUTION_DESCRIPTOR[] = "solutionDescriptor";
const char SOLUTION_LIST[] = "solutionList";
const char STATUS_FEASIBLE[] = "feasible";
const char STATUS_INFEASIBLE[] = "infeasible";
const char STATUS_INVALID[] = "invalid";
const char STATUS_NOT_SOLVED[] = "notSolved";
const char STATUS_OPTIMAL[] = "optimal";
const char STATUS_SATISFIED[] = "satisfied";
const char STATUS_TIMEOUT[] = "timeout";
const char STATUS_UNSATISFIED[] = "unsatisfied";
const char VAR_DOMAIN_LIST[] = "dom";
const char VAR_IDENTIFIER[] = "id";
}  // namespace jsonsolution

namespace jsoncombinatorsolution {
const char SOLUTION_LIST[] = "solutionList";
}  // jsoncombinatorsolution

namespace jsonschedulingsolution {
const char DAY[] = "day";
const char OBJECTIVE_VALUES_LIST[] = "objectiveValuesList";
const char SHIFTS[] = "shifts";
}  // jsonschedulingsolution

namespace jsonvrpsolution {
const char LOAD[] = "load";
const char OBJECTIVE_VALUES_LIST[] = "objectiveValuesList";
const char ROUTE[] = "route";
const char TOT_DISTANCE_ALL_ROUTES[] = "totDistanceAllRoutes";
const char TOT_LOAD_ALL_ROUTES[] = "totLoadAllRoutes";
const char TOT_ROUTE_DISTANCE[] = "totRouteDistance";
const char TOT_ROUTE_LOAD[] = "totRouteLoad";
const char VEHICLE_ID[] = "vehicleId";
}  // jsonvrpsolution

namespace jsonmetrics {
const char ENGINE_CREATION_TIME_MSEC[] = "engineCreationTimeMsec";
const char LOAD_ENVIRONMENT_LATENCY_MSEC[] = "loadEnvironmentLatencyMsec";
const char LOAD_MODEL_LATENCY_MSEC[] = "loadModelLatencyMsec";
const char OPTIMIZER_LATENCY_MSEC[] = "optimizerLatencyMsec";
}  // jsonmetrics


namespace jsonmipsolution {
const char OBJECTIVE_VALUES_LIST[] = "objectiveValuesList";
const char RESULT_FEASIBLE[] = "feasible";
const char RESULT_INFEASIBLE[] = "infeasible";
const char RESULT_NOT_SOLVED[] = "notSolved";
const char RESULT_OPTIMUM[] = "optimum";
const char RESULT_UNBOUNDED[] = "unbounded";
}  // jsonmipsolution


}  // namespace optilab
