//
// Copyright OptiLab 2019. All rights reserved.
//
// Pipeline of processors.
//

#pragma once

#include <memory>  // for std::shared_ptr
#include <deque>

#include "engine/optimizer_processor.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS ProcessorPipeline {
 public:
  using SPtr = std::shared_ptr<ProcessorPipeline>;

 public:
  /// Returns the actual pipeline, i.e., the list of processors
  inline const std::deque<OptimizerBaseProcessor::SPtr>& getPipeline() const { return pPipeline; }

  /// Push the given processor to the front of the pipeline.
  /// The processor pushed into the front of the pipeline is used as a "publisher"
  /// publishing its result to the next (current front before push) processor which
  /// will become a subscriber:
  /// Pipeline before:
  /// [ P_1 | P_2 | ... ]
  /// Pipeline after pushing P_j to the front:
  /// [ P_j | P_1 | P_2 | ... ]
  /// where P_j is a publisher and P_1 becomes a subscriber and so on.
  /// In turn, P_1 is a publisher for P_2 observer, etc.
  template<class Proc>
  void pushFrontProcessor(std::shared_ptr<Proc> processor)
  {
    if (!pPipeline.empty())
    {
      processor->registerOptimizerObserver(observerDowncast(pPipeline.front(), *processor));
    }
    pPipeline.push_front(processor);
  }

  /// Push the given processor to the back of the pipeline.
  /// The processor pushed into the back of the pipeline is used as a "subscriber"
  /// subscribing to the current back of the pipeline.
  /// Pipeline before:
  /// [ P_i-1 | P_i ]
  /// Pipeline after pushing P_i+1 to the back:
  /// [ P_i-1 | P_i | P_i+1 ]
  /// where P_i becomes a publisher and P_i+1 becomes a subscriber to it
  template<class Proc>
  void pushBackProcessor(std::shared_ptr<Proc> processor)
  {
    if (!pPipeline.empty())
    {
      publisherDowncast(pPipeline.back(), *processor)->registerOptimizerObserver(processor);
    }
    pPipeline.push_back(processor);
  }

  /// Returns the processor at the front of the pipeline.
  /// @note the returned processor is casted to an OptimizerObserver.
  /// This allows the caller of this method to directly execute work by calling the
  /// "execute(...)" method of the optimizer observer.
  /// The execute will propagate downstream the work to the other processors
  template <class Work>
  typename OptimizerObserver<Work>::SPtr front() const
  {
    typename OptimizerObserver<Work>::SPtr optObserver(
        std::dynamic_pointer_cast<OptimizerObserver<Work>>(pPipeline.front()));
    if (!optObserver)
    {
      throw std::logic_error("ProcessorPipeline - front: "
          "invalid downcast to OptimizerObserver for processor " +
          pPipeline.front()->getProcessorName());
    }
    return optObserver;
  }

  /// Returns the processor at the back of the pipeline.
  /// @note the returned processor is casted to an OptimizerPublisher
  template <class Work>
  typename OptimizerPublisher<Work>::SPtr back() const
  {
    typename OptimizerPublisher<Work>::SPtr optPublisher(
        std::dynamic_pointer_cast<OptimizerPublisher<Work>>(pPipeline.back()));
    if (!optPublisher)
    {
      throw std::logic_error("ProcessorPipeline - back: "
          "invalid downcast to OptimizerPublisher for processor " +
          pPipeline.back()->getProcessorName());
    }
    return optPublisher;
  }

  /// Looks into the pipeline for the specified (type of) processor and returns
  /// it if found.
  /// Returns an empty pointer otherwise
  template <class OptimizerType>
  typename OptimizerType::SPtr find(const std::string& procName) const
  {
    for (int procIdx = 0; procIdx < pPipeline.size(); ++procIdx)
    {
      if (pPipeline.at(procIdx)->getProcessorName() == procName)
      {
        typename OptimizerType::SPtr proc(
            std::dynamic_pointer_cast<OptimizerType>(pPipeline.at(procIdx)));
        if (!proc)
        {
          throw std::logic_error("ProcessorPipeline - find: "
              "invalid downcast to specifier type for processor " +
              pPipeline.at(procIdx)->getProcessorName());
        }
        return proc;
      }
    }

    // Return and empty pointer
    return typename OptimizerType::SPtr();
  }

  /// Returns the total number of tasks, i.e., total work
  /// in the pipeline distributed across processors
  int totWorkInPipeline() const;

  /// Blocks the calling thread until all tasks in the pipeline are processed by
  /// all processors.
  /// @note this doesn't block the caller to keep pushing more tasks
  /// into the pipeline
  void waitOnEmptyPipeline() const;

 private:
  /// Actual pipeline collecting the processors.
  /// The pipeline is a deque that allows to append processor to the front and to
  /// the back of the pipeline.
  /// @note the type stored in the pipeline is a smart pointer to an optimizer
  /// base processor
  std::deque<OptimizerBaseProcessor::SPtr> pPipeline;

  /// Utility function: down-casts the given base optimizer to an observer optimizer
  template <class WorkType>
  static typename OptimizerObserver<WorkType>::SPtr observerDowncast(
      OptimizerBaseProcessor::SPtr optProc, const OptimizerPublisher<WorkType>&)
  {
    typename OptimizerObserver<WorkType>::SPtr observer =
        std::dynamic_pointer_cast<OptimizerObserver<WorkType>>(optProc);
    if (!observer)
    {
      throw std::runtime_error("ProcessorPipeline - observerDowncast: "
          "failed dynamic cast to OptimizerObserver type for processor " +
          optProc->getProcessorName());
    }
    return observer;
  }

  /// Utility function: down-casts the given base optimizer to a publisher optimizer
  template <class WorkType>
  static typename OptimizerPublisher<WorkType>::SPtr publisherDowncast(
      OptimizerBaseProcessor::SPtr optProc, const OptimizerObserver<WorkType>&)
  {
    typename OptimizerPublisher<WorkType>::SPtr publisher =
        std::dynamic_pointer_cast<OptimizerPublisher<WorkType>>(optProc);
    if (!publisher)
    {
      throw std::runtime_error("ProcessorPipeline - publisherDowncast: "
          "failed dynamic cast to OptimizerPublisher type for processor " +
          optProc->getProcessorName());
    }
    return publisher;
  }

};

}  // namespace optilab
