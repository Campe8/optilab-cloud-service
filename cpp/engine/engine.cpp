#include "engine/engine.hpp"

#include <stdexcept>  // for std::runtime_error

#include <spdlog/spdlog.h>

namespace optilab {

Engine::Engine(const std::string& engineName, int numPipelines)
: pNumPipelines(numPipelines),
  pEngineName(engineName)
{
  if (pNumPipelines < 1)
  {
    throw std::invalid_argument("Engine - invalid number of pipelines: " +
                                std::to_string(pNumPipelines));
  }
}

void Engine::init()
{
  // Builds the pipeline
  if (getNumPipelines() == 0)
  {
    spdlog::warn("Engine - init: no pipelines defined, return " + pEngineName);
    return;
  }

  for (int pIdx = 0; pIdx < getNumPipelines(); ++pIdx)
  {
    pProcessorPipelineList.push_back(buildPipeline(pIdx));
    if (!pProcessorPipelineList.back())
    {
      throw std::runtime_error("Engine - init: "
          "empty processor pipeline for pipeline " + std::to_string(pIdx));
    }
  }
}  // init

ProcessorPipeline::SPtr Engine::getProcessorPipeline(int pipeline) const
{
  if (pipeline < 0 || pipeline >= pNumPipelines)
  {
    const std::string errMsg = "Engine - getProcessorPipeline: "
        "invalid pipeline index " + std::to_string(pipeline);
    spdlog::error(errMsg);
    throw std::out_of_range(errMsg);
  }
  return pProcessorPipelineList.at(pipeline);
}  // getProcessorPipeline

void Engine::startUp()
{
  // Perform engine-specific start-up
  performStartup();
}

}  // namespace optilab
