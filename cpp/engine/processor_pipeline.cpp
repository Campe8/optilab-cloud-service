#include "engine/processor_pipeline.hpp"

#include <algorithm>   // for std::for_each
#include <functional>  // for std::bind
#include <numeric>     // for std::accumulate

namespace {

int getWorkFromProcessor(int initWork, const optilab::OptimizerBaseProcessor::SPtr& proc)
{
  return (initWork + proc->numProcessingTask());
}

}  // namespace

namespace optilab {

int ProcessorPipeline::totWorkInPipeline() const
{
  return std::accumulate(pPipeline.begin(), pPipeline.end(), 0, getWorkFromProcessor);
}  // totWorkInPipeline

void ProcessorPipeline::waitOnEmptyPipeline() const
{
  std::for_each(pPipeline.begin(), pPipeline.end(),
                [](OptimizerBaseProcessor::SPtr proc)
                {
                  proc->waitForTaskToComplete();
                });
}  //waitOnEmptyPipeline

}  // namespace optilab
