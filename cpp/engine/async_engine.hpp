//
// Copyright OptiLab 2019. All rights reserved.
//
// Asynchronous engine.
// It receives a sequence of tasks, pushes those tasks into a queue and performs those
// tasks asynchronously (FIFO) on a separate thread.
//

#pragma once

#include <cassert>
#include <chrono>
#include <exception>
#include <memory>     // for std::shared_ptr
#include <mutex>
#include <stdexcept>  // for std::logic_error
#include <string>
#include <thread>
#include <vector>

#include <boost/chrono.hpp>
#include <boost/thread.hpp>

#include <spdlog/spdlog.h>

#include "engine/async_thread_processor.hpp"
#include "engine/engine.hpp"
#include "system/system_export_defs.hpp"

namespace {
/// Sleep time in msec. to wait at each iteration before checking
/// if the queue of tasks is empty
constexpr int kThreadSleepingTimeMsec{50};
}  // namespace

namespace optilab {

  template<class Task>
  class AsyncEngine : public Engine {
  public:
    using SPtr = std::shared_ptr<AsyncEngine>;
    
  public:

    /// Returns the number of tasks in the processor pipeline
    int numTasksInPipeline(int pipeline = 0) const override
    {
      return getProcessorPipeline(pipeline)->totWorkInPipeline();
    }

    /// Tear-down call to stop and terminate this engine
    void tearDown() override
    {
      // Return asap is the processor is not active
      if (!isActive()) return;

      {
        // Critical section
        LockGuard lock(pProcessorMutex);

        for (auto& pipeline : pPipelineEntryPointList)
        {
          pipeline->turnDown();
          pipeline.reset();
        }
        pProcessorPipelineList.clear();
      }
    }

    /// Returns true if this processor is active, false otherwise
    bool isActive() const override
    {
      {
        // Critical section
        LockGuard lock(pProcessorMutex);
        for (const auto& pipeline : pPipelineEntryPointList)
        {
          if (!pipeline) return false;
        }
      }
      return true;
    }

    /// Blocks the calling thread for "aTimeoutMsec" seconds or, if -1, until
    /// the engine processor is done processing all the tasks in the queue
    void waitOnTaskCompletion(int timeoutMsec, int pipeline = 0) const override
    {
      using TimePoint = boost::chrono::system_clock::time_point;
      if (timeoutMsec < -1)
      {
        const std::string errMsg = "AsyncEngineProcessor - waitOnTaskCompletion: "
            "invalid timeout (msec) " + std::to_string(timeoutMsec);
        spdlog::error(errMsg);
        throw std::logic_error(errMsg);
      }

      // Get the time to wait until based on the give timeout in msec
      const TimePoint waitOn = boost::chrono::system_clock::now() +
          boost::chrono::milliseconds(timeoutMsec);
      {
        // Critical section.
        // @note acquire the mutex to block the queue.
        // No other thread can push another task into the queue.
        LockGuard lock(pProcessorMutex);

        // Loop until there are no more tasks to execute
        while (numTasksInPipeline(pipeline) > 0)
        {
          TimePoint waitUntil =  boost::chrono::system_clock::now();

          // Return asap if waited already too long
          if (timeoutMsec != -1 && waitUntil >= waitOn)
          {
            return;
          }

          // Sleep a bit before checking again
          waitUntil += boost::chrono::milliseconds(kThreadSleepingTimeMsec);

          if (timeoutMsec != -1)
          {
            waitUntil = std::min(waitUntil, waitOn);
          }

          try
          {
            // @note the following can throw an exception of type
            // boost::thread_interrupted
            // if this thread was interrupted for some reasons (e.g., on shutdown).
            // In this case, return asap since there is no backlog to wait on
            boost::this_thread::sleep_until(waitUntil);
          }
          catch(std::exception& ex)
          {
            const std::string errMsg = "AsyncEngineProcessor - waitOnTaskCompletion: "
                + std::string(ex.what());
            spdlog::error(errMsg);
            throw ex;
          }
          catch(...)
          {
            const std::string errMsg = "AsyncEngineProcessor - waitOnTaskCompletion: "
                "undefined error";
            spdlog::error(errMsg);
            throw;
          }
        }

        // Exit critical section and release the mutex.
        // This will unblock other threads waiting to push tasks in the processor queue
      }
    }
    
  protected:
    AsyncEngine(const std::string& engineName, int numPipelines = 1)
    : Engine(engineName, numPipelines)
   {
   }

    void init() override
    {
      // Initialize pipeline with parent method
      Engine::init();
      assert(!pProcessorPipelineList.empty());

      // Push the processor as first processor on the pipelines
      for (int idx = 0; idx < getNumPipelines(); ++idx)
      {
        // Create the asynchronous thread processor to push in front of the pipeline.
        // This processor will make each pipeline and asynchronous pipeline
        pProcessorPipelineList[idx]->pushFrontProcessor(std::make_shared<AsyncProc>());
      }
    }

    void performStartup() override
    {
      if (pProcessorPipelineList.empty())
      {
        throw std::runtime_error("AsyncEngine - performStartup: empty processor pipeline");
      }

      {
        // Critical section on the pipeline
        LockGuard lock(pProcessorMutex);

        // Resize the entry point list to hold one entry point per pipeline
        pPipelineEntryPointList.resize(getNumPipelines());

        // Get the first processor in the pipeline,
        // i.e., the entry point of the pipeline
        for (int idx = 0; idx < getNumPipelines(); ++idx)
        {
          pPipelineEntryPointList[idx] = pProcessorPipelineList[idx]->template front<Task>();
          assert(pPipelineEntryPointList[idx]);

          // Initialize the processors
          pPipelineEntryPointList[idx]->init();
        }
        // Leave critical section
      }
    }

    /// Pushes the given task into the queue of tasks to execute
    void pushTask(const Task& task, int pipeline = 0)
    {
      if (pipeline < 0 || pipeline >= getNumPipelines())
      {
        throw std::out_of_range("AsyncEngine - pushTask: "
            "invalid pipeline index " + std::to_string(pipeline));
      }

      {
        // Critical section
        LockGuard lock(pProcessorMutex);
        try
        {
          assert(pPipelineEntryPointList[pipeline]);
          pPipelineEntryPointList[pipeline]->execute(task);
        }
        catch (std::exception& ex)
        {
          const std::string errMsg = "AsyncEngineProcessor - pushTask: "
              "error on executing a task " + std::string(ex.what());
          spdlog::error(errMsg);
          pPipelineEntryPointList[pipeline]->turnDown();
          getProcessorPipeline(pipeline).reset();
          throw ex;
        }
        catch (...)
        {
          const std::string errMsg = "AsyncEngineProcessor - pushTask: "
              "undefined error on executing a task";
          spdlog::error(errMsg);
          pPipelineEntryPointList[pipeline]->turnDown();
          getProcessorPipeline(pipeline).reset();
          throw;
        }
      }
    }

    /// Flushes the pipeline
    void flushPipeline(int pipeline = 0)
    {
      if (pipeline < 0 || pipeline >= getNumPipelines())
      {
        throw std::out_of_range("AsyncEngine - pushTask: "
            "invalid pipeline index " + std::to_string(pipeline));
      }

      {
        // Critical section
        LockGuard lock(pProcessorMutex);
        try
        {
          pPipelineEntryPointList[pipeline]->flushWork();
        }
        catch (std::exception& ex)
        {
          const std::string errMsg = "AsyncEngineProcessor - flushPipeline: "
              "error on executing a task " + std::string(ex.what());
          spdlog::error(errMsg);
          pPipelineEntryPointList[pipeline]->turnDown();
          getProcessorPipeline(pipeline).reset();
          throw ex;
        }
        catch (...)
        {
          const std::string errMsg = "AsyncEngineProcessor - flushPipeline: "
              "error on executing a task";
          spdlog::error(errMsg);
          pPipelineEntryPointList[pipeline]->turnDown();
          getProcessorPipeline(pipeline).reset();
          throw;
        }
      }
    }

  private:
    /// Lock type on the internal mutex
    using LockGuard = boost::lock_guard<boost::mutex>;

    /// Asynchronous thread processor used to make this pipeline asynchronous
    using AsyncProc = AsyncThreadProcessor<Task>;

  private:
    /// Entry point for the processor pipeline,
    /// i.e., the first processor
    using EntryPointProcessor = OptimizerObserver<Task>;

    /// Pipeline entry points,
    /// i.e., the first observer processor
    std::vector<typename EntryPointProcessor::SPtr> pPipelineEntryPointList;

    /// Mutex synchronizing the access on the internal processor queue
    mutable boost::mutex pProcessorMutex;
  };
  
} // end namespace optilab
