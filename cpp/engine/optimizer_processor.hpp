//
// Copyright OptiLab 2019. All rights reserved.
//
// Processor for an optimizer.
//

#pragma once

#include <cassert>
#include <deque>
#include <memory>     // for std::shared_ptr
#include <stdexcept>  // for std::invalid_argument
#include <string>
#include <vector>

#include <boost/thread.hpp>

#include <spdlog/spdlog.h>

#include "system/system_export_defs.hpp"

namespace optilab {

/**
 * Optimizer processor: processing unit.
 * Workflow:
 * 1) constructor
 * 2) init()
 * 3) execute
 * 4) turnDown()
 * 5) destructor
 */
class SYS_EXPORT_CLASS OptimizerBaseProcessor {
 public:
  using SPtr = std::shared_ptr<OptimizerBaseProcessor>;

 public:
  OptimizerBaseProcessor(const std::string& procName=std::string())
   : pProcName(procName)
  {
  }

  virtual ~OptimizerBaseProcessor() = default;

  /// Returns the name of this processor
  const std::string& getProcessorName() const { return pProcName; }

  /// Initializes this processor
  virtual void init() {}

  /// Turns down this processor and cleans up resources
  virtual void turnDown() {}

  /// Returns the number of processing tasks in this processor.
  /// @note returns the total number of tasks in this processor,
  /// even if they have not yet started
  virtual int numProcessingTask() const = 0;

  /// Blocks the calling thread until the value returns by "numProcessingTask(...)" reaches zero.
  /// In other words, blocks the calling thread until all the tasks are processed
  /// by this processor
  virtual void waitForTaskToComplete() const = 0;

 private:
  /// Name of this processor
  std::string pProcName;
};

/**
 * Optimizer observer (pub/obs pattern).
 * Optimizer listening for events from publisher.
 * Observed events are then processed by this optimizer.
 */
template<class WorkIn>
class OptimizerObserver {
 public:
   using SPtr = std::shared_ptr<OptimizerObserver<WorkIn>>;

 public:
   virtual ~OptimizerObserver() = default;

   /// Executes the given unit of work
   virtual void execute(WorkIn workUnit)
   {
     boost::this_thread::interruption_point();
     preProcessWork(workUnit);
     processWork(workUnit);
     postProcessWork(workUnit);
   }

   /// Flushes any unit of work
   virtual void flushWork()
   {
     boost::this_thread::interruption_point();
     preFlush();
     flush();
     postFlush();
   }

   /// Initializes this observer
   virtual void init() {}

   /// Turns down this observer
   virtual void turnDown() {}

 protected:
   /// Pre-process the given unit of work.
   /// @note by default there is no pre-processing
   virtual void preProcessWork(WorkIn& workUnit) {}

   /// Process the given unit of work
   virtual void processWork(WorkIn workUnit) = 0;

   /// Post-process the given unit of work.
   /// @note by default there is no post-processing
   virtual void postProcessWork(WorkIn& workUnit) {}

   /// Pre-flushing operations.
   /// @note by default this is a no-op
   virtual void preFlush() {}

   /// Flushing operations.
   /// @note by default this is a no-op
   virtual void flush() {}

   /// Post-flushing operations.
   /// @note by default this is a no-op
   virtual void postFlush() {}
};

/**
 * Optimizer publisher (pub/sub pattern).
 * Optimizer listening for events from publisher.
 * Observed events are then processed by this optimizer.
 */
template<class WorkOut>
class OptimizerPublisher {
 public:
   using SPtr = std::shared_ptr<OptimizerPublisher<WorkOut>>;

   /// Notification actions to peform on observers
   enum NotificationAction {
     NA_INIT,
     NA_TURN_DOWN,
     NA_FLUSH
   };

 public:
   virtual ~OptimizerPublisher() = default;

   /// Register the given observer to this publisher
   void registerOptimizerObserver(typename OptimizerObserver<WorkOut>::SPtr obs)
   {
     if (!obs)
     {
       throw std::invalid_argument("OptimizerPublisher - registerOptimizerObserver: "
           "empty observer");
     }
     pObserverRegister.push_back(obs);
   }

   /// Notifies all subscribed observers about a given unit of work
   void notifyOnWork(WorkOut workUnit) const
   {
     for (auto& obs : pObserverRegister)
     {
       assert(obs);
       obs->execute(workUnit);
       boost::this_thread::interruption_point();
     }
   }

   /// Notifies all subscribed observers to perform an action
   void notifyOnAction(NotificationAction action)
   {
     for (auto& obs : pObserverRegister)
     {
       assert(obs);
       switch(action)
       {
         case NotificationAction::NA_INIT:
         {
           obs->init();
           break;
         }
         case NotificationAction::NA_TURN_DOWN:
         {
           obs->turnDown();
           break;
         }
         default:
         {
           if (action != NotificationAction::NA_FLUSH)
           {
             throw std::runtime_error("OptimizerPublisher - notifyOnAction: "
                 "invalid action " + std::to_string(action));
           }
           obs->flushWork();
           break;
         }
       }
       boost::this_thread::interruption_point();
     }
   }

 private:
   /// Register of (optimizer) observers
   std::vector<typename OptimizerObserver<WorkOut>::SPtr> pObserverRegister;
};

/**
 * Optimizer processor:
 * WorkIn -> Processor -> WorkOut
 */
template<class WorkIn, class WorkOut>
class OptimizerProcessor : public OptimizerObserver<WorkIn>,
                           public OptimizerPublisher<WorkOut>,
                           public OptimizerBaseProcessor {
public:
  using SPtr = std::shared_ptr<OptimizerProcessor<WorkIn, WorkOut>>;

public:
  OptimizerProcessor(const std::string& procName = "OptimizerProcessor")
  : OptimizerBaseProcessor(procName)
  {
  }

  virtual ~OptimizerProcessor() = default;

  void init() override
  {
    // Similar to C++ construction process:
    // 1) first initialize parent process
    OptimizerObserver<WorkIn>::init();

    // 2) then initialize this observer processors
    this->notifyOnAction(OptimizerPublisher<WorkOut>::NotificationAction::NA_INIT);
  }

  /// Pushes some unit of work to the attached observers
  void doWork(WorkOut workUnit) const { this->notifyOnWork(workUnit); }

  void turnDown() override
  {
    // Similar to C++ destruction process:
    // 1) first propagate turn-down to all observers of this publisher
    this->notifyOnAction(OptimizerPublisher<WorkOut>::NotificationAction::NA_TURN_DOWN);

    // 2) then turn-down parent instance
    OptimizerObserver<WorkIn>::turnDown();
  }

  virtual void flush() override
  {
    OptimizerObserver<WorkIn>::flush();

    // Propagate
    this->notifyOnAction(OptimizerPublisher<WorkOut>::NotificationAction::NA_FLUSH);
  }

  /// Default returns zero processing tasks
  int numProcessingTask() const override { return 0; }

  /// Default wait returns asap
  void waitForTaskToComplete() const override {}
};

}  // namespace optilab
