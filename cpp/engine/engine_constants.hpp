//
// Copyright OptiLab 2019. All rights reserved.
//
// Constants for engine.
//

#pragma once

namespace optilab {

/// This enumeration captures the different frameworks
/// available as optimizers.
enum EngineFrameworkType : int {
  /// Operational Research.
  /// @Deprecated
  OR_FRAMEWORK = 0,

  /// Connector blocks.
  /// @Deprecated
  CONNECTORS_FRAMEWORK,

  /// Back-end worker service.
  /// @Deprecated
  BACKEND_SERVICE_FRAMEWORK,

  /// Evolutionary computation.
  /// @Deprecated
  EC_FRAMEWORK,

  /// Routing toolbox framework
  ROUTING_TOOLBOX,

  /// Scheduling toolbox framework
  SCHEDULING_TOOLBOX,

  /// OR toolbox framework
  OR_TOOLBOX,

  /// Evolutionary toolbox
  EVOLUTIONARY_TOOLBOX,

  /// Data and ports toolbox
  DATA_AND_PORTS_TOOLBOX,

  /// Constraint Programming toolbox
  CP_TOOLBOX,

  /// Undefined framework
  UNDEF_FRAMEWORK
};

/// Optimizer package enumeration captures the different
/// packages supported by OptiLab.
/// Packages are usually third-party solver, engine, etc.
enum OptimizerPkg : int
{
  /// Google OR-Tools - https://developers.google.com/optimization/
  OP_OR_TOOLS = 0,

  /// Gecode - https://www.gecode.org/
  OP_GECODE,

  /// SCIP - https://scip.zib.de/
  OP_SCIP,

  /// Dummy type to separate OR packages
  OP_OR_PACKAGES_END,

  /// OpenGA - https://github.com/Arash-codedev/openGA
  OP_OPEN_GA,

  /// Undefined package.
  /// @note MUST BE LAST
  OP_UNDEF
};

/// This enumeration. captures the different framework classes
/// available as optimizers.
/// Roughly speaking, these are the "block types" provided
/// by all frameworks
enum EngineClassType : int {
  EC_COMBINATOR = 0,
  /// Constraint Programming
  EC_CP,
  /// Mixed Integer Programming
  EC_MIP,
  /// Genetic algorithms
  EC_GA,
  /// Vehicle Routing Problem
  EC_VRP,
  /// Scheduling Problem
  EC_SCHEDULING,
  EC_UNDEF
};

/// General engine constants
namespace engineconsts {
extern const char ENGINE_CLASS_TYPE_COMBINATOR[];
extern const char ENGINE_CLASS_TYPE_CP[];
extern const char ENGINE_CLASS_TYPE_GA[];
extern const char ENGINE_CLASS_TYPE_MIP[];
extern const char ENGINE_CLASS_TYPE_SCHEDULING[];
extern const char ENGINE_CLASS_TYPE_VRP[];
extern const char ENGINE_ERROR_MSG[];
extern const char ENGINE_FRAMEWORK_TYPE_CONNECTORS[];
extern const char ENGINE_FRAMEWORK_TYPE_EC[];
extern const char ENGINE_FRAMEWORK_TYPE_OR[];
extern const char ENGINE_PACKAGE_TYPE_GECODE[];
extern const char ENGINE_PACKAGE_TYPE_OPEN_GA[];
extern const char ENGINE_PACKAGE_TYPE_OR_TOOLS[];
extern const char ENGINE_PACKAGE_TYPE_SCIP[];
}  // namespace engineconsts

namespace jsonsolution {
extern const char MODEL_ID[];
extern const char MODEL_STATUS[];
extern const char NUM_SOLUTIONS[];
extern const char OBJECTIVE_VALUE_LIST[];
extern const char SOLUTION_ASSIGNMENT_LIST[];
extern const char SOLUTION_DESCRIPTOR[];
extern const char SOLUTION_LIST[];
extern const char STATUS_FEASIBLE[];
extern const char STATUS_INFEASIBLE[];
extern const char STATUS_INVALID[];
extern const char STATUS_NOT_SOLVED[];
extern const char STATUS_OPTIMAL[];
extern const char STATUS_SATISFIED[];
extern const char STATUS_TIMEOUT[];
extern const char STATUS_UNSATISFIED[];
extern const char VAR_DOMAIN_LIST[];
extern const char VAR_IDENTIFIER[];
}  // namespace jsonsolution

namespace jsoncombinatorsolution {
extern const char SOLUTION_LIST[];
}  // jsoncombinatorsolution

/// Scheduling-specific solution constants
namespace jsonschedulingsolution {
extern const char DAY[];
extern const char OBJECTIVE_VALUES_LIST[];
extern const char SHIFTS[];
}  // jsonschedulingsolution

/// VRP-specific solution constants
namespace jsonvrpsolution {
extern const char LOAD[];
extern const char OBJECTIVE_VALUES_LIST[];
extern const char ROUTE[];
extern const char TOT_DISTANCE_ALL_ROUTES[];
extern const char TOT_LOAD_ALL_ROUTES[];
extern const char TOT_ROUTE_DISTANCE[];
extern const char TOT_ROUTE_LOAD[];
extern const char VEHICLE_ID[];
}  // jsonvrpsolution

namespace jsonmetrics {
extern const char ENGINE_CREATION_TIME_MSEC[];
extern const char LOAD_ENVIRONMENT_LATENCY_MSEC[];
extern const char LOAD_MODEL_LATENCY_MSEC[];
extern const char OPTIMIZER_LATENCY_MSEC[];
}  // jsonmetrics

/// MIP-specific solution constants
namespace jsonmipsolution {
extern const char OBJECTIVE_VALUES_LIST[];
extern const char RESULT_FEASIBLE[];
extern const char RESULT_INFEASIBLE[];
extern const char RESULT_NOT_SOLVED[];
extern const char RESULT_OPTIMUM[];
extern const char RESULT_UNBOUNDED[];
}  // jsonmipsolution

}  // namespace optilab
