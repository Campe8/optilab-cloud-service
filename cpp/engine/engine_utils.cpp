#include "engine/engine_utils.hpp"

#include <cassert>
#include <memory>
#include <stdexcept>  // for std::runtime_error

#include "engine/engine_constants.hpp"

namespace {

constexpr int kZeroSolutions = 0;

}  // namespace

namespace optilab {
  
namespace engineutils {

JSON::SPtr createCPSatSolutionJson(const std::string& modelName)
{
  JSON::SPtr json = std::make_shared<JSON>();

  auto modelId = json->createValue(modelName);
  auto numSol = json->createValue(kZeroSolutions);
  auto status = json->createValue(std::string(jsonsolution::STATUS_SATISFIED));
  json->add(jsonsolution::MODEL_ID, modelId);
  json->add(jsonsolution::NUM_SOLUTIONS, numSol);
  json->add(jsonsolution::MODEL_STATUS, status);

  return json;
}  // createCPSatSolutionJson

JSON::SPtr createCPUnsatSolutionJson(const std::string& modelName)
{
  JSON::SPtr json = std::make_shared<JSON>();

  auto modelId = json->createValue(modelName);
  auto numSol = json->createValue(kZeroSolutions);
  auto status = json->createValue(std::string(jsonsolution::STATUS_UNSATISFIED));
  json->add(jsonsolution::MODEL_ID, modelId);
  json->add(jsonsolution::NUM_SOLUTIONS, numSol);
  json->add(jsonsolution::MODEL_STATUS, status);

  return json;
}  // createCPUnsatSolutionJson

JSON::SPtr createMetricsJson(MetricsRegister::SPtr metricsReg)
{
  if (!metricsReg)
  {
    throw std::invalid_argument("createMetricsJson - empty pointer to the metrics register");
  }

  JSON::SPtr json = std::make_shared<JSON>();
  for (const auto& metric : *metricsReg)
  {
    json->add(metric.first, json->createValue(metric.second));
  }

  return json;
}  // createMetricsJson

EngineFrameworkType getEngineFrameworkTypeFromString(const std::string& frameworkType)
{
  if (frameworkType == engineconsts::ENGINE_FRAMEWORK_TYPE_CONNECTORS)
  {
    return EngineFrameworkType::CONNECTORS_FRAMEWORK;
  }
  else if (frameworkType == engineconsts::ENGINE_FRAMEWORK_TYPE_OR)
  {
    return EngineFrameworkType::OR_FRAMEWORK;
  }
  else
  {
    throw std::runtime_error("getEngineFrameworkTypeFromString - unrecognized framework " +
                             frameworkType);
  }
}  // getEngineFrameworkTypeFromString

OptimizerPkg getEnginePackageTypeFromString(const std::string& packageType)
{
  if (packageType == engineconsts::ENGINE_PACKAGE_TYPE_OR_TOOLS)
  {
    return OptimizerPkg::OP_OR_TOOLS;
  }
  else if (packageType == engineconsts::ENGINE_PACKAGE_TYPE_GECODE)
  {
    return OptimizerPkg::OP_GECODE;
  }
  else if (packageType == engineconsts::ENGINE_PACKAGE_TYPE_SCIP)
  {
    return OptimizerPkg::OP_SCIP;
  }
  else
  {
    throw std::runtime_error("getEnginePackageTypeFromString - unrecognized package " +
                             packageType);
  }
}  // getEngineFrameworkTypeFromString

EngineClassType getEngineClassTypeFromString(const std::string& classType)
{
  if (classType == engineconsts::ENGINE_CLASS_TYPE_CP)
  {
    return EngineClassType::EC_CP;
  }
  else if (classType == engineconsts::ENGINE_CLASS_TYPE_MIP)
  {
    return EngineClassType::EC_MIP;
  }
  else if (classType == engineconsts::ENGINE_CLASS_TYPE_SCHEDULING)
  {
    return EngineClassType::EC_SCHEDULING;
  }
  else if (classType == engineconsts::ENGINE_CLASS_TYPE_VRP)
  {
    return EngineClassType::EC_VRP;
  }
  else if (classType == engineconsts::ENGINE_CLASS_TYPE_COMBINATOR)
  {
    return EngineClassType::EC_COMBINATOR;
  }
  else
  {
    throw std::runtime_error("getEngineClassTypeFromString - unrecognized class type " +
                             classType);
  }
}  // getEngineClassTypeFromString

}  // namespave engineutils
  
}  // namespace optilab
