//
// Copyright OptiLab 2019-2020. All rights reserved.
//
// Implementation of a generic parallel solver.
//

#pragma once

#include <cstdint>  // for uint64_t
#include <limits>   // for std::numeric_limits
#include <memory>   // for std::shared_ptr

#include "utilities/timer.hpp"
#include "meta_bb/framework.hpp"
#include "meta_bb/instance.hpp"
#include "meta_bb/meta_bb_constants.hpp"
#include "meta_bb/meta_bb_struct.hpp"
#include "meta_bb/node.hpp"
#include "meta_bb/paramset.hpp"
#include "meta_bb/racing_rampup_paramset.hpp"
#include "meta_bb/solution.hpp"
#include "optimizer_network/network_communicator.hpp"
#include "optimizer_network/packet.hpp"
#include "system/system_export_defs.hpp"

// Forward declarations
namespace optilab {
namespace  metabb {
class SolverImpl;
}  // namespace metabb
}  // namespace optilab

namespace optilab {
namespace  metabb {

class SYS_EXPORT_CLASS Solver {
 public:
  using SPtr = std::shared_ptr<Solver>;

 public:
  /// Default constructor.
  /// Creates an object without any framework, parameter set, or any
  /// other internal data-structure or object.
  /// This is mostly used for derived classes and testing
  Solver();

  /// Constructor.
  /// @note if timer is not specified, it builds and starts a new timer
  Solver(Framework::SPtr framework, ParamSet::SPtr paramSet, Instance::SPtr instance,
         timer::Timer::SPtr timer = nullptr);

  virtual ~Solver();

  /// Returns the framework instance hold by this solver
  inline Framework::SPtr getFramework() const noexcept { return pFramework; }

  /// Returns true if this solver has been warm-started, returns false otherwise
  inline bool isWarmStarted() const noexcept { return pWarmStarted; }

  /// Runs this solver
  void run();

  /// Runs this solver on the given node
  void run(Node::SPtr node);

  /// Runs this solver on the given racing ramp-up parameter set
  void run(RacingRampUpParamSet::SPtr racingRampUpParamSet);

  /// Returns the wallclock time in msec spent in solving the current node
  inline uint64_t getElapsedTimeOfNodeSolvingMsec()
  {
    return (pTimer->getWallClockTimeMsec() - pNodeStartTimeMsec);
  }

  /// Returns the best dual bound value obtained at warm start
  inline double getGlobalBestDualBoundValueAtWarmStart() const noexcept
  {
    return pGlobalBestDualBoundValueAtWarmStart;
  }

  /// Returns the LM best dual bound value
  inline double getLMBestDualBoundValue() const noexcept { return pLMBestDualBoundValue; }

  /// Returns the number of nodes that acts as a threshold,
  /// if the number of nodes solved is less than this value,
  /// this solver is a candidate to enter in stop solving
  inline int getNumStopSolvingMode() const noexcept
  {
    return (pParamSet->getDescriptor()).numstopsolvingmode();
  }

  /// Returns the time in msec acting as a threshold.
  /// If the node computing time is less than this value,
  /// that can be a candidate to stop solving
  inline uint64_t getTimeStopSolvingModeMsec() const noexcept
  {
    return static_cast<uint64_t>((pParamSet->getDescriptor()).timestopsolvingmodemsec());
  }

  /// Returns the root node time in msec
  inline uint64_t getRootNodeTimeMsec() const noexcept { return pRootNodeTimeMsec; }

  /// Returns the value used as a gap for stop solving.
  /// In that scenario, this solver is stopped and all generated nodes except
  /// the root are thrown away
  inline double getBoundGapForStopSolving() const noexcept
  {
    return (pParamSet->getDescriptor()).bgapstopsolvingmode();
  }

  /// // Bound gap "g" to identify good nodes:
  /// Returns the bound gap "g" used to identify good nodes.
  /// @note a node is "good" if:
  ///  ((bound_value - best_bound_value) / best_bound_value) < "g"
  inline double getBoundGapForCollectingMode() const noexcept
  {
    return (pParamSet->getDescriptor()).bgapcollectingmode();
  }

  /// Probes listening for any incoming message.
  /// If a message is received, it process it.
  /// @note this is a non-blocking call
  void receiveMessages();

  /// Returns true if the global incumbent value is updated
  /// during the "receiveMessages(...)" routine.
  /// Returns false otherwise
  inline bool isGlobalIncumbentUpdated() const noexcept
  {
    return pGlobalIncumbentValueUpdateFlag;
  }

  /// Sets the flag indicating that the global incumbent value
  /// is NOT updated in "receiveMessages(...)" routine
  inline void globalIncumbentValueIsReflected() noexcept
  {
    pGlobalIncumbentValueUpdateFlag = false;
  }

  /// Returns true if the ramp-up phase is finished,
  /// returns false otherwise
  inline bool isRampUp() const noexcept { return pRampUp; }

  /// Returns true if racing interrupt has been requested,
  /// returns false otherwise
  inline bool isRacingInterruptRequested() const noexcept
  {
    return (pRacingInterruptIsRequested || pRestartingRacing);
  }

  /// Returns true if racing is used during ramp-up phase
  /// (e.g., instead of using normal ramp-up)
  bool isRacingRampUp() const noexcept;

  /// Returns true if this solver is the winner of racing ramp-up.
  /// Returns false otherwise
  inline bool isRacingWinner() const noexcept { return pRacingWinner; }

  /// Returns true if the collecting mode is interrupted (sending nodes to the LM).
  /// Returns false otherwise
  inline bool isCollectingInterrupt() const noexcept { return pCollectingInterrupt; }

  /// Sets the root node time in msec
  void setRootNodeTime();

  /// Sends the solution to the root network node if it improves the given solution
  void sendIfImprovedSolutionWasFound(Solution::SPtr sol);
  void saveIfImprovedSolutionWasFound(Solution::SPtr sol);

  /// Sends the local solution to the root network node
  void sendLocalSolution();
  bool notificationIsNecessary();

  /// Sends this solver state to the root network node
  void sendSolverState(long long numNodesSolved, int numNodesLeft, double bestDualBoundValue,
                       uint64_t timeMsec);

  /// Returns true if the termination mode of this solver is interrupted.
  /// Returns false otherwise
  inline bool isInterrupting() const noexcept
  {
    return pTerminationMode == solver::TerminationMode::TM_INTERRUPTED_TERMINATION;
  }

  /// Returns true if the termination mode of this solver is normal.
    /// Returns false otherwise
  inline bool isTerminationRequested() const noexcept
  {
    return pTerminationMode == solver::TerminationMode::TM_NORMAL_TERMINATION;
  }

  /// Returns true if there is a new root node set.
  /// Returns false otherwise
  inline bool newNodeExists() const noexcept { return (!!pNewNode); }

  /// Returns true if this solver is in collecting mode.
  /// Returns false otherwise
  inline bool isInCollectingMode() const noexcept
  {
    return pCollectingMode || pCollectingManyNodes;
  }

  /// Returns true if this solver is in aggressive collecting mode,
  /// i.e., if the solver has two nodes, one is sent to the LM.
  /// Returns false otherwise
  inline bool isAggressiveCollecting() const noexcept
  {
    return pAggressiveCollecting;
  }

  /// Returns true if many nodes collection has been requested by the LM.
  /// Returns false otherwise
  inline bool isManyNodesCollectionRequested() const noexcept
  {
    return pCollectingManyNodes;
  }

  /// Returns the threshold value in terms of number of nodes
  /// given the number of processed nodes, including the current node being solved.
  /// The threshold value determines the "no node transfer" mode
  int getThresholdValue(int numNodes);

  /// Sends the node to the root network node
  void sendNode(long long n, int depth, double dualBound, double estimateValue,
                DiffSubproblem::SPtr diffSubproblem);

  /// Request another node from the LM
  void sendAnotherNodeRequest(double bestDualBoundValue);

  inline bool getNotificaionProcessed() const noexcept { return pNotificationProcessed; }

  /// Returns the global best incumbent value
  inline double getGlobalBestIncumbentValue() const noexcept
  {
    return pGlobalBestIncumbentValue;
  }

  /// Returns the current node being solved
  inline Node::SPtr getCurrentNode() const noexcept { return pNode; }

  /// Returns the current instance problem being solved
  inline Instance::SPtr getInstance() const noexcept { return pInstance; }

  /// Returns the parameter set used by this solver to solve
  /// the nodes assigned to it
  inline ParamSet::SPtr getParamSet() const noexcept { return pParamSet; }

  /// Returns the rank of the network communicator used by this solver
  virtual int getRank() const noexcept { return pFramework->getComm()->getRank(); }

  inline void countInPrecheckSolvedParaNodes() noexcept { pNumNodesSolvedAtPreCheck++; }

  void waitMessageIfNecessary();

  /// Returns the number of nodes sent while in collecting mode
  inline int getNumNodesSendInCollectingMode() noexcept { return pNumSendInCollectingMode; }

  /// Returns true if this solver is in racing stage.
  /// Returns false otherwise
  bool isRacingStage() const noexcept;

  /// Terminates the racing stage
  inline void terminateRacing()
  {
     assert(pRacingRampUpParamSet);
     pRacingRampUpParamSet.reset();
     pRacingInterruptIsRequested = false;
     pRacingIsInterrupted = false;
  }

  /// Returns the global best incumbent solution
  inline Solution::SPtr getGlobalBestIncumbentSolution() const noexcept
  {
     return pGlobalBestIncumbentSolution;
  }

  inline bool isWaitingForSpecificMessage() const noexcept { return pWaitingSpecificMessage; }

  inline bool isBreaking() const noexcept { return pNumTransferNodeLimit > 0; }

  inline double getTargetBound() const noexcept { return pTargetBound; }

  inline bool isTransferLimitReached() const noexcept
  {
    return (pNumTransferredNodes >= pNumTransferNodeLimit);
  }

  inline void resetBreakingInfo()
  {
     pTargetBound = metaconst::MIN_DOUBLE;
     pNumTransferNodeLimit = -1;
     pNumTransferredNodes = -1;
     pCollectingManyNodes = false;
  }

  inline bool isOnceBreak() const noexcept { return pOnceBreak; }
  inline void setOnceBreak() noexcept
  {
     pNumCollectOnce = -1;
     pCollectingManyNodes = true;
     pOnceBreak = true;
  }

  /// Returns true if aggressive pre-solving is specified.
  /// Returns false otherwise
  inline bool isAggressivePresolvingSpecified() const noexcept
  {
    return getAggresivePresolvingDepth() >= 0;
  }

  int getAggresivePresolvingDepth() const noexcept
  {
    return (pParamSet->getDescriptor()).aggressivepresolvedepth();
  }

  inline int getAggresivePresolvingStopDepth() const noexcept
  {
    return (pParamSet->getDescriptor()).aggressivepresolvestopdepth();
  }

  inline int getSubMIPDepth() const noexcept { return (!!pNode) ? pNode->getDepth() : -1; }

  inline void setSendBackAllNodes() noexcept
  {
    pNumCollectOnce = -1;
    pCollectingManyNodes = true;
  }

  /// Returns true is this solver has been asked to collect many nodes by the LM
  inline bool isCollectingAllNodes() const noexcept
  {
     return pCollectingManyNodes && (pNumCollectOnce < 0);
  }

  inline int getBigDualGapSubtreeHandlingStrategy() const noexcept
  {
    // Strategies:
    // 0 - throw away;
    // 1 - send back to LM.
    // Currently supported: throw away
    return 0;
  }

  inline bool isGivenGapReached() const noexcept { return pGivenGapIsReached; }

  inline bool isIterativeBreakDownApplied() const noexcept { return false; }

  /// Sets integer infeasibility provided the sum of integer infeasibility
  /// and the number of integer infeasibility
  inline void setIntegerInfeasibility (double sum, int count)
  {
     if (pMinIntInfeasibilitySum > sum) pMinIntInfeasibilitySum = sum;
     if (pMaxIntInfeasibilitySum < sum) pMaxIntInfeasibilitySum = sum;
     if (pMinNumIntInfeasibility > count) pMinNumIntInfeasibility = count;
     if (pMaxNumIntInfeasibility < count) pMaxNumIntInfeasibility = count;
  }

  /// Sets the number of root node simplex iterations
  inline void setRootNodeSimplexIter(int iter) noexcept { pNumSimplexIterRoot = iter; }

  /// Returns the merging status of the current node being solved
  inline Node::NodeMergingStatus getCurrentSolvingNodeMergingStatus() const noexcept
  {
    return (!!pNode) ?
            pNode->getMergingStatus() :
            Node::NodeMergingStatus::NMS_NO_MERGING_NODE;
  }

  /// Returns the initial dual bound value of the node currently being solved
  inline double getCurrentSolvingNodeInitialDualBound() const noexcept
  {
    return (!!pNode) ? pNode->getInitialDualBoundValue() : std::numeric_limits<double>::max();
  }

  /// Returns the instance of the timer used by this solver
  inline timer::Timer::SPtr getTimer() const noexcept { return pTimer; }

  /// Returns the average dual bound gain
  inline double getAverageDualBoundGain() const noexcept { return pAverageDualBoundGain; }
  inline void setNotEnoughGain() noexcept { pEnoughGainObtained = false; }
  inline bool isEnoughGainObtained() const noexcept { return pEnoughGainObtained; }
  inline bool isDualBoundGainTestNeeded() const noexcept { return pTestDualBoundGain; }

  /// Returns true if this solver terminated normally.
  /// Returns false otherwise
  virtual bool wasTerminatedNormally() = 0;

  /// Tries the given solution as new solution
  virtual void tryNewSolution(Solution::SPtr sol) = 0;

  /// Sets light weight root node process,
  /// i.e., sets fast root node computation
  virtual void setLightWeightRootNodeProcess() = 0;

  /// Sets the original root node process
  virtual void setOriginalRootNodeProcess() = 0;

  /// Returns the number of simplex iterations
  virtual long long getSimplexIter() = 0;

  /// Returns the number of restarts
  virtual int getNumRestarts() { return 0; }

  virtual bool canGenerateSpecialCutOffValue() { return false; }

  double getCutOffValue() { return pGlobalBestCutOffValue; }

  /// Sets the termination mode for this solver
  inline void setTerminationMode(solver::TerminationMode termMode) noexcept
  {
    pTerminationMode = termMode;
  }

  /// Returns the termination mode
  inline solver::TerminationMode getTerminationMode() const noexcept { return pTerminationMode; }

  /// Updates the number of transferred local cuts
  inline void updateNumTransferredLocalCuts(int numCuts)
  {
    pNumTransferredLocalCuts += numCuts;
    if (pMinTransferredLocalCuts > numCuts)
    {
      pMinTransferredLocalCuts = numCuts;
    }

    if (pMaxTransferredLocalCuts < numCuts)
    {
      pMaxTransferredLocalCuts = numCuts;
    }
  }

  /// Returns true if another node is requested by this solver,
  /// returns false otherwise
  inline bool isAnotherNodeIsRequested() const noexcept { return pAnotherNodeIsRequested; }

 protected:
  friend class SolverImpl;

 protected:
  /// Framework this solver runs on
  Framework::SPtr pFramework;

  /// Pointer to the network communicator used by the solver
  /// to send/receive messages across the network
  optnet::NetworkCommunicator::SPtr pCommunicator;

  /// Generator notification identifier
  int pNotificationIdGenerator{0};

  /// Parameter set
  ParamSet::SPtr pParamSet;

  /// Racing ramp-up parameter set
  RacingRampUpParamSet::SPtr pRacingRampUpParamSet;

  /// Racing ramp-up parameter set of the winner solver
  RacingRampUpParamSet::SPtr pWinnerRacingRampUpParamSet;

  /// Pointer to the timer
  timer::Timer::SPtr pTimer;

  /// Pointer to this solver's implementation
  std::shared_ptr<SolverImpl> pSolverImpl;

  /// Global best dual bound value which is set when system warm starts
  double pGlobalBestDualBoundValueAtWarmStart{std::numeric_limits<double>::lowest()};

  /// Global best incumbent value
  double pGlobalBestIncumbentValue{std::numeric_limits<double>::max()};

  /// Incumbent value which is pending to update
  double pPendingIncumbentValue{std::numeric_limits<double>::max()};

  /// Global best cutoff value
  double pGlobalBestCutOffValue{std::numeric_limits<double>::max()};

  /// Load Manager best dual bound value
  double pLMBestDualBoundValue{std::numeric_limits<double>::lowest()};

  /// Global best solution.
  /// @note this is not always feasible for the current sub-MIP
  Solution::SPtr pGlobalBestIncumbentSolution;

  /// Incumbent solution generated locally by this solver
  Solution::SPtr pLocalIncumbentSolution;

  /// Solution which is pending to update
  Solution::SPtr pPendingSolution;

  /// Root problem instance,
  /// i.e., the original problem to solve
  Instance::SPtr pInstance;

  /// Current solving node, being solved by this solver
  Node::SPtr pNode;

  /// Current solving node, being solved by this solver
  Node::SPtr pNewNode;

  /// Flag indicating whether or not this solver is in collecting mode
  bool pCollectingMode{false};

  /// Flag indicating aggressive collecting.
  /// If true and if this solver has two nodes, the solver sends one to the LM
  bool pAggressiveCollecting{false};

  /// Number of nodes to send to the LM when in collecting mode
  int pNumSendInCollectingMode{0};

  /// Number of nodes that need to be collected once
  int pNumCollectOnce{0};

  /// Flag indicating that many nodes collecting is requested by the LM
  bool pCollectingManyNodes{false};

  /// If true, the solver is interrupted and all nodes are collected by the LM
  bool pCollectingInterrupt{false};

  /// Termination mode for this solver
  solver::TerminationMode pTerminationMode{solver::TerminationMode::TM_NO_TERMINATION};

  /// Flag indicating whether or not the solver is warm-started
  bool pWarmStarted{false};

  /// If true, indicates that the ramp-up phase is finished
  bool pRampUp{false};

  /// If true, it indicates that a racing interrupt is requested
  bool pRacingInterruptIsRequested{false};

  /// If true, it indicates that racing phase is interrupted
  bool pRacingIsInterrupted{false};

  /// If true, it indicates that this solver is the racing ramp-up winner
  bool pRacingWinner{false};

  /// Flag indicating whether or not another node is requested
  bool pAnotherNodeIsRequested{false};

  /// If true, it indicates that this solver is waiting for a
  /// specified message
  bool pWaitingSpecificMessage{false};

  /// If true, it indicates that fast root node computation is required
  bool pLightWeightRootNodeComputation{false};

  /// If true, it indicates that this solver is restating racing
  bool pRestartingRacing{false};

  /// Flag indicating whether or not the sub-MIP is broken down once
  bool pOnceBreak{false};

  /// Root node process time of the current Node in msec.
  uint64_t pRootNodeTimeMsec{0};

  /// Accumulated root node process time solved
  /// by this solver so far in msec
  uint64_t pTotalRootNodeTimeMsec{0};

  /// Minimum time consumed by root node process in msec
  uint64_t pMinRootNodeTimeMsec{0};

  /// Maximum time consumed by root node process in msec
  uint64_t pMaxRootNodeTimeMsec{0};

  /// Previous node notification time in msec
  uint64_t pPreviousNotificationTimeMsec{0};

  /// Start time of current Node in Msec
  uint64_t pNodeStartTimeMsec{0};

  /// Previous stop solving time of this solver in msec
  uint64_t pPreviousStopTimeMsec{0};

  /// Idle time before start solving the first Node in msec
  uint64_t pIdleTimeToFirstNodeMsec{0};

  /// Idle time between Node(s) processing in msec
  uint64_t pIdleTimeBetweenNodesMsec{0};

  /// Idle time after the last Node was solved in msec
  uint64_t pIdleTimeAfterLastNodeMsec{0};

  /// Idle time spent in waiting a message within collecting mode in msec
  uint64_t pIdleTimeToWaitNotificationIdMsec{0};

  /// Idle time spent in waiting an acknowledgment of completion in msec
  uint64_t pIdleTimeToWaitAckCompletionMsec{0};

  /// Toket time for deterministic runs
  uint64_t pIdleTimeToWaitTokenMsec{0};

  /// Number of nodes solved,
  /// i.e., the number of subtree nodes rooted from the root node solved by
  /// this solver
  int pNumSolved{0};

  /// Number of nodes sent by this solver
  int pNumSent{0};

  /// Number of improvements of the incumbent value
  int pNumImprovedIncumbent{0};

  /// Number of nodes solved without pre-processes
  int pNumSolvedWithNoPreprocesses{0};

  /// Accumulated number of nodes solved in by this solver
  int pTotalNumNodesSolved{0};

  /// Minimum number of subtree nodes rooted from the node solved by this solver
  int pMinNumSolved{std::numeric_limits<int>::max()};

  /// Maximum number of subtree nodes rooted from the node solved by this solver
  int pMaxNumSolved{std::numeric_limits<int>::lowest()};

  /// Number of local cuts transferred from this solver.
  /// @note cuts are constraints added to a model to
  /// restrict (cut away) non integer solutions
  /// that would otherwise be solutions of the continuous relaxation.
  /// The addition of cuts usually reduces
  /// the number of branches needed to solve a MIP
  int pNumTransferredLocalCutsFromSolver{0};

  /// Minimum number of local cuts transferred from this solver
  int pMinTransferredLocalCutsFromSolver{std::numeric_limits<int>::max()};

  /// Maximum number of local cuts transferred from this solver
  int pMaxTransferredLocalCutsFromSolver{std::numeric_limits<int>::lowest()};

  /// Number of total restarts
  int pNumTotalRestarts{0};

  /// Minimum number of restarts
  int pMinRestarts{std::numeric_limits<int>::max()};

  /// Maximum number of restarts
  int pMaxRestarts{std::numeric_limits<int>::lowest()};

  /// Accumulated number of nodes sent from this solver
  int pTotalNumNodesSent{0};

  /// Accumulated number of improvements of incumbent value in this solver
  int pTotalNumImprovedIncumbent{0};

  /// Number of nodes received by this solcver
  int pNumNodesReceived{0};

  /// Number of nodes (received and) solved in this solver
  int pNumNodesSolved{0};

  /// Number of nodes solved at root node
  int pNumNodesSolvedAtRoot{0};

  /// Number of nodes solved when pre-checking for root node solvability
  int pNumNodesSolvedAtPreCheck{0};

  /// Number of simplex iteration at root node
  int pNumSimplexIterRoot{0};

  /// Number of local cuts (including conflict cuts) transferred from a node
  int pNumTransferredLocalCuts{0};

  /// Minimum number of local cuts (including conflict cuts) transferred from a node
  int pMinTransferredLocalCuts{std::numeric_limits<int>::max()};

  /// Maximum number of local cuts (including conflict cuts) transferred from a node
  int pMaxTransferredLocalCuts{std::numeric_limits<int>::lowest()};

  /// Number of tightened variable bounds during racing
  int pNumVarTightened{0};

  /// Number of integer variable bounds tightened during racing
  int pNumIntVarTightened{0};

  /// Minimum sum of integer infeasibility
  double pMinIntInfeasibilitySum{std::numeric_limits<double>::max()};

  /// Maximum sum of integer infeasibility
  double pMaxIntInfeasibilitySum{0.0};

  /// Minimum number of integer infeasibility
  int pMinNumIntInfeasibility{std::numeric_limits<int>::max()};

  /// Maximum number of integer infeasibility
  int pMaxNumIntInfeasibility{0};

  /// If true, it indicates that update pending solution process is proceeding
  bool pUpdatePendingSolutionIsProceeding{false};

  /// If true, it indicates that global incumbent value
  /// is updated in "receiveMessages(...)" routine
  bool pGlobalIncumbentValueUpdateFlag{false};

  /// If true, it indicates that the notification is issued
  /// but not receive the corresponding LM
  bool pNotificationProcessed{false};

  /// Absolute values smaller than this variable are considered zero
  double pEps{0.0};

  /// Target bound value for breaking
  double pTargetBound{std::numeric_limits<double>::lowest()};

  /// Limit number of transferring nodes for breaking
  int pNumTransferNodeLimit{-1};

  /// keep track number of transferred nodes for breaking
  int pNumTransferredNodes{-1};

  /// Dual bound value for a subproblem
  double pSolverDualBound{std::numeric_limits<double>::lowest()};

  /// Average dual bound gain
  double pAverageDualBoundGain{0.0};

  /// Indicates whether or not the root node process improved dual bound enough
  bool pEnoughGainObtained{true};

  /// Indicates whether or not the subproblem is already freed
  bool pSubproblemFreed{false};

  /// Indicates whether or not the given gap is reached
  bool pGivenGapIsReached{false};

  /// Indicates whether or not the dual bound gain needs to be tested
  bool pTestDualBoundGain{false};

  /// Indicates whether or not there is no wait mode sending applied
  bool pNoWaitModeSend{false};

  /// Indicates whether or not this solver should stay alive after receiving
  /// an interrupt request
  bool pStayAliveAfterInterrupt{false};

  bool receiveNewNodeAndReactivate();

  /// Waits for the specified tag to be received
  void waitSpecifiedTagMessage(net::MetaBBPacketTag::PacketTagType tagType);

  /// Waits for a notification Id Message
  inline void waitNotificationIdMessage()
  {
    waitSpecifiedTagMessage(net::MetaBBPacketTag::PacketTagType::PPT_NOTIFICATION_ID);
  }

  void waitAckCompletion();
  void restartRacing();

  void sendCompletionOfCalculation(uint64_t stopTimeMsec);
  bool updateGlobalBestIncumbentSolution(Solution::SPtr sol);
  bool updateGlobalBestIncumbentValue(double newValue);
  bool updateGlobalBestCutOffValue(double newValue);
  void updatePendingSolution();

  /*********************************************************
   * Pure virtual methods and virtual methods              *
   * to be implemented by the underlying back-end solver   *
   *********************************************************/

  /// Sets the racing parameters indicating whether or not those are
  /// the parameters of the winning solver
  virtual void setRacingParams(RacingRampUpParamSet::SPtr racingParms, bool winnerParam) = 0;

  /// Sets the winning racing parameters
  virtual void setWinnerRacingParams(RacingRampUpParamSet::SPtr racingParams) = 0;

  /// Creates a subproblem
  virtual void createSubproblem() = 0;

  /// Free subproblem
  virtual void freeSubproblem() = 0;

  // Solving method
  virtual void solve() = 0;

  /// Returns the number of nodes solved
  virtual long long getNumNodesSolved() = 0;

  /// Returns the number of nodes left in the solver
  virtual int getNumNodesLeft() = 0;

  /// Returns the dual bound value
  virtual double getDualBoundValue() = 0;

  /// Perform re-initialization of the model instance
  virtual void reinitializeInstance() = 0;

  /// Resets the original node selection strategy
  virtual void setOriginalNodeSelectionStrategy() = 0;

  virtual int lowerBoundTightened(int source, optnet::Packet::UPtr packet) { return 0; }
  virtual int upperBoundTightened(int source, optnet::Packet::UPtr packet) { return 0; }

  /// Returns the number of tightened variables during racing
  virtual int getNumVarTightened() { return 0; }

  /// Returns the number of tightened integer variables during racing
  virtual int getNumIntVarTightened() { return 0; }

  /// Change the search strategy
  virtual void changeSearchStrategy(solver::SearchStrategy searchStrategy) {}
};

}  // namespace metabb
}  // namespace optilab
