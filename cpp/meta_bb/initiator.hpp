//
// Copyright OptiLab 2019-2020. All rights reserved.
//
// Base class for the Initiator.
// The initiator acts as a cache that holds the original problem
// and the incumbent solution.
//

#pragma once

#include <limits>     // for std::numeric_limits
#include <memory>     // for std::shared_ptr
#include <string>
#include <stdexcept>  // for std::invalid_argument
#include <vector>

#include "meta_bb/diff_subproblem.hpp"
#include "meta_bb/framework.hpp"
#include "meta_bb/initial_stat.hpp"
#include "meta_bb/instance.hpp"
#include "meta_bb/meta_bb_constants.hpp"
#include "meta_bb/paramset.hpp"
#include "meta_bb/racing_rampup_paramset.hpp"
#include "meta_bb/solution.hpp"
#include "system/system_export_defs.hpp"
#include "utilities/timer.hpp"

namespace optilab {
namespace  metabb {

class SYS_EXPORT_CLASS Initiator {
 public:
  using SPtr = std::shared_ptr<Initiator>;

  /// Warm start is a list of pairs:
  /// <index of the variable, value of the variable>
  using WarmStartHint = std::vector<std::pair<int, double>>;

 public:
  /// Constructor:
  /// - framework: the framework this initiator has to run on
  /// - timer: the pointer to the (global) timer instance used by this initiator
  /// @note if the pointer to the timer is empty, it creates a new timer and starts it.
  /// @note throw std::invalid_argument if the pointer to the framework is empty
  Initiator(Framework::SPtr framework, timer::Timer::SPtr timer=nullptr)
 : pFramework(framework),
   pTimer((!!timer) ? timer : std::make_shared<timer::Timer>()),
   pSolvedAtInit(false),
   pSolvedAtReInit(false)
 {
    if (!pFramework)
    {
      throw std::invalid_argument("Initiator: empty pointer to the framework");
    }
 }

  virtual ~Initiator() = default;

  /// Returns the warm-start hint list
  inline const WarmStartHint& getRawWarmStartData() const noexcept { return pWarmStart; }

  /// Returns true if it is warm started, false otherwise
  inline bool isWarmStarted() const noexcept { return !pWarmStart.empty(); }

  /// Returns true if the instance is already solved at initialization time,
  /// returns false otherwise
  inline bool isSolveAtInit() const noexcept { return pSolvedAtInit; }

  /// Returns true if the instance is already solved at re-initialization time,
  /// returns false otherwise
  inline bool isSolveAtReInit() const noexcept { return pSolvedAtReInit; }

  /// Returns the BBFramework hold by this instance of Initiator
  Framework::SPtr getFramework() const noexcept { return pFramework; }

  /// Returns the number of tightened variable lower bounds
  inline int getNumTightenedVarLB() const noexcept
  {
    return static_cast<int>(pThightenedVarLBList.size());
  }

  /// Returns the number of tightened variable upper bounds
  inline int getNumTightenedVarUB() const noexcept
  {
    return static_cast<int>(pThightenedVarUBList.size());
  }

  /// Sets the specified value as tightened variable lower bound on the specified variable.
  /// @note throws std::out_of_range exception on invalid index
  inline void setTightenedVarLB(int varIdx, double val)
  {
    pThightenedVarLBList.at(varIdx) = val;
  }

  /// Sets the specified value as tightened variable upper bound on the specified variable.
  /// @note throws std::out_of_range exception on invalid index
  inline void setTightenedVarUB(int varIdx, double val)
  {
    pThightenedVarUBList.at(varIdx) = val;
  }

  /// Returns the lower bound for the specified variable.
  /// @note throws std::out_of_range exception on invalid index
  inline double getTightenedVarLB(int varIdx) const
  {
    return pThightenedVarLBList.empty() ?
            std::numeric_limits<double>::max() :
            pThightenedVarLBList.at(varIdx);
  }

  /// Returns the upper bound for the specified variable.
  /// @note throws std::out_of_range exception on invalid index
  inline double getTightenedVarUB(int varIdx) const
  {
    return pThightenedVarLBList.empty() ?
            std::numeric_limits<double>::lowest() :
            pThightenedVarUBList.at(varIdx);
  }

  /// Returns true if the variable bounds are tightened,
  /// returns false otherwise
  inline bool areTightenedVarBounds() const noexcept
  {
    return getNumTightenedVarLB() > 0;
  }

  /// Initialize the initiator given a paramset
  virtual int init(ParamSet::SPtr paramset) = 0;

  /// Re-initialize given the number of restart racing previously done
  virtual int reInit(int numRestartedRacing) = 0;

  /// Returns the optimization instance build by this initiator
  virtual Instance::SPtr getInstance() = 0;

  /// Builds and return the DiffSubproblem object for the root node
  virtual DiffSubproblem::SPtr buildRootNodeDiffSubproblem() = 0;

  /// Sends solver initialization message over the network
  virtual void sendSolverInitializationMessage() = 0;

  virtual void generateRacingRampUpParameterSets(
      std::vector<RacingRampUpParamSet::SPtr>& racingRampUpParamSetList) = 0;

  /// Converts (and returns) an internal value to an external value
  virtual double convertToExternalValue(double internalValue) = 0;

  /// Returns the best incumbent optimization solution
  virtual Solution::SPtr getGlobalBestIncumbentSolution() = 0;

  /// Tries to set the incumbent solution
  virtual bool tryToSetIncumbentSolution(Solution::SPtr sol, bool checksol) = 0;

  /// Returns the absolute value
  virtual double getAbsGap(double dualBoundValue) = 0;

  /// Returns the gap
  virtual double getGap(double dualBoundValue) = 0;

  /// Returns the absolute value gap
  virtual double getAbsGapValue() = 0;

  /// Returns the gap value
  virtual double getGapValue() = 0;

  /// Returns the epsilon value to be used in the framework.
  /// If a value is less than this epsilon, it will be replaced with zero
  virtual double getEpsilon() const noexcept = 0;

  /// Sets the final status of the solver
  virtual void setFinalSolverStatus(solver::FinalSolverState status) noexcept = 0;

  /// Sets the total number of solved nodes
  virtual void setNumSolvedNodes(long long numNodes) noexcept = 0;

  /// Sets the (final) dual bound
  virtual void setDualBound(double bound) noexcept = 0;

  /// Returns true if a feasible solution exists,
  /// returns false otherwise
  virtual bool isFeasibleSolution() = 0;

  //// Sets initial stat on initiator
  virtual void accumulateInitialStat(InitialStat::SPtr initialStat) = 0;

  /// Writes the solution
  virtual void writeSolution(const std::string& message) = 0;

  /// Returns true if  the objective value is known to be integral in every feasible solution,
  /// returns false otherwise
  virtual bool isObjIntegral() { return false; }

  /// Returns true if the initiator can generate a special cut-off value.
  /// @note default returns false
  virtual bool canGenerateSpecialCutOffValue() { return false; }

  /// Sets initial statistics on DiffSubproblem
  virtual void setInitialStatOnDiffSubproblem(int minDepth, int maxDepth,
                                              DiffSubproblem::SPtr diffSubproblem) = 0;
 protected:
  /// Pointer to the BB Framework and network communicator
  Framework::SPtr pFramework;

  /// Pointer to the timer instance
  timer::Timer::SPtr pTimer;

  /// Warm-start list
  WarmStartHint pWarmStart;

  /// Flag indicating whether or not the instance was solved
  /// at initialization time
  bool pSolvedAtInit;

  /// Flag indicating whether or not the instance was solved at re-initialization time
  bool pSolvedAtReInit;

  /// List of tightened lower bounds of the variables in the instance
  std::vector<double> pThightenedVarLBList;

  /// List of tightened upper bounds of the variables in the instance
  std::vector<double> pThightenedVarUBList;
};

}  // namespace metabb
}  // namespace optilab
