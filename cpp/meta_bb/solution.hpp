//
// Copyright OptiLab 2019. All rights reserved.
//
// Base class encapsulating the solution of an optimization problem
// solved by the distributed branch and bound framework.
//

#pragma once

#include <memory>  // for std::shared_ptr
#include <stdexcept>  // for std::runtime_error

#include "meta_bb/framework.hpp"
#include "optimizer_network/network_packet.hpp"
#include "optimizer_network/packet.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace  metabb {

class SYS_EXPORT_CLASS Solution {
 public:
  using SPtr = std::shared_ptr<Solution>;

 public:
  Solution() = default;

  virtual ~Solution() = default;

  /// Returns the value of the objective function
  virtual double getObjectiveFuntionValue() = 0;

  /// Returns a clone of this object
  virtual Solution::SPtr clone() = 0;

  /// Broadcast solution data to all other network nodes
  virtual int broadcast(const Framework::SPtr& framework, int root) noexcept = 0;

  /// Send solution to the given destination
  virtual int send(const Framework::SPtr& framework, int destination) noexcept = 0;

  /// Uploads the content of the given network packet into this solution.
  /// If the given packet is not a node network packet, throws std::runtime_error
  /// Returns zero on success, non-zero otherwise
  virtual int upload(optnet::Packet::UPtr packet) noexcept = 0;

  /// Returns cut-off value
  virtual double getCutOffValue()
  {
    throw std::runtime_error("Solution - getCutOffValue: "
            "this solver cannot obtain a cutoff value");
  }
};

}  // namespace metabb
}  // namespace optilab
