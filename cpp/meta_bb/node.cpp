#include "meta_bb/node.hpp"

#include <algorithm>  // for std::min
#include <cassert>
#include <string>

#include <spdlog/spdlog.h>

namespace optilab {
namespace  metabb {

Node::Node(const NodeId& nodeId,
           const NodeId& generatorNodeId,
           int depth,
           double dualBoundValue,
           double originalDualBoundValue,
           double estimatedValue,
           DiffSubproblem::SPtr diffSubproblem)
: pDepth(depth),
  pDualBoundValue(dualBoundValue),
  pInitialDualBoundValue(originalDualBoundValue),
  pEstimatedValue(estimatedValue),
  pNodeId(nodeId),
  pGeneratorNodeId(generatorNodeId),
  pParent(nullptr),
  pDiffSubproblem(diffSubproblem)
{
  // Set diff. subproblem if any
  if (pDiffSubproblem)
  {
    pDiffSubproblemInfo = true;
  }
}

Node::~Node()
{
  try {
    cleanupNode();
  }
  catch(...)
  {
    spdlog::warn("Node - destructor: undefined error");
  }
}

void Node::cleanupNode()
{
  assert((pMergingStatus != NodeMergingStatus::NMS_NO_MERGING_NODE) ||
         (pMergingStatus == NodeMergingStatus::NMS_NO_MERGING_NODE && !pMergeNodeInfo));
   if (pParent)
   {
     auto localParentPtr = std::dynamic_pointer_cast<NodeGenealogicalLocalRel>(pParent);
     if (!localParentPtr)
     {
       spdlog::error("Node - cleanupNode: invalid cast to NodeGenealogicalLocalRel, return");
       return;
     }
     assert(localParentPtr->hasValue());

     if (!pChildren.empty())
     {
       // Iterate over all this node's children
       for (const auto& it : pChildren)
       {
         if (it.second->getType() == NodeGenealogicalRel::NodeGeneaLogicalRelType::NGLRT_LOCAL)
         {
           auto localChildPtr = std::dynamic_pointer_cast<NodeGenealogicalLocalRel>(it.second);
           if (!localChildPtr)
           {
             spdlog::error("Node - cleanupNode: invalid cast to NodeGenealogicalLocalRel, return");
             return;
           }

           // For each child, set the parent as this node's parent
           localChildPtr->getValue()->setParent(
               std::make_shared<NodeGenealogicalLocalRel>(localParentPtr->getNodeId(),
                                                          localParentPtr->getValue()));

           // Add this node's child as new children of the this node's parent
           localParentPtr->getValue()->addChild(
               std::make_shared<NodeGenealogicalLocalRel>(localChildPtr->getNodeId(),
                                                          localChildPtr->getValue()));
         }
       }  // for
     }

     // Remove this node from the parent's list of children
     localParentPtr->getValue()->removeChild(pNodeId);
   }
   else if (!pChildren.empty())
   {
     // Iterate over all this node's children
     for (const auto& it : pChildren)
     {
       if (it.second->getType() == NodeGenealogicalRel::NodeGeneaLogicalRelType::NGLRT_LOCAL)
       {
         auto localChildPtr = std::dynamic_pointer_cast<NodeGenealogicalLocalRel>(it.second);
         if (!localChildPtr)
         {
           spdlog::error("Node - cleanupNode: invalid cast to NodeGenealogicalLocalRel, return");
           return;
         }

         // For each child, set the nullptr as parent since this node being deleted was the
         // only ancestor for that child
         localChildPtr->getValue()->setParent(nullptr);
       }
     }  // for
     pChildren.clear();
   }

   // Address the case that this node is being merged
   if (pMergeNodeInfo != nullptr)
   {
     // If this node doesn't have merged nodes but it has being merged to some other node,
     // decrement the some other node's number of merged nodes (since this node is being deleted)
     if ((pMergeNodeInfo->numMergedNodes == 0) && (pMergeNodeInfo->mergedTo != nullptr))
     {
       pMergeNodeInfo->mergedTo->numMergedNodes--;
     }

     // Cannot cleanup a node which status (merge info) is merging.
     // If so, return (this is mostly done for testing purposes.
     // Here it should check that is not merging
     if (pMergeNodeInfo->status == mergenodes::MergeNodeInfo::Status::STATUS_MERGING)
     {
       spdlog::error("Node - cleanupNode: cleanup node that has a merge info which is "
               "in MERGING status");
     }

     if (pMergeNodeInfo->status ==
         mergenodes::MergeNodeInfo::Status::STATUS_MERGED_REPRESENTATIVE)
     {
       // Iterate over the list of fixed variables and decrease the counter of merged nodes
       // since this node with fixed variable is being deleted
       assert(pMergeNodeInfo->keyIndex >= 0 &&
              pMergeNodeInfo->keyIndex < pMergeNodeInfo->fixedVariableList.size());
       auto traverseVar = pMergeNodeInfo->fixedVariableList[pMergeNodeInfo->keyIndex]->next;
       for (; traverseVar; traverseVar = traverseVar->next)
       {
         // The traverse variable doesn't have merged nodes but it has being merged to
         // another node
         if (traverseVar->mnode->numMergedNodes == 0 && traverseVar->mnode->mergedTo != nullptr)
         {
           // Update the counter for all other variables sharing the same merge node info struct
           traverseVar->mnode->mergedTo->numMergedNodes--;
           // traverseVar->mnode->mergedTo = nullptr;
           assert(traverseVar->mnode->node);

           if (traverseVar->mnode->node->getDualBoundValue() <
                   traverseVar->mnode->node->getDualBoundValue())
           {
             traverseVar->mnode->node->setMergingStatus(NodeMergingStatus::NMS_CHECKING);
             traverseVar->mnode->status = mergenodes::MergeNodeInfo::Status::STATUS_MERGING;
           }
           else
           {
             // Merging representative was deleted: this node should be deleted
             traverseVar->mnode->node->setMergingStatus(
                     NodeMergingStatus::NMS_MERGING_REPRESENTATIVE_WAS_DELETED);
             traverseVar->mnode->status = mergenodes::MergeNodeInfo::Status::STATUS_DELETED;
           }
           traverseVar->mnode->numMergedNodes = -1;
           traverseVar->mnode->numSameValueVariables = -1;
           traverseVar->mnode->keyIndex = -1;
         }
       }
     }

     for (auto& fixedVar : (pMergeNodeInfo->fixedVariableList))
     {
       for (auto traverseVarLoop = fixedVar->prev; traverseVarLoop;
           traverseVarLoop = traverseVarLoop->prev)
       {
         // Decrease the number of fixed variables with same value since this variable
         // is being deleteted
         traverseVarLoop->numSameValue--;
       }

       if (fixedVar->prev)
       {
         // Remove this variable from the list, i.e., connect previous to next
         fixedVar->prev->next = fixedVar->next;
         if (fixedVar->next)
         {
           // Connect next variable back pointer to this variable back pointer
           fixedVar->next->prev = fixedVar->prev;
         }
       }
       else if (fixedVar->next)
       {
         // No previous variable, set next previous to nullptr (this was the head of the list)
         fixedVar->next->prev = nullptr;
       }
     }
     (pMergeNodeInfo->fixedVariableList).clear();

     if (pMergeNodeInfo->origDiffSubproblem &&
         (pMergeNodeInfo->origDiffSubproblem).get() != pDiffSubproblem.get())
     {
       pMergeNodeInfo->origDiffSubproblem.reset();
     }
   }
   pDiffSubproblem.reset();
}  // cleanupNode

void Node::setParent(NodeGenealogicalRel::SPtr parent)
{
  if (!parent)
  {
    // Override the parent with nullptr and return
    pParent = parent;
    return;
  }

  // Check that the parent has a value.
  // @note only local relationships are currently supported
  auto localParentPtr = std::dynamic_pointer_cast<NodeGenealogicalLocalRel>(parent);
  if (!localParentPtr)
  {
    const std::string errMsg("Node - setParent: "
            "invalid cast to NodeGenealogicalLocalRel (currently only support).");
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  if (!(localParentPtr->hasValue()))
  {
    const std::string errMsg("Node - setParent: "
            "parent doesn't have a value set (i.e., a pointer to a node).");
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  // Otherwise set the given parent as current parent
  pParent = parent;
}

void Node::removeChild(const NodeId& nodeId)
{
  RelationshipMap::iterator iter = pChildren.find(nodeId);
  if (iter != pChildren.end())
  {
    pChildren.erase(iter);
  }
  else
  {
    const std::string errMsg = "Node - removeChild: child with (seq. number) id " +
        std::to_string(nodeId.getSeqNum()) + " not found";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }
}  // removeChild

double Node::getMinimumDualBoundInDesendants(double val)
{
  // Return the input value if this node doesn't have any children
  if (pChildren.empty()) return val;

  for (const auto& it : pChildren)
  {
    if (it.second->getType() == NodeGenealogicalRel::NodeGeneaLogicalRelType::NGLRT_LOCAL)
    {
      auto localChildPtr = std::dynamic_pointer_cast<NodeGenealogicalLocalRel>(it.second);
      if (!localChildPtr)
      {
        throw std::runtime_error("Node - getMinimumDualBoundInDesendants: invalid cast to local "
            "genealogical relationship");
      }

      // Recursively update the lower bound value
      val = std::min(val, localChildPtr->getValue()->getDualBoundValue());
      val = std::min(val, localChildPtr->getValue()->getMinimumDualBoundInDesendants(val));
    }
  }

  return val;
}  // getMinimumDualBoundInDesendants

}  // namespace metabb
}  // namespace optilab
