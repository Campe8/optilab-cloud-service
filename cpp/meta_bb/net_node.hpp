//
// Copyright OptiLab 2020. All rights reserved.
//
// Implementation of the metabb Node using network communicators
// based on ZeroMQ to exchange messages over the network.
//
#pragma once

#include <memory>  // for std::shared_ptr

#include "meta_bb/node.hpp"

#include "meta_bb/diff_subproblem.hpp"
#include "meta_bb/framework.hpp"
#include "meta_bb/tree_id.hpp"
#include "optilab_protobuf/meta_bb_packet.pb.h"
#include "optimizer_network/network_packet.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace  metabb {

class SYS_EXPORT_CLASS NetNode : public Node {
 public:
  using SPtr = std::shared_ptr<NetNode>;

 public:
  /// Default constructor
  NetNode() = default;

  /// Constructor:
  /// - nodeId: identifier of this node
  /// - generatorNodeId: identifier of the node that generated this node
  /// - depth: depth of this node in the search tree
  /// - dualBoundValue: dual bound value of this node
  /// - originalDualBoundValue: original dual bound value
  /// - estimatedValue: estimated value for this node
  /// - diffSubproblem: difference between the original (full) problem to be solved
  ///   and the (sub) problem that has to be solved by this node.
  ///   In other words, the "diffSubproblem" is the actual optimization problem that
  ///   this node is in charge to solve
  NetNode(const NodeId& nodeId,
          const NodeId& generatorNodeId,
          int depth,
          double dualBoundValue,
          double originalDualBoundValue,
          double estimatedValue,
          const DiffSubproblem::SPtr& diffSubproblem);

  /// Destructor:
  /// checks the node parent and children and re-connect parent to children
  ~NetNode() override;

  /// Returns a clone of this optimization node
  Node::SPtr clone(const Framework::SPtr& framework) override;

  /// Broadcasts this node from the given root to all other nodes in the network.
  /// Returns zero on success, non-zero otherwise
  int broadcast(const Framework::SPtr& framework, int root) noexcept override;

  /// Sends this node to given destination.
  /// Returns zero on success, non-zero otherwise
  int send(const Framework::SPtr& framework, int destination) noexcept override;

  /// Uploads the content of the given network packet into this node.
  /// Returns zero on success, non-zero otherwise
  int upload(const Framework::SPtr& framework, optnet::Packet::UPtr packet) noexcept override;

 protected:
  /// Builds and returns a network packet encoding this node
  optnet::Packet::UPtr buildNetworkPacket();

  /// Upload the node proto packet serialized as string into this node
  int uploadPacket(const std::string& bytes);

  /// Upload the node proto packet into this node
  //int uploadPacket(optnet::NetworkPacket<NodeProtoPacket>::UPtr& nodePacket);
};

}  // namespace metabb

/// Specialized template for network node packet comparison
template<> bool optnet::NetworkPacket<metabb::NodeProtoPacket>::operator==(
    const NetworkPacket<metabb::NodeProtoPacket>& other) const noexcept;

/// Specialized template to serialize NodeProtoPacket(s)
template<> std::pair<optnet::BaseNetworkPacket::BaseNetworkPacketType, std::string>
optnet::NetworkPacket<metabb::NodeProtoPacket>::serialize() const noexcept;

}  // namespace optilab


