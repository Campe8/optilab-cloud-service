//
// Copyright OptiLab 2019-2020. All rights reserved.
//
// General utility structs for the metabb namespace
//

#pragma once

#include <limits>  // for std::numeric_limits
#include <list>
#include <memory>  // for std::shared_ptr
#include <vector>

#include "optimizer_network/packet.hpp"
#include "system/system_export_defs.hpp"

// Forward declarations
namespace optilab {
namespace metabb {

class Node;
class DiffSubproblem;

}  // namespace parallelbb
}  // namespace optilab

namespace optilab {
namespace metabb {

namespace net {

struct SYS_EXPORT_STRUCT MetaBBPacketTag : public optnet::PacketTag {
  enum PacketTagType : int {
    PPT_ACK_COMPLETION,
    PPT_ALLOW_TO_BE_IN_COLLECTING_MODE,
    PPT_ANOTHER_NODE_REQUEST,
    PPT_BREAKING,
    PPT_COLLECT_ALL_NODES,
    PPT_COMPLETION_OF_CALCULATION,
    PPT_CUTOFF_VALUE,
    PPT_DIFF_SUBPROBLEM,
    PPT_GIVEN_GAP_IS_REACHED,
    PPT_GLOBAL_BEST_DUAL_BOUND_VALUE_AT_WARM_START,
    PPT_HARD_TIME_LIMIT,
    PPT_IN_COLLECTING_MODE,
    PPT_INCUMBENT_VALUE,
    PPT_INITIAL_STAT,
    PPT_INTERRUPT_REQUEST,
    PPT_LB_BOUND_TIGHTENED_BOUND,
    PPT_LB_BOUND_TIGHTENED_INDEX,
    PPT_LIGHT_WEIGHT_ROOT_NODE_PROCESS,
    PPT_LM_BEST_BOUND_VALUE,
    PPT_NO_NODES,
    PPT_NO_TEST_DUAL_BOUND_GAIN,
    PPT_NO_WAIT_MODE_SEND,
    PPT_NODE_RECEIVED,
    PPT_NOTIFICATION_ID,
    PPT_OUT_COLLECTING_MODE,
    PPT_RACING_RAMP_UP_PARAMSET,
    PPT_RAMP_UP,
    PPT_RESTART,
    PPT_RETRY_RAMP_UP,
    PPT_SOLUTION,
    PPT_SOLVER_STATE,
    PPT_TERMINATE_REQUEST,
    PPT_TERMINATE_SOLVING_TO_RESTART,
    PPT_TERMINATED,
    PPT_TEST_DUAL_BOUND_GAIN,
    PPT_TOKEN,
    PPT_UB_BOUND_TIGHTENED_BOUND,
    PPT_UB_BOUND_TIGHTENED_INDEX,
    PPT_WINNER,
    PTT_NODE,
    PPT_UNDEF
  };

  using UPtr = std::unique_ptr<MetaBBPacketTag>;

  MetaBBPacketTag(PacketTagType tag) : optnet::PacketTag(static_cast<int>(tag)) {}
  ~MetaBBPacketTag() override {}

  inline PacketTagType getPacketTagType() const noexcept
  {
    return static_cast<PacketTagType>(optnet::PacketTag::getTag());
  }
};

}  // net

// Forward declarations
namespace mergenodes {
struct FixedVariable;
}  // namespace mergenodes

/// Utilities and structs for merging nodes
namespace mergenodes {

struct SYS_EXPORT_STRUCT MergeNodeInfo {
  using SPtr = std::shared_ptr<MergeNodeInfo>;

  enum class Status {
    STATUS_MERGING,
    STATUS_MERGED_REPRESENTATIVE,
    STATUS_MERGE_CHECKING_TO_OTHER_NODE,
    STATUS_MERGED_TO_OTHER_NODE,
    STATUS_CANNOT_MERGE,
    STATUS_DELETED,
    STATUS_UNDEFINED
  };

  /// Status of this MergeNodeInfo
  Status status;

  /// Number of fixed values which are the same as those of the merged node.
  /// @note if < 0, this node is not merging target,
  /// i.e., is not going to be merged
  int numSameValueVariables{-1};

  /// Number of merged nodes with this node:
  /// > 0) head
  /// = 0) merging to the other node
  /// < 0) no merging node
  int numMergedNodes{-1};

  /// Index to the fixed variable in the fixed variables list that can reach all
  /// merging nodes
  int keyIndex{-1};

  /// Collection, list of variables with a fixed value.
  /// Each entry in the list is a FixedVariable object.
  /// In turn, a FixedVariable is linked (double linked list) to
  /// other fixed variables with the same value.
  /// @note this object manages the life-time duration of the
  /// FixedVariable objects
  std::vector<std::shared_ptr<FixedVariable>> fixedVariableList;

  /// Pointer to the merge node info this node is merged to
  MergeNodeInfo* mergedTo{nullptr};

  /// Pointer to the next merged node info
  MergeNodeInfo* next;

  /// Pointer to the node owning to this MergeNodeInfo.
  /// @note the owning node has control over the life-time
  /// management of this object
  Node* node{nullptr};

  /// Original diff-subproblem
  std::shared_ptr<DiffSubproblem> origDiffSubproblem;

  /// Merged diff-subproblem, if this node is merged and this is the head
  std::shared_ptr<DiffSubproblem> mergedDiffSubproblem;

  /// Returns the number of fixed variables this MergeNodeInfo is managing
  int getNumFixedVariables() const noexcept { return static_cast<int>(fixedVariableList.size()); }

  /// Returns true if this node is ready to be merged, returns false otherwise
  inline bool isReadyToBeMerged() const noexcept
  {
    return status == mergenodes::MergeNodeInfo::Status::STATUS_MERGING && isNotAMergingNode();
  }

  inline bool isMergingHead() const noexcept { return numMergedNodes > 0; }
  inline bool isMergingToOtherNode() const noexcept { return numMergedNodes == 0; }
  inline bool isNotAMergingNode() const noexcept { return numMergedNodes < 0; }

  /// Returns the pointer to the FixedVariable at "KeyIndex",
  /// i.e., the fixed variable that can be used to reach all following merging nodes
  std::shared_ptr<FixedVariable> getKeyFixedVariable() const
  {
    if (keyIndex < 0 || keyIndex >= static_cast<int>(fixedVariableList.size()))
    {
      return std::shared_ptr<FixedVariable>();
    }

    return fixedVariableList.at(keyIndex);
  }
};

struct SYS_EXPORT_STRUCT FixedVariable {
  using SPtr = std::shared_ptr<FixedVariable>;

  /// The number of same value fixed in the following variables
  int numSameValue{-1};

  /// Index of this fixed variable among all solvers
  int index{-1};

  /// Actual (fixed) value
  double value{std::numeric_limits<double>::lowest()};

  /// Pointer to merge node info struct this variable belongs to.
  /// @note this object does not own the life-time management
  /// of the owning MergeNodeInfo object
  MergeNodeInfo* mnode{nullptr};

  /// Pointer to the next node which has the same fixed value
  FixedVariable* next{nullptr};

  /// Pointer to the previous node which has the same fixed value
  FixedVariable* prev{nullptr};
};

/**
 * FixedValue is used by the merge node algorithms
 * in the load manager as an entry in a table of
 * variables having a fixed value.
 * The table is used as a lookup table to merge
 * nodes that have the same fixed value.
 */
struct SYS_EXPORT_STRUCT FixedValue {
  using SPtr = std::shared_ptr<FixedValue>;

  /// Value for a fixed variable
  double value{std::numeric_limits<double>::lowest()};

  /// List of FixedVariable objects all having the same fixed value
  std::list<FixedVariable*> fixedVariableList;

  /// Pointer to the next fixed value
  std::shared_ptr<FixedValue> next;
};

/**
 * Utility data structure used to merge nodes
 * in the node pool.
 */
/*
struct SYS_EXPORT_STRUCT MergedNodeListElement {
  using SPtr = std::shared_ptr<MergedNodeListElement>;

  /// Pointer to the node owning the MergeNodeInfo
  /// represented by this merge list element
  Node* node{nullptr};

  /// Pointer to the next element in the list
  std::shared_ptr<MergedNodeListElement> next;
};
*/
/**
 * Utility data structure used to sort
 * FixedVariable objects.
 */
struct SYS_EXPORT_STRUCT SortedVariable {
  SortedVariable(int idx, FixedVariable* var)
  : idxInFixedVariables(idx),
    fixedVariable(var)
  {
  }

  /// Index in the fixedVariableList array from the
  /// MergeNodeInfo object
  int idxInFixedVariables{-1};

  /// Pointer to the fixed variable represented by this
  /// SortedVariable object
  FixedVariable* fixedVariable{nullptr};
};

}  // namespace mergenodes

}  // namespace metabb
}  // namespace optilab
