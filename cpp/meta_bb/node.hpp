//
// Copyright OptiLab 2019-2020. All rights reserved.
//
// Tree node for the branch-and-bound meta algorithm.
// Each tree node contains a DiffSubproblem,
// i.e., the difference between the original problem
// and the subproblem represented by the node itself.
// Notice that the DiffSubproblem can be full problem.
// Each node can be either a root node or belong to
// a subtree.
// Basically a tree node is a wrapper around a
// DiffSubproblem with auxiliary methods to retrieve
// the bounds of that subproblem.
//

#pragma once

#include <memory>  // for std::shared_ptr
#include <sparsepp/spp.h>

#include "meta_bb/diff_subproblem.hpp"
#include "meta_bb/framework.hpp"
#include "meta_bb/meta_bb_constants.hpp"
#include "meta_bb/meta_bb_struct.hpp"
#include "meta_bb/tree_id.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace  metabb {

class SYS_EXPORT_CLASS Node {
 public:
  enum class NodeMergingStatus {
    /// Default, there is no merging node
    NMS_NO_MERGING_NODE = 0,

    /// Checking to merge the node
    NMS_CHECKING,

    /// The node was merged and it is the
    /// representative node, i.e., other nodes
    /// merged into this node
    NMS_MERGED,

    /// The node was merged into another node
    NMS_MERGED_TO_THE_OTHER_NODE,

    /// The node cannot be merged
    NMS_CANNOT_BE_MERGED,

    /// The merging node representative (to merge into)
    /// was deleted
    NMS_MERGING_REPRESENTATIVE_WAS_DELETED
  };

  using SPtr = std::shared_ptr<Node>;

 public:
  /// Default constructor
  Node() = default;

  /// Constructor:
  /// - nodeId: identifier of this node
  /// - generatorNodeId: identifier of the node that generated this node
  /// - depth: depth of this node in the search tree
  /// - dualBoundValue: dual bound value of this node
  /// - originalDualBoundValue: original dual bound value
  /// - estimatedValue: estimated value for this node
  /// - diffSubproblem: difference between the original (full) problem to be solved
  ///   and the (sub) problem that has to be solved by this node.
  ///   In other words, the "diffSubproblem" is the actual optimization problem that
  ///   this node is in charge to solve
  Node(const NodeId& nodeId,
       const NodeId& generatorNodeId,
       int depth,
       double dualBoundValue,
       double originalDualBoundValue,
       double estimatedValue,
       DiffSubproblem::SPtr diffSubproblem);

  /// Destructor:
  /// checks the node parent and children and re-connect parent to children
  virtual ~Node();

  /// Returns true if this node is the root node, false otherwise
  inline bool isRootNode() const noexcept
  {
    // Returns true (i.e., root node) if this node is not part of a sub-tree
    // and it is own by the default LM and it doesn't have a sequence number specified
    return pNodeId.getSubtreeId().getLMId() == treeconsts::DEFAULT_UNSPECIFIED_ID &&
        pNodeId.getSubtreeId().getGlobalSubstreeIdInLM() ==
            treeconsts::DEFAULT_UNSPECIFIED_ID &&
        pNodeId.getSubtreeId().getSolverId() == treeconsts::DEFAULT_UNSPECIFIED_ID &&
        pNodeId.getSeqNum() == treeconsts::DEFAULT_UNSPECIFIED_ID;
  }

  /// Returns true if the given node as the same identifier of this node.
  /// Returns false otherwise
  inline bool isSameNodeId(const Node& node) const noexcept
  {
     return pNodeId == node.pNodeId;
  }

  /// Returns true if the parent of this node is the same as the parent of the given node,
  /// i.e., returns true if this node and the given node are children of the same parent.
  /// Returns false otherwise
  inline bool isSameParentNodeId(const Node& node) const noexcept
  {
    return pGeneratorNodeId == node.pGeneratorNodeId;
  }

  /// Returns true if the subtree of this node is the same as the subtree of the given node.
  /// Returns false otherwise
  inline bool isSameParentNodeSubtreeId(const NodeId& nodeId) const noexcept
  {
    return pGeneratorNodeId.getSubtreeId() == nodeId.getSubtreeId();
  }

  /// Returns true if this node's subtree is the same as the subtree of the given node.
  /// Returns false otherwise
  inline bool isSameSubtreeId(const Node& node) const noexcept
  {
    return pNodeId.getSubtreeId() == node.pNodeId.getSubtreeId();
  }

  /// Returns true if this node's global subtree identifier in the load manager is the same as
  /// the one of the given node.
  /// Returns false otherwise
  inline bool isSameLMId(const Node& node) const noexcept
  {
    return getLMId() == node.getLMId();
  }

  /// Returns true if the LM of this node's subtree is the same as the given LM id.
  /// Returns false otherwise
  inline bool isSameLMId(const int lmId) const noexcept
  {
    return getLMId() == lmId;
  }

  /// Returns true if this node's global subtree ID in LM is the same as the one
  /// of the node given as argument.
  /// Returns false otherwise
  inline bool isSameGlobalSubtreeIdInLM(const Node& node) const noexcept
  {
    return getGlobalSubtreeIdInLM() == node.getGlobalSubtreeIdInLM();
  }

  /// Returns true if this node's global subtree ID in LM is the same as the one
  /// given as argument.
  /// Returns false otherwise
  inline bool isSameGlobalSubtreeIdInLMAs(const int globalSubtreeIdInLM)  const noexcept
  {
    return getGlobalSubtreeIdInLM() == globalSubtreeIdInLM;
  }

  /// Returns the LM id this node has been generated from
  inline int getLMId() const noexcept { return pNodeId.getSubtreeId().getLMId(); }

  /// Returns the global subtree id managed by the Load Manager this node belongs to
  inline int getGlobalSubtreeIdInLM() const noexcept
  {
    return pNodeId.getSubtreeId().getGlobalSubstreeIdInLM();
  }

  /// Sets the global subtree id this node belongs to
  inline void setGlobalSubtreeId(int lmId, int subtreeId) noexcept
  {
    pNodeId.getSubtreeId().getLMId() = lmId;
    pNodeId.getSubtreeId().getGlobalSubstreeIdInLM() = subtreeId;
  }

  /// Returns the solver's id solving this node
  inline int getSolverId() const noexcept
  {
    return pNodeId.getSubtreeId().getSolverId();
  }

  /// Sets this node solver's id
  inline void setSolverId(int id) noexcept
  {
    pNodeId.getSubtreeId().getSolverId() = id;
  }

  /// Returns this node id
  inline const NodeId& getNodeId() const noexcept
  {
    return pNodeId;
  }

  /// Sets the id for this node
  inline void setNodeId(const NodeId& nodeId) noexcept
  {
    pNodeId = nodeId;
  }

  /// Returns the generator's id of this node
  inline NodeId getGeneratorNodeId() const noexcept
  {
    return pGeneratorNodeId;
  }

  /// Sets the generator id of this node
  inline void setGeneratorNodeId(NodeId generatorNodeId) noexcept
  {
    pGeneratorNodeId = generatorNodeId;
  }

  /// Returns depth of the node
  inline int getDepth() const noexcept { return pDepth; }

  /// Sets the depth of the node
  inline void setDepth(int depth) noexcept { pDepth = depth; }

  /// Returns the dual bound value
  inline double getDualBoundValue() const noexcept { return pDualBoundValue; }

  /// Sets the dual bound value for this ndoe
  inline void setDualBoundValue(double val) noexcept { pDualBoundValue = val; }

  /// Returns the initial dual bound value that was set for this node
  inline double getInitialDualBoundValue() const noexcept { return pInitialDualBoundValue; }

  /// Sets the initial dual bound value for this node
  inline void setInitialDualBoundValue(double val) noexcept { pInitialDualBoundValue = val; }

  /// Resets the dual bound value to the initial dual bound value
  inline void resetDualBoundValue() noexcept { pDualBoundValue = pInitialDualBoundValue; }

  /// Sets estimated value
  inline void setEstimatedValue(double val) noexcept { pEstimatedValue = val; }

  /// Returns estimated value
  inline double getEstimatedValue() const noexcept { return pEstimatedValue; }

  /// Sets the diff-subproblem handled by this node
  inline void setDiffSubproblem(DiffSubproblem::SPtr diffSubproblem) noexcept
  {
    pDiffSubproblem = diffSubproblem;
  }

  /// Returns the pointer to the diff-subproblem handled by this node
  inline DiffSubproblem::SPtr getDiffSubproblem() const noexcept { return pDiffSubproblem; }

  /// Sets the parent of this node.
  /// Throws std::runtime_error if the parent is not nullptr and
  // it doesn't have a value, i.e., if the pointer to its value is nullptr.
  /// @note overrides any previously set parent
  void setParent(NodeGenealogicalRel::SPtr parent);

  /// Returns the ancestor of this node
  inline NodeGenealogicalRel::SPtr getParent() const noexcept { return pParent; }

  /// Returns true if this node has any descendants.
  /// Returns false otherwise
  inline bool hasChildren() const noexcept { return !pChildren.empty(); }

  /// Adds a child/descendant of this node
  inline void addChild(NodeGenealogicalRel::SPtr descendant) noexcept
  {
    pChildren[descendant->getNodeId()] = descendant;
  }

  /// Removes a child node provided its node identifier
  void removeChild(const NodeId& nodeId);

  /// Updates the initial dual bound value to the minimum dual bound value of the subtree,
  /// i.e., to the minimum dual bound among all children
  inline void updateInitialDualBoundToSubtreeDualBound() noexcept
  {
    pInitialDualBoundValue = getMinimumDualBoundInDesendants(pDualBoundValue);
  }

  /// Recursively look for the minimum dual bound value lower than the given value on
  /// all children (direct and not direct, until the leaves) of this node.
  /// @note this works only on LOCAL nodes, i.e., children that are actually present
  /// on the same machine this node is stored
  double getMinimumDualBoundInDesendants(double val);

  /// Set the merge node info for this node
  inline void setMergeNodeInfo(mergenodes::MergeNodeInfo::SPtr mnInfo) noexcept
  {
    pMergeNodeInfo = mnInfo;
  }

  /// Returns the merge node information hold by this node
  mergenodes::MergeNodeInfo::SPtr getMergeNodeInfo() const noexcept { return pMergeNodeInfo; }

  /// Set the merging status for this node
  inline void setMergingStatus(NodeMergingStatus status) noexcept
  {
    pMergingStatus = status;
  }

  /// Returns the merging status of this node
  inline NodeMergingStatus getMergingStatus() const noexcept
  {
    return pMergingStatus;
  }

  /// Sets collect nodes flag
  inline void collectsNodes() noexcept { pNodesAreCollected = true; }

  /// Returns true if nodes are colledted, false otherwise
  inline bool areNodesCollected() const noexcept { return pNodesAreCollected; }

  /// Returns a clone of this optimization node
  virtual SPtr clone(const Framework::SPtr& comm) = 0;

  /// Broadcasts this node from the given root to all other nodes in the network.
  /// Returns zero on success, non-zero otherwise
  virtual int broadcast(const Framework::SPtr& framework, int root) noexcept = 0;

  /// Sends this node to given destination.
  /// Returns zero on success, non-zero otherwise
  virtual int send(const Framework::SPtr& framework, int destination) noexcept = 0;

  /// Uploads the content of the given network packet into this node.
  /// Returns zero on success, non-zero otherwise
  virtual int upload(const Framework::SPtr& framework, optnet::Packet::UPtr packet) noexcept = 0;

 protected:
  using RelationshipMap = spp::sparse_hash_map<NodeId, NodeGenealogicalRel::SPtr>;

 protected:
  /// Depth from the root node of the original tree
  int pDepth{-1};

  /// Dual bound value
  double pDualBoundValue{std::numeric_limits<double>::lowest()};

  /// Dual bound value received on creation
  double pInitialDualBoundValue{0.0};

  /// Estimated value
  double pEstimatedValue{0.0};

  /// Flag indicating whether or not this node contains
  /// a valid pointer to a diffSubproblem instance
  bool pDiffSubproblemInfo{false};

  /// Id of this node,
  /// i.e., the solving node identifier
  NodeId pNodeId;

  /// Subtree root node identifier of generator
  NodeId pGeneratorNodeId;

  /// Pointer to the parent node.
  /// @note this field is not transferred
  NodeGenealogicalRel::SPtr pParent;

  /// Difference between solving instance data and subproblem data
  DiffSubproblem::SPtr pDiffSubproblem;

  /// Merge status:
  /// -1) no merging node
  ///  0) checking
  ///  1) merged (representative)
  ///  2) merged to the other node
  ///  3) cannot be merged
  ///  4) merging representative was deleted
  NodeMergingStatus pMergingStatus{NodeMergingStatus::NMS_NO_MERGING_NODE};

  /// Pointer to the merge node info structure.
  /// @note if this pointer is empty, no merging is performed
  mergenodes::MergeNodeInfo::SPtr pMergeNodeInfo;

  /// Subproblems generated from this nodes are collected at interruption.
  /// @note this field is not transferred
  bool pNodesAreCollected{false};

  /// Collection of pointers to children of this node.
  /// @note this field is not transferred
  RelationshipMap pChildren;

 private:
  /// Utility function: cleans up the objects and relations
  /// allocated by or within the node before destructor is called
  void cleanupNode();
};

}  // namespace metabb
}  // namespace optilab
