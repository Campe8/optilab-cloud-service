//
// Copyright OptiLab 2020. All rights reserved.
//
// Base class encapsulating initial statistics.
//

#pragma once

#include <memory>

#include "meta_bb/framework.hpp"
#include "optimizer_network/packet.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace  metabb {

class SYS_EXPORT_CLASS InitialStat {
public:
  using SPtr = std::shared_ptr<InitialStat>;

public:
  InitialStat() = default;
  virtual ~InitialStat() = default;

  /// Returns the get maximum depth
  virtual int getMaxDepth() = 0;

  /// Returns a clone of this initial statistics object
  virtual SPtr clone(const Framework::SPtr& framework) = 0;

  /// Sends a copy of this statistics to the given destination
  virtual int send(const Framework::SPtr& comm, int dest) noexcept = 0;

  /// Uploads the content of the given network packet into this InitialStat object.
  /// If the given packet is not a node network packet, throws std::runtime_error
  /// Returns zero on success, non-zero otherwise
  virtual int upload(optnet::Packet::UPtr packet) noexcept = 0;
};

}  // namespace metabb
}  // namespace optilab
