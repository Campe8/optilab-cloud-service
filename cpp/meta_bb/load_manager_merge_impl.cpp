#include "meta_bb/load_manager_merge_impl.hpp"

#include <cassert>
#include <list>
#include <stdexcept>  // for std::runtime_error
#include <string>

#include <spdlog/spdlog.h>

namespace optilab {
namespace  metabb {

LoadManagerMergeImpl::LoadManagerMergeImpl(LoadManager* lm)
: pLM(lm)
{
  assert(pLM);

  // Set the number of bound changes required for merging nodes.
  // @note if the number is -1, set it automatically
  pNumVarBoundChangesOfBestNode = pLM->getParams().numboundchangesofmergenodes();
}

void LoadManagerMergeImpl::initMergeNodesStructs()
{
  // There must be at least one variable in the model
  const auto varIdxRange = pLM->pInitiator->getInstance()->getVarIndexRange();
  assert(varIdxRange > 0);

  // Initialize the table of fixed value variables.
  // The maximum number of fixed variable are the variables in the model
  pVarIndexTable.resize(varIdxRange, mergenodes::FixedValue::SPtr());
  pMergeInfoHead = nullptr;
  pMergeInfoTail = nullptr;
}  // initMergeNodesStructs

void LoadManagerMergeImpl::mergeNodes(Node::SPtr node)
{
  assert(!!node);

  // Keep track of time
  const auto startTime = pLM->pTimer->getWallClockTimeMsec();

  // Use the MergeNodeInfo information own by the node to merge nodes
  auto mnode = node->getMergeNodeInfo();

  // The node should not have been merged to another node.
  // Rather, it should be the representative node of the merged nodes.
  // Therefore, it should also have at least one fixed variable
  assert(mnode->mergedTo == nullptr);
  assert(mnode->status == mergenodes::MergeNodeInfo::Status::STATUS_MERGED_REPRESENTATIVE);
  assert(mnode->getKeyFixedVariable());

  // Go over all the fixed variable and create a list of merging node elements.
  // The idea is to traverse all the fixed variables, take their corresponding nodes,
  // and merge them all together with the first element of the list.
  // @note this list is used to remove the merged nodes from the node pool
  int numMerged = 0;
  std::list<Node*> mergedNodeList;

  // Start from the second fixed variable (this and following will be merged
  // with first element of the list)
  auto traverseFixedVar = mnode->getKeyFixedVariable()->next;
  for (; traverseFixedVar != nullptr; traverseFixedVar = traverseFixedVar->next)
  {
    // Skip, continue, if the node is not merging or is merging to a node
    // that is not the first element of the list
    assert(traverseFixedVar->mnode);
    if (!(traverseFixedVar->mnode->isMergingToOtherNode()) ||
            (mnode.get() != traverseFixedVar->mnode->mergedTo)) continue;

    // Otherwise, add the node to the list of nodes to merge together
    mergedNodeList.push_back(traverseFixedVar->mnode->node);

    // Set the pointer to node to merge into the first element
    (mergedNodeList.back())->setMergingStatus(Node::NodeMergingStatus::NMS_MERGED_TO_THE_OTHER_NODE);
    numMerged++;
  }
  assert(mnode->numMergedNodes == numMerged);

  // Remove the nodes to merge from the node pool
  const auto delNodes = pLM->pNodePool->removeMergedNodes(mergedNodeList);
  assert(delNodes == numMerged);

  // Set the merged subproblem into the node as new subproblem to solve.
  // @note this is where the merged nodes become one single (sub)problem
  node->setDiffSubproblem(mnode->mergedDiffSubproblem);
  node->setMergeNodeInfo(nullptr);
  node->setMergingStatus(Node::NodeMergingStatus::NMS_MERGED);

  // Reset the merging node information.
  // @note the current merge node info should not have a "merge to" info
  // but it should be the one merged into
  mnode->origDiffSubproblem = nullptr;
  mnode->mergedDiffSubproblem = nullptr;
  mnode->origDiffSubproblem = nullptr;
  deleteMergeNodeInfo(mnode.get());
  mnode.reset();

  pLM->pLMTerminationState.numNodesDeletedByMerging += numMerged;
  if (pLM->getParams().quiet())
  {
    std::stringstream ss;
    ss << "LoadManager - mergeNodes: " << numMerged + 1 << " nodes merged at " <<
            pLM->pTimer->getWallClockTimeMsec() << " msec. Dual Bound: " <<
            pLM->pInitiator->convertToExternalValue(node->getDualBoundValue());
    spdlog::info(ss.str());
    std::cout << ss.str() << std::endl;
  }

  // Store the time required for merging nodes
  pLM->pLMTerminationState.mergeNodeTime += pLM->pTimer->getWallClockTimeMsec() - startTime;
}  // mergeNodes

void LoadManagerMergeImpl::addNodeToMergeNodeStructs(Node::SPtr node)
{
  assert(!!node);

  // Check the number of bound changes required for merging nodes.
  // If this is the first time a node is added,
  // use the received node as best node and set the number of bound changes.
  // @note pNumVarBoundChangesOfBestNode is a ParamSet parameter and represents
  // the number of bound changes needed for merging.
  // The value = -1 means that it set automatically by the first node to be merged (default).
  // 0 means everything
  if (pNumVarBoundChangesOfBestNode < 0)
  {
    // Set the number of fixed variables of best node as the number
    // from the given node
    pNumVarBoundChangesOfBestNode = node->getDiffSubproblem()->getNBoundChanges();
  }

  if (pNumVarBoundChangesOfBestNode > 0 &&
      node->getDiffSubproblem() &&
      node->getDiffSubproblem()->getNBoundChanges() <= pNumVarBoundChangesOfBestNode)
  {
    // The given node is out of merging candidates.
    // Do not generate the same merging node twice,
    // return asap
    return;
  }

  // Keep track of time
  const auto startTime = pLM->pTimer->getWallClockTimeMsec();

  // Instantiate a new MergeNodeInfo data structure and set its status as "merging"
  auto newMergeInfo = std::make_shared<mergenodes::MergeNodeInfo>();
  newMergeInfo->status = mergenodes::MergeNodeInfo::Status::STATUS_MERGING;
  newMergeInfo->node = node.get();

  // Set the new merge node info into the node.
  // @note the status is "checking" since it is still checking
  // if this node can be merged
  node->setMergingStatus(Node::NodeMergingStatus::NMS_CHECKING);
  node->setMergeNodeInfo(newMergeInfo);

  // Connect the links between merging nodes and get the fixed variables list
  if (node->getDiffSubproblem())
  {
    // @note this will initialize the list of fixed variables
    // in the merge node info data structure
    node->getDiffSubproblem()->getFixedVariables(
            pLM->pInitiator->getInstance(), newMergeInfo->fixedVariableList);
  }

  // Cannot merge a node with no fixed variables (no other nodes to merge with).
  // If that is the case, return asap
  if (newMergeInfo->getNumFixedVariables() == 0)
  {
    node->setMergingStatus(Node::NodeMergingStatus::NMS_CANNOT_BE_MERGED);
    node->setMergeNodeInfo(nullptr);
    newMergeInfo.reset();
    pLM->pLMTerminationState.addingNodeToMergeStructTimeMsec +=
            (pLM->pTimer->getWallClockTimeMsec() - startTime);
    return;
  }

  // Here there are fixed variables retrieved from the node.
  // Connect this merge node info with others that have the same value
  // for fixed variables

  // Initialize tail and head of the list
  if (pMergeInfoTail == nullptr)
  {
    // Only one element, head and tail point to the same element
    pMergeInfoTail = newMergeInfo.get();
    pMergeInfoHead = newMergeInfo.get();
  }
  else
  {
    // Set the new element as tail and move tail pointer
    // ahead of one
    pMergeInfoTail->next = newMergeInfo.get();
    pMergeInfoTail = newMergeInfo.get();
  }

  // Initialize each fixed variable with this merge node info.
  // @note the list of fixed variables was previously instantiated by the
  // diff. subproblem of the node
  for (auto fixedVar : newMergeInfo->fixedVariableList)
  {
    // Set the owning merge info on each fixed variable
    fixedVar->mnode = newMergeInfo.get();

    // Instantiate a new FixedValue object encapsulating
    // the fixed value.
    // @note the "index" in the fixed variable is the index of the variable
    // in the original problem
    mergenodes::FixedValue::SPtr fixedValue;
    const auto varIdxInInstance = fixedVar->index;
    if (!pVarIndexTable.at(varIdxInInstance))
    {
      // There variable is not registered in the table, register it.
      // @note the index table is "just" a map of variable's index
      // and their fixed value
      fixedValue = std::make_shared<mergenodes::FixedValue>();
      fixedValue->value = fixedVar->value;

      // Set the value in the index table
      pVarIndexTable[varIdxInInstance] = fixedValue;
    }
    else
    {
      // The variable is already registered in the table.
      // Find the last value in the list with same value as the mergeInfo value
      // and "plug-in" a new FixedValue entry
      auto valListTail = pVarIndexTable[varIdxInInstance];
      fixedValue = valListTail;
      for (; fixedValue != nullptr &&
      !EPSEQ(fixedValue->value, fixedVar->value, metaconst::DEAFULT_NUM_EPSILON);
      fixedValue = fixedValue->next)
      {
        // valTail is used to keep track of the last value
        // on the list if next is nullptr.
        // If so, valTail is used to "attach" a new FixedValue at the
        // end of the list
        valListTail = fixedValue;
      }

      // Now attach the value after the list of similar values
      if (fixedValue == nullptr)
      {
        // Create a new FixedValue for the current value to add
        fixedValue = std::make_shared<mergenodes::FixedValue>();
        fixedValue->value = fixedVar->value;
        valListTail->next = fixedValue;
      }
    }
    assert(fixedValue);

    // Add the new fixed variable to the list in the fixed value
    if (fixedValue->fixedVariableList.empty())
    {
      fixedValue->fixedVariableList.push_back(fixedVar.get());
    }
    else
    {
      // Set the pointer from the fixed variable of one node
      // to the fixed variable with the same value of another
      // merging node
      fixedValue->fixedVariableList.back()->next = fixedVar.get();
      fixedValue->fixedVariableList.back()->next->prev = fixedValue->fixedVariableList.back();
      fixedValue->fixedVariableList.push_back(fixedVar.get());
    }

    // Add another value to all variables with fixed value in the list.
    // @note "numSameValue" is the number of same value fixed in the
    // "following" variables
    for (auto fvar : fixedValue->fixedVariableList)
    {
      if (fvar == (fixedValue->fixedVariableList).back()) break;
      fvar->numSameValue++;
    }
  }

  // Update time counters
  pLM->pLMTerminationState.addingNodeToMergeStructTimeMsec +=
          (pLM->pTimer->getWallClockTimeMsec() - startTime);
} // addNodeToMergeNodeStructs

void LoadManagerMergeImpl::generateMergeNodesCandidates()
{
  // Keep track of time
  const auto startTime = pLM->pTimer->getWallClockTimeMsec();

  // Iterate over the MergeInfoNodes.
  // @note MergeInfoHead is set when adding a node to the merge node structs.
  // In other words, by calling the "addNodeToMergeNodeStructs(...)" method
  auto mpre = pMergeInfoHead;
  auto mnode = pMergeInfoHead;
  while (mnode)
  {
    assert((mnode->node->getMergeNodeInfo()).get() == mnode);
    if (mnode->isReadyToBeMerged())
    {
      // Prepare a list of fixed variables sorted in descending order by
      // the number of variable with the same value.
      //
      //    MergedNodeInfo
      //
      // 4     3     1     1      List of sorted variables
      // #---->#---->#---->#
      // |     |     |     |
      // * p1  *     *     *      Fixed variables
      // |     |
      // * p2  *
      // |     |
      // * p3  *
      // |
      // * p4
      //
      // where p1, p2, p3, and p4 point to other MergedNodeInfo
      // objects having the same fixed value.
      // @note the pointers point to another FixedVariable object
      // owned by another MergedNodeInfo rather than pointing to a
      // MergedNodeInfo directly.
      //
      int idx{0};
      std::multimap<int, mergenodes::SortedVariable, std::greater<int>> descendent;
      for (const auto& fixedVar : mnode->fixedVariableList)
      {
        descendent.insert(std::make_pair(
                fixedVar->numSameValue,
                mergenodes::SortedVariable(idx++, fixedVar.get())));
      }

      // Try to merge candidates.
      // Start with the first sorted variable,
      // take the corresponding fixed variable and traverse all the fixed variable
      // with same value.
      // In other words, start with
      //
      // 4
      // #--
      // |
      // * p1
      // |
      // * p2
      // |
      // * p3
      // |
      // * p4
      //
      auto it = descendent.begin();

      // Set the new key index of the first element in descendent
      // in the merge node to pick that fixed variable
      mnode->keyIndex = it->second.idxInFixedVariables;
      mnode->numSameValueVariables = 1;

      // Start traversing the fixed variables list from the second fixed variable
      // in the list
      int numNodes{0};
      auto traverseFixedVariable = mnode->getKeyFixedVariable()->next;
      for (; traverseFixedVariable != nullptr; traverseFixedVariable = traverseFixedVariable->next)
      {
        // Initialize all merging nodes.
        // Go over all the FixedVariables in the list.
        // Take their MergeNodeInfo parent pointer and if its status is MERGING,
        // then set "mnode" as the MergeNodeInfo to merge to
        assert(traverseFixedVariable->mnode);
        if (traverseFixedVariable->mnode->isReadyToBeMerged())
        {
          assert(traverseFixedVariable->mnode != mnode);

          // The fixed variable merge node info (i.e., the node itself)
          // will be merged to the head of the list
          traverseFixedVariable->mnode->mergedTo = mnode;
          traverseFixedVariable->mnode->numMergedNodes = 0;
          traverseFixedVariable->mnode->numSameValueVariables = 1;
          numNodes++;
        }
      }

      // Now consider all the fixed variables merging to another node.
      // In particular, all those merging to the current mnode.
      // In other words, proceed with
      //
      //    3     1     1
      // -->#---->#---->#
      //    |     |     |
      //    * p1  * p1  * p1
      //    |
      //    * p2
      //    |
      //    * p3
      //
      ++it;
      for (; it != descendent.end(); ++it)
      {
        // Check if at least one node can be merged.
        // @note the following for-loop is only used to determine if a node
        // can be merged, or if we the for loop (it) can be broken sooner
        traverseFixedVariable = mnode->fixedVariableList[it->second.idxInFixedVariables]->next;
        for (; (!!traverseFixedVariable); traverseFixedVariable = traverseFixedVariable->next)
        {
          if (traverseFixedVariable->mnode->numMergedNodes == 0 &&
                  (traverseFixedVariable->mnode->mergedTo == mnode))
          {
            // Check if at least one node can be merged
            if (traverseFixedVariable->mnode->numSameValueVariables ==
                    mnode->numSameValueVariables) break;
          }
        }

        if (traverseFixedVariable == nullptr)
        {
          // No node can be merged
          break;
        }

        // At least one node can be merged, merge nodes
        mnode->numSameValueVariables++;
        traverseFixedVariable = mnode->fixedVariableList[it->second.idxInFixedVariables]->next;
        for (; traverseFixedVariable != nullptr;
                traverseFixedVariable = traverseFixedVariable->next)
        {
          if (traverseFixedVariable->mnode->numMergedNodes == 0 &&
                  (traverseFixedVariable->mnode->mergedTo == mnode))
          {
            // Update the counter
            if (traverseFixedVariable->mnode->numSameValueVariables ==
                    (mnode->numSameValueVariables - 1))
            {
              traverseFixedVariable->mnode->numSameValueVariables++;
            }
          }
        }
      }

      // If the number of fixed variables is too small,
      // then the merged node is not created
      const auto nFixedVar = static_cast<int>(
              mnode->getNumFixedVariables() * pLM->getParams().fixedvariablesratioinmerging());
      if (numNodes < 2 || nFixedVar < 1 ||
          mnode->numSameValueVariables < nFixedVar ||
          (pNumVarBoundChangesOfBestNode > 0 &&
                  mnode->numSameValueVariables < pNumVarBoundChangesOfBestNode))
      {
        // Cleanup everything
        for (auto cleanup = mnode->fixedVariableList[mnode->keyIndex]->next;
                cleanup != nullptr; cleanup = cleanup->next)
        {
          if (cleanup->mnode->mergedTo == mnode)
          {
            assert(cleanup->mnode->status == mergenodes::MergeNodeInfo::Status::STATUS_MERGING);
            cleanup->mnode->numSameValueVariables = -1;
            cleanup->mnode->numMergedNodes = -1;
            cleanup->mnode->keyIndex = -1;
            cleanup->mnode->mergedTo = nullptr;
          }
        }
        assert(mnode->origDiffSubproblem == nullptr);
        assert(mnode->mergedDiffSubproblem == nullptr);

        // Store the old merge info before setting it to nullptr
        // and calling the destructor.
        // The merge info could be the one currently in use
        // which is a raw pointer
        auto oldMergeInfo = mnode->node->getMergeNodeInfo();
        mnode->node->setMergeNodeInfo(nullptr);
        mnode->node->setMergingStatus(Node::NodeMergingStatus::NMS_CANNOT_BE_MERGED);

        auto doomed = mnode;
        if (mnode == pMergeInfoHead)
        {
          pMergeInfoHead = mnode->next;
          mpre = pMergeInfoHead;
          mnode = pMergeInfoHead;
        }
        else
        {
          mpre->next = mnode->next;
          mnode = mnode->next;
        }

        if (mnode == pMergeInfoTail)
        {
          pMergeInfoTail = mpre;
        }

        deleteMergeNodeInfo(doomed);
      }
      else
      {
        // Nodes can be merged here
        // a) Calculate the number of merging nodes
        int numMergedNodes = 0;
        for (auto cleanup = mnode->fixedVariableList[mnode->keyIndex]->next;
             cleanup != nullptr;
             cleanup = cleanup->next)
        {
          if (cleanup->mnode->mergedTo == mnode)
          {
            if (mnode->numSameValueVariables == cleanup->mnode->numSameValueVariables)
            {
              numMergedNodes++;
              cleanup->mnode->status =
                      mergenodes::MergeNodeInfo::Status::STATUS_MERGE_CHECKING_TO_OTHER_NODE;
            }
            else
            {
              assert(cleanup->mnode->status == mergenodes::MergeNodeInfo::Status::STATUS_MERGING);
              cleanup->mnode->numSameValueVariables = -1;
              cleanup->mnode->numMergedNodes = -1;
              cleanup->mnode->keyIndex = -1;
              cleanup->mnode->mergedTo = nullptr;
            }
          }
        }
        mnode->numMergedNodes = numMergedNodes;
        assert(numMergedNodes > 0);

        // b) Prepare the list of fixed variables
        int idx = 0;
        std::vector<mergenodes::FixedVariable*> fixedVariablesList(
                mnode->numSameValueVariables);
        for (auto pos = descendent.begin(); pos != descendent.end(); ++pos)
        {
          fixedVariablesList[idx++] = pos->second.fixedVariable;

          // Break when all variables are set
          if (idx == mnode->numSameValueVariables) break;
        }

        // c) Set the original subproblem
        mnode->origDiffSubproblem = mnode->node->getDiffSubproblem();

        // d) Create the merged subproblem
        mnode->mergedDiffSubproblem =
                mnode->origDiffSubproblem->createDiffSubproblem(
                        pLM->pFramework, pLM->pInitiator, fixedVariablesList);
        fixedVariablesList.clear();

        // e) Set the new subproblem in the owning node
        mnode->node->setDiffSubproblem(mnode->mergedDiffSubproblem);

        // f) Change the status of the MergeNodeInfo to merged
        mnode->status = mergenodes::MergeNodeInfo::Status::STATUS_MERGED_REPRESENTATIVE;

        assert(mnode->mergedTo == nullptr);
        mpre = mnode;
        mnode = mnode->next;
      }
    }
    else
    {
      mpre = mnode;
      mnode = mnode->next;
    }
  }

  // Clear the variable's index table
  if (!pVarIndexTable.empty())
  {
    for (int idx = 0; idx < pLM->pInitiator->getInstance()->getVarIndexRange(); ++idx)
    {
      while (pVarIndexTable[idx])
      {
        auto del = pVarIndexTable[idx];
        pVarIndexTable[idx] = pVarIndexTable[idx]->next;
        del.reset();
      }
    }

    pVarIndexTable.clear();
  }

  // Set time
  pLM->pLMTerminationState.generateMergeNodesCandidatesTimeMsec +=
          (pLM->pTimer->getWallClockTimeMsec() - startTime);
}  // generateMergeNodesCandidates

void LoadManagerMergeImpl::regenerateMergeNodesCandidates(Node::SPtr node)
{
  // Keep track of time
  const auto startTime = pLM->pTimer->getWallClockTimeMsec();
  auto mnode = node->getMergeNodeInfo();
  assert(mnode);
  assert(mnode->node == node.get());
  assert(mnode->status == mergenodes::MergeNodeInfo::Status::STATUS_MERGED_REPRESENTATIVE);
  assert(mnode->mergedTo == nullptr);

  // Reset node
  node->setMergeNodeInfo(nullptr);
  node->resetDualBoundValue();
  node->setDiffSubproblem(mnode->origDiffSubproblem);
  mnode->mergedDiffSubproblem.reset();

  // Set new range
  pMergeInfoHead = nullptr;
  pMergeInfoTail = nullptr;
  mergenodes::MergeNodeInfo* mprev{nullptr};
  for (auto traverseFixedVariable = mnode->getKeyFixedVariable()->next;
          traverseFixedVariable != nullptr; traverseFixedVariable = traverseFixedVariable->next)
  {
    if (pMergeInfoTail)
    {
      mprev->next = traverseFixedVariable->mnode;
      pMergeInfoTail = traverseFixedVariable->mnode;
      mprev = traverseFixedVariable->mnode;
    }

    if (mnode.get() == traverseFixedVariable->mnode->mergedTo && !pMergeInfoHead)
    {
      pMergeInfoHead = traverseFixedVariable->mnode;
      pMergeInfoTail = traverseFixedVariable->mnode;
      mprev = traverseFixedVariable->mnode;
    }
  }

  if (pMergeInfoHead)
  {
    assert(pMergeInfoTail);
    pMergeInfoTail->next = nullptr;
  }

  // Remove mnode
  mnode->node->setMergingStatus(Node::NodeMergingStatus::NMS_NO_MERGING_NODE);
  deleteMergeNodeInfo(mnode.get());

  if (pMergeInfoHead)
  {
    // Regenerate the merge node candidates
    generateMergeNodesCandidates();
  }

  // Set timer
  pLM->pLMTerminationState.regenerateMergeNodesCandidatesTime +=
          (pLM->pTimer->getWallClockTimeMsec() - startTime);
}  // regenerateMergeNodesCandidates

void LoadManagerMergeImpl::deleteMergeNodeInfo(mergenodes::MergeNodeInfo* mnode)
{
  assert(mnode != nullptr);
  if ((mnode->numMergedNodes == 0) && mnode->mergedTo)
  {
    assert(mnode->status == mergenodes::MergeNodeInfo::Status::STATUS_MERGE_CHECKING_TO_OTHER_NODE);
    assert(mnode->mergedTo->status ==
            mergenodes::MergeNodeInfo::Status::STATUS_MERGED_REPRESENTATIVE);
    assert(mnode->mergedTo->mergedTo == nullptr);
    mnode->mergedTo->numMergedNodes--;

    // Reset the "mergeTo" information
    if (mnode->mergedTo->numMergedNodes == 0 && mnode->mergedTo->node->getMergeNodeInfo())
    {
       assert(mnode->mergedTo == (mnode->mergedTo->node->getMergeNodeInfo()).get());

       // Store the merge node info before deleting it to avoid raw pointer
       // to point to a deleted instance
       auto oldMergeNodeInfo = mnode->mergedTo->node->getMergeNodeInfo();
       mnode->mergedTo->node->setDiffSubproblem(mnode->mergedTo->origDiffSubproblem);
       mnode->mergedTo->node->setMergeNodeInfo(nullptr);
       mnode->mergedTo->node->setMergingStatus(Node::NodeMergingStatus::NMS_NO_MERGING_NODE);
       mnode->mergedTo->mergedDiffSubproblem = nullptr;
       mnode->mergedTo->origDiffSubproblem = nullptr;

       // Recursive call to the node this one has been merged to
       deleteMergeNodeInfo(mnode->mergedTo);
    }
    mnode->mergedTo = nullptr;
  }

  if (mnode->status == mergenodes::MergeNodeInfo::Status::STATUS_MERGED_REPRESENTATIVE)
  {
    assert(mnode->mergedTo == nullptr);
    if (mnode->node->getMergingStatus() == Node::NodeMergingStatus::NMS_NO_MERGING_NODE)
    {
      // Merging failed
      for (auto traverse = mnode->getKeyFixedVariable()->next; traverse != nullptr;
              traverse = traverse->next)
      {
        if (traverse->mnode->numMergedNodes == 0 && mnode == traverse->mnode->mergedTo)
        {
          traverse->mnode->mergedTo->numMergedNodes--;
          if (traverse->mnode->mergedTo->numMergedNodes == 0 &&
                  traverse->mnode->mergedTo->node->getMergeNodeInfo())
          {
            // @note mNode == traverse->mnode->mergedTo
            // Store old merge node info before deleting it (smart pointers)
            auto oldMergeNodeInfo = traverse->mnode->mergedTo->node->getMergeNodeInfo();
            traverse->mnode->mergedTo->node->setDiffSubproblem(
                    traverse->mnode->mergedTo->origDiffSubproblem);
            traverse->mnode->mergedTo->node->setMergeNodeInfo(nullptr);
            traverse->mnode->mergedTo->node->setMergingStatus(
                    Node::NodeMergingStatus::NMS_NO_MERGING_NODE);
            traverse->mnode->mergedTo->mergedDiffSubproblem = nullptr;
            traverse->mnode->mergedTo->origDiffSubproblem = nullptr;
            assert(traverse->mnode->mergedTo->mergedTo == nullptr);
            deleteMergeNodeInfo(traverse->mnode->mergedTo);
          }

          auto traverseMergedNodePtr = traverse->mnode->node;
          traverse->mnode->mergedTo = nullptr;
          traverse->mnode->node->setMergingStatus(Node::NodeMergingStatus::NMS_CHECKING);
          traverse->mnode->status = mergenodes::MergeNodeInfo::Status::STATUS_MERGING;
          traverse->mnode->numMergedNodes = -1;
          traverse->mnode->numSameValueVariables = -1;
          traverse->mnode->keyIndex = -1;
        }
      }
    }
    else
    {
      // Merging not failed
      for (auto traverse = mnode->getKeyFixedVariable()->next; traverse != nullptr;
              traverse = traverse->next)
      {
        if (traverse->mnode->numMergedNodes == 0 && mnode == traverse->mnode->mergedTo)
        {
          traverse->mnode->mergedTo->numMergedNodes--;
          if (traverse->mnode->mergedTo->numMergedNodes == 0 &&
                  traverse->mnode->mergedTo->node->getMergeNodeInfo())
          {
            // @note mNode == traverse->mnode->mergedTo
            // Store old merge node info before deleting it (smart pointers)
            auto oldMergeNodeInfo = traverse->mnode->mergedTo->node->getMergeNodeInfo();
            traverse->mnode->mergedTo->node->setDiffSubproblem(
                    traverse->mnode->mergedTo->origDiffSubproblem);
            traverse->mnode->mergedTo->node->setMergeNodeInfo(nullptr);
            traverse->mnode->mergedTo->node->setMergingStatus(
                    Node::NodeMergingStatus::NMS_NO_MERGING_NODE);
            traverse->mnode->mergedTo->mergedDiffSubproblem = nullptr;
            traverse->mnode->mergedTo->origDiffSubproblem = nullptr;
            assert(traverse->mnode->mergedTo->mergedTo == nullptr);
            deleteMergeNodeInfo(traverse->mnode->mergedTo);
          }
          traverse->mnode->mergedTo = nullptr;

          if (traverse->mnode->node->getDualBoundValue() < mnode->node->getDualBoundValue())
          {
            traverse->mnode->node->setMergingStatus(
                    Node::NodeMergingStatus::NMS_CHECKING);
            traverse->mnode->status =
                    mergenodes::MergeNodeInfo::Status::STATUS_MERGING;
            traverse->mnode->numMergedNodes = -1;
            traverse->mnode->numSameValueVariables = -1;
            traverse->mnode->keyIndex = -1;
          }
          else
          {
            // @note merging representative was deleted,
            // i.e., this node should be deleted
            traverse->mnode->node->setMergingStatus(
                    Node::NodeMergingStatus::NMS_MERGING_REPRESENTATIVE_WAS_DELETED);
            traverse->mnode->status =
                    mergenodes::MergeNodeInfo::Status::STATUS_DELETED;
            traverse->mnode->numMergedNodes = -1;
            traverse->mnode->numSameValueVariables = -1;
            traverse->mnode->keyIndex = -1;
          }
        }
      }
    }
  }

  if (mnode->getNumFixedVariables() > 0)
  {
    for (auto idx = 0; idx < mnode->getNumFixedVariables(); ++idx)
    {
      for (auto traverse = mnode->fixedVariableList[idx]->prev;
              traverse != nullptr; traverse = traverse->prev)
      {
        traverse->numSameValue--;
      }

      if (mnode->fixedVariableList[idx]->prev)
      {
        mnode->fixedVariableList[idx]->prev->next = mnode->fixedVariableList[idx]->next;
        if (mnode->fixedVariableList[idx]->next)
        {
          mnode->fixedVariableList[idx]->next->prev = mnode->fixedVariableList[idx]->prev;
        }
      }
      else
      {
        if (mnode->fixedVariableList[idx]->next)
        {
          mnode->fixedVariableList[idx]->next->prev = nullptr;
        }
      }
    }
  }
}  // deleteMergeNodeInfo

}  // namespace metabb
}  // namespace optilab
