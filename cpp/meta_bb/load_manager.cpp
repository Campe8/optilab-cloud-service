#include "meta_bb/load_manager.hpp"

#include <cassert>
#include <cmath>       // for std::ceil
#include <functional>  // for std::greater
#include <ostream>
#include <stdexcept>   // for std::invalid_argument
#include <sstream>
#include <utility>     // for std::make_pair

#include <boost/any.hpp>
#include <spdlog/spdlog.h>

#include "meta_bb/load_manager_impl.hpp"
#include "meta_bb/load_manager_merge_impl.hpp"
#include "meta_bb/meta_bb_constants.hpp"
#include "meta_bb/meta_bb_macros.hpp"
#include "meta_bb/solver_state.hpp"

namespace {
#define VOID_CAST(ptr) (static_cast<void*>(ptr))

// Rank of the LoadManager.
// @note LoadManage should ALWAYS HAVE rank 0
constexpr int kLoadManagerRank = 0;

// Active solvers are solving a received subproblem by alternately solving nodes
// and transferring half of the child nodes to the LM
constexpr int kRampUpPhaseNormal = 0;

// LM sends the root node to all solvers which start solving it immediately.
// Each solver uses a different version of parameter settings, branching variable selection,
// and permutation of variables and constraints, in order to generate different search trees
constexpr int kRampUpPhaseRacing = 1;

// Same as "kRampUpPhaseRacing" but rebuilds the tree after racing and proceeds from there
constexpr int kRampUpPhaseRebuildTreeAfterRacing = 2;

constexpr std::size_t kSolverOriginalRank = 1;

// An internal multiplier for number of nodes left in the winning node
// in order to stop racing
constexpr double kNumNodesLeftToStopRacingMultInternal = 2.0;

// Default num. collect once multiplier value
constexpr int kDefaultNumCollectOnceMult = 5;

// Status of the first collecting mode:
// -1) not being in collecting mode
//  0) in collecting mode once
//  1) collecting mode is terminated once
constexpr int kFirstCollectingModeNotBeingInCollecting = -1;
constexpr int kFirstCollectingModeInCollectingModeOnce =  0;
constexpr int kFirstCollectingModeTerminatedOnce       =  1;

// Error value - no error
constexpr int kErrorNoError = 0;

// Maximum number of dual boung gains to keep in the last several
// dual bound gains
constexpr std::size_t kMaxNumDualBoundGainsInQueue = 7;
}  // namespace

namespace optilab {
namespace  metabb {

int LoadManager::GetLoadManagerRank()
{
  return kLoadManagerRank;
}  // GetLoadManagerRank

LoadManager::LoadManager()
{
}

LoadManager::LoadManager(Framework::SPtr framework, ParamSet::SPtr paramSet,
                         Initiator::SPtr initiator, timer::Timer::SPtr timer)
: pFramework(framework),
  pParamSet(paramSet),
  pInitiator(initiator),
  pTimer(timer)
{
  if (!pFramework)
  {
    const std::string errMsg = "LoadManager - empty pointer to Framework";
    spdlog::error(errMsg);
    throw std::invalid_argument(errMsg);
  }

  if (!pParamSet)
  {
    const std::string errMsg = "LoadManager - empty pointer to ParameterSet";
    spdlog::error(errMsg);
    throw std::invalid_argument(errMsg);
  }

  if (!pInitiator)
  {
    const std::string errMsg = "LoadManager - empty pointer to Initiator";
    spdlog::error(errMsg);
    throw std::invalid_argument(errMsg);
  }

  if (!pTimer)
  {
    pTimer = std::make_shared<timer::Timer>();
  }

  // Set the network communicator
  pCommunicator = std::dynamic_pointer_cast<optnet::NetworkCommunicator>(pFramework->getComm());
  if (!pCommunicator)
  {
    const std::string errMsg = "LoadManager - invalid downcast from base network communicator "
            "to network communicator";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  if (pCommunicator->getRank() != kLoadManagerRank)
  {
    const std::string errMsg = std::string("LoadManager - invalid rank for LoadManager: ") +
            std::to_string(pCommunicator->getRank()) + " instead of " +
            std::to_string(kLoadManagerRank);
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  // Initialize the load manager
  spdlog::info("LoadManager - start initialization");
  initLoadManager();
  spdlog::info("LoadManager - initialization done");
}

void LoadManager::initLoadManager()
{
  // Set the rank for the termination state of this load manager
  pLMTerminationState.rank = pCommunicator->getRank();

  // Create the load manager's implementation instance holding the callback functions
  // that are triggered upon receiving network messages
  const bool isRacing{true};
  pLoadManagerImpl = std::make_shared<LoadManagerImpl>(this, !isRacing);
  pLoadManagerRacingRampUpImpl = std::make_shared<LoadManagerImpl>(this, isRacing);

  // Create the load manager implementation for merge nodes
  pLoadManagerMergeImpl = std::make_shared<LoadManagerMergeImpl>(this);
;
  // Ramp-up mode:
  // 0) normal
  // 1) racing
  // 2) rebuild tree after racing.
  const auto rampUpProcess = getParams().rampupphaseprocess();
  const auto ctbr = getParams().communicatetighterboundsinracing();
  if (rampUpProcess  == kRampUpPhaseNormal)
  {
    spdlog::info("LoadManager - ramp-up: normal");
  }
  else if (rampUpProcess  == kRampUpPhaseRacing)
  {
    spdlog::info("LoadManager - ramp-up: racing");
  }
  else if (rampUpProcess  == kRampUpPhaseRebuildTreeAfterRacing)
  {
    spdlog::info("LoadManager - ramp-up: racing and rebuild tree after racing");
  }
  else
  {
    const std::string errMsg = "LoadManager - ramp-up: unrecognized ramp-up method: " +
        std::to_string(rampUpProcess);
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  if (!getParams().quiet())
  {
    std::ostringstream ss;
    ss << "LM start solving status (msec) " << pTimer->getWallClockTimeMsec() << std::endl;
  }

  // Instantiate the pool of solvers
  const double bgap = getParams().bgapcollectingmode();
  pSolverPool = std::make_shared<SolverPoolForMinimization>(
          getParams().multiplierforcollectingmode(),
          bgap,
          getParams().multiplierforbgapcollectingmode(),
          kSolverOriginalRank,
          pFramework, pParamSet, pTimer);

  // Instantiate the pool of nodes
  if (getParams().cleanup())
  {
    pNodePool = std::make_shared<NodePoolForCleanUp>(bgap);
  }
  else
  {
    // @note it is always a minimization problem
    pNodePool = std::make_shared<NodePoolForMinimization>(bgap);
  }

  // Set epsilon
  pEpsilon = pInitiator->getEpsilon();

  // Reset the string keeping track of the last checkpoint in time
  pLastCheckpointTimeStrMsec.clear();

  // Set breaking flag.
  // @note the following parameters:
  // - numsolvernodesstartbreaking
  // - numstopbreaking
  // have default value of zero.
  // If zero, no breaking is applied, hence, breaking is trivially finished
  pIsBreakingFinished = (getParams().numsolvernodesstartbreaking() == 0) ||
          (getParams().numstopbreaking() == 0);

  // Ratio for random nodes.
  // If less than or equal the minimum epsilon, don't perform random selection at all
  const double randomSelecRatio = getParams().randomnodeselectionratio();
  if (!EPSEQ(randomSelecRatio, 0.0, metaconst::MIN_EPSILON))
  {
    pNumNodesNormalSelection = -1;
  }

  // Collect all open nodes after ramp-up process
  const bool collectOnce = getParams().collectonce();
  if (!collectOnce ||
      !((rampUpProcess == kRampUpPhaseRacing ||
          rampUpProcess == kRampUpPhaseRebuildTreeAfterRacing)))
  {
    // Set all winnder nodes as already collected if
    // 1) there is no need to collect nodes after ramp-up phase; or
    // 2) if there is no ramp-up phase at all
    pWinnerSolverNodesCollected = true;
  }
}  // initLoadManager

LoadManager::~LoadManager()
{
  // Tear down the manager
  try
  {
    tearDown();
  }
  catch(...)
  {
    spdlog::info("LoadManager - destructor: unknown exception");
  }
}

void LoadManager::waitForAndExecuteIncomingMessage(std::shared_ptr<LoadManagerImpl> callback)
{
  assert(!!callback);

  // Keep track of idle time counter
  timer::Timer idleTime;

  // Wait for any incoming message from any source
  optnet::Packet::UPtr packet(new optnet::Packet());
  ASSERT_COMM_CALL(pCommunicator->probe(packet));

  pLMTerminationState.idleTimeMsec += idleTime.getWallClockTimeMsec();

  // Check if the received packet has a tag that is supported
  auto tag = utils::castToMetaBBPacketTagAndGetTagOrThrow(packet->packetTag);
  if (!(callback->isHandlerSupported(tag)))
  {
    const std::string errMsg = "LoadManager - waitForAndExecuteIncomingMessage: "
        "error on message handler, tag not supported";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  // Run the callback function on the received tagged message
  // Get the source
  const int source = pCommunicator->convertNetworkAddrToRank(packet->networkAddr);
  if ((*callback)(source, std::move(packet)))
  {
    const std::string errMsg = "LoadManager - waitForAndExecuteIncomingMessage: "
        "error on callback message handler";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }
}  // waitForAndExecuteIncomingMessage

void LoadManager::tearDown()
{
  spdlog::info("LoadManager - start tear-down process");

  if (pTearDown)
  {
    spdlog::info("LoadManager - tear-down already processed, return");
    return;
  }

  // If no solver has terminated yet, wait to receive termination messages
  if ((pNumTerminated == 0) &&
      ((pRacingSolverPool && (pRacingSolverPool->getNumActiveSolvers() > 0)) ||
       (pSolverPool->getNumActiveSolvers() > 0) ||
        pInterruptIsRequested))
  {
    // Terminate execution of all solvers
    terminateSolvers();

    // Switch phase to termination phase
    pRunningPhase = RunningPhase::LMRP_TERMINATION;
    while (true)
    {
      waitForAndExecuteIncomingMessage(pLoadManagerImpl);

      // If the number of terminated solvers equals the number of solvers,
      // break asap
      if (pNumTerminated == pSolverPool->getNumSolvers()) break;
    }  // while
  }

  // All solvers are terminated here

  // Write the solution
  pInitiator->writeSolution("Final Solution");

  // Set statistics on the node and solver pools usage
  if (pNodePool)
  {
    pLMTerminationState.maxUsageOfNodePool = static_cast<uint64_t>(pNodePool->getMaxUsageOfPool());
    pLMTerminationState.initialP = static_cast<uint64_t>(
            getParams().numnodestoswitchtocollectingmode());
    pLMTerminationState.mMaxCollectingNodes = static_cast<uint64_t>(
            pSolverPool->getMMaxCollectingNodes());
    pLMTerminationState.numNodesInNodePool = static_cast<uint64_t>(pNodePool->getNumNodes());
  }

  // Set the status of the solvers on tear-down
  if (pInitialNodesGenerated)
  {
    pInitiator->setFinalSolverStatus(solver::FinalSolverState::FSS_INITIAL_NODES_GENERATED);
  }
  else if (pHardTimeLimitIsReached)
  {
    // Timeout
    pInitiator->setFinalSolverStatus(solver::FinalSolverState::FSS_HARD_TIME_LIMIT_REACHED);
  }
  else if (pForceInterrupt || (pComputationIsInterrupted && !pRacingTermination))
  {
    // Forced interrupt
    pInitiator->setFinalSolverStatus(solver::FinalSolverState::FSS_COMPUTING_INTERRUPTED);
  }
  else
  {
    // The problem is solved,
    // i.e., the time limits has not being reached, no interruptions, and there are no
    // initial nodes generated left to explore
    pInitiator->setFinalSolverStatus(solver::FinalSolverState::FSS_PROBLEM_SOLVED);
  }

  // Set number of nodes solved and final dual bound value
  if (pInitialNodesGenerated)
  {
    pInitiator->setNumSolvedNodes(std::max<uint64_t>(uint64_t{1},
                                                     pSolverPool->getTotalNodesSolved()));

    const double bestDualBoundValue = pNodePool->getBestDualBoundValue();
    pInitiator->setDualBound(bestDualBoundValue);
    pLMTerminationState.externalGlobalBestDualBoundValue =
            pInitiator->convertToExternalValue(bestDualBoundValue);
  }
  else if (pRacingTermination)
  {
    // Racing is terminating due to some external factors.
    // Check if this is due to some interrupt of time limit
    if ((pForceInterrupt || pHardTimeLimitIsReached) && pRacingSolverPool)
    {
      pInitiator->setNumSolvedNodes(pRacingSolverPool->getNumNodesSolvedInBestSolver());
    }
    else
    {
      // Set the number of nodes solved at termination of racing
      pInitiator->setNumSolvedNodes(pNumNodesSolvedAtRacingTermination);
    }

    // Set the better (higher) lower bound, i.e., closer to the best upper bound
    if (pLMTerminationState.globalBestDualBoundValue < pMinimalDualBoundNormalTermSolvers)
    {
      pLMTerminationState.globalBestDualBoundValue = pMinimalDualBoundNormalTermSolvers;
    }

    // Set dual bound from the global best dual bound value
    pInitiator->setDualBound(pLMTerminationState.globalBestDualBoundValue);
    pLMTerminationState.externalGlobalBestDualBoundValue =
        pInitiator->convertToExternalValue(pLMTerminationState.globalBestDualBoundValue);
  }
  else
  {
    // Here racing is done.
    // Check if the stop is due to some force interruption or time limits
    if (!pForceInterrupt && !pHardTimeLimitIsReached && !pComputationIsInterrupted)
    {
      pInitiator->setNumSolvedNodes(std::max<uint64_t>(uint64_t{1},
                                                       pSolverPool->getTotalNodesSolved()));

      auto bestGlbIncumbent = pInitiator->getGlobalBestIncumbentSolution();
      if (bestGlbIncumbent && pAllCompInfeasibleAfterSolution)
      {
        // After the solution, no solver found another feasible one
        pInitiator->setDualBound(bestGlbIncumbent->getObjectiveFuntionValue());
        pLMTerminationState.externalGlobalBestDualBoundValue =
                pInitiator->convertToExternalValue(bestGlbIncumbent->getObjectiveFuntionValue());
      }
      else
      {
        if (pLMTerminationState.globalBestDualBoundValue < pMinimalDualBoundNormalTermSolvers)
        {
          // If there is an incumbent, set the dual bound w.r.t. to the incumbent.
          // Otherwise set it as the minimal dual bound of solvers on termination
          auto bestGlbIncumbent = pInitiator->getGlobalBestIncumbentSolution();
          const double bestDualBound = bestGlbIncumbent ?
                  std::min(bestGlbIncumbent->getObjectiveFuntionValue(),
                           pMinimalDualBoundNormalTermSolvers) :
                           pMinimalDualBoundNormalTermSolvers;
          pLMTerminationState.globalBestDualBoundValue = bestDualBound;
        }
        pInitiator->setDualBound(pLMTerminationState.globalBestDualBoundValue);
        pLMTerminationState.externalGlobalBestDualBoundValue =
                pInitiator->convertToExternalValue(pLMTerminationState.globalBestDualBoundValue);
      }
    }
    else
    {
      // Here racing is not terminated yet and there is no force interruption or
      // time limit reached:
      // pForceInterrupt || pHardTimeLimitIsReached || pComputationIsInterrupted
      if (isRacingStage())
      {
        if (!pRacingSolverPool)
        {
          const std::string errMsg = "LoadManager - tearDown: "
                  "computation interrupted in racing stage, and the pointer to the "
                  "racing solver pool is empty";
          spdlog::error(errMsg);
          return;
        }
        // While racing, get and set the best global best dual bound value, and
        // the number of solved nodes in the best (racing) solver
        pInitiator->setNumSolvedNodes(pRacingSolverPool->getNumNodesSolvedInBestSolver());
      }
      else
      {
        // Not in racing stage anymore
        pInitiator->setNumSolvedNodes(
                std::max<uint64_t>(uint64_t{1}, pSolverPool->getTotalNodesSolved()));
      }
      pInitiator->setDualBound(pLMTerminationState.globalBestDualBoundValue);
      pLMTerminationState.externalGlobalBestDualBoundValue =
              pInitiator->convertToExternalValue(pLMTerminationState.globalBestDualBoundValue);
    }
  }

  if (pRacingRampUpWinnerParamSet)
  {
    pRacingRampUpWinnerParamSet.reset();
  }

  if (pSolverPool)
  {
    pLMTerminationState.numNodesLeftInAllSolvers =
            static_cast<uint64_t>(pSolverPool->getNumNodesInAllSolvers());

    spdlog::info("LoadManager - tearDown: "
            "number of nodes solved in all solvers " +
            std::to_string(pSolverPool->getTotalNodesSolved()));
  }

  // Set the total wall-clock time
  pLMTerminationState.runningTimeMsec = pTimer->getWallClockTimeMsec();
  pLMTerminationState.isCheckpointState = false;
  pSolverPool.reset();
  pRacingSolverPool.reset();
  pNodePool.reset();
  pNodePoolToRestart.reset();
  pNodePoolBufferToRestart.reset();
  pNodePoolBufferToGenerateCPF.reset();

  pTearDown = true;

  spdlog::info("LoadManager - tear-down process terminated");
}  // tearDown

void LoadManager::run(Node::SPtr node)
{
  /*
   * Normal ramp-up:
   * 1) LM sends the root node to one of the solvers;
   * 2) Solver(s) working on a node/subtree solve half of the subtree nodes and
   *    send the other half to the LM;
   * 3) repeat (2) until LM has at least p "good" nodes in the node pool;
   * 4) Solver(s) solve all the subtree without sending anything to LM;
   * 5) Solver(s) ask for a new node from the LM once done;
   * 5) If LM has less than p "good" nodes, go to (2).
   */

  // There should be no racing ramp-up pool yet
  assert(!pRacingSolverPool);
  if (!node)
  {
    throw std::runtime_error("LoadManager - normal racing run: empty node");
  }

  // Get the best (local) dual bound value.
  // @note this is computed as the maximum between the global best dual bound and the
  // minimum between solver and node pool best dual bounds
  const double globalBestDualBoundValueLocal = getGlobalBestDualBoundValueLocal();

  // Activate one of the solvers in the pool to send the (root) node to solve,
  // and send it the (root) node to solve
  const int activatedSolverId = pSolverPool->activateSolver(
          node,
          pRacingSolverPool,
          (pRunningPhase == RunningPhase::LMRP_RAMPUP),
          pNodePool->getNumOfGoodNodes(globalBestDualBoundValueLocal),
          pAverageLastSeveralDualBoundGains);

  spdlog::info("LoadManager - normal racing run: "
          "activating worker/solver with id " + std::to_string(activatedSolverId));

  // Increase the number of nodes sent to a solver in the solver pool
  pLMTerminationState.numNodesSent++;

  // Ramp-up should be normal, if so and the LM has to collect all open nodes
  // after ramp-up, send the "collect all nodes" message to the activated solver
  const auto rampUpProcess = getParams().rampupphaseprocess();
  const auto collectAllNodesAfterRampUp = getParams().collectonce();
  if (rampUpProcess == kRampUpPhaseNormal && collectAllNodesAfterRampUp)
  {
    // Send the collect nodes message to the activated solver from the solver pool
    const int numNodes{-1};
    utils::sendDataMessageToNetworkNode<int>(
            pFramework,
            net::MetaBBPacketTag::PacketTagType::PPT_COLLECT_ALL_NODES,
            activatedSolverId,
            numNodes);
  }

  if (!getParams().quiet())
  {
    std::stringstream ss;
    ss << "LoadManager - normal racing run:\n" <<
            "\tTime (msec.)" << pTimer->getWallClockTimeMsec() << "\n" <<
            "\tActivated solver (rank)" << activatedSolverId << "\n" <<
            "\tDual bound value " <<
            pInitiator->convertToExternalValue(node->getDualBoundValue()) << "\n";
    if (pInitiator->getGlobalBestIncumbentSolution())
    {
      ss << "\tGap " << pInitiator->getGap(node->getDualBoundValue()) * 100 << "%\n";
    }
    spdlog::info(ss.str());
  }

  spdlog::info("LoadManager - normal racing run: "
        "sent node to worker/solver " + std::to_string(activatedSolverId));
  LOG_TIME("LoadManager (normal ramp-up)");

  // Call the actual run method,
  // this will start the solving process
  run();
}  // run

void LoadManager::run(Node::SPtr node,
                      int numRacingSolvers,
                      std::vector<RacingRampUpParamSet::SPtr>& racingRampupParams)
{
  /*
   * Racing ramp-up:
   * 1) LM sends the root node to all solvers;
   * 2) Solvers start solving the root node of the pre-solved instance.
   *    This is done using different racing parameters to explore different parts of
   *    the search tree on each solver;
   * 3) The "winning" solver (winning because of its duality gap and number of open nodes)
   *    sends all of its open nodes to the LM;
   * 4) LM sends a termination message to all other solvers;
   * 5) LM distributes the collected nodes to all idle solvers.
   *    If there are less than p "good" nodes, LM changes strategy to normal ramp-up until it
   *    collects at least p "good" nodes.
   */

  // Create a racing solver pool
  pRacingSolverPool = std::make_shared<RacingSolverPool>(static_cast<int>(kSolverOriginalRank),
                                                         pFramework,
                                                         pParamSet,
                                                         pTimer);

  // Broadcast the (root) node to all the solvers/workers.
  // This activates racing solvers starting from root node
  spdlog::info("LoadManager - racing ramp-up run: broadcast root node to all solvers");
  ASSERT_COMM_CALL(node->broadcast(pFramework, kLoadManagerRank));

  // Activate the racing solver pool on the given (root) node
  pRacingSolverPool->activate(node);

  spdlog::info("LoadManager - racing ramp-up run: activated solver racing pool");
  LOG_TIME("LoadManager (racing ramp-up)");

  // Increase the number of nodes sent to a solver in the solver pool
  pLMTerminationState.numNodesSent++;

  if (!getParams().quiet())
  {
    std::stringstream ss;
    ss << "LoadManager - racing ramp-up run: solvers started racing with dual bound value " <<
            node->getDualBoundValue();

    spdlog::info(ss.str());
    LOG_TIME("LoadManager (racing ramp-up)");
  }

  while (true)
  {
    waitForAndExecuteIncomingMessage(pLoadManagerRacingRampUpImpl);

    // Break loop on timeout
    if (pHardTimeLimitIsReached) break;

    // If racing ramp-up needs to be restarted
    if (pRestartRacing)
    {
      // If there are no active solvers in the solver pool and restart racing returns
      // a non-zero value (e.g., error), then return since it is solved
      if (pRacingSolverPool->getNumActiveSolvers() == 0 && restartRacing()) return;

      // Otherwise continue with next loop iteration since the racing is restarted
      continue;
    }

    // Check the running phase of the load manager and
    // take appropriate actions
    switch (pRunningPhase)
    {
      case RunningPhase::LMRP_RAMPUP:
      {
        // If there is no racing solver pool, activate the standard solver pool,
         // run solving the node, and return
         if (!pRacingSolverPool)
         {
           // Activate the solver pool
           pSolverPool->activate();

           // Run standard, distributed branch and bound
           run();

           // Done, return
           return;
         }

         // Racing is terminating.
         // Run standard branch and bound or break this loop
         if (pRacingTermination)
         {
           // If there are no active solvers in the racing solver pool,
           // run standard branch and bound
           if (pRacingSolverPool->getNumActiveSolvers() == 0)
           {
             pRacingSolverPool.reset();

             // Run standard, distributed branch and bound
             run();

             // Done, return
             return;
           }

           // There are active solvers in the racing solver pool.
           // Send interrupt to all solvers since racing ramp-up is terminated
           sendInterruptRequest();

           spdlog::info("LoadManager - racing ramp-up run: "
                         "racing terminated, sent interrupt request to solvers");
           LOG_TIME("LoadManager (racing ramp-up)");

           // Break the loop
           break;
         }

         // Check for hard timeout limit
         auto timeoutMsec = getParams().timelimitmsec();
         if (timeoutMsec > 0 && pTimer->getWallClockTimeMsec() > timeoutMsec)
         {
           // If the timeout is reached, send interrupt request to all solvers/workers
           spdlog::info("LoadManager - racing ramp-up run: "
               "timeout reached, break racing ramp-up");
           LOG_TIME("LoadManager (racing ramp-up)");

           pHardTimeLimitIsReached = true;
           sendInterruptRequest();

           // Done, break the loop
           break;
         }

         // Check if there is a "winner" node on a feasible solution,
         // i.e., a solution where +INF - obj > epsilon
         auto bestSol = pInitiator->getGlobalBestIncumbentSolution();
         const bool feasibleSol = bestSol &&
             EPSLT(bestSol->getObjectiveFuntionValue(), metaconst::MAX_DOUBLE,
                   metaconst::DEAFULT_NUM_EPSILON);
         if (pRacingSolverPool->isWinnerDecided(feasibleSol))
         {
           // There is a winner node with a feasible solution.
           // Check if the racing ramp-up needs to be restarted when the primal solution
           // is updated during rmap-up.
           // @note check first if the primal solution has been updated during racing
           if (pPrimalSolutionUpdated && getParams().restartracing())
           {
             // If racing ramp-up is not restarted yet (double check here), restart it
             if (!pRestartRacing)
             {
               // Send a terminate-to-restart tag to all solver/workers (except the worker 0)
               for(int workerIdx = 1; workerIdx < pCommunicator->getSize(); ++workerIdx)
               {
                 utils::sendTagMessageToNetworkNode(
                         pFramework,
                         net::MetaBBPacketTag::PacketTagType::PPT_TERMINATE_SOLVING_TO_RESTART,
                         workerIdx);
               }

               // Restart racing ramp-up is now true
               pRestartRacing = true;
             }
           }
           else
           {
             // Do not restart racing ramp-up but the winner is decided.
             // Get the winner of the racing ramp-up
             pRacingWinner = pRacingSolverPool->getWinner();
             assert(pRacingWinner > 0);

             spdlog::info("LoadManager - racing ramp-up run: "
                 "found winner of racing ramp up " + std::to_string(pRacingWinner));

             // Activate the solver (from the solver pool, not the racing pool) corresponding
             // to the winning solver and activate it on the root node used in the racing phase
             const int numNodesLeftInWinnerNode =
                 pRacingSolverPool->getNumNodesLeftInSolver(pRacingWinner);
             pSolverPool->activateSolver(pRacingWinner,
                                         pRacingSolverPool->extractRacingRootNode(),
                                         numNodesLeftInWinnerNode);

             // Deactivate the same winner solver from the racing pool (since now it is active
             // in the standard solver's pool)
             pRacingSolverPool->deactivateSolver(pRacingWinner);
             assert(pRacingSolverPool->getNumActiveSolvers() >= 0);

             // Communicate the winner solver (now active in the solver's pool) that it is
             // the winner solver
             utils::sendTagMessageToNetworkNode(
                     pFramework,
                     net::MetaBBPacketTag::PacketTagType::PPT_WINNER,
                     pRacingWinner);


             // Get the number of nodes left in a solver to stop racing.
             // If there are enough nodes to stop racing, set corresponding flags,
             // and switch to ramp-up process
             const int numNodesLeftToStopRacing = getParams().stopracingnumnodesleft();
             const double numNodesLeftToStopRacingMult = getParams().stopracingnumnodesleftmult();
             if (static_cast<double>(numNodesLeftInWinnerNode) >
             (kNumNodesLeftToStopRacingMultInternal *
             numNodesLeftToStopRacing *
             numNodesLeftToStopRacingMult) ||
             numNodesLeftInWinnerNode <= 0)
             {
               getParams().set_collectonce(false);
               getParams().set_mergenodesatrestart(false);
               getParams().set_racingstatbranching(false);
               getParams().set_rampupphaseprocess(kRampUpPhaseRacing);

               // All winner solver nodes are (will be) collected
               pWinnerSolverNodesCollected = true;
               spdlog::info("LoadManager - racing ramp-up run: "
                   "ramp-up phase switched to racing");
             }

             // If the number of nodes in the winner solver is greater than
             // the number of solvers in the solver pool check
             // whether or not to collect open nodes
             const int numSolvers = static_cast<int>(pSolverPool->getNumSolvers());
             if (numNodesLeftInWinnerNode > numSolvers)
             {
               if (getParams().collectonce())
               {
                 int numCollect = std::max(getParams().numcollectonce(),
                                           numSolvers * kDefaultNumCollectOnceMult);

                 // Send collect nodes message to the winner node,
                 // to send "numCollect" nodes to the LM and collect them
                 utils::sendDataMessageToNetworkNode<int>(
                         pFramework,
                         net::MetaBBPacketTag::PacketTagType::PPT_COLLECT_ALL_NODES,
                         pRacingWinner,
                         numCollect);

                 // Check if the tree needs to be rebuild after ramp-up phase,
                 // if so, merge nodes structs
                 if (getParams().rampupphaseprocess() == kRampUpPhaseRebuildTreeAfterRacing)
                 {
                   // Set merging flag in process
                   pMerging = true;

                   // Initialize the merge nodes process to rebuild the tree
                   pLoadManagerMergeImpl->initMergeNodesStructs();
                 }
               }
             }
             else
             {
               // Winner solver nodes are not collected
               pWinnerSolverNodesCollected = false;
             }

             // Set the racing ramp-up parameters of the winner solver,
             // and reset the parameters in the list given as argument to this run
             pRacingRampUpWinnerParamSet = racingRampupParams[pRacingWinner - 1];
             pRacingRampUpWinnerParamSet->setWinnerRank(pRacingWinner);
             racingRampupParams[pRacingWinner - 1].reset();

             // Communicate the winner parameters to all the other solvers
             for(int workerIdx = 1; workerIdx < pCommunicator->getSize(); ++workerIdx)
             {
               if (racingRampupParams[workerIdx - 1])
               {
                 ASSERT_COMM_CALL(pRacingRampUpWinnerParamSet->send(pFramework, workerIdx));
               }
             }

             // Run the branch and bound process.
             // @note keep running as ramp-up phase, since in the "run(...)"
             // it will be switched normal mode
             run();

             // Done, return from the run
             return;
           }
         }
         break;
      }
      default:
      {
        // Every other phase that is not ramp-up, it is not valid
        const std::string errMsg = "LoadManager - run: "
            "invalid running phase for racing ramp-up " +
            std::to_string(static_cast<int>(pRunningPhase));
        spdlog::error(errMsg);
        throw std::runtime_error(errMsg);
      }
    }
  }  // while
}  // run

void LoadManager::run()
{
  // Start communication loop
  while (true)
  {
    // Check if there are active solvers in the solver pool.
    // If not, the solving process may be terminated
    if (pSolverPool->getNumActiveSolvers()  == 0)
    {
      // Check if the Node pool is empty and, if so, the running phase
      if (pNodePool->isEmpty())
      {
        if (pRunningPhase != RunningPhase::LMRP_TERMINATION)
        {
          // There are no active solvers and the node pool is empty,
          // but the phase is not termination yet.
          // Set termination phase
          spdlog::info("LoadManager - run: terminate phase started");
          LOG_TIME("LoadManager (run)");

          // No active solver exists
          terminateSolvers();
          pRunningPhase = RunningPhase::LMRP_TERMINATION;
        }
        else
        {
          // Phase is termination, check if all the solvers in the pool,
          // i.e., racing and standard pool, are terminated
          const auto numSolvers = pSolverPool->getNumSolvers();
          if ((pRacingSolverPool &&
                  (numSolvers == static_cast<int>(pRacingSolverPool->getNumActiveSolvers() +
                                                  pNumTerminated))) ||
              numSolvers == pNumTerminated)
          {
            // Nothing to do, everything is terminated, break
            break;
          }
        }
      }
      else
      {
        // Node pool is not empty (but there are no active solvers).
        // There is a solution already found

        // Check if the initial nodes have been generated
        if (pInitialNodesGenerated)
        {
          if (pRunningPhase != RunningPhase::LMRP_TERMINATION)
          {
            // Update global best dual bound
            updateGlobalBestDualBoundWithNodePool();

            // Terminate all solvers
            terminateSolvers();
            pRunningPhase = RunningPhase::LMRP_TERMINATION;
          }
          else
          {
            // Phase is already termination.
            // If the number of terminated solvers is the number of solver in the pool,
            // done, break the loop
            if (pSolverPool->getNumSolvers() == pNumTerminated) break;
          }
        }
      } // node pool check
    }  // no active solvers

    // If it is not in termination phase already, check the number of idle solvers
    // required for termination.
    // @note if the phase is already in termination, keep running
    const int numIdleSolvers = getParams().numidlesolverstoterminate();
    if (numIdleSolvers > 0 &&
        pFirstCollectingModeState == kFirstCollectingModeTerminatedOnce &&
        static_cast<int>(pSolverPool->getNumInactiveSolvers()) >= numIdleSolvers &&
        pRunningPhase != RunningPhase::LMRP_TERMINATION)
    {
      // Update global best dual bound
      updateGlobalBestDualBoundWithNodePool();

      // Terminate solvers
      spdlog::info("LoadManager - run: "
          "reached the maximum number of inactive solvers to terminate search, exit");
      LOG_TIME("LoadManager (run)");
      terminateSolvers();

      // Return asap
      return;
    }

    // Check whether or not to break the loop:
    // - timeout
    // @note check on timeout only if its not racing and all solvers are deactivated
    if (!pRacingSolverPool && pSolverPool->getNumActiveSolvers() == 0 && timeout())
    {
      pHardTimeLimitIsReached = true;
      break;
    }

    // Check whether or not to break the loop:
    // - racing terminated
    // - no racing solver pool
    // - no active solvers in the solver pool
    // - one single node in the node pool
    // @note these checks ensure that the loop doesn't break during racing
    if (!pRacingSolverPool &&
        pRacingTermination &&
        pSolverPool->getNumActiveSolvers() == 0 &&
        pNodePool->getNumNodes() == 1)
    {
      break;
    }

    // Wait for any incoming message from any network node
    waitForAndExecuteIncomingMessage(pLoadManagerImpl);

    // Completion message may delay
    if (pRacingSolverPool && pRacingSolverPool->getNumActiveSolvers() == 0)
    {
      // Here is where the racing solver pool gets reset to indicate
      // completion of racing phase
      pRacingSolverPool.reset();
      if (pRacingTermination) break;
    }

    // Check the running phase and take the correspondent action
    switch (pRunningPhase)
    {
      case RunningPhase::LMRP_RAMPUP:
      {
        // Phase is in ramp-up.
        // Check for racing and its termination
        if (pRacingTermination || timeout())
        {
          // Send interrupt request to solvers
          sendInterruptRequest();

          // Switch phase to termination
          pRunningPhase = RunningPhase::LMRP_TERMINATION;
        }
        else
        {
          // Get the best (local) dual bound value and the corresponding number of good nodes
          const auto globalBestDualBoundValueLocal = getGlobalBestDualBoundValueLocal();
          const auto numToSwith = getParams().numnodestoswitchtocollectingmode();
          auto numToSwithMul = getParams().multiplierforcollectingmode();
          const auto numGoodNodes = pNodePool->getNumOfGoodNodes(globalBestDualBoundValueLocal);

          // No inactive solvers in the solver pool
          if (pSolverPool->getNumInactiveSolvers() == 0)
          {
            // Check if there are enough nodes in the node pool so it is possible
            // to switch out from collecting mode and keep doing racing
            // @note check also if there are double number of nodes in general even if they
            // are not strictly all good nodes
            if (getParams().dualboundgaintest()) numToSwithMul = 1.0;
            if (pSolverPool->isActive() &&
               ((numGoodNodes > numToSwith * numToSwithMul) ||
                (pNodePool->getNumNodes() > numToSwith * numToSwithMul * 2 && numGoodNodes  > 0)))
            {
              // Switch to ramp-up from collecting mode
              sendRampUpToAllSolvers();

              // Next phase, after ramp-up, is normal running
              pRunningPhase = RunningPhase::LMRP_RUNNING;
            }
          }
          else
          {
            // Some inactive solvers in ramp-up
            const int rampUpPhaseProcess = getParams().rampupphaseprocess();

            // Switch to normal running if:
            // - he winner nodes are collected
            // - LM is in racing ramp-up
            if (pWinnerSolverNodesCollected &&
               (rampUpPhaseProcess == kRampUpPhaseRacing ||
                rampUpPhaseProcess == kRampUpPhaseRebuildTreeAfterRacing))
            {
              if (pSolverPool->isActive() && (
                  (static_cast<double>(numGoodNodes) > numToSwith * numToSwithMul) ||
                  (pNodePool->getNumNodes() > numToSwith * numToSwithMul * 2 && numGoodNodes  > 0)))
              {
                // Switch to ramp-up from collecting mode
                sendRampUpToAllSolvers();

                // Next phase is normal
                pRunningPhase = RunningPhase::LMRP_RUNNING;
              }
            }
          }

          // Send nodes to solvers
          sendNodesToIdleSolvers();
        }
        break;
      }
      case RunningPhase::LMRP_RUNNING:
      {
        // Phase is normal running.
        // Check if racing phase needs to be terminated
        if (pRacingTermination || timeout())
        {
          // Interrupt all solvers/workers
          sendInterruptRequest();

          // Prepare for next phase which is termination
          pRunningPhase = RunningPhase::LMRP_TERMINATION;
        }
        else
        {
          // No termination, send nodes to idle solvers
          sendNodesToIdleSolvers();
        }

        // Switch back to ramp-up phase if:
        // - collecting mode is restarted
        // - the node pool is empty
        // - there is no racing solver pool or the racing solver pool has a winner
        if (pIsCollectingModeRestarted &&
            pNodePool->isEmpty() &&
            (!pRacingSolverPool || (pRacingSolverPool->getWinner() > 0)))
        {
          // If the node pool has been empty for too long, restart ramp-up.
          // @note the current phase is normal
          const auto timestamp = pTimer->getWallClockTimeMsec();
          if ((timestamp - pEmptyNodePoolTimeMsec) >
              (getParams().timetoincreasecmsmsec() * 2))
          {
            sendRetryRampUpToAllSolvers();
            pRunningPhase = RunningPhase::LMRP_RAMPUP;
          }
        }

        // TODO add the huge imbalance in threshold time check here
        break;
      }
      case RunningPhase::LMRP_TERMINATION:
      {
        // Termination, nothing to do, break out of the loop
        break;
      }
      default:
      {
        const std::string errMsg = "LoadManager - run: "
            "undefined running phase " + std::to_string(static_cast<int>(pRunningPhase));
        spdlog::error(errMsg);
        throw std::runtime_error(errMsg);
      }
    }  // switch
  }  // while
}  // run

void LoadManager::sendRampUpToAllSolvers()
{
  for(int workerIdx = 1; workerIdx < pCommunicator->getSize(); ++workerIdx)
  {
    utils::sendTagMessageToNetworkNode(
            pFramework,
            net::MetaBBPacketTag::PacketTagType::PPT_RAMP_UP,
            workerIdx);
  }
}  // sendRampUpToAllSolvers

void LoadManager::sendRetryRampUpToAllSolvers()
{
  for(int workerIdx = 1; workerIdx < pCommunicator->getSize(); ++workerIdx)
  {
    utils::sendTagMessageToNetworkNode(
            pFramework,
            net::MetaBBPacketTag::PacketTagType::PPT_RETRY_RAMP_UP,
            workerIdx);
  }
}  // sendRampUpToAllSolvers

void LoadManager::sendInterruptRequest()
{
  // Return asap if interrupt is requested already
  if (pInterruptIsRequested) return;

  // Communicate interrupt request to all solvers
  int exitSolverRequest = 0;
  for(int workerIdx = 1; workerIdx < pCommunicator->getSize(); ++workerIdx)
  {
    utils::sendDataMessageToNetworkNode<int>(
            pFramework,
            net::MetaBBPacketTag::PacketTagType::PPT_INTERRUPT_REQUEST,
            workerIdx,
            exitSolverRequest);
  }

  pInterruptIsRequested = true;
}  // sendInterruptRequest

void LoadManager::terminateAllSolvers()
{
  for(int workerIdx = 1; workerIdx < pCommunicator->getSize(); ++workerIdx)
  {
    utils::sendTagMessageToNetworkNode(
            pFramework,
            net::MetaBBPacketTag::PacketTagType::PPT_TERMINATE_REQUEST,
            workerIdx);
  }
}  // terminateAllSolvers

bool LoadManager::updateSolution(const Solution::SPtr& sol)
{
  assert(sol);
  if (!pInitiator->getGlobalBestIncumbentSolution())
  {
    // No global incumbent solution yet
    return pInitiator->tryToSetIncumbentSolution(sol, false);
  }

  // There is a global incumbent, check if if the soution is better than
  // the global incumbent
  if (sol->getObjectiveFuntionValue() <
          pInitiator->getGlobalBestIncumbentSolution()->getObjectiveFuntionValue())
  {
    return pInitiator->tryToSetIncumbentSolution(sol, false);
  }

  return false;
}  // updateSolution

void LoadManager::sendIncumbentValue(int sourceRank)
{
  const double glbBestIncumbent =
          pInitiator->getGlobalBestIncumbentSolution()->getObjectiveFuntionValue();
  if (!getParams().noupperboundtransferinracing() || !isRacingStage())
  {
    // Transfer incumbent if not in racing stage
    for(int workerIdx = 1; workerIdx < pCommunicator->getSize(); ++workerIdx)
    {
      if (workerIdx == sourceRank) continue;
      utils::sendDataMessageToNetworkNode<double>(
              pFramework,
              net::MetaBBPacketTag::PacketTagType::PPT_INCUMBENT_VALUE,
              workerIdx,
              glbBestIncumbent);
    }
  }

  // Branch nodes whose lower bound is greater than the best incumbent
  const auto numRemovedNodes = pNodePool->removeBoundedNodes(glbBestIncumbent);
  pLMTerminationState.numNodesDeletedInLM += static_cast<uint64_t>(numRemovedNodes);

  if (!getParams().quiet())
  {
    std::stringstream ss;
    ss << "LoadManager - sendIncumbentValue: incumbent value " << glbBestIncumbent <<
            " removed " << numRemovedNodes << " from the node pool";
    spdlog::info(ss.str());
    std::cout << ss.str() << std::endl;
  }
}  // sendIncumbentValue

void LoadManager::sendCutOffValue(int sourceRank)
{
  const double glbBestCutOff =
          pInitiator->getGlobalBestIncumbentSolution()->getCutOffValue();
  if (!getParams().noupperboundtransferinracing() || !isRacingStage())
  {
    // Transfer incumbent if not in racing stage
    for(int workerIdx = 1; workerIdx < pCommunicator->getSize(); ++workerIdx)
    {
      if (workerIdx == sourceRank) continue;
      utils::sendDataMessageToNetworkNode<double>(
              pFramework,
              net::MetaBBPacketTag::PacketTagType::PPT_CUTOFF_VALUE,
              workerIdx,
              glbBestCutOff);
    }
  }

  // Branch nodes whose lower bound is greater than the cutoff
  const auto numRemovedNodes = pNodePool->removeBoundedNodes(glbBestCutOff);
  pLMTerminationState.numNodesDeletedInLM += static_cast<uint64_t>(numRemovedNodes);

  if (!getParams().quiet())
  {
    std::stringstream ss;
    ss << "LoadManager - sendCutOffValue: cutoff value " << glbBestCutOff <<
            " removed " << numRemovedNodes << " from the node pool";
    spdlog::info(ss.str());
    std::cout << ss.str() << std::endl;
  }
}  // sendCutOffValue

void LoadManager::deactivateRacingSolverPool(int rank)
{
  pNumNodesSolvedInInterruptedRacingSolvers = pRacingSolverPool->getNumNodesSolvedInBestSolver();
  pNumNodesLeftInInterruptedRacingSolvers = pRacingSolverPool->getNumNodesLeftInBestSolver();
  if (pRacingSolverPool->isActive(rank))
  {
    pRacingSolverPool->deactivateSolver(rank);
  }

  // Check if the solver is active in the solver pool
  if (pSolverPool->isSolverActive(rank))
  {
    // Solver is active, check if it is in collecting mode
    if (pSolverPool->isSolverInCollectingMode(rank))
    {
      pSolverPool->deactivateSolver(rank, 0, pNodePool);
      if (!pNodePoolBufferToRestart)
      {
        auto tempTime = pSolverPool->getSwichOutTime();
        pSolverPool->switchOutCollectingMode();
        pSolverPool->setSwichOutTime(tempTime);
        pSolverPool->switchIntoCollectingMode(pNodePool);
      }
    }
    else
    {
      // Solver not in collecting mode, simply deactivate it
      pSolverPool->deactivateSolver(rank, 0, pNodePool);
    }
  }

  // Done with racing solver pool (reset it) if:
  // - interrupt is not requested
  // - racing doesn't need to be restarted
  // - there are no active solvers in the racing pool
  if ((!pInterruptIsRequested) && (!pRestartRacing) &&
      pRacingSolverPool->getNumActiveSolvers() == 0)
  {
    pRacingSolverPool.reset();
  }
}  // deactivateRacingSolverPool

int LoadManager::restartRacing()
{
  if (pInitiator->reInit(pNumRestartRacing))
  {
    // Too many restarts already, return
    pRestartRacing = false;
    return 1;
  }

  // Broadcast the original problem to the nodes in the network
  auto instance = pInitiator->getInstance();
  instance->broadcast(pFramework, kLoadManagerRank);

  // Get the root node from the racing solver pool,
  // and set default initial values for dual bound (neg. inf)
  auto rootNode = pRacingSolverPool->extractNode();
  rootNode->setDualBoundValue(metaconst::DOUBLE_NEG_INF);
  rootNode->setInitialDualBoundValue(metaconst::DOUBLE_NEG_INF);
  rootNode->setEstimatedValue(metaconst::DOUBLE_NEG_INF);

  // Re-create racing solver pool
  const int origRank{1};
  pRacingSolverPool = std::make_shared<RacingSolverPool>(origRank, pFramework, pParamSet, pTimer);

  // Activate racing solver with root node:
  // 1) broadcast the root node to all solvers;
  // 2) activate the pool
  ASSERT_COMM_CALL(rootNode->broadcast(pFramework, kLoadManagerRank));
  pRacingSolverPool->activate(rootNode);
  pLMTerminationState.numNodesSent++;

  if (getParams().quiet())
  {
    std::stringstream ss;
    ss << "LoadManager - restartRacing:: racing restarted " << pNumRestartRacing <<
            " times at time (msec)" <<
            pTimer->getWallClockTimeMsec();
    spdlog::info(ss.str());
    std::cout << ss.str() << std::endl;
  }

  // Reset the valuea for primal solution and racing flag
  pPrimalSolutionUpdated = false;
  pRestartRacing = false;

  // Reset the global dual values in the load manager's state
  pLMTerminationState.globalBestDualBoundValue = metaconst::DOUBLE_NEG_INF;
  pLMTerminationState.externalGlobalBestDualBoundValue = metaconst::DOUBLE_NEG_INF;

  // Return no error
  return 0;
}  // restartRacing

void LoadManager::changeSearchStrategyOfAllSolversToBestBoundSearch()
{
  // TODO
}  //changeSearchStrategyOfAllSolversToBestBoundSearch

void LoadManager::changeSearchStrategyOfAllSolversToOriginalSearch()
{
  // TODO
}  //changeSearchStrategyOfAllSolversToOriginalSearch

bool LoadManager::sendNodesToIdleSolvers()
{
  // Check if there is no need to send nodes, i.e., if:
  // 1) the load manager is merging nodes (do not send nodes while merging them!); or
  // 2) initial nodes are generated; or
  // 3) the load manager is terminating; or
  // 4) the winner solver nodes are not yet collected or the number of inactive solvers
  //    is lower than the number of active solvers in the racing solver pool
  if (pMerging || pInitialNodesGenerated ||
          pRunningPhase == RunningPhase::LMRP_TERMINATION ||
          pHugeImbalance ||
          (!pRestartedFromCheckpointFile &&
                  (!pWinnerSolverNodesCollected ||
                          (pRacingSolverPool && pRacingSolverPool->getNumInactiveSolvers() <
                                  pRacingSolverPool->getNumActiveSolvers()))))
  {
    return false;
  }

  // Loop until there are inactive solvers and there is at least one node
  // in the node pool.
  // In each loop, find a node to send and send it
  bool sentNode = false;
  while (pSolverPool->getNumInactiveSolvers() > 0 && !(pNodePool->isEmpty()))
  {
    // Loop until there is a node in the node pool
    Node::SPtr node;
    while (!pNodePool->isEmpty())
    {
      // Extract a node (either randomly or the top of the heap)
      // @note a negative values means that no random selection is performed (default)
      if (pNumNodesNormalSelection >= 0 && !pWarmStartNodeTransferring)
      {
        const bool extractNodeAtRandom = pNumNodesNormalSelection >
            static_cast<int>(1.0 / getParams().randomnodeselectionratio());
        node = extractNodeAtRandom ? pNodePool->extractNodeRandomly() : pNodePool->extractNode();
      }
      else
      {
        // Do not extract random nodes
        node = pNodePool->extractNode();
      }

      // No node available, break the loop
      if (node == nullptr) break;

      // Assert the the current selected node is not merged or, if merged,
      // if is the representative node, not one merged into another
      assert(!(node->getMergeNodeInfo()) ||
             (node->getMergeNodeInfo() &&
                     node->getMergeNodeInfo()->status ==
                             mergenodes::MergeNodeInfo::Status::STATUS_MERGED_REPRESENTATIVE &&
                             (node->getMergeNodeInfo()->mergedTo == nullptr)));
      if (getParams().generatereducedcheckpointfiles() &&
          ((!node->getMergeNodeInfo()) ||
           (node->getMergeNodeInfo() && node->getMergeNodeInfo()->numMergedNodes == 0)))
      {
        if (node->getMergeNodeInfo())
        {
          auto mnode = node->getMergeNodeInfo();
          if (mnode->origDiffSubproblem)
          {
            node->setDiffSubproblem(mnode->origDiffSubproblem);
            mnode->mergedDiffSubproblem = nullptr;
            mnode->mergedDiffSubproblem = nullptr;
            mnode->origDiffSubproblem = nullptr;
          }
          node->setMergeNodeInfo(0);
          node->setMergingStatus(Node::NodeMergingStatus::NMS_NO_MERGING_NODE);
          pLoadManagerMergeImpl->deleteMergeNodeInfo(mnode.get());
        }

        pNodePoolBufferToGenerateCPF->insert(node);
        node = nullptr;
        continue;
      }

      // Check if the current node is a candidate node to be sent to an idle solver.
      // The check is performed considering the node's dual bound w.r.t.
      // the current global best incumbent solution
      const auto glbIncumbentSolVal = (pInitiator->getGlobalBestIncumbentSolution() != nullptr) ?
              pInitiator->getGlobalBestIncumbentSolution()->getObjectiveFuntionValue() :
              metaconst::DOUBLE_NEG_INF;
      const auto nodeDualBound = node->getDualBoundValue();
      if (!pInitiator->getGlobalBestIncumbentSolution() ||
              (pInitiator->getGlobalBestIncumbentSolution() &&
                      (nodeDualBound < glbIncumbentSolVal ||
                              (pInitiator->isObjIntegral() &&
                                      static_cast<int>(std::ceil(nodeDualBound)) <
                                      static_cast<int>(glbIncumbentSolVal)))))
      {
        // The node has a potential valid dual bound
        // i.e., lower than the current incumbent solution
        if (pInitiator->getAbsGap(nodeDualBound) > pInitiator->getAbsGapValue() ||
            pInitiator->getGap(nodeDualBound) > pInitiator->getGapValue())
        {
          // The gap is greater than the default gap value,
          // the node is a candidate node to be sent
          break;
        }
        else
        {
          // The node is not a candidate node to be sent,
          // delete it here
          node = nullptr;
          pLMTerminationState.numNodesDeletedInLM++;

          // Check whether or not to switch node selection strategy
          if (pNumNodesNormalSelection >= 0 && !pWarmStartNodeTransferring)
          {
            const bool extractNodeAtRandom = pNumNodesNormalSelection >
                static_cast<int>(1.0 / getParams().randomnodeselectionratio());
            pNumNodesNormalSelection = extractNodeAtRandom ? 0 : (pNumNodesNormalSelection + 1);
          }
        }
      }
      else
      {
        // Not a candidate node, just delete it
        node = nullptr;
        pLMTerminationState.numNodesDeletedInLM++;
      }
    }  // while node pool is not empty

    if (node)
    {
      // Here there is candidate node to be sent.
      // Switch node selection strategy if needed
      if (pNumNodesNormalSelection >= 0 && !pWarmStartNodeTransferring)
      {
        const bool extractNodeAtRandom = pNumNodesNormalSelection >
            static_cast<int>(1.0 / getParams().randomnodeselectionratio());
        pNumNodesNormalSelection = extractNodeAtRandom ? 0 : (pNumNodesNormalSelection + 1);
      }

      // If the node has some MergeNodeInfo and it doesn't have merged nodes,
      // delete all the MergeNodeInfo information
      if (node->getMergeNodeInfo() && (node->getMergeNodeInfo()->numMergedNodes == 0))
      {
         auto mnode = node->getMergeNodeInfo();
         node->setDiffSubproblem(mnode->origDiffSubproblem);
         node->setMergeNodeInfo(nullptr);
         node->setMergingStatus(Node::NodeMergingStatus::NMS_NO_MERGING_NODE);
         mnode->mergedDiffSubproblem.reset();
         mnode->origDiffSubproblem = nullptr;
         pLoadManagerMergeImpl->deleteMergeNodeInfo(mnode.get());
      }

      // Check for racing stat. branching, if parent is the root node
      if (getParams().racingstatbranching() &&
              node->isSameParentNodeSubtreeId(NodeId()) &&
              node->getDiffSubproblem())
      {
        pInitiator->setInitialStatOnDiffSubproblem(
                pMinDepthInWinnerSolverNodes, pMaxDepthInWinnerSolverNodes,
                node->getDiffSubproblem());
      }

      // Calculate the number of good nodes w.r.t. the best dual bound value
      // of the selected node and activate a solver in the racing solver pool
      // on the selected node
      double glbBestDualBoundValueLocal = std::max(
              std::min(pSolverPool->getGlobalBestDualBoundValue(),
                       pNodePool->getBestDualBoundValue()),
              pLMTerminationState.globalBestDualBoundValue);
      int destination = pSolverPool->activateSolver(
              node,
              pRacingSolverPool,
              (pRunningPhase == RunningPhase::LMRP_RAMPUP),
              pNodePool->getNumOfGoodNodes(glbBestDualBoundValueLocal),
              pAverageLastSeveralDualBoundGains);

      if (destination < 0)
      {
        // Cannot activate, re-insert the node into the node pool
        pNodePool->insert(node);
        return sentNode;
      }
      else
      {
        // Send the node to the destination
        pLMTerminationState.numNodesSent++;
        sentNode = true;
        if (pRunningPhase == RunningPhase::LMRP_RAMPUP &&
                getParams().rampupphaseprocess() == kRampUpPhaseNormal &&
                getParams().collectonce() &&
                pSolverPool->getNumInactiveSolvers() > 0 &&
                pSolverPool->getNumActiveSolvers() * 2 < (pSolverPool->getNumSolvers() +
                        getParams().numnodestoswitchtocollectingmode()))
        {
          int numCollect = -1;
          utils::sendDataMessageToNetworkNode<int>(
                  pFramework,
                  net::MetaBBPacketTag::PacketTagType::PPT_COLLECT_ALL_NODES,
                  destination,
                  numCollect);
        }

        if (!getParams().quiet())
        {
          std::stringstream ss;
          ss << "LoadManager - sendNodesToIdleSolvers: send node to solver " <<
                  destination << " at time (msec.) " << pTimer->getWallClockTimeMsec() <<
                  " dual bound " << pInitiator->convertToExternalValue(node->getDualBoundValue());
          spdlog::info(ss.str());
          std::cout << ss.str() << std::endl;
        }
      }
    }
    else
    {
      break;
    }
  }  // while there are active solvers and node pool is not empty

  return sentNode;
}  // sendNodesToIdleSolvers

void LoadManager::sendTagToAllSolvers(const net::MetaBBPacketTag::PacketTagType tag)
{
  for (int idx = 1; idx < pCommunicator->getSize(); ++idx)
  {
    utils::sendTagMessageToNetworkNode(pFramework, tag, idx);
  }
}  // sendTagToAllSolvers

void LoadManager::warmStart()
{
  throw std::runtime_error("LoadManager - warmStart: not supported");
}  // warmStart

void LoadManager::updateGlobalBestDualBoundWithNodePool()
{
  // Update global best dual bound
  pLMTerminationState.globalBestDualBoundValue =
      std::max(pNodePool->getBestDualBoundValue(), pLMTerminationState.globalBestDualBoundValue);

  // Update the external global best dual bound
  pLMTerminationState.externalGlobalBestDualBoundValue =
          pInitiator->convertToExternalValue(pLMTerminationState.globalBestDualBoundValue);
}  // updateGlobalBestDualBoundWithNodePool

}  // namespace metabb
}  // namespace optilab
