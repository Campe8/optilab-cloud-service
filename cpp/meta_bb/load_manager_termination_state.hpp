//
// Copyright OptiLab 2019-2020. All rights reserved.
//
// Load manager termination state.
//

#pragma once

#include <cstddef>  // for std::size_t
#include <cstdint>  // for uint64_t
#include <limits>   // for std::numeric_limits
#include <memory>   // for std::shared_ptr

#include "system/system_export_defs.hpp"

namespace optilab {
namespace  metabb {

struct SYS_EXPORT_STRUCT LoadManagerTerminationState {
 public:
  LoadManagerTerminationState() = default;
  ~LoadManagerTerminationState() = default;

  LoadManagerTerminationState(LoadManagerTerminationState&&) = default;
  LoadManagerTerminationState& operator=(LoadManagerTerminationState&&) = default;

  LoadManagerTerminationState(const LoadManagerTerminationState&) = default;
  LoadManagerTerminationState& operator=(const LoadManagerTerminationState&) = default;

  /// This state is at checkpoint or not
  bool isCheckpointState{true};

  /// Rank of this LoadManager
  int rank{0};

  /// Number of warm starts
  std::size_t numWarmStarts{0};

  /// Number of nodes sent from LM to a solver
  uint64_t numNodesSent{0};

  uint64_t numNodesSentImmediately{0};
  uint64_t numNodesSentImmediatelyAnotherNode{0};

  /// Number of nodes deleted in the LM
  uint64_t numNodesDeletedInLM{0};

  /// Number of nodes deleted by merging them
  uint64_t numNodesDeletedByMerging{0};

  uint64_t numNodesFailedToSendBack{0};
  uint64_t numNodesFailedToSendBackAnotherNode{0};

  /// Number of nodes sent from LM to a solver
  uint64_t numNodesReceived{0};

  /// Idle time in msec for load manager
  uint64_t idleTimeMsec{0};

  /// Running time in msec for load manager
  uint64_t runningTimeMsec{0};

  /// Merging time
  uint64_t addingNodeToMergeStructTimeMsec{0};
  uint64_t generateMergeNodesCandidatesTimeMsec{0};
  uint64_t regenerateMergeNodesCandidatesTime{0};
  uint64_t mergeNodeTime{0};

  /// Number of nodes in the node pool
  uint64_t numNodesInNodePool{0};

  /// Maximum usage of node pool
  uint64_t maxUsageOfNodePool{0};

  /// Initial P value.
  /// The P value is the number of good nodes to
  /// always keep in the node pool
  uint64_t initialP{0};

  /// Maximum multiplier for the number of collecting mode nodes
  uint64_t mMaxCollectingNodes{0};

  /// Number of nodes left in all solvers
  uint64_t numNodesLeftInAllSolvers{0};

  /// Best global dual bound found
  double globalBestDualBoundValue{std::numeric_limits<double>::lowest()};

  /// Best external global dual bound value found
  double externalGlobalBestDualBoundValue{std::numeric_limits<double>::lowest()};
};

}  // namespace parallelbb

}  // namespace optilab
