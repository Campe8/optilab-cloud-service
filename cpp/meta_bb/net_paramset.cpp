#include "meta_bb/net_paramset.hpp"

#include <cassert>
#include <stdexcept>  // for std::invalid_argument

#include <google/protobuf/util/message_differencer.h>
#include <spdlog/spdlog.h>

#include "meta_bb/meta_bb_constants.hpp"

namespace optilab {
namespace  metabb {

NetParamSet::NetParamSet()
: ParamSet()
{
}  // ParamSet

NetParamSet::NetParamSet(const SolverProtoParamSet& solverParamSet)
: ParamSet(solverParamSet)
{
}

NetParamSet::NetParamSet(const ProtoParamSetDescriptor& descriptorParamSet)
: ParamSet(descriptorParamSet)
{
}

NetParamSet::NetParamSet(const SolverProtoParamSet& solverParamSet,
                         const ProtoParamSetDescriptor& descriptorParamSet)
: ParamSet(solverParamSet, descriptorParamSet)
{
}

int NetParamSet::broadcast(const Framework::SPtr& framework, int root,
                           bool sendSolverProtoParamSet) noexcept
{
  if (!framework)
  {
    spdlog::error("NetParamSet - broadcast: empty network communicator");
    return metaconst::ERR_GENERIC_ERROR;
  }
  assert(framework->getComm());

  // Create the optimizer paramset packet to send to the destination
  optnet::Packet::UPtr packet;
  if (framework->getComm()->getRank() == root)
  {
    // Broadcasting this packet to the network
    packet = optnet::Packet::UPtr(new optnet::Packet());
    if (sendSolverProtoParamSet)
    {
      packet->networkPacket = optnet::BaseNetworkPacket::UPtr(
              new optnet::NetworkPacket<SolverProtoParamSet>(getSolverParamset()));
    }
    else
    {
      packet->networkPacket = optnet::BaseNetworkPacket::UPtr(
              new optnet::NetworkPacket<ProtoParamSetDescriptor>(getDescriptor()));
    }
  }

  // Send/receive the packet
  if (framework->getComm()->broadcast(packet, root))
  {
    return metaconst::ERR_GENERIC_ERROR;
  }

  if (framework->getComm()->getRank() != root)
  {
    // Packet received
    if (packet->networkPacket->packetType ==
            optnet::BaseNetworkPacket::BaseNetworkPacketType::BNPT_META_BB_DESCRIPTOR_PARAMSET)
    {
      auto networkPacket = optnet::castBaseToNetworkPacket<std::string>(
              std::move(packet->networkPacket));
      if (!networkPacket)
      {
        spdlog::error("NetParamSet - receive: error downcasting to NetworkPacket<std::string>");
        return metaconst::ERR_GENERIC_ERROR;
      }

      if (networkPacket->size() != 1)
      {
        spdlog::error("NetParamSet - receive: wrong packet data size " +
                      std::to_string(networkPacket->size()));
        return metaconst::ERR_GENERIC_ERROR;
      }

      pParamSetDescriptor.Clear();
      pParamSetDescriptor.ParseFromString(networkPacket->data.at(0));
    }
    else if (packet->networkPacket->packetType ==
            optnet::BaseNetworkPacket::BaseNetworkPacketType::BNPT_META_BB_SOLVER_PARAMSET)
    {
      auto networkPacket = optnet::castBaseToNetworkPacket<std::string>(
              std::move(packet->networkPacket));
      if (!networkPacket)
      {
        spdlog::error("NetParamSet - receive: error downcasting to NetworkPacket<std::string>");
        return metaconst::ERR_GENERIC_ERROR;
      }

      if (networkPacket->size() != 1)
      {
        spdlog::error("NetParamSet - receive: wrong packet data size " +
                      std::to_string(networkPacket->size()));
        return metaconst::ERR_GENERIC_ERROR;
      }

      pSolverParamSet.Clear();
      pSolverParamSet.ParseFromString(networkPacket->data.at(0));
    }
    else
    {
      spdlog::error("NetParamSet - receive: invalid network packet type received");
      return metaconst::ERR_GENERIC_ERROR;
    }
  }

  return metaconst::ERR_NO_ERROR;
}  // broadcast

}  // namespace metabb

template<> bool optnet::NetworkPacket<metabb::SolverProtoParamSet>::operator==(
    const NetworkPacket<metabb::SolverProtoParamSet>& other) const noexcept
{
  if (this->data.size() != other.data.empty()) return false;
  return google::protobuf::util::MessageDifferencer::Equals(this->data[0], other.data[0]);
}

template<>
std::pair<optnet::BaseNetworkPacket::BaseNetworkPacketType, std::string>
optnet::NetworkPacket<metabb::SolverProtoParamSet>::serialize() const noexcept
{
  if (data.size() != 1)
  {
    spdlog::error("NetworkPacket<SolverProtoParamSet> - invalid data size");
    return {BaseNetworkPacketType::BNPT_UNSPECIFIED_PACKET, ""};
  }

  try
  {
    return {BaseNetworkPacketType::BNPT_META_BB_SOLVER_PARAMSET, data.at(0).SerializeAsString()};
  }
  catch(...)
  {
    spdlog::error("NetworkPacket<SolverProtoParamSet> - undefined error when serializing protobuf");
    return {BaseNetworkPacketType::BNPT_UNSPECIFIED_PACKET, ""};
  }
}  // serialize

template<> bool optnet::NetworkPacket<metabb::ProtoParamSetDescriptor>::operator==(
    const NetworkPacket<metabb::ProtoParamSetDescriptor>& other) const noexcept
{
  if (this->data.size() != other.data.empty()) return false;
  return google::protobuf::util::MessageDifferencer::Equals(this->data[0], other.data[0]);
}

template<>
std::pair<optnet::BaseNetworkPacket::BaseNetworkPacketType, std::string>
optnet::NetworkPacket<metabb::ProtoParamSetDescriptor>::serialize() const noexcept
{
  if (data.size() != 1)
  {
    spdlog::error("NetworkPacket<ProtoParamSetDescriptor> - invalid data size");
    return {BaseNetworkPacketType::BNPT_UNSPECIFIED_PACKET, ""};
  }

  try
  {
    return {BaseNetworkPacketType::BNPT_META_BB_DESCRIPTOR_PARAMSET,
      data.at(0).SerializeAsString()};
  }
  catch(...)
  {
    spdlog::error("NetworkPacket<ProtoParamSetDescriptor> - "
            "undefined error when serializing protobuf");
    return {BaseNetworkPacketType::BNPT_UNSPECIFIED_PACKET, ""};
  }
}  // serialize

}  // namespace optilab
