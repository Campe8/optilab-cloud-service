//
// Copyright OptiLab 2019. All rights reserved.
//
// Macros for the meta branch-and-bound framework.
//

#pragma once

#include <cmath>      // for fabs
#include <stdexcept>  // for std::runtime_error
#include <sstream>
#include <string>

#include <spdlog/spdlog.h>

/// Absolute value of argument
#define REALABS(x) (std::fabs(x))

/// Returns true if |x - y| <= eps.
/// Returns false otherwise
#define EPSEQ(x,y,eps) (REALABS((x)-(y)) <= (eps))

/// Returns true if x - y < -eps,
/// in other words if y - x > eps
#define EPSLT(x,y,eps) ((x)-(y) < -(eps))

/// Returns true if x - y <= -eps,
/// in other words if y - x => eps
#define EPSLE(x,y,eps) ((x)-(y) <= (eps))

/// Returns true if x > y up to eps, returns false otherwise
#define EPSGT(x,y,eps) ((x)-(y) > (eps))

/// Returns true if x >= y up to eps, returns false otherwise
#define EPSGE(x,y,eps) ((x)-(y) >= -(eps))

/// Calls and checks that the distributed communication was successful,
/// i.e., no error (err == 0).
/// Throws std::runtime_error if there was an error
#define ASSERT_COMM_CALL(distMsgCall)                         \
  do                                                          \
  {                                                           \
    int err = distMsgCall;                                    \
    if (err != 0)                                             \
    {                                                         \
      std::ostringstream ss;                                  \
      ss << "Error on communication: file \"" << __FILE__ <<  \
      "\" function " << __func__ << " line " << __LINE__ <<   \
      " code " << err;                                        \
      const std::string errMsg = ss.str();                    \
      spdlog::error(errMsg);                                  \
      throw std::runtime_error(errMsg);                       \
    }                                                         \
  } while (0)

#define LOG_TIME(fcnName)                                         \
  do                                                              \
  {                                                               \
    spdlog::info(std::string(fcnName) + " - Checkpoint: " +       \
                 std::to_string(pTimer->getWallClockTimeMsec()) + \
                 " msec"); \
  } while (0)

