//
// Copyright OptiLab 2019-2020. All rights reserved.
//
// Base class encapsulating the difference between the model instance and a subproblem.
// The difference subproblem is the (sub) problem sent to nodes to solve.
//

#pragma once

#include <memory>  // for std::shared_ptr
#include <vector>

#include "meta_bb/framework.hpp"
#include "meta_bb/meta_bb_struct.hpp"
#include "meta_bb/instance.hpp"
#include "system/system_export_defs.hpp"

// Forward declarations
namespace optilab {
namespace  metabb {
class Initiator;
}  // namespace metabb
}  // namespace optilab

namespace optilab {
namespace  metabb {

class SYS_EXPORT_CLASS DiffSubproblem {
 public:
  using SPtr = std::shared_ptr<DiffSubproblem>;

 public:
  /// Default constructor
  DiffSubproblem() = default;

  virtual ~DiffSubproblem() = default;

  /// Returns a clone of this object
  virtual SPtr clone(const Framework::SPtr& framework) = 0;

  /// Returns the number of bound changes
  virtual int getNBoundChanges() const noexcept = 0;

  /// Sets up the given list with the fixed variables and returns the number of variables
  /// in the list (i.e., its size)
  virtual int getFixedVariables(const Instance::SPtr instance,
                                std::vector<mergenodes::FixedVariable::SPtr>& fixedVarList) = 0;

  /// Builds and returns a new instance of DiffSubproblem from the given list of fixed variables
  virtual SPtr createDiffSubproblem(
      Framework::SPtr framework, std::shared_ptr<Initiator> initiator,
      const std::vector<mergenodes::FixedVariable*>& fixedVarList) = 0;

  /// Sends this diff. subproblem to given destination.
  /// Returns zero on success, non-zero otherwise
  virtual int send(const Framework::SPtr& framework, int destination) noexcept = 0;

  /// Receives a DiffSubproblem from the given source.
  /// Returns zero on success, non-zero otherwise.
  /// @note this is a blocking call
  virtual int receive(const Framework::SPtr& framework, int source) noexcept = 0;

  /// Broadcast this diff. subproblem to the network
  virtual int broadcast(const Framework::SPtr& framework, int root) noexcept = 0;
};

}  // namespace metabb
}  // namespace optilab

