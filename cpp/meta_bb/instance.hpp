//
// Copyright OptiLab 2019. All rights reserved.
//
// Base class for any optimization problem instance to be solved
// with the meta branch and bound framework.
//

#pragma once

#include <memory>  // for std::shared_ptr
#include <string>

#include "meta_bb/framework.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace  metabb {

class SYS_EXPORT_CLASS Instance {
 public:
  using SPtr = std::shared_ptr<Instance>;

 public:
  /// Default constructor
  Instance() = default;

  virtual ~Instance() = default;

  /// Returns the name of this instance (i.e., the problem's name)
  virtual std::string getName() const noexcept = 0;

  /// Returns the number of variables in this instance
  virtual int getVarIndexRange() const noexcept = 0;

  /// Broadcast this instance to all the solvers in the network
  virtual int broadcast(const Framework::SPtr& framework, int root) noexcept = 0;
};

}  // namespace metabb
}  // namespace optilab

