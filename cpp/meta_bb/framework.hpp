//
// Copyright OptiLab 2019-2020. All rights reserved.
//
// Base class for the meta branch and bound framework.
// The framework acts as a network communicator and a builder class.
//

#pragma once

#include <cstdint>  // for uint64_t
#include <memory>   // for std::shared_ptr

#include "meta_bb/meta_bb_constants.hpp"
#include "optimizer_network/network_base_communicator.hpp"
#include "optimizer_network/packet.hpp"
#include "system/system_export_defs.hpp"
#include "utilities/timer.hpp"

// Forward declarations
namespace optilab {
namespace  metabb {
class CalculationState;
class DiffSubproblem;
class InitialStat;
class Node;
class NodeId;
class ParamSet;
class RacingRampUpParamSet;
class Solution;
class SolverState;
class SolverTerminationState;
}  // namespace metabb
}  // namespace optilab

namespace optilab {
namespace  metabb {

class SYS_EXPORT_CLASS Framework {
 public:
  using SPtr = std::shared_ptr<Framework>;

 public:
  /// Default framework.
  /// The network communicator must be set by derived classes.
  Framework() = default;

  /// Constructor:
  /// - comm: network communicator used to send and receive packets over the network
  explicit Framework(optnet::NetworkBaseCommunicator::SPtr comm);

  virtual ~Framework() = default;

  /// Initializes the communicator in this framework.
  /// Returns zero on success, non-zero otherwise.
  /// @note if "initTimeoutMsec" is specified and greater than zero, this method will wait up
  /// to "initTimeoutMsec" msec. before returning if the hub is not able to respond successfully.
  /// A negative timeout value (default) makes the network communicator wait indefinitely
  void init(int initTimeoutMsec = -1) { getComm()->init(initTimeoutMsec); }

  /// Terminates this communicator and the framework.
  /// This method makes a "best attempt" to abort all tasks running
  /// on this communicator
  void abort() { getComm()->abort(); }

  /// Returns the network communicator used in this framework
  const optnet::NetworkBaseCommunicator::SPtr& getComm() const { return pNetworkComm; }

  /// Returns the pointer to the network communicator used in this framework
  optnet::NetworkBaseCommunicator* getCommMutable() { return pNetworkComm.get(); }

  /// Returns a default parameter set
  virtual std::shared_ptr<ParamSet> buildParamSet() = 0;

  /// Returns a default racing ramp-up parameter set
  virtual std::shared_ptr<RacingRampUpParamSet> buildRacingRampUpParamSet() = 0;

  /// Returns a default calculation state
  virtual std::shared_ptr<CalculationState> buildCalculationState() = 0;

  /// Returns a calculation state built on the given input arguments
  virtual std::shared_ptr<CalculationState> buildCalculationState(
      uint64_t compTimeMsec,
      uint64_t rootTimeMsec,
      int numNodesSolved,
      int numNodesSent,
      int numImprovedIncumbent,
      solver::TerminationState terminationState,
      int numNodesSolverWithoutPreprocess,
      int numSimplexIterRoot,
      double avgSimplexIter,
      int numTransferredLocalCuts,
      int minTransferredLocalCuts,
      int maxTransferredLocalCuts,
      int numRestarts,
      double minInfeasibilitySum,
      double maxInfeasibilitySum,
      int minInfeasibilityNum,
      int maxInfeasibilityNum,
      double dualBound) = 0;

  /// Builds and returns a default initial stat object
  virtual std::shared_ptr<InitialStat> buildInitialStat() = 0;

  /// Builds and returns a default node
  virtual std::shared_ptr<Node> buildNode() = 0;

  /// Builds and returns a node with specified parameters
  virtual std::shared_ptr<Node> buildNode(
      NodeId nodeId,
      NodeId generatorNodeId,
      int depth,
      double dualBoundValue,
      double originalDualBoundValue,
      double estimatedValue,
      std::shared_ptr<DiffSubproblem> diffSubproblem) = 0;

  /// Builds and returns a default solver state
  virtual std::shared_ptr<SolverState> buildSolverState() = 0;

  /// Builds and returns a solver state with specified values
  virtual std::shared_ptr<SolverState> buildSolverState(
      int racingStage,
      unsigned int notificationId,
      int LMId,
      int globalSubtreeId,
      long long nodesSolved,
      int nodesLeft,
      double bestDualBoundValue,
      double globalBestPrimalBoundValue,
      uint64_t timeMsec,
      double averageDualBoundGain) = 0;

  /// Builds and returns a default solver termination state
  virtual std::shared_ptr<SolverTerminationState> buildSolverTerminationState() = 0;

  /// Builds and returns a solver termination state with specified values
  virtual std::shared_ptr<SolverTerminationState> buildSolverTerminationState(
      solver::InterruptionPoint interrupted,
      int rank,
      int totalNumSolved,
      int minNumSolved,
      int maxNumSolved,
      int totalNumSent,
      int totalNumImprovedIncumbent,
      int numNodesReceived,
      int numNodesSolved,
      int numNodesSolvedAtRoot,
      int numNodesSolvedAtPreCheck,
      int numTransferredLocalCutsFromSolver,
      int minNumTransferredLocalCutsFromSolver,
      int maxNUmTransferredLocalCutsFromSolver,
      int numTotalRestarts,
      int minNumRestarts,
      int maxNumRestarts,
      int numVarTightened,
      int numIntVarTightenedInt,
      uint64_t runningTimeMsec,
      uint64_t idleTimeToFirstNodeMsec,
      uint64_t idleTimeBetweenNodesMsec,
      uint64_t idleTimeAfterLastNodeMsec,
      uint64_t idleTimeToWaitNotificationIdMsec,
      uint64_t idleTimeToWaitAckCompletionMsec,
      uint64_t idleTimeToWaitTokenMsec,
      uint64_t totalRootNodeTimeMsec,
      uint64_t minRootNodeTimeMsec,
      uint64_t maxRootNodeTimeMsec) = 0;

  /// Returns a new instance of a DiffSubproble,
  /// i.e., the class storing the difference between instance and subproblem
  virtual std::shared_ptr<DiffSubproblem> buildDiffSubproblem() = 0;

  /// Returns a new instance of a default solution
  virtual std::shared_ptr<Solution> buildSolution() = 0;

  /// Builds and returns a timer which can be on or off depending on
  /// the input argument
  virtual timer::Timer::SPtr buildTimer(bool startTimer);

 protected:
  void setNetworkCommunicator(optnet::NetworkBaseCommunicator::SPtr comm);

 private:
  /// Pointer to the network communicator used to send/receive packets
  /// across the network
  optnet::NetworkBaseCommunicator::SPtr pNetworkComm;
};

}  // namespace metabb
}  // namespace optilab
