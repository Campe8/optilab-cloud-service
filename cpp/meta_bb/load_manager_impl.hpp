//
// Copyright OptiLab 2020. All rights reserved.
//
// Implementation of load manager.
// This is a convenience class for function handlers.
//

#pragma once

#include <memory>  // for std::shared_ptr

#include "meta_bb/load_manager.hpp"
#include "meta_bb/meta_bb_struct.hpp"
#include "optimizer_network/network_base_communicator.hpp"
#include "optimizer_network/packet.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace  metabb {

class SYS_EXPORT_CLASS LoadManagerImpl {
 public:
  using SPtr = std::shared_ptr<LoadManagerImpl>;

 public:
  LoadManagerImpl(LoadManager* lm, bool racingRampUp=false);
  virtual ~LoadManagerImpl() = default;

  /// Operator calling the proper handler on the given tag
  int operator()(int source, optnet::Packet::UPtr packet);

  /// Returns true if there is an handler for the given tag.
  /// Returns false otherwise
  bool isHandlerSupported(net::MetaBBPacketTag::PacketTagType tag);

  /// Sets the racing ramp-up flag as active
  inline void setRacingRampup() noexcept { pIsRacingRampUp = true; }

 protected:
  bool pIsRacingRampUp{false};

  /// Pointer to the solver owning this instance
  LoadManager* pLM;

  /// Returns the load manager's framework
  inline Framework::SPtr getFramework() const { return pLM->pFramework; }

  /// Returns the pointer to the solver pool
  inline SolverPool::SPtr getSolverPool() const { return pLM->pSolverPool; }

  /// Returns the pointer to the node pool
  inline NodePool::SPtr getNodePool() const { return pLM->pNodePool; }

  /// Returns the pointer to the network communicator
  inline optnet::NetworkBaseCommunicator* getComm() { return pLM->pFramework->getCommMutable(); }

  /// Returns the parameters set
  inline ProtoParamSetDescriptor& getParams() { return pLM->getParams(); }

  /// Virtual callback handlers
  virtual int processTagNode(int source, optnet::Packet::UPtr packet);
  virtual int processTagSolution(int source, optnet::Packet::UPtr packet);
  virtual int processTagSolverState(int source, optnet::Packet::UPtr packet);
  virtual int processTagCompletionOfCalculation(int source, optnet::Packet::UPtr packet);
  virtual int processTagAnotherNodeRequest(int source, optnet::Packet::UPtr packet);
  virtual int processTagTerminated(int source, optnet::Packet::UPtr packet);
  virtual int processTagHardTimeLimit(int source, optnet::Packet::UPtr packet);
  virtual int processTagStatistics(int source, optnet::Packet::UPtr packet);
  virtual int processTagToken(int source, optnet::Packet::UPtr packet);
  virtual int processTagAllowToBeInCollectingMode(int source, optnet::Packet::UPtr packet);
  virtual int processTagLbBoundTightened(int source, optnet::Packet::UPtr packet);
  virtual int processTagUbBoundTightened(int source, optnet::Packet::UPtr packet);
  virtual int processRacingRampUpTagSolverState(int source, optnet::Packet::UPtr packet);
  virtual int processRacingRampUpTagCompletionOfCalculation(int source,
                                                            optnet::Packet::UPtr packet);

  /// Utility function: returns true if the load manager is in ramp-up phase,
  /// returns false otherwise
  inline bool isRampUpPhase() const noexcept
  {
    return pLM->pRunningPhase == LoadManager::RunningPhase::LMRP_RAMPUP;
  }

  /// Utility function: returns the number of "good nodes" in the node pool
  /// w.r.t. the given dual bound value
  inline long long getNumGoodNodes(double dualBound) const
  {
    return static_cast<long long>(pLM->pNodePool->getNumOfGoodNodes(dualBound));
  }

  /// Utility function: returns the solver's pool global best dual bound value
  inline double getSolverPoolBestDualBound() const
  {
    return pLM->pSolverPool->getGlobalBestDualBoundValue();
  }

  /// Utility function: returns the node's pool best dual bound value
  inline double getNodePoolBestDualBound() const
  {
    return pLM->pNodePool->getBestDualBoundValue();
  }

  /// Utility function, logs time
  void logTime(const std::string& fcnName);

  /// Switch the solver pool out of collecting mode and right back into collecting mode
  /// in order to restart the collecting mode process.
  /// @note if the given flag is true, the maximum number of solvers that can be
  /// in collecting mode will be increased.
  /// @note the maximum number of solvers that can be in collecting mode will be increased
  /// only if the solver can actually add more solvers to collecting mode
  void switchSolversToCollectingMode(bool increaseMaxNumSolversInCollectingMode = false);
};

}  // namespace metabb
}  // namespace optilab
