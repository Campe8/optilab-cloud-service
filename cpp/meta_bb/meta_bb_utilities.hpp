//
// Copyright OptiLab 2019-2020. All rights reserved.
//
// Utilities for the meta_bb framework.
//

#pragma once

#include <stdexcept>  // for std::runtime_error
#include <string>
#include <utility>    // for std::move
#include <vector>

#include "meta_bb/framework.hpp"
#include "meta_bb/meta_bb_struct.hpp"
#include "optimizer_network/network_packet.hpp"
#include "optimizer_network/packet.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace metabb {

namespace utils {

/**
 * Struct used to wrap information about
 * the status of an operation or method call
 */
struct SYS_EXPORT_STRUCT OpStatus {
  OpStatus() = default;
  OpStatus(const std::string& errMsg)
  : pStatusOk(false), pErrMsg(errMsg)
  {
  }

  /// Returns true if the status is okay, false otherwise
  inline bool isOk() const noexcept { return pStatusOk; }

  /// Returns the error message, if any
  inline const std::string& errMsg() const noexcept { return pErrMsg; }
 private:
  /// Status
  bool pStatusOk{true};

  /// Error description message, if any
  std::string pErrMsg;
};

// Sends the given tag to the given network node
SYS_EXPORT_FCN void sendTagMessageToNetworkNode(Framework::SPtr framework,
                                                net::MetaBBPacketTag::PacketTagType tag,
                                                int rank);

/// Casts a packet tag to a MetaBBPacketTag type.
/// Returns nullptr if the conversion is not possible
SYS_EXPORT_FCN net::MetaBBPacketTag::UPtr castToMetaBBPacketTag(optnet::PacketTag::UPtr packetTag);

/// Tries to cast to MetaBBPacketTag. On success, returns the packet tag type.
/// On failure, throws std::runtime_error
SYS_EXPORT_FCN net::MetaBBPacketTag::PacketTagType castToMetaBBPacketTagAndGetTagOrThrow(
        optnet::PacketTag::UPtr& packetTag);

template<typename T>
void sendDataMessageToNetworkNode(
        Framework::SPtr framework,
        net::MetaBBPacketTag::PacketTagType tag,
        int rank,
        const T& data)
{
  assert(framework);
  optnet::Packet::UPtr packet(new optnet::Packet());
  packet->packetTag = net::MetaBBPacketTag::UPtr(new net::MetaBBPacketTag(tag));
  packet->networkPacket =
          typename optnet::NetworkPacket<T>::UPtr(new optnet::NetworkPacket<T>(data));

  if (framework->getComm()->send(std::move(packet), rank) != 0)
  {
    throw std::runtime_error("Error on communication file (send) on sendDataMessageToNetworkNode");
  }
}

template<typename T>
void sendListDataMessageToNetworkNode(Framework::SPtr framework,
                                      net::MetaBBPacketTag::PacketTagType tag,
                                      int rank,
                                      const std::vector<T>& data)
{
  assert(framework);
  optnet::Packet::UPtr packet(new optnet::Packet());
  packet->packetTag = net::MetaBBPacketTag::UPtr(new net::MetaBBPacketTag(tag));
  packet->networkPacket =
          typename optnet::NetworkPacket<T>::UPtr(new optnet::NetworkPacket<T>(data));

  if (framework->getComm()->send(std::move(packet), rank) != 0)
  {
    throw std::runtime_error("Error on communication file (send) on sendDataMessageToNetworkNode");
  }
}


template<typename T>
std::vector<T> receiveDataMessageToNetworkNode(Framework::SPtr framework,
                                               net::MetaBBPacketTag::PacketTagType tag,
                                               int rank)
{
  assert(framework);
  const int dummyContentData{0};
  optnet::Packet::UPtr packet(new optnet::Packet());
  packet->packetTag = net::MetaBBPacketTag::UPtr(new net::MetaBBPacketTag(tag));
  if (framework->getComm()->receive(packet, rank) != 0)
  {
    throw std::runtime_error("Error on communication file (receive) on "
            "receiveDataMessageToNetworkNode");
  }

  auto npacket = optnet::castBaseToNetworkPacket<T>(std::move(packet->networkPacket));
  if (!npacket)
  {
    throw std::runtime_error("Error on communication file on "
            "receiveDataMessageToNetworkNode: cannot cast content to network packet");
  }
  return npacket->data;
}

// Sends the given tag to the given network node
SYS_EXPORT_FCN void receiveTagMessageFromNetworkNode(Framework::SPtr framework,
                                                     optnet::Packet::UPtr& packet,
                                                     net::MetaBBPacketTag::PacketTagType tag,
                                                     int rank);


}  // namespace utils

}  // namespave metabb
}  // namespace optilab
