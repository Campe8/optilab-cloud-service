#include "meta_bb/net_node.hpp"

#include <memory>     // for std::dynamic_pointer_cast
#include <stdexcept>  // for std::invalid_argument
#include <string>
#include <utility>    // for std::move

#include <google/protobuf/util/message_differencer.h>
#include <spdlog/spdlog.h>

#include "meta_bb/meta_bb_constants.hpp"
#include "meta_bb/meta_bb_struct.hpp"
#include "meta_bb/meta_bb_utilities.hpp"
#include "optimizer_network/network_communicator.hpp"
#include "optimizer_network/packet.hpp"

namespace optilab {
namespace  metabb {

NetNode::NetNode(const NodeId& nodeId,
                 const NodeId& generatorNodeId,
                 int depth,
                 double dualBoundValue,
                 double originalDualBoundValue,
                 double estimatedValue,
                 const DiffSubproblem::SPtr& diffSubproblem)
: Node(nodeId,
       generatorNodeId,
       depth,
       dualBoundValue,
       originalDualBoundValue,
       estimatedValue,
       diffSubproblem)
{
}

NetNode::~NetNode()
{
  // No-op
}

Node::SPtr NetNode::clone(const Framework::SPtr& framework)
{
  if (!framework)
  {
    throw std::invalid_argument("NetNode - clone: empty pointer to the Framework");
  }

  if (pDiffSubproblem)
  {
    return std::make_shared<NetNode>(pNodeId, pGeneratorNodeId, pDepth, pDualBoundValue,
                                     pInitialDualBoundValue, pInitialDualBoundValue,
                                     pDiffSubproblem->clone(framework));
  }
  else
  {
    return std::make_shared<NetNode>(pNodeId, pGeneratorNodeId, pDepth, pDualBoundValue,
                                     pInitialDualBoundValue, pInitialDualBoundValue, nullptr);
  }
}  // clone

optnet::Packet::UPtr NetNode::buildNetworkPacket()
{
  // Create the optimizer node packet to send to the destination
  optnet::Packet::UPtr packet(new optnet::Packet());

  // Add the content as protobuf message
  NodeProtoPacket nodeProto;
  nodeProto.set_nodeidlmid(pNodeId.getSubtreeId().getLMId());
  nodeProto.set_nodeidglobalsubtreeidinlm(pNodeId.getSubtreeId().getGlobalSubstreeIdInLM());
  nodeProto.set_nodeidsolverid(pNodeId.getSubtreeId().getSolverId());
  nodeProto.set_nodeidseqnum(pNodeId.getSeqNum());
  nodeProto.set_gennodeidlmid(pGeneratorNodeId.getSubtreeId().getLMId());
  nodeProto.set_gennodeidglobalsubtreeidinlm(
      pGeneratorNodeId.getSubtreeId().getGlobalSubstreeIdInLM());
  nodeProto.set_gennodeidsolverid(pGeneratorNodeId.getSubtreeId().getSolverId());
  nodeProto.set_gennodeidseqnum(pGeneratorNodeId.getSeqNum());
  nodeProto.set_depth(pDepth);
  nodeProto.set_dualboundvalue(pDualBoundValue);
  nodeProto.set_initialdualboundvalue(pInitialDualBoundValue);
  nodeProto.set_estimatedvalue(pEstimatedValue);
  nodeProto.set_diffsubprobleminfo(pDiffSubproblemInfo);
  nodeProto.set_mergingstatus(static_cast<int>(pMergingStatus));

  // Set the protobuf message as new network packet
  packet->networkPacket = optnet::BaseNetworkPacket::UPtr(
      new optnet::NetworkPacket<NodeProtoPacket>(nodeProto));

  return std::move(packet);
}  // buildNetworkPacket

int NetNode::broadcast(const Framework::SPtr& framework, int root) noexcept
{
  if (!framework)
  {
    spdlog::error("NetNode - broadcast: empty framework");
    return metaconst::ERR_GENERIC_ERROR;
  }

  // Broadcast or received a broadcasted packet containing a node
  optnet::Packet::UPtr packet;
  if (framework->getComm()->getRank() == root)
  {
    // This is the broadcasting node.
    // Create the optimizer node packet to send to the destination
    packet = buildNetworkPacket();
  }

  // Send/receive the node
  if (framework->getComm()->broadcast(packet, root))
  {
    return metaconst::ERR_GENERIC_ERROR;
  }

  if (framework->getComm()->getRank() != root)
  {
    // This packet was deserialized as a std::string packet containing
    // the bytes of the serialized original message.
    // Therefore, the packet must be first deserialized into a proper
    // NetworkPacket
    if (packet->networkPacket->packetType !=
            optnet::BaseNetworkPacket::BaseNetworkPacketType::BNPT_META_BB_PACKET)
    {
      spdlog::error("NetNode - broadcast: invalid network packet type received");
      return metaconst::ERR_GENERIC_ERROR;
    }

    auto networkPacket = optnet::castBaseToNetworkPacket<std::string>(
            std::move(packet->networkPacket));
    if (!networkPacket)
    {
      spdlog::error("NetNode - broadcast: error downcasting to NetworkPacket<std::string>");
      return metaconst::ERR_GENERIC_ERROR;
    }

    if (networkPacket->size() != 1)
    {
      spdlog::error("NetNode - broadcast: wrong packet data size " +
                    std::to_string(networkPacket->size()));
      return metaconst::ERR_GENERIC_ERROR;
    }

    const auto errCode = uploadPacket(networkPacket->data.at(0));
    if (errCode != metaconst::ERR_NO_ERROR) return errCode;
  }

  // Send/receive the diffSubproblem as well.
  // @note check on the presence of the diff. subproblem since the root node
  // doesn't have any diff. subproblem
  if (pDiffSubproblemInfo)
  {
    // If this communicator network is different than the root that is supposed
    // to send the diff. subproblem, reset the diff. subproblem
    if (framework->getComm()->getRank() != root)
    {
      pDiffSubproblem = framework->buildDiffSubproblem();
    }

    // Send or receive the diff. subproblem to the other nodes in the network
    if (pDiffSubproblem->broadcast(framework, root))
    {
      return metaconst::ERR_GENERIC_ERROR;
    }
  }

  return metaconst::ERR_NO_ERROR;
}  // broadcast

int NetNode::send(const Framework::SPtr& framework, int destination) noexcept
{
  if (!framework)
  {
    spdlog::error("NetNode - send: empty framework");
    return metaconst::ERR_GENERIC_ERROR;
  }

  // Create the optimizer node packet to send to the destination
  optnet::Packet::UPtr packet = buildNetworkPacket();

  // Add the node tag
  packet->packetTag = optnet::PacketTag::UPtr(new net::MetaBBPacketTag(
      net::MetaBBPacketTag::PacketTagType::PTT_NODE));

  if (framework->getComm()->send(std::move(packet), destination))
  {
    return metaconst::ERR_GENERIC_ERROR;
  }

  // Send the diffSubproblem as well.
  // @note check on the presence of the diff. subproblem since the root node
  // doesn't have any diff. subproblem
  if (pDiffSubproblemInfo)
  {
    if (pDiffSubproblem->send(framework, destination))
    {
      return metaconst::ERR_GENERIC_ERROR;
    }
  }

  return metaconst::ERR_NO_ERROR;
}  // send

int NetNode::upload(const Framework::SPtr& framework, optnet::Packet::UPtr packet) noexcept
{
  if (!framework)
  {
    spdlog::error("NetNode - upload: empty framework");
    return metaconst::ERR_GENERIC_ERROR;
  }

  if (!packet)
  {
    spdlog::error("NetNode - upload: empty packet");
    return metaconst::ERR_GENERIC_ERROR;
  }

  // This packet was deserialized as a std::string packet containing
  // the bytes of the serialized original message.
  // Therefore, the packet must be first deserialized into a proper
  // NetworkPacket
  if (packet->networkPacket->packetType !=
          optnet::BaseNetworkPacket::BaseNetworkPacketType::BNPT_META_BB_PACKET)
  {
    spdlog::error("NetNode - upload: invalid network packet type received");
    return metaconst::ERR_GENERIC_ERROR;
  }

  // Check that the received packet is a NODE packet
  try
  {
    const auto tag = utils::castToMetaBBPacketTagAndGetTagOrThrow(packet->packetTag);
    if (tag != net::MetaBBPacketTag::PacketTagType::PTT_NODE)
    {
      spdlog::error("NetNode - upload: invalid tag received");
      return metaconst::ERR_GENERIC_ERROR;
    }
  }
  catch(...)
  {
    spdlog::error("NetNode - upload: no tag received");
    return metaconst::ERR_GENERIC_ERROR;
  }

  auto networkPacket = optnet::castBaseToNetworkPacket<std::string>(
          std::move(packet->networkPacket));
  if (!networkPacket)
  {
    spdlog::error("NetNode - receive: error downcasting to NetworkPacket<std::string>");
    return metaconst::ERR_GENERIC_ERROR;
  }

  if (networkPacket->size() != 1)
  {
    spdlog::error("NetNode - receive: wrong packet data size " +
                  std::to_string(networkPacket->size()));
    return metaconst::ERR_GENERIC_ERROR;
  }

  const auto errCode = uploadPacket(networkPacket->data.at(0));
  if (errCode != metaconst::ERR_NO_ERROR) return errCode;

  // Check if the diff. subproblem info needs to be retrieved as well
  if (pDiffSubproblemInfo)
  {
    // Receive the diff. subproblem.
    // @note that this is a blocking call
    auto comm = std::dynamic_pointer_cast<optnet::NetworkCommunicator>(framework->getComm());
    if (!comm)
    {
      spdlog::error("NetNode - receive: invalid network communicator cast from base communicator");
      return metaconst::ERR_GENERIC_ERROR;
    }
    pDiffSubproblem = framework->buildDiffSubproblem();
    if (pDiffSubproblem->receive(framework, comm->convertNetworkAddrToRank(packet->networkAddr)))
    {
      return metaconst::ERR_GENERIC_ERROR;
    }
  }

  return metaconst::ERR_NO_ERROR;
}  // upload

int NetNode::uploadPacket(const std::string& bytes)
{
  // Get the node proto packet and upload its content into this node
  NodeProtoPacket nodeProto;
  nodeProto.ParseFromString(bytes);
  pNodeId.getSubtreeId().getLMId() = nodeProto.nodeidlmid();
  pNodeId.getSubtreeId().getGlobalSubstreeIdInLM() = nodeProto.nodeidglobalsubtreeidinlm();
  pNodeId.getSubtreeId().getSolverId() = nodeProto.nodeidsolverid();
  pNodeId.getSeqNum() = nodeProto.nodeidseqnum();
  pGeneratorNodeId.getSubtreeId().getLMId() = nodeProto.gennodeidlmid();
  pGeneratorNodeId.getSubtreeId().getGlobalSubstreeIdInLM() =
          nodeProto.gennodeidglobalsubtreeidinlm();
  pGeneratorNodeId.getSubtreeId().getSolverId() = nodeProto.gennodeidsolverid();
  pGeneratorNodeId.getSeqNum() = nodeProto.gennodeidseqnum();
  pDepth = nodeProto.depth();
  pDualBoundValue = nodeProto.dualboundvalue();
  pInitialDualBoundValue = nodeProto.initialdualboundvalue();
  pEstimatedValue = nodeProto.estimatedvalue();
  pDiffSubproblemInfo = nodeProto.diffsubprobleminfo();
  pMergingStatus = static_cast<NodeMergingStatus>(nodeProto.mergingstatus());

  return metaconst::ERR_NO_ERROR;
}  // uploadPacket

}  // namespace metabb

template<> bool optnet::NetworkPacket<metabb::NodeProtoPacket>::operator==(
    const NetworkPacket<metabb::NodeProtoPacket>& other) const noexcept
{
  if (this->data.size() != other.data.empty()) return false;
  return google::protobuf::util::MessageDifferencer::Equals(this->data[0], other.data[0]);
}

template<> std::pair<optnet::BaseNetworkPacket::BaseNetworkPacketType, std::string>
optnet::NetworkPacket<metabb::NodeProtoPacket>::serialize() const noexcept
{
  if (data.size() != 1)
  {
    spdlog::error("NetworkPacket<NodeProtoPacket> - invalid data size");
    return {BaseNetworkPacketType::BNPT_UNSPECIFIED_PACKET, ""};
  }

  try
  {
    return {BaseNetworkPacketType::BNPT_META_BB_PACKET, data.at(0).SerializeAsString()};
  }
  catch(...)
  {
    spdlog::error("NetworkPacket<NodeProtoPacket> - undefined error when serializing protobuf");
    return {BaseNetworkPacketType::BNPT_UNSPECIFIED_PACKET, ""};
  }
}  // serialize

}  // namespace optilab
