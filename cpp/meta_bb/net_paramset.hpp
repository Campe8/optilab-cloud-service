//
// Copyright OptiLab 2020. All rights reserved.
//
// Parameter set class implemented over optnet framework.
//

#pragma once

#include <cstddef>  // for std::size_t
#include <memory>   // for std::shared_ptr

#include "meta_bb/framework.hpp"
#include "meta_bb/paramset.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace metabb {

class SYS_EXPORT_CLASS NetParamSet : public ParamSet {
 public:
  using SPtr = std::shared_ptr<NetParamSet>;

 public:
  NetParamSet();
  explicit NetParamSet(const SolverProtoParamSet& solverParamSet);
  explicit NetParamSet(const ProtoParamSetDescriptor& descriptorParamSet);
  NetParamSet(const SolverProtoParamSet& solverParamSet,
              const ProtoParamSetDescriptor& descriptorParamSet);

  virtual ~NetParamSet() = default;

  /// Broadcasts this paramset from the given root to all other nodes in the network.
  /// Returns zero on success, non-zero otherwise.
  /// @note default implementation returns no-error on no-op. This is done to allow
  /// local solvers that don't use distributed computing to still use ParamSet.
  /// @note if "sendSolverProtoParamSet" is set to true,
  /// this method will broadcast ONLY the SolverProtoParamSet information.
  /// If the flag is false (default), it will broadcast the
  /// ProtoParamSetDescriptor protobuf message needed for distributed computation
  int broadcast(const Framework::SPtr& framework, int root,
                bool sendSolverProtoParamSet=false) noexcept override;
};

}  // namespace metabb

template<> bool optnet::NetworkPacket<metabb::SolverProtoParamSet>::operator==(
    const NetworkPacket<metabb::SolverProtoParamSet>& other) const noexcept;

template<> std::pair<optnet::BaseNetworkPacket::BaseNetworkPacketType, std::string>
optnet::NetworkPacket<metabb::SolverProtoParamSet>::serialize() const noexcept;

template<> bool optnet::NetworkPacket<metabb::ProtoParamSetDescriptor>::operator==(
    const NetworkPacket<metabb::ProtoParamSetDescriptor>& other) const noexcept;

template<> std::pair<optnet::BaseNetworkPacket::BaseNetworkPacketType, std::string>
optnet::NetworkPacket<metabb::ProtoParamSetDescriptor>::serialize() const noexcept;

}  // namespace optilab
