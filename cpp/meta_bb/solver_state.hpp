//
// Copyright OptiLab 2019. All rights reserved.
//
// Base class for the state of the solver transfered among network nodes.
//

#pragma once

#include <cstdint>  // for uint64_t
#include <limits>   // for std::numeric_limits
#include <memory>   // for std::shared_ptr

#include "meta_bb/framework.hpp"
#include "meta_bb/meta_bb_struct.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace  metabb {

class SYS_EXPORT_CLASS SolverState {
 public:
  using SPtr = std::shared_ptr<SolverState>;

 public:
  SolverState() = default;

  SolverState(bool racingStage,
              unsigned int notificationId,
              int lmId,
              int globalSubtreeId,
              long long numSolvedNodes,
              int numNodesLeft,
              double bestDualBoundValue,
              double globalBestPrimalBoundValue,
              uint64_t timeMsec,
              double avgDualBoundGainRecv)
  : pRacingStage(racingStage),
    pNotificationId(notificationId),
    pLMId(lmId),
    pGlobalSubtreeIdInLM(globalSubtreeId),
    pNumberOfSolvedNodes(numSolvedNodes),
    pNodesLeft(numNodesLeft),
    pBestDualBoundValue(bestDualBoundValue),
    pBestGlobalPrimalBoundValue(globalBestPrimalBoundValue),
    pTimeMsec(timeMsec),
    pAvgDualBoundGainRecv(avgDualBoundGainRecv)
  {
  }

  virtual ~SolverState() = default;

  /// Returns true if the solver is in racing stage
  inline bool isRacingStage() const noexcept { return pRacingStage; }

  /// Returns the notification identifier of this solver state
  inline int getNotificationId() const noexcept { return pNotificationId; }

  /// Returns the id of the load manager
  inline int getLMId() const noexcept { return pLMId; }

  /// Returns the global subtree identifier of this node
  inline int getGlobalSubtreeId() const noexcept { return pGlobalSubtreeIdInLM; }

  /// Returns the best dual bound value found so far
  inline double getSolverLocalBestDualBoundValue() const noexcept { return pBestDualBoundValue; }

  /// Returns the best global primal bound value found so far
  inline double getGlobalBestPrimalBoundValue() const noexcept
  {
    return pBestGlobalPrimalBoundValue;
  }

  /// Returns the number of nodes solved so far
  inline long long getNumNodesSolved() const noexcept { return pNumberOfSolvedNodes; }

  /// Returns the number of nodes left to solve
  inline int getNumNodesLeft() const noexcept { return pNodesLeft; }

  /// Returns time in msec.
  inline uint64_t getTimeMsec() const noexcept { return pTimeMsec; }

  /// Returns the average dual bound gain received
  inline double getAverageDualBoundGainRecv() const noexcept { return pAvgDualBoundGainRecv; }

  /// Send s the solver's state to the given destination
  virtual int send(const Framework::SPtr& fr, int destination) noexcept = 0;

  /// Uploads the content of the given network packet into this SolverState.
  /// If the given packet is not a node network packet, throws std::runtime_error
  /// Returns zero on success, non-zero otherwise
  virtual int upload(optnet::Packet::UPtr packet) noexcept = 0;

 protected:
  /// Flag indicating whether or not the solver is in racing stage.
  /// @note a value of 1 means that the solver is in racing stage
  bool pRacingStage{false};

  /// Unique id for this state
  unsigned int pNotificationId{0};

  /// Load manager identifier of the current node
  int pLMId{-1};

  /// Global subtree identifier of the current node in the load coordinator
  int pGlobalSubtreeIdInLM{-1};

  /// Number of solved nodes so far
  long long pNumberOfSolvedNodes{0};

  /// Number of remaining nodes to explore
  int pNodesLeft{-1};

  /// Best dual bound value found so far
  double pBestDualBoundValue{0.0};

  /// Global best primal bound value
  double pBestGlobalPrimalBoundValue{std::numeric_limits<double>::max()};

  /// Time in seconds
  uint64_t pTimeMsec{0};

  /// Average dual bound gain received
  double pAvgDualBoundGainRecv{0.0};
};

}  // namespace parallelbb

}  // namespace optilab
