#include "meta_bb/net_calculation_state.hpp"

#include <memory>     // for std::dynamic_pointer_cast
#include <stdexcept>  // for std::invalid_argument
#include <string>
#include <utility>    // for std::move

#include <google/protobuf/util/message_differencer.h>
#include <spdlog/spdlog.h>

#include "meta_bb/meta_bb_constants.hpp"
#include "meta_bb/meta_bb_struct.hpp"
#include "meta_bb/meta_bb_utilities.hpp"
#include "optimizer_network/network_communicator.hpp"
#include "optimizer_network/network_packet.hpp"
#include "optimizer_network/packet.hpp"

namespace optilab {
namespace  metabb {

NetCalculationState::NetCalculationState(uint64_t compTimeMsec,
                                         uint64_t rootTimeMsec,
                                         int numNodesSolved,
                                         int numNodesSent,
                                         int numImprovedIncumbent,
                                         solver::TerminationState terminationState,
                                         int numNodesSolverWithoutPreprocess,
                                         std::size_t numSimplexIterRoot,
                                         double avgSimplexIter,
                                         std::size_t numTransferredLocalCuts,
                                         std::size_t minTransferredLocalCuts,
                                         std::size_t maxTransferredLocalCuts,
                                         std::size_t numRestarts,
                                         double minInfeasibilitySum,
                                         double maxInfeasibilitySum,
                                         std::size_t minInfeasibilityNum,
                                         std::size_t maxInfeasibilityNum,
                                         double dualBound)
: CalculationState(
        compTimeMsec,
        rootTimeMsec,
        numNodesSolved,
        numNodesSent,
        numImprovedIncumbent,
        terminationState,
        numNodesSolverWithoutPreprocess,
        numSimplexIterRoot,
        avgSimplexIter,
        numTransferredLocalCuts,
        minTransferredLocalCuts,
        maxTransferredLocalCuts,
        numRestarts,
        minInfeasibilitySum,
        maxInfeasibilitySum,
        minInfeasibilityNum,
        maxInfeasibilityNum,
        dualBound){}

NetCalculationState::~NetCalculationState()
{
  // No-op
}

int NetCalculationState::send(const Framework::SPtr& framework, int destination) noexcept
{
  if (!framework)
  {
    spdlog::error("NetCalculationState - send: empty network communicator");
    return metaconst::ERR_GENERIC_ERROR;
  }

  // Create the optimizer node packet to send to the destination
  optnet::Packet::UPtr packet = buildNetworkPacket();

  // Add the node tag
  packet->packetTag = optnet::PacketTag::UPtr(new net::MetaBBPacketTag(
          net::MetaBBPacketTag::PacketTagType::PPT_COMPLETION_OF_CALCULATION));

  if (framework->getComm()->send(std::move(packet), destination))
  {
    return metaconst::ERR_GENERIC_ERROR;
  }

  return metaconst::ERR_NO_ERROR;
}  // send

int NetCalculationState::upload(optnet::Packet::UPtr packet) noexcept
{
  if (!packet)
  {
    spdlog::error("NetCalculationState - upload: empty packet");
    return metaconst::ERR_GENERIC_ERROR;
  }

  // Check that the received packet is a calculation packet
  try
  {
    const auto tag = utils::castToMetaBBPacketTagAndGetTagOrThrow(packet->packetTag);
    if (tag != net::MetaBBPacketTag::PacketTagType::PPT_COMPLETION_OF_CALCULATION)
    {
      spdlog::error("NetCalculationState - upload: invalid tag received");
      return metaconst::ERR_GENERIC_ERROR;
    }
  }
  catch(...)
  {
    spdlog::error("NetCalculationState - upload: no tag received");
    return metaconst::ERR_GENERIC_ERROR;
  }

  // This packet was deserialized as a std::string packet containing
  // the bytes of the serialized original message.
  // Therefore, the packet must be first deserialized into a proper
  // NetworkPacket
  if (packet->networkPacket->packetType !=
          optnet::BaseNetworkPacket::BaseNetworkPacketType::BNPT_META_BB_PACKET)
  {
    spdlog::error("NetCalculationState - upload: invalid network packet type received");
    return metaconst::ERR_GENERIC_ERROR;
  }

  auto networkPacket = optnet::castBaseToNetworkPacket<std::string>(
          std::move(packet->networkPacket));
  if (!networkPacket)
  {
    spdlog::error("NetCalculationState - upload: error downcasting to NetworkPacket<std::string>");
    return metaconst::ERR_GENERIC_ERROR;
  }

  if (networkPacket->size() != 1)
  {
    spdlog::error("NetCalculationState - upload: wrong packet data size " +
                  std::to_string(networkPacket->size()));
    return metaconst::ERR_GENERIC_ERROR;
  }

  // Get the calculation state proto packet and upload its content into this node
  CalculationStateProtoPacket calcStateProto;
  calcStateProto.ParseFromString(networkPacket->data.at(0));

  pCompTimeMsec = calcStateProto.comptimemsec();
  pRootTimeMsec = calcStateProto.roottimemsec();
  pNodesSolved = calcStateProto.nodessolved();
  pNodesSent = calcStateProto.nodessent();
  pNumImprovedIncumbent = calcStateProto.numimprovedincumbent();
  pTerminationState = static_cast<solver::TerminationState>(calcStateProto.terminationstate());
  pNumSolvedWithNoPreprocesses = calcStateProto.numsolvedwithnopreprocess();
  pNumSimplexIterRoot = calcStateProto.numsimplexiterroot();
  pAvgSimplexIter = calcStateProto.avgsimplexiter();
  pNumTransferredLocalCuts = calcStateProto.numtransferredlocalcuts();
  pMinTransferredLocalCuts = calcStateProto.mintransferredlocalcuts();
  pMaxTransferredLocalCuts = calcStateProto.maxtransferredlocalcuts();
  pNumRestarts = calcStateProto.numrestarts();
  pMinInfeasibilitySum = calcStateProto.mininfeasibilitysum();
  pMaxInfeasibilitySum = calcStateProto.maxinfeasibilitysum();
  pMinInfeasibilityNum = calcStateProto.mininfeasibilitynum();
  pMaxInfeasibilityNum = calcStateProto.maxinfeasibilitynum();
  pDualBound = calcStateProto.dualbound();

  return metaconst::ERR_NO_ERROR;
}  // upload

optnet::Packet::UPtr NetCalculationState::buildNetworkPacket()
{
  // Create the optimizer node packet to send to the destination
  optnet::Packet::UPtr packet(new optnet::Packet());

  // Add the content as protobuf message
  CalculationStateProtoPacket calcStateProto;
  calcStateProto.set_comptimemsec(pCompTimeMsec);
  calcStateProto.set_roottimemsec(pRootTimeMsec);
  calcStateProto.set_nodessolved(pNodesSolved);
  calcStateProto.set_nodessent(pNodesSent);
  calcStateProto.set_numimprovedincumbent(pNumImprovedIncumbent);
  calcStateProto.set_terminationstate(static_cast<int>(pTerminationState));
  calcStateProto.set_numsolvedwithnopreprocess(pNumSolvedWithNoPreprocesses);
  calcStateProto.set_numsimplexiterroot(pNumSimplexIterRoot);
  calcStateProto.set_avgsimplexiter(pAvgSimplexIter);
  calcStateProto.set_numtransferredlocalcuts(pNumTransferredLocalCuts);
  calcStateProto.set_mintransferredlocalcuts(pMinTransferredLocalCuts);
  calcStateProto.set_maxtransferredlocalcuts(pMaxTransferredLocalCuts);
  calcStateProto.set_numrestarts(pNumRestarts);
  calcStateProto.set_mininfeasibilitysum(pMinInfeasibilitySum);
  calcStateProto.set_maxinfeasibilitysum(pMaxInfeasibilitySum);
  calcStateProto.set_mininfeasibilitynum(pMinInfeasibilityNum);
  calcStateProto.set_maxinfeasibilitynum(pMaxInfeasibilityNum);
  calcStateProto.set_dualbound(pDualBound);

  // Set the protobuf message as new network packet
  packet->networkPacket = optnet::BaseNetworkPacket::UPtr(
      new optnet::NetworkPacket<CalculationStateProtoPacket>(calcStateProto));

  return std::move(packet);
}  // buildNetworkPacket

}  // namespace metabb

template<> bool optnet::NetworkPacket<metabb::CalculationStateProtoPacket>::operator==(
    const NetworkPacket<metabb::CalculationStateProtoPacket>& other) const noexcept
{
  if (this->data.size() != other.data.empty()) return false;
  return google::protobuf::util::MessageDifferencer::Equals(this->data[0], other.data[0]);
}

template<> std::pair<optnet::BaseNetworkPacket::BaseNetworkPacketType, std::string>
optnet::NetworkPacket<metabb::CalculationStateProtoPacket>::serialize() const noexcept
{
  if (data.size() != 1)
  {
    spdlog::error("NetworkPacket<CalculationStateProtoPacket> - invalid data size");
    return {BaseNetworkPacketType::BNPT_UNSPECIFIED_PACKET, ""};
  }

  try
  {
    return {BaseNetworkPacketType::BNPT_META_BB_PACKET, data.at(0).SerializeAsString()};
  }
  catch(...)
  {
    spdlog::error("NetworkPacket<CalculationStateProtoPacket> - "
            "undefined error when serializing protobuf");
    return {BaseNetworkPacketType::BNPT_UNSPECIFIED_PACKET, ""};
  }
}  // serialize

}  // namespace optilab
