//
// Copyright OptiLab 2020. All rights reserved.
//
// Solver state class implemented on top of the
// optnet framework.
//

#pragma once

#include <cstdint>  // for uint64_t
#include <memory>   // for std::shared_ptr

#include "meta_bb/framework.hpp"
#include "meta_bb/solver_state.hpp"
#include "optilab_protobuf/meta_bb_packet.pb.h"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace  metabb {

class SYS_EXPORT_CLASS NetSolverState : public SolverState {
 public:
  using SPtr = std::shared_ptr<NetSolverState>;

 public:
  /// Default constructor
  NetSolverState();

  NetSolverState(bool racingStage,
                 unsigned int notificationId,
                 int lmId,
                 int globalSubtreeId,
                 long long numSolvedNodes,
                 int numNodesLeft,
                 double bestDualBoundValue,
                 double globalBestPrimalBoundValue,
                 uint64_t timeMsec,
                 double avgDualBoundGainRecv);

  /// Destructor:
  /// checks the node parent and children and re-connect parent to children
  ~NetSolverState() override;

  /// Sends this state to the destination using the given framework
  int send(const Framework::SPtr& framework, int destination) noexcept override;

  /// Uploads the content of the given network packet into this calculation state..
  /// If the given packet is not a node network packet, throws std::runtime_error
  /// Returns zero on success, non-zero otherwise
  int upload(optnet::Packet::UPtr packet) noexcept override;

 protected:
  /// Builds and returns a network packet encoding this solver state
  optnet::Packet::UPtr buildNetworkPacket();
};

}  // namespace metabb

/// Specialized template for network node packet comparison
template<> bool optnet::NetworkPacket<metabb::SolverStateProtoPacket>::operator==(
    const NetworkPacket<metabb::SolverStateProtoPacket>& other) const noexcept;

/// Specialized template to serialize NetSolverState(s)
template<> std::pair<optnet::BaseNetworkPacket::BaseNetworkPacketType, std::string>
optnet::NetworkPacket<metabb::SolverStateProtoPacket>::serialize() const noexcept;

}  // namespace optilab
