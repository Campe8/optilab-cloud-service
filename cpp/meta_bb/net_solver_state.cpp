#include "meta_bb/net_solver_state.hpp"

#include <memory>     // for std::dynamic_pointer_cast
#include <stdexcept>  // for std::invalid_argument
#include <string>
#include <utility>    // for std::move

#include <google/protobuf/util/message_differencer.h>
#include <spdlog/spdlog.h>

#include "meta_bb/meta_bb_constants.hpp"
#include "meta_bb/meta_bb_struct.hpp"
#include "meta_bb/meta_bb_utilities.hpp"
#include "optimizer_network/network_communicator.hpp"
#include "optimizer_network/packet.hpp"

namespace optilab {
namespace  metabb {

NetSolverState::NetSolverState()
: SolverState()
{
}

NetSolverState::NetSolverState(bool racingStage,
                               unsigned int notificationId,
                               int lmId,
                               int globalSubtreeId,
                               long long numSolvedNodes,
                               int numNodesLeft,
                               double bestDualBoundValue,
                               double globalBestPrimalBoundValue,
                               uint64_t timeMsec,
                               double avgDualBoundGainRecv)
: SolverState(racingStage,
              notificationId,
              lmId,
              globalSubtreeId,
              numSolvedNodes,
              numNodesLeft,
              bestDualBoundValue,
              globalBestPrimalBoundValue,
              timeMsec,
              avgDualBoundGainRecv)
{
}

NetSolverState::~NetSolverState()
{
  // No-op
}

int NetSolverState::send(const Framework::SPtr& framework, int destination) noexcept
{
  if (!framework)
  {
    spdlog::error("NetSolverState - send: empty network communicator");
    return metaconst::ERR_GENERIC_ERROR;
  }

  // Create the optimizer node packet to send to the destination
  optnet::Packet::UPtr packet = buildNetworkPacket();

  // Add the node tag
  packet->packetTag = optnet::PacketTag::UPtr(new net::MetaBBPacketTag(
          net::MetaBBPacketTag::PacketTagType::PPT_SOLVER_STATE));

  if (framework->getComm()->send(std::move(packet), destination))
  {
    return metaconst::ERR_GENERIC_ERROR;
  }

  return metaconst::ERR_NO_ERROR;
}  // send

int NetSolverState::upload(optnet::Packet::UPtr packet) noexcept
{
  if (!packet)
  {
    spdlog::error("NetSolverState - upload: empty packet");
    return metaconst::ERR_GENERIC_ERROR;
  }

  // Check that the received packet is a NODE packet
  try
  {
    const auto tag = utils::castToMetaBBPacketTagAndGetTagOrThrow(packet->packetTag);
    if (tag != net::MetaBBPacketTag::PacketTagType::PPT_SOLVER_STATE)
    {
      spdlog::error("NetSolverState - upload: invalid tag received");
      return metaconst::ERR_GENERIC_ERROR;
    }
  }
  catch(...)
  {
    spdlog::error("NetSolverState - upload: no tag received");
    return metaconst::ERR_GENERIC_ERROR;
  }

  // This packet was deserialized as a std::string packet containing
  // the bytes of the serialized original message.
  // Therefore, the packet must be first deserialized into a proper
  // NetworkPacket
  if (packet->networkPacket->packetType !=
          optnet::BaseNetworkPacket::BaseNetworkPacketType::BNPT_META_BB_PACKET)
  {
    spdlog::error("NetSolverState - upload: invalid network packet type received");
    return metaconst::ERR_GENERIC_ERROR;
  }

  auto networkPacket = optnet::castBaseToNetworkPacket<std::string>(
          std::move(packet->networkPacket));
  if (!networkPacket)
  {
    spdlog::error("NetSolverState - receive: error downcasting to NetworkPacket<std::string>");
    return metaconst::ERR_GENERIC_ERROR;
  }

  if (networkPacket->size() != 1)
  {
    spdlog::error("NetSolverState - receive: wrong packet data size " +
                  std::to_string(networkPacket->size()));
    return metaconst::ERR_GENERIC_ERROR;
  }

  // Get the calculation state proto packet and upload its content into this node
  SolverStateProtoPacket solverStateProto;
  solverStateProto.ParseFromString(networkPacket->data.at(0));
  pRacingStage = solverStateProto.racingstage();
  pNotificationId = solverStateProto.notificationid();
  pLMId = solverStateProto.lmid();
  pGlobalSubtreeIdInLM = solverStateProto.globalsubtreeidinlm();
  pNumberOfSolvedNodes = solverStateProto.numberofsolvednodes();
  pNodesLeft = solverStateProto.nodesleft();
  pBestDualBoundValue = solverStateProto.bestdualboundvalue();
  pBestGlobalPrimalBoundValue = solverStateProto.bestglobalprimalboundvalue();
  pTimeMsec = solverStateProto.timemsec();
  pAvgDualBoundGainRecv = solverStateProto.avgdualboundgainrecv();

  return metaconst::ERR_NO_ERROR;
}  // upload

optnet::Packet::UPtr NetSolverState::buildNetworkPacket()
{
  // Create the optimizer node packet to send to the destination
  optnet::Packet::UPtr packet(new optnet::Packet());

  // Add the content as protobuf message
  SolverStateProtoPacket solverStateProto;
  solverStateProto.set_racingstage(pRacingStage);
  solverStateProto.set_notificationid(pNotificationId);
  solverStateProto.set_lmid(pLMId);
  solverStateProto.set_globalsubtreeidinlm(pGlobalSubtreeIdInLM);
  solverStateProto.set_numberofsolvednodes(pNumberOfSolvedNodes);
  solverStateProto.set_nodesleft(pNodesLeft);
  solverStateProto.set_bestdualboundvalue(pBestDualBoundValue);
  solverStateProto.set_bestglobalprimalboundvalue(pBestGlobalPrimalBoundValue);
  solverStateProto.set_timemsec(pTimeMsec);
  solverStateProto.set_avgdualboundgainrecv(pAvgDualBoundGainRecv);

  // Set the protobuf message as new network packet
  packet->networkPacket = optnet::BaseNetworkPacket::UPtr(
      new optnet::NetworkPacket<SolverStateProtoPacket>(solverStateProto));

  return std::move(packet);
}  // buildNetworkPacket

}  // namespace metabb

template<> bool optnet::NetworkPacket<metabb::SolverStateProtoPacket>::operator==(
    const NetworkPacket<metabb::SolverStateProtoPacket>& other) const noexcept
{
  if (this->data.size() != other.data.empty()) return false;
  return google::protobuf::util::MessageDifferencer::Equals(this->data[0], other.data[0]);
}

template<>
std::pair<optnet::BaseNetworkPacket::BaseNetworkPacketType, std::string>
optnet::NetworkPacket<metabb::SolverStateProtoPacket>::serialize() const noexcept
{
  if (data.size() != 1)
  {
    spdlog::error("NetworkPacket<SolverStateProtoPacket> - invalid data size");
    return {BaseNetworkPacketType::BNPT_UNSPECIFIED_PACKET, ""};
  }

  try
  {
    return {BaseNetworkPacketType::BNPT_META_BB_PACKET, data.at(0).SerializeAsString()};
  }
  catch(...)
  {
    spdlog::error("NetworkPacket<SolverStateProtoPacket> - "
            "undefined error when serializing protobuf");
    return {BaseNetworkPacketType::BNPT_UNSPECIFIED_PACKET, ""};
  }
}  // serialize

}  // namespace optilab
