#include "meta_bb/linear_solver.hpp"

#include <cassert>
#include <cmath>  // for std::isnan

#include <ortools/linear_solver/model_validator.h>
#include <ortools/lp_data/mps_reader.h>

#include "meta_bb/meta_bb_constants.hpp"

namespace optilab {
namespace metabb {
extern MPSolverInterface* buildSCIPInterface(MPSolver* const solver);
}  // namespave metabb
}  // namespace optilab

namespace {
optilab::metabb::MPSolverInterface* buildSolverInterface(optilab::metabb::MPSolver* solver)
{
  assert(solver);
  switch(solver->getOptimizationPackageType())
  {
    case optilab::metabb::MPSolver::OptimizationSolverPackageType::OSPT_SCIP:
    {
      return buildSCIPInterface(solver);
    }
    default:
    {
      throw std::runtime_error("buildSolverInterface: solver package type not found " +
                               std::to_string(static_cast<int>(
                                   solver->getOptimizationPackageType())));
    }
  }
}  // buildSolverInterface
}  // namespace

namespace optilab {
namespace  metabb {

MPSolver::MPSolver(const std::string& solverName, OptimizationSolverPackageType solverPackageType)
: pSolverName(solverName),
  pSolverPackage(solverPackageType),
  pTimeoutMsec(std::numeric_limits<uint64_t>::max()),
  pNumThreads(1)
{
  // Create the actual interface to the back-end solver
  pInterface.reset(buildSolverInterface(this));

  // Create the objective given the interface
  pObjective.reset(new MPObjective(pInterface.get()));
}

MPSolver::~MPSolver()
{
  clear();
  pInterface.reset(nullptr);
}

void MPSolver::clear()
{
  // Clear all internal vectors and maps
  pObjective->clear();
  pVariableList.clear();
  pConstraintList.clear();
  pVarNameToIdxMap.clear();
  pConNameToIdxMap.clear();
  pExtractedVariableList.clear();
  pExtractedConstraintList.clear();
  pInterface->reset();
  pWarmStart.clear();
}  // clear

void MPSolver::reset()
{
  pInterface->reset();
}  // reset

bool MPSolver::interruptSolve()
{
  return pInterface->interruptSolve();
}

int MPSolver::loadMPSModelFromFile(const std::string& filePath)
{
  // Return value:
  // 0: success;
  // 1: invalid model;
  // 2: infeasible model.
  constexpr int kRetValInvalid = 1;
  if (filePath.empty()) return kRetValInvalid;

  generateVariableNameIndex();
  generateConstraintNameIndex();

  /// Instance of the ORTools protobuf model used on legacy formats
  operations_research::MPModelProto modelProto;
  const auto loadStatus = operations_research::glop::MPSReader().ParseFile(filePath, &modelProto);
  if (!loadStatus.ok())
  {
    const std::string errMsg = "MPSolver - cannot load mps file: " + loadStatus.ToString();
    spdlog::error(errMsg);
    return kRetValInvalid;
  }

  return loadModelFromProto(modelProto, true, true);
}  // loadMPSModelFromFile

int MPSolver::loadModelFromProto(const operations_research::MPModelProto& inputModel,
                                 bool clearNames, bool checkModelValidity)
{
  // Return value:
  // 0: success;
  // 1: invalid model;
  // 2: infeasible model.
  constexpr int kRetValSuccess = 0;
  constexpr int kRetValInvalid = 0;
  constexpr int kRetValInfeasible = 0;
  const std::string empty;
  if (checkModelValidity)
  {
    const std::string error = operations_research::FindErrorInMPModelProto(inputModel);
    if (!error.empty())
    {
      return error.find("Infeasible") == std::string::npos
                         ? kRetValInvalid
                         : kRetValInfeasible;
    }
  }

  MPObjective* const objective = getObjectivePtr();

  // Set variables
  for (int idx = 0; idx < inputModel.variable_size(); ++idx)
  {
    const operations_research::MPVariableProto& var_proto = inputModel.variable(idx);
    auto variable =
         buildNumVar(var_proto.lower_bound(), var_proto.upper_bound(),
                     clearNames ? empty : var_proto.name());

    variable->setInteger(var_proto.is_integer());
    objective->setCoefficient(variable, var_proto.objective_coefficient());
  }

  // Set constraints
  for (const operations_research::MPConstraintProto& ct_proto : inputModel.constraint())
  {
    if (ct_proto.lower_bound() == -infinity() &&
        ct_proto.upper_bound() == infinity())
    {
      continue;
    }

    auto ct =
        buildRowConstraint(ct_proto.lower_bound(), ct_proto.upper_bound(),
                           clearNames ? empty : ct_proto.name());
    ct->setIsLazy(ct_proto.is_lazy());
    for (int idx = 0; idx < ct_proto.var_index_size(); ++idx)
    {
      ct->setCoefficient(pVariableList[ct_proto.var_index(idx)], ct_proto.coefficient(idx));
    }
  }

  for (const operations_research::MPGeneralConstraintProto& general_constraint :
      inputModel.general_constraint())
  {
    switch (general_constraint.general_constraint_case())
    {
      case operations_research::MPGeneralConstraintProto::kIndicatorConstraint: {
        const auto& proto =
            general_constraint.indicator_constraint().constraint();
        if (proto.lower_bound() == -infinity() &&
            proto.upper_bound() == infinity()) {
          continue;
        }

        const int constraintIndex = getNumConstraints();
        auto constraint = std::shared_ptr<MPConstraint>(
            new MPConstraint(constraintIndex, proto.lower_bound(), proto.upper_bound(),
                             clearNames ? "" : proto.name(), pInterface.get()));
        pConNameToIdxMap[constraint->getName()] = constraintIndex;
        pConstraintList.push_back(constraint);
        pExtractedConstraintList.push_back(false);

        constraint->setIsLazy(proto.is_lazy());
        for (int idx = 0; idx < proto.var_index_size(); ++idx)
        {
          constraint->setCoefficient(pVariableList[proto.var_index(idx)],
                                     proto.coefficient(idx));
        }

        auto variable =
            pVariableList[general_constraint.indicator_constraint().var_index()];
        constraint->pIndicatorVariable = variable;
        constraint->pIndicatorValue =
            general_constraint.indicator_constraint().var_value();

        if (!pInterface->addIndicatorConstraint(constraint.get()))
        {
          return kRetValInvalid;
        }
        break;
      }
      default:
        return kRetValInvalid;
    }
  }

  objective->setOptimizationDirection(inputModel.maximize());
  if (inputModel.has_objective_offset())
  {
    objective->setOffset(inputModel.objective_offset());
  }

  // Stores any hints about where to start the solve.
  pWarmStart.clear();
  for (int idx = 0; idx < inputModel.solution_hint().var_index_size(); ++idx)
  {
    pWarmStart.push_back(
        std::make_pair(pVariableList[inputModel.solution_hint().var_index(idx)],
                       inputModel.solution_hint().var_value(idx)));
  }

  return kRetValSuccess;
}  // loadModelFromProtoInternal

void MPSolver::setStartingLpBasis(const std::vector<BasisStatus>& variable_statuses,
                                  const std::vector<BasisStatus>& constraint_statuses)
{
  pInterface->setStartingLPBasis(variable_statuses, constraint_statuses);
}  // setStartingLpBasis

std::shared_ptr<MPVariable> MPSolver::getVariableByName(const std::string& var) const noexcept
{
  if (pVarNameToIdxMap.empty())
  {
    generateVariableNameIndex();
  }

  auto it = pVarNameToIdxMap.find(var);
  if (it == pVarNameToIdxMap.end()) return nullptr;
  return pVariableList[it->second];
}  // getVariableByName

std::shared_ptr<MPConstraint> MPSolver::getConstraintByName(const std::string& con) const noexcept
{
  if (pConNameToIdxMap.empty())
  {
    generateConstraintNameIndex();
  }

  auto it = pConNameToIdxMap.find(con);
  if (it == pConNameToIdxMap.end()) return nullptr;
  return pConstraintList[it->second];
}  // getConstraintByName

std::shared_ptr<MPVariable> MPSolver::buildVar(double lb, double ub, bool isInt,
                                               const std::string& name)
{
  static const std::string varPrefix = "_v_";

  std::string varName = name;
  const int varIdx = getNumVariables();
  if (varName.empty())
  {
    varName = varPrefix + std::to_string(varIdx);
  }

  if (pVarNameToIdxMap.find(varName) != pVarNameToIdxMap.end())
  {
    const std::string& errMsg = "MPSolver - buildVar: variable with name " + varName +
        " already registered";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  // Build a new variable and setup the corresponding maps and lists
  pVarNameToIdxMap[varName] = varIdx;
  pExtractedVariableList.push_back(false);
  pVariableList.push_back(
      MPVariable::SPtr( new MPVariable(varIdx, lb, ub, isInt, varName, pInterface.get())));

  // Add the variable to the back-end solver through the interface
  pInterface->addVariable(pVariableList.back().get());

  // Return the newly created variable
  return pVariableList.back();
}  // buildVar

std::shared_ptr<MPVariable> MPSolver::buildNumVar(double lb, double ub, const std::string& name)
{
  return buildVar(lb, ub, false, name);
}  // buildNumVar

std::shared_ptr<MPVariable> MPSolver::buildIntVar(double lb, double ub, const std::string& name)
{
  return buildVar(lb, ub, true, name);
}  // buildIntVar

std::shared_ptr<MPVariable> MPSolver::buildBoolVar(const std::string& name)
{
  return buildVar(0.0, 1.0, true, name);
}  // buildBoolVar

std::vector<std::shared_ptr<MPVariable>> MPSolver::buildVarArray(int num, double lb, double ub,
                                                                 bool isInt,
                                                                 const std::string& name)
{
  if (num <= 0)
  {
    const std::string& errMsg = "MPSolver - buildVarArray: invalid number of variables " +
        std::to_string(num);
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  std::vector<std::shared_ptr<MPVariable>> varList;
  varList.reserve(num);
  for (int idx = 0; idx < num; ++idx)
  {
    const std::string varName = name + "_" + std::to_string(idx);
    varList.push_back(buildVar(lb, ub, isInt, varName));
  }

  return varList;
}  // buildVarArray

std::vector<std::shared_ptr<MPVariable>> MPSolver::buildNumVarArray(int num, double lb, double ub,
                                                                    const std::string& name)
{
  return buildVarArray(num, lb, ub, false, name);
}  //buildNumVarArray

std::vector<std::shared_ptr<MPVariable>> MPSolver::buildIntVarArray(int num, double lb, double ub,
                                                                    const std::string& name)
{
  return buildVarArray(num, lb, ub, true, name);
}  //buildIntVarArray

std::vector<std::shared_ptr<MPVariable>> MPSolver::buildBoolVarArray(int num,
                                                                     const std::string& name)
{
  return buildVarArray(num, 0.0, 1.0, true, name);
}  //buildBoolVarArray

std::shared_ptr<MPConstraint> MPSolver::buildRowConstraint(const std::string& name)
{
  return buildRowConstraint(-infinity(), +infinity(), name);
}  // buildRowConstraint

std::shared_ptr<MPConstraint> MPSolver::buildRowConstraint(double lb, double ub,
                                                           const std::string& name)
{
  static const std::string conPrefix = "_c_";

  std::string conName = name;
  const int conIdx = getNumConstraints();
  if (conName.empty())
  {
    conName = conPrefix + std::to_string(conIdx);
  }

  if (pConNameToIdxMap.find(conName) != pConNameToIdxMap.end())
  {
    const std::string& errMsg = "MPSolver - buildRowConstraint: variable with name " + conName +
        " already registered";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  // Build a new row constraint and setup the corresponding maps and lists
  pConNameToIdxMap[conName] = conIdx;
  pExtractedConstraintList.push_back(false);
  pConstraintList.push_back(MPConstraint::SPtr(
      new MPConstraint(conIdx, lb, ub, conName, pInterface.get())));

  // Add the constraint to the back-end solver through the interface
  pInterface->addRowConstraint(pConstraintList.back().get());

  // Return the newly created constraint
  return pConstraintList.back();
}  // buildRowConstraint

int MPSolver::computeMaxConstraintSize(int minConstraintIndex, int maxConstraintIndex) const
{
  int maxConstraintSize = 0;
  assert(minConstraintIndex >= 0);
  assert(maxConstraintIndex < getNumConstraints());

  for (int idx = minConstraintIndex; idx < maxConstraintIndex; ++idx)
  {
    auto con = pConstraintList[idx].get();
    if (static_cast<int>(con->pCoefficients.size()) > maxConstraintSize) {
      maxConstraintSize = static_cast<int>(con->pCoefficients.size());
    }
  }
  return maxConstraintSize;
}  // computeMaxConstraintSize

bool MPSolver::hasInfeasibleConstraints() const
{
  for (const auto& conPtr : pConstraintList)
  {
    if (conPtr->lowerBound() > conPtr->upperBound())
    {
      const std::string warnMsg = "Constraint " + conPtr->getName() +
          " has invalid infeasible bounds: lower bound " + std::to_string(conPtr->lowerBound()) +
          " upper bound " + std::to_string(conPtr->upperBound());
      spdlog::warn(warnMsg);
      return true;
    }
  }
  return false;
}  // hasInfeasibleConstraints

bool MPSolver::hasIntegerVariables() const
{
  for (const auto& var : pVariableList)
  {
    if (var->isInteger()) return true;
  }
  return false;
}  // hasIntegerVariables

void MPSolver::generateVariableNameIndex() const
{
  pVarNameToIdxMap.clear();
  for (const auto& var : pVariableList)
  {
    pVarNameToIdxMap[var->getName()] = var->getIndex();
  }
}  // generateVariableNameIndex

/// Generates the map from constraint names to their indices
void MPSolver::generateConstraintNameIndex() const
{
  pConNameToIdxMap.clear();
  for (const auto& var : pVariableList)
  {
    pConNameToIdxMap[var->getName()] = var->getIndex();
  }
}  // generateConstraintNameIndex

MPSolver::ResultStatus MPSolver::solve()
{
  // Solve with empty parameters,
  // i.e., default parameters
  ParamSet defaultParams;
  return solve(defaultParams);
}  // solve


MPSolver::ResultStatus MPSolver::solve(const ParamSet& params)
{
  // Check if the model is infeasible.
  // If so, return asap
  if (hasInfeasibleConstraints())
  {
    pInterface->pResultStatus = MPSolver::ResultStatus::INFEASIBLE;
    return pInterface->resultStatus();
  }

  // Let the interface solve the model and return the status
  return pInterface->solve(params);
}  // solve

bool MPSolver::clampSolutionWithinBounds()
{
  pInterface->extractModel();
  for (const auto& var : pVariableList)
  {
    const double value = var->getSolutionValue();
    if (std::isnan(value))
    {
      // Return asap if the value is NaN
      return false;
    }

    // Set proper bounds
    if (value < var->lowerBound())
    {
      var->setSolutionValue(var->lowerBound());
    }
    else if (value > var->upperBound())
    {
      var->setSolutionValue(var->upperBound());
    }
  }

  // Interface and MPSolver are now synchronized on solution.
  // Set the corresponding state
  pInterface->pSynchStatus = MPSolverInterface::SOLUTION_SYNCHRONIZED;

  // Return true on success,
  // i.e., no NaN variable values
  return true;
}  // clampSolutionWithinBounds

uint64_t MPSolver::numSimplexIterations() const
{
  return pInterface->numSimplexIterations();
}  // numSimplexIterations

uint64_t MPSolver::numBBNodes() const
{
  return pInterface->numNodes();
}  // numBBNodes

bool MPSolver::setNumThreads(int numThreads)
{
  if (numThreads < 1) return false;
  return pInterface->setNumThreads(numThreads);
}  // setNumThreads

uint64_t MPSolver::getBuildModelTimeMsec() const
{
  return pInterface->getModelLoadTimeMsec();
}  // getBuildModelTimeMsec

uint64_t MPSolver::getSolveModelTimeMsec() const
{
  return pInterface->getSolveTimeMsec();
}  // getSolveModelTimeMsec

void MPSolver::setWarmStart(std::vector<std::pair<std::shared_ptr<MPVariable>, double>> warmStart)
{
  pWarmStart = std::move(warmStart);
}  // setWarmStart

void MPVariable::setInteger(bool integer)
{
  if (pIsInteger != integer)
  {
    pIsInteger = integer;
    if (pInterface->variableIsExtracted(pIndex))
    {
      pInterface->setVariableInteger(pIndex, integer);
    }
  }
}  // setInteger

void MPVariable::setBranchingPriority(int priority)
{
  if (priority == pBranchingPriority) return;
  pBranchingPriority = priority;
  pInterface->branchingPriorityChangedForVariable(pIndex);
}  // setBranchingPriority

MPSolver::BasisStatus MPVariable::basisStatus() const
{
  if (!pInterface->isContinuous())
  {
    spdlog::error("MPVariable - basisStatus: only available for continuous problems");
    return MPSolver::FREE;
  }
  if (!pInterface->checkSolutionIsSynchronizedAndExists())
  {
    return MPSolver::FREE;
  }

  return pInterface->columnStatus(pIndex);
}  // basisStatus

void MPVariable::setBounds(double lb, double ub)
{
  const bool change = lb != pLowerBound || ub != pUpperBound;
  pLowerBound = lb;
  pUpperBound = ub;

  if (change && pInterface->variableIsExtracted(pIndex))
  {
    pInterface->setVariableBounds(pIndex, pLowerBound, pUpperBound);
  }
}  // setBounds

double MPVariable::getSolutionValue() const
{
  if (!pInterface->checkSolutionIsSynchronizedAndExists()) return 0.0;

  // If the underlying solver supports integer variables, and this is an integer
  // variable, round the solution value (i.e., clients usually expect precise
  // integer values for integer variables).
  return (pIsInteger && pInterface->isMIP()) ? round(pSolutionValue) : pSolutionValue;
}

double MPVariable::getReducedCost() const {
  if (!pInterface->isContinuous())
  {
    spdlog::error("MPVariable - getReducedCost: "
        "reduced cost only available for continuous problems");
    return 0.0;
  }
  if (!pInterface->checkSolutionIsSynchronizedAndExists()) return 0.0;
  return pReducedCost;
}  // getReducedCost

bool MPConstraint::containsNewVariables()
{
  const int lastVariableIndex = pInterface->lastVariableIndex();
  for (const auto& entry : pCoefficients)
  {
    const int varIdx = entry.first->getIndex();
    if (varIdx >= lastVariableIndex || !pInterface->variableIsExtracted(varIdx))
    {
      return true;
    }
  }
  return false;
}  // containsNewVariables

double MPConstraint::getDualValue() const
{
  if (!pInterface->isContinuous())
  {
    spdlog::error("MPVariable - getDualValue: "
        "dual cost only available for continuous problems");
    return 0.0;
  }
  if (!pInterface->checkSolutionIsSynchronizedAndExists()) return 0.0;
  return pDualValue;
}  // getDualValue

MPSolver::BasisStatus MPConstraint::basisStatus() const
{
  if (!pInterface->isContinuous())
  {
    spdlog::error("MPConstraint - basisStatus: only available for continuous problems");
    return MPSolver::FREE;
  }
  if (!pInterface->checkSolutionIsSynchronizedAndExists())
  {
    return MPSolver::FREE;
  }

  return pInterface->rowStatus(pIndex);
}  // basisStatus

void MPConstraint::clear()
{
  pInterface->clearConstraint(this);
  pCoefficients.clear();
}

void MPConstraint::setBounds(double lb, double ub)
{
  const bool change = lb != pLowerBound || ub != pUpperBound;
  pLowerBound = lb;
  pUpperBound = ub;
  if (change && pInterface->constraintIsExtracted(pIndex))
  {
    pInterface->setConstraintBounds(pIndex, pLowerBound, pUpperBound);
  }
}  // setBounds

double MPConstraint::getCoefficient(const MPVariable::SPtr& var) const
{
  if (!var) return 0.0;
  auto it = pCoefficients.find(var.get());
  if (it == pCoefficients.end()) return 0.0;
  return it->second;
}  // getCoefficient

void MPConstraint::setCoefficient(const MPVariable::SPtr& var, double coeff)
{
  if (!var) return;
  if (coeff == 0.0)
  {
    auto it = pCoefficients.find(var.get());

    // If setting a coefficient to 0 when this coefficient did not
    // exist or was already 0, do nothing: skip
    // interface_->SetCoefficient() and do not store a coefficient in
    // the map.  Note that if the coefficient being set to 0 did exist
    // and was not 0, we do have to keep a 0 in the coefficients_ map,
    // because the extraction of the constraint might rely on it,
    // depending on the underlying solver.
    if (it != pCoefficients.end() && it->second != 0.0)
    {
      const double oldValue = it->second;
      it->second = 0.0;
      pInterface->setCoefficient(this, var.get(), 0.0, oldValue);
    }
    return;
  }

  auto insertionResult = pCoefficients.insert(std::make_pair(var.get(), coeff));
  const double oldValue = insertionResult.second ? 0.0 : insertionResult.first->second;

  insertionResult.first->second = coeff;
  pInterface->setCoefficient(this, var.get(), coeff, oldValue);
}  // setCoefficient

double MPObjective::getCoefficient(const MPVariable::SPtr& var) const
{
  if (!var) return 0.0;
  auto it = pCoefficients.find(var.get());
  if (it == pCoefficients.end()) return 0.0;
  return it->second;
}  // getCoefficient

void MPObjective::setCoefficient(const MPVariable::SPtr& var, double coeff)
{
  if (!var) return;
  if (coeff == 0.0)
  {
    auto it = pCoefficients.find(var.get());
    if (it == pCoefficients.end() || it->second == 0.0) return;
    it->second = 0.0;
  }
  else
  {
    pCoefficients[var.get()] = coeff;
  }
  pInterface->setObjectiveCoefficient(var.get(), coeff);
}  // setCoefficient

void MPObjective::setOffset(double value)
{
  pOffset = value;
  pInterface->setObjectiveOffset(pOffset);
}  // setOffset

void MPObjective::clear()
{
  pInterface->clearObjective();
  pCoefficients.clear();
  pOffset = 0.0;

  // Default set minimization
  setMinimization();
}  // clear

void MPObjective::setOptimizationDirection(bool maximize)
{
  pInterface->pMaximize = maximize;
  pInterface->setOptimizationDirection(maximize);
}  // setOptimizationDirection

bool MPObjective::maximization() const { return pInterface->pMaximize; }

bool MPObjective::minimization() const { return !pInterface->pMaximize; }

double MPObjective::getValue() const
{
  return pInterface->objectiveValue();
}  // getValue

double MPObjective::getBestBound() const
{
  return pInterface->bestObjectiveBound();
}  // getBestBound

MPSolverInterface::MPSolverInterface(MPSolver* const solver)
: pSolver(solver),
  pSynchStatus(SynchronizationStatus::MODEL_SYNCHRONIZED),
  pResultStatus(MPSolver::NOT_SOLVED),
  pMaximize(false),
  pLastConstraintIndex(0),
  pLastVariableIndex(0),
  pObjectiveValue(0.0)
{
}

double MPSolverInterface::trivialWorstObjectiveBound() const
{
  return pMaximize ? -std::numeric_limits<double>::infinity() :
      std::numeric_limits<double>::infinity();
}  // trivialWorstObjectiveBound

double MPSolverInterface::objectiveValue() const
{
  if (!checkSolutionIsSynchronizedAndExists()) return 0;
  return pObjectiveValue;
}  // objectiveValue

bool MPSolverInterface::checkSolutionIsSynchronized() const
{
  if (pSynchStatus != SynchronizationStatus::SOLUTION_SYNCHRONIZED)
  {
    spdlog::warn("MPSolverInterface - checkSolutionIsSynchronized: "
        "the model has been changed since the solution was last computed");
    return false;
  }
  return true;
}  // checkSolutionIsSynchronized

bool MPSolverInterface::checkSolutionExists() const
{
  if (pResultStatus != MPSolver::OPTIMAL && pResultStatus != MPSolver::FEASIBLE)
  {
    return false;
  }
  return true;
}  // checkSolutionExists

bool MPSolverInterface::checkBestObjectiveBoundExists() const
{
  if (pResultStatus != MPSolver::OPTIMAL && pResultStatus != MPSolver::FEASIBLE)
  {
    return false;
  }
  return true;
}  // checkBestObjectiveBoundExists

void MPSolverInterface::extractModel()
{
  switch (pSynchStatus) {
    case SynchronizationStatus::MUST_RELOAD: {
      extractNewVariables();
      extractNewConstraints();
      extractObjective();

      pLastConstraintIndex = static_cast<int>(pSolver->pConstraintList.size());
      pLastVariableIndex = static_cast<int>(pSolver->pVariableList.size());
      pSynchStatus = SynchronizationStatus::MODEL_SYNCHRONIZED;
      break;
    }
    case SynchronizationStatus::MODEL_SYNCHRONIZED: {
      // Everything has already been extracted
      break;
    }
    case SynchronizationStatus::SOLUTION_SYNCHRONIZED: {
      // Nothing has changed since last solve
      break;
    }
  }
}  // extractModel

void MPSolverInterface::resetExtractionInformation()
{
  pSynchStatus = SynchronizationStatus::MUST_RELOAD;
  pLastConstraintIndex = 0;
  pLastVariableIndex = 0;
  pSolver->pExtractedVariableList.assign(pSolver->pVariableList.size(), false);
  pSolver->pExtractedConstraintList.assign(pSolver->pConstraintList.size(), false);
}  // resetExtractionInformation

void MPSolverInterface::invalidateSolutionSynchronization()
{
  if (pSynchStatus == SynchronizationStatus::SOLUTION_SYNCHRONIZED)
  {
    pSynchStatus = SynchronizationStatus::MODEL_SYNCHRONIZED;
  }
}  // invalidateSolutionSynchronization

void MPSolverInterface::setCommonParameters(const ParamSet& param)
{
  setPrimalTolerance(param.getSolverParamset().primaltolerancevalue());
  setDualTolerance(param.getSolverParamset().dualtolerancevalue());
  setPresolveMode(param.getSolverParamset().presolvevalue());
  int value = param.getSolverParamset().lpalgorithmvalue();
  if (value != solverparamset::DEFAULT_INTEGER_PARAM_VALUE)
  {
    setLPAlgorithm(value);
  }
}  // setCommonParameters

void MPSolverInterface::setMIPParameters(const ParamSet& param)
{
  setRelativeMipGap(param.getSolverParamset().relativemipgapvalue());
}  // setMIPParameters

}  // namespace metabb
}  // namespace optilab
