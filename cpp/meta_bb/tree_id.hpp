//
// Copyright OptiLab 2019. All rights reserved.
//
// Tree and node identifier classes.
//

#pragma once

#include <cstddef>     // for std::size_t
#include <memory>      // for std::shared_ptr

#include <boost/functional/hash.hpp>

#include "meta_bb/meta_bb_constants.hpp"
#include "system/system_export_defs.hpp"

// Forward declarations
namespace optilab {
namespace  metabb {
class Node;
}  // metabb
}  // optilab

namespace optilab {
namespace  metabb {

/**
 * Unique identifier for a sub-tree of the
 * branch and bound generated tree.
 */
class SYS_EXPORT_CLASS SubtreeId {
 public:
  /// Default constructor
  SubtreeId() = default;

  /// Constructor:
  /// - lmId: load manager identifier that created this sub-tree
  /// - glbSubtreeId: id used by the load manager to identify this sub-tree
  /// - solverId: id of the solver owning this sub-tree
  SubtreeId(int lmId, int glbSubtreeId, int solverId)
  : pLMId(lmId), pGlobalSubtreeIdInLM(glbSubtreeId), pSolverId(solverId)
  {
  }

  SubtreeId(const SubtreeId& other) = default;
  SubtreeId(SubtreeId&& other) = default;

  ~SubtreeId() = default;

  SubtreeId& operator=(SubtreeId&& other) = default;
  SubtreeId& operator=(const SubtreeId& other) = default;

  bool operator==(const SubtreeId& other) const
  {
    return (pLMId == other.pLMId) &&
        (pGlobalSubtreeIdInLM == other.pGlobalSubtreeIdInLM) &&
        (pSolverId == other.pSolverId);
  }

  bool operator!=(const SubtreeId& other) const
  {
    return (pLMId != other.pLMId) ||
        (pGlobalSubtreeIdInLM != other.pGlobalSubtreeIdInLM) ||
        (pSolverId != other.pSolverId);
  }

  bool operator<(const SubtreeId& other) const
  {
    if (pLMId < other.pLMId) return true;
    if (pLMId == other.pLMId &&
        pGlobalSubtreeIdInLM < other.pGlobalSubtreeIdInLM) return true;
    if (pLMId == other.pLMId &&
        pGlobalSubtreeIdInLM == other.pGlobalSubtreeIdInLM &&
        pSolverId < other.pSolverId) return true;
    return false;
  }

  /// Return the Load Manager identifier that created this sub-tree
  inline int getLMId() const noexcept { return pLMId; }
  inline int& getLMId() noexcept { return pLMId; }

  /// Returns the sub-tree identifier used by the load manager to identify this subtree
  inline int getGlobalSubstreeIdInLM() const noexcept { return pGlobalSubtreeIdInLM; }
  inline int& getGlobalSubstreeIdInLM() noexcept { return pGlobalSubtreeIdInLM; }

  /// Returns the solver's id owning this sub-tree
  inline int getSolverId() const noexcept { return pSolverId; }
  inline int& getSolverId() noexcept { return pSolverId; }

 private:
  /// Identifier of the load manager
  int pLMId{treeconsts::DEFAULT_UNSPECIFIED_ID};

  /// Global subtree identifier managed by the load manager
  int pGlobalSubtreeIdInLM{treeconsts::DEFAULT_UNSPECIFIED_ID};

  /// Identifier of the solver owning this subtree
  int pSolverId{treeconsts::DEFAULT_UNSPECIFIED_ID};
};

/**
 * Identifier for a branch and bound node
 * of the branch-and-bound tree.
 */
class SYS_EXPORT_CLASS NodeId {
 public:
  /// Default constructor
  NodeId() = default;

  /// Constructor:
  /// - subtreeId: sequence number uniquely identifying this node in the search tree
  /// - subtreeId: identifier of the subtree this node belongs to
  NodeId(long long seqIdNum, const SubtreeId& subtreeId)
  : pSeqNum(seqIdNum), pSubtreeId(subtreeId)
  {
  }

  NodeId(const NodeId& other) = default;
  NodeId(NodeId&& other) = default;

  ~NodeId() = default;

  NodeId& operator=(NodeId&& other) = default;
  NodeId& operator=(const NodeId& other) = default;

  bool operator==(const NodeId& other) const
  {
    return pSubtreeId == other.pSubtreeId &&
        pSeqNum == other.pSeqNum;
  }

  bool operator!=(const NodeId& other) const
  {
    return pSubtreeId != other.pSubtreeId ||
        pSeqNum != other.pSeqNum;
  }

  bool operator<(const NodeId& other) const
  {
    if (pSubtreeId < other.pSubtreeId) return true;
    if (pSubtreeId == other.pSubtreeId && pSeqNum < other.pSeqNum) return true;
    return false;
  }

  /// Returns the sequence number identifier of this node
  inline long long getSeqNum() const noexcept { return pSeqNum; }
  inline long long& getSeqNum() noexcept { return pSeqNum; }

  /// Returns the subtree identifier this node belongs to
  inline const SubtreeId& getSubtreeId() const noexcept { return pSubtreeId; }
  inline SubtreeId& getSubtreeId() noexcept { return pSubtreeId; }

 private:
  /// Sequential number in the subtree
  long long pSeqNum{treeconsts::DEFAULT_UNSPECIFIED_ID};

  /// Subtree id this node belongs to
  SubtreeId pSubtreeId;
};

/**
 * Class encapsulating a branch and bound tree node relationship with
 * parents and children.
 */
class NodeGenealogicalRel {
 public:
  /// Relationship type
  enum NodeGeneaLogicalRelType {
    /// Local means on the same machine
    NGLRT_LOCAL,

    /// Remote means that the relationship is with a node
    /// present on a different machine
    NGLRT_REMOTE
  };

  using SPtr = std::shared_ptr<NodeGenealogicalRel>;

public:
  virtual ~NodeGenealogicalRel() = default;

  /// Returns the type of this relationship (e.g., local or remote)
  inline NodeGeneaLogicalRelType getType() const noexcept { return pType; }

  /// Returns the node id of this relationship.
  /// @note the node id is the identifier of the node in relationship
  /// with the node holding this relationship.
  /// For example, if a child B (even not direct child) of a node A,
  /// have this NodeGenealogicalRel as relationship,
  /// then B's NodeGenealogicalRel NodeId would be A, and A would have a
  /// descendant NodeGenealogicalRel with NodeId B
  inline const NodeId& getNodeId() const noexcept { return pGenealogicalNodeId; }
  inline NodeId& getNodeId() noexcept { return pGenealogicalNodeId; }

protected:
  NodeGenealogicalRel(NodeId nodeId, NodeGeneaLogicalRelType type)
  : pGenealogicalNodeId(nodeId), pType(type) {}

private:
   /// Descendant (child) or ascendant (parent) NodeId
   NodeId pGenealogicalNodeId;

   /// Type of node
   NodeGeneaLogicalRelType pType;
};

class NodeGenealogicalLocalRel : public NodeGenealogicalRel {
public:
  using SPtr = std::shared_ptr<NodeGenealogicalLocalRel>;

public:
  NodeGenealogicalLocalRel()
  : NodeGenealogicalRel(NodeId(), NodeGeneaLogicalRelType::NGLRT_LOCAL)
  {}

  NodeGenealogicalLocalRel(const NodeId& nodeId, const std::shared_ptr<Node>& node)
  : NodeGenealogicalRel(nodeId, NodeGeneaLogicalRelType::NGLRT_LOCAL),
    pNode(node)
  {}

  /// Returns true if the relation has a value, returns false otherwise
  inline bool hasValue() const noexcept { return !pNode.expired(); }

  /// Returns the pointer to the node in relation with the node holding this relationship
  inline std::shared_ptr<Node> getValue() const noexcept { return pNode.lock(); }

private:
  std::weak_ptr<Node> pNode;
};

class NodeGenealogicalRemoteRel : public NodeGenealogicalRel {
public:
  using SPtr = std::shared_ptr<NodeGenealogicalRemoteRel>;

public:
  NodeGenealogicalRemoteRel()
  : NodeGenealogicalRel(NodeId(), NodeGeneaLogicalRelType::NGLRT_REMOTE)
  {}

  NodeGenealogicalRemoteRel(const NodeId& nodeId, int loadManagerId)
  : NodeGenealogicalRel(nodeId, NodeGeneaLogicalRelType::NGLRT_REMOTE),
    pTransferringLMId(loadManagerId)
  {}

  /// Returns true if the relation has a value, returns false otherwise
  inline bool hasValue() const noexcept { return pTransferringLMId >= 0; }

  /// Returns the transferring load manager identifier since this is
  /// a remote relationship
  inline int getValue() const noexcept { return pTransferringLMId; }

private:
  /// Load manager identifier transfer to or transferred from
  int pTransferringLMId{-1};
};

}  // namespace metabb
}  // namespace optilab

// Hash function specialization to use for map data structures using
// node ids as map keys
namespace std {
template <> struct hash<optilab::metabb::NodeId>
{
  std::size_t operator()(const optilab::metabb::NodeId& nodeId) const
  {
    std::size_t seed = 0;
    boost::hash_combine(seed, nodeId.getSeqNum());
    boost::hash_combine(seed, nodeId.getSubtreeId().getLMId());
    boost::hash_combine(seed, nodeId.getSubtreeId().getGlobalSubstreeIdInLM());
    boost::hash_combine(seed, nodeId.getSubtreeId().getSolverId());
    return seed;
  }
};
}  // namespace std

