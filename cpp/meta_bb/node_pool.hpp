//
// Copyright OptiLab 2019-2020. All rights reserved.
//
// Pool of nodes.
//

#pragma once

#include <algorithm>  // for std::max
#include <cassert>
#include <cmath>    // for fabs
#include <cstddef>  // for std::size_t
#include <cstdlib>  // for rand
#include <limits>   // for std::numeric_limits
#include <list>
#include <map>
#include <memory>   // for std::shared_ptr
#include <stdexcept>  // for std::runtime_error
#include <utility>  // for std::make_pair

#include "meta_bb/meta_bb_constants.hpp"
#include "meta_bb/meta_bb_macros.hpp"
#include "meta_bb/meta_bb_struct.hpp"
#include "meta_bb/node.hpp"
#include "system/system_export_defs.hpp"

namespace {
constexpr double keps{1.0e-12};
constexpr double kPosInf{std::numeric_limits<double>::max()};
}  // namespace

namespace optilab {
namespace  metabb {

class NodeSortCriterion {
public:
  // Sort node by their dual bound value in increasing order.
  // @note break ties with the number of bound changes
  bool operator()(const Node::SPtr& n1, const Node::SPtr& n2) const
  {
    return EPSLT(n1->getDualBoundValue(), n2->getDualBoundValue(), keps) ||
            (EPSEQ(n1->getDualBoundValue(), n2->getDualBoundValue(), keps) &&
                    n1->getDiffSubproblem() && n2->getDiffSubproblem() &&
                    n1->getDiffSubproblem()->getNBoundChanges() <
                    n2->getDiffSubproblem()->getNBoundChanges());

   }
};

class NodeSortCriterionForCleanUp {
public:
  // Sort node by their dual bound value in decreasing order.
  // @note break ties with the number of bound changes
  bool operator()(const Node::SPtr& n1, const Node::SPtr& n2) const
  {
    return EPSGT(n1->getDualBoundValue(),n2->getDualBoundValue(),keps) ||
            (EPSEQ(n1->getDualBoundValue(),n2->getDualBoundValue(),keps) &&
                    n1->getDiffSubproblem() && n2->getDiffSubproblem() &&
                    n1->getDiffSubproblem()->getNBoundChanges() >
                    n2->getDiffSubproblem()->getNBoundChanges());

   }
};
}  // namespace metabb
}  // namespace optilab

namespace optilab {
namespace  metabb {

class SYS_EXPORT_CLASS NodePool {
 public:
  using SPtr = std::shared_ptr<NodePool>;

 public:
  /// Constructor
  /// - bGap: threshold value of gap for good nodes.
  ///   Bound gap "bGap" to identify good nodes. A node is good when:
  ///   (bound_value - best_bound_value) / best_bound_value < "bGap".
  explicit NodePool(double bGap) : pBGap(bGap) {}

  virtual ~NodePool() = default;

  /// Frees the node pool
  virtual void freePool() = 0;

  /// Insert the given node in the pool
  virtual void insert(Node::SPtr node) = 0;

  /// Returns true if the pool of nodes is empty, false otherwise
  virtual bool isEmpty() const noexcept = 0;

  /// Returns a node from the pool
  virtual Node::SPtr extractNode() = 0;

  /// Returns a random node from the pool
  virtual Node::SPtr extractNodeRandomly() = 0;

  /// Returns the best dual bound found so far
  virtual double getBestDualBoundValue() const noexcept = 0;

  /// Returns the number of "good nodes",
  /// i.e., the number of nodes that have dual bound "close" to the best global
  /// dual bound value.
  /// @note a node is "good" if:
  /// node's dual bound ~ global dual bound
  virtual std::size_t getNumOfGoodNodes(double globalBestBound) const noexcept = 0;

  /// Returns the number of nodes in the pool
  virtual std::size_t getNumNodes() const noexcept = 0;

  /// Removes from the pool the nodes that are "bounded" to the given
  /// incumbent value, i.e., the nodes not worth expanding.
  /// Returns the number of deleted nodes
  virtual int removeBoundedNodes(double incumbentValue) = 0;

  virtual void updateDualBoundsForSavingNodes() = 0;

  /// Removes the merged nodes from this pool.
  /// @note if given a nullptr, returns 0
  virtual int removeMergedNodes(std::list<Node*>& nodeList) = 0;

  /// Returns the maximum usage of the pool
  inline std::size_t getMaxUsageOfPool() const noexcept { return pMaxUsageOfPool; }

 protected:
  /// bGap value for good nodes
  double pBGap{0};

  /// Keep track of the maximum usage of this pool
  std::size_t pMaxUsageOfPool{0};
};

class SYS_EXPORT_CLASS NodePoolForMinimization : public NodePool {
 public:
  NodePoolForMinimization(double bGap) : NodePool(bGap) {}

  ~NodePoolForMinimization()
  {
    freePool();
  }

  void freePool() override
  {
    if (!pAscendingPool.empty())
    {
      auto it = pAscendingPool.begin();
      for (;it != pAscendingPool.end();)
      {
        if (it->second) (it->second).reset();
        pAscendingPool.erase(it++);
       }
    }

    pAscendingPool.clear();
  }

  void insert(Node::SPtr node) override
  {
    pAscendingPool.insert(std::make_pair(node, node));
    if (pMaxUsageOfPool < pAscendingPool.size())
    {
      pMaxUsageOfPool = pAscendingPool.size();
    }
  }

  bool isEmpty() const noexcept override { return pAscendingPool.empty(); }

  /// Returns a node from the pool
  Node::SPtr extractNode() override
  {
    Node::SPtr extracted;
    auto it = pAscendingPool.begin();
    while (it != pAscendingPool.end())
    {
      if (it->second->getMergeNodeInfo())
      {
        // Node is merging
        assert(it->second->getMergeNodeInfo()->status !=
                mergenodes::MergeNodeInfo::Status::STATUS_MERGING);
        if (it->second->getMergingStatus() ==
                Node::NodeMergingStatus::NMS_MERGING_REPRESENTATIVE_WAS_DELETED)
        {
          pAscendingPool.erase(it);
        }
        else
        {
          if (it->second->getMergeNodeInfo()->status ==
                  mergenodes::MergeNodeInfo::Status::STATUS_MERGE_CHECKING_TO_OTHER_NODE)
          {
            auto mergedTo = it->second->getMergeNodeInfo()->mergedTo;
            assert(mergedTo->status ==
                    mergenodes::MergeNodeInfo::Status::STATUS_MERGED_REPRESENTATIVE);
            it++;
          }
          else
          {
            extracted = it->second;
            pAscendingPool.erase(it);
            break;
          }
        }
      }
      else
      {
        // No merging node
        extracted = it->second;
        pAscendingPool.erase(it);
        break;
      }
    }
    return extracted;
  }

  /// Returns a random node from the pool
  Node::SPtr extractNodeRandomly() override
  {
    Node::SPtr extracted;
    auto it = pAscendingPool.begin();

    auto numNodes = pAscendingPool.size();
    if (numNodes == 0)
    {
      return extracted;
    }

    int pos = 0;
    if (numNodes > 10)
    {
      // Pick a random node
      pos = rand() % static_cast<int>(numNodes * 0.8) + static_cast<int>(numNodes * 0.1);
      for (int ctr = 0; ctr < pos; ctr++) it++;
    }
    assert(it != pAscendingPool.end());

    if (it->second->getMergeNodeInfo())
    {
      // Node is merging
      assert(it->second->getMergeNodeInfo()->status !=
              mergenodes::MergeNodeInfo::Status::STATUS_MERGING);
      if (it->second->getMergingStatus() ==
              Node::NodeMergingStatus::NMS_MERGING_REPRESENTATIVE_WAS_DELETED)
      {
        pAscendingPool.erase(it);
      }
      else
      {
        if (it->second->getMergeNodeInfo()->status ==
                mergenodes::MergeNodeInfo::Status::STATUS_MERGE_CHECKING_TO_OTHER_NODE)
        {
          auto it2 = pAscendingPool.begin();
          while (it2 != it)
          {
            auto mergedTo = it->second->getMergeNodeInfo()->mergedTo;
            auto mergedToNode = mergedTo->node;
            if ((it2->second).get() == mergedToNode)
            {
               extracted = it2->second;
               pAscendingPool.erase(it2);
               break;
            }
            it2++;
          }
        }
        else
        {
          extracted = it->second;
          pAscendingPool.erase(it);
        }
      }
    }
    else
    {
      extracted = it->second;
      pAscendingPool.erase(it);
    }

    // If no node was randomly found,
    // check nodes from the head of this pool
    return (!!extracted) ? extracted : extractNode();
  }

  void updateDualBoundsForSavingNodes() override
  {
    // For all parent nodes, update the dual bound
    // to the best dual bound among all children
    for (auto& it : pAscendingPool)
    {
      if (!it.second->getParent())
      {
        it.second->updateInitialDualBoundToSubtreeDualBound();
      }
    }
  }

  double getBestDualBoundValue() const noexcept override
  {
    if (isEmpty())
    {
      // No node exists, there is no lower bound
      return kPosInf;
    }

    // Pick the first node in the pool which has the best bound
    // @note nodes are sorted
    const auto it = pAscendingPool.begin();
    return it->second->getDualBoundValue();
  }

  std::size_t getNumOfGoodNodes(double globalBestBound) const noexcept override
  {
    std::size_t num{0};
    for (auto it = pAscendingPool.begin();
            it != pAscendingPool.end() &&
                    (((it->second->getDualBoundValue()) - globalBestBound) /
                            std::max(fabs(globalBestBound), 1.0)) < pBGap; ++it)
    {
      num++;
    }
    return num;
  }  // getNumOfGoodNodes

  std::size_t getNumNodes() const noexcept override
  {
    return pAscendingPool.size();
  }

  int removeBoundedNodes(double incumbentValue) override
  {
    int numDeleted{0};
    if (isEmpty()) return numDeleted;

    for (auto it = pAscendingPool.begin(); it != pAscendingPool.end();)
    {
      assert(it->second);
      if (!(it->second->getMergeNodeInfo()))
      {
        // No merging node, easy delete
        if (it->second->getDualBoundValue() > incumbentValue ||
                it->second->getMergingStatus() ==
                        Node::NodeMergingStatus::NMS_MERGING_REPRESENTATIVE_WAS_DELETED)
        {
          numDeleted++;
          pAscendingPool.erase(it++);
        }
        else
        {
          it++;
        }
      }
      else
      {
        // There is a merging status: delete only if "checking to other node:
        if (it->second->getMergeNodeInfo()->status ==
                mergenodes::MergeNodeInfo::Status::STATUS_MERGE_CHECKING_TO_OTHER_NODE)
        {

          if (it->second->getDualBoundValue() > incumbentValue ||
                  it->second->getMergingStatus() ==
                          Node::NodeMergingStatus::NMS_MERGING_REPRESENTATIVE_WAS_DELETED)
          {
            numDeleted++;
            pAscendingPool.erase(it++);
          }
          else
          {
            it++;
          }
        }
        else
        {
          it++;
        }
      }
    }

    return numDeleted;
  }  // removeBoundedNodes

  /// Removes the merged nodes from this pool
  int removeMergedNodes(std::list<Node*>& nodeList) override
  {
    int numDeleted{0};
    assert(!isEmpty());

    int ctr{0};
    for (auto it = pAscendingPool.begin(); it != pAscendingPool.end();)
    {
      // Break if all nodes in the list have been visited
      if (nodeList.empty()) break;

      // @note remove only ALREADY merged nodes
      assert(it->second);
      auto nodeFromPool = it->second;
      if (nodeFromPool->getMergingStatus() ==
              Node::NodeMergingStatus::NMS_MERGED_TO_THE_OTHER_NODE)
      {
        const auto listLenOld = nodeList.size();
        nodeList.remove(nodeFromPool.get());

        if (nodeList.size() < listLenOld)
        {
          pAscendingPool.erase(it++);
          numDeleted++;
        }
        else
        {
          it++;
        }
      }
      else
      {
        it++;
      }
    }

    return numDeleted;
  }  // removeMergedNodes

 private:
  // Actual node pool stored as an ascending map,
  // sorted by dual bound values of the nodes
  std::multimap<Node::SPtr, Node::SPtr, NodeSortCriterion> pAscendingPool;
};

class SYS_EXPORT_CLASS NodePoolForCleanUp : virtual public NodePool {
public:
  NodePoolForCleanUp(double bGap) : NodePool(bGap) {}

  ~NodePoolForCleanUp()
  {
    freePool();
  }

  void freePool() override
  {
    if (!pDescendingPool.empty())
    {
      auto it = pDescendingPool.begin();
      for (;it != pDescendingPool.end();)
      {
        if (it->second) (it->second).reset();
        pDescendingPool.erase(it++);
       }
    }

    pDescendingPool.clear();
  }

  void insert(Node::SPtr node) override
  {
    pDescendingPool.insert(std::make_pair(node, node));
    if (pMaxUsageOfPool < pDescendingPool.size())
    {
      pMaxUsageOfPool = pDescendingPool.size();
    }
  }

  bool isEmpty() const noexcept override { return pDescendingPool.empty(); }

  Node::SPtr extractNode() override
  {
    Node::SPtr extracted;
    auto it = pDescendingPool.begin();
    while (it != pDescendingPool.end())
    {
      if (it->second->getMergeNodeInfo())
      {
        throw std::runtime_error("NodePoolForCleanUp - extractNode: "
                "node merging is used in clean-up process");
      }
      extracted = it->second;
      pDescendingPool.erase(it);
      break;
    }

    return extracted;
  }

  Node::SPtr extractNodeRandomly() override
  {
    return extractNode();
  }

  void updateDualBoundsForSavingNodes() override
  {
    // For all parent nodes, update the dual bound
    // to the best dual bound among all children
    for (auto& it : pDescendingPool)
    {
      if (!it.second->getParent())
      {
        it.second->updateInitialDualBoundToSubtreeDualBound();
      }
    }
  }

  double getBestDualBoundValue() const noexcept override
  {
    if (isEmpty())
    {
      // No node exists, there is no lower bound
      return kPosInf;
    }

    // Pick the last node in the pool which has the best bound
    // @note nodes are sorted
    const auto it = pDescendingPool.rbegin();
    return it->second->getDualBoundValue();
  }

  std::size_t getNumOfGoodNodes(double globalBestBound) const noexcept override
  {
    std::size_t num{0};
    for (auto it = pDescendingPool.rbegin();
            it != pDescendingPool.rend() &&
                    (((it->second->getDualBoundValue()) - globalBestBound) /
                            std::max(fabs(globalBestBound), 1.0)) < pBGap; ++it)
    {
      num++;
    }
    return num;
  }  // getNumOfGoodNodes

  std::size_t getNumNodes() const noexcept override
  {
    return pDescendingPool.size();
  }

  int removeBoundedNodes(double incumbentValue) override
  {
    int numDeleted{0};
    if (pDescendingPool.empty()) return numDeleted;

    for (auto it = pDescendingPool.begin(); it != pDescendingPool.end(); )
    {
      assert(it->second);
      if (!it->second->getMergeNodeInfo())
      {
        if (it->second->getDualBoundValue() > incumbentValue)
        {
          numDeleted++;
          pDescendingPool.erase(it++);
        }
        else
        {
          it++;
        }
      }
      else
      {
        throw std::runtime_error("NodePoolForCleanUp - removeBoundedNodes: "
                "node merging is used in clean-up process");
      }
    }

    return numDeleted;
  }

  int removeMergedNodes(std::list<Node*>&) override
  {
    throw std::runtime_error("NodePoolForCleanUp - removeMergedNodes: "
            "node merging is used in clean-up process");
  }

 private:
  // Actual node pool stored as a descending map,
  // sorted by dual bound values of the nodes
  std::multimap<Node::SPtr, Node::SPtr, NodeSortCriterionForCleanUp> pDescendingPool;
};

}  // namespace metabb
}  // namespace optilab
