#include "meta_bb/solver_pool.hpp"

#include <algorithm>  // for std::min
#include <cassert>
#include <stdexcept>  // for std::invalid_argument
#include <string>
#include <utility>    // for std::make_pair

#include "meta_bb/meta_bb_struct.hpp"
#include "meta_bb/meta_bb_utilities.hpp"

#define SOLVER_POOL_INDEX(rank) (rank - pOriginRank)

namespace optilab {
namespace  metabb {

void SolverPoolElement::activate(Node::SPtr node)
{
  pSolverStatus = solver::SolverStatus::SS_ACTIVE;
  pCurrentNode = node;
  if (pCurrentNode)
  {
    // Set the best dual bound value, if any
    pBestDualBoundValue = node->getDualBoundValue();
  }
  else
  {
    pBestDualBoundValue = metaconst::DOUBLE_NEG_INF;
  }

  pNumOfNodesSolved = 0;

  // At least one node left (the current one)
  pNumOfNodesLeft = 1;
}  // activate

void SolverPoolElement::racingActivate()
{
  pSolverStatus = solver::SolverStatus::SS_RACING;
  pCurrentNode = nullptr;
  pNumOfNodesSolved = -1;
  pBestDualBoundValue =  metaconst::DOUBLE_NEG_INF;

  // At least one node left (the current one)
  pNumOfNodesLeft = 1;
}  // racingActivate

void SolverPoolElement::deactivate()
{
  pSolverStatus = solver::SolverStatus::SS_INACTIVE;
  if (pCurrentNode)
  {
    pCurrentNode = nullptr;
  }

  pCollectingMode = false;
  pCandidateOfCollecting = false;
  pCollectingIsProhibited = false;
  pDualBoundGainTesting = false;

   /** do not touch "generator" **/
  pNumOfNodesSolved = 0;
  pNumOfNodesLeft = 0;
  pNumOfDiffNodesLeft = 0;
  pBestDualBoundValue = 0.0;
}  // deactivate

Node::SPtr SolverPoolElement::kill()
{
  pSolverStatus = solver::SolverStatus::SS_DEAD;
  pCollectingMode = false;
  pCandidateOfCollecting = false;
  pCollectingIsProhibited = false;
  pDualBoundGainTesting = false;

  pNumOfNodesSolved = 0;
  pNumOfNodesLeft = 0;
  pNumOfDiffNodesLeft = 0;
  pBestDualBoundValue = 0.0;
  auto node = pCurrentNode;
  pCurrentNode.reset();
  return node;
}  // kill

void SolverPoolElement::setDualBoundValue(double dualBoundValue)
{
  if (!pCurrentNode)
  {
    throw std::runtime_error("SolverPoolElement - setDualBoundValue: no active node");
  }
  pCurrentNode->setDualBoundValue(dualBoundValue);
}


SelectionHeap::SelectionHeap(int size, bool isCollectingHeap)
: isCollectingHeap(isCollectingHeap),
  pMaxHeapSize(size),
  pHeapSize(0)
{
  resize(size);
}

void SelectionHeap::resize(int size)
{
  // @note index 0 is used as a sentinel
  pHeap.resize(size + 1);
  pMaxHeapSize = size;
  pHeapSize = std::min(pHeapSize, size);
}  // resize

void SelectionHeap::upHeap(int pos)
{
  upHeapImpl(pos, isCollectingHeap);
}  // upHeap


void SelectionHeap::downHeap(int pos)
{
  downHeapImpl(pos, isCollectingHeap);
}  // downHeap

SelectionHeap::ResultOfInsert SelectionHeap::insert(SolverPoolElement::SPtr solver)
{
   if (pHeapSize >= pMaxHeapSize)
   {
     return SelectionHeap::ResultOfInsert::ROI_FAILED_BY_FULL;
   }

   // Insert the solver
   assert((pHeapSize + 1) < static_cast<int>(pHeap.size()));
   pHeap[++pHeapSize] = solver;

   // heapify
   upHeap(pHeapSize);
   return SelectionHeap::ResultOfInsert::ROI_SUCCEEDED;
}  // insert

SolverPoolElement::SPtr SelectionHeap::top() const noexcept
{
  if (getHeapSize() == 0)
  {
    return SolverPoolElement::SPtr();
  }
  return pHeap.at(1);
}

SolverPoolElement::SPtr SelectionHeap::remove()
{
  SolverPoolElement::SPtr solver = pHeap[1];

  pHeap[1] = pHeap[pHeapSize];
  pHeapSize--;

  // Heapify
  downHeap(1);
  if (pHeapSize == 0)
  {
    pHeap[1].reset();
  }

  if (isCollectingHeap)
  {
    solver->setCollectingModeSolverHeapElement(0);
  }
  else
  {
    solver->setSelectionHeapElement(0);
  }

  return solver;
}  // remove

DescendingSelectionHeap::DescendingSelectionHeap(int size, bool isCollectingHeap)
: SelectionHeap(size, isCollectingHeap)
{
}

void
DescendingSelectionHeap::updateDualBoundValue(SolverPoolElement::SPtr solver,
                                              double newDualBoundValue)
{
  int pos = isCollectingHeap ?
      solver->getCollectingModeSolverHeapElement() :
      solver->getSelectionHeapElement();
  if (solver->getBestDualBoundValue() < newDualBoundValue)
  {
    solver->setBestDualBoundValue(newDualBoundValue);
    SelectionHeap::upHeap(pos);
  }
  else
  {
    solver->setBestDualBoundValue(newDualBoundValue);
    SelectionHeap::downHeap(pos);
  }
}  // updateDualBoundValue

void DescendingSelectionHeap::deleteElement(SolverPoolElement::SPtr solver)
{
  int pos = isCollectingHeap ?
      solver->getCollectingModeSolverHeapElement() :
      solver->getSelectionHeapElement();

  if (pos == pHeapSize)
  {
    // No need to rearrange heap elements
    pHeap[pHeapSize--].reset();
  }
  else
  {
    auto erasingALowerBestBound =
        pHeap[pos]->getBestDualBoundValue() < pHeap[pHeapSize]->getBestDualBoundValue();
    pHeap[pos] = pHeap[pHeapSize];
    pHeap[pHeapSize--].reset();
    if (erasingALowerBestBound)
    {
      SelectionHeap::upHeap(pos);
    }
    else
    {
      SelectionHeap::downHeap(pos);
    }
  }

  if (isCollectingHeap)
  {
    solver->setCollectingModeSolverHeapElement(0);
  }
  else
  {
    solver->setSelectionHeapElement(0);
  }
}  // deleteElement

void DescendingSelectionHeap::upHeapImpl(int pos, bool isCollectingHeap)
{
  SolverPoolElement::SPtr she;
  she = pHeap[pos];

  pHeap[0].reset();
  while (pHeap[pos/2] != nullptr &&
      (pHeap[pos/2]->getBestDualBoundValue() < she->getBestDualBoundValue()))
  {
    pHeap[pos] = pHeap[pos/2];
    if (isCollectingHeap)
    {
      pHeap[pos]->setCollectingModeSolverHeapElement(pos);
    }
    else
    {
      pHeap[pos]->setSelectionHeapElement(pos);
    }
    pos = pos/2;
  }

  pHeap[pos] = she;
  if (isCollectingHeap)
  {
    pHeap[pos]->setCollectingModeSolverHeapElement(pos);
  }
  else
  {
    pHeap[pos]->setSelectionHeapElement(pos);
  }
}  // upHeapImpl

void DescendingSelectionHeap::downHeapImpl(int pos, bool isCollectingHeap)
{
  int j;
  SolverPoolElement::SPtr she;

  she = pHeap[pos];
  while (pos <= (pHeapSize/2))
  {
    j = pos + pos;
    if ( j < pHeapSize &&
        (pHeap[j]->getBestDualBoundValue() < pHeap[j+1]->getBestDualBoundValue()))
    {
      j++;
    }

    if (she->getBestDualBoundValue() > pHeap[j]->getBestDualBoundValue()) break;
    pHeap[pos] = pHeap[j];
    if (isCollectingHeap)
    {
      pHeap[pos]->setCollectingModeSolverHeapElement(pos);
    }
    else
    {
      pHeap[pos]->setSelectionHeapElement(pos);
    }
    pos = j;
  }
  pHeap[pos] = she;
  if (isCollectingHeap)
  {
    pHeap[pos]->setCollectingModeSolverHeapElement(pos);
  }
  else
  {
    pHeap[pos]->setSelectionHeapElement(pos);
  }
}  // downHeap

AscendingSelectionHeap::AscendingSelectionHeap(int size, bool isCollectingHeap)
: SelectionHeap(size, isCollectingHeap)
{
}

void AscendingSelectionHeap::updateDualBoundValue(SolverPoolElement::SPtr solver,
                                                  double newDualBoundValue)
{
  int pos = isCollectingHeap ?
      solver->getCollectingModeSolverHeapElement() :
      solver->getSelectionHeapElement();
  if (solver->getBestDualBoundValue() > newDualBoundValue)
  {
    solver->setBestDualBoundValue(newDualBoundValue);
    SelectionHeap::upHeap(pos);
  }
  else
  {
    solver->setBestDualBoundValue(newDualBoundValue);
    SelectionHeap::downHeap(pos);
  }
}  // updateDualBoundValue

void AscendingSelectionHeap::deleteElement(SolverPoolElement::SPtr solver)
{
  int pos = isCollectingHeap ?
      solver->getCollectingModeSolverHeapElement() :
      solver->getSelectionHeapElement();
  if (pos == pHeapSize)
  {
    // no need to rearrange heap element
    pHeap[pHeapSize--].reset();
  }
  else
  {
    if (pHeap[pos]->getBestDualBoundValue() > pHeap[pHeapSize]->getBestDualBoundValue())
    {
      pHeap[pos] = pHeap[pHeapSize];
      pHeap[pHeapSize--] = 0;
      SelectionHeap::upHeap(pos);
    }
    else
    {
      pHeap[pos] = pHeap[pHeapSize];
      pHeap[pHeapSize--] = 0;
      SelectionHeap::downHeap(pos);
    }
  }
  solver->setSelectionHeapElement(0);
}  // deleteElement

void AscendingSelectionHeap::upHeapImpl(int pos, bool isCollectingHeap)
{
  SolverPoolElement::SPtr she;
  she = pHeap[pos];
  pHeap[0].reset();

  while (pHeap[pos/2] != nullptr &&
      (pHeap[pos/2]->getBestDualBoundValue() > she->getBestDualBoundValue()))
  {
    pHeap[pos] = pHeap[pos/2];
    if (isCollectingHeap)
    {
      pHeap[pos]->setCollectingModeSolverHeapElement(pos);
    }
    else
    {
      pHeap[pos]->setSelectionHeapElement(pos);
    }
    pos = pos/2;
  }
  pHeap[pos] = she;
  if (isCollectingHeap)
  {
    pHeap[pos]->setCollectingModeSolverHeapElement(pos);
  }
  else
  {
    pHeap[pos]->setSelectionHeapElement(pos);
  }
}  // upHeapImpl

void AscendingSelectionHeap::downHeapImpl(int pos, bool isCollectingHeap)
{
   int j;
   SolverPoolElement::SPtr she;

   she = pHeap[pos];
   while (pos <= (pHeapSize/2))
   {
      j = pos + pos;
      if (j < pHeapSize &&
          (pHeap[j]->getBestDualBoundValue() > pHeap[j+1]->getBestDualBoundValue()))
      {
        j++;
      }

      if (she->getBestDualBoundValue() < pHeap[j]->getBestDualBoundValue()) break;
      pHeap[pos] = pHeap[j];
      if (isCollectingHeap)
      {
        pHeap[pos]->setCollectingModeSolverHeapElement(pos);
      }
      else
      {
        pHeap[pos]->setSelectionHeapElement(pos);
      }
      pos = j;
   }
   pHeap[pos] = she;
   if (isCollectingHeap)
   {
     pHeap[pos]->setCollectingModeSolverHeapElement(pos);
   }
   else
   {
     pHeap[pos]->setSelectionHeapElement(pos);
   }
}  // downHeap

SolverPool::SolverPool(double MP, double BGap, double MBGap, int origRank,
                       Framework::SPtr framework, ParamSet::SPtr paramSet,
                       timer::Timer::SPtr timer)
: pBGap(BGap),
  pMP(MP),
  pMBGap(MBGap),
  pOriginRank(origRank),
  pFramework(framework),
  pParamSet(paramSet),
  pTimer(timer)
{
  if (!pFramework)
  {
    throw std::invalid_argument("SolverPool - empty pointer to framework");
  }

  if (!pParamSet)
  {
    throw std::invalid_argument("SolverPool - empty pointer to paramset");
  }

  if (!pTimer)
  {
    pTimer = std::make_shared<timer::Timer>();
  }

  // Get the total number of solvers in the network communication layer
  pNumSolvers = pFramework->getComm()->getSize() - pOriginRank;
  pNumGenerators = pNumSolvers;

  // Resize the solver's pool to contain all the solvers in the network
  pPool.resize(pNumSolvers);

  // Add the solvers into the pool
  for (int sidx = 0; sidx < pNumSolvers; sidx++)
  {
    const auto solverRank = pOriginRank + sidx;
    pPool[sidx] = std::make_shared<SolverPoolElement>(solverRank);

    // Set the solver as not a generator solver
    if (sidx >= pNumGenerators) pPool[sidx]->setNoGenerator();

    // Add this solver in the map of inactive solvers
    pInactiveSolversMap.insert(std::make_pair(solverRank, pPool.at(sidx)));
  }

  // Set the maximum number of collecting mode solvers
  const auto maxCollSolvers = (pParamSet->getDescriptor()).maxnumofcollectingmodesolvers();
  if (maxCollSolvers > 0)
  {
    pNumMaxCollectingModeSolvers = std::min(maxCollSolvers, pNumSolvers);
  }
  else if (maxCollSolvers == 0)
  {
    // At least one solver must be in collecting mode
    pNumMaxCollectingModeSolvers = std::max(static_cast<int>(pNumSolvers / 2), 1);
  }
  else
  {
    // No limit on the number of collecting solver
    pNumMaxCollectingModeSolvers = pNumSolvers;
  }

  pNumLimitCollectingModeSolvers =
      (pParamSet->getDescriptor()).minnumofcollectingmodesolvers();
  pAbsoluteGap = (pParamSet->getDescriptor()).abgapforswitchingtobestsolver();
  pBreakingFirstSubtree = false;
}

bool SolverPool::isSolverActive(int rank) const
{
  const auto origRank = SOLVER_POOL_INDEX(rank);
  if (origRank >= pPool.size())
  {
    throw std::out_of_range("SolverPool - isSolverActive: "
            "invalid rank " + std::to_string(rank));
  }

  return pPool.at(origRank)->isActive();
}  //isSolverActive

bool SolverPool::isSolverInCollectingMode(int rank) const
{
  const auto origRank = SOLVER_POOL_INDEX(rank);
  if (origRank >= pPool.size())
  {
    throw std::out_of_range("SolverPool - isSolverInCollectingMode: "
            "invalid rank " + std::to_string(rank));
  }

   return pPool.at(origRank)->isInCollectingMode();
}  // isSolverInCollectingMode

Node::SPtr SolverPool::getCurrentNode(int rank) const
{
  const auto origRank = SOLVER_POOL_INDEX(rank);
  if (origRank >= pPool.size())
  {
    throw std::out_of_range("SolverPool - getCurrentNode: "
            "invalid rank " + std::to_string(rank));
  }

  return  pPool.at(origRank)->getCurrentNode();
}  // getCurrentNode

Node::SPtr SolverPool::extractCurrentNodeAndInactivate(int rank, NodePool::SPtr nodePool)
{
  if (!nodePool)
  {
    throw std::invalid_argument("SolverPool - extractCurrentNodeAndInactivate: "
            "empty node pool");
  }

  const auto origRank = SOLVER_POOL_INDEX(rank);
  if (origRank >= pPool.size())
  {
    throw std::out_of_range("SolverPool - extractCurrentNodeAndInactivate: "
            "invalid rank " + std::to_string(rank));
  }

  // Get the current node being solved by the specified solver
  Node::SPtr node = pPool.at(origRank)->extractCurrentNode();

  // Number of nodes solved should be added when LM receives termination message
  deactivateSolver(rank, 0, nodePool);
  return node;
}  // extractCurrentNodeAndInactivate

bool SolverPool::currentSolvingNodeHasDescendant(int rank) const
{
  const auto origRank = SOLVER_POOL_INDEX(rank);
  if (origRank >= pPool.size())
  {
    throw std::out_of_range("SolverPool - currentSolvingNodeHasDescendant: "
            "invalid rank " + std::to_string(rank));
  }

  if (!pPool.at(origRank)->getCurrentNode())
  {
    throw std::runtime_error("SolverPool - currentSolvingNodeHasDescendant: "
            "invalid (empty) node in solver with rank " + std::to_string(rank));
  }
  return pPool.at(origRank)->getCurrentNode()->hasChildren();
}  // currentSolvingNodeHasDescendant

uint64_t SolverPool::getNumNodesSolved(int rank) const
{
  assert(isSolverActive(rank));

  const auto origRank = SOLVER_POOL_INDEX(rank);
  if (origRank >= pPool.size())
  {
    throw std::out_of_range("SolverPool - getNumNodesSolved: "
            "invalid rank " + std::to_string(rank));
  }

  return pPool.at(origRank)->getNumOfNodesSolved();
}  // getNumNodesSolved

int SolverPool::getNumOfNodesLeft(int rank)
{
  assert(isSolverActive(rank));

  const auto origRank = SOLVER_POOL_INDEX(rank);
  if (origRank >= pPool.size())
  {
    throw std::out_of_range("SolverPool - getNumOfNodesLeft: "
            "invalid rank " + std::to_string(rank));
  }

  return pPool.at(origRank)->getNumOfNodesLeft();
}  // getNumOfNodesLeft

double SolverPool::getDualBoundValue(int rank) const
{
   assert(isSolverActive(rank));

   const auto origRank = SOLVER_POOL_INDEX(rank);
   if (origRank >= pPool.size())
   {
     throw std::out_of_range("SolverPool - getDualBoundValue: "
             "invalid rank " + std::to_string(rank));
   }

   return pPool.at(origRank)->getBestDualBoundValue();
}  // getDualBoundValue

void SolverPool::setTermState(int rank, SolverTerminationState::SPtr termState)
{
  const auto origRank = SOLVER_POOL_INDEX(rank);
  if (origRank >= pPool.size())
  {
    throw std::out_of_range("SolverPool - setTermState: "
            "invalid rank " + std::to_string(rank));
  }

  pPool.at(origRank)->setTermState(termState);
}  // setTermState

SolverTerminationState::SPtr SolverPool::getTermState(int rank) const
{
  const auto origRank = SOLVER_POOL_INDEX(rank);
  if (origRank >= pPool.size())
  {
    throw std::out_of_range("SolverPool - getTermState: "
            "invalid rank " + std::to_string(rank));
  }

  return pPool.at(origRank)->getTermState();
}  // getTermState

void SolverPool::setCollectingIsAllowed(int rank)
{
  const auto origRank = SOLVER_POOL_INDEX(rank);
  if (origRank >= pPool.size())
  {
    throw std::out_of_range("SolverPool - setCollectingIsAllowed: "
            "invalid rank " + std::to_string(rank));
  }

  return pPool.at(origRank)->setCollectingIsAllowed();
}  // setCollectingIsAllowed

bool SolverPool::isDualBounGainTesting(int rank) const
{
  const auto origRank = SOLVER_POOL_INDEX(rank);
  if (origRank >= pPool.size())
  {
    throw std::out_of_range("SolverPool - isDualBounGainTesting: "
            "invalid rank " + std::to_string(rank));
  }

  return pPool.at(origRank)->isDualBoundGainTesting();
}  // isDualBounGainTesting

void SolverPool::activateSolverImpl(int rank, Node::SPtr node)
{
  // The solver pool becomes active
  pActive = true;
  if (rank < pOriginRank || rank >= (pOriginRank + pNumSolvers))
  {
    throw std::out_of_range("SolverPool - activateSolver: "
            "invalid rank " + std::to_string(rank));
  }

  const auto origRank = SOLVER_POOL_INDEX(rank);
  assert(origRank < pPool.size());
  if (pPool.at(origRank)->getStatus() == solver::SolverStatus::SS_INACTIVE)
  {
    // The solver is not active, it should be among the inactive solvers
    auto it = pInactiveSolversMap.find(rank);
    if ((it == pInactiveSolversMap.end()) || (it->second->getRank() != rank)
            || (pPool.at(origRank)->getRank() != rank)) {
      throw std::runtime_error("SolverPool - activateSolver: "
              "inactive solver rank not found " + std::to_string(rank));
    }

    // Remove the solver from the list of inactive solvers
    pInactiveSolversMap.erase(it);
  }

  // Add the solver to the list of active solvers,
  // and activate the solver on the given node
  pActiveSolversMap.insert(std::make_pair(rank, pPool.at(origRank)));
  pPool.at(origRank)->activate(node);
}  // activateSolverImpl

void SolverPool::activateSolver(int rank, Node::SPtr node)
{
  if (!node)
  {
    throw std::invalid_argument("SolverPool - activateSolver: "
            "empty pointer to node");
  }

  // Activate the solver,
  // i.e., put it into the active pool map
  activateSolverImpl(rank, node);

  // One more node in a solver
  pNumNodesInSolver++;

  // Set the solver on the heap (after being activated)
  pSelectionHeap->insert(pPool.at(SOLVER_POOL_INDEX(rank)));

  // Send the activated node to the (remote) solver
  // with specified rank
  node->send(pFramework, rank);
}  // activateSolver

void SolverPool::activateSolver(int rank, Node::SPtr node, int numNodesLeft)
{
  // Activate the solver,
  // i.e., put it into the active pool map
  activateSolverImpl(rank, node);

  // Set the number of nodes left
  const auto origRank = SOLVER_POOL_INDEX(rank);
  pPool.at(origRank)->setNumOfNodesLeft(numNodesLeft);

  // One more node in a solver
  pNumNodesInSolver += numNodesLeft;

  // Set the solver on the heap (after being activated)
  pSelectionHeap->insert(pPool.at(origRank));
}  // activateSolver

int SolverPool::activateSolver(Node::SPtr node, std::shared_ptr<RacingSolverPool> racingSolverPool,
                               bool isRampUpPhase, int numGoodNodesInNodePool,
                               double avgDualBoundGain)
{
  // The solver pool becomes active
  pActive = true;

  // Pick the solver to activate
  int rank;
  auto it = pInactiveSolversMap.begin();
  if (!racingSolverPool)
  {
    if (it == pInactiveSolversMap.end())
    {
      throw std::runtime_error("SolverPool - activateSolver: "
              "inactive solver rank not found " + std::to_string(rank));
    }
    rank = it->second->getRank();
  }
  else
  {
    // Pick the first inactive solver in the racing pool
    for (; it != pInactiveSolversMap.end(); ++it)
    {
      if (!(racingSolverPool->isSolverActive(it->second->getRank())))
      {
        rank = it->second->getRank();
        break;
      }
    }

    // No solver found
    if (it == pInactiveSolversMap.end()) return -1;
  }

  // Set the selected solver as active
  const auto origRank = SOLVER_POOL_INDEX(rank);
  pActiveSolversMap.insert(std::make_pair(rank, pPool.at(origRank)));

  pPool.at(origRank)->activate(node);

  // One more node in (the active) solver
  pNumNodesInSolver++;

  // Set the solver on the heap (after being activated)
  pSelectionHeap->insert(pPool.at(origRank));

  // Remove the solver from the map of inactive solvers
  pInactiveSolversMap.erase(it);

  // Send the activated node to the (remote) solver
  // with specified rank
  node->send(pFramework, rank);

  // Return the activated solver
  return rank;
}  // activateSolver

void SolverPool::deactivateSolver(int rank, uint64_t numSolvedNodes, NodePool::SPtr nodePool)
{
  if (rank < pOriginRank || rank >= (pOriginRank + pNumSolvers))
  {
    throw std::invalid_argument("SolverPool - deactivateSolver: "
            "invalid rank " + std::to_string(rank));
  }

  if(pBreakingFirstSubtree && rank == 1)
  {
    pBreakingFirstSubtree = false;
  }

  // Switch out of collecting mode to prepare the solver to be deactivated
  sendSwitchOutCollectingModeIfNecessary(rank);

  const auto origRank = SOLVER_POOL_INDEX(rank);
  assert(origRank < pPool.size());
  if (pPool.at(origRank)->getStatus() == solver::SolverStatus::SS_ACTIVE)
  {
    // The solver is active, it should be among the active solvers
    auto it = pActiveSolversMap.find(rank);
    if ((it == pActiveSolversMap.end()) || (it->second->getRank() != rank)
            || (pPool.at(origRank)->getRank() != rank)) {
      throw std::runtime_error("SolverPool - deactivateSolver: "
              "inactive solver rank not found " + std::to_string(rank));
    }

    // Add the number of nodes solved by the solver to deactivate
    pNumNodesSolvedInSolvers += numSolvedNodes - it->second->getNumOfNodesSolved();

    // This solver will be deactivated so it won't have the nodes in it anymore
    pNumNodesInSolver -= it->second->getNumOfNodesLeft();

    if (isInCollectingMode() && pPool.at(origRank)->isOutCollectingMode())
    {
      // There is one less solver in collecting mode
      pNumCollectingModeSolvers--;
      if (pSelectionHeap->top()->getNumOfNodesLeft() > 1 &&
              pSelectionHeap->top()->isGenerator() &&
            (!(pSelectionHeap->top()->isCollectingProhibited())) &&
              pSelectionHeap->top()->isOutCollectingMode())
      {
        // The solver with the best dual bound should always in collecting mode first
        switchIntoCollectingToSolver(pSelectionHeap->top()->getRank(), nodePool);
      }
    }

    // If the solver was a candidate for collecting mode,
    // remove it from the candidate for collecting mode map since the solver
    // is being deactivated
    if (it->second->isCandidateOfCollecting())
    {
      auto itCollMode = pCandidatesOfCollectingModeSolversMap.begin();
      for (; itCollMode != pCandidatesOfCollectingModeSolversMap.end(); ++itCollMode)
      {
        if (itCollMode->second->getRank() == rank) break;
      }

      assert(itCollMode != pCandidatesOfCollectingModeSolversMap.end());
      itCollMode->second->setCandidateOfCollecting(false);
      pCandidatesOfCollectingModeSolversMap.erase(itCollMode);
    }

    // Remove the solver from the map of inactive solvers
    pActiveSolversMap.erase(it);
  }
  else
  {
    throw std::runtime_error("SolverPool - deactivateSolver: "
            "inactive solver rank not found " + std::to_string(rank));
  }

  // The the solver from all heaps.
  // @note the following method needs the solver to have dual bound,
  // so should be before "deactivate(...)"
  pSelectionHeap->deleteElement(pPool.at(origRank));
  if (pCollectingModeSolverHeap && pPool.at(origRank)->isInCollectingMode())
  {
    pCollectingModeSolverHeap->deleteElement(pPool.at(origRank));
  }

  // Deactivate the solver
  pPool.at(origRank)->deactivate();
  pInactiveSolversMap.insert(std::make_pair(rank, pPool.at(origRank)));
}  // deactivateSolver

Node::SPtr SolverPool::solverKill(int rank)
{
  if (rank < pOriginRank || rank >= (pOriginRank + pNumSolvers))
  {
    throw std::out_of_range("SolverPool - solverKill: "
            "invalid rank " + std::to_string(rank));
  }

  const auto origRank = SOLVER_POOL_INDEX(rank);
  assert(origRank < pPool.size());
  if (pPool.at(origRank)->getStatus() == solver::SolverStatus::SS_ACTIVE)
  {
    // The solver is active, it should be among the active solvers
    auto it = pActiveSolversMap.find(rank);
    if ((it == pActiveSolversMap.end()) || (it->second->getRank() != rank)
            || (pPool.at(origRank)->getRank() != rank)) {
      throw std::runtime_error("SolverPool - solverKill: "
              "inactive solver rank not found " + std::to_string(rank));
    }

    // Removed the solver from the map of active solvers
    pNumNodesSolvedInSolvers -= it->second->getNumOfNodesSolved();
    pNumNodesInSolver -= it->second->getNumOfNodesLeft();
    pActiveSolversMap.erase(it);
  }
  else
  {
    throw std::runtime_error("SolverPool - solverKill: "
            "inactive solver rank not found " + std::to_string(rank));
  }

  // Note that the element needs to have the dual bound set before the call to "kill(...)".
  // This is done by the call to "deleteElement(...)"
  pSelectionHeap->deleteElement(pPool.at(origRank));
  if (pCollectingModeSolverHeap && pPool[origRank]->isInCollectingMode())
  {
    pCollectingModeSolverHeap->deleteElement(pPool.at(origRank));
  }

  // Insert the solver into the map of killed solvers and kill the solver
  pDeadSolversMap.insert(std::make_pair(rank, pPool.at(origRank)));
  return  pPool.at(origRank)->kill();
}  // solverKill

void SolverPool::switchOutCollectingMode()
{
  if (!isInCollectingMode())
  {
    // Not in collecting mode, nothing to do, return asap
    return;
  }

  // If there are active solvers, switch out of collecting mode
  // for each active solver
  for (auto it = pActiveSolversMap.begin(); it != pActiveSolversMap.end(); ++it)
  {
    const int rank = it->second->getRank();
    sendSwitchOutCollectingModeIfNecessary(rank);
    if (pPool.at(SOLVER_POOL_INDEX(rank))->isCandidateOfCollecting())
    {
      pPool.at(SOLVER_POOL_INDEX(rank))->setCandidateOfCollecting(false);
    }
  }

  // Switch out of collecting mode
  assert(pNumCollectingModeSolvers == 0);
  pCandidatesOfCollectingModeSolversMap.clear();
  pCollectingMode = false;
  setSwichOutTime(pTimer->getWallClockTimeMsec());
}  // switchOutCollectingMode

void SolverPool::enforcedSwitchOutCollectingMode(int rank)
{
  if (rank < pOriginRank || rank >= (pOriginRank + pNumSolvers))
  {
    throw std::out_of_range("SolverPool - solverKill: "
            "invalid rank " + std::to_string(rank));
  }

  const auto origRank = SOLVER_POOL_INDEX(rank);
  if (!(pPool.at(origRank)->isOutCollectingMode()))
  {
    pPool.at(origRank)->setCollectingMode(false);
    if (pCollectingModeSolverHeap)
    {
      pCollectingModeSolverHeap->deleteElement(pPool.at(origRank));
    }
    pNumCollectingModeSolvers--;
  }
}  //enforcedSwitchOutCollectingMode

void SolverPool::sendSwitchOutCollectingModeIfNecessary(int rank)
{
  if (rank < pOriginRank || rank >= (pOriginRank + pNumSolvers))
  {
    throw std::out_of_range("SolverPool - solverKill: "
            "invalid rank " + std::to_string(rank));
  }

  const auto origRank = SOLVER_POOL_INDEX(rank);
  if (!(pPool.at(origRank)->isOutCollectingMode()))
  {
    // Send out-collecting-mode packet to the specified solver.
    // @note the data has dummy content, the tag is the information sent
    // to the solver
    utils::sendTagMessageToNetworkNode(
            pFramework,
            net::MetaBBPacketTag::PacketTagType::PPT_OUT_COLLECTING_MODE,
            rank);

    // Set the solver as not in collecting mode
    pPool.at(origRank)->setCollectingMode(false);
    if (pCollectingModeSolverHeap)
    {
      pCollectingModeSolverHeap->deleteElement(pPool.at(origRank));
    }
    pNumCollectingModeSolvers--;
  }
}  //sendSwitchOutCollectingModeIfNecessary

void SolverPool::switchIntoCollectingToSolver(int rank, NodePool::SPtr nodePool)
{
  const auto origRank = SOLVER_POOL_INDEX(rank);
  int numGoodNodes = static_cast<int>(nodePool->getNumOfGoodNodes(getGlobalBestDualBoundValue()));
  int numCollect = std::min(
          pPool.at(origRank)->getNumOfNodesLeft() / 4,
          static_cast<int>(
                  getNumInactiveSolvers() +
                  (pParamSet->getDescriptor()).numnodestoswitchtocollectingmode() *
                  pMCollectingNodes * (pParamSet->getDescriptor()).multiplierforcollectingmode() -
                  numGoodNodes));

  if (numGoodNodes <
          ((pParamSet->getDescriptor()).numnodestoswitchtocollectingmode() * pMCollectingNodes) / 4)
  {
    // Indicate aggressive collecting
    numCollect = 0 - (numCollect + 1);
  }
  assert(pPool.at(origRank)->isGenerator());
  assert(!(pPool.at(origRank)->isCollectingProhibited()));

  if (nodePool->isEmpty() && pNumLimitCollectingModeSolvers > 10)
  {
    utils::sendTagMessageToNetworkNode(
            pFramework,
            net::MetaBBPacketTag::PacketTagType::PPT_NO_WAIT_MODE_SEND,
            rank);
  }

  // Send collecting mode message to the solver
  utils::sendTagMessageToNetworkNode(
          pFramework,
          net::MetaBBPacketTag::PacketTagType::PPT_IN_COLLECTING_MODE,
          rank);

  // Remove the solver for candidates for collecting and set it
  // i the heap as collecting mode solver
  if (pPool.at(origRank)->isCandidateOfCollecting())
  {
    auto itCollMode = pCandidatesOfCollectingModeSolversMap.begin();
    for (; itCollMode != pCandidatesOfCollectingModeSolversMap.end(); ++itCollMode)
    {
      if (itCollMode->second->getRank() == rank) break;
    }

    assert(itCollMode != pCandidatesOfCollectingModeSolversMap.end());
    itCollMode->second->setCandidateOfCollecting(false);
    pCandidatesOfCollectingModeSolversMap.erase(itCollMode);
  }

  pPool.at(origRank)->setCollectingMode(true);
  if (pCollectingModeSolverHeap)
  {
    pCollectingModeSolverHeap->insert(pPool.at(origRank));
  }

  // Increase the number of collecting mode solvers
  pNumCollectingModeSolvers++;
}  //switchIntoCollectingToSolver

RacingSolverPool::RacingSolverPool(int origRank, Framework::SPtr framework, ParamSet::SPtr paramSet,
                                   timer::Timer::SPtr timer)
: pOriginRank(origRank),
  pFramework(framework),
  pParamSet(paramSet),
  pTimer(timer)
{
  if (!pFramework)
  {
    throw std::invalid_argument("RacingSolverPool - empty pointer to framework");
  }

  if (!pParamSet)
  {
    throw std::invalid_argument("RacingSolverPool - empty pointer to paramset");
  }

  if (!pTimer)
  {
    pTimer = std::make_shared<timer::Timer>();
  }

  // Get the total number of solvers in the network communication layer
  pNumSolvers = pFramework->getComm()->getSize() - pOriginRank;

  // Resize the solver's pool to contain all the solvers in the network
  pPool.resize(pNumSolvers);

  // Add the solvers into the pool
  for (int sidx = 0; sidx < pNumSolvers; sidx++)
  {
    const auto solverRank = pOriginRank + sidx;
    pPool[sidx] = std::make_shared<SolverPoolElement>(solverRank);
  }

  // Use a Max Heap as selection heap
  pSelectionHeap = std::make_shared<DescendingSelectionHeap>(pNumSolvers);
}

RacingSolverPool::~RacingSolverPool()
{
  pSelectionHeap.reset();
  for (auto& ptr : pPool)
  {
    ptr.reset();
  }
}

Node::SPtr RacingSolverPool::extractRacingRootNode()
{
  if (!pRootNode)
  {
    throw std::runtime_error("RacingSolverPool - extractRacingRootNode: "
            "no root node set");
  }
  pRootNode->setDualBoundValue(getGlobalBestDualBoundValue());
  pRootNode->setInitialDualBoundValue(getGlobalBestDualBoundValue());
  return pRootNode->clone(pFramework);
}

double RacingSolverPool::getGlobalBestDualBoundValue() const
{
  return (pSelectionHeap->getHeapSize() > 0) ?
          pBestDualBound :
          metaconst::DOUBLE_NEG_INF;
}  // getGlobalBestDualBoundValue

double RacingSolverPool::getDualBoundValue(int rank) const
{
  const auto origRank = SOLVER_POOL_INDEX(rank);
  if (origRank >= pPool.size())
  {
    throw std::out_of_range("RacingSolverPool - getDualBoundValue: "
            "invalid rank " + std::to_string(rank));
  }

  return  pPool.at(origRank)->getBestDualBoundValue();
}  // getDualBoundValue

long long RacingSolverPool::getNumOfNodesSolved(int rank) const
{
  const auto origRank = SOLVER_POOL_INDEX(rank);
  if (origRank >= pPool.size())
  {
    throw std::out_of_range("RacingSolverPool - getNumOfNodesSolved: "
            "invalid rank " + std::to_string(rank));
  }

  return  pPool.at(origRank)->getNumOfNodesSolved();
}  // getDualBoundValue

int RacingSolverPool::getNumNodesLeft(int rank) const
{
  const auto origRank = SOLVER_POOL_INDEX(rank);
  if (origRank >= pPool.size())
  {
    throw std::out_of_range("RacingSolverPool - getNumNodesLeft: "
            "invalid rank " + std::to_string(rank));
  }

  return  pPool.at(origRank)->getNumOfNodesLeft();
}  // getDualBoundValue

void RacingSolverPool::activate(Node::SPtr node)
{
  if (!node)
  {
    throw std::invalid_argument("RacingSolverPool - activate: "
            "empty pointer to the root node");
  }

  pRootNode = node;
  for (int rank = pOriginRank; rank < (pOriginRank + pNumSolvers); rank++)
  {
    // Activate the solver
    const auto origRank = SOLVER_POOL_INDEX(rank);
    pPool.at(origRank)->racingActivate();

    // This is called after "racingActivate(...)" to fix the dual bound
    // in the solver pool element
    pSelectionHeap->insert(pPool.at(origRank));
    pNumActiveSolvers++;
  }

  // One node in the best solver
  pNumNodesInBestSolver = 1;
}  // activate

bool RacingSolverPool::isActive(int rank) const
{
  const auto origRank = SOLVER_POOL_INDEX(rank);
  if (origRank >= pPool.size())
  {
    throw std::out_of_range("RacingSolverPool - isActive: "
            "invalid rank " + std::to_string(rank));
  }

  return (pPool.at(origRank)->isRacingStage() ||
          pPool.at(origRank)->isEvaluationStage());
}  // isActive

void RacingSolverPool::updateSolverStatus(int rank, long long numNodesSolved, int numNodesLeft,
                                          double solverLocalBestDualBound)
{
  const auto origRank = SOLVER_POOL_INDEX(rank);
  if (origRank >= pPool.size())
  {
    throw std::out_of_range("RacingSolverPool - updateSolverStatus: "
            "invalid rank " + std::to_string(rank));
  }

  if (!(pPool.at(origRank)->isRacingStage() || pPool.at(origRank)->isEvaluationStage()))
  {
    throw std::invalid_argument("RacingSolverPool - updateSolverStatus: "
            "cannot update a non-racing solver " + std::to_string(rank));
  }

  // Check the termination criteria:
  // 0 - stop at the number of nodes left reached by the top heap (i.e., best) solver;
  // 1 - stop at time limit;
  // 2 - stop at the solver with the best dual bound value has a certain number of nodes;
  // 3 - adaptive (node first, i.e., case (0) then case (1));
  // 4 - adaptive (time first, i.e., case (1) then case (0)).
  // Default value is 4
  const auto racingTermCriteria = (pParamSet->getDescriptor()).racingrampupterminationcriteria();
  if (racingTermCriteria)
  {
    // Get the number of nodes left in a solver to stop racing
    const auto stopOnNodesLeftVal = (pParamSet->getDescriptor()).stopracingnumnodesleft();
    if (pPool.at(origRank)->getNumOfNodesLeft() < stopOnNodesLeftVal &&
            numNodesLeft >= stopOnNodesLeftVal)
    {
      pPool.at(origRank)->switchIntoEvaluation();
      pNumEvaluationStage++;
    }

    if(pPool.at(origRank)->getNumOfNodesLeft() >= stopOnNodesLeftVal &&
            numNodesLeft >= stopOnNodesLeftVal)
    {
      pPool.at(origRank)->switchOutEvaluation();
      pNumEvaluationStage--;
    }
  }
  else if (!(pPool.at(origRank)->isEvaluationStage()))
  {
    pPool.at(origRank)->switchIntoEvaluation();
    pNumEvaluationStage++;
  }
  pPool.at(origRank)->setNumOfDiffNodesSolved(
          numNodesSolved - pPool.at(origRank)->getNumOfNodesSolved());
  pPool.at(origRank)->setNumOfNodesSolved(numNodesSolved);
  pPool.at(origRank)->setNumOfDiffNodesLeft(numNodesLeft - pPool.at(origRank)->getNumOfNodesLeft());
  pPool.at(origRank)->setNumOfNodesLeft(numNodesLeft);
  if (rank != pOriginRank)
  {
    int previousTop = pSelectionHeap->top()->getRank();
    pSelectionHeap->updateDualBoundValue(pPool.at(pPool.at(origRank)->getSelectionHeapElement()),
                                         solverLocalBestDualBound);
    if (pSelectionHeap->top()->getRank() == rank ||
            pSelectionHeap->top()->getRank() != previousTop)
    {
      // Notice that in racing ramp-up, winner nodes should be the number of nodes left
      pNumNodesInBestSolver = pSelectionHeap->top()->getNumOfNodesLeft();
      pNumNodesSolvedInBestSolver = pSelectionHeap->top()->getNumOfNodesSolved();
    }

    // Update best dual bound
    if (pSelectionHeap->getHeapSize() > 0)
    {
      if (pSelectionHeap->top()->getBestDualBoundValue() > pBestDualBound)
      {
        pBestDualBound = pSelectionHeap->top()->getBestDualBoundValue();
      }
    }
  }

  if (pBestDualBoundInSolvers < solverLocalBestDualBound)
  {
    pBestDualBoundInSolvers = solverLocalBestDualBound;
  }
}  // updateSolverStatus

bool RacingSolverPool::isWinnerDecided(bool feasibleSol)
{
  // Get the ratio for the number of solvers that count on the racing
  const auto solverRatio = pParamSet->getDescriptor().countingsolverratioinracing();
  const auto racingTermCriteria = pParamSet->getDescriptor().racingrampupterminationcriteria();

  // Check on termination criteria.
  // @note in all cases there must be a "sufficient" number of solvers in evaluation stage
  switch (racingTermCriteria)
  {
    case 0:
    {
      // Stop at the number of nodes left in the heap top solver (i.e., best) reached
      // to the value specified in paramSet.
      // This holds as long as there are "enough" solvers in evaluation stage
      const auto numEvalSolversToStopRacing =
              pParamSet->getDescriptor().numevaluationsolverstostopracing();
      if ((numEvalSolversToStopRacing == -1 && pNumEvaluationStage == pNumSolvers) ||
              (numEvalSolversToStopRacing == 0 && pNumEvaluationStage >= (pNumSolvers/2)) ||
              (numEvalSolversToStopRacing > 0 && pNumEvaluationStage >= numEvalSolversToStopRacing))
      {
        if (pSelectionHeap->top()->getNumOfNodesLeft() >
            pParamSet->getDescriptor().stopracingnumnodesleft())
        {
          pWinnerRank = pSelectionHeap->top()->getRank();
          return true;
        }
      }
      break;
    }
    case 1:
    {
      // Stop at time limit
      if (pNumEvaluationStage >= static_cast<int>(pNumSolvers * solverRatio))
      {
        if (pTimer->getWallClockTimeMsec() > pParamSet->getDescriptor().stopracingtimelimitmsec())
        {
          pWinnerRank = pSelectionHeap->top()->getRank();
          return true;
        }
      }
      break;
    }
    case 2:
    {
      // Stop at the solver with the best dual bound value has a certain number of nodes
      if (pNumEvaluationStage >= static_cast<int>(pNumSolvers * solverRatio))
      {
        if (pSelectionHeap->top()->getNumOfNodesLeft() >
          pParamSet->getDescriptor().stopracingnumnodesleft())
        {
          pWinnerRank = pSelectionHeap->top()->getRank();
          return true;
        }
      }
      break;
    }
    case 3:
    {
      // Adaptive (node first).
      // Check if there are enough nodes to perform the check before
      // actually checking the nodes left
      if (pSelectionHeap->top()->getNumOfNodesLeft() <= pNumSolvers &&
              pSelectionHeap->top()->getNumOfNodesLeft() <=
              pParamSet->getDescriptor().stopracingnumnodesleft())
      {
        // Not enough number of nodes left to stop racing
        return false;
      }

      // There should be enough solvers in evaluation stage
      if (pNumEvaluationStage < static_cast<int>(pNumSolvers * solverRatio))
      {
        return false;
      }

      // Check if the solvers have to keep racing until a first solution if found
      if (pParamSet->getDescriptor().keepracinguntiltofindfirstsolution() && !feasibleSol)
      {
        // No feasible solution has been found yet
        return false;
      }

      // First check nodes
      if (pSelectionHeap->top()->getNumOfNodesLeft() >
         pParamSet->getDescriptor().stopracingnumnodesleft())
      {
        pWinnerRank = pSelectionHeap->top()->getRank();
        return true;
      }

      // Then check time
      if (pTimer->getWallClockTimeMsec() > pParamSet->getDescriptor().stopracingtimelimitmsec())
      {
        pWinnerRank = pSelectionHeap->top()->getRank();
        return true;
      }
      break;
    }
    case 4:
    {
      // Adaptive (time first).
      // Check if the solvers have to keep racing until a first solution if found
      if (pParamSet->getDescriptor().keepracinguntiltofindfirstsolution() && !feasibleSol)
      {
        // No feasible solution has been found yet
        return false;
      }

      // There should be enough solvers in evaluation stage
      if (pNumEvaluationStage < static_cast<int>(pNumSolvers * solverRatio))
      {
        return false;
      }

      // First check time
      if (pTimer->getWallClockTimeMsec() > pParamSet->getDescriptor().stopracingtimelimitmsec())
      {
        pWinnerRank = pSelectionHeap->top()->getRank();
        return true;
      }

      // then check nodes
      if (pSelectionHeap->top()->getNumOfNodesLeft() <= pNumSolvers
              && pSelectionHeap->top()->getNumOfNodesLeft()
                      <= pParamSet->getDescriptor().stopracingnumnodesleft())
      {
        // Not enough number of nodes left to stop racing
        return false;
      }

      if (pSelectionHeap->top()->getNumOfNodesLeft()
              > pParamSet->getDescriptor().stopracingnumnodesleft())
      {
        pWinnerRank = pSelectionHeap->top()->getRank();
        return true;
      }
      break;
    }
    default:
      throw std::runtime_error("RacingSolverPool - isWinnerDecided: "
              "invalid criteria");
  }
  return false;
}  // isWinnerDecided

void RacingSolverPool::deactivateSolver(int rank)
{
  const auto origRank = SOLVER_POOL_INDEX(rank);
  if (!(pPool.at(origRank)->isRacingStage()) && !(pPool.at(origRank)->isEvaluationStage()))
  {
    throw std::invalid_argument("RacingSolverPool - deactivateSolver: "
            "cannot update a non-racing solver " + std::to_string(rank));
  }

  if (pPool.at(origRank)->getBestDualBoundValue() > pBestDualBoundInSolvers)
  {
    pBestDualBoundInSolvers = pPool.at(origRank)->getBestDualBoundValue();
  }
  pPool.at(origRank)->deactivate();
  pNumInactiveSolvers++;
  pNumActiveSolvers--;
}  // deactivateSolver

bool RacingSolverPool::isEvaluationStage(int rank) const
{
  const auto origRank = SOLVER_POOL_INDEX(rank);
  if (origRank >= pPool.size())
  {
    throw std::out_of_range("RacingSolverPool - isActive: "
            "invalid rank " + std::to_string(rank));
  }

  return pPool.at(origRank)->isEvaluationStage();
}  // isEvaluationStage

SolverPoolForMinimization::SolverPoolForMinimization(double MP,
                                                     double BGap,
                                                     double MBGap,
                                                     int origRank,
                                                     Framework::SPtr framework,
                                                     ParamSet::SPtr paramSet,
                                                     timer::Timer::SPtr timer)
: SolverPool(MP, BGap, MBGap, origRank, framework, paramSet, timer)
{
  // Instantiate a min heap
  pSelectionHeap = std::make_shared<AscendingSelectionHeap>(pNumSolvers);
  if ((pParamSet->getDescriptor().solverorderincollectingmode()) == 0)
  {
    pCollectingModeSolverHeap = std::make_shared<CollectingModeSolverMinHeap>(
            std::min(pNumMaxCollectingModeSolvers * 2, pNumSolvers));
  }
}

double SolverPoolForMinimization::getGlobalBestDualBoundValue()
{
  if (pSelectionHeap->getHeapSize() > 0)
  {
    return pSelectionHeap->top()->getBestDualBoundValue();
  }
  return metaconst::DOUBLE_POS_INF;
}  // getGlobalBestDualBoundValue

void SolverPoolForMinimization::switchIntoCollectingMode(NodePool::SPtr nodePool)
{
  const auto collModeIntervalMsec = pParamSet->getDescriptor().collectingmodeinteravalmsec();
  if (collModeIntervalMsec >= 0 && getSwichOutTime() > 0 &&
          (pTimer->getWallClockTimeMsec() - getSwichOutTime()) < collModeIntervalMsec &&
          pMCollectingNodes < 100)
  {
    if (!(pCandidatesOfCollectingModeSolversMap.empty()) &&
            pCandidatesOfCollectingModeSolversMap.begin()->second->getNumOfDiffNodesLeft() < 0)
    {
      pMCollectingNodes--;
      if (pMCollectingNodes <= 0) pMCollectingNodes = 1;
    }
    else
    {
      pMCollectingNodes++;
      if (pMCollectingNodes > pMMaxCollectingNodes) pMCollectingNodes = pMMaxCollectingNodes;
    }
    setSwichOutTime(0);
  }

  int numCollect =
          static_cast<int>(getNumInactiveSolvers()) +
          static_cast<int>(pParamSet->getDescriptor().multiplierforcollectingmode() *
                           pMCollectingNodes) -
          static_cast<int>(nodePool->getNumOfGoodNodes(getGlobalBestDualBoundValue()));

  if (pActiveSolversMap.empty())
  {
    // No active solver, return asap
    pCollectingMode = true;
    if (!pBeforeInitialCollect) pBeforeFinishingFirstCollect = false;
    pBeforeInitialCollect = false;
    return;
  }

  if (pBreakingFirstSubtree)
  {
    // For each active solver, check if the active solver is
    // the one with rank "1", i.e., the first subtree.
    // If so, send the collecting mode tag with the number of nodes to collect
    for(auto it = pActiveSolversMap.begin(); it != pActiveSolversMap.end(); ++it)
    {
      // Need to find the first solver to break at the first subtree
      if (it->second->getRank() == 1)
      {
        auto rank = it->second->getRank();
        assert(pPool.at(SOLVER_POOL_INDEX(rank))->isGenerator());
        if (nodePool->isEmpty() && pNumLimitCollectingModeSolvers > 10)
        {
          utils::sendTagMessageToNetworkNode(
                  pFramework,
                  net::MetaBBPacketTag::PacketTagType::PPT_NO_WAIT_MODE_SEND,
                  rank);
        }
        utils::sendDataMessageToNetworkNode<int>(
                          pFramework,
                          net::MetaBBPacketTag::PacketTagType::PPT_IN_COLLECTING_MODE,
                          rank,
                          numCollect);
        numCollect = 0;
        pPool.at(SOLVER_POOL_INDEX(rank))->setCollectingMode(true);
        if (pCollectingModeSolverHeap)
        {
          pCollectingModeSolverHeap->insert(pPool.at(SOLVER_POOL_INDEX(rank)));
        }

        // Done with the rank 1, increase the number of solvers in collecting mode
        // and break the loop
        pNumCollectingModeSolvers++;
        break;
      }
    }
  }

  if (numCollect > 0)
  {
    double globalBestDualBoundValue = pSelectionHeap->top()->getBestDualBoundValue();

    // Check the collecting mode order of collecting mode request:
    // -1: no ordering;
    //  0: ordered by best dual bound value;
    //  1: ordered by number of nodes left;
    //  2: choose alternatively the best bound and the number of nodes orders
    switch(pParamSet->getDescriptor().solverorderincollectingmode())
    {
      case -1:
      {
        // No ordering
        for(auto it = pActiveSolversMap.begin(); it != pActiveSolversMap.end(); ++it)
        {
          // Check if the current solver has a good node
          if (it->second->getNumOfNodesLeft() > 1 &&
                  hasGoodNode(it->second, globalBestDualBoundValue))
          {
            auto rank = it->second->getRank();
            const auto origRank = SOLVER_POOL_INDEX(rank);
            if (pPool.at(origRank)->isGenerator() &&
                    pPool.at(origRank)->isOutCollectingMode())
            {
              int nCollectPerSolver =
                      std::min(pPool.at(origRank)->getNumOfNodesLeft() / 4, numCollect);
              if (nodePool->getNumOfGoodNodes(getGlobalBestDualBoundValue()) < pMCollectingNodes/4)
              {
                // The following indicates aggressive collecting
                nCollectPerSolver = 0 - (nCollectPerSolver + 1);
              }

              if (nodePool->isEmpty() && pNumLimitCollectingModeSolvers > 10)
              {
                utils::sendTagMessageToNetworkNode(
                        pFramework,
                        net::MetaBBPacketTag::PacketTagType::PPT_NO_WAIT_MODE_SEND,
                        rank);
              }
              utils::sendDataMessageToNetworkNode<int>(
                        pFramework,
                        net::MetaBBPacketTag::PacketTagType::PPT_IN_COLLECTING_MODE,
                        rank,
                        nCollectPerSolver);

              numCollect -= nCollectPerSolver;
              pPool.at(origRank)->setCollectingMode(true);
              pNumCollectingModeSolvers++;
            }
          }
          else
          {
            // The following code is meant to address the case of number of collecting mode solvers
            // getting increased
            if (!(nodePool->isEmpty()) &&
                    (getValueForGoodNode(it->second, globalBestDualBoundValue) > (pBGap * pMBGap)))
            {
              int rank = it->second->getRank();
              sendSwitchOutCollectingModeIfNecessary(rank);
            }
          }
        }
        break;
      }
      case 0:
      {
        // Ordering by best dual bound value
        std::multimap<double, int> boundOrderMap;
        for (auto it = pActiveSolversMap.begin(); it!= pActiveSolversMap.end(); ++it)
        {
          boundOrderMap.insert(std::make_pair(it->second->getBestDualBoundValue(),
                                              it->second->getRank()));
        }

        auto pbo = boundOrderMap.begin();
        for (; pbo != boundOrderMap.end() &&
          pNumLimitCollectingModeSolvers > pNumCollectingModeSolvers; ++pbo)
        {
          int rank = pbo->second;
          const auto origRank = SOLVER_POOL_INDEX(rank);
          if (pPool.at(origRank)->getNumOfNodesLeft() > 1)
          {
            // The solver should have at least two nodes left
            if (pPool.at(origRank)->isGenerator() && pPool.at(origRank)->isOutCollectingMode())
            {
              int nCollectPerSolver =
                      std::min(pPool.at(origRank)->getNumOfNodesLeft()/4, numCollect);
              if (nodePool->getNumOfGoodNodes(getGlobalBestDualBoundValue()) < pMCollectingNodes/4)
              {
                // The following indicates aggressive collecting
                nCollectPerSolver = 0 - (nCollectPerSolver + 1);
              }
              if (nodePool->isEmpty() && pNumLimitCollectingModeSolvers > 10)
              {
                utils::sendTagMessageToNetworkNode(
                        pFramework,
                        net::MetaBBPacketTag::PacketTagType::PPT_NO_WAIT_MODE_SEND,
                        rank);
              }
              utils::sendDataMessageToNetworkNode<int>(
                        pFramework,
                        net::MetaBBPacketTag::PacketTagType::PPT_IN_COLLECTING_MODE,
                        rank,
                        nCollectPerSolver);

              numCollect -= nCollectPerSolver;
              if (pPool.at(origRank)->isCandidateOfCollecting())
              {
                auto pCandidate = pCandidatesOfCollectingModeSolversMap.begin();
                for (; pCandidate != pCandidatesOfCollectingModeSolversMap.end(); ++pCandidate)
                {
                  if (pCandidate->second->getRank() == rank) break;
                }
                assert(pCandidate != pCandidatesOfCollectingModeSolversMap.end());
                pCandidate->second->setCandidateOfCollecting(false);
                pCandidatesOfCollectingModeSolversMap.erase(pCandidate);
              }
              pPool.at(origRank)->setCollectingMode(true);

              assert(pCollectingModeSolverHeap);
              pCollectingModeSolverHeap->insert(pPool.at(origRank));
              pNumCollectingModeSolvers++;
            }
          }
        } // for

        for(; pbo != boundOrderMap.end() &&
          static_cast<int>(pCandidatesOfCollectingModeSolversMap.size()) <
          std::min(pNumLimitCollectingModeSolvers,
                   pParamSet->getDescriptor().nummaxcanditatesforcollecting()); ++pbo)
        {
          int rank = pbo->second;
          const auto origRank = SOLVER_POOL_INDEX(rank);
          if (pPool.at(origRank)->getNumOfNodesLeft() > 1 &&
                  pPool.at(origRank)->isGenerator() &&
                  (!pPool.at(origRank)->isCandidateOfCollecting()))
          {
            pCandidatesOfCollectingModeSolversMap.insert(
                    std::make_pair(pPool.at(origRank)->getBestDualBoundValue(),
                                   pPool.at(origRank)));
            pPool.at(origRank)->setCandidateOfCollecting(true);
          }
        }
        break;
      }
      case 1:
      {
        // Ordering by number of nodes left:
        // using a map to sort by the number of nodes left
        std::multimap<int, int> nNodesOrderMap;
        for (auto it = pActiveSolversMap.begin(); it!= pActiveSolversMap.end(); ++it)
        {
          nNodesOrderMap.insert(std::make_pair(it->second->getNumOfNodesLeft(),
                                               it->second->getRank()));
        }

        for (auto pno = nNodesOrderMap.begin();
                pno != nNodesOrderMap.end() &&
                        pNumLimitCollectingModeSolvers > pNumCollectingModeSolvers; ++pno)
        {
          int rank = pno->second;
          const auto origRank = SOLVER_POOL_INDEX(rank);
          if (pPool.at(origRank)->getNumOfNodesLeft() > 1 &&
                  pPool.at(origRank)->isGenerator() &&
                  hasGoodNode(pPool.at(origRank), globalBestDualBoundValue))
          {
            if (pPool.at(origRank)->isOutCollectingMode())
            {
              int numCollectPerSolver = std::min(
                      pPool.at(origRank)->getNumOfNodesLeft()/4, numCollect);
              if (nodePool->getNumOfGoodNodes(getGlobalBestDualBoundValue()) < pMCollectingNodes/4)
              {
                // The following indicates aggressive collecting
                numCollectPerSolver = 0 - (numCollectPerSolver + 1);
              }
              if (nodePool->isEmpty() && pNumLimitCollectingModeSolvers > 10)
              {
                utils::sendTagMessageToNetworkNode(
                        pFramework,
                        net::MetaBBPacketTag::PacketTagType::PPT_NO_WAIT_MODE_SEND,
                        rank);
              }
              utils::sendDataMessageToNetworkNode<int>(
                        pFramework,
                        net::MetaBBPacketTag::PacketTagType::PPT_IN_COLLECTING_MODE,
                        rank,
                        numCollectPerSolver);

              numCollect -= numCollectPerSolver;
              pPool.at(origRank)->setCollectingMode(true);
              pNumCollectingModeSolvers++;
            }
          }
          else
          {
            // The following code is meant to address the case of number of collecting mode solvers
            // getting increased
            if (!(nodePool->isEmpty()) &&
                (getValueForGoodNode(
                        pPool.at(origRank), globalBestDualBoundValue) > (pBGap * pMBGap)))
            {
              sendSwitchOutCollectingModeIfNecessary(rank);
            }
          }
        }
        break;
      }
      default:
        throw std::runtime_error("SolverPoolForMinimization - switchIntoCollectingMode: "
                "invalid mode type");
    }
  }

  // Flags indicating the collecting mode is on
  pCollectingMode = true;
  if (!pBeforeInitialCollect) pBeforeFinishingFirstCollect = false;
  pBeforeInitialCollect = false;
}  // switchIntoCollectingMode

void SolverPoolForMinimization::updateSolverStatus(int rank, uint64_t numNodesSolved,
                                                   int numNodesLeft,
                                                   double solverLocalBestDualBound,
                                                   NodePool::SPtr nodePool)
{
  auto it = pActiveSolversMap.find(rank);
  if (it == pActiveSolversMap.end())
  {
    throw std::runtime_error("SolverPoolForMinimization - updateSolverStatus: "
            "invalid rank " + std::to_string(rank));
  }

  pNumNodesSolvedInSolvers += numNodesSolved - it->second->getNumOfNodesSolved();
  pNumNodesInSolver += numNodesLeft - it->second->getNumOfNodesLeft();
  it->second->setNumOfDiffNodesSolved(numNodesSolved - it->second->getNumOfNodesSolved());
  it->second->setNumOfNodesSolved(numNodesSolved);
  it->second->setNumOfDiffNodesLeft(numNodesLeft - it->second->getNumOfNodesLeft());
  it->second->setNumOfNodesLeft(numNodesLeft);
  if (it->second->getBestDualBoundValue() < solverLocalBestDualBound)
  {
    it->second->setDualBoundValue(solverLocalBestDualBound);
    pSelectionHeap->updateDualBoundValue(pPool.at(it->second->getSelectionHeapElement()),
                                         solverLocalBestDualBound);
     if (it->second->isInCollectingMode())
     {
       if (pCollectingModeSolverHeap)
       {
         pCollectingModeSolverHeap->updateDualBoundValue(
                 pPool.at(it->second->getCollectingModeSolverHeapElement()),
                 solverLocalBestDualBound);
       }
     }
  }
  double globalBestDualBoundValue = pSelectionHeap->top()->getBestDualBoundValue();
  if (globalBestDualBoundValue > it->second->getBestDualBoundValue())
  {
    throw std::runtime_error("SolverPoolForMinimization - updateSolverStatus: "
            "invalid global best dual bound update");
  }

  const auto origRank = SOLVER_POOL_INDEX(rank);
  if (!pCollectingMode)
  {
    // Throw error if the solver pool is out of collecting mode
    // but the solver is not
    if (!pPool.at(origRank)->isOutCollectingMode())
    {
      throw std::runtime_error("SolverPoolForMinimization - updateSolverStatus: "
              "not out of collecting mode for solver " + std::to_string(rank));
    }
  }

  int numCollect{0};
  if (!pPool.at(origRank)->isInCollectingMode())
  {
    if (!(pSelectionHeap->top()->isInCollectingMode()) &&
            pSelectionHeap->top()->isGenerator() &&
            (!pSelectionHeap->top()->isCollectingProhibited()) &&
            pSelectionHeap->top()->getNumOfNodesLeft() > 1)
    {
      if (pNumLimitCollectingModeSolvers > pNumCollectingModeSolvers)
      {
        switchIntoCollectingToSolver(pSelectionHeap->top()->getRank(), nodePool);
      }
      else
      {
        if (pPool.at(origRank)->getBestDualBoundValue() - pAbsoluteGap <
                pSelectionHeap->top()->getBestDualBoundValue())
        {
           if (!(pBreakingFirstSubtree && rank == 1))
           {
             if (!nodePool->isEmpty())
             {
               sendSwitchOutCollectingModeIfNecessary(rank);
             }
             switchIntoCollectingToSolver(pSelectionHeap->top()->getRank(), nodePool);
           }
        }
      }
    }
    else
    {
      if ((pCollectingModeSolverHeap != nullptr) &&
          (pCollectingModeSolverHeap->getHeapSize() > 0) &&
          !(pBreakingFirstSubtree && pCollectingModeSolverHeap->getHeapSize() <= 1) &&
          (it->second->getBestDualBoundValue() -
                  pCollectingModeSolverHeap->top()->getBestDualBoundValue()) > pAbsoluteGap &&
                    (!pCandidatesOfCollectingModeSolversMap.empty() &&
                            pCandidatesOfCollectingModeSolversMap.begin()->
                            second->getBestDualBoundValue() + pAbsoluteGap) <
                            it->second->getBestDualBoundValue())
      {
        // Check if there are enough nodes to switch out of collecting mode
        if (!nodePool->isEmpty())
        {
          sendSwitchOutCollectingModeIfNecessary(rank);
        }

        // Pick and use a candidate solver for collecting mode
        pickAndUseSolverFromCandidateForCollectingModeSolver(rank, nodePool);
      }
      else
      {
        if(!(nodePool->isEmpty()) &&
                ((getValueForGoodNode(it->second, globalBestDualBoundValue) > (pBGap * pMBGap))))
        {
          sendSwitchOutCollectingModeIfNecessary(it->second->getRank());
        }
      }
    }
  }
  else
  {
    // Current notifying solver is out of collecting mode
    if (pNumLimitCollectingModeSolvers > pNumCollectingModeSolvers)
    {
      // Check if:
      // 1) the current solver is the top of the selection heap; and
      // 2) the top is also a generator solver; and
      // 3) the top can collect nodes (i.e., not prohibited); and
      // 4) the number of nodes left in top is at least two.
      // If so, set the top as collecting mode solver
      if (pPool.at(origRank)->getRank() == pSelectionHeap->top()->getRank() &&
              pPool.at(origRank)->isGenerator() &&
              (!pPool.at(origRank)->isCollectingProhibited()) &&
              pSelectionHeap->top()->getNumOfNodesLeft() > 1)
      {
        switchIntoCollectingToSolver(rank, nodePool);
      }
    }
    else
    {
      // Switch out of collecting mode since the limit is equal to the number
      // of collecting mode solvers.
      // Check if:
      // 1) the node pool is not empty (i.e., there are nodes to solve); and
      // 2) the given solver (input) has a difference of number of solvers (between previous
      //    notification and this current one) greater than one; and
      // 3) there is a collecting mode solver heap; and
      // 4) either the algorithm breaks at the first subtree or the collecting mode heap has
      //    mode than one solver; and
      // 5) the given solver has a best dual bound smaller than the best dual bound of the top of
      //    the collecting mode solver heap.
      // If all the above hold, switch out of collecting mode the top of the collecting mode heap
      if (!(nodePool->isEmpty()) &&
              pPool.at(origRank)->getNumOfDiffNodesLeft() > 1 &&
              pCollectingModeSolverHeap &&
              !(pBreakingFirstSubtree && pCollectingModeSolverHeap->getHeapSize() <= 1) &&
              (pPool.at(origRank)->getBestDualBoundValue() + pAbsoluteGap)
              < pCollectingModeSolverHeap->top()->getBestDualBoundValue())
      {
        sendSwitchOutCollectingModeIfNecessary(pCollectingModeSolverHeap->top()->getRank());
      }
    }

    // Check if there are idle solvers to be used
    bool manyIdleSolversExist = getNumInactiveSolvers() > (pNumSolvers/4);
    if (pNumLimitCollectingModeSolvers > pNumCollectingModeSolvers)
    {
      if (manyIdleSolversExist)
      {
        // There are idle solvers and the limit of collecting mode solvers
        // is not yet reached
        auto tempTimestamp = getSwichOutTime();
        switchOutCollectingMode();
        setSwichOutTime(tempTimestamp);
        switchIntoCollectingMode(nodePool);
      }
      else
      {
        if (pSelectionHeap->top()->getNumOfNodesLeft() > 1 &&
                pSelectionHeap->top()->isGenerator() &&
                !(pSelectionHeap->top()->isCollectingProhibited()) &&
                pSelectionHeap->top()->isOutCollectingMode())
        {
          // Switch the top of the selection heap into a collecting mode solver
          switchIntoCollectingToSolver(pSelectionHeap->top()->getRank(), nodePool);
        }
        else
        {
          // The top of the selection heap cannot be used as collecting mode solver.
          // Pick another solver to be used as collecting mode solver
          if (!pCandidatesOfCollectingModeSolversMap.empty())
          {
            pickAndUseSolverFromCandidateForCollectingModeSolver(rank, nodePool);
          }
          else
          {
            // There is no candidate solver for collecting mode
            if (pSelectionHeap->top()->getNumOfNodesLeft() > 1 &&
                    pSelectionHeap->top()->isGenerator() &&
                    !(pSelectionHeap->top()->isCollectingProhibited()) &&
                    pSelectionHeap->top()->isOutCollectingMode() &&
                    !(nodePool->isEmpty()) &&
                    ((getValueForGoodNode(
                            it->second, globalBestDualBoundValue) > (pBGap * pMBGap))))
            {
              switchIntoCollectingToSolver(rank, nodePool);
            }
          }
        }
      }
    }
    else if (pPool.at(origRank)->isOutCollectingMode())
    {
      // Limit on the number of collecting mode solver has been reached
      // and the given solver is out of collecting mode.
      // Remove it from the candidate solvers for collecting mode
      if (!pCandidatesOfCollectingModeSolversMap.empty())
      {
        if (it->second->isCandidateOfCollecting())
        {
          auto candidate = pCandidatesOfCollectingModeSolversMap.begin();
          for (; candidate != pCandidatesOfCollectingModeSolversMap.end(); ++candidate)
          {
            // The candidate has been found, break the loop
            if (candidate->second->getRank() == rank) break;
          }

          if (candidate == pCandidatesOfCollectingModeSolversMap.end())
          {
            throw std::runtime_error("SolverPoolForMinimization - updateSolverStatus: "
                    "invalid rank for candidate solvers for collecting mode " +
                    std::to_string(rank));
          }
          candidate->second->setCandidateOfCollecting(false);
          pCandidatesOfCollectingModeSolversMap.erase(candidate);
        }

        // Switch the current solver with a candidate one if the candidate
        // has a better best dual bound value
        if (static_cast<int>(pCandidatesOfCollectingModeSolversMap.size()) ==
                std::min(pNumLimitCollectingModeSolvers,
                         pParamSet->getDescriptor().nummaxcanditatesforcollecting()))
        {
          auto rcandidate = pCandidatesOfCollectingModeSolversMap.rbegin();
          if (it->second->getNumOfNodesLeft() > 1 && it->second->isGenerator() &&
                  rcandidate->second->getBestDualBoundValue() > it->second->getBestDualBoundValue())
          {
            auto ptrCandidate = rcandidate->second;
            ++rcandidate;
            assert((rcandidate.base()->second).get() == ptrCandidate.get());

            rcandidate.base()->second->setCandidateOfCollecting(false);
            pCandidatesOfCollectingModeSolversMap.erase(rcandidate.base());
            pCandidatesOfCollectingModeSolversMap.insert(
                    std::make_pair(it->second->getBestDualBoundValue(), it->second)
                  );
            it->second->setCandidateOfCollecting(true);
          }
        }
        else
        {
          if (pCollectingModeSolverHeap &&
                  !(pBreakingFirstSubtree && pCollectingModeSolverHeap->getHeapSize() <= 1) &&
                  it->second->getNumOfNodesLeft() > 1 && it->second->isGenerator() &&
                  (it->second->getBestDualBoundValue() -
                          pCollectingModeSolverHeap->top()->getBestDualBoundValue()) < pAbsoluteGap)
          {
            pCandidatesOfCollectingModeSolversMap.insert(
                    std::make_pair(it->second->getBestDualBoundValue(), it->second));
            it->second->setCandidateOfCollecting(true);
          }
        }
      }
      else
      {
        // pCandidatesOfCollectingModeSolversMap is empty
        if (pCollectingModeSolverHeap &&
                !(pBreakingFirstSubtree && pCollectingModeSolverHeap->getHeapSize() <= 1) &&
                it->second->getNumOfNodesLeft() > 1 && it->second->isGenerator() &&
                (it->second->getBestDualBoundValue() -
                        pCollectingModeSolverHeap->top()->getBestDualBoundValue()) < pAbsoluteGap)
        {
          pCandidatesOfCollectingModeSolversMap.insert(
                  std::make_pair(it->second->getBestDualBoundValue(), it->second));
          it->second->setCandidateOfCollecting(true);
        }
      }
    }
  }
}  // updateSolverStatus

void SolverPoolForMinimization::pickAndUseSolverFromCandidateForCollectingModeSolver(
        int rankSolver, NodePool::SPtr nodePool)
{
  auto candidate = pCandidatesOfCollectingModeSolversMap.begin();
  assert(candidate->second->isOutCollectingMode());

  // Calculate the number of nodes to be collected as the minimum between:
  // a) 1/4 of the number of nodes left in the candidate
  // b) the number of inactive solvers plus the default number of nodes to collect minus
  //    the number of good nodes in the node pool
  int numCollect =
          std::min(candidate->second->getNumOfNodesLeft()/4,
                   static_cast<int>(getNumInactiveSolvers()) +
                   static_cast<int>(pParamSet->getDescriptor().multiplierforcollectingmode() *
                                    pMCollectingNodes) -
                   static_cast<int>(nodePool->getNumOfGoodNodes(getGlobalBestDualBoundValue())));

  // Check if the number of good nodes in the node pool is less than the standard
  // number of nodes to collect.
  if (static_cast<int>(nodePool->getNumOfGoodNodes(getGlobalBestDualBoundValue()))
          < pMCollectingNodes / 4)
  {
    // If so, use aggressive collecting.
    // @note the following indicates aggressive collecting
    numCollect = 0 - (numCollect + 1);
  }
  if (nodePool->isEmpty() && pNumLimitCollectingModeSolvers > 10)
  {
    // Node pool is empty, communicate to the given solver to not wait for sending nodes.
    // TODO check whether this should be the candidate solver and not the given rank
    utils::sendTagMessageToNetworkNode(pFramework,
                                       net::MetaBBPacketTag::PacketTagType::PPT_NO_WAIT_MODE_SEND,
                                       rankSolver);
  }

  // Turn the candidate into a collecting mode solver
  utils::sendDataMessageToNetworkNode<int>(
          pFramework, net::MetaBBPacketTag::PacketTagType::PPT_IN_COLLECTING_MODE,
          candidate->second->getRank(), numCollect);

  candidate->second->setCollectingMode(true);
  if (pCollectingModeSolverHeap)
  {
    pCollectingModeSolverHeap->insert(candidate->second);
  }

  // One more collecting mode solver
  pNumCollectingModeSolvers++;

  // The solver is not a candidate for collecting anymore
  // since it is actively collecting
  candidate->second->setCandidateOfCollecting(false);
  pCandidatesOfCollectingModeSolversMap.erase(candidate);
}  // pickAndUseSolverFromCandidateForCollectingModeSolver

}  // namespace metabb
}  // namespace optilab
