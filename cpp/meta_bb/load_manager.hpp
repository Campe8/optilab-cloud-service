//
// Copyright OptiLab 2019-2020. All rights reserved.
//
// The load manager is the main component of the framework.
// It coordinates the load and splits the nodes between different workers,
// managing the search among the solvers/workers.
//

#pragma once

#include <algorithm>   // for std::max
#include <cstddef>     // for std::size_t
#include <cstdint>     // for uint64_t
#include <deque>
#include <functional>  // for std::function
#include <limits>      // for std::numeric_limits
#include <memory>      // for std::shared_ptr
#include <string>
#include <unordered_map>
#include <vector>

#include "meta_bb/calculation_state.hpp"
#include "meta_bb/initiator.hpp"
#include "meta_bb/framework.hpp"
#include "meta_bb/load_manager_termination_state.hpp"
#include "meta_bb/node.hpp"
#include "meta_bb/node_pool.hpp"
#include "meta_bb/meta_bb_constants.hpp"
#include "meta_bb/meta_bb_utilities.hpp"
#include "meta_bb/paramset.hpp"
#include "meta_bb/racing_rampup_paramset.hpp"
#include "meta_bb/solution.hpp"
#include "meta_bb/solver_pool.hpp"
#include "meta_bb/meta_bb_struct.hpp"
#include "optimizer_network/network_communicator.hpp"
#include "optimizer_network/packet.hpp"
#include "system/system_export_defs.hpp"
#include "utilities/timer.hpp"

// Forward declarations
namespace optilab {
namespace  metabb {
class LoadManagerImpl;
class LoadManagerMergeImpl;
}  // namespace metabb
}  // namespace optilab

namespace optilab {
namespace  metabb {

class SYS_EXPORT_CLASS LoadManager {
 public:
  enum RunningPhase {
    LMRP_RAMPUP = 0,
    LMRP_RUNNING,
    LMRP_TERMINATION
  };

  using SPtr = std::shared_ptr<LoadManager>;

 public:
  /// Default constructor.
  /// Creates an object without any framework, parameter set, or any
  /// other internal data-structure or object.
  /// This is mostly used for derived classes and testing.
  /// @note this does NOT call the initialize method.
  /// See also "initLoadManager(...)"
  LoadManager();

  /// Constructor for LoadManger.
  /// @note throws std::invalid_argument is any of the input arguments (besides time)
  /// is an empty pointer.
  /// @note if timer is not specified, it instantiates and runs a new timer.
  /// @note the framework MUST hold an "optnet::NetworkCommunicator" instance.
  /// If not, throws std::runtime_error
  LoadManager(Framework::SPtr framework, ParamSet::SPtr paramSet,
              Initiator::SPtr initiator, timer::Timer::SPtr timer=nullptr);

  /// Destructor of the LoadManager object.
  /// @note this will call the virtual "tearDown(...)" method
  virtual ~LoadManager();

  static int GetLoadManagerRank();

  /// Interrupt the load manager
  inline void interrupt() { pForceInterrupt = true; }

  /// Perform warm start
  void warmStart();

  /// Runs the load manager on the given node.
  /// Solve the node with normal rump-up,
  /// i.e., active solvers solve the received node/subproblem by
  /// alternately solving nodes and transferring half of the children nodes back
  /// to the LM until LM has at least p good nodes.
  /// After that, active solvers solve their subproblems
  void run(Node::SPtr node);

  /// Runs the load manager on the given node.
  /// Solve the node with racing ramp-up.
  /// The method has a given number of racing solvers and a
  /// given set of ramp-up parameters to perform racing ramp-up
  void run(Node::SPtr node, int numRacingSolvers,
           std::vector<RacingRampUpParamSet::SPtr>& racingRampupParams);

  /// Sets the global best incumbent solution
  void setGlobalBestIncumbentSolution(Solution::SPtr sol);

 protected:
  friend class LoadManagerImpl;
  friend class LoadManagerMergeImpl;

  /// Flag indicating tear-down has beend executed already
  bool pTearDown{false};

  /// Termination state for the load manager (counters and time)
  LoadManagerTerminationState pLMTerminationState;

  /// Initializes the load manager
  virtual void initLoadManager();

  /// Tear down the load manager
  virtual void tearDown();

  /**
   * The following methods are purely for derived classes to set
   * their own implementations of initiator, framework, etc.
   * For example, these methods can be used by mock classes for unit testing.
   */
  inline void setTimer(timer::Timer::SPtr timer) { pTimer = timer; }
  inline void setFramework(Framework::SPtr framework) { pFramework = framework; }
  inline void setInitiator(Initiator::SPtr initiator) { pInitiator = initiator; }
  inline void setParamSet(ParamSet::SPtr paramSet) { pParamSet = paramSet; }
  inline void setNodePool(NodePool::SPtr nodePool) { pNodePool = nodePool; }
  inline void setSolverPool(SolverPool::SPtr solverPool) { pSolverPool = solverPool; }
  inline void setCommunicator(optnet::NetworkCommunicator::SPtr comm) { pCommunicator = comm; }
  inline void setRacingSolverPool(RacingSolverPool::SPtr racingPool)
  {
    pRacingSolverPool = racingPool;
  }

  /**
   * Get methods for derived classes.
   */
  inline bool isRacingTermination() const noexcept { return pRacingTermination; }
  inline int getNumNodesSolvedAtRacingTermination() const noexcept
  {
    return pNumNodesSolvedAtRacingTermination;
  }
  inline std::size_t getNumTerminated() const noexcept { return pNumTerminated; }
  inline bool isHardTimeLimitReached() const noexcept { return pHardTimeLimitIsReached; }

 private:
  /// Pointer to the framework
  Framework::SPtr pFramework;

  /// Pointer to the network communicator used by the solver
  /// to send/receive messages across the network
  optnet::NetworkCommunicator::SPtr pCommunicator;

  /// Pointer to this load manager's implementation
  std::shared_ptr<LoadManagerImpl> pLoadManagerImpl;
  std::shared_ptr<LoadManagerImpl> pLoadManagerRacingRampUpImpl;

  /// Pointer to this load manager's merge implementation
  std::shared_ptr<LoadManagerMergeImpl> pLoadManagerMergeImpl;

  /// State of the load manager.
  /// @note by default, the manager starts in ramp-up phase
  RunningPhase pRunningPhase{RunningPhase::LMRP_RAMPUP};

  /// Global sub-tree id
  int pGlobalSubtreeIdGen{-1};

  /// Pointer to the set of parameters
  ParamSet::SPtr pParamSet;

  /// Pointer to the instance of the initiator
  Initiator::SPtr pInitiator;

  /// Pointer to the timer used in the manager
  timer::Timer::SPtr pTimer;

  /// Force interrupt flag, set by clients of the interface
  bool pForceInterrupt{false};

  /// Flag indicating that this run is restarted from a checkpoint file
  bool pRestartedFromCheckpointFile{false};

  /// Flag indicating that the initial nodes have been generated
  bool pInitialNodesGenerated{false};

  /// Status of the first collecting mode:
  /// -1) not being in collecting mode
  ///  0) in collecting mode once
  ///  1) collecting mode is terminated once
  int pFirstCollectingModeState{-1};

  /// Flag indicating if collecting mode has been restarted or not
  bool pIsCollectingModeRestarted{false};

  /// Flag indicating that breaking is finished (or not)
  bool pIsBreakingFinished{true};

  /// All nodes collecting solver Id:
  /// -1) no collecting
  int pBreakingSolverId{-1};

  /// Number of replacing to a better node
  int pNumReplaceToBetterNode{0};

  /// Flag indicating that the current computation is interrupted (or not)
  bool pComputationIsInterrupted{false};

  /// Flag indicating whether the hard time limit is reached or not
  bool pHardTimeLimitIsReached{false};

  /// Flag indicating whether all winner solver nodes have been collected or not
  bool pWinnerSolverNodesCollected{false};

  /// Flag to indicate that all solver interrupt messages are requested
  bool pInterruptIsRequested{false};

  /// Flag indicating whether the primal solution is updated or not
  bool pPrimalSolutionUpdated{false};

  /// Flag indicating that racing ramp-up is restarting
  bool pRestartRacing{false};

  /// Number of racing stages restarted
  int pNumRestartRacing{0};

  /// Pool of nodes
  NodePool::SPtr pNodePool;

  /// Pool of running solvers/workers
  SolverPool::SPtr pSolverPool;

  /// Pool of racing solvers/workers
  RacingSolverPool::SPtr pRacingSolverPool;

  /// Number of nodes solved when interrupting racing solvers
  long long pNumNodesSolvedInInterruptedRacingSolvers{-1};

  /// Number of nodes left to solve when interrupting racing solvers
  long long pNumNodesLeftInInterruptedRacingSolvers{-1};

  /// Number of nodes subject to random selection from the node pool.
  /// @note -1 indicates no random selection performed
  int pNumNodesNormalSelection{-1};

  /// Previous checkpoint time in msec
  uint64_t pPreviousCheckpointTimeMsec{0};

  /// String representing last checkpoint.
  /// @note if this string is empty, it means no checkpoint was performed
  std::string pLastCheckpointTimeStrMsec;

  /// Measure how long the node pool stays in an empty state.
  /// This is initialized every time the node pool becomes empty,
  /// By default it is set to uint64_t max
  uint64_t pEmptyNodePoolTimeMsec{std::numeric_limits<uint64_t>::max()};

  /// Epsilon value used to round to zero (when value is lower than epsilon).
  /// @note this value is initialized to zero
  double pEpsilon{0.0};

  /// Racing winner
  int pRacingWinner{-1};

  /// Mininum depth for winner
  int pMinDepthInWinnerSolverNodes{std::numeric_limits<int>::max()};

  /// Maximum depth for winner
  int pMaxDepthInWinnerSolverNodes{-1};

  /// Parameter set of the winner in racing ramp-up
  RacingRampUpParamSet::SPtr pRacingRampUpWinnerParamSet;

  /// Racing termination flag
  bool pRacingTermination{false};

  /// Number of nodes solved at racing termination
  int pNumNodesSolvedAtRacingTermination{0};

  /// Flag indicating that merging is in process
  bool pMerging{false};

  /// Counter to check all solver's termination
  std::size_t pNumTerminated{0};

  /// Number of collected solvers
  std::size_t pNumCollectedSolvers{0};

  /// Average dual bound gain.
  /// @note this can be negative at restart
  double pAverageDualBoundGain{0.0};

  /// Number of nodes whose dual bound gains are counted
  int pNumAverageDualBoundGain{0};

  /// Deque to keep track of last several dual bound gains
  std::deque<double> pLastSeveralDualBoundGains;

  /// Average dual bound gain for last several ones
  double pAverageLastSeveralDualBoundGains{0.0};

  /// Start time in msec of starving active solvers
  uint64_t pStarvingTimeMsec{0};

  /// Start time of huge imbalance situation
  uint64_t pHugeImbalanceTimeMsec{0};

  /// Node pool used to restart in ramp-down phase
  NodePool::SPtr pNodePoolToRestart;

  /// Node pool for buffering nodes in huge imbalance situation
  NodePool::SPtr pNodePoolBufferToRestart;

  /// Node pool for buffering nodes used to generate reduced checkpoint files
  NodePool::SPtr pNodePoolBufferToGenerateCPF;

  /// Node pool used to keep "n" nodes in checkpoint file,
  /// i.e., "n" nodes are not processed in current runs
  NodePool::SPtr pNodePoolToKeepCheckpointFileNodes;

  bool pAllCompInfeasibleAfterSolution{false};

  double pMinimalDualBoundNormalTermSolvers{std::numeric_limits<double>::max()};

  bool pWarmStartNodeTransferring{false};

  /// Flag indicating if there is a huge imbalance situation going on
  bool pHugeImbalance{false};

  /// Pointer to the current pending solution
  Solution::SPtr pPendingSolution;

  inline ProtoParamSetDescriptor& getParams() { return pParamSet->getDescriptor(); }

  /// Utility function, wait for any incoming message from any network node.
  /// Upon receiving the message, execute the correspondent callback
  void waitForAndExecuteIncomingMessage(std::shared_ptr<LoadManagerImpl> callback);

  /// Sends ramp-up message to all solvers
  void sendRampUpToAllSolvers();

  /// Sends retry ramp-up phase to all solvers
  void sendRetryRampUpToAllSolvers();

  /// Sends interrupt message to all solvers/workers
  void sendInterruptRequest();

  /// Terminates all solvers
  void terminateAllSolvers();
  inline void terminateSolvers() { terminateAllSolvers(); }

  /// Update the current best solution.
  /// Returns true if the update was successful, return false otherwise
  bool updateSolution(const Solution::SPtr& sol);

  /// Sends the incumbent value received from "sourceRank" to all other solvers.
  /// @note the sourceRank is used to avoid sending the incumbent to the source itself
  void sendIncumbentValue(int sourceRank);

  /// Sends the cutoff value from "sourceRank" to all other solvers.
  /// @note the sourceRank is used to avoid sending the incumbent to the source itself
  void sendCutOffValue(int sourceRank);

  /// Returns a new global subtree identifier
  inline int getNewGlobalSubtreeId() noexcept { return ++pGlobalSubtreeIdGen; }

  /// Actual branch-and-bound run process
  void run();

  /// Sends nodes from node pool to idle solvers
  bool sendNodesToIdleSolvers();

  /// Returns true if the LM is in racing stage,
  /// returns false otherwise
  inline bool isRacingStage()
  {
    // It is in racing stage if:
    // 1) it is not warm started (no need for racing); and
    // 2) running phase is ramp-up (it is in racing); and
    // 3) ramp-up phase is racing; and
    // 4) there is no racing winner yet
    // @note RampUpPhaseProcess == 0 is normal,
    // while RampUpPhaseProcess > 0 is racing
    return
        !pInitiator->isWarmStarted() &&
        pRunningPhase == RunningPhase::LMRP_RAMPUP &&
        getParams().rampupphaseprocess() > 0 &&
        pRacingWinner < 0;
  }

  /// Deactivates a solver in the racing solver pool
  void deactivateRacingSolverPool(int rank);

  /// Broadcasts the given tag to all solvers
  void sendTagToAllSolvers(const net::MetaBBPacketTag::PacketTagType tag);

  /// Restarts racing and reset the flag "pRestartRacing" to false.
  /// Returns zero on success, non-zero otherwise
  int restartRacing();

  /// Switches the search strategy on all solvers to "best bound"
  void changeSearchStrategyOfAllSolversToBestBoundSearch();

  /// Switches the search strategy on all solvers back to the original strategy
  void changeSearchStrategyOfAllSolversToOriginalSearch();

  /// Utility function: updates the global best dual bound value in the
  /// LM termination state w.r.t. the node pool
  void updateGlobalBestDualBoundWithNodePool();

  /// Returns the best global dual bound value between the solver pool, the node pool,
  /// and the LM termination state
  inline double getGlobalBestDualBoundValueLocal() const
  {
    return std::max(
            std::min(pSolverPool->getGlobalBestDualBoundValue(),
                     pNodePool->getBestDualBoundValue()),
            pLMTerminationState.globalBestDualBoundValue);
  }

  /// Utility function: returns true on timeout, false otherwise
  inline bool timeout()
  {
    const double timelimitMsec = getParams().timelimitmsec();
    return timelimitMsec > 0 && static_cast<double>(pTimer->getWallClockTimeMsec()) >
    timelimitMsec;
  }

  // The following are used when logging is activated
  // void writeTransferLog(int solverId, CalculationState::SPtr state);
  // void writeTransferLog(int solverId);
  // void writeTransferLogInRacing(int solverId, CalculationState::SPtr state);
  // void writeTransferLogInRacing(int solverId);
  // void writeSubtreeInfo(int source, CalculationState::SPtr calcState);
};

}  // namespace metabb
}  // namespace optilab
