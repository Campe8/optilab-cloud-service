//
// Copyright OptiLab 2020. All rights reserved.
//
// Solver termination state class implemented on top of the
// optnet framework.
//

#pragma once

#include <cstdint>  // for uint64_t
#include <memory>   // for std::shared_ptr

#include "meta_bb/framework.hpp"
#include "meta_bb/solver_termination_state.hpp"
#include "optilab_protobuf/meta_bb_packet.pb.h"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace  metabb {

class SYS_EXPORT_CLASS NetSolverTerminationState : public SolverTerminationState {
 public:
  using SPtr = std::shared_ptr<NetSolverTerminationState>;

 public:
  /// Default constructor
  NetSolverTerminationState();

  NetSolverTerminationState(solver::InterruptionPoint interrupted,
                            int rank,
                            int totalNumSolved,
                            int minNumSolved,
                            int maxNumSolved,
                            int totalNumSent,
                            int totalNumImprovedIncumbent,
                            int numNodesReceived,
                            int numNodesSolved,
                            int numNodesSolvedAtRoot,
                            int numNodesSolvedAtPreCheck,
                            int numTransferredLocalCutsFromSolver,
                            int minNumTransferredLocalCutsFromSolver,
                            int maxNUmTransferredLocalCutsFromSolver,
                            int numTotalRestarts,
                            int minNumRestarts,
                            int maxNumRestarts,
                            int numVarTightened,
                            int numIntVarTightenedInt,
                            uint64_t runningTimeMsec,
                            uint64_t idleTimeToFirstNodeMsec,
                            uint64_t idleTimeBetweenNodesMsec,
                            uint64_t idleTimeAfterLastNodeMsec,
                            uint64_t idleTimeToWaitNotificationIdMsec,
                            uint64_t idleTimeToWaitAckCompletionMsec,
                            uint64_t idleTimeToWaitTokenMsec,
                            uint64_t totalRootNodeTimeMsec,
                            uint64_t minRootNodeTimeMsec,
                            uint64_t maxRootNodeTimeMsec);

  /// Destructor:
  /// checks the node parent and children and re-connect parent to children
  ~NetSolverTerminationState() override;

  /// Sends this state to the destination using the given framework
  int send(const Framework::SPtr& framework, int destination) noexcept override;

  /// Uploads the content of the given network packet into this calculation state..
  /// If the given packet is not a node network packet, throws std::runtime_error
  /// Returns zero on success, non-zero otherwise
  int upload(optnet::Packet::UPtr packet) noexcept override;

 protected:
  /// Builds and returns a network packet encoding this solver state
  optnet::Packet::UPtr buildNetworkPacket();
};

}  // namespace metabb

/// Specialized template for network node packet comparison
template<> bool optnet::NetworkPacket<metabb::SolverTerminationStateProtoPacket>::operator==(
    const NetworkPacket<metabb::SolverTerminationStateProtoPacket>& other) const noexcept;

/// Specialized template to serialize NetSolverState(s)
template<> std::pair<optnet::BaseNetworkPacket::BaseNetworkPacketType, std::string>
optnet::NetworkPacket<metabb::SolverTerminationStateProtoPacket>::serialize() const noexcept;

}  // namespace optilab
