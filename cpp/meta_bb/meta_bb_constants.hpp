//
// Copyright OptiLab 2019. All rights reserved.
//
// Constants for metabb namespace.
//

#pragma once

#include "system/system_export_defs.hpp"

namespace optilab {
namespace  metabb {

namespace solverparamset {
/// Placeholder value for default double parameter
extern const double DEFAULT_DOUBLE_PARAM_VALUE;
extern const double DEFAULT_DUAL_TOLERANCE;

/// Placeholder value for default integer parameter
extern const double DEFAULT_INTEGER_PARAM_VALUE;
extern const double DEFAULT_PRIMAL_TOLERANCE;
extern const double DEFAULT_RELATIVE_MIP_GAP;

/// Placeholder value for default unknown double parameter
extern const double DEFAULT_UNKNOWN_DOUBLE_PARAM_VALUE;

/// Placeholder value for default unknown integer parameter
extern const double DEFAULT_UNKNOWN_INTEGER_PARAM_VALUE;
} // namespace solverparamset

namespace metaconst {
/// Default upper bound for floating points to be considered zero
extern const double DEAFULT_NUM_EPSILON;
extern const double DOUBLE_NEG_INF;
extern const double DOUBLE_POS_INF;
extern const int ERR_GENERIC_ERROR SYS_EXPORT_VAR;
extern const int ERR_NO_ERROR SYS_EXPORT_VAR;

/// Max double same as DOUBLE_POS_INF
extern const double MAX_DOUBLE;

/// MIN double same as DOUBLE_NEG_INF
extern const double MIN_DOUBLE;

/// Minimum value for numerical epsilon
extern const double MIN_EPSILON;
}  // metaconst

namespace  treeconsts {
extern const int DEFAULT_UNSPECIFIED_ID SYS_EXPORT_VAR;
}  // namespace treeconsts

namespace solver {
enum FinalSolverState : int {
   FSS_INITIAL_NODES_GENERATED,
   FSS_ABORTED,
   FSS_HARD_TIME_LIMIT_REACHED,
   FSS_COMPUTING_INTERRUPTED,
   FSS_PROBLEM_SOLVED,
   FSS_REQUESTED_SUBPROBLEMS_SOLVED
};

enum TerminationMode : int {
  TM_NO_TERMINATION,
  TM_NORMAL_TERMINATION,
  TM_INTERRUPTED_TERMINATION,
  TM_TIME_LIMIT_TERMINATION
};

enum SearchStrategy : int {
  SS_ORIGINAL,
  SS_BEST_BOUND
};

enum NotificationSychronization : int {
  NS_ALWAYS_SYNCHRONIZE = 0,
  NS_EVERY_ITERATION_IN_COLLECTING = 1,
  NS_NEVER_SYNCHRONIZE = 2
};

enum TerminationState : int {
  TS_TERMINATED_NORMALLY = 0,
  TS_TERMINATED_BY_ANOTHER_NODE,
  TS_TERMINATED_BY_INTERRUPT_REQUEST,
  TS_TERMINATED_IN_RACING_STAGE,
  TS_INTERRUPTED_IN_RACING_STAGE,
  TS_INTERRUPTED_IN_MERGING,
  TS_TERMINATED_BY_TIME_LIMIT
};

// Indicates whether or not a solver has been interrupted
// and if so, what stage was in
enum InterruptionPoint : int {
  IP_NOT_INTERRUPTED = 0,
  IP_INTERRUPTED,
  IP_INTERRUPTED_CHECKPOINT,
  IP_INTERRUPTED_RACING_RAMPUP
};

enum SolverStatus : int {
  SS_INACTIVE = 0,
  SS_RACING,
  SS_RACING_EVALUATION,
  SS_ACTIVE,
  SS_DEAD
};

extern const int RAMP_UP_PHASE_NORMAL;
extern const int RAMP_UP_PHASE_RACING;
extern const int RAMP_UP_PHASE_REBUILD_TREE_AFTER_RACING;
} // namespace solver

namespace loadmanager {

enum RampUpPhaseProcess : int {
  RPP_NORMAL = 0,
  RPP_RACING,
  RPP_REBUILD_TREE_AFTER_RACING
};

}  // namespace loadmanager

}  // namespace metabb
}  // namespace optilab
