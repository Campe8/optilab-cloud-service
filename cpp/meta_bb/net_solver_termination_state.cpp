#include "meta_bb/net_solver_termination_state.hpp"

#include <memory>     // for std::dynamic_pointer_cast
#include <stdexcept>  // for std::invalid_argument
#include <string>
#include <utility>    // for std::move

#include <google/protobuf/util/message_differencer.h>
#include <spdlog/spdlog.h>

#include "meta_bb/meta_bb_constants.hpp"
#include "meta_bb/meta_bb_struct.hpp"
#include "meta_bb/meta_bb_utilities.hpp"
#include "optimizer_network/network_communicator.hpp"
#include "optimizer_network/packet.hpp"

namespace optilab {
namespace  metabb {

NetSolverTerminationState::NetSolverTerminationState()
: SolverTerminationState()
{
}

NetSolverTerminationState::NetSolverTerminationState(solver::InterruptionPoint interrupted,
                                                     int rank,
                                                     int totalNumSolved,
                                                     int minNumSolved,
                                                     int maxNumSolved,
                                                     int totalNumSent,
                                                     int totalNumImprovedIncumbent,
                                                     int numNodesReceived,
                                                     int numNodesSolved,
                                                     int numNodesSolvedAtRoot,
                                                     int numNodesSolvedAtPreCheck,
                                                     int numTransferredLocalCutsFromSolver,
                                                     int minNumTransferredLocalCutsFromSolver,
                                                     int maxNUmTransferredLocalCutsFromSolver,
                                                     int numTotalRestarts,
                                                     int minNumRestarts,
                                                     int maxNumRestarts,
                                                     int numVarTightened,
                                                     int numIntVarTightenedInt,
                                                     uint64_t runningTimeMsec,
                                                     uint64_t idleTimeToFirstNodeMsec,
                                                     uint64_t idleTimeBetweenNodesMsec,
                                                     uint64_t idleTimeAfterLastNodeMsec,
                                                     uint64_t idleTimeToWaitNotificationIdMsec,
                                                     uint64_t idleTimeToWaitAckCompletionMsec,
                                                     uint64_t idleTimeToWaitTokenMsec,
                                                     uint64_t totalRootNodeTimeMsec,
                                                     uint64_t minRootNodeTimeMsec,
                                                     uint64_t maxRootNodeTimeMsec)
: SolverTerminationState(interrupted,
                         rank,
                         totalNumSolved,
                         minNumSolved,
                         maxNumSolved,
                         totalNumSent,
                         totalNumImprovedIncumbent,
                         numNodesReceived,
                         numNodesSolved,
                         numNodesSolvedAtRoot,
                         numNodesSolvedAtPreCheck,
                         numTransferredLocalCutsFromSolver,
                         minNumTransferredLocalCutsFromSolver,
                         maxNUmTransferredLocalCutsFromSolver,
                         numTotalRestarts,
                         minNumRestarts,
                         maxNumRestarts,
                         numVarTightened,
                         numIntVarTightenedInt,
                         runningTimeMsec,
                         idleTimeToFirstNodeMsec,
                         idleTimeBetweenNodesMsec,
                         idleTimeAfterLastNodeMsec,
                         idleTimeToWaitNotificationIdMsec,
                         idleTimeToWaitAckCompletionMsec,
                         idleTimeToWaitTokenMsec,
                         totalRootNodeTimeMsec,
                         minRootNodeTimeMsec,
                         maxRootNodeTimeMsec)
{
}

NetSolverTerminationState::~NetSolverTerminationState()
{
  // No-op
}

int NetSolverTerminationState::send(const Framework::SPtr& framework, int destination) noexcept
{
  if (!framework)
  {
    spdlog::error("NetSolverTerminationState - send: empty network communicator");
    return metaconst::ERR_GENERIC_ERROR;
  }

  // Create the optimizer node packet to send to the destination
  optnet::Packet::UPtr packet = buildNetworkPacket();

  // Add the node tag
  packet->packetTag = optnet::PacketTag::UPtr(new net::MetaBBPacketTag(
          net::MetaBBPacketTag::PacketTagType::PPT_TERMINATED));

  if (framework->getComm()->send(std::move(packet), destination))
  {
    return metaconst::ERR_GENERIC_ERROR;
  }

  return metaconst::ERR_NO_ERROR;
}  // send

int NetSolverTerminationState::upload(optnet::Packet::UPtr packet) noexcept
{
  if (!packet)
  {
    spdlog::error("NetSolverTerminationState - upload: empty packet");
    return metaconst::ERR_GENERIC_ERROR;
  }

  // Check that the received packet is a NODE packet
  try
  {
    const auto tag = utils::castToMetaBBPacketTagAndGetTagOrThrow(packet->packetTag);
    if (tag != net::MetaBBPacketTag::PacketTagType::PPT_TERMINATED)
    {
      spdlog::error("NetSolverTerminationState - upload: invalid tag received");
      return metaconst::ERR_GENERIC_ERROR;
    }
  }
  catch(...)
  {
    spdlog::error("NetSolverTerminationState - upload: no tag received");
    return metaconst::ERR_GENERIC_ERROR;
  }

  // This packet was deserialized as a std::string packet containing
  // the bytes of the serialized original message.
  // Therefore, the packet must be first deserialized into a proper
  // NetworkPacket
  if (packet->networkPacket->packetType !=
          optnet::BaseNetworkPacket::BaseNetworkPacketType::BNPT_META_BB_PACKET)
  {
    spdlog::error("NetSolverTerminationState - upload: invalid network packet type received");
    return metaconst::ERR_GENERIC_ERROR;
  }

  auto networkPacket = optnet::castBaseToNetworkPacket<std::string>(
          std::move(packet->networkPacket));
  if (!networkPacket)
  {
    spdlog::error("NetSolverTerminationState - receive: "
            "error downcasting to NetworkPacket<std::string>");
    return metaconst::ERR_GENERIC_ERROR;
  }

  if (networkPacket->size() != 1)
  {
    spdlog::error("NetSolverTerminationState - receive: wrong packet data size " +
                  std::to_string(networkPacket->size()));
    return metaconst::ERR_GENERIC_ERROR;
  }

  // Get the calculation state proto packet and upload its content into this node
  SolverTerminationStateProtoPacket solverTerminationStateProto;
  solverTerminationStateProto.ParseFromString(networkPacket->data.at(0));
  pInterrupted = static_cast<solver::InterruptionPoint>(solverTerminationStateProto.interrupted());
  pRank = solverTerminationStateProto.rank();
  pTotalNumNodesSolved = solverTerminationStateProto.totalnumsolved();
  pMinNumNodesSolved = solverTerminationStateProto.minnumsolved();
  pMaxNumNodesSolved = solverTerminationStateProto.maxnumsolved();
  pTotalNumNodesSent = solverTerminationStateProto.totalnumsent();
  pTotalNumImprovedIncumbent = solverTerminationStateProto.totalnumimprovedincumbent();
  pNumNodesReceived = solverTerminationStateProto.numnodesreceived();
  pNumNodesSolved = solverTerminationStateProto.numnodessolved();
  pNumNodesSolvedAtRoot = solverTerminationStateProto.numnodessolvedatroot();
  pNumNodesSolvedAtPreCheck = solverTerminationStateProto.numnodessolvedatprecheck();
  pNumTransferredLocalCutsFromSolver =
          solverTerminationStateProto.numtransferredlocalcutsfromsolver();
  pMinTransferredLocalCutsFromSolver =
          solverTerminationStateProto.minnumtransferredlocalcutsfromsolver();
  pMaxTransferredLocalCutsFromSolver =
          solverTerminationStateProto.maxnumtransferredlocalcutsfromsolver();
  pNumTotalRestarts = solverTerminationStateProto.numtotalrestarts();
  pMinNumRestarts = solverTerminationStateProto.minnumrestarts();
  pMaxNumRestarts = solverTerminationStateProto.maxnumrestarts();
  pNumVarBoundsTightened = solverTerminationStateProto.numvartightened();
  pNumIntVarBoundsTightened = solverTerminationStateProto.numintvartightenedint();
  pRunningTimeMsec = solverTerminationStateProto.runningtimemsec();
  pIdleTimeToFirstNodeMsec = solverTerminationStateProto.idletimetofirstnodemsec();
  pIdleTimeBetweenNodesMsec = solverTerminationStateProto.idletimebetweennodesmsec();
  pIdleTimeAfterLastNodesMsec = solverTerminationStateProto.idletimeafterlastnodemsec();
  pIdleTimeToWaitForNotificationIdMsec =
          solverTerminationStateProto.idletimetowaitnotificationidmsec();
  pIdleTimeToWaitForAckCompletionMsec =
          solverTerminationStateProto.idletimetowaitackcompletionmsec();
  pIdleTimeToWaitForTokenMsec = solverTerminationStateProto.idletimetowaittokenmsec();
  pTotalRootNodeTimeMsec = solverTerminationStateProto.totalrootnodetimemsec();
  pMinRootNodeTimeMsec = solverTerminationStateProto.minrootnodetimemsec();
  pMaxRootNodeTimeMsec = solverTerminationStateProto.maxrootnodetimemsec();

  return metaconst::ERR_NO_ERROR;
}  // upload

optnet::Packet::UPtr NetSolverTerminationState::buildNetworkPacket()
{
  // Create the optimizer node packet to send to the destination
  optnet::Packet::UPtr packet(new optnet::Packet());

  // Add the content as protobuf message
  SolverTerminationStateProtoPacket solverTerminationStateProto;
  solverTerminationStateProto.set_interrupted(static_cast<int>(pInterrupted));
  solverTerminationStateProto.set_rank(pRank);
  solverTerminationStateProto.set_totalnumsolved(pTotalNumNodesSolved);
  solverTerminationStateProto.set_minnumsolved(pMinNumNodesSolved);
  solverTerminationStateProto.set_maxnumsolved(pMaxNumNodesSolved);
  solverTerminationStateProto.set_totalnumsent(pTotalNumNodesSent);
  solverTerminationStateProto.set_totalnumimprovedincumbent(pTotalNumImprovedIncumbent);
  solverTerminationStateProto.set_numnodesreceived(pNumNodesReceived);
  solverTerminationStateProto.set_numnodessolved(pNumNodesSolved);
  solverTerminationStateProto.set_numnodessolvedatroot(pNumNodesSolvedAtRoot);
  solverTerminationStateProto.set_numnodessolvedatprecheck(pNumNodesSolvedAtPreCheck);
  solverTerminationStateProto.set_numtransferredlocalcutsfromsolver(
          pNumTransferredLocalCutsFromSolver);
  solverTerminationStateProto.set_minnumtransferredlocalcutsfromsolver(
          pMinTransferredLocalCutsFromSolver);
  solverTerminationStateProto.set_maxnumtransferredlocalcutsfromsolver(
          pMaxTransferredLocalCutsFromSolver);
  solverTerminationStateProto.set_numtotalrestarts(pNumTotalRestarts);
  solverTerminationStateProto.set_minnumrestarts(pMinNumRestarts);
  solverTerminationStateProto.set_maxnumrestarts(pMaxNumRestarts);
  solverTerminationStateProto.set_numvartightened(pNumVarBoundsTightened);
  solverTerminationStateProto.set_numintvartightenedint(pNumIntVarBoundsTightened);
  solverTerminationStateProto.set_runningtimemsec(pRunningTimeMsec);
  solverTerminationStateProto.set_idletimetofirstnodemsec(pIdleTimeToFirstNodeMsec);
  solverTerminationStateProto.set_idletimebetweennodesmsec(pIdleTimeBetweenNodesMsec);
  solverTerminationStateProto.set_idletimeafterlastnodemsec(pIdleTimeAfterLastNodesMsec);
  solverTerminationStateProto.set_idletimetowaitnotificationidmsec(
          pIdleTimeToWaitForNotificationIdMsec);
  solverTerminationStateProto.set_idletimetowaitackcompletionmsec(
          pIdleTimeToWaitForAckCompletionMsec);
  solverTerminationStateProto.set_idletimetowaittokenmsec(pIdleTimeToWaitForTokenMsec);
  solverTerminationStateProto.set_totalrootnodetimemsec(pTotalRootNodeTimeMsec);
  solverTerminationStateProto.set_minrootnodetimemsec(pMinRootNodeTimeMsec);
  solverTerminationStateProto.set_maxrootnodetimemsec(pMaxRootNodeTimeMsec);

  // Set the protobuf message as new network packet
  packet->networkPacket = optnet::BaseNetworkPacket::UPtr(
      new optnet::NetworkPacket<SolverTerminationStateProtoPacket>(solverTerminationStateProto));

  return std::move(packet);
}  // buildNetworkPacket

}  // namespace metabb

template<> bool optnet::NetworkPacket<metabb::SolverTerminationStateProtoPacket>::operator==(
    const NetworkPacket<metabb::SolverTerminationStateProtoPacket>& other) const noexcept
{
  if (this->data.size() != other.data.empty()) return false;
  return google::protobuf::util::MessageDifferencer::Equals(this->data[0], other.data[0]);
}

template<>
std::pair<optnet::BaseNetworkPacket::BaseNetworkPacketType, std::string>
optnet::NetworkPacket<metabb::SolverTerminationStateProtoPacket>::serialize() const noexcept
{
  if (data.size() != 1)
  {
    spdlog::error("NetworkPacket<SolverTerminationStateProtoPacket> - invalid data size");
    return {BaseNetworkPacketType::BNPT_UNSPECIFIED_PACKET, ""};
  }

  try
  {
    return {BaseNetworkPacketType::BNPT_META_BB_PACKET, data.at(0).SerializeAsString()};
  }
  catch(...)
  {
    spdlog::error("NetworkPacket<SolverTerminationStateProtoPacket> - "
            "undefined error when serializing protobuf");
    return {BaseNetworkPacketType::BNPT_UNSPECIFIED_PACKET, ""};
  }
}  // serialize

}  // namespace optilab
