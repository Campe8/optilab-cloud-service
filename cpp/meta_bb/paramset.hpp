//
// Copyright OptiLab 2019-2020. All rights reserved.
//
// Class grouping all the parameters used by the
// branch and bound framework to perform distributed
// branch and bound.
// @note Parameters are encoded as a protobuf message.
//

#pragma once

#include <cstddef>  // for std::size_t
#include <memory>   // for std::shared_ptr

#include "meta_bb/framework.hpp"
#include "optilab_protobuf/meta_bb_paramset.pb.h"
#include "system/system_export_defs.hpp"

// Constants related to parameter settings
namespace optilab {
namespace metabb {
namespace paramset {
extern const int kPresolveOn;
extern const int kPresolveOff;
extern const int kIncrementalityOn;
extern const int kIncrementalityOff;
extern const int kLPAlgorithmDual;
extern const int kLPAlgorithmPrimal;
extern const int kLPAlgorithmBarrier;
}  // namespace paramset
}  // namespace metabb
}  // namespace optilab

namespace optilab {
namespace metabb {

class SYS_EXPORT_CLASS ParamSet {
 public:
  using SPtr = std::shared_ptr<ParamSet>;

 public:
  ParamSet();
  explicit ParamSet(const SolverProtoParamSet& solverParamSet);
  explicit ParamSet(const ProtoParamSetDescriptor& descriptorParamSet);
  ParamSet(const SolverProtoParamSet& solverParamSet,
           const ProtoParamSetDescriptor& descriptorParamSet);

  virtual ~ParamSet() = default;

  /// Resets the solver set of parameters,
  /// i.e., SolverProtoParamSet, to their default values
  void resetSolverParamset();

  /// Resets the metabb set of parameters,
  /// i.e., ProtoParamSetDescriptor to their default values
  void resetDescriptorParamset();

  /// Returns the set of parameters used by MP solvers.
  /// This method is mostly used by the MPSolverInterface and solver interfaces,
  /// e.g., SCIP, Cplex, etc., to set the solver's specific parameters
  inline const SolverProtoParamSet& getSolverParamset() const noexcept { return pSolverParamSet; }
  inline SolverProtoParamSet& getSolverParamset() { return pSolverParamSet; }

  /// Returns the set of parameters used by the meta branch and bound architecture
  inline const ProtoParamSetDescriptor& getDescriptor() const noexcept
  {
    return pParamSetDescriptor;
  }
  inline ProtoParamSetDescriptor& getDescriptor() { return pParamSetDescriptor; }

  /// Broadcasts this paramset from the given root to all other nodes in the network.
  /// Returns zero on success, non-zero otherwise.
  /// @note default implementation returns no-error on no-op. This is done to allow
  /// local solvers that don't use distributed computing to still use ParamSet.
  /// @note if "sendSolverProtoParamSet" is set to true (default),
  /// this method will broadcast ONLY the SolverProtoParamSet information needed for
  /// distributed computation. If the flag is false, it will broadcast the
  /// ProtoParamSetDescriptor protobuf message
  virtual int broadcast(const Framework::SPtr& framework, int root,
                        bool sendSolverProtoParamSet=true) noexcept { return 0; }

 protected:
  SolverProtoParamSet pSolverParamSet;
  ProtoParamSetDescriptor pParamSetDescriptor;
};

}  // namespace metabb
}  // namespace optilab
