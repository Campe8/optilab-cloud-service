#include "meta_bb/paramset.hpp"

#include <stdexcept>  // for std::invalid_argument

#include "meta_bb/meta_bb_constants.hpp"

namespace optilab {
namespace metabb {
namespace paramset {
const int kPresolveOn = 1;
const int kPresolveOff = 0;
const int kIncrementalityOn = 1;
const int kIncrementalityOff = 0;
const int kLPAlgorithmDual = 1;
const int kLPAlgorithmPrimal = 2;
const int kLPAlgorithmBarrier = 3;
}  // namespace paramset
}  // namespace metabb
}  // namespace optilab

namespace optilab {
namespace  metabb {

ParamSet::ParamSet()
{
  // Reset the solver set of parameters paramse
  resetSolverParamset();

  // Reset the meta branch and bound set of parameters
  resetDescriptorParamset();
}  // ParamSet

ParamSet::ParamSet(const SolverProtoParamSet& solverParamSet)
{
  pSolverParamSet.CopyFrom(solverParamSet);

  // Reset the meta branch and bound set of parameters
  resetDescriptorParamset();
}

ParamSet::ParamSet(const ProtoParamSetDescriptor& descriptorParamSet)
{
  // Reset the solver set of parameters paramse
  resetSolverParamset();

  pParamSetDescriptor.CopyFrom(descriptorParamSet);
}

ParamSet::ParamSet(const SolverProtoParamSet& solverParamSet,
                   const ProtoParamSetDescriptor& descriptorParamSet)
{
  pSolverParamSet.CopyFrom(solverParamSet);
  pParamSetDescriptor.CopyFrom(descriptorParamSet);
}

void ParamSet::resetSolverParamset()
{
  pSolverParamSet.set_relativemipgapvalue(solverparamset::DEFAULT_RELATIVE_MIP_GAP);
  pSolverParamSet.set_primaltolerancevalue(solverparamset::DEFAULT_PRIMAL_TOLERANCE);
  pSolverParamSet.set_dualtolerancevalue(solverparamset::DEFAULT_DUAL_TOLERANCE);
  pSolverParamSet.set_presolvevalue(paramset::kPresolveOn);
  pSolverParamSet.set_scalingvalue(solverparamset::DEFAULT_INTEGER_PARAM_VALUE);
  pSolverParamSet.set_lpalgorithmvalue(solverparamset::DEFAULT_INTEGER_PARAM_VALUE);
  pSolverParamSet.set_incrementalityvalue(paramset::kIncrementalityOn);
  pSolverParamSet.set_lpalgorithmisdefault(true);
}  // resetSolverParamset

void ParamSet::resetDescriptorParamset()
{
  pParamSetDescriptor.set_rootnodesolvabilitycheck(false);
  pParamSetDescriptor.set_userootnodecuts(false);
  pParamSetDescriptor.set_transferlocalcuts(true);
  pParamSetDescriptor.set_transferconflictscuts(false);
  pParamSetDescriptor.set_collectonce(true);
  pParamSetDescriptor.set_distributebestprimalsolution(true);
  pParamSetDescriptor.set_racingstatbranching(true);
  pParamSetDescriptor.set_nopreprocessinginlm(false);
  pParamSetDescriptor.set_noupperboundtransferinracing(false);
  pParamSetDescriptor.set_mergenodesatrestart(true);
  pParamSetDescriptor.set_deterministic(false);
  pParamSetDescriptor.set_allboundchangestransfer(true);
  pParamSetDescriptor.set_restartracing(false);
  pParamSetDescriptor.set_checkfeasibilityinlm(false);
  pParamSetDescriptor.set_distributedlocalbranching(false);
  pParamSetDescriptor.set_communicatetighterboundsinracing(true);
  pParamSetDescriptor.set_keepracinguntiltofindfirstsolution(false);
  pParamSetDescriptor.set_rampupphaseprocess(2);
  pParamSetDescriptor.set_numnodestoswitchtocollectingmode(1);
  pParamSetDescriptor.set_nodetransfermode(1);
  pParamSetDescriptor.set_notificationsynchronization(0);
  pParamSetDescriptor.set_minnumofcollectingmodesolvers(1);
  pParamSetDescriptor.set_maxnumofcollectingmodesolvers(-1);
  pParamSetDescriptor.set_solverorderincollectingmode(0);
  pParamSetDescriptor.set_stopracingnumnodesleft(300);
  pParamSetDescriptor.set_racingrampupterminationcriteria(4);
  pParamSetDescriptor.set_numnodeskeepinginrootsolver(0);
  pParamSetDescriptor.set_numinitialnodes(300);
  pParamSetDescriptor.set_maxnracingparamsetseed(64);
  pParamSetDescriptor.set_trynumvariableorderinracing(100);
  pParamSetDescriptor.set_trynumbranchingorderinracing(100);
  pParamSetDescriptor.set_numevaluationsolverstostopracing(-1);
  pParamSetDescriptor.set_nummaxcanditatesforcollecting(10);
  pParamSetDescriptor.set_numtransferlimitforbreaking(100);
  pParamSetDescriptor.set_numstopsolvingmode(3);
  pParamSetDescriptor.set_aggressivepresolvestopdepth(8);
  pParamSetDescriptor.set_finalcheckpointnumsolvers(10);
  pParamSetDescriptor.set_nummaxracingbaseparameters(0);
  pParamSetDescriptor.set_multiplierforcollectingmode(2.0);
  pParamSetDescriptor.set_multipliertodeterminethresholdvalue(1.5);
  pParamSetDescriptor.set_bgapcollectingmode(0.4);
  pParamSetDescriptor.set_multiplierforbgapcollectingmode(10.0);
  pParamSetDescriptor.set_abgapforswitchingtobestsolver(1.0);
  pParamSetDescriptor.set_bgapstopsolvingmode(0.33);
  pParamSetDescriptor.set_notificationintervalmsec(1000.0);
  pParamSetDescriptor.set_timelimitmsec(-1.0);
  pParamSetDescriptor.set_checkpointintervalmsec(3600000.0);
  pParamSetDescriptor.set_stopracingtimelimitmsec(720000.0);
  pParamSetDescriptor.set_timetoincreasecmsmsec(10000.0);
  pParamSetDescriptor.set_ratiotoapplylightweightrootprocess(0.5);
  pParamSetDescriptor.set_multiplierforbreakingtargetbound(1.03);
  pParamSetDescriptor.set_fixedvariablesratioinmerging(0.9);
  pParamSetDescriptor.set_countingsolverratioinracing(0.5);
  pParamSetDescriptor.set_randomnodeselectionratio(0.2);
  pParamSetDescriptor.set_dualboundgainbranchratio(0.5);
  pParamSetDescriptor.set_collectingmodeinteravalmsec(10000.0);
  pParamSetDescriptor.set_restartinrampdownthresholdtimemsec(-1.0);
  pParamSetDescriptor.set_restartinrampdownactivesolverratio(0.7);
  pParamSetDescriptor.set_timestopsolvingmodemsec(-1.0);
  pParamSetDescriptor.set_numsolvernodesstartbreaking(0);
  pParamSetDescriptor.set_numstopbreaking(0);
  pParamSetDescriptor.set_numboundchangesofmergenodes(-1);
  pParamSetDescriptor.set_stopracingnumnodesleftmult(3.0);
  pParamSetDescriptor.set_numcollectonce(-1);
  pParamSetDescriptor.set_numidlesolverstoterminate(-1);
  pParamSetDescriptor.set_dualboundgaintest(false);
  pParamSetDescriptor.set_initialnodesgeneration(false);
  pParamSetDescriptor.set_quiet(true);
  pParamSetDescriptor.set_logsolvingstatus(false);
  pParamSetDescriptor.set_transferbranchstats(false);
  pParamSetDescriptor.set_setalldefaultsafterracing(false);
  pParamSetDescriptor.set_lightweightrootnodeprocess(false);
  pParamSetDescriptor.set_nosolverpresolvingatroot(false);
  pParamSetDescriptor.set_noaggressiveseparatorinracing(true);
  pParamSetDescriptor.set_noallboundchangestransferinracing(false);
  pParamSetDescriptor.set_breakfirstsubtree(false);
  pParamSetDescriptor.set_checkgapinlm(false);
  pParamSetDescriptor.set_cleanup(true);
  pParamSetDescriptor.set_generatereducedcheckpointfiles(false);
  pParamSetDescriptor.set_outputpresolvedinstance(false);
  pParamSetDescriptor.set_outputparams(4);
  pParamSetDescriptor.set_aggressivepresolvedepth(-1);
  pParamSetDescriptor.set_instancetransfermethod(2);
  pParamSetDescriptor.set_noalternatesolving(100);
  pParamSetDescriptor.set_stopracingtimelimitmultipliermsec(50);
  pParamSetDescriptor.set_stopracingnumberofnodesleftmultiplier(3.0);
  pParamSetDescriptor.set_hugeimbalanceactivesolverratio(0.9);
  pParamSetDescriptor.set_optimizationsolverpackagetype(0);
}  // resetDescriptorParamset

}  // namespace metabb
}  // namespace optilab
