//
// Copyright OptiLab 2019-2020. All rights reserved.
//
// Base class encapsulating the parameter set for racing ramp-up.
//

#pragma once

#include <cstdint>  // for uint64_t
#include <limits>   // for std::numeric_limits
#include <memory>   // for std::shared_ptr

#include "meta_bb/framework.hpp"
#include "optimizer_network/packet.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace  metabb {

class SYS_EXPORT_CLASS RacingRampUpParamSet {
 public:
  enum class RacingTerminationCriteria : int {
    RTC_WITH_NODES_LEFT = 0,
    RTC_WITH_TIME_LIMIT,
    RTC_UNDEF,
  };

  using SPtr = std::shared_ptr<RacingRampUpParamSet>;

 public:
  RacingRampUpParamSet() = default;

  /// Constructor:
  /// - termCriteria: termination criteria for racing ramp-up
  /// - numNodesLeft: number of nodes left used for termination.
  ///   If a negative number if provided, the number of nodes won't be used as
  ///   termination criteria
  /// - timeoutMsec: timeout in msec. used for termination. By default it is set to +inf.
  RacingRampUpParamSet(RacingTerminationCriteria termCriteria, int numNodesLeft,
                       uint64_t timeoutMsec=std::numeric_limits<uint64_t>::max())
  : pTerminationCriteria(termCriteria),
    pNumNodesLeft(numNodesLeft),
    pTimeLimitMsec(timeoutMsec)
  {
  }

  virtual ~RacingRampUpParamSet() = default;

  /// Returns the termination criteria for the racing ramp-up
  inline RacingTerminationCriteria getRacingTerminationCriteria() const noexcept
  {
    return pTerminationCriteria;
  }

  /// Returns the number of nodes used as termination criteria
  inline int getStopRacingNumNodesLeft() const noexcept { return pNumNodesLeft; }

  /// Returns the timeout in msec. used as termination criteria
  inline uint64_t getStopRacingTimeoutMsec() const noexcept { return pTimeLimitMsec; }

  /// Sets the id of the winner network node
  virtual void setWinnerRank(int rank) noexcept { (void)rank; }

  /// Sends this racing ramp-up parameter set to the given destination
  virtual int send(Framework::SPtr framework, int destination) = 0;

  /// Uploads a racing ramp-up parameter set.
  virtual int upload(optnet::Packet::UPtr packet) = 0;

  /// Returns the strategy for racing-rampup
  virtual int getStrategy() = 0;

 protected:
  /// Criteria for racing ramp-up termination
  RacingTerminationCriteria pTerminationCriteria{RacingTerminationCriteria::RTC_UNDEF};

  /// Stop racing number of nodes limits
  int pNumNodesLeft{-1};

  /// Stop racing timeout in msec.
  uint64_t pTimeLimitMsec{std::numeric_limits<uint64_t>::max()};
};

}  // namespace metabb
}  // namespace optilab
