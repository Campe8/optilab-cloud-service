//
// Copyright OptiLab 2019-2020. All rights reserved.
//
// Pool of solvers.
//

#pragma once

#include <cassert>
#include <cmath>    // for std::fabs
#include <cstddef>  // for std::size_t
#include <cstdint>  // for uint64_t
#include <map>
#include <memory>   // for std::shared_ptr
#include <vector>

#include <sparsepp/spp.h>

#include "meta_bb/framework.hpp"
#include "meta_bb/meta_bb_constants.hpp"
#include "meta_bb/node.hpp"
#include "meta_bb/node_pool.hpp"
#include "meta_bb/paramset.hpp"
#include "meta_bb/solver_termination_state.hpp"
#include "system/system_export_defs.hpp"
#include "utilities/timer.hpp"

#define SOLVER_POOL_INDEX(rank) (rank - pOriginRank)

/// Forward declarations
namespace optilab {
namespace metabb {
class RacingSolverPool;
}  // metabb
}  // optilab

namespace optilab {
namespace metabb {

/**
 * Element of a solver pool.
 * An element of a solver pool is a wrapper around a solver,
 * together with its solving node.
 */
class SYS_EXPORT_CLASS SolverPoolElement {
 public:
  using SPtr = std::shared_ptr<SolverPoolElement>;

 public:
  SolverPoolElement(int rank)
 : pRank(rank)
 {
 }

  ~SolverPoolElement()
  {
    if (pCurrentNode) pCurrentNode.reset();
    if (pTermState) pTermState.reset();
  }

  /// Activates this solver pool element on the given node
  void activate(Node::SPtr node);

  /// Activates this solver for racing
  void racingActivate();

  /// Deactivates this solver
  void deactivate();

  /// Kills this solver and returns the current node
  Node::SPtr kill();

  /// Returns this solver's rank
  inline int getRank() const noexcept { return pRank; }

  /// Returns the current node
  inline Node::SPtr getCurrentNode() const noexcept { return pCurrentNode; }

  /// Returns the current node.
  /// @note this method resets the pointer to the current node
  inline Node::SPtr extractCurrentNode() noexcept
  {
    auto copy = pCurrentNode;
    pCurrentNode.reset();
    return copy;
  }

  /// Sets the selection heap element
  inline void setSelectionHeapElement(int selectionHeapElement)
  {
    pSelectionHeapElement = selectionHeapElement;
  }

  /// Returns the selection heap element
  inline int getSelectionHeapElement() const noexcept  { return pSelectionHeapElement; }

  /// Sets the collecting mode solver heap element
  inline void setCollectingModeSolverHeapElement(int collectingHeapElement)
  {
    pCollectingModeSolverHeapElement = collectingHeapElement;
  }

  /// Returns the collecting mode solver heap element
  inline int getCollectingModeSolverHeapElement() const noexcept
  {
    return pCollectingModeSolverHeapElement;
  }

  /// Returns the number of nodes solved by this solver
  inline long long getNumOfNodesSolved() const noexcept { return pNumOfNodesSolved; }

  /// Returns the number of nodes left
  inline int getNumOfNodesLeft() const noexcept { return pNumOfNodesLeft; }

  /// Sets the number of nodes left
  inline void setNumOfNodesLeft(int numOfNodesLeft) { pNumOfNodesLeft = numOfNodesLeft; }

  /// Sets the number of nodes solved
  inline void setNumOfNodesSolved(long long numOfNodesSolved)
  {
    pNumOfNodesSolved = numOfNodesSolved;
  }

  /// Returns the number of nodes left difference between current number
  /// and that in the previous notification time
  inline int getNumOfDiffNodesSolved() const noexcept {  return pNumOfDiffNodesSolved; }

  /// Sets the number of nodes left difference between current number and that
  /// in the previous notification time
  inline void setNumOfDiffNodesSolved(int numOfDiff) { pNumOfDiffNodesSolved = numOfDiff; }

  /// Sets the dual bound value on the curent node.
  /// Throws std::runtime_error if there is no curren node
  void setDualBoundValue(double dualBoundValue);

  /// Returns the number of nodes left difference between current number
  /// and that in the previous notification time
  inline int getNumOfDiffNodesLeft() const noexcept { return pNumOfDiffNodesLeft; }

  /// Sets the nunber of nodes left difference between current number
  /// and that in the previous notification time
  inline void setNumOfDiffNodesLeft(int numOfDiff)
  {
    pNumOfDiffNodesLeft = numOfDiff;
  }

  /// Returns the best dual bound value
  inline double getBestDualBoundValue() const noexcept { return pBestDualBoundValue; }

  /// Sets the best dual bound value
  inline void setBestDualBoundValue(double bestDualBoundValue) noexcept
  {
    pBestDualBoundValue = bestDualBoundValue;
  }

  /// Returns this solver's status
  inline solver::SolverStatus getStatus() const noexcept { return pSolverStatus; }

  /// Returns true if this solver is active, false otherwise
  inline bool isActive() const noexcept
  {
     return pSolverStatus == solver::SolverStatus::SS_ACTIVE;
  }

  /// Returns true if this solver is out of collecting mode,
  /// false otherwise
  inline bool isOutCollectingMode() const noexcept { return !pCollectingMode; }

  /// Returns true if the solver is in collecting mode,
  /// returns false otherwise
  inline bool isInCollectingMode() const noexcept { return pCollectingMode; }

    /** set collecting mode */
  inline void setCollectingMode(bool collectingMode) noexcept
  {
    pCollectingMode = collectingMode;
  }

  /// Returns true if the solver is candidate of collecting mode solver,
  /// returns false otherwise
  inline bool isCandidateOfCollecting() const noexcept { return pCandidateOfCollecting; }

  /// Sets this solver as candidate of collecting mode solver
  inline void setCandidateOfCollecting(bool collectingMode) noexcept
  {
    pCandidateOfCollecting = collectingMode;
  }

  /// Sets termination state
  inline void setTermState(SolverTerminationState::SPtr termState)
  {
       if (pTermState) pTermState.reset();
       pTermState = termState;
  }

  /// Returns the termination state
  inline SolverTerminationState::SPtr getTermState() const noexcept { return pTermState; }

  /// Switches the solver into evaluation stage
  inline void switchIntoEvaluation()
  {
    assert(pSolverStatus == solver::SolverStatus::SS_RACING);
    pSolverStatus = solver::SolverStatus::SS_RACING_EVALUATION;
  }

  /// Switches the solver out of evaluation stage
  inline void switchOutEvaluation()
  {
    assert(pSolverStatus == solver::SolverStatus::SS_RACING_EVALUATION);
    pSolverStatus = solver::SolverStatus::SS_RACING;
  }

  /// Returns true if this solver is in racing stage,
  /// returns false otherwise
  inline bool isRacingStage() const noexcept
  {
    return pSolverStatus == solver::SolverStatus::SS_RACING;
  }

  /// Returns true if this solver is in evaluation stage,
  /// returns false otherwise
  inline bool isEvaluationStage() const noexcept
  {
    return pSolverStatus == solver::SolverStatus::SS_RACING_EVALUATION;
  }

  /// Makes this solver No generator
  inline void setNoGenerator() { pGenerator = false; }

  /// Returns true if this is a generator solver,
  /// returns false otherwise
  inline bool isGenerator() const noexcept { return pGenerator; }

  /// Prohibits to be in collecting mode
  inline void prohibitCollectingMode() noexcept { pCollectingIsProhibited = true; }

  /// Allows to be in collecting mode
  inline void setCollectingIsAllowed() noexcept { pCollectingIsProhibited = false; }

  /// Checks if this solver cannot be allowed in collecting mode
  inline bool isCollectingProhibited() const noexcept { return pCollectingIsProhibited; }

  /// Check if dual bound gain is testing in this solver or not
  inline bool isDualBoundGainTesting() const noexcept { return pDualBoundGainTesting; }

  /// Sets the dual bound gain testing value
  inline void setDualBoundGainTesting(bool value) noexcept { pDualBoundGainTesting = value; }

 private:
  /// Rank of the solver.
  /// This corresponds to the rank of the network node
  /// running this solver
  int pRank;

  /// Status of the solver
  solver::SolverStatus pSolverStatus{solver::SolverStatus::SS_INACTIVE};

  /// Indicates whether or not the current solver is in collecting mode
  bool pCollectingMode{false};

  /// Indicates that this solver is a candidate for collecting mode solver
  bool pCandidateOfCollecting{false};

  /// Indicate whether or not this solver can generate sub-problems
  bool pGenerator{false};

  /// Indicates that collecting is temporary prohibited
  bool pCollectingIsProhibited{false};

  /// Indicates that dual bound gain is testing
  bool pDualBoundGainTesting{false};

  /// Current node being solver by the solver
  Node::SPtr pCurrentNode;

  /// Index in the selection heap
  int pSelectionHeapElement{0};

  /// Index in the collecting mode heap element
  int pCollectingModeSolverHeapElement{0};

  /// Number of nodes solved by the solver.
  /// @note -1 is the special value which means that the solver
  /// was never updated in racing
  long long pNumOfNodesSolved{0};

  /// Number of nodes solved difference between the current
  /// number and that in the previous notification time
  int pNumOfDiffNodesSolved{0};

  /// Number of nodes left
  int pNumOfNodesLeft{0};

  /// Difference between current number of nodes left and previous
  /// notification time
  int pNumOfDiffNodesLeft{0};

  /// Best dual bound value found so far
  double pBestDualBoundValue{0.0};

  /// Solver's termination state
  SolverTerminationState::SPtr pTermState;
};

/**
 * Solver selection heap.
 * Heap used for solver's selection.
 */
class SYS_EXPORT_CLASS SelectionHeap {
public:
  enum ResultOfInsert
  {
    ROI_SUCCEEDED,
    ROI_FAILED_BY_FULL
  };

  using SPtr = std::shared_ptr<SelectionHeap>;

public:
  /// Constructor:
  /// - size: size of the heap in number of elements
  /// - isCollectingHeap: boolean  flag indicating whether or not this is a
  ///   heap used for collecting mode solvers
  SelectionHeap(int size, bool isCollectingHeap = false);
  virtual ~SelectionHeap() = default;

  /// Inserts a new solver (pool element) into the heap
  ResultOfInsert insert(SolverPoolElement::SPtr solver);

  /// Returns the top element of the heap
  SolverPoolElement::SPtr top() const noexcept;

  /// Removes top priority solver from the heap
  SolverPoolElement::SPtr remove();

  /// Resizes the heap
  void resize(int size);

  /// Get current used heap size
  inline int getHeapSize() const noexcept { return pHeapSize; }

  /// Get max heap size
  inline int getMaxHeapSize() const noexcept { return pMaxHeapSize; }

  /// "Up" method for the heap,
  /// i.e., up-heap the element at given position
  virtual void upHeap(int pos);

  /// "Down" method for the heap,
  /// i.e., down-heap the element at given position
  virtual void downHeap(int pos);

  /// Updates selection heap by a new dual bound value of the solver
  virtual void updateDualBoundValue(SolverPoolElement::SPtr solver, double val) = 0;

  /// Deletes the given solver from the selection heap
  virtual void deleteElement(SolverPoolElement::SPtr solver) = 0;

protected:
  /// "Up" method for the heap,
  /// i.e., up-heap the element at given position
  virtual void upHeapImpl(int pos, bool isCollectingHeap) = 0;

  /// "Down" method for the heap,
  /// i.e., down-heap the element at given position
  virtual void downHeapImpl(int pos, bool isCollectingHeap) = 0;

  /// Boolean flag indicating whether or not this heap contains
  /// collecting mode solvers
  bool isCollectingHeap;

  /// Max size of the heap
  int pMaxHeapSize;

  /// Current size of the heap
  int pHeapSize;

  /// Heap data structure implemented as a vector of pointers
  std::vector<SolverPoolElement::SPtr> pHeap;
};

/// Descending heap, i.e., Max Heap for descending order (top is max value)
class SYS_EXPORT_CLASS DescendingSelectionHeap : public SelectionHeap {
public:
   DescendingSelectionHeap(int size, bool isCollectingHeap = false);
   ~DescendingSelectionHeap() override = default;
   void updateDualBoundValue(SolverPoolElement::SPtr solver, double val) override;
   void deleteElement(SolverPoolElement::SPtr solver) override;

protected:
   void upHeapImpl(int pos, bool isCollectingHeap) override;
   void downHeapImpl(int pos, bool isCollectingHeap) override;
};

/// Ascending heap, i.e., Min Heap, for ascending order (top is min value)
class SYS_EXPORT_CLASS AscendingSelectionHeap : public SelectionHeap {
public:
   AscendingSelectionHeap(int size, bool isCollectingHeap = false);
   ~AscendingSelectionHeap() override = default;
   void updateDualBoundValue(SolverPoolElement::SPtr solver, double val) override;
   void deleteElement(SolverPoolElement::SPtr solver) override;

protected:
   void upHeapImpl(int pos, bool isCollectingHeap) override;
   void downHeapImpl(int pos, bool isCollectingHeap) override;
};

/**
 * Collecting mode solver heap.
 * Heap used for solver's selection in collecting mode.
 *
 * @note this is a MAX heap.
 */
class SYS_EXPORT_CLASS CollectingModeSolverHeap : public DescendingSelectionHeap {
public:
  CollectingModeSolverHeap(int size)
  : DescendingSelectionHeap(size, true) {}
  ~CollectingModeSolverHeap() = default;
};

/**
 * Collecting mode descending solver heap.
 * Heap used for solver's selection in collecting mode.
 *
 * @note this is a MAX heap.
 */
using DescendingCollectingModeSolverHeap = CollectingModeSolverHeap;

/**
 * Collecting mode ascending solver heap.
 * Heap used for solver's selection in collecting mode.
 *
 * @note this is a MIN heap.
 */
class SYS_EXPORT_CLASS AscendingCollectingModeSolverHeap : public AscendingSelectionHeap {
public:
  AscendingCollectingModeSolverHeap(int size)
  : AscendingSelectionHeap(size, true) {}
  ~AscendingCollectingModeSolverHeap() = default;
};

/**
 * Useful renaming with standard names.
 */
using CollectingModeSolverMaxHeap = DescendingCollectingModeSolverHeap;
using CollectingModeSolverMinHeap = AscendingCollectingModeSolverHeap;

class SYS_EXPORT_CLASS SolverPool {
 public:
  using SPtr = std::shared_ptr<SolverPool>;

 public:
  /// Constructor:
  /// - MP: multiplier for the threshold value p.
  ///   When the number of good nodes becomes greater than
  ///   MP * NumNodesToSwitchToCollectingMode, stop collecting mode
  /// - BGap: threshold value of gap for good nodes.
  ///   Bound gap "BGap" to identify good nodes. A node is good when:
  ///   (bound_value - best_bound_value) / best_bound_value < "BGap".
  /// - MBGap: multiplier of the bGap (good nodes) value.
  ///   A solver in collecting mode gets out of collecting mode when
  ///   (best_dual_bound - global_best_dual_bound) / global_best_dual_bound > multBGap * bGap
  /// - origRank: origin rank of solvers managed by this solver pool.
  /// - framework: framework used in the branch and bound system.
  /// - paramSet: parameter set instance.
  /// - timer: timer used in the pool.
  /// @note throws std::invalid_argument on empty pointers
  SolverPool(double MP,
             double BGap,
             double MBGap,
             int origRank,
             Framework::SPtr framework,
             ParamSet::SPtr paramSet,
             timer::Timer::SPtr timer = nullptr);

  virtual ~SolverPool()
  {
    for (auto& solver : pPool)
    {
      solver.reset();
    }
  }

  /// Returns true if this pool is active, false otherwise
  inline bool isActive() const noexcept { return pActive; }

  /// Activates this pool
  inline void activate() noexcept { pActive = true; }

  /// Returns the number of solvers
  inline int getNumSolvers() const noexcept { return pNumSolvers; }

  /// Returns the number of nodes in all solvers
  inline uint64_t getNumNodesInAllSolvers() const noexcept { return pNumNodesInSolver; }
  inline uint64_t getNumNodesInSolvers() const noexcept { return getNumNodesInAllSolvers(); }

  /// Returns the number of nodes solved in the current set of
  /// running solvers in the pool
  inline uint64_t getNumNodesSolvedInSolvers() const noexcept
  {
    return pNumNodesSolvedInSolvers;
  }

  /// Adds number of nodes solved
  inline void addNumNodesSolved(uint64_t numOfNodesSolved) noexcept
  {
    pNumNodesSolvedInSolvers += numOfNodesSolved;
  }

  /// Returns the total number of solved nodes so far
  inline uint64_t getTotalNodesSolved() const noexcept { return pNumTotalNodesSolved; }

  /// Adds "num" to the total number of nodes solved so far
  inline void addTotalNodesSolved(uint64_t num) noexcept { pNumTotalNodesSolved += num; }

  /// Returns the number of active solvers
  inline std::size_t getNumActiveSolvers() const noexcept { return pActiveSolversMap.size(); }

  /// Returns the number of inactive solvers
  inline std::size_t getNumInactiveSolvers() const noexcept
  {
    return pInactiveSolversMap.size();
  }

  /// Returns true if the limit on the number of collecting mode solvers can be increased.
  /// Returns false otherwise
  inline bool canIncreaseLimitNumLimitCollectingModeSolvers() const noexcept
  {
     return (pNumLimitCollectingModeSolvers < pNumMaxCollectingModeSolvers);
  }

  /// Returns the limit on the number of solvers that can be in collecting mode
  inline uint64_t getNumLimitCollectingModeSolvers() const noexcept
  {
    return pNumLimitCollectingModeSolvers;
  }

  /// Increases the limit on the number of solvers that can get into collecting mode
  inline void incNumLimitCollectingModeSolvers()
  {
    pNumLimitCollectingModeSolvers = std::min(
        pNumLimitCollectingModeSolvers+1, std::min(pNumMaxCollectingModeSolvers, pNumSolvers));
  }

  /// Returns true if the the pool is in collecting mode.
  /// Returns false otherwise
  inline bool isInCollectingMode() const noexcept { return pCollectingMode; }

  /// Returns true if the given solver is active.
  /// Returns false otherwise
  bool isSolverActive(int rank) const;

  /// Returns true if the specified solver is in collecting mode.
  /// Returns false otherwise
  bool isSolverInCollectingMode(int rank) const;

  /// Returns the node currently being solved by the solver with specified id.
  /// @note throws std::out_of_range exception for invalid ranks
  Node::SPtr getCurrentNode(int rank) const;
  inline Node::SPtr getCurrentNodeFromSolver(int rank) const
  {
    return getCurrentNode(rank);
  }

  /// Extracts the current solving Node by solver rank
  Node::SPtr extractCurrentNodeAndInactivate(int rank, NodePool::SPtr nodePool);
  inline Node::SPtr extractCurrentNodeAndDeactivate(int rank, NodePool::SPtr nodePool)
  {
    return extractCurrentNodeAndInactivate(rank, nodePool);
  }

  /// Returns true if the current solving node of the specified solver
  /// has descendants.
  /// Returns false otherwise
  bool currentSolvingNodeHasDescendant(int rank) const;
  inline bool currentSolvingNodeHasChildren(int rank) const
  {
    return currentSolvingNodeHasDescendant(rank);
  }

  /// Returns the number of nodes solved by the specified solver
  uint64_t getNumNodesSolved(int rank) const;

  /// Returns the number of nodes left to solve in the specified solver
  int getNumOfNodesLeft(int rank);

  /// Returns the number of nodes left in the solver that has the best dual bound value
  inline int getNumOfNodesLeftInBestSolver() const
  {
    return (pSelectionHeap->getHeapSize() > 0) ?
        pSelectionHeap->top()->getNumOfNodesLeft() : 0;
  }

  /// Returns the rank (id) of the best solver.
  /// Returns -1 if no solver is registered
  inline int getBestSolver() const
  {
    return (pSelectionHeap->getHeapSize() > 0) ?
        pSelectionHeap->top()->getRank() : -1;
  }

  /// Returns the dual bound value of the solving node being solved
  /// by the specified rank
  double getDualBoundValue(int rank) const;

  /// Sets the solver termination state on the specified solver
  void setTermState(int rank, SolverTerminationState::SPtr termState);

  /// Returns the termination state of the specified solver
  SolverTerminationState::SPtr getTermState(int rank) const;

  /// Updates the dual bound values of saving nodes to their dual bound values for subtrees
  inline void updateDualBoundsForSavingNodes()
  {
    for (int idx = 1; idx < pFramework->getComm()->getSize(); idx++)
    {
      if (getCurrentNode(idx) && (getCurrentNode(idx)->getParent() == nullptr))
      {
        getCurrentNode(idx)->updateInitialDualBoundToSubtreeDualBound();
      }
    }
  }

  /// Set collecting mode is allowed to the solver specified by rank
  void setCollectingIsAllowed(int rank);

  /// Returns the rank of an inactive solver
  inline int getInactiveSolverRank()
  {
    return (pInactiveSolversMap.begin() == pInactiveSolversMap.end()) ?
        -1 : pInactiveSolversMap.begin()->second->getRank();
  }

  /// Activates the solver with specified rank to run on the given node
  void activateSolver(int rank, Node::SPtr node);

  /// Activates the solver with specified rank to run on the given node and with
  /// given number of nodes left.
  /// @note this method is used to register the activation of the racing winner solver.
  /// In other words, this method does not communicate the solver to run on the node
  /// but simply sets the racing winner solver as active in the internal pool
  void activateSolver(int rank, Node::SPtr node, int numNodesLeft);

  /// Activates any idle solver from the given racing pool to run on the given node.
  /// Returns the rank of the activated solver
  int activateSolver(Node::SPtr node, std::shared_ptr<RacingSolverPool> racingSolverPool,
                     bool isRampUpPhase, int numGoodNodesInNodePool, double avgDualBoundGain);

  /// Deactivates the specified solver.
  /// @note the method receives the number of solved nodes and the pointer to the
  /// node pool to change the solver into collecting mode
  void deactivateSolver(int rank, uint64_t numSolvedNodes, NodePool::SPtr nodePool);

  /// Kills the specified solver
  Node::SPtr solverKill(int rank);

  /// Switches the pool out from collecting mode on all solvers
  void switchOutCollectingMode();

  /// Forces the specified solver to switch out from collecting mode
  void enforcedSwitchOutCollectingMode(int rank);

  /// Sends PPT_OUT_COLLECTING_MODE tag
  /// to the solver with specified rank to swith it out
  /// from the collecting mode
  void sendSwitchOutCollectingModeIfNecessary(int rank);

  /// Returns the best global dual bound value
  virtual double getGlobalBestDualBoundValue() = 0;

  /// Switches the pool into collecting mode
  virtual void switchIntoCollectingMode(NodePool::SPtr nodePool) = 0;


  /// Updates the status of the specified solver with the specified information
  virtual void updateSolverStatus(int rank,
                                  uint64_t numNodesSolved,
                                  int numNodesLeft,
                                  double solverLocalBestBound,
                                  NodePool::SPtr nodePool) = 0;

  /// Returns the Multiplier for collecting mode
  inline int getMCollectingNodes() const noexcept { return pMCollectingNodes; }
  inline int getMMaxCollectingNodes() const noexcept { return pMMaxCollectingNodes; }

  inline uint64_t getSwichOutTime() const noexcept { return pSwitchOutTimeMsec; }
  inline void setSwichOutTime(uint64_t timeMsec) noexcept { pSwitchOutTimeMsec = timeMsec; }

  bool isDualBounGainTesting(int rank) const;

 protected:
  /// Flag indicating whether the pool is active or not
  bool pActive{false};

  /// Threshold value for bound gap
  double pBGap{0.0};

  /// Multiplier of the threshold value p
  double pMP{0.0};

  /// Multiplier for the bgap value
  double pMBGap{0.0};

  /// Allowable absolute dual bound gap to the best solver
  double pAbsoluteGap{0.0};

  /// Origin rank of solvers managed by this solver pool
  int pOriginRank{-1};

  /// Number of solvers in this pool
  int pNumSolvers{0};

  /// Number of generators
  int pNumGenerators{0};

  /// Number of collecting mode solvers in this pool
  int pNumCollectingModeSolvers{0};

  /// Maximum number of solvers that can be in collecting mode
  int pNumMaxCollectingModeSolvers{0};

  /// Limit number of solvers that can be in collecting mode
  int pNumLimitCollectingModeSolvers{0};

  /// Number of nodes solved in the current set of running solvers
  uint64_t pNumNodesSolvedInSolvers{0};

  /// Total number of nodes solved so far
  uint64_t pNumTotalNodesSolved{0};

  /// Current number of nodes in all solver
  uint64_t pNumNodesInSolver{0};

  /// Indicates whether or not collecting mode is active
  bool pCollectingMode{false};

  /// Indicates whether or not the first sub-tree is breaking
  bool pBreakingFirstSubtree{false};

  /// Indicates whether or not the pool is before the initial collect
  bool pBeforeInitialCollect{true};

  /// Indicates whether or not the pool is before the finishing the first collect
  bool pBeforeFinishingFirstCollect{true};

  /// Inactive solvers map
  spp::sparse_hash_map<int, SolverPoolElement::SPtr> pInactiveSolversMap;

  /// Active solvers map
  spp::sparse_hash_map<int, SolverPoolElement::SPtr> pActiveSolversMap;

  /// Dead solvers map
  spp::sparse_hash_map<int, SolverPoolElement::SPtr> pDeadSolversMap;

  /// Map for candidate collecting mode solvers
  std::multimap<double, SolverPoolElement::SPtr> pCandidatesOfCollectingModeSolversMap;

  /// Solver pool indexed by solver's rank
  std::vector<SolverPoolElement::SPtr> pPool;

  /// Heap of active solvers (ascending or descending order)
  SelectionHeap::SPtr pSelectionHeap;

  /// Pointers to collecting mode solvers in ascending or descending order
  SelectionHeap::SPtr pCollectingModeSolverHeap;

  /// Message passing framework
  Framework::SPtr pFramework;

  /// Runtime parameters for parallelization
  ParamSet::SPtr pParamSet;

  /// Internal timer used for metrics
  timer::Timer::SPtr pTimer;

  /// Switch out time msec
  uint64_t pSwitchOutTimeMsec{0};

  /// Multiplier for number of collecting nodes
  int pMCollectingNodes{1};

  /// Maximum multiplier for the number of collecting nodes
  int pMMaxCollectingNodes{1};

  /// Number of dual bound gain testing solvers
  int pNumDualBoundGainTesting{0};

  /// Sends switch in-collecting mode to a specific solver
  void switchIntoCollectingToSolver(int rank, NodePool::SPtr nodePool);

  /// Returns the value to be used to discriminate good nodes from standard nodes
  inline double getValueForGoodNode(const SolverPoolElement::SPtr& solver,
                                    double globalBestDualBound) const
  {
    assert(solver);
    return ((solver->getBestDualBoundValue() - globalBestDualBound) /
            std::max(std::fabs(globalBestDualBound), 1.0));
  }

  /// Returns true if the given solver is working on a good node,
  /// returns false otherwise
  inline bool hasGoodNode(const SolverPoolElement::SPtr& solver, double globalBestDualBound) const
  {
    // As per paper "ParaSCIP - a parallel extension of SCIP",
    // a node is "good" if its best dual bound is "close enough" to the
    // global dual bound
    return getValueForGoodNode(solver, globalBestDualBound) < pBGap;
  }

 private:
  /// Utility function: used by "activateSolver(...)", this function
  /// erases the solver with given rank from the map of inactive solvers
  /// and activates the solver.
  /// It puts the active solver into the active solvers map
  void activateSolverImpl(int rank, Node::SPtr node);
};

/**
 * Solver pool used for racing.
 */
class SYS_EXPORT_CLASS RacingSolverPool {
 public:
  using SPtr = std::shared_ptr<RacingSolverPool>;

 public:
  RacingSolverPool(int origRank,
                   Framework::SPtr framework,
                   ParamSet::SPtr paramSet,
                   timer::Timer::SPtr timer = nullptr);

  virtual ~RacingSolverPool();

  /// Returns the number of solvers in the pool
  inline int getNumSolvers() const noexcept { return pNumSolvers; }

  /// Extracts and returns the racing root node
  Node::SPtr extractRacingRootNode();
  inline Node::SPtr extractNode() { return extractRacingRootNode(); }

  /// Returns the dual bound value of the given solver
  double getDualBoundValue(int rank) const;

  /// Returns the global dual bound value
  double getGlobalBestDualBoundValue() const;

  /// Returns the number of nodes solved by the specified solver
  long long getNumOfNodesSolved(int rank) const;

  /// Returns thenumber of nodes left in the specified solver
  int getNumNodesLeft(int rank) const;
  inline int getNumNodesLeftInSolver(int rank) const { return getNumNodesLeft(rank); }

  /// Returns the rank of the winner solver
  inline int getWinner() const noexcept { return pWinnerRank; }

  /// Returns the number of nodes in the best solver
  inline long long getNumNodesSolvedInBestSolver() const noexcept
  {
    return pNumNodesSolvedInBestSolver;
  }

  /// Returns the number of nodes left in the best solver
  inline long long getNumNodesLeftInBestSolver() const noexcept
  {
    return pNumNodesInBestSolver;
  }

  /// Returns the dual bound value in inactivated solvers
  inline double getBestDualBoundInInactivatedSolvers() const noexcept
  {
     return pBestDualBoundInSolvers;
  }
  inline double getBestDualBoundDeactivatedSolvers() const noexcept
  {
     return getBestDualBoundInInactivatedSolvers();
  }

  /// Activate racing ramp-up solver pool. All solvers become active on the given node.
  /// @note node is the root node entry point for the racing ramp-up.
  /// In other words, the given node is the root node.
  /// @note throws std::invalid_argument on empty node
  void activate(Node::SPtr node);

  /// Check whether the specified solver is active or not
  bool isActive(int rank) const;
  inline bool isSolverActive(int rank) const { return isActive(rank); }

  /// Update the status of the solver in the racing pool.
  /// It switches the solver into evaluation stage if needed, updates the number of
  /// nodes solved and left to solver, and updates the dual bound value in the selection heap
  void updateSolverStatus(int rank, long long numNodesSolved, int numNodesLeft,
                          double solverLocalBestDualBound);

  /// Returns true if the winner for racing has been decided.
  /// Returns false otherwise
  bool isWinnerDecided(bool feasibleSol);

  // Deactivates the solver in the pool specified by the given solver id
  void deactivateSolver(int rank);

  /// Returns the number of active solvers
  inline int getNumActiveSolvers() const noexcept { return pNumActiveSolvers; }

  /// Returns the number of inactive solvers
  inline int getNumInactiveSolvers() const noexcept { return pNumInactiveSolvers; }

  /// Returns true if the solver specified in an argument is in evaluation stage.
  /// Returns false otherwise
  bool isEvaluationStage(int rank) const;

 private:
  /// Rank of the winner solver of racing ramp-up
  int pWinnerRank{-1};

  /// Origin rank managed by this solver pool
  int pOriginRank{-1};

  /// Number of solver
  int pNumSolvers{0};

  /// Number of solvers that are in evaluation stage
  int pNumEvaluationStage{0};

  /// Number of nodes solved in the best solver
  long long pNumNodesSolvedInBestSolver{0};

  /// Number of nodes in the best solver
  long long pNumNodesInBestSolver{0};

  /// Number of active solvers
  int pNumActiveSolvers{0};

  /// Number of inactive solvers
  int pNumInactiveSolvers{0};

  /// Dual bounds
  double pBestDualBound{metaconst::DOUBLE_NEG_INF};
  double pBestDualBoundInSolvers{metaconst::DOUBLE_NEG_INF};

  /// Solver pool indexed by solver's rank
  std::vector<SolverPoolElement::SPtr> pPool;

  /// Heap of active solvers (ascending or descending order)
  SelectionHeap::SPtr pSelectionHeap;

  /// Message passing framework
  Framework::SPtr pFramework;

  /// Runtime parameters for parallelization
  ParamSet::SPtr pParamSet;

  /// Pointer to the root node of the proble
  /// from which each solver will start racing
  Node::SPtr pRootNode;

  /// Internal timer used for metrics
  timer::Timer::SPtr pTimer;
};

/**
 * Solver Pool for Minimization Problems.
 *
 * This class extends the base class SolverPool.
 */
class SYS_EXPORT_CLASS SolverPoolForMinimization : public SolverPool {
public:
  SolverPoolForMinimization(double MP,
                            double BGap,
                            double MBGap,
                            int origRank,
                            Framework::SPtr framework,
                            ParamSet::SPtr paramSet,
                            timer::Timer::SPtr timer = nullptr);

  /// Returns the best global dual bound value
  double getGlobalBestDualBoundValue() override;

  /// Switches the pool into collecting mode
  void switchIntoCollectingMode(NodePool::SPtr nodePool) override;

  /// Updates the status of the specified solver with the specified information
  void updateSolverStatus(int rank, uint64_t numNodesSolved, int numNodesLeft,
                          double solverLocalBestDualBound,
                          NodePool::SPtr nodePool) override;

private:
  void pickAndUseSolverFromCandidateForCollectingModeSolver(int rankSolver,
                                                            NodePool::SPtr nodePool);

};

}  // namespace metabb
}  // namespace optilab
