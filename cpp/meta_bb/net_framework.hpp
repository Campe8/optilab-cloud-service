//
// Copyright OptiLab 2020. All rights reserved.
//
// Framework class implementation based on the optnet framework.
// @note this is NOT a complete implementation.
// In particular, some methods are still pure.
// This class implements some methods that are directly related
// to the optnet framework.
//

#pragma once

#include <cstdint>  // for uint64_t
#include <memory>   // for std::shared_ptr

#include "meta_bb/framework.hpp"
#include "meta_bb/meta_bb_constants.hpp"
#include "optimizer_network/network_communicator.hpp"
#include "optimizer_network/network_hub.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace  metabb {

/**
 * Network Framework class.
 * The architecture looks like the following:
 *
 *           [Load Manager] <>-+---- NetworkHub          | Root
 *                  |          +---- NetworkCommunicator | Rank_0
 *           +------+------+
 *           |      |      |
 *       Solver_1  ...  Solver_n
 *        Rank_1         Rank_n
 *
 * Notice that the root (rank_0) does not have any solver.
 * In turn, it runs the hub and the root NetworkCommunicator.
 */
class SYS_EXPORT_CLASS NetFramework : public Framework {
 public:
  using SPtr = std::shared_ptr<NetFramework>;

 public:
  /// Constructor for NetFramework.
  /// The constructor builds a NetworkCommunicator.
  /// If the "isRoot" flag is true, the framework also creates and runs
  /// a network HUB.
  /// The root framework has rank 0 and it is in charge of sending network
  /// synchronization messages to all other nodes in the network
  explicit NetFramework(bool isRoot);

  virtual ~NetFramework();

  /// Cleanup resources.
  /// Returns zero on success, non-zero otherwise
  int tearDown() noexcept;

  /// Returns the rank of the communicator hold by this framework
  inline int getRank() const noexcept { return pIsInit ? pCommunicator->getRank() : -1; }

  /// Returns the size of the network
  inline int getNetworkSize() const noexcept { return pIsInit ? pCommunicator->getSize() : -1; }

  /// Returns true if this NetFramework is root,
  /// returns false otherwise
  inline bool isRootFramework() const noexcept { return pIsRoot; }

  /// Returns a default parameter set
  std::shared_ptr<ParamSet> buildParamSet() override;

  /// Returns a default calculation state
  std::shared_ptr<CalculationState> buildCalculationState() override;

  /// Returns a calculation state built on the given input arguments
  std::shared_ptr<CalculationState> buildCalculationState(
      uint64_t compTimeMsec,
      uint64_t rootTimeMsec,
      int numNodesSolved,
      int numNodesSent,
      int numImprovedIncumbent,
      solver::TerminationState terminationState,
      int numNodesSolverWithoutPreprocess,
      int numSimplexIterRoot,
      double avgSimplexIter,
      int numTransferredLocalCuts,
      int minTransferredLocalCuts,
      int maxTransferredLocalCuts,
      int numRestarts,
      double minInfeasibilitySum,
      double maxInfeasibilitySum,
      int minInfeasibilityNum,
      int maxInfeasibilityNum,
      double dualBound) override;

  /// Builds and returns a default node
  std::shared_ptr<Node> buildNode() override;

  /// Builds and returns a node with specified parameters
  std::shared_ptr<Node> buildNode(
      NodeId nodeId,
      NodeId generatorNodeId,
      int depth,
      double dualBoundValue,
      double originalDualBoundValue,
      double estimatedValue,
      std::shared_ptr<DiffSubproblem> diffSubproblem) override;

  /// Builds and returns a default solver state
  std::shared_ptr<SolverState> buildSolverState() override;

  /// Builds and returns a solver state with specified values
  std::shared_ptr<SolverState> buildSolverState(
      int racingStage,
      unsigned int notificationId,
      int LMId,
      int globalSubtreeId,
      long long nodesSolved,
      int nodesLeft,
      double bestDualBoundValue,
      double globalBestPrimalBoundValue,
      uint64_t timeMsec,
      double averageDualBoundGain) override;

  /// Builds and returns a default solver termination state
  std::shared_ptr<SolverTerminationState> buildSolverTerminationState() override;

  /// Builds and returns a solver termination state with specified values
  std::shared_ptr<SolverTerminationState> buildSolverTerminationState(
      solver::InterruptionPoint interrupted,
      int rank,
      int totalNumSolved,
      int minNumSolved,
      int maxNumSolved,
      int totalNumSent,
      int totalNumImprovedIncumbent,
      int numNodesReceived,
      int numNodesSolved,
      int numNodesSolvedAtRoot,
      int numNodesSolvedAtPreCheck,
      int numTransferredLocalCutsFromSolver,
      int minNumTransferredLocalCutsFromSolver,
      int maxNUmTransferredLocalCutsFromSolver,
      int numTotalRestarts,
      int minNumRestarts,
      int maxNumRestarts,
      int numVarTightened,
      int numIntVarTightenedInt,
      uint64_t runningTimeMsec,
      uint64_t idleTimeToFirstNodeMsec,
      uint64_t idleTimeBetweenNodesMsec,
      uint64_t idleTimeAfterLastNodeMsec,
      uint64_t idleTimeToWaitNotificationIdMsec,
      uint64_t idleTimeToWaitAckCompletionMsec,
      uint64_t idleTimeToWaitTokenMsec,
      uint64_t totalRootNodeTimeMsec,
      uint64_t minRootNodeTimeMsec,
      uint64_t maxRootNodeTimeMsec) override;

 protected:
  /// Initializes this framework's connections.
  /// Returns zero on success, non-zero otherwise
  int initConnections() noexcept;

  /// Send an abort message to all nodes in the network.
  /// @note throws std::runtime_error if the framework has not been initialized.
  /// @note only ONE node in the network (root) should call abort.
  /// If multiple nodes call abort, the result is undefined behavior.
  /// TODO remove the limitation that only one node can call abort.
  void abortConnections() const;

 private:
  /**
   * Register of connectors used by the network communicator.
   */
  struct ConnectorsRegister {
    /// List of sockets
    optilab::optnet::SocketConnector::SocketSPtr pSendSocket;
    optilab::optnet::SocketConnector::SocketSPtr pRecvSocket;
    optilab::optnet::SocketConnector::SocketSPtr pBroadcastSendSocket;
    optilab::optnet::SocketConnector::SocketSPtr pBroadcastRecvSocket;

    /// List of SocketConnectors
    optilab::optnet::SocketConnector::UPtr pPToPSendConn;
    optilab::optnet::SocketConnector::UPtr pPToPRecvConn;
    optilab::optnet::BroadcastSocketConnector::UPtr pBcastSendConn;
    optilab::optnet::BroadcastSocketConnector::UPtr pBcastRecvConn;

    /// Context
    std::unique_ptr<zmq::context_t> pContext;
  };

 private:
  /// Flag indicating whether this framework is root or not
  bool pIsRoot;

  /// Flag indicating whether or not
  /// this framework has been initialized
  bool pIsInit;

  /// Register of connectors
  ConnectorsRegister pConnRegister;

  /// Hub for the root framework
  optnet::NetworkHub::SPtr pHub;

  /// Network communicator running on this NetFramework
  optilab::optnet::NetworkCommunicator::SPtr pCommunicator;

  /// Initialize the sockets
  void initSockets();

  /// Initializes this network framework as root NetFramework
  void initRootFramework();

  /// Initializes this network framework as non-root NetFramework
  void initFramework();
};

}  // namespace metabb
}  // namespace optilab
