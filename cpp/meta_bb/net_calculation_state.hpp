//
// Copyright OptiLab 2020. All rights reserved.
//
// Calculation state class implemented on top of the
// optnet framework.
//

#pragma once

#include <cstddef>  // for std::size_t
#include <cstdint>  // for uint64_t
#include <memory>   // for std::shared_ptr

#include "meta_bb/calculation_state.hpp"
#include "optilab_protobuf/meta_bb_packet.pb.h"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace  metabb {

class SYS_EXPORT_CLASS NetCalculationState : public CalculationState {
 public:
  using SPtr = std::shared_ptr<CalculationState>;

 public:
  /// Default constructor
  NetCalculationState() = default;

  /// Constructor:
  /// - compTimeMsec: computation time of this node in seconds
  /// - rootTimeMsec: computation time of the root node in seconds
  /// - numSolved: number of nodes solved
  /// - numNodesSent: number of nodes sent
  /// - numImprovedIncumbent: number of improved solutions generated in this solver
  /// - terminationState states whether or not this state/computation is in termination state
  /// - numNodesSolverWithoutPreprocess: number of nodes solved without pre-process
  /// - numSimplexIterRoot: number of simplex iteration at the root node
  /// - avgSimplexIter: average number of simplex iteration not at the root node
  /// - numTransferredLocalCuts: number of local cuts transferred from a node
  /// - minTransferredLocalCuts: minimum number of local cuts transferred from a node
  /// - maxTransferredLocalCuts: maximum number of local cuts transferred from a node
  /// - numRestarts: number of restarts
  /// - minInfeasibilitySum: minimum sum of integer infeasibility
  /// - maxInfeasibilitySum: maximum sum of integer infeasibility
  /// - minInfeasibilityNum: minimum number of integer infeasibility
  /// - maxInfeasibilityNum: maximum number of integer infeasibility
  /// - dualBound: dual bound value
  NetCalculationState(uint64_t compTimeMsec,
                      uint64_t rootTimeMsec,
                      int numNodesSolved,
                      int numNodesSent,
                      int numImprovedIncumbent,
                      solver::TerminationState terminationState,
                      int numNodesSolverWithoutPreprocess,
                      std::size_t numSimplexIterRoot,
                      double avgSimplexIter,
                      std::size_t numTransferredLocalCuts,
                      std::size_t minTransferredLocalCuts,
                      std::size_t maxTransferredLocalCuts,
                      std::size_t numRestarts,
                      double minInfeasibilitySum,
                      double maxInfeasibilitySum,
                      std::size_t minInfeasibilityNum,
                      std::size_t maxInfeasibilityNum,
                      double dualBound);

  /// Destructor:
  /// checks the node parent and children and re-connect parent to children
  ~NetCalculationState() override;

  /// Sends this state to the destination using the given framework
  int send(const Framework::SPtr& framework, int destination) noexcept override;

  /// Uploads the content of the given network packet into this calculation state..
  /// If the given packet is not a node network packet, throws std::runtime_error
  /// Returns zero on success, non-zero otherwise
  int upload(optnet::Packet::UPtr packet) noexcept override;

 protected:
  /// Builds and returns a network packet encoding this calculation state
  optnet::Packet::UPtr buildNetworkPacket();
};

}  // namespace metabb

/// Specialized template for network node packet comparison
template<> bool optnet::NetworkPacket<metabb::CalculationStateProtoPacket>::operator==(
    const NetworkPacket<metabb::CalculationStateProtoPacket>& other) const noexcept;

/// Specialized template to serialize NodeProtoPacket(s)
template<> std::pair<optnet::BaseNetworkPacket::BaseNetworkPacketType, std::string>
optnet::NetworkPacket<metabb::CalculationStateProtoPacket>::serialize() const noexcept;

}  // namespace optilab
