//
// Copyright OptiLab 2019-2020. All rights reserved.
//
// Implementation of a generic parallel solver.
// This is a convenience class for function handlers.
//

#pragma once

#include <memory>  // for std::shared_ptr

#include "meta_bb/solver.hpp"
#include "meta_bb/meta_bb_struct.hpp"
#include "optimizer_network/packet.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace  metabb {

class SYS_EXPORT_CLASS SolverImpl {
 public:
  using SPtr = std::shared_ptr<SolverImpl>;

 public:
  SolverImpl(Solver* solver);

  virtual ~SolverImpl() = default;

  /// Operator calling the proper handler on the given tag
  int operator()(int source, optnet::Packet::UPtr packet);

  /// Returns true if there is an handler for the given tag.
  /// Returns false otherwise
  bool isHandlerSupported(net::MetaBBPacketTag::PacketTagType tag);

 protected:
  /// Pointer to the solver owning this instance
  Solver* pSolver;

  inline Solver& solver() { return *pSolver; }

  virtual int processTagNode(int source, optnet::Packet::UPtr packet);
  virtual int processTagNodeReceived(int source, optnet::Packet::UPtr packetg);
  virtual int processTagRampUp(int source, optnet::Packet::UPtr packet);
  virtual int processTagRetryRampUp(int source, optnet::Packet::UPtr packet);
  virtual int processTagSolution(int source, optnet::Packet::UPtr packet);
  virtual int processTagIncumbentValue(int source, optnet::Packet::UPtr packet);
  virtual int processTagGlobalBestDualBoundValueAtWarmStart(int source,
                                                            optnet::Packet::UPtr packet);
  virtual int processTagNoNodes(int source, optnet::Packet::UPtr packet);
  virtual int processTagInCollectingMode(int source, optnet::Packet::UPtr packet);
  virtual int processTagCollectAllNodes(int source, optnet::Packet::UPtr packet);
  virtual int processTagOutCollectingMode(int source, optnet::Packet::UPtr packet);
  virtual int processTagLMBestBoundValue(int source, optnet::Packet::UPtr packetg);
  virtual int processTagNotificationId(int source, optnet::Packet::UPtr packet);
  virtual int processTagTerminateRequest(int source, optnet::Packet::UPtr packet);
  virtual int processTagInterruptRequest(int source, optnet::Packet::UPtr packet);
  virtual int processTagWinnerRacingRampUpParamSet(int source, optnet::Packet::UPtr packet);
  virtual int processTagWinner(int source, optnet::Packet::UPtr packet);
  virtual int processTagLightWeightRootNodeProcess(int source, optnet::Packet::UPtr packet);
  virtual int processTagBreaking(int source, optnet::Packet::UPtr packet);
  virtual int processTagTerminateSolvingToRestart(int source, optnet::Packet::UPtr packet);
  virtual int processTagGivenGapIsReached(int source, optnet::Packet::UPtr packet);
  virtual int processTagTestDualBoundGain(int source, optnet::Packet::UPtr packet);
  virtual int processTagNoTestDualBoundGain(int source, optnet::Packet::UPtr packet);
  virtual int processTagNoWaitModeSend(int source, optnet::Packet::UPtr packet);
  virtual int processTagRestart(int source, optnet::Packet::UPtr packet);
  virtual int processTagLbBoundTightened(int source, optnet::Packet::UPtr packet);
  virtual int processTagUbBoundTightened(int source, optnet::Packet::UPtr packet);
  virtual int processTagCutOffValue(int source, optnet::Packet::UPtr packet);
};


}  // namespace metabb
}  // namespace optilab
