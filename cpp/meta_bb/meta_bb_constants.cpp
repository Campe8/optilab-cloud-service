#include "meta_bb/meta_bb_constants.hpp"

#include <limits>  // for std::numeric_limits


namespace optilab {
namespace  metabb {

namespace solverparamset {
const double DEFAULT_DOUBLE_PARAM_VALUE = -1.0;
const double DEFAULT_DUAL_TOLERANCE = 1e-7;
const double DEFAULT_INTEGER_PARAM_VALUE = -1;
const double DEFAULT_PRIMAL_TOLERANCE = 1e-07;
const double DEFAULT_RELATIVE_MIP_GAP = 1e-4;
const double DEFAULT_UNKNOWN_DOUBLE_PARAM_VALUE = -2.0;
const double DEFAULT_UNKNOWN_INTEGER_PARAM_VALUE = -2;
} // namespace solverparamset

namespace metaconst {
const double DEAFULT_NUM_EPSILON = 1e-9;
const double DOUBLE_POS_INF = std::numeric_limits<double>::max();
const double DOUBLE_NEG_INF = std::numeric_limits<double>::lowest();
const int ERR_GENERIC_ERROR = 1;
const int ERR_NO_ERROR = 0;
const double MAX_DOUBLE = std::numeric_limits<double>::max();
const double MIN_DOUBLE = std::numeric_limits<double>::lowest();
const double MIN_EPSILON = 1e-20;
}  // metaconst

namespace  treeconsts {
const int DEFAULT_UNSPECIFIED_ID = -1;
}  // namespace treeconsts

namespace solver {
const int RAMP_UP_PHASE_NORMAL = 0;
const int RAMP_UP_PHASE_RACING = 1;
const int RAMP_UP_PHASE_REBUILD_TREE_AFTER_RACING = 2;
}  // namespace solver

}  // namespace metabb
}  // namespace optilab
