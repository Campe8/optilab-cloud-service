#include "meta_bb/meta_bb_utilities.hpp"

#include <cassert>

#include "meta_bb/meta_bb_macros.hpp"

namespace optilab {
namespace metabb {
namespace utils {

void sendTagMessageToNetworkNode(Framework::SPtr framework,
                                 net::MetaBBPacketTag::PacketTagType tag, int rank)
{
  assert(framework);
  const int dummyContentData{0};
  optnet::Packet::UPtr packet(new optnet::Packet());
  packet->packetTag = net::MetaBBPacketTag::UPtr(new net::MetaBBPacketTag(tag));
  packet->networkPacket = optnet::NetworkPacket<int>::UPtr(
          new optnet::NetworkPacket<int>(dummyContentData));
  ASSERT_COMM_CALL(framework->getComm()->send(std::move(packet), rank));
}  //sendTagMessageToNetworkNode

void receiveTagMessageFromNetworkNode(Framework::SPtr framework, optnet::Packet::UPtr& packet,
                                      net::MetaBBPacketTag::PacketTagType tag, int rank)
{
  assert(packet);
  assert(framework);
  packet->packetTag = net::MetaBBPacketTag::PacketTag::UPtr(
          new net::MetaBBPacketTag::PacketTag(tag));
  ASSERT_COMM_CALL(framework->getComm()->receive(packet, rank));
}  // receiveTagMessageFromNetworkNode

net::MetaBBPacketTag::PacketTagType castToMetaBBPacketTagAndGetTagOrThrow(
        optnet::PacketTag::UPtr& packetTag)
{
  if (!packetTag)
  {
    throw std::invalid_argument("castToMetaBBPacketTagAndGetTagOrThrow - empty packet tag");
  }

  // Notice that dynamic_cast cannot be used since the received packet from the socket
  // sets up a pointer to the base class not the derived packet class.
  // TODO store information about the packet type
  auto metabbTag = static_cast<net::MetaBBPacketTag*>(packetTag.get());
  if (metabbTag == nullptr)
  {
    throw std::runtime_error("castToMetaBBPacketTagAndGetTagOrThrow - "
            "invalid downcast to MetaBBPacketTag");
  }

  return metabbTag->getPacketTagType();
}  // castToMetaBBPacketTagAndGetTagOrThrow

net::MetaBBPacketTag::UPtr castToMetaBBPacketTag(optnet::PacketTag::UPtr packetTag)
{
  // If nullptr, return asap
  if (!packetTag)
  {
    return nullptr;
  }

  // Store the raw pointer to clear memory if cast fails
  auto* rawPtr = packetTag.get();

  // Notice that dynamic_cast cannot be used since the received packet from the socket
  // sets up a pointer to the base class not the derived packet class.
  // TODO store information about the packet type
  auto tagPtr = static_cast<net::MetaBBPacketTag*>(packetTag.release());

  // Check if cast has failed
  if (!tagPtr)
  {
    delete rawPtr;
    throw std::runtime_error("castToMetaBBPacketTag: cannot cast PacketTag to MetaBBPacketTag");
  }

  // Returns the casted tag packet
  net::MetaBBPacketTag::UPtr metaTagPacket;
  metaTagPacket.reset(tagPtr);
  return metaTagPacket;
}  // castToMetaBBPacketTag

}  // namespace utils
}  // namespave metabb
}  // namespace optilab
