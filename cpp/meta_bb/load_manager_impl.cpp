#include "meta_bb/load_manager_impl.hpp"

#include <cassert>
#include <limits>     // for std::numeric_limits
#include <stdexcept>  // for std::runtime_error
#include <sstream>
#include <string>
#include <utility>    // for std::move

#include <spdlog/spdlog.h>

#include "meta_bb/initial_stat.hpp"
#include "meta_bb/load_manager_impl.hpp"
#include "meta_bb/load_manager_merge_impl.hpp"
#include "meta_bb/meta_bb_constants.hpp"
#include "meta_bb/meta_bb_macros.hpp"
#include "meta_bb/meta_bb_utilities.hpp"
#include "meta_bb/solver_state.hpp"
#include "meta_bb/solver_termination_state.hpp"
#include "meta_bb/tree_id.hpp"
#include "optimizer_network/network_packet.hpp"

namespace {
// Error value - no error
constexpr int kErrorNoError = 0;

// Status of the first collecting mode:
// -1) not being in collecting mode
//  0) in collecting mode once
//  1) collecting mode is terminated once
constexpr int kFirstCollectingModeNotBeingInCollecting = -1;
constexpr int kFirstCollectingModeInCollectingModeOnce =  0;
constexpr int kFirstCollectingModeTerminatedOnce       =  1;

// Maximum number of dual boung gains to keep in the last several
// dual bound gains
constexpr std::size_t kMaxNumDualBoundGainsInQueue = 7;
}  // namespace

namespace optilab {
namespace  metabb {

LoadManagerImpl::LoadManagerImpl(LoadManager* lm, bool racingRampUp)
: pIsRacingRampUp(racingRampUp),
  pLM(lm)
{
  assert(pLM);
}

bool LoadManagerImpl::isHandlerSupported(net::MetaBBPacketTag::PacketTagType tag)
{
  bool supported = false;
  switch(tag)
  {
    case net::MetaBBPacketTag::PacketTagType::PPT_ACK_COMPLETION:
    case net::MetaBBPacketTag::PacketTagType::PPT_ALLOW_TO_BE_IN_COLLECTING_MODE:
    case net::MetaBBPacketTag::PacketTagType::PPT_ANOTHER_NODE_REQUEST:
    case net::MetaBBPacketTag::PacketTagType::PPT_COMPLETION_OF_CALCULATION:
    case net::MetaBBPacketTag::PacketTagType::PPT_HARD_TIME_LIMIT:
    case net::MetaBBPacketTag::PacketTagType::PPT_INITIAL_STAT:
    case net::MetaBBPacketTag::PacketTagType::PPT_LB_BOUND_TIGHTENED_INDEX:
    case net::MetaBBPacketTag::PacketTagType::PPT_SOLUTION:
    case net::MetaBBPacketTag::PacketTagType::PPT_SOLVER_STATE:
    case net::MetaBBPacketTag::PacketTagType::PPT_TERMINATED:
    case net::MetaBBPacketTag::PacketTagType::PPT_TOKEN:
    case net::MetaBBPacketTag::PacketTagType::PPT_UB_BOUND_TIGHTENED_INDEX:
    case net::MetaBBPacketTag::PacketTagType::PTT_NODE:
      supported = true;
      break;
    default:
      supported = false;
      break;
  }
  return supported;
}  // isHandlerSupported

int LoadManagerImpl::operator()(int source, optnet::Packet::UPtr packet)
{
  if (!packet)
  {
    throw std::runtime_error("LoadManagerImpl - functor operator: empty input packet");
  }

  if (!packet->packetTag)
  {
    throw std::runtime_error("LoadManagerImpl - functor operator: empty input packet tag");
  }

  net::MetaBBPacketTag* metabbPacket = static_cast<net::MetaBBPacketTag*>((packet->packetTag).get());
  switch(metabbPacket->getPacketTagType())
  {
    case net::MetaBBPacketTag::PacketTagType::PTT_NODE:
      return processTagNode(source, std::move(packet));
    case net::MetaBBPacketTag::PacketTagType::PPT_SOLUTION:
      return processTagSolution(source, std::move(packet));
    case net::MetaBBPacketTag::PacketTagType::PPT_SOLVER_STATE:
    {
      if (!pIsRacingRampUp)
      {
        return processTagSolverState(source, std::move(packet));
      }
      else
      {
        return processRacingRampUpTagSolverState(source, std::move(packet));
      }
    }
    case net::MetaBBPacketTag::PacketTagType::PPT_COMPLETION_OF_CALCULATION:
    {
      if (!pIsRacingRampUp)
      {
        return processTagCompletionOfCalculation(source, std::move(packet));
      }
      else
      {
        return processRacingRampUpTagCompletionOfCalculation(source, std::move(packet));
      }
    }
    case net::MetaBBPacketTag::PacketTagType::PPT_ANOTHER_NODE_REQUEST:
      return processTagAnotherNodeRequest(source, std::move(packet));
    case net::MetaBBPacketTag::PacketTagType::PPT_TERMINATED:
      return processTagTerminated(source, std::move(packet));
    case net::MetaBBPacketTag::PacketTagType::PPT_HARD_TIME_LIMIT:
      return processTagHardTimeLimit(source, std::move(packet));
    case net::MetaBBPacketTag::PacketTagType::PPT_INITIAL_STAT:
      return processTagStatistics(source, std::move(packet));
    case net::MetaBBPacketTag::PacketTagType::PPT_TOKEN:
      return processTagToken(source, std::move(packet));
    case net::MetaBBPacketTag::PacketTagType::PPT_ALLOW_TO_BE_IN_COLLECTING_MODE:
      return processTagAllowToBeInCollectingMode(source, std::move(packet));
    case net::MetaBBPacketTag::PacketTagType::PPT_LB_BOUND_TIGHTENED_INDEX:
      return processTagLbBoundTightened(source, std::move(packet));
    case net::MetaBBPacketTag::PacketTagType::PPT_UB_BOUND_TIGHTENED_INDEX:
      return processTagUbBoundTightened(source, std::move(packet));
    default:
      const std::string errMsg = "LoadManagerImpl - no handler for given tag";
      spdlog::error(errMsg);
      throw std::runtime_error(errMsg);
  }
}  // operator()

void LoadManagerImpl::logTime(const std::string& fcnName)
{
  std::stringstream ss;
  ss << fcnName << " - Checkpoint (msec.): " << pLM->pTimer->getWallClockTimeMsec();
  spdlog::info(ss.str());
  std::cout << ss.str() << std::endl;
}  //logTime

void LoadManagerImpl::switchSolversToCollectingMode(bool increaseMaxNumSolversInCollectingMode)
{
  // Collecting mode is not restarted
  auto timeMsecOld = pLM->pSolverPool->getSwichOutTime();

  // Switch the solver pool out from collecting mode to restart the
  // collecting mode process
  pLM->pSolverPool->switchOutCollectingMode();

  // Set switch out time
  pLM->pSolverPool->setSwichOutTime(timeMsecOld);

  bool increasedNum = false;
  if (increaseMaxNumSolversInCollectingMode &&
          pLM->pSolverPool->canIncreaseLimitNumLimitCollectingModeSolvers())
  {
    increasedNum = true;
    pLM->pSolverPool->incNumLimitCollectingModeSolvers();
  }

  // Switch the solver back to collecting mode.
  // @note this restarts the collecting mode process
  pLM->pSolverPool->switchIntoCollectingMode(pLM->pNodePool);

  if (increasedNum)
  {
    spdlog::info("LoadManager::processTagSolverState - "
        "collecting mode is restarted with a higher limit of collecting mode solvers " +
        std::to_string(pLM->pSolverPool->getNumLimitCollectingModeSolvers()));
  }
  else
  {
    spdlog::info("LoadManager::processTagSolverState - "
        "collecting mode is restarted with collecting mode solvers " +
        std::to_string(pLM->pSolverPool->getNumLimitCollectingModeSolvers()));
  }
  logTime("LoadManager (switchSolversToCollectingMode)");
}  // switchSolversToCollectingMode

int LoadManagerImpl::processTagNode(int source, optnet::Packet::UPtr packet)
{
  spdlog::info("LoadManager::processTagNode - source " + std::to_string(source));
  logTime("LoadManager (processTagNode)");

  // Create a new node and upload its content from the received packet
  auto node = pLM->pFramework->buildNode();
  node->upload(pLM->pFramework, std::move(packet));

  pLM->pLMTerminationState.numNodesReceived++;
  if (pLM->pNodePoolToRestart)
  {
    // Ramp-down phase active, delete the node and return
    node.reset();
    return kErrorNoError;
  }

  // If there is no primal solution or the node's dual bound is lower
  // than the primal solution, add this node to the node pool
  auto sol = pLM->pInitiator->getGlobalBestIncumbentSolution();
  if (!sol || (sol && node->getDualBoundValue() < sol->getObjectiveFuntionValue()))
  {
    // Create a new global subtree identifier and set it as subtree id of the new created node
    node->setGlobalSubtreeId(getComm()->getRank(), pLM->getNewGlobalSubtreeId());

    // Get the ancestor for the newly created node from the solver pool.
    // The ancestor is the source node from which this node was received.
    // In other words, the node currently being solved by the solver that sent this node
    auto parentNode = pLM->pSolverPool->getCurrentNodeFromSolver(source);

    // Set the ancestor in the newly created node as local ancestor (i.e., parent)
    node->setParent(std::make_shared<NodeGenealogicalLocalRel>(
            parentNode->getNodeId(), parentNode));

    // Similarly, set the newly created node as descendant
    // of the ancestor node (i.e., child)
    parentNode->addChild(std::make_shared<NodeGenealogicalLocalRel>(node->getNodeId(), node));

    // Check if there is a huge imbalance going on,
    // if so, insert the node in the node buffering pool
    if (pLM->pHugeImbalance)
    {
      pLM->pNodePoolBufferToRestart->insert(node);
    }
    else
    {
      // otherwise insert the node in the standard node pool
      pLM->pNodePool->insert(node);
    }

    // Check if merging is in process (e.g., rebuilding the tree after racing ramp-up).
    // If so, add the node to the merge node structs
    if (pLM->pMerging)
    {
      pLM->pLoadManagerMergeImpl->addNodeToMergeNodeStructs(node);
    }

    // Check if racing status branching is applied.
    // If so, and the source node is the winner of the racing ramp-up,
    // update the winner depth with the new node depth
    if (getParams().racingstatbranching() && source == pLM->pRacingWinner &&
            !(pLM->pWinnerSolverNodesCollected))
    {
      pLM->pMinDepthInWinnerSolverNodes = std::min(
              pLM->pMinDepthInWinnerSolverNodes, node->getDepth());
    }

    // If all solvers are active or "sendNodesToIdleSolvers(...)" returns false,
    // check if the phase is not in ramp-up and the solver pool is in collecting mode and
    // there are already enough good nodes collected.
    // If all the above is true, switch the solver pool out of collecting mode
    if (!(pLM->pSolverPool->getNumInactiveSolvers() > 0 &&
            pLM->sendNodesToIdleSolvers()))
    {
      // Get the best (local) dual bound value
      const double globalBestDualBoundValueLocal = pLM->getGlobalBestDualBoundValueLocal();
      const auto numToSwith = getParams().numnodestoswitchtocollectingmode();
      const double numToSwithMul = getParams().multiplierforcollectingmode();
      if (!isRampUpPhase())
      {
        if (pLM->pSolverPool->isInCollectingMode() &&
                (getNumGoodNodes(globalBestDualBoundValueLocal) >
                 numToSwithMul * numToSwith * pLM->pSolverPool->getMCollectingNodes()))
        {
          // Switch out of collecting mode since enough good nodes are present
          // in the node pool
          pLM->pSolverPool->switchOutCollectingMode();
          pLM->pFirstCollectingModeState = kFirstCollectingModeTerminatedOnce;
          pLM->pIsCollectingModeRestarted = false;
        }
      }
    }
  }
  else
  {
    // Here there is no solution or there is already a global best incumbent solution and
    // the node's dual bound value is greater than the best incumber solution.
    // This means that this node is "useless", it doesn't have the solution under its subtree.

    // Assert that there is a gap between the primal and the dual bound of the node of,
    // at least, epsilon
    assert(!(EPSLT(node->getDualBoundValue(),
                   pLM->pInitiator->getGlobalBestIncumbentSolution()->getObjectiveFuntionValue(),
                   pLM->pEpsilon)));

    // Delete the node
    node.reset();
    pLM->pLMTerminationState.numNodesDeletedInLM++;

    // Check if the phase is not ramp-up, if the solver pool is not in collecting mode,
    // if there are enough inactive solvers, and if the node pool is empty.
    // In the above are true, switch the solver pool to collecting mode
    if (!isRampUpPhase())
    {
      if (!(pLM->pSolverPool->isInCollectingMode()) &&
              pLM->pNodePool->isEmpty() &&
              pLM->pSolverPool->getNumInactiveSolvers() > (pLM->pSolverPool->getNumSolvers() * 0.1))
      {
        // No nodes in the node pool, switch into collecting mode
        pLM->pSolverPool->switchIntoCollectingMode(pLM->pNodePool);

        // Update the status based on whether or not this was the first time
        // the solver pool switched to collecting mode
        if (pLM->pFirstCollectingModeState == kFirstCollectingModeNotBeingInCollecting &&
                pLM->pSolverPool->isInCollectingMode())
        {
          // Now it is being in collecting mode at least once
          pLM->pFirstCollectingModeState = kFirstCollectingModeInCollectingModeOnce;
        }
      }
    }
  }

  spdlog::info("LoadManager::processTagNode - tag received and processed");
  logTime("LoadManager (processTagNode)");

  // Communicate back to the sender that the tag has been received (and processed)
  utils::sendTagMessageToNetworkNode(
          pLM->pFramework,
          net::MetaBBPacketTag::PacketTagType::PPT_NODE_RECEIVED,
          source);

  return kErrorNoError;
} // processTagNode

int LoadManagerImpl::processTagSolution(int source, optnet::Packet::UPtr packet)
{
  spdlog::info("LoadManager::processTagSolution - source " + std::to_string(source));
  logTime("LoadManager (processTagNode)");

  // Create a new instance of a solution and instantiate it from the source node
  auto sol = getFramework()->buildSolution();
  sol->upload(std::move(packet));

  spdlog::info("LoadManager::processTagSolution - solution received: " +
               std::to_string(pLM->pInitiator->convertToExternalValue(
                       sol->getObjectiveFuntionValue())));

  // Update the solution in the load manager
  if (pLM->updateSolution(sol))
  {
    // Update done, delete the solution
    sol.reset();

    // Check if there is a special cut-off value from the Initiator.
    // If so, send the cut-off value to the source
    if (pLM->pInitiator->canGenerateSpecialCutOffValue())
    {
      pLM->sendCutOffValue(source);
    }

    // Send the incumbent (best solution found) to all other solvers/workers
    pLM->sendIncumbentValue(source);

    pLM->pPrimalSolutionUpdated = true;
    pLM->pAllCompInfeasibleAfterSolution = true;

    if (!getParams().quiet())
    {
      std::stringstream ss;
      ss << "LoadManager - processTagSolution: solution received from solver " << source <<
              " at time (msec.) " << pLM->pTimer->getWallClockTimeMsec() << " with value " <<
              pLM->pInitiator->convertToExternalValue(
                      pLM->pInitiator->getGlobalBestIncumbentSolution()->
                      getObjectiveFuntionValue());
      spdlog::info(ss.str());
      std::cout << ss.str() << std::endl;
    }
  }

  // Clear solution before returning.
  // @note solution is a smart pointer and it should be deleted
  // when returning to the caller. However, calling the destructor here
  // allows the caller to control possible side-effects
  // triggered by solution's destructor
  sol.reset();

  return kErrorNoError;
}  // processTagSolution

int LoadManagerImpl::processTagSolverState(int source, optnet::Packet::UPtr packet)
{
  // Send the state of the solver to LM
  spdlog::info("LoadManager::processTagSolverState - source " + std::to_string(source));
  logTime("LoadManager (processTagSolverState)");

  double globalBestDualBoundValue = metaconst::MIN_DOUBLE;
  double globalBestDualBoundValueLocal = metaconst::MIN_DOUBLE;

  // Build a new solver state and receive that from the given source
  auto solverState = getFramework()->buildSolverState();
  solverState->upload(std::move(packet));

  // Solver is in racing phase
  if (solverState->isRacingStage())
  {
    // Get the best dual bound value among solver pool and node pool
    globalBestDualBoundValueLocal = globalBestDualBoundValue = std::min(
            getSolverPoolBestDualBound(), getNodePoolBestDualBound());

    if (!getParams().quiet())
    {
      std::stringstream ss;
      ss << "LoadManager::processTagSolverState - state solver " << source <<
              " at time (msec.) " << pLM->pTimer->getWallClockTimeMsec() <<
              " with a dual bound value of " << pLM->pInitiator->convertToExternalValue(
                      solverState->getSolverLocalBestDualBoundValue());
      spdlog::info(ss.str());
      std::cout << ss.str() << std::endl;
    }
  }
  else
  {
    // Solver is not in racing phase
    double solverDualBoundGain = 0.0;
    double sum = 0.0;

    // Check if the total number of solved nodes for source in the solver pool
    // and from the solver state differ, and if source is not the root node.
    // If so, update the average dual bound gain with the received value
    if (pLM->pSolverPool->getNumNodesSolved(source) == 0 &&
        solverState->getNumNodesSolved() > 0 &&
        !(pLM->pSolverPool->getCurrentNodeFromSolver(source)->isRootNode()))
    {
      // Calculate the gain in dual bound as the difference between the bound for that
      // node (solver state/source node) and the bound for that same node from the solver pool
      solverDualBoundGain =
          solverState->getSolverLocalBestDualBoundValue() -
          pLM->pSolverPool->getCurrentNodeFromSolver(source)->getDualBoundValue();

      // Store the dual bound gain and sum up all the "last several" gains
      pLM->pLastSeveralDualBoundGains.push_back(solverDualBoundGain);
      for (auto gain : (pLM->pLastSeveralDualBoundGains))
      {
        sum += gain;
      }

      // Compute the average in dual bound gains
      pLM->pAverageLastSeveralDualBoundGains = sum / (pLM->pLastSeveralDualBoundGains).size();

      // Update the average dual bound gain (moving average)
      const double numNodesAvgBoundGain = static_cast<double>(pLM->pNumAverageDualBoundGain);
      pLM->pAverageDualBoundGain =
              pLM->pAverageDualBoundGain * (numNodesAvgBoundGain/(numNodesAvgBoundGain + 1.0)) +
              solverDualBoundGain * (1.0/(numNodesAvgBoundGain + 1.0));

      // Pop back from the last several dual bound gains if the queue is too long
      if ((pLM->pLastSeveralDualBoundGains).size() > kMaxNumDualBoundGainsInQueue)
      {
        // Pop oldest bound gain
        pLM->pLastSeveralDualBoundGains.pop_front();
      }

      // Increase the counter of average dual bound gain to prepare for next average
      pLM->pNumAverageDualBoundGain++;
    }

    // Update the solver pool with the new solver status from the solver state received
    // as input to this callback function
    pLM->pSolverPool->updateSolverStatus(
            source,
            solverState->getNumNodesSolved(),
            solverState->getNumNodesLeft(),
            solverState->getSolverLocalBestDualBoundValue(),
            pLM->pNodePool);

    if (getParams().generatereducedcheckpointfiles())
    {
      globalBestDualBoundValueLocal = globalBestDualBoundValue = std::min(
              std::min(std::min(getNodePoolBestDualBound(),
                                pLM->pNodePoolBufferToGenerateCPF->getBestDualBoundValue()),
                       getSolverPoolBestDualBound()),
              pLM->pLMTerminationState.globalBestDualBoundValue);
    }
    else
    {
      globalBestDualBoundValueLocal = globalBestDualBoundValue = std::max(
              std::min(std::min(getSolverPoolBestDualBound(), getNodePoolBestDualBound()),
                       pLM->pMinimalDualBoundNormalTermSolvers),
             pLM->pLMTerminationState.globalBestDualBoundValue);
    }

    // Get the node currently being solved by the source solver
    auto node = pLM->pSolverPool->getCurrentNodeFromSolver(source);
    if (node->getMergeNodeInfo() && solverState->getNumNodesSolved() > 2 )
    {
      // If the node has information about merging and the number of nodes solved
      // by the solver is greater than 2, then merge this node
      pLM->pLoadManagerMergeImpl->mergeNodes(node);
      if (getParams().generatereducedcheckpointfiles())
      {
        assert(!node->getMergeNodeInfo());
        pLM->pNodePoolBufferToGenerateCPF->insert(node);
      }
    }
  }

  // Check whether or not to distribute the best primal solution to solvers
  if (getParams().distributebestprimalsolution())
  {
    auto bestSol = pLM->pInitiator->getGlobalBestIncumbentSolution();
    if (bestSol &&
        bestSol->getObjectiveFuntionValue() < solverState->getGlobalBestPrimalBoundValue())
    {
      // The best solution/primal is better than what the solver has,
      // communicate the updated primal
      bestSol->send(getFramework(), source);
    }
  }

  if (getParams().checkgapinlm())
  {
    if (pLM->pInitiator->getAbsGap(globalBestDualBoundValue) < pLM->pInitiator->getAbsGapValue() ||
            pLM->pInitiator->getGap(globalBestDualBoundValue) < pLM->pInitiator->getGapValue())
    {
      for (int idx = 1; idx < pLM->pSolverPool->getNumSolvers(); ++idx)
      {
        utils::sendTagMessageToNetworkNode(
                getFramework(),
                net::MetaBBPacketTag::PacketTagType::PPT_GIVEN_GAP_IS_REACHED,
                idx);
      }
    }
    else
    {
      if (pLM->pInitiator->getAbsGap(solverState->getSolverLocalBestDualBoundValue()) <
              pLM->pInitiator->getAbsGapValue() ||
              pLM->pInitiator->getGap(solverState->getSolverLocalBestDualBoundValue()) <
              pLM->pInitiator->getGapValue())
      {
        utils::sendTagMessageToNetworkNode(
                getFramework(),
                net::MetaBBPacketTag::PacketTagType::PPT_GIVEN_GAP_IS_REACHED,
                source);
      }
    }
  }

  // Send the updated best dual bound from the node pool to the source solver
  const auto lmBestDualBoundValue = getNodePoolBestDualBound();
  utils::sendDataMessageToNetworkNode<double>(
          getFramework(),
          net::MetaBBPacketTag::PacketTagType::PPT_LM_BEST_BOUND_VALUE,
          source,
          lmBestDualBoundValue);

  // Send notification id (event received) back to sender
  const auto notificationId = solverState->getNotificationId();
  utils::sendDataMessageToNetworkNode<int>(
          getFramework(),
          net::MetaBBPacketTag::PacketTagType::PPT_NOTIFICATION_ID,
          source,
          notificationId);

  // Check if initial nodes run is required and if the number of nodes is greater
  // than the number of nodes in the node pool and in the solvers.
  // If so, stop generating initial nodes
  if (getParams().initialnodesgeneration() &&
      static_cast<long long>(
              getSolverPool()->getNumNodesInAllSolvers() + getNodePool()->getNumNodes()) >=
              getParams().numinitialnodes())
  {
    for (int idx = 1; idx <= getSolverPool()->getNumSolvers(); idx++)
    {
      // Communicate to all (active) solvers to stop collecting nodes
      int numCollect = -1;
      if (getSolverPool()->isSolverActive(idx))
      {
        utils::sendDataMessageToNetworkNode<int>(
                getFramework(),
                net::MetaBBPacketTag::PacketTagType::PPT_COLLECT_ALL_NODES,
                idx,
                numCollect);
      }
    }

    // Initial nodes are generated
    pLM->pInitialNodesGenerated = true;
  }
  else
  {
    if (!isRampUpPhase())
    {
      if (getNumGoodNodes(globalBestDualBoundValueLocal) > 0)
      {
        // Enough good nodes
        pLM->pEmptyNodePoolTimeMsec = std::numeric_limits<uint64_t>::max();
        pLM->pIsCollectingModeRestarted = false;
      }
      else
      {
        // Node pool doesn't have good nodes.
        // Collecting mode must be restarted
        if (getSolverPool()->getNumInactiveSolvers() > 0)
        {
          // There are some inactive solvers and collecting mode is not restarted yet
          if (!pLM->pIsCollectingModeRestarted)
          {
            switchSolversToCollectingMode();

            if (!getParams().quiet())
            {
              std::stringstream ss;
              ss << "LoadManager::processTagSolverState - collecting mode is restarted with " <<
                    getSolverPool()->getNumLimitCollectingModeSolvers() << " at time (msec.) " <<
                    pLM->pTimer->getWallClockTimeMsec();
              spdlog::info(ss.str());
              std::cout << ss.str() << std::endl;
            }

            // Collecting mode is restarted
            if (getSolverPool()->isInCollectingMode()) pLM->pIsCollectingModeRestarted = true;
          }
        }
        else
        {
          // Here there are no inactive solvers
          if (pLM->pIsCollectingModeRestarted)
          {
            // Collecting mode is already restarted
            pLM->pEmptyNodePoolTimeMsec = std::numeric_limits<uint64_t>::max();
            pLM->pIsCollectingModeRestarted = false;
          }
        }

        const auto timeNow = pLM->pTimer->getWallClockTimeMsec();
        if ((timeNow - pLM->pEmptyNodePoolTimeMsec) < 0)
        {
          pLM->pEmptyNodePoolTimeMsec = timeNow;
        }
      }

      if (pLM->pTimer->getWallClockTimeMsec() - pLM->pEmptyNodePoolTimeMsec >
        getParams().timetoincreasecmsmsec())
      {
        // Check if the node pool is empty and there can be more solvers swithing to
        // collecting mode
        bool increaseNumSolversInCollectingMode = (getNodePool()->getNumNodes() == 0);
        switchSolversToCollectingMode(increaseNumSolversInCollectingMode);

        if (getSolverPool()->isInCollectingMode())
        {
          pLM->pIsCollectingModeRestarted = true;
          pLM->pEmptyNodePoolTimeMsec = std::numeric_limits<uint64_t>::max();
        }
      }
    }  // pRunningPhase != RunningPhase::RP_RAMPUP

    // Check if the solver is racing, the phase is not ramp-up and there are not enough
    // nodes in the node pool
    int numToSwith = getParams().numnodestoswitchtocollectingmode();
    double numToSwithMul = getParams().multiplierforcollectingmode();
    if ((!solverState->isRacingStage()) &&
        pLM->pRunningPhase != LoadManager::RunningPhase::LMRP_RAMPUP &&
        !(getSolverPool()->isInCollectingMode()) &&
        (getNodePool()->getNumOfGoodNodes(globalBestDualBoundValueLocal) <
                numToSwith * numToSwithMul))
    {
      // Switch to collecting mode
      getSolverPool()->switchIntoCollectingMode(getNodePool());
      if (pLM->pFirstCollectingModeState == kFirstCollectingModeNotBeingInCollecting &&
              getSolverPool()->isInCollectingMode())
      {
        // First time in collecting mode
        pLM->pFirstCollectingModeState = kFirstCollectingModeInCollectingModeOnce;
      }
    }

    if (!pLM->pIsBreakingFinished)
    {
      // Breaking is not finished.
      // Check if the solver is racing and running phase is normal
      if (!solverState->isRacingStage() &&
              pLM->pRunningPhase == LoadManager::RunningPhase::LMRP_RUNNING)
      {
        const auto numNodesToStopBreaking = getParams().numstopbreaking();
        if (getNodePool()->getNumNodes() > numNodesToStopBreaking ||
            getSolverPool()->getNumNodesInSolvers() < numNodesToStopBreaking)
        {
          // Finish breaking
          pLM->pIsBreakingFinished = true;
        }
        else
        {
          if (pLM->pBreakingSolverId == -1)
          {
            if (getSolverPool()->getNumOfNodesLeftInBestSolver() >
                 getParams().numsolvernodesstartbreaking())
            {
              pLM->pBreakingSolverId = getSolverPool()->getBestSolver();
              assert(pLM->pBreakingSolverId != -1);

              // Use the best solver as breaking solver and send the information to that solver
              double targetBound = (getSolverPool()->getGlobalBestDualBoundValue() *
                      getParams().multiplierforbreakingtargetbound());
              double numLimitTransfer = static_cast<double>(
                      getParams().numtransferlimitforbreaking());

              std::vector<double> breakingData{targetBound, numLimitTransfer};
              utils::sendListDataMessageToNetworkNode<double>(
                      getFramework(),
                      net::MetaBBPacketTag::PacketTagType::PPT_BREAKING,
                      pLM->pBreakingSolverId,
                      breakingData);
            }
            else
            {
              if(((getSolverPool()->getGlobalBestDualBoundValue() +
                  getParams().abgapforswitchingtobestsolver() * 3) >
                  solverState->getSolverLocalBestDualBoundValue()) &&
                  getSolverPool()->getNumNodesInAllSolvers() >
                  std::max(static_cast<int>(getSolverPool()->getNumSolvers() * 2),
                           getParams().numstopbreaking() * 2) &&
                  solverState->getNumNodesLeft() >
                  (getSolverPool()->getNumNodesInAllSolvers() * 0.5))
              {
                pLM->pBreakingSolverId = source;
                double targetBound = (solverState->getSolverLocalBestDualBoundValue() *
                    getParams().multiplierforbreakingtargetbound());
                double numLimitTransfer = static_cast<double>(
                        getParams().numtransferlimitforbreaking());

                std::vector<double> breakingData{targetBound, numLimitTransfer};
                utils::sendListDataMessageToNetworkNode<double>(
                        getFramework(),
                        net::MetaBBPacketTag::PacketTagType::PPT_BREAKING,
                        pLM->pBreakingSolverId,
                        breakingData);
              }
            }
          }
        }
      }
    }
    else
    {
      // breaking is finished
      if (pLM->pRunningPhase == LoadManager::RunningPhase::LMRP_RUNNING &&
          getNodePool()->getNumNodes() < getParams().numtransferlimitforbreaking() &&
          getSolverPool()->getNumNodesInAllSolvers() >
          std::max(getParams().numtransferlimitforbreaking() * 2,
                   static_cast<int>(getSolverPool()->getNumSolvers() * 2)))
      {
        // Break again.
        // Several solvers can be in breaking situation.
        // That is, braking message can be sent to breaking solver
        pLM->pIsBreakingFinished = false;
        pLM->pBreakingSolverId = -1;
      }
    }
  }

  pLM->pLMTerminationState.globalBestDualBoundValue = std::max(
          globalBestDualBoundValue, pLM->pLMTerminationState.globalBestDualBoundValue);

  pLM->pLMTerminationState.externalGlobalBestDualBoundValue =
          pLM->pInitiator->convertToExternalValue(globalBestDualBoundValue);

  // Delete the solver state
  solverState.reset();

  return kErrorNoError;
}  // processTagSolverState

int LoadManagerImpl::processTagCompletionOfCalculation(int source, optnet::Packet::UPtr packet)
{
  // Send the state of the solver to LM
  spdlog::info("LoadManager::processTagCompletionOfCalculation - source " + std::to_string(source));
  logTime("LoadManager (processTagCompletionOfCalculation)");

  // Build a new solver state and receive that from the given source
  auto calcState = getFramework()->buildCalculationState();
  calcState->upload(std::move(packet));

  // Check if all winner solver nodes are collected and if the source node
  // is the winner node of the racing
  if ((!pLM->pWinnerSolverNodesCollected) && pLM->pRacingWinner == source)
  {
    // Winner solver nodes are now going to be collected from the winner source node
    pLM->pWinnerSolverNodesCollected = true;
    if (pLM->pMerging)
    {
      // Merging is in process.
      // Merge anyways, since merge nodes candidates have to be generated
      // even if running with InitialNodesGeneration
      pLM->pLoadManagerMergeImpl->generateMergeNodesCandidates();
      pLM->pMerging = false;
    }

    // Check if initial nodes have been generated
    if (getParams().initialnodesgeneration() &&
        getNodePool()->getNumNodes() >= getParams().numinitialnodes())
    {
      pLM->pInitialNodesGenerated = true;
    }
  }

  solver::TerminationState termState = calcState->getTerminationState();
  if (!getParams().quiet())
  {
    std::stringstream ss;
    ss << "LoadManagerImpl::processTagCompletionOfCalculation - termination time (msec.) " <<
            pLM->pTimer->getWallClockTimeMsec() << " for solver " << source <<
            " with value " << pLM->pInitiator->convertToExternalValue(
                    calcState->getDualBoundValue());
    spdlog::info(ss.str());
    std::cout << ss.str() << std::endl;
  }

  const double dualBoundVal = pLM->pInitiator->convertToExternalValue(
          calcState->getDualBoundValue());
  switch(termState)
  {
    case solver::TerminationState::TS_TERMINATED_NORMALLY:
    {
      std::stringstream ss;
      ss << "LoadManager::processTagCompletionOfCalculation - " <<
              "normal termination " << dualBoundVal;
      spdlog::info(ss.str());

      Node::SPtr node = getSolverPool()->getCurrentNodeFromSolver(source);
      if (node->getMergeNodeInfo())
      {
        node->setDualBoundValue(dualBoundVal);
        pLM->pLoadManagerMergeImpl->mergeNodes(node);
      }

      // Check if there is racing for the source
      if ((!pLM->pRacingSolverPool) || (!pLM->pRacingSolverPool->isSolverActive(source)))
      {
        if (pLM->pNodePoolToRestart)
        {
          auto solvingNode = getSolverPool()->extractCurrentNodeAndDeactivate(
                  source, getNodePool());
          if (solvingNode->getParent())
          {
            solvingNode.reset();
          }
          else
          {
            // To stand a safety side about timing issue.
            // Two branch nodes may be removed
            pLM->pNodePoolToRestart->insert(solvingNode);
          }
          return kErrorNoError;
        }

        // Deactivate the solver from the solver pool
        if (getSolverPool()->isSolverInCollectingMode(source))
        {
          getSolverPool()->deactivateSolver(source, calcState->getNumNodesSolved(), getNodePool());
          if (!pLM->pNodePoolBufferToRestart)
          {
            switchSolversToCollectingMode();
          }
        }
        else
        {
          getSolverPool()->deactivateSolver(source, calcState->getNumNodesSolved(), getNodePool());
        }
        getSolverPool()->addTotalNodesSolved(calcState->getNumNodesSolved());
      }

      if (!EPSEQ(calcState->getDualBoundValue(), metaconst::MIN_DOUBLE,
                 pLM->pInitiator->getEpsilon()))
      {
        pLM->pAllCompInfeasibleAfterSolution = false;
        if (EPSLE(pLM->pLMTerminationState.globalBestDualBoundValue,
                  calcState->getDualBoundValue(),
                  pLM->pInitiator->getEpsilon()) &&
                pLM->pMinimalDualBoundNormalTermSolvers > calcState->getDualBoundValue())
        {
          if (pLM->pInitiator->getGlobalBestIncumbentSolution())
          {
            pLM->pMinimalDualBoundNormalTermSolvers = std::min(
                    pLM->pInitiator->getGlobalBestIncumbentSolution()->getObjectiveFuntionValue(),
                    calcState->getDualBoundValue());
          }
          else
          {
            pLM->pMinimalDualBoundNormalTermSolvers = calcState->getDualBoundValue();
          }
        }
      }
      break;
    }
    case solver::TerminationState::TS_INTERRUPTED_IN_RACING_STAGE:
    {
      // Do not send a node here.
      // Send node after solver termination state is received and
      // racing solver is deactivated.
      // Do not have to update counters of SolverPool
      std::stringstream ss;
      ss << "LoadManager::processTagCompletionOfCalculation - " <<
              "interrupted in racing stage " << dualBoundVal;
      spdlog::info(ss.str());
      break;
    }
    case solver::TerminationState::TS_TERMINATED_BY_ANOTHER_NODE:
    {
      std::stringstream ss;
      ss << "LoadManager::processTagCompletionOfCalculation - " <<
              "terminated by another node " << dualBoundVal;
      spdlog::info(ss.str());

      /// In this case the following two numbers should be different
      /// # Total > # Solved
      getSolverPool()->addNumNodesSolved(calcState->getNumNodesSolved());
      getSolverPool()->addTotalNodesSolved(calcState->getNumNodesSolved());
      break;
    }
    case solver::TerminationState::TS_TERMINATED_BY_INTERRUPT_REQUEST:
    {
      std::stringstream ss;
      ss << "LoadManager::processTagCompletionOfCalculation - " <<
              "interrupt requested  " << dualBoundVal;
      spdlog::info(ss.str());

      if ((!pLM->pRacingSolverPool) || (!pLM->pRacingSolverPool->isSolverActive(source)))
      {
        // RacingSolverPool entry is deactivated when it receives
        // SolverTerminationState message
        auto solvingNode = getSolverPool()->extractCurrentNodeAndInactivate(source, getNodePool());
        if (pLM->pNodePoolToRestart)
        {
          if (solvingNode->getParent())
          {
            solvingNode.reset();
          }
          else
          {
            pLM->pNodePoolToRestart->insert(solvingNode);
          }
        }
        else
        {
          getNodePool()->insert(solvingNode);
        }
      }

      // @note no update to
      // pLM->pLMTerminationState.globalBestDualBoundValue, and
      // pLM->pLMTerminationState.externalGlobalBestDualBoundValue,
      // just use SolverState update
      break;
    }
    case solver::TerminationState::TS_TERMINATED_IN_RACING_STAGE:
    {
      std::stringstream ss;
      ss << "LoadManager::processTagCompletionOfCalculation - " <<
              "interrupt in racing stage " << dualBoundVal;
      spdlog::info(ss.str());

      pLM->pRacingTermination = true;
      pLM->pNumNodesSolvedAtRacingTermination = calcState->getNumNodesSolved();

      if (!EPSEQ(calcState->getDualBoundValue(), metaconst::MIN_DOUBLE,
                 pLM->pInitiator->getEpsilon()) &&
          EPSEQ(pLM->pMinimalDualBoundNormalTermSolvers, metaconst::MAX_DOUBLE,
                pLM->pInitiator->getEpsilon()))
      {
        if (pLM->pInitiator->getGlobalBestIncumbentSolution())
        {
          pLM->pMinimalDualBoundNormalTermSolvers = std::min(
                  pLM->pInitiator->getGlobalBestIncumbentSolution()->getObjectiveFuntionValue(),
                  calcState->getDualBoundValue());
        }
        else
        {
          pLM->pMinimalDualBoundNormalTermSolvers = calcState->getDualBoundValue();
        }
      }

      if (EPSLE(pLM->pLMTerminationState.globalBestDualBoundValue, calcState->getDualBoundValue(),
                pLM->pInitiator->getEpsilon()) &&
              pLM->pMinimalDualBoundNormalTermSolvers < calcState->getDualBoundValue())
      {
        if (pLM->pInitiator->getGlobalBestIncumbentSolution())
        {
          pLM->pMinimalDualBoundNormalTermSolvers = std::min(
                  pLM->pInitiator->getGlobalBestIncumbentSolution()->getObjectiveFuntionValue(),
                  calcState->getDualBoundValue());        }
        else
        {
          pLM->pMinimalDualBoundNormalTermSolvers = calcState->getDualBoundValue();
        }
      }

      if (pLM->pInitiator->getGlobalBestIncumbentSolution() &&
          (EPSEQ(calcState->getDualBoundValue(),
                 pLM-> pInitiator->getGlobalBestIncumbentSolution()->getObjectiveFuntionValue(),
                 pLM->pInitiator->getEpsilon()) ||
                  EPSEQ(calcState->getDualBoundValue(), metaconst::MIN_DOUBLE,
                        pLM->pInitiator->getEpsilon())))
      {
        // Update global best dual bound
        pLM->pLMTerminationState.globalBestDualBoundValue =
                pLM->pInitiator->getGlobalBestIncumbentSolution()->getObjectiveFuntionValue();

        // Update the external global best dual bound
        pLM->pLMTerminationState.externalGlobalBestDualBoundValue =
                pLM->pInitiator->convertToExternalValue(
                        pLM->pLMTerminationState.globalBestDualBoundValue);
      }
      break;
    }
    case solver::TerminationState::TS_INTERRUPTED_IN_MERGING:
    {
      std::stringstream ss;
      ss << "LoadManager::processTagCompletionOfCalculation - " <<
              "interrupted in merging " << dualBoundVal;
      spdlog::info(ss.str());

      auto solvingNode = getSolverPool()->extractCurrentNodeAndInactivate(source, getNodePool());
      assert(solvingNode->getMergeNodeInfo());
      pLM->pLoadManagerMergeImpl->regenerateMergeNodesCandidates(solvingNode);
      getNodePool()->insert(solvingNode);
      break;
    }
    case solver::TerminationState::TS_TERMINATED_BY_TIME_LIMIT:
    {
      std::stringstream ss;
      ss << "LoadManager::processTagCompletionOfCalculation - " <<
              "erminated by time limit" << dualBoundVal;
      spdlog::info(ss.str());
      if ((!pLM->pRacingSolverPool) || (!pLM->pRacingSolverPool->isSolverActive(source)))
      {
        // RacingSolverPool entry is deactivated when it receives
        // SolverTerminationState message
        auto solvingNode = getSolverPool()->extractCurrentNodeAndInactivate(source, getNodePool());
        if (pLM->pNodePoolToRestart)
        {
          if (solvingNode->getParent())
          {
            solvingNode.reset();
          }
          else
          {
            pLM->pNodePoolToRestart->insert(solvingNode);
          }
        }
        else
        {
          getNodePool()->insert(solvingNode);
        }
      }
      break;
    }
    default:
      throw std::runtime_error("LoadManager::processTagCompletionOfCalculation - "
          "invalid termination state");
  }  // switch

  if (calcState->getTerminationState() == solver::TerminationState::TS_TERMINATED_BY_TIME_LIMIT)
  {
    std::stringstream ss;
    ss << "###### Solver " << getComm()->getRank() << " terminated on timeout";
    if (!getParams().quiet())
    {
      spdlog::info(ss.str());
    }
    std::cout << ss.str() << std::endl;
  }
  calcState.reset();

  // ============== RECURSIVE RECEIVE ==============
  // Wait for any incoming message from any source
  optnet::Packet::UPtr termPacket(new optnet::Packet());
  termPacket->packetTag = net::MetaBBPacketTag::UPtr(new net::MetaBBPacketTag(
          net::MetaBBPacketTag::PacketTagType::PPT_TERMINATED));
  ASSERT_COMM_CALL(getFramework()->getComm()->receive(termPacket, source));
  // ===============================================

  auto tstate = getFramework()->buildSolverTerminationState();
  tstate->upload(std::move(termPacket));

  // Check how the solver was terminated
  switch (tstate->getInterruptedMode())
  {
    case solver::InterruptionPoint::IP_INTERRUPTED_CHECKPOINT:
    {
      // Check-point termination, normal termination.
      // Save termination status to check point file,
      // keep this information to solver pool
      getSolverPool()->setTermState(source, tstate);

      // Do not delete tstate!
      // It is saved in SolverPool
      if (pLM->pRunningPhase != LoadManager::RunningPhase::LMRP_TERMINATION &&
              pLM->pNodePoolToRestart &&
              termState != solver::TerminationState::TS_TERMINATED_BY_TIME_LIMIT)
      {
        if (getNodePool()->isEmpty())
        {
          pLM->pLMTerminationState.numNodesFailedToSendBack++;
          if (pLM->pRunningPhase != LoadManager::RunningPhase::LMRP_RAMPUP &&
              !(getSolverPool()->isInCollectingMode()))
          {
            getSolverPool()->switchIntoCollectingMode(getNodePool());
            if (pLM->pFirstCollectingModeState == kFirstCollectingModeNotBeingInCollecting &&
                    getSolverPool()->isInCollectingMode())
              {
                pLM->pFirstCollectingModeState = kFirstCollectingModeInCollectingModeOnce;
              }
          }
        }
        else
        {
          if (pLM->sendNodesToIdleSolvers())
          {
            pLM->pLMTerminationState.numNodesSentImmediately++;
          }
        }
      }
      break;
    }
    case solver::InterruptionPoint::IP_INTERRUPTED_RACING_RAMPUP:
    {
      pLM->deactivateRacingSolverPool(source);
      if (pLM->pRunningPhase != LoadManager::RunningPhase::LMRP_TERMINATION &&
          termState != solver::TerminationState::TS_TERMINATED_BY_TIME_LIMIT)
      {
        if (getNodePool()->isEmpty())
        {
          pLM->pLMTerminationState.numNodesFailedToSendBack++;
          if (pLM->pRunningPhase != LoadManager::RunningPhase::LMRP_RAMPUP &&
              !(getSolverPool()->isInCollectingMode()))
          {
            getSolverPool()->switchIntoCollectingMode(getNodePool());
            if (pLM->pFirstCollectingModeState == kFirstCollectingModeNotBeingInCollecting &&
                    getSolverPool()->isInCollectingMode())
              {
                pLM->pFirstCollectingModeState = kFirstCollectingModeInCollectingModeOnce;
              }
          }
        }
        else
        {
          if (pLM->sendNodesToIdleSolvers())
          {
            pLM->pLMTerminationState.numNodesSentImmediately++;
          }
        }
      }
      tstate.reset();
      break;
    }
    default:
      throw std::runtime_error("LoadManager::processTagCompletionOfCalculatio - "
              "invalid interruption mode");
  }  // switch

  if (getParams().quiet() && pLM->pRacingTermination)
  {
    // Do not wait for statistical information from other solvers
    pLM->pNumTerminated = 1;
    pLM->tearDown();
    return kErrorNoError;
  }

  if (source == pLM->pBreakingSolverId)
  {
    pLM->pBreakingSolverId = -1;
    pLM->pIsBreakingFinished = false;
  }

  return kErrorNoError;
}  // processTagCompletionOfCalculation

int LoadManagerImpl::processTagAnotherNodeRequest(int source, optnet::Packet::UPtr packet)
{
  // The packet should contain the double best dual bound value
  auto dpacket = optnet::castBaseToNetworkPacket<double>(std::move(packet->networkPacket));
  if (dpacket->size() != 1)
  {
    const std::string errMsg = "LoadManagerImpl - processTagAnotherNodeRequest: "
        "invalid data size, expected 1, received " + std::to_string(dpacket->size());
    spdlog::error(errMsg);
    return metaconst::ERR_GENERIC_ERROR;
  }

  const auto bestDualBoundValue = dpacket->data[0];
  if (getNodePool()->isEmpty() || getSolverPool()->currentSolvingNodeHasChildren(source))
  {
    utils::sendTagMessageToNetworkNode(
            getFramework(),
            net::MetaBBPacketTag::PacketTagType::PPT_NO_NODES,
            source);
    pLM->pLMTerminationState.numNodesFailedToSendBack++;
  }
  else
  {
    Node::SPtr node;
    while(!getNodePool()->isEmpty())
    {
      node = getNodePool()->extractNode();

      // Break asap if no node is available in the node pool
      if (node == nullptr) break;
      if (!(pLM->pInitiator->getGlobalBestIncumbentSolution()) ||
              (node->getDualBoundValue() <
                      pLM->pInitiator->getGlobalBestIncumbentSolution()->
                      getObjectiveFuntionValue()))
      {
        // Found a candidate node
        break;
      }
      else
      {
        node.reset();
        pLM->pLMTerminationState.numNodesDeletedInLM++;
      }
    }  // while

    if (node)
    {
      const auto diffDualBound =
              getSolverPool()->getDualBoundValue(source) - node->getDualBoundValue();
      if (diffDualBound > 0.0 &&
              REALABS((diffDualBound / std::max(std::abs(node->getDualBoundValue()), 1.0))) >
      getParams().bgapstopsolvingmode())
      {
        getSolverPool()->sendSwitchOutCollectingModeIfNecessary(source);
        auto solvingNode = getSolverPool()->extractCurrentNodeAndDeactivate(source, getNodePool());
        solvingNode->setDualBoundValue(bestDualBoundValue);
        solvingNode->setInitialDualBoundValue(bestDualBoundValue);
        getNodePool()->insert(solvingNode);
        if (solvingNode->getMergeNodeInfo())
        {
          pLM->pLoadManagerMergeImpl->mergeNodes(solvingNode);
        }

        double globalBestDualBoundValueLocal = std::max(
                std::min(getSolverPool()->getGlobalBestDualBoundValue(),
                         getNodePool()->getBestDualBoundValue()),
                         pLM->pLMTerminationState.globalBestDualBoundValue);
        getSolverPool()->activateSolver(source, node);

        pLM->pLMTerminationState.numNodesSent++;
        pLM->pLMTerminationState.numNodesSentImmediatelyAnotherNode++;
      }
      else
      {
        getNodePool()->insert(node);
        utils::sendTagMessageToNetworkNode(
                getFramework(),
                net::MetaBBPacketTag::PacketTagType::PPT_NO_NODES,
                source);
        pLM->pLMTerminationState.numNodesFailedToSendBack++;
      }
    }
    else
    {
      // No node available
      utils::sendTagMessageToNetworkNode(
              getFramework(),
              net::MetaBBPacketTag::PacketTagType::PPT_NO_NODES,
              source);
      pLM->pLMTerminationState.numNodesFailedToSendBack++;
    }
  }

  return kErrorNoError;
}  // processTagAnotherNodeRequest

int LoadManagerImpl::processTagTerminated(int source, optnet::Packet::UPtr packet)
{
  auto solverTermState = getFramework()->buildSolverTerminationState();
  solverTermState->upload(std::move(packet));

  if (!pLM->pRacingTermination &&
          solverTermState->getInterruptedMode() == solver::InterruptionPoint::IP_INTERRUPTED)
  {
    pLM->pComputationIsInterrupted = true;
  }

  pLM->pNumTerminated++;
  solverTermState.reset();
  return kErrorNoError;
}  // processTagTerminated

int LoadManagerImpl::processTagHardTimeLimit(int source, optnet::Packet::UPtr)
{
  pLM->pHardTimeLimitIsReached = true;
  return kErrorNoError;
}  // processTagHardTimeLimit

int LoadManagerImpl::processTagStatistics(int source, optnet::Packet::UPtr packet)
{
  auto initialStat = getFramework()->buildInitialStat();
  initialStat->upload(std::move(packet));
  if (pLM->pMaxDepthInWinnerSolverNodes < initialStat->getMaxDepth())
  {
    pLM->pMaxDepthInWinnerSolverNodes = initialStat->getMaxDepth();
  }

  pLM->pInitiator->accumulateInitialStat(initialStat);
  initialStat.reset();

  return kErrorNoError;
}  // processTagStatistics

int LoadManagerImpl::processTagToken(int, optnet::Packet::UPtr)
{
  return kErrorNoError;
}  // processTagToken

int LoadManagerImpl::processTagAllowToBeInCollectingMode(int source, optnet::Packet::UPtr)
{
  getSolverPool()->setCollectingIsAllowed(source);
  return kErrorNoError;
}  // processTagAllowToBeInCollectingMode

int LoadManagerImpl::processTagLbBoundTightened(int source, optnet::Packet::UPtr packet)
{
  // The packet should contain the index and bound
  auto dpacket = optnet::castBaseToNetworkPacket<double>(std::move(packet->networkPacket));
  if (dpacket->size() != 2)
  {
    const std::string errMsg = "LoadManagerImpl - processTagLbBoundTightened: "
        "invalid data size, expected 2, received " + std::to_string(dpacket->size());
    spdlog::error(errMsg);
    return metaconst::ERR_GENERIC_ERROR;
  }

  const auto boundList = dpacket->data;
  if (EPSLT(pLM->pInitiator->getTightenedVarLB(static_cast<int>(boundList[0])),
            boundList[1], metaconst::MIN_EPSILON))
  {
    pLM->pInitiator->setTightenedVarLB(static_cast<int>(boundList[0]), boundList[1]);
    if (pLM->pRacingSolverPool && (pLM->pRacingSolverPool->getNumInactiveSolvers() == 0))
    {
      for (int idx = 1; idx <= pLM->pRacingSolverPool->getNumActiveSolvers(); idx++)
      {
        if (idx != source)
        {
          std::vector<double> boundsData{boundList[0], boundList[1]};
          utils::sendListDataMessageToNetworkNode<double>(
                  getFramework(),
                  net::MetaBBPacketTag::PacketTagType::PPT_LB_BOUND_TIGHTENED_INDEX,
                  idx,
                  boundsData);
         }
      }
    }
  }

  return kErrorNoError;
}  // processTagLbBoundTightened

int LoadManagerImpl::processTagUbBoundTightened(int source, optnet::Packet::UPtr packet)
{
  // The packet should contain the index and bound
  auto dpacket = optnet::castBaseToNetworkPacket<double>(std::move(packet->networkPacket));
  if (dpacket->size() != 2)
  {
    const std::string errMsg = "LoadManagerImpl - processTagLbBoundTightened: "
        "invalid data size, expected 2, received " + std::to_string(dpacket->size());
    spdlog::error(errMsg);
    return metaconst::ERR_GENERIC_ERROR;
  }

  const auto boundList = dpacket->data;
  if (EPSGT(pLM->pInitiator->getTightenedVarUB(static_cast<int>(boundList[0])),
            boundList[1], metaconst::MIN_EPSILON))
  {
    pLM->pInitiator->setTightenedVarUB(static_cast<int>(boundList[0]), boundList[1]);
    if (pLM->pRacingSolverPool && (pLM->pRacingSolverPool->getNumInactiveSolvers() == 0))
    {
      for (int idx = 1; idx <= pLM->pRacingSolverPool->getNumActiveSolvers(); idx++)
      {
        if (idx != source)
        {
          std::vector<double> boundsData{boundList[0], boundList[1]};
          utils::sendListDataMessageToNetworkNode<double>(
                  getFramework(),
                  net::MetaBBPacketTag::PacketTagType::PPT_UB_BOUND_TIGHTENED_INDEX,
                  idx,
                  boundsData);
         }
      }
    }
  }

  return kErrorNoError;
}  // processTagUbBoundTightened

int LoadManagerImpl::processRacingRampUpTagSolverState(int source, optnet::Packet::UPtr packet)
{
  auto solverState = getFramework()->buildSolverState();
  solverState->upload(std::move(packet));

  assert(solverState->isRacingStage());
  if (!pLM->pRestartRacing)
  {
    pLM->pRacingSolverPool->updateSolverStatus(
            source,
            solverState->getNumNodesSolved(),
            solverState->getNumNodesLeft(),
            solverState->getSolverLocalBestDualBoundValue());
    assert(pLM->pRacingSolverPool->getNumNodesLeft(source) == solverState->getNumNodesLeft() );
  }

  if (!getParams().noupperboundtransferinracing())
  {
    // The following should be before noticationId back to the source solver
    if (getParams().distributebestprimalsolution())
    {
      if (pLM->pInitiator->getGlobalBestIncumbentSolution() &&
              pLM->pInitiator->getGlobalBestIncumbentSolution()->getObjectiveFuntionValue() <
              solverState->getGlobalBestPrimalBoundValue())
      {
        pLM->pInitiator->getGlobalBestIncumbentSolution()->send(getFramework(), source);
      }
    }
  }

  if (getParams().checkgapinlm())
  {
    if (pLM->pInitiator->getAbsGap(solverState->getSolverLocalBestDualBoundValue()) <
            pLM->pInitiator->getAbsGapValue() ||
            pLM->pInitiator->getGap(solverState->getSolverLocalBestDualBoundValue()) <
            pLM->pInitiator->getGapValue()
          )
    {
      utils::sendTagMessageToNetworkNode(
              getFramework(),
              net::MetaBBPacketTag::PacketTagType::PPT_GIVEN_GAP_IS_REACHED,
              source);
    }
  }

  const auto lmBestDualBoundValue = pLM->pRacingSolverPool->getGlobalBestDualBoundValue();
  utils::sendDataMessageToNetworkNode<double>(
          getFramework(),
          net::MetaBBPacketTag::PacketTagType::PPT_LM_BEST_BOUND_VALUE,
          source,
          lmBestDualBoundValue);

  const auto notificationId = solverState->getNotificationId();
  utils::sendDataMessageToNetworkNode<int>(
          getFramework(),
          net::MetaBBPacketTag::PacketTagType::PPT_NOTIFICATION_ID,
          source,
          notificationId);

  if (pLM->pLMTerminationState.globalBestDualBoundValue < lmBestDualBoundValue)
  {
    pLM->pLMTerminationState.globalBestDualBoundValue = lmBestDualBoundValue;
    pLM->pLMTerminationState.externalGlobalBestDualBoundValue =
            pLM->pInitiator->convertToExternalValue(lmBestDualBoundValue);
  }

  solverState.reset();

  return kErrorNoError;
}  // processRacingRampUpTagSolverState

int LoadManagerImpl::processRacingRampUpTagCompletionOfCalculation(int source,
                                                                   optnet::Packet::UPtr packet)
{
  auto calcState = getFramework()->buildCalculationState();
  calcState->upload(std::move(packet));

  if (!getParams().quiet())
  {
    std::stringstream ss;
    ss << "LoadManagerImpl::processRacingRampUpTagCompletionOfCalculation - "
            "termination time (msec.) " << pLM->pTimer->getWallClockTimeMsec() <<
            " for solver " << source << " with value " <<
            pLM->pInitiator->convertToExternalValue(calcState->getDualBoundValue()) << " on ";
    switch(calcState->getTerminationState()) {
      case solver::TerminationState::TS_TERMINATED_BY_INTERRUPT_REQUEST:
      case solver::TerminationState::TS_INTERRUPTED_IN_RACING_STAGE:
      {
        ss << "interrupted by time limit or by some solver terminated in racing stage";
        break;
      }
      case solver::TerminationState::TS_TERMINATED_IN_RACING_STAGE:
      {
        ss << "terminated in racing stage";
        break;
      }
      case solver::TerminationState::TS_TERMINATED_BY_TIME_LIMIT:
      {
        ss << "interrupted by time limit";
        break;
      }
      default:
        throw std::runtime_error("LoadManagerImpl::processRacingRampUpTagCompletionOfCalculation - "
                "invalid termination state");
      }
    spdlog::info(ss.str());
    std::cout << ss.str() << std::endl;
  }

  switch(calcState->getTerminationState())
  {
    case solver::TerminationState::TS_TERMINATED_IN_RACING_STAGE:
    {
      pLM->pRacingTermination = true;
      pLM->pNumNodesSolvedAtRacingTermination = calcState->getNumNodesSolved();
      if (!EPSEQ(calcState->getDualBoundValue(), metaconst::MIN_DOUBLE,
                 pLM->pInitiator->getEpsilon()) &&
              EPSEQ(pLM->pMinimalDualBoundNormalTermSolvers, metaconst::MAX_DOUBLE,
                    pLM->pInitiator->getEpsilon()))
      {
        if (pLM->pInitiator->getGlobalBestIncumbentSolution())
        {
          pLM->pMinimalDualBoundNormalTermSolvers = std::min(
                  pLM->pInitiator->getGlobalBestIncumbentSolution()->getObjectiveFuntionValue(),
                  calcState->getDualBoundValue());
        }
        else
        {
          pLM->pMinimalDualBoundNormalTermSolvers = calcState->getDualBoundValue();
        }
      }

      if (EPSLE(pLM->pLMTerminationState.globalBestDualBoundValue, calcState->getDualBoundValue(),
                pLM->pInitiator->getEpsilon()) &&
              pLM->pMinimalDualBoundNormalTermSolvers < calcState->getDualBoundValue())
      {
        if (pLM->pInitiator->getGlobalBestIncumbentSolution())
        {
          pLM->pMinimalDualBoundNormalTermSolvers = std::min(
                  pLM->pInitiator->getGlobalBestIncumbentSolution()->getObjectiveFuntionValue(),
                  calcState->getDualBoundValue());
        }
        else
        {
          pLM->pMinimalDualBoundNormalTermSolvers = calcState->getDualBoundValue();
        }
      }

      const auto stateDualBound = calcState->getDualBoundValue();
      if (pLM->pInitiator->getGlobalBestIncumbentSolution() &&
              (EPSEQ(stateDualBound,
                     pLM->pInitiator->getGlobalBestIncumbentSolution()->getObjectiveFuntionValue(),
                     pLM->pInitiator->getEpsilon()) ||
                      EPSEQ(stateDualBound, metaconst::MIN_DOUBLE, pLM->pInitiator->getEpsilon()) ||
                      EPSEQ(calcState->getDualBoundValue(), metaconst::MAX_DOUBLE,
                            pLM->pInitiator->getEpsilon()) ||
                            (getParams().communicatetighterboundsinracing() &&
                            pLM->pInitiator->getGlobalBestIncumbentSolution()->
                            getObjectiveFuntionValue() < stateDualBound) ||
                            (calcState->getNumNodesSolved() == 0)))
      {
        pLM->pLMTerminationState.globalBestDualBoundValue =
                pLM->pInitiator->getGlobalBestIncumbentSolution()->getObjectiveFuntionValue();
        pLM->pLMTerminationState.externalGlobalBestDualBoundValue =
                pLM->pInitiator->convertToExternalValue(
                        pLM->pLMTerminationState.globalBestDualBoundValue);
        if (pLM->pInitiator->getGlobalBestIncumbentSolution())
        {
          pLM->pMinimalDualBoundNormalTermSolvers = std::min(
                  pLM->pInitiator->getGlobalBestIncumbentSolution()->getObjectiveFuntionValue(),
                  pLM->pLMTerminationState.globalBestDualBoundValue);
        }
        else
        {
          pLM->pMinimalDualBoundNormalTermSolvers =
                  pLM->pLMTerminationState.globalBestDualBoundValue;
        }
      }
      break;
    }
    case solver::TerminationState::TS_TERMINATED_BY_TIME_LIMIT:
    {
      std::cout << "####### Rank " << getFramework()->getComm()->getRank() <<
              " solver terminated with timelimit in solver side. #######" << std::endl;

      break;
    }
    default:
      break;
  }

  calcState.reset();

  // ============== RECURSIVE RECEIVE ==============
  // Wait for any incoming message from any source
  optnet::Packet::UPtr termPacket(new optnet::Packet());
  termPacket->packetTag = net::MetaBBPacketTag::UPtr(new net::MetaBBPacketTag(
          net::MetaBBPacketTag::PacketTagType::PPT_TERMINATED));
  ASSERT_COMM_CALL(getFramework()->getComm()->receive(termPacket, source));
  // ===============================================

  auto tstate = getFramework()->buildSolverTerminationState();
  tstate->upload(std::move(termPacket));
  tstate.reset();

  pLM->deactivateRacingSolverPool(source);
  if (getParams().quiet() && pLM->pRacingTermination)
  {
    pLM->pNumTerminated = 1;
    pLM->tearDown();
  }

  return kErrorNoError;
}  // processRacingRampUpTagCompletionOfCalculation

}  // namespace metabb
}  // namespace optilab
