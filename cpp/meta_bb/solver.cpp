#include "meta_bb/solver.hpp"

#include <algorithm>  // for std::max
#include <cassert>
#include <stdexcept>  // for std::invalid_argument
#include <utility>    // for std::move

#include <spdlog/spdlog.h>

#include "meta_bb/calculation_state.hpp"
#include "meta_bb/load_manager.hpp"
#include "meta_bb/meta_bb_macros.hpp"
#include "meta_bb/solver_impl.hpp"
#include "meta_bb/solver_state.hpp"
#include "meta_bb/solver_termination_state.hpp"
#include "meta_bb/tree_id.hpp"
#include "optimizer_network/network_packet.hpp"

namespace optilab {
namespace  metabb {

Solver::Solver()
{
}

Solver::Solver(Framework::SPtr framework, ParamSet::SPtr paramSet, Instance::SPtr instance,
               timer::Timer::SPtr timer)
: pFramework(framework),
  pParamSet(paramSet),
  pInstance(instance),
  pTimer(timer)
{
  if (!pFramework)
  {
    throw std::invalid_argument("Solver - empty pointer to the framework");
  }

  if (!pParamSet)
  {
    throw std::invalid_argument("Solver - empty pointer to the parameter set");
  }

  if (!pInstance)
  {
    throw std::invalid_argument("Solver - empty pointer to the instance");
  }

  // Set timer if not given in input
  if (!pTimer)
  {
    pTimer = std::make_shared<timer::Timer>();
  }

  // Set the network communicator
  pCommunicator = std::dynamic_pointer_cast<optnet::NetworkCommunicator>(pFramework->getComm());
  if (!pCommunicator)
  {
    throw std::runtime_error("Solver - invalid downcast from base network communicator "
            "to network communicator");
  }

  // Create the solver's implementation instance holding the callback functions
  // that are triggered upon receiving network messages
  pSolverImpl = std::make_shared<SolverImpl>(this);
}

Solver::~Solver()
{
  try
  {
    receiveMessages();
  }
  catch(...)
  {
    const std::string errMsg = "Solver - destructor: "
        "unknown error";
    spdlog::error(errMsg);
    return;
  }

  // Calculate the idle time
  const auto stopTime = pTimer->getWallClockTimeMsec();

  pIdleTimeAfterLastNodeMsec = stopTime - pPreviousStopTimeMsec;

  // Check if the solver has been interrupted or not
  solver::InterruptionPoint interrupted =
      (pTerminationMode == solver::TerminationMode::TM_INTERRUPTED_TERMINATION) ?
          solver::InterruptionPoint::IP_INTERRUPTED :
          solver::InterruptionPoint::IP_NOT_INTERRUPTED;

  double detTime = -1.0;

  // Create and send the solver's termination state to the root node
  auto solverTerminationState = pFramework->buildSolverTerminationState(
      interrupted,
      pCommunicator->getRank(),
      pTotalNumNodesSolved,
      pMinNumSolved,
      pMaxNumSolved,
      pTotalNumNodesSent,
      pTotalNumImprovedIncumbent,
      pNumNodesReceived,
      pNumNodesSolved,
      pNumNodesSolvedAtRoot,
      pNumNodesSolvedAtPreCheck,
      pNumTransferredLocalCuts,
      pMinTransferredLocalCuts,
      pMaxTransferredLocalCuts,
      pNumTotalRestarts,
      pMinRestarts,
      pMaxRestarts,
      pNumVarTightened,
      pNumIntVarTightened,
      stopTime,
      pIdleTimeToFirstNodeMsec,
      pIdleTimeBetweenNodesMsec,
      pIdleTimeAfterLastNodeMsec,
      pIdleTimeToWaitNotificationIdMsec,
      pIdleTimeToWaitAckCompletionMsec,
      pIdleTimeToWaitTokenMsec,
      pTotalRootNodeTimeMsec,
      pMinRootNodeTimeMsec,
      pMaxRootNodeTimeMsec);
  solverTerminationState->send(pFramework, 0);
}

bool Solver::isRacingRampUp() const noexcept
{
  const int val = (pParamSet->getDescriptor()).rampupphaseprocess();
  return (val == solver::RAMP_UP_PHASE_RACING) ||
      (val == solver::RAMP_UP_PHASE_REBUILD_TREE_AFTER_RACING);
}  // isRacingRampUp

bool Solver::isRacingStage() const noexcept
{
  const int val = (pParamSet->getDescriptor()).rampupphaseprocess();
  return (pRacingRampUpParamSet != nullptr) && ((val == solver::RAMP_UP_PHASE_RACING) ||
      (val == solver::RAMP_UP_PHASE_REBUILD_TREE_AFTER_RACING));
}  // isRacingStage

void Solver::updatePendingSolution()
{
  // Do not update while updating already
  if(pUpdatePendingSolutionIsProceeding) return;

  pUpdatePendingSolutionIsProceeding = true;
  if (!pPendingSolution)
  {
    updateGlobalBestIncumbentValue(pPendingIncumbentValue);
    pPendingIncumbentValue = std::numeric_limits<double>::max();
    pUpdatePendingSolutionIsProceeding = false;
    return;
  }

  if (updateGlobalBestIncumbentSolution(pPendingSolution))
  {
    tryNewSolution(pPendingSolution);
  }
  pPendingIncumbentValue = std::numeric_limits<double>::max();
  pPendingSolution.reset();
  pUpdatePendingSolutionIsProceeding = false;
}  // updatePendingSolution

void Solver::run(Node::SPtr node)
{
  pNode = node;
  run();
}  // run

void Solver::run(RacingRampUpParamSet::SPtr racingRampUpParamSet)
{
  // Build and receive the root node from the LoadManager
  auto rootNode = pFramework->buildNode();
  ASSERT_COMM_CALL(rootNode->broadcast(pFramework, LoadManager::GetLoadManagerRank()));

  pNumNodesReceived++;
  pRacingRampUpParamSet = racingRampUpParamSet;
  setRacingParams(racingRampUpParamSet, false);

  // Feasible solution may be received
  receiveMessages();

  // Run this solver on the root node
  run(rootNode);
}  // run

void Solver::run()
{
  // Runs this solver on the internal root node.
  // This method waits for a new Node from the LM.
  // Moreover, if it receives a termination message,
  //it stops running.
  while (true)
  {
    if (!pNode)
    {
      // Wait to receive a new node or a termination message.
      // If a termination message is received, return asap
      if (!receiveNewNodeAndReactivate()) return;
    }

    // A new node is received

    // Check if this solver received the paramset of another winner
    // solver during racing ramp-up
    if (pWinnerRacingRampUpParamSet)
    {
      // Set the winner racing parameters and interrupt racing stage
      setWinnerRacingParams(pWinnerRacingRampUpParamSet);
      pRacingInterruptIsRequested = false;
    }

    // Reset all racing parameters if needed
    if (pParamSet->getDescriptor().setalldefaultsafterracing() &&
            pRacingWinner && pNumNodesReceived == 2)
    {
      setWinnerRacingParams(0);
      pWinnerRacingRampUpParamSet.reset();
      pRacingInterruptIsRequested = false;
    }

    pRacingWinner = false;
    pCollectingMode = false;
    pNumSendInCollectingMode = 0;
    pAggressiveCollecting = false;
    pCollectingManyNodes = false;
    pNumCollectOnce = 0;

    resetBreakingInfo();
    pOnceBreak = false;
    pNoWaitModeSend = false;

    // Calculate metrics according to waiting time for nodes
    pNodeStartTimeMsec = pTimer->getWallClockTimeMsec();
    if (pPreviousStopTimeMsec == 0.0)
    {
       pIdleTimeToFirstNodeMsec = pNodeStartTimeMsec;
    }
    else
    {
       pIdleTimeBetweenNodesMsec += (pNodeStartTimeMsec - pPreviousStopTimeMsec);
    }

    // Create subproblem into target solver environment
    pSubproblemFreed = false;
    createSubproblem();
    updatePendingSolution();

    // Check if there is a global best incumbent solution
    if (pGlobalBestIncumbentSolution)
    {
      // If so, try the new solution
      tryNewSolution(pGlobalBestIncumbentSolution);
    }

    // Reset the global incumbent flag since the global incumbent
    // has been just used
    pGlobalIncumbentValueUpdateFlag = false;

    // Start solving the sub-tree problem from the root node
    assert(pNode);
    solve();

    // Done solving.
    // Notify completion of a calculation of a Node
    pPreviousStopTimeMsec = pTimer->getWallClockTimeMsec();
    const auto compTime = pPreviousStopTimeMsec - pNodeStartTimeMsec;

    // Update the number of nodes solved
    pNumSolved = getNumNodesSolved();

    // Send completion of calculation and update counters and accumulated time.
    // @note before sending completion state, receiving message should be checked.
    receiveMessages();
    sendCompletionOfCalculation(compTime);

    // Free solving environment for subproblem
    if (!pSubproblemFreed)
    {
       freeSubproblem();
    }

    // If light wait root node computation is applied, rest it
    if (pLightWeightRootNodeComputation)
    {
       setOriginalRootNodeProcess();
       pLightWeightRootNodeComputation = false;
    }

    // Update the current node with the
    // newNode and reset newNode
    assert(pNode);
    pNode.reset();
    if (pNewNode)
    {
      pNode = pNewNode;
      pNewNode = nullptr;
    }

    // Return asap if termination mode is on and
    // this solver doesn't need to stay alive after interruption
    if (pTerminationMode && !pStayAliveAfterInterrupt)
    {
       return;
    }
  }  // while
}  // run

void Solver::receiveMessages()
{
  // Check if there is any message
  optnet::Packet::UPtr packet(new optnet::Packet());
  while (pCommunicator->iprobe(packet))
  {
    // Get the source address from the sender network address
    int source = pCommunicator->convertNetworkAddrToRank(packet->networkAddr);
    auto packetTag = static_cast<net::MetaBBPacketTag*>(packet->packetTag.get());
    if (pSolverImpl->isHandlerSupported(packetTag->getPacketTagType()))
    {
      int status = (*pSolverImpl)(source, std::move(packet));
      if (status)
      {
        const std::string errMsg = "Solver - receiveMessages: "
            "error while running an handler on a tag and source";
        spdlog::error(errMsg);

        // Abort the process on error
        abort();
      }
    }
    else
    {
      const std::string errMsg = "Solver - receiveMessages: "
          "no message handler";
      spdlog::error(errMsg);
      throw std::runtime_error(errMsg);
    }

    packet.reset(new optnet::Packet());
  }  // while
}  // receiveMessages

void Solver::setRootNodeTime()
{
  pRootNodeTimeMsec = pTimer->getWallClockTimeMsec() - pNodeStartTimeMsec;
  if (pRootNodeTimeMsec < pMinRootNodeTimeMsec)
  {
    pMinRootNodeTimeMsec = pRootNodeTimeMsec;
  }
  if (pRootNodeTimeMsec > pMaxRootNodeTimeMsec)
  {
    pMaxRootNodeTimeMsec = pRootNodeTimeMsec;
  }
}  // setRootNodeTime

void Solver::sendIfImprovedSolutionWasFound(Solution::SPtr sol)
{
  if (!sol)
  {
    const std::string errMsg = "Solver - sendIfImprovedSolutionWasFound: "
        "empty pointer to solution";
    spdlog::error(errMsg);
    throw std::invalid_argument(errMsg);
  }

  // If there is no global best incumbent solution OR
  // there is a global best incumbent and the current solution is better than
  // the global one => send the solution to the network root node
  if (!pGlobalBestIncumbentSolution ||
      (pGlobalBestIncumbentSolution &&
          EPSLT(sol->getObjectiveFuntionValue(),
                pGlobalBestIncumbentSolution->getObjectiveFuntionValue(), pEps)))
  {
    pGlobalBestIncumbentValue = sol->getObjectiveFuntionValue();

    // Global incumbent value is updated
    pGlobalIncumbentValueUpdateFlag = true;

    assert(!pLocalIncumbentSolution);
    sol->send(pFramework, 0);

    // Increase the number of improving incumbents
    pNumImprovedIncumbent++;
  }
}  // sendIfImprovedSolutionWasFound

void Solver::saveIfImprovedSolutionWasFound(Solution::SPtr sol)
{
  if (!sol)
  {
    const std::string errMsg = "Solver - saveIfImprovedSolutionWasFound: "
        "empty pointer to solution";
    spdlog::error(errMsg);
    throw std::invalid_argument(errMsg);
  }

  // Compare to globalBestIncumbentValue before updating the local solution
  if (EPSLT(sol->getObjectiveFuntionValue(), pGlobalBestIncumbentValue, pEps))
  {
    if (pLocalIncumbentSolution)
    {
      // If there is a local incumbent solution and the given solution is better than
      // the local one => save solution
      if (EPSLT(sol->getObjectiveFuntionValue(),
                pLocalIncumbentSolution->getObjectiveFuntionValue(), pEps))
      {
        // Update current local solution
        pLocalIncumbentSolution = sol;

        // Increase the number of improving incumbents
        pNumImprovedIncumbent++;
      }
      else
      {
        // Reset local incumbent solution
        pLocalIncumbentSolution.reset();
      }
    }
    else
    {
      // There is no local solution to compare to,
      // just store the given solution as local solution
      pLocalIncumbentSolution = sol;

      // Increase the number of improving incumbents
      pNumImprovedIncumbent++;
    }
  }
}  // saveIfImprovedSolutionWasFound

void Solver::sendLocalSolution()
{
  // If there is a local incumbent not yet received, send it
  if (pLocalIncumbentSolution && !pNotificationProcessed)
  {
    if (!pGlobalBestIncumbentSolution)
    {
      // Send and set the global best incumbent value if not preset
      pLocalIncumbentSolution->send(pFramework, 0);
      pGlobalBestIncumbentSolution = pLocalIncumbentSolution;
    }
    else
    {
      // If the global best incumbent value is present and the local incumbent
      // is better than the global, send the local and set it as global best incumbent
      if (EPSLT(pLocalIncumbentSolution->getObjectiveFuntionValue(),
                pGlobalBestIncumbentSolution->getObjectiveFuntionValue(), pEps))
      {
        pLocalIncumbentSolution->send(pFramework, 0);
        pGlobalBestIncumbentSolution = pLocalIncumbentSolution;
      }
    }

    // Reset the local incumbent solution
    pLocalIncumbentSolution.reset();
  }
}  // sendLocalSolution

bool Solver::notificationIsNecessary()
{
  if (!pRampUp)
  {
    // RampUp is not finished and the solver in in racing stage
    if(isRacingStage())
    {
      if (pLMBestDualBoundValue < getDualBoundValue())
      {
        // The current dual bound is higher than the best LM dual bound.
        // This means that the current dual bound may be closer to the incumbent.
        // this requires a notification
        return true;
      }
      else
      {
        // Check the notification interval and send notifications to LM
        // if the interval is expired (from previous notification)
        return (pTimer->getWallClockTimeMsec() - pPreviousNotificationTimeMsec) >
        (pParamSet->getDescriptor()).notificationintervalmsec();
      }
    }
    else
    {
      // normal ramp-up phase, notify
      return true;
    }
  }
  else
  {
    // RampUp phase is finished
    if (pCollectingMode || pCollectingManyNodes)
    {
       return true;
    }

    // Check the notification interval and send notifications to LM
    // if the interval is expired (from previous notification)
    return (pTimer->getWallClockTimeMsec() - pPreviousNotificationTimeMsec) >
    (pParamSet->getDescriptor()).notificationintervalmsec();
  }
}  // notificationIsNecessary

void Solver::sendSolverState(long long numNodesSolved, int numNodesLeft,
                             double bestDualBoundValue, uint64_t timeMsec)
{
  // If a notification has been already processed but not received by the LM,
  // don't send the state but return
  if (pNotificationProcessed) return;

  int racingStage = 0;
  if (isRacingStage())
  {
    racingStage = 1;
  }

  double tempGlobalBestPrimalBound = metaconst::DOUBLE_POS_INF;
  if (pGlobalBestIncumbentSolution)
  {
    // Set the global best incumbent value
    tempGlobalBestPrimalBound = pGlobalBestIncumbentSolution->getObjectiveFuntionValue();
  }

  // Create a new solver state
  SolverState::SPtr solverState = pFramework->buildSolverState(
      racingStage,
      ++pNotificationIdGenerator,
      pNode->getLMId(),
      pNode->getGlobalSubtreeIdInLM(),
      numNodesSolved,
      numNodesLeft,
      std::max(bestDualBoundValue, pNode->getDualBoundValue()),
      tempGlobalBestPrimalBound,
      timeMsec,
      pAverageDualBoundGain);

  // Send the solver state to the root network node
  solverState->send(pFramework, 0);

  // Reset the state
  solverState.reset();

  // Update the notification timer variable
  pPreviousNotificationTimeMsec = pTimer->getWallClockTimeMsec();

  // A notification is issued
  pNotificationProcessed = true;
}  // sendSolverState

int Solver::getThresholdValue(int numNodes)
{
  if ((pParamSet->getDescriptor()).deterministic()) return 10;

  const auto compTimeMsec =
      pTimer->getWallClockTimeMsec() - (pIdleTimeToFirstNodeMsec + pIdleTimeBetweenNodesMsec);

  // @note +1 refers to this Node
  const double meanRootNodeTimeMsec =
      (pTotalRootNodeTimeMsec + pRootNodeTimeMsec ) / ((pNumNodesSolved + 1) * 1.0);
  double meanNodeTimeMsec = 0;

  // Less than or equal to 10 sec. and meanRootNode time less than or equal 0.0001 sec.
  // treat as unreliable
  if ((compTimeMsec - (pTotalRootNodeTimeMsec + pRootNodeTimeMsec )) > 10000
      && meanRootNodeTimeMsec > 0)
  {
    double val =
        (compTimeMsec - (pTotalRootNodeTimeMsec + pRootNodeTimeMsec)) /
        ((pTotalNumNodesSolved + numNodes - pNumNodesSolved)) * 1.0;

    meanNodeTimeMsec = (val >= 0) ? val : 0;
  }

  int threshold;
  if (meanNodeTimeMsec < 0.001)
  {
    // Heuristics
    threshold = (pRootNodeTimeMsec < 10) ?
        100 :
        ((pRootNodeTimeMsec < 100) ?
            50 :
            ((pRootNodeTimeMsec < 1000) ? 10 : 5));
  }
  else
  {
    if ((meanRootNodeTimeMsec / meanNodeTimeMsec) > 5000)
    {
      threshold =
          static_cast<int>((pParamSet->getDescriptor()).multipliertodeterminethresholdvalue() *
                           (meanRootNodeTimeMsec / meanNodeTimeMsec));
      if (threshold > 100)
      {
        threshold = (meanNodeTimeMsec > 1000.0) ? 3 : 100;
      }
    }
    else
    {
      // Heuristics
      threshold = (pRootNodeTimeMsec < 10) ?
          100 :
          ((pRootNodeTimeMsec < 100) ?
              50 :
              ((pRootNodeTimeMsec < 1000) ? 10 : 5));
    }
  }

  return (threshold < 2) ? 2 : threshold;
}  // getThresholdValue

void Solver::sendNode(long long n, int depth, double dualBound, double estimateValue,
                      DiffSubproblem::SPtr diffSubproblem)
{
  // Build a new node
  Node::SPtr node = pFramework->buildNode(
      NodeId(-1, SubtreeId(pNode->getNodeId().getSubtreeId().getLMId(),
                           pNode->getNodeId().getSubtreeId().getGlobalSubstreeIdInLM(),
                           pCommunicator->getRank())),
             NodeId(n, pNode->getNodeId().getSubtreeId()),
             pNode->getDepth() + depth,
             std::max(dualBound, pNode->getDualBoundValue()),
             dualBound,
             estimateValue,
             diffSubproblem);

  // Start idle time counter
  timer::Timer idleTimer;

  // Send the node to the root network node
  node->send(pFramework, 0);
  node.reset();

  if (pNumSendInCollectingMode <= 0 || !pNoWaitModeSend)
  {
    // Wait to receive the confirmation that node was received
    waitSpecifiedTagMessage(net::MetaBBPacketTag::PacketTagType::PPT_NODE_RECEIVED);
  }

  pNumSent++;
  if (pNumSendInCollectingMode > 0) pNumSendInCollectingMode--;
  if (pNumCollectOnce > 0) pNumCollectOnce--;
  if (pAggressiveCollecting && pNumSendInCollectingMode == 0) pAggressiveCollecting = false;
  if (pCollectingManyNodes && pNumCollectOnce == 0) pCollectingManyNodes = false;
  if (isBreaking()) pNumTransferredNodes++;
}  // sendNode

void Solver::sendAnotherNodeRequest(double bestDualBoundValue)
{
  // Return asap is another request is being performed
  if (pAnotherNodeIsRequested) return;

  // Create a network packet storing the best dual bound value,
  // and the tag describing this request
  optnet::Packet::UPtr packet(new optnet::Packet());
  packet->networkPacket =
      optnet::BaseNetworkPacket::UPtr(new optnet::NetworkPacket<double>(bestDualBoundValue));
  packet->packetTag =
      net::MetaBBPacketTag::UPtr(new net::MetaBBPacketTag(
          net::MetaBBPacketTag::PacketTagType::PPT_ANOTHER_NODE_REQUEST));

  // Send the request to the root network node
  ASSERT_COMM_CALL(pCommunicator->send(std::move(packet), 0));

  // Set another node request flag
  pAnotherNodeIsRequested = true;
}  // sendAnotherNodeRequest

void Solver::waitMessageIfNecessary()
{
  if ((pParamSet->getDescriptor()).notificationsynchronization() ==
      static_cast<int>(solver::NotificationSychronization::NS_ALWAYS_SYNCHRONIZE) ||
      !pRampUp ||
      ((pParamSet->getDescriptor()).notificationsynchronization() ==
          static_cast<int>(solver::NotificationSychronization::NS_EVERY_ITERATION_IN_COLLECTING)
          && (pCollectingMode || pCollectingManyNodes)))
  {
     waitNotificationIdMessage();
  }
}  // waitMessageIfNecessary

bool Solver::receiveNewNodeAndReactivate()
{
  optnet::Packet::UPtr packet(new optnet::Packet());
  while(true)
  {
    pCommunicator->probe(packet);

    // Get the source address from the sender network address
    int source = pCommunicator->convertNetworkAddrToRank(packet->networkAddr);
    auto packetTag = static_cast<net::MetaBBPacketTag*>(packet->packetTag.get());
    if (pSolverImpl->isHandlerSupported(packetTag->getPacketTagType()))
    {
      int status = (*pSolverImpl)(source, std::move(packet));
      if (status)
      {
        const std::string errMsg = "Solver - receiveNewNodeAndReactivate: "
            "error while running an handler on a tag and source";
        spdlog::error(errMsg);

        // Abort the process on error
        abort();
      }
    }
    else
    {
      const std::string errMsg = "Solver - receiveNewNodeAndReactivate: "
          "no message handler";
      spdlog::error(errMsg);
      throw std::runtime_error(errMsg);
    }

    // Reset the packet to receive next one
    packet.reset(new optnet::Packet());

    if (pNode)
    {
      pPreviousNotificationTimeMsec= pTimer->getWallClockTimeMsec();
      return true;
    }

    // Break if termination mode is required
    if (pTerminationMode && !pStayAliveAfterInterrupt)
    {
      break;
    }
  }  // while

  return false;
}  // receiveNewNodeAndReactivate

void Solver::waitSpecifiedTagMessage(net::MetaBBPacketTag::PacketTagType tag)
{
  timer::Timer idleTimer;
  pWaitingSpecificMessage = true;

  // Create the packet to receive from the root network node
  optnet::Packet::UPtr packet(new optnet::Packet());
  packet->packetTag = net::MetaBBPacketTag::UPtr(new net::MetaBBPacketTag(tag));
  while (pCommunicator->waitSpecTagFromSpecSource(packet, 0) != 0)
  {
    // Get the source address from the sender network address
    auto packetTag = static_cast<net::MetaBBPacketTag*>(packet->packetTag.get());
    if (pSolverImpl->isHandlerSupported(packetTag->getPacketTagType()))
    {
      // Handle the packet anyways
      int status = (*pSolverImpl)(0, std::move(packet));
      if (status)
      {
        const std::string errMsg = "Solver - receiveMessages: "
            "error while running an handler on a tag and source";
        spdlog::error(errMsg);

        // Abort the process on error
        abort();
      }
    }
    else
    {
      const std::string errMsg = "Solver - receiveMessages: "
          "no message handler";
      spdlog::error(errMsg);
      throw std::runtime_error(errMsg);
    }

    // Reset the tag for next loop
    (packet->packetTag).reset(new net::MetaBBPacketTag(tag));
  }  // while

  // Update idle time spent waiting for a notification
  pIdleTimeToWaitNotificationIdMsec += idleTimer.getWallClockTimeMsec();

  // Handle the packet with given input "tag"
  auto packetTag = static_cast<net::MetaBBPacketTag*>(packet->packetTag.get());
  assert(pSolverImpl->isHandlerSupported(packetTag->getPacketTagType()));

  int status = (*pSolverImpl)(0, std::move(packet));
  if (status)
  {
    const std::string errMsg = "Solver - receiveMessages: "
        "error while running an handler on a tag and source";
    spdlog::error(errMsg);

    // Abort the process on error
    abort();
  }

  // Done waiting for specified message
  pWaitingSpecificMessage = false;
}  // waitSpecifiedTagMessage

void Solver::waitAckCompletion()
{
  timer::Timer idleTimer;
  pWaitingSpecificMessage = true;

  // Create the packet to receive from the root network node
  optnet::Packet::UPtr packet(new optnet::Packet());
  packet->packetTag = net::MetaBBPacketTag::UPtr(new net::MetaBBPacketTag(
      net::MetaBBPacketTag::PacketTagType::PPT_ACK_COMPLETION));
  while (pCommunicator->waitSpecTagFromSpecSource(packet, 0) != 0)
  {
    // Get the source address from the sender network address
    auto packetTag = static_cast<net::MetaBBPacketTag*>(packet->packetTag.get());
    if (pSolverImpl->isHandlerSupported(packetTag->getPacketTagType()))
    {
      // Handle the packet anyways
      int status = (*pSolverImpl)(0, std::move(packet));
      if (status)
      {
        const std::string errMsg = "Solver - receiveMessages: "
            "error while running an handler on a tag and source";
        spdlog::error(errMsg);

        // Abort the process on error
        abort();
      }
    }
    else
    {
      const std::string errMsg = "Solver - receiveMessages: "
          "no message handler";
      spdlog::error(errMsg);
      throw std::runtime_error(errMsg);
    }

    // Reset the tag for next loop
    (packet->packetTag).reset(new net::MetaBBPacketTag(
        net::MetaBBPacketTag::PacketTagType::PPT_ACK_COMPLETION));
  }  // while

  // Update idle time spent waiting for ack message
  pIdleTimeToWaitAckCompletionMsec += idleTimer.getWallClockTimeMsec();

  // Wait to receive the notification id message
  (packet->packetTag).reset(new net::MetaBBPacketTag(
      net::MetaBBPacketTag::PacketTagType::PPT_ACK_COMPLETION));
  ASSERT_COMM_CALL(pCommunicator->receive(packet, 0));

  // Done waiting for specified message
  pWaitingSpecificMessage = false;
}  // waitAckCompletion

void Solver::restartRacing()
{
  assert(!pInstance);

  freeSubproblem();
  pSubproblemFreed = true;

  reinitializeInstance();

  // Receive the node from the LoadManager
  assert(!pNewNode);
  pNewNode = pFramework->buildNode();
  ASSERT_COMM_CALL(pNewNode->broadcast(pFramework, LoadManager::GetLoadManagerRank()));

  pNumNodesReceived++;
  pRacingIsInterrupted = false;
  pRestartingRacing = false;
}  // restartRacing

void Solver::sendCompletionOfCalculation(uint64_t stopTimeMsec)
{
  // Zero means terminated with success
  solver::TerminationState terminationState = solver::TerminationState::TS_TERMINATED_NORMALLY;
  bool needToResetMaximalDualBound = pRacingIsInterrupted;

  // Set the proper termination state
  if (pGivenGapIsReached)
  {
    if (isRacingStage())
    {
      terminationState = solver::TerminationState::TS_TERMINATED_IN_RACING_STAGE;
    }
  }
  else
  {
    if (pTerminationMode == solver::TerminationMode::TM_INTERRUPTED_TERMINATION)
    {
      terminationState = solver::TerminationState::TS_TERMINATED_BY_INTERRUPT_REQUEST;
    }
    else
    {
      if (pNewNode)
      {
        // Terminated and there is a new node
        terminationState = solver::TerminationState::TS_TERMINATED_BY_ANOTHER_NODE;
      }
      else
      {
        if (pAnotherNodeIsRequested)
        {
          // Wait to receive another node
          optnet::Packet::UPtr packet(new optnet::Packet());
          while (pCommunicator->probe(packet))
          {
            // Get the source address from the sender network address
            int source = pCommunicator->convertNetworkAddrToRank(packet->networkAddr);
            auto packetTag = static_cast<net::MetaBBPacketTag*>(packet->packetTag.get());
            if (pSolverImpl->isHandlerSupported(packetTag->getPacketTagType()))
            {
              int status = (*pSolverImpl)(source, std::move(packet));
              if (status)
              {
                const std::string errMsg = "Solver - receiveMessages: "
                    "error while running an handler on a tag and source";
                spdlog::error(errMsg);

                // Abort the process on error
                abort();
              }
            }
            else
            {
              const std::string errMsg = "Solver - receiveMessages: "
                  "no message handler";
              spdlog::error(errMsg);
              throw std::runtime_error(errMsg);
            }

            if (pNewNode)
            {
              terminationState = solver::TerminationState::TS_TERMINATED_BY_ANOTHER_NODE;
              break;
            }

            if (!pAnotherNodeIsRequested) break;
          }  // while

          if (pNode->getMergingStatus() == Node::NodeMergingStatus::NMS_CANNOT_BE_MERGED)
          {
            terminationState = solver::TerminationState::TS_INTERRUPTED_IN_MERGING;
          }
        }

        if (isRacingStage() && !pNewNode && !pRacingWinner)
        {
          if (!pRacingIsInterrupted && wasTerminatedNormally())
          {
            terminationState = solver::TerminationState::TS_TERMINATED_IN_RACING_STAGE;
          }
          else
          {
            terminationState = solver::TerminationState::TS_INTERRUPTED_IN_RACING_STAGE;
            pRacingInterruptIsRequested = false;
            pRacingIsInterrupted = false;
          }
        }
        else
        {
          if (!wasTerminatedNormally())
          {
            const std::string errMsg = "Solver with rank "
                + std::to_string(pCommunicator->getRank()) +
                " was terminated abnormally";
            spdlog::error(errMsg);
            throw std::runtime_error(errMsg);
          }
        }
      }
    }
  }  // else

  double averageSimplexIter = 0.0;
  if (pNumSolved <= 1)
  {
     setRootNodeTime();
     setRootNodeSimplexIter(getSimplexIter());
  }
  else
  {
     averageSimplexIter =
         static_cast<double>((getSimplexIter() - pNumSimplexIterRoot)/(pNumSolved - 1));
  }

  // Create and send a new calculation stae to the root network node
  auto calculationState = pFramework->buildCalculationState(
      stopTimeMsec, pRootNodeTimeMsec, pNumSolved, pNumSent, pNumImprovedIncumbent,
      terminationState, pNumSolvedWithNoPreprocesses, pNumSimplexIterRoot,
      averageSimplexIter, pNumTransferredLocalCuts, pMinTransferredLocalCuts,
      pMaxTransferredLocalCuts, getNumRestarts(), pMinIntInfeasibilitySum,
      pMaxIntInfeasibilitySum, pMinNumIntInfeasibility, pMaxNumIntInfeasibility,
      pSolverDualBound);

  // Send the state to the root network node
  calculationState->send(pFramework, 0);
  calculationState.reset();

  // Update counters
  if (pNumSolved < pMinNumSolved) pMinNumSolved = pNumSolved;
  if (pNumSolved > pMaxNumSolved) pMaxNumSolved = pNumSolved;
  pTotalNumNodesSolved += pNumSolved;
  pTotalNumNodesSent += pNumSent;
  pTotalNumImprovedIncumbent += pNumImprovedIncumbent;
  pNumNodesSolved++;
  if(pNumSolved == 1) pNumNodesSolvedAtRoot++;

  pNumTransferredLocalCutsFromSolver += pNumTransferredLocalCuts;
  if (pMinTransferredLocalCutsFromSolver > pMinTransferredLocalCuts)
  {
    pMinTransferredLocalCutsFromSolver = pMinTransferredLocalCuts;
  }
  if (pMaxTransferredLocalCutsFromSolver < pMaxTransferredLocalCuts)
  {
    pMaxTransferredLocalCutsFromSolver = pMaxTransferredLocalCuts;
  }

  const int numRestarts = getNumRestarts();
  pNumTotalRestarts += numRestarts;
  if (pMinRestarts > numRestarts)
  {
     pMinRestarts = numRestarts;
  }
  if (pMaxRestarts < numRestarts)
  {
    pMaxRestarts = numRestarts;
  }

  // Reset counters
  pNumSolved = 0;
  pNumSent = 0;
  pNumImprovedIncumbent = 0;
  pNumSolvedWithNoPreprocesses = 0;
  pNumTransferredLocalCuts = 0;
  pMinTransferredLocalCuts = std::numeric_limits<int>::max();
  pMaxTransferredLocalCuts = std::numeric_limits<int>::lowest();


  pTotalRootNodeTimeMsec += pRootNodeTimeMsec;
  pRootNodeTimeMsec = 0.0;

  pMinIntInfeasibilitySum = std::numeric_limits<double>::max();
  pMaxIntInfeasibilitySum = 0.0;
  pMinNumIntInfeasibility = std::numeric_limits<int>::max();
  pMaxNumIntInfeasibility = 0;

  // Get the time
  double stopTime = pTimer->getWallClockTimeMsec();

  // Interruption point
  solver::InterruptionPoint interruptionPoint;

  // Send the termination state to the root network node
  auto isInRacingStage = isRacingStage();
  if (isInRacingStage)
  {
    if (pNewNode)
    {
      pNumNodesReceived--;
    }

    // Racing has been interruped
    interruptionPoint = solver::InterruptionPoint::IP_INTERRUPTED_RACING_RAMPUP;
  }
  else
  {
    // Checkpoint interruption
    interruptionPoint = solver::InterruptionPoint::IP_INTERRUPTED_CHECKPOINT;
  }

  // Create and send the solver's termination state to the root network node
  auto solverTerminationState = pFramework->buildSolverTerminationState(
      interruptionPoint,
      pCommunicator->getRank(),
      pTotalNumNodesSolved,
      pMinNumSolved,
      pMaxNumSolved,
      pTotalNumNodesSent,
      pTotalNumImprovedIncumbent,
      pNumNodesReceived,
      pNumNodesSolved,
      pNumNodesSolvedAtRoot,
      pNumNodesSolvedAtPreCheck,
      pNumTransferredLocalCuts,
      pMinTransferredLocalCuts,
      pMaxTransferredLocalCuts,
      pNumTotalRestarts,
      pMinRestarts,
      pMaxRestarts,
      getNumVarTightened(),
      getNumIntVarTightened(),
      stopTime,
      pIdleTimeToFirstNodeMsec,
      pIdleTimeBetweenNodesMsec,
      0,
      pIdleTimeToWaitNotificationIdMsec,
      pIdleTimeToWaitAckCompletionMsec,
      pIdleTimeToWaitTokenMsec,
      pTotalRootNodeTimeMsec,
      pMinRootNodeTimeMsec,
      stopTime);
  solverTerminationState->send(pFramework, 0);
  solverTerminationState.reset();

  if (isInRacingStage)
  {
    assert(!pRacingWinner);

    // Re-initialize all counters
    pMinNumSolved = std::numeric_limits<int>::max();
    pMaxNumSolved = std::numeric_limits<int>::lowest();
    pTotalNumNodesSolved = 0;
    pTotalNumNodesSent = 0;
    pTotalNumImprovedIncumbent = 0;
    pNumNodesReceived = (pNewNode) ? 1 : 0;
    pNumNodesSolved = 0;
    pNumNodesSolvedAtRoot = 0;
    pNumNodesSolvedAtPreCheck = 0;
    pTotalRootNodeTimeMsec = 0;
    pMinRootNodeTimeMsec = 0;
    pMaxRootNodeTimeMsec = 0;

    // Check whether or not to restart racing
    if (pRestartingRacing)
    {
      restartRacing();
    }
    else
    {
      // No restart, terminate racing
      terminateRacing();
    }
  }

  pNumSendInCollectingMode = 0;
  if (needToResetMaximalDualBound) pSolverDualBound = std::numeric_limits<int>::lowest();
  pGivenGapIsReached = false;
}  // sendCompletionOfCalculation

bool Solver::updateGlobalBestIncumbentSolution(Solution::SPtr sol)
{
  if (!sol)
  {
    const std::string errMsg = "Solver - updateGlobalBestIncumbentSolution: "
        "empty pointer to solution";
    spdlog::error(errMsg);
    throw std::invalid_argument(errMsg);
  }

  // Check if there is already a best incumbent solution
  if (pGlobalBestIncumbentSolution)
  {
    // The received solution is better than the current global best incumbent
    if (EPSLE(sol->getObjectiveFuntionValue(),
              pGlobalBestIncumbentSolution->getObjectiveFuntionValue(),
              metaconst::DEAFULT_NUM_EPSILON))
    {
      // Replace the global best incumbent with received solution
      if (sol != pGlobalBestIncumbentSolution)
      {
        pGlobalBestIncumbentSolution = sol;
      }

      // Update the best incumbent value
      if (EPSLE(sol->getObjectiveFuntionValue(), pGlobalBestIncumbentValue,
                metaconst::DEAFULT_NUM_EPSILON))
      {
        pGlobalBestIncumbentValue = sol->getObjectiveFuntionValue();
        pGlobalIncumbentValueUpdateFlag = true;
      }

      // Solution has been updated
      return true;
     }
     else
     {
       // Solution has not been updated
       return false;
     }
  }
  else
  {
    // There was no best global incumbent solution,
    // set it to the received solution
    pGlobalBestIncumbentSolution = sol;

    // Check whether or not to update the best incumbent value
    if (EPSLE(sol->getObjectiveFuntionValue(), pGlobalBestIncumbentValue,
              metaconst::DEAFULT_NUM_EPSILON))
    {
      pGlobalBestIncumbentValue = sol->getObjectiveFuntionValue();
      pGlobalIncumbentValueUpdateFlag = true;
    }

    // Solution has been updated
    return true;
  }
}  // updateGlobalBestIncumbentSolution

bool Solver::updateGlobalBestIncumbentValue(double newValue)
{
  if (newValue < pGlobalBestIncumbentValue)
  {
    pGlobalBestIncumbentValue = newValue;
    pGlobalIncumbentValueUpdateFlag = true;
    return true;
  }
  return false;
}  // updateGlobalBestIncumbentValue

bool Solver::updateGlobalBestCutOffValue(double newValue)
{
  if (newValue < pGlobalBestCutOffValue)
  {
    pGlobalBestCutOffValue = newValue;
    return true;
  }
  return false;
}  // updateGlobalBestCutOffValue

}  // namespace metabb
}  // namespace optilab
