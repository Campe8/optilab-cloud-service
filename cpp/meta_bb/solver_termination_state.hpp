//
// Copyright OptiLab 2019-2020. All rights reserved.
//
// Base class encapsulating the state of a solver
// when it has done computing solutions.
// The termination state is transfered from a
// network node to the root network node,
// i.e., it is transfered to the load coordinator.
//

#pragma once

#include <cstdint>  // for uint64_t
#include <memory>   // for std::shared_ptr

#include "meta_bb/framework.hpp"
#include "meta_bb/meta_bb_constants.hpp"
#include "meta_bb/meta_bb_struct.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace  metabb {

class SYS_EXPORT_CLASS SolverTerminationState {
 public:
  using SPtr = std::shared_ptr<SolverTerminationState>;

 public:
  SolverTerminationState() = default;

  /// Constructor.
  /// Among other arguments:
  /// - interrupted: indicates whether the solver was interrupted or not.
  ///   This value can be
  ///     0) not interrupted;
  ///     1) interrupted
  ///     2) checkpoint
  ///     3) racing ramp-up
  /// - rank: id of the worker/solver creating this termination state
  /// - totalNumSolved: number of nodes solved by this solver/worker
  /// - totalNumSent: total number of nodes sent by this solver/worker
  /// - numNodesReceived: total number of nodes received by this solver/worker
  /// - numTotalRestarts: total number of restarts
  /// - runningTimeMsec: total running time in msec of the solver/worker
  SolverTerminationState(
      solver::InterruptionPoint interrupted,
      int rank,
      int totalNumSolved,
      int minNumSolved,
      int maxNumSolved,
      int totalNumSent,
      int totalNumImprovedIncumbent,
      int numNodesReceived,
      int numNodesSolved,
      int numNodesSolvedAtRoot,
      int numNodesSolvedAtPreCheck,
      int numTransferredLocalCutsFromSolver,
      int minNumTransferredLocalCutsFromSolver,
      int maxNUmTransferredLocalCutsFromSolver,
      int numTotalRestarts,
      int minNumRestarts,
      int maxNumRestarts,
      int numVarTightened,
      int numIntVarTightenedInt,
      uint64_t runningTimeMsec,
      uint64_t idleTimeToFirstNodeMsec,
      uint64_t idleTimeBetweenNodesMsec,
      uint64_t idleTimeAfterLastNodeMsec,
      uint64_t idleTimeToWaitNotificationIdMsec,
      uint64_t idleTimeToWaitAckCompletionMsec,
      uint64_t idleTimeToWaitTokenMsec,
      uint64_t totalRootNodeTimeMsec,
      uint64_t minRootNodeTimeMsec,
      uint64_t maxRootNodeTimeMsec)
  : pInterrupted(interrupted),
    pRank(rank),
    pTotalNumNodesSolved(totalNumSolved),
    pMinNumNodesSolved(minNumSolved),
    pMaxNumNodesSolved(maxNumSolved),
    pTotalNumNodesSent(totalNumSent),
    pTotalNumImprovedIncumbent(totalNumImprovedIncumbent),
    pNumNodesReceived(numNodesReceived),
    pNumNodesSolved(numNodesSolved),
    pNumNodesSolvedAtRoot(numNodesSolvedAtRoot),
    pNumNodesSolvedAtPreCheck(numNodesSolvedAtPreCheck),
    pNumTransferredLocalCutsFromSolver(numTransferredLocalCutsFromSolver),
    pMinTransferredLocalCutsFromSolver(minNumTransferredLocalCutsFromSolver),
    pMaxTransferredLocalCutsFromSolver(maxNUmTransferredLocalCutsFromSolver),
    pNumTotalRestarts(numTotalRestarts),
    pMinNumRestarts(minNumRestarts),
    pMaxNumRestarts(maxNumRestarts),
    pNumVarBoundsTightened(numVarTightened),
    pNumIntVarBoundsTightened(numIntVarTightenedInt),
    pRunningTimeMsec(runningTimeMsec),
    pIdleTimeToFirstNodeMsec(idleTimeToFirstNodeMsec),
    pIdleTimeBetweenNodesMsec(idleTimeBetweenNodesMsec),
    pIdleTimeAfterLastNodesMsec(idleTimeAfterLastNodeMsec),
    pIdleTimeToWaitForNotificationIdMsec(idleTimeToWaitNotificationIdMsec),
    pIdleTimeToWaitForAckCompletionMsec(idleTimeToWaitAckCompletionMsec),
    pIdleTimeToWaitForTokenMsec(idleTimeToWaitTokenMsec),
    pTotalRootNodeTimeMsec(totalRootNodeTimeMsec),
    pMinRootNodeTimeMsec(minRootNodeTimeMsec),
    pMaxRootNodeTimeMsec(maxRootNodeTimeMsec)
  {
  }

  virtual ~SolverTerminationState() = default;

  /// Returns the interrupted mode causing this termination state
  inline solver::InterruptionPoint getInterruptedMode() const { return pInterrupted; }

  /// Sends this termination state to the given network node destination
  virtual int send(const Framework::SPtr& framework, int destination) noexcept = 0;

  /// Uploads the content of the given network packet into this termination state.
  /// If the given packet is not a node network packet, throws std::runtime_error
  /// Returns zero on success, non-zero otherwise
  virtual int upload(optnet::Packet::UPtr packet) noexcept = 0;

 protected:
  /// Indicates whether the solver was interrupted or not:
  /// 0) not interrupted;
  /// 1) interrupted
  /// 2) checkpoint
  /// 3) racing ramp-up
  solver::InterruptionPoint pInterrupted{solver::InterruptionPoint::IP_NOT_INTERRUPTED};

  /// Rank of the solver creating this state, i.e., the id of the worker
  /// running the solver
  int pRank{-1};

  /// Total number of solved nodes
  int pTotalNumNodesSolved{-1};

  /// Minimum number of subtree nodes rooted to the node solved by the solver
  /// creating this termination state
  int pMinNumNodesSolved{-1};

  /// Maximum number of subtree nodes rooted to the node solved by the solver
  /// creating this termination state
  int pMaxNumNodesSolved{-1};

  /// Total number of nodes sent by the solver
  int pTotalNumNodesSent{-1};

  /// Total number of improving incumbents found by the solver
  int pTotalNumImprovedIncumbent{-1};

  /// Total number of nodes received in the solver creating this termination state
  int pNumNodesReceived{-1};

  /// Total number of nodes (received and) solved
  int pNumNodesSolved{-1};

  /// Total number of nodes solved at root node before sending
  int pNumNodesSolvedAtRoot{-1};

  /// Number of nodes solved at pre-checking during check for satisfiability
  int pNumNodesSolvedAtPreCheck{-1};

  /// Number of local cuts transferred from this solver
  int pNumTransferredLocalCutsFromSolver{0};

  /// Minimum number of local cuts transferred from this solver
  int pMinTransferredLocalCutsFromSolver{0};

  /// Maximium number of local cuts transferred from this solver
  int pMaxTransferredLocalCutsFromSolver{0};

  /// Total number of restarts
  int pNumTotalRestarts{0};

  /// Minimum number of restarts
  int pMinNumRestarts{0};

  /// Maximum number of restarts
  int pMaxNumRestarts{0};

  /// Number of variable bounds tightened during racing stage
  int pNumVarBoundsTightened{0};

  /// Number of integral variable bounds tightened during racing stage
  int pNumIntVarBoundsTightened{0};

  /// Total running time in msec of the solver
  uint64_t pRunningTimeMsec{0};

  /// Idle time in msec before start solving the first node
  uint64_t pIdleTimeToFirstNodeMsec{0};

  /// Idle time in msec between nodes processing
  uint64_t pIdleTimeBetweenNodesMsec{0};

  /// Idle time in msec after the last node was solved
  uint64_t pIdleTimeAfterLastNodesMsec{0};

  /// Idle time in msec spent on waiting for the notification id
  uint64_t pIdleTimeToWaitForNotificationIdMsec{0};

  /// Idle time in msec spent on waiting for the completion message
  uint64_t pIdleTimeToWaitForAckCompletionMsec{0};

  /// Idle time in msec spent on waiting for the token
  uint64_t pIdleTimeToWaitForTokenMsec{0};

  /// Total time in msec spent by root node processing
  uint64_t pTotalRootNodeTimeMsec{0};

  /// Minimum time in msec spent by root node processing
  uint64_t pMinRootNodeTimeMsec{0};

  /// Maximum time in msec spent by root node processing
  uint64_t pMaxRootNodeTimeMsec{0};
};

}  // namespace metabb
}  // namespace optilab
