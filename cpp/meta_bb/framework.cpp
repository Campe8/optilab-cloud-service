#include "meta_bb/framework.hpp"

#include <stdexcept>  // for std::invalid_argument

namespace optilab {
namespace  metabb {

Framework::Framework(optnet::NetworkBaseCommunicator::SPtr comm)
{
  setNetworkCommunicator(comm);
}

void Framework::setNetworkCommunicator(optnet::NetworkBaseCommunicator::SPtr comm)
{
  if (!comm)
  {
    throw std::invalid_argument("Framework - empty pointer to the network base communicator");
  }
  pNetworkComm = comm;
}  // setNetworkCommunicator

timer::Timer::SPtr Framework::buildTimer(bool startTimer)
{
  return std::make_shared<timer::Timer>(startTimer);
}  //buildTimer

}  // namespace metabb
}  // namespace optilab
