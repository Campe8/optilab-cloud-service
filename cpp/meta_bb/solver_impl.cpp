#include "meta_bb/solver_impl.hpp"

#include <cassert>
#include <stdexcept>  // for std::runtime_error
#include <string>

#include <spdlog/spdlog.h>

#include "meta_bb/meta_bb_constants.hpp"
#include "meta_bb/meta_bb_macros.hpp"
#include "meta_bb/meta_bb_struct.hpp"
#include "meta_bb/meta_bb_utilities.hpp"
#include "optimizer_network/network_packet.hpp"

namespace optilab {
namespace  metabb {

SolverImpl::SolverImpl(Solver* solver)
: pSolver(solver)
{
  assert(pSolver);
}

bool SolverImpl::isHandlerSupported(net::MetaBBPacketTag::PacketTagType tag)
{
  bool supported = false;
  switch(tag)
  {
    case net::MetaBBPacketTag::PacketTagType::PPT_ACK_COMPLETION:
    case net::MetaBBPacketTag::PacketTagType::PPT_ANOTHER_NODE_REQUEST:
    case net::MetaBBPacketTag::PacketTagType::PPT_BREAKING:
    case net::MetaBBPacketTag::PacketTagType::PPT_COLLECT_ALL_NODES:
    case net::MetaBBPacketTag::PacketTagType::PPT_CUTOFF_VALUE:
    case net::MetaBBPacketTag::PacketTagType::PPT_GIVEN_GAP_IS_REACHED:
    case net::MetaBBPacketTag::PacketTagType::PPT_GLOBAL_BEST_DUAL_BOUND_VALUE_AT_WARM_START:
    case net::MetaBBPacketTag::PacketTagType::PPT_IN_COLLECTING_MODE:
    case net::MetaBBPacketTag::PacketTagType::PPT_INCUMBENT_VALUE:
    case net::MetaBBPacketTag::PacketTagType::PPT_INTERRUPT_REQUEST:
    case net::MetaBBPacketTag::PacketTagType::PPT_LIGHT_WEIGHT_ROOT_NODE_PROCESS:
    case net::MetaBBPacketTag::PacketTagType::PPT_LM_BEST_BOUND_VALUE:
    case net::MetaBBPacketTag::PacketTagType::PPT_NO_NODES:
    case net::MetaBBPacketTag::PacketTagType::PPT_NO_TEST_DUAL_BOUND_GAIN:
    case net::MetaBBPacketTag::PacketTagType::PPT_NO_WAIT_MODE_SEND:
    case net::MetaBBPacketTag::PacketTagType::PPT_NODE_RECEIVED:
    case net::MetaBBPacketTag::PacketTagType::PPT_NOTIFICATION_ID:
    case net::MetaBBPacketTag::PacketTagType::PPT_OUT_COLLECTING_MODE:
    case net::MetaBBPacketTag::PacketTagType::PPT_RACING_RAMP_UP_PARAMSET:
    case net::MetaBBPacketTag::PacketTagType::PPT_RAMP_UP:
    case net::MetaBBPacketTag::PacketTagType::PPT_RESTART:
    case net::MetaBBPacketTag::PacketTagType::PPT_RETRY_RAMP_UP:
    case net::MetaBBPacketTag::PacketTagType::PPT_SOLUTION:
    case net::MetaBBPacketTag::PacketTagType::PPT_TERMINATE_REQUEST:
    case net::MetaBBPacketTag::PacketTagType::PPT_TERMINATE_SOLVING_TO_RESTART:
    case net::MetaBBPacketTag::PacketTagType::PPT_TEST_DUAL_BOUND_GAIN:
    case net::MetaBBPacketTag::PacketTagType::PPT_WINNER:
    case net::MetaBBPacketTag::PacketTagType::PTT_NODE:
      supported = true;
      break;
    default:
      supported = false;
      break;
  }

  const auto rampupProcess = (pSolver->pParamSet->getDescriptor()).rampupphaseprocess();
  if (rampupProcess == loadmanager::RampUpPhaseProcess::RPP_RACING ||
      rampupProcess == loadmanager::RampUpPhaseProcess::RPP_REBUILD_TREE_AFTER_RACING ||
      (pSolver->pParamSet->getDescriptor()).communicatetighterboundsinracing())
  {
    if (tag == net::MetaBBPacketTag::PacketTagType::PPT_LB_BOUND_TIGHTENED_INDEX ||
        tag == net::MetaBBPacketTag::PacketTagType::PPT_UB_BOUND_TIGHTENED_INDEX)
    {
      supported = true;
    }
  }

  return supported;
}  // isHandlerSupported

int SolverImpl::operator()(int source, optnet::Packet::UPtr packet)
{
  if (!packet)
  {
    throw std::runtime_error("SolverImpl - functor operator: empty input packet");
  }

  net::MetaBBPacketTag* metabbPacket = static_cast<net::MetaBBPacketTag*>((packet->packetTag).get());
  switch(metabbPacket->getPacketTagType())
  {
    case net::MetaBBPacketTag::PacketTagType::PPT_BREAKING:
      return processTagBreaking(source, std::move(packet));
    case net::MetaBBPacketTag::PacketTagType::PPT_COLLECT_ALL_NODES:
      return processTagCollectAllNodes(source, std::move(packet));
    case net::MetaBBPacketTag::PacketTagType::PPT_CUTOFF_VALUE:
      return processTagCutOffValue(source, std::move(packet));
    case net::MetaBBPacketTag::PacketTagType::PPT_GIVEN_GAP_IS_REACHED:
      return processTagGivenGapIsReached(source, std::move(packet));
    case net::MetaBBPacketTag::PacketTagType::PPT_GLOBAL_BEST_DUAL_BOUND_VALUE_AT_WARM_START:
      return processTagGlobalBestDualBoundValueAtWarmStart(source, std::move(packet));
    case net::MetaBBPacketTag::PacketTagType::PPT_IN_COLLECTING_MODE:
      return processTagInCollectingMode(source, std::move(packet));
    case net::MetaBBPacketTag::PacketTagType::PPT_INCUMBENT_VALUE:
      return processTagIncumbentValue(source, std::move(packet));
    case net::MetaBBPacketTag::PacketTagType::PPT_INTERRUPT_REQUEST:
      return processTagInterruptRequest(source, std::move(packet));
    case net::MetaBBPacketTag::PacketTagType::PPT_LIGHT_WEIGHT_ROOT_NODE_PROCESS:
      return processTagLightWeightRootNodeProcess(source, std::move(packet));
    case net::MetaBBPacketTag::PacketTagType::PPT_LM_BEST_BOUND_VALUE:
      return processTagLMBestBoundValue(source, std::move(packet));
    case net::MetaBBPacketTag::PacketTagType::PPT_NO_NODES:
      return processTagNoNodes(source, std::move(packet));
    case net::MetaBBPacketTag::PacketTagType::PPT_NO_TEST_DUAL_BOUND_GAIN:
      return processTagNoTestDualBoundGain(source, std::move(packet));
    case net::MetaBBPacketTag::PacketTagType::PPT_NO_WAIT_MODE_SEND:
      return processTagNoWaitModeSend(source, std::move(packet));
    case net::MetaBBPacketTag::PacketTagType::PPT_NODE_RECEIVED:
      return processTagNodeReceived(source, std::move(packet));
    case net::MetaBBPacketTag::PacketTagType::PPT_NOTIFICATION_ID:
      return processTagNotificationId(source, std::move(packet));
    case net::MetaBBPacketTag::PacketTagType::PPT_OUT_COLLECTING_MODE:
      return processTagOutCollectingMode(source, std::move(packet));
    case net::MetaBBPacketTag::PacketTagType::PPT_RACING_RAMP_UP_PARAMSET:
      return processTagWinnerRacingRampUpParamSet(source, std::move(packet));
    case net::MetaBBPacketTag::PacketTagType::PPT_RAMP_UP:
      return processTagRampUp(source, std::move(packet));
    case net::MetaBBPacketTag::PacketTagType::PPT_RESTART:
      return processTagRestart(source, std::move(packet));
    case net::MetaBBPacketTag::PacketTagType::PPT_RETRY_RAMP_UP:
      return processTagRetryRampUp(source, std::move(packet));
    case net::MetaBBPacketTag::PacketTagType::PPT_SOLUTION:
      return processTagSolution(source, std::move(packet));
    case net::MetaBBPacketTag::PacketTagType::PPT_TERMINATE_REQUEST:
      return processTagTerminateRequest(source, std::move(packet));
    case net::MetaBBPacketTag::PacketTagType::PPT_TERMINATE_SOLVING_TO_RESTART:
      return processTagTerminateSolvingToRestart(source, std::move(packet));
    case net::MetaBBPacketTag::PacketTagType::PPT_TEST_DUAL_BOUND_GAIN:
      return processTagTestDualBoundGain(source, std::move(packet));
    case net::MetaBBPacketTag::PacketTagType::PPT_WINNER:
      return processTagWinner(source, std::move(packet));
    case net::MetaBBPacketTag::PacketTagType::PTT_NODE:
      return processTagNode(source, std::move(packet));
    case net::MetaBBPacketTag::PacketTagType::PPT_LB_BOUND_TIGHTENED_INDEX:
      return processTagLbBoundTightened(source, std::move(packet));
    case net::MetaBBPacketTag::PacketTagType::PPT_UB_BOUND_TIGHTENED_INDEX:
      return processTagUbBoundTightened(source, std::move(packet));
    default:
      const std::string errMsg = "SolverImpl - no handler for given tag";
      spdlog::error(errMsg);
      throw std::runtime_error(errMsg);
  }
}  // operator()

int SolverImpl::processTagNode(int source, optnet::Packet::UPtr packet)
{
  if (pSolver->pNode)
  {
    pSolver->pNewNode = pSolver->pFramework->buildNode();
    pSolver->pNewNode->upload(pSolver->pFramework, std::move(packet));

    if (pSolver->pNewNode->getMergingStatus() == Node::NodeMergingStatus::NMS_CANNOT_BE_MERGED)
    {
      // This means, the received node is separated
      pSolver->pNewNode->setMergingStatus(Node::NodeMergingStatus::NMS_NO_MERGING_NODE);
    }
  }
  else
  {
    pSolver->pNode = pSolver->pFramework->buildNode();
    pSolver->pNode->upload(pSolver->pFramework, std::move(packet));
    if (pSolver->pNode->getMergingStatus() == Node::NodeMergingStatus::NMS_CANNOT_BE_MERGED)
     {
       // This means, the received node is separated
        pSolver->pNode->setMergingStatus(Node::NodeMergingStatus::NMS_NO_MERGING_NODE);
     }
  }

  pSolver->pAnotherNodeIsRequested = false;
  pSolver->pNumNodesReceived++;

  return metaconst::ERR_NO_ERROR;
}  // processTagNode

int SolverImpl::processTagNodeReceived(int source, optnet::Packet::UPtr packet)
{
  /*
   * The following has already been done with a general tag
   * on the caller side:
  utils::receiveTagMessageFromNetworkNode(
          pSolver->getFramework(),
          packet,
          net::MetaBBPacketTag::PacketTagType::PPT_NODE_RECEIVED,
          source);
  */
  return metaconst::ERR_NO_ERROR;
}  // processTagNodeReceived

int SolverImpl::processTagRampUp(int source, optnet::Packet::UPtr packet)
{
  (void) source;
  (void) packet;

  if (pSolver->isRacingStage())
  {
    assert(!pSolver->pRacingWinner);
  }
  pSolver->pRampUp = true;

  pSolver->pEnoughGainObtained = true;

  pSolver->setOriginalNodeSelectionStrategy();
  pSolver->pCollectingManyNodes = false;
  pSolver->pCollectingMode = false;
  pSolver->pAggressiveCollecting = false;
  pSolver->pNumSendInCollectingMode = 0;
  pSolver->pNoWaitModeSend = false;
  return metaconst::ERR_NO_ERROR;
}  // processTagRampUp

int SolverImpl::processTagRetryRampUp(int source, optnet::Packet::UPtr packet)
{
  (void) source;
  (void) packet;

  pSolver->pRampUp = false;

  pSolver->pCollectingMode = false;
  pSolver->pAggressiveCollecting = false;
  pSolver->pNumSendInCollectingMode = 0;
  pSolver->pNoWaitModeSend = false;

  return metaconst::ERR_NO_ERROR;
}  // processTagRetryRampUp

int SolverImpl::processTagSolution(int source, optnet::Packet::UPtr packet)
{
  (void) source;

  // Build and receive a solution from the source
  auto sol = pSolver->pFramework->buildSolution();
  sol->upload(std::move(packet));

  if (EPSLE(sol->getObjectiveFuntionValue(), pSolver->pGlobalBestIncumbentValue,
            metaconst::DEAFULT_NUM_EPSILON))
  {
    pSolver->pPendingSolution = sol;
    pSolver->pPendingIncumbentValue = pSolver->pPendingSolution->getObjectiveFuntionValue();
  }
  return metaconst::ERR_NO_ERROR;
}  // processTagSolution

int SolverImpl::processTagIncumbentValue(int source, optnet::Packet::UPtr packet)
{
  // The packet should contain the double incumbent value
  auto dpacket = optnet::castBaseToNetworkPacket<double>(std::move(packet->networkPacket));
  if (dpacket->size() != 1)
  {
    const std::string errMsg = "SolverImpl - processTagIncumbentValue: invalid data size, "
        "expected 1, received " + std::to_string(dpacket->size());
    spdlog::error(errMsg);
    return metaconst::ERR_GENERIC_ERROR;
  }
  const double incumbent = dpacket->data[0];
  pSolver->updateGlobalBestIncumbentValue(incumbent);

  return metaconst::ERR_NO_ERROR;
}  // processTagIncumbentValue

int SolverImpl::processTagGlobalBestDualBoundValueAtWarmStart(
    int source, optnet::Packet::UPtr packet)
{
  auto dpacket = optnet::castBaseToNetworkPacket<double>(std::move(packet->networkPacket));
  if (dpacket->size() != 1)
  {
    const std::string errMsg = "SolverImpl - processTagGlobalBestDualBoundValueAtWarmStart: "
        "invalid data size, expected 1, received " + std::to_string(dpacket->size());
    spdlog::error(errMsg);
    return metaconst::ERR_GENERIC_ERROR;
  }
  pSolver->pGlobalBestDualBoundValueAtWarmStart = dpacket->data[0];

  return metaconst::ERR_NO_ERROR;
}  // processTagGlobalBestDualBoundValueAtWarmStart

int SolverImpl::processTagNoNodes(int source, optnet::Packet::UPtr packet)
{
  pSolver->pAnotherNodeIsRequested = false;
  return metaconst::ERR_NO_ERROR;
}  // processTagNoNodes

int SolverImpl::processTagInCollectingMode(int source, optnet::Packet::UPtr packet)
{
  (void)source;

  // The packet should contain the double incumbent value
  auto ipacket = optnet::castBaseToNetworkPacket<int>(std::move(packet->networkPacket));
  if (ipacket->size() != 1)
  {
    const std::string errMsg = "SolverImpl - processTagInCollectingMode: "
        "invalid data size, expected 1, received " + std::to_string(ipacket->size());
    spdlog::error(errMsg);
    return metaconst::ERR_GENERIC_ERROR;
  }
  pSolver->pNumSendInCollectingMode = ipacket->data[0];

  if (pSolver->pNumSendInCollectingMode < 0)
  {
    pSolver->pNumSendInCollectingMode = (0 - pSolver->pNumSendInCollectingMode) - 1;
    pSolver->pAggressiveCollecting = true;
  }

  pSolver->pCollectingMode = true;

  return metaconst::ERR_NO_ERROR;
}  // processTagInCollectingMode

int SolverImpl::processTagCollectAllNodes(int source, optnet::Packet::UPtr packet)
{
  (void)source;

  auto ipacket = optnet::castBaseToNetworkPacket<int>(std::move(packet->networkPacket));
  if (ipacket->size() != 1)
  {
    const std::string errMsg = "SolverImpl - processTagCollectAllNodes: "
        "invalid data size, expected 1, received " + std::to_string(ipacket->size());
    spdlog::error(errMsg);
    return metaconst::ERR_GENERIC_ERROR;
  }
  pSolver->pNumCollectOnce = ipacket->data[0];

  pSolver->pCollectingManyNodes = true;
  pSolver->pNoWaitModeSend = true;

  return metaconst::ERR_NO_ERROR;
}  // processTagCollectAllNodes

int SolverImpl::processTagOutCollectingMode(int source, optnet::Packet::UPtr packet)
{
  (void)source;
  (void)packet;
  if (pSolver->pTerminationMode == solver::TerminationMode::TM_NO_TERMINATION)
  {
    pSolver->setOriginalNodeSelectionStrategy();
  }
  pSolver->pCollectingMode = false;
  pSolver->pAggressiveCollecting = false;
  pSolver->pNumSendInCollectingMode = 0;
  pSolver->pNoWaitModeSend = false;

  return metaconst::ERR_NO_ERROR;
}  // processTagOutCollectingMode

int SolverImpl::processTagLMBestBoundValue(int source, optnet::Packet::UPtr packet)
{
  (void)source;

  auto dpacket = optnet::castBaseToNetworkPacket<double>(std::move(packet->networkPacket));
  if (dpacket->size() != 1)
  {
    const std::string errMsg = "SolverImpl - processTagLMBestBoundValue: "
        "invalid data size, expected 1, received " + std::to_string(dpacket->size());
    spdlog::error(errMsg);
    return metaconst::ERR_GENERIC_ERROR;
  }
  pSolver->pLMBestDualBoundValue = dpacket->data[0];

  return metaconst::ERR_NO_ERROR;
}  // processTagLMBestBoundValue

int SolverImpl::processTagNotificationId(int source, optnet::Packet::UPtr packet)
{
  (void)source;

  auto uipacket = optnet::castBaseToNetworkPacket<int>(std::move(packet->networkPacket));
  if (uipacket->size() != 1)
  {
    const std::string errMsg = "SolverImpl - processTagNotificationId: "
        "invalid data size, expected 1, received " + std::to_string(uipacket->size());
    spdlog::error(errMsg);
    return metaconst::ERR_GENERIC_ERROR;
  }

  int notificationId = uipacket->data[0];
  if (notificationId == pSolver->pNotificationIdGenerator)
  {
    pSolver->pNotificationProcessed = false;
  }
  else
  {
    const std::string errMsg = "SolverImpl - processTagNotificationId: "
        "notificationId received is " + std::to_string(notificationId) +
        ", but generator value is " + std::to_string(pSolver->pNotificationIdGenerator);
    spdlog::error(errMsg);
    return metaconst::ERR_GENERIC_ERROR;
  }

  return metaconst::ERR_NO_ERROR;
}  // processTagNotificationId

int SolverImpl::processTagTerminateRequest(int source, optnet::Packet::UPtr packet)
{
  (void)source;
  (void)packet;
  pSolver->pTerminationMode = solver::TerminationMode::TM_NORMAL_TERMINATION;
  return metaconst::ERR_NO_ERROR;
}  // processTagTerminateRequest

int SolverImpl::processTagInterruptRequest(int source, optnet::Packet::UPtr packet)
{
  (void)source;

  auto ipacket = optnet::castBaseToNetworkPacket<int>(std::move(packet->networkPacket));
  if (ipacket->size() != 1)
  {
    const std::string errMsg = "SolverImpl - processTagInterruptRequest: "
        "invalid data size, expected 1, received " + std::to_string(ipacket->size());
    spdlog::error(errMsg);
    return metaconst::ERR_GENERIC_ERROR;
  }
  int exitSolerRequest = ipacket->data[0];

  pSolver->pTerminationMode = solver::TerminationMode::TM_INTERRUPTED_TERMINATION;
  if (exitSolerRequest == 1)
  {
    pSolver->pCollectingInterrupt = true;
    pSolver->setSendBackAllNodes();
  }
  if (exitSolerRequest == 2)
  {
    pSolver->pStayAliveAfterInterrupt = true;
  }
  else
  {
    pSolver->pStayAliveAfterInterrupt = false;
  }

  return metaconst::ERR_NO_ERROR;
}  // processTagInterruptRequest

int SolverImpl::processTagWinnerRacingRampUpParamSet(int source, optnet::Packet::UPtr packet)
{
  (void)source;

  // @note the received racing parameter set is always winner one,
  // because the initially racing parameter set is broadcasted
  pSolver->pWinnerRacingRampUpParamSet = pSolver->pFramework->buildRacingRampUpParamSet();
  pSolver->pWinnerRacingRampUpParamSet->upload(std::move(packet));

  // Interrupt racing (no point to race anymore)
  pSolver->pRacingInterruptIsRequested = true;
  if (pSolver->isRacingStage())
  {
     assert(!(pSolver->pRacingWinner));
     pSolver->pRacingIsInterrupted = true;
  }

  return metaconst::ERR_NO_ERROR;
}  // processTagWinnerRacingRampUpParamSet

int SolverImpl::processTagWinner(int source, optnet::Packet::UPtr packet)
{
  (void)source;
  (void)packet;

  // This is the winner solver
  pSolver->pRacingWinner = true;
  assert(!pSolver->pWinnerRacingRampUpParamSet);

  // LM does not send racing param to winner
  pSolver->pWinnerRacingRampUpParamSet = pSolver->pRacingRampUpParamSet;

  // No racing stage now
  pSolver->pRacingRampUpParamSet = nullptr;
  return metaconst::ERR_NO_ERROR;
}  // processTagWinner

int SolverImpl::processTagLightWeightRootNodeProcess(int source, optnet::Packet::UPtr packet)
{
  (void)source;
  (void)packet;
  pSolver->pLightWeightRootNodeComputation = true;
  pSolver->setLightWeightRootNodeProcess();

  return metaconst::ERR_NO_ERROR;
}  // processTagLightWeightRootNodeProcess

int SolverImpl::processTagBreaking(int source, optnet::Packet::UPtr packet)
{
  auto dpacket = optnet::castBaseToNetworkPacket<double>(std::move(packet->networkPacket));
  if (dpacket->size() != 2)
  {
    const std::string errMsg = "SolverImpl - processTagLMBestBoundValue: "
            "invalid data size, expected 2, received " + std::to_string(dpacket->size());
    spdlog::error(errMsg);
    return metaconst::ERR_GENERIC_ERROR;
  }

  // Target bound as first message
  pSolver->pTargetBound = dpacket->data[0];
  pSolver->pNumTransferNodeLimit = static_cast<int>(dpacket->data[1]);

  pSolver->pNumTransferredNodes = 0;
  pSolver->pCollectingManyNodes = true;

  return metaconst::ERR_NO_ERROR;
}  // processTagBreaking

int SolverImpl::processTagTerminateSolvingToRestart(int source, optnet::Packet::UPtr packet)
{
  (void)source;
  (void)packet;

  assert(pSolver->isRacingStage());
  pSolver->pRestartingRacing = true;
  pSolver->pRacingIsInterrupted = true;
  return metaconst::ERR_NO_ERROR;
}  // processTagTerminateSolvingToRestart

int SolverImpl::processTagGivenGapIsReached(int source, optnet::Packet::UPtr packet)
{
  (void)source;
  (void)packet;

  pSolver->pGivenGapIsReached = true;
  return metaconst::ERR_NO_ERROR;
}  // processTagGivenGapIsReached

int SolverImpl::processTagTestDualBoundGain(int source, optnet::Packet::UPtr packet)
{
  (void)source;
  (void)packet;

  pSolver->pTestDualBoundGain = true;
  pSolver->pEnoughGainObtained = true;
  return metaconst::ERR_NO_ERROR;
}  // processTagTestDualBoundGain

int SolverImpl::processTagNoTestDualBoundGain(int source, optnet::Packet::UPtr packet)
{
  (void)source;
  (void)packet;

  pSolver->pTestDualBoundGain = false;
  pSolver->pEnoughGainObtained = true;
  return metaconst::ERR_NO_ERROR;
}  // processTagNoTestDualBoundGain

int SolverImpl::processTagNoWaitModeSend(int source, optnet::Packet::UPtr packet)
{
  (void)source;
  (void)packet;

  pSolver->pNoWaitModeSend = true;
  return metaconst::ERR_NO_ERROR;
}  // processTagNoWaitModeSend

int SolverImpl::processTagRestart(int source, optnet::Packet::UPtr packet)
{
  (void)source;
  (void)packet;

  pSolver->pStayAliveAfterInterrupt = false;
  pSolver->pTerminationMode = solver::TerminationMode::TM_NO_TERMINATION;
  pSolver->pRampUp = false;
  return metaconst::ERR_NO_ERROR;
}  // processTagRestart

int SolverImpl::processTagLbBoundTightened(int source, optnet::Packet::UPtr packet)
{
  return pSolver->lowerBoundTightened(source, std::move(packet));
}  // processTagLbBoundTightened

int SolverImpl::processTagUbBoundTightened(int source, optnet::Packet::UPtr packet)
{
  return pSolver->upperBoundTightened(source, std::move(packet));
}  // processTagUbBoundTightened

int SolverImpl::processTagCutOffValue(int source, optnet::Packet::UPtr packet)
{
  double cutOffValue;
  auto dpacket = optnet::castBaseToNetworkPacket<double>(std::move(packet->networkPacket));
  if (dpacket->size() != 1)
  {
    const std::string errMsg = "SolverImpl - processTagLMBestBoundValue: "
        "invalid data size, expected 1, received " + std::to_string(dpacket->size());
    spdlog::error(errMsg);
    return metaconst::ERR_GENERIC_ERROR;
  }

  // Get cutoff value
  cutOffValue = dpacket->data[0];
  pSolver->updateGlobalBestCutOffValue(cutOffValue);
  return metaconst::ERR_NO_ERROR;
}  // processTagCutOffValue

}  // namespace metabb
}  // namespace optilab
