//
// Copyright OptiLab 2019-2020. All rights reserved.
//
// Base class for calculation state,
// i.e., the state of the computation of a solver.
//

#pragma once

#include <cstddef>  // for std::size_t
#include <cstdint>  // for uint64_t
#include <climits>
#include <limits>   // for std::numeric_limits
#include <memory>   // for std::shared_ptr

#include <boost/logic/tribool.hpp>

#include "meta_bb/framework.hpp"
#include "meta_bb/meta_bb_constants.hpp"
#include "meta_bb/meta_bb_struct.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace  metabb {

class SYS_EXPORT_CLASS CalculationState {
 public:
  using SPtr = std::shared_ptr<CalculationState>;

 public:
  /// Default constructor
  CalculationState() = default;

  /// Constructor:
  /// - compTimeMsec: computation time of this node in seconds
  /// - rootTimeMsec: computation time of the root node in seconds
  /// - numSolved: number of nodes solved
  /// - numNodesSent: number of nodes sent
  /// - numImprovedIncumbent: number of improved solutions generated in this solver
  /// - terminationState states whether or not this state/computation is in termination state
  /// - numNodesSolverWithoutPreprocess: number of nodes solved without pre-process
  /// - numSimplexIterRoot: number of simplex iteration at the root node
  /// - avgSimplexIter: average number of simplex iteration not at the root node
  /// - numTransferredLocalCuts: number of local cuts transferred from a node
  /// - minTransferredLocalCuts: minimum number of local cuts transferred from a node
  /// - maxTransferredLocalCuts: maximum number of local cuts transferred from a node
  /// - numRestarts: number of restarts
  /// - minInfeasibilitySum: minimum sum of integer infeasibility
  /// - maxInfeasibilitySum: maximum sum of integer infeasibility
  /// - minInfeasibilityNum: minimum number of integer infeasibility
  /// - maxInfeasibilityNum: maximum number of integer infeasibility
  /// - dualBound: dual bound value
  CalculationState(uint64_t compTimeMsec,
                   uint64_t rootTimeMsec,
                   int numNodesSolved,
                   int numNodesSent,
                   int numImprovedIncumbent,
                   solver::TerminationState terminationState,
                   int numNodesSolverWithoutPreprocess,
                   std::size_t numSimplexIterRoot,
                   double avgSimplexIter,
                   std::size_t numTransferredLocalCuts,
                   std::size_t minTransferredLocalCuts,
                   std::size_t maxTransferredLocalCuts,
                   std::size_t numRestarts,
                   double minInfeasibilitySum,
                   double maxInfeasibilitySum,
                   std::size_t minInfeasibilityNum,
                   std::size_t maxInfeasibilityNum,
                   double dualBound)
  : pCompTimeMsec(compTimeMsec),
    pRootTimeMsec(rootTimeMsec),
    pNodesSolved(numNodesSolved),
    pNodesSent(numNodesSent),
    pNumImprovedIncumbent(numImprovedIncumbent),
    pTerminationState(terminationState),
    pNumSolvedWithNoPreprocesses(numNodesSolverWithoutPreprocess),
    pNumSimplexIterRoot(numSimplexIterRoot),
    pAvgSimplexIter(avgSimplexIter),
    pNumTransferredLocalCuts(numTransferredLocalCuts),
    pMinTransferredLocalCuts(minTransferredLocalCuts),
    pMaxTransferredLocalCuts(maxTransferredLocalCuts),
    pNumRestarts(numRestarts),
    pMinInfeasibilitySum(minInfeasibilitySum),
    pMaxInfeasibilitySum(maxInfeasibilitySum),
    pMinInfeasibilityNum(minInfeasibilityNum),
    pMaxInfeasibilityNum(maxInfeasibilityNum),
    pDualBound(dualBound)
  {
  }

  virtual ~CalculationState() = default;

  /// Returns the computation time of this node in msec
  inline uint64_t getCompTimeMsec() const noexcept { return pCompTimeMsec; }

  /// Returns the computation time of the root node in msec
  inline uint64_t getRootTimeMsec() const noexcept { return pRootTimeMsec; }

  /// Returns the number of restarts
  inline int getNumRestarts() const noexcept { return pNumRestarts; }

  /// Returns the average node computation time without the root computation time
  inline double getAvgNodeCompTimeNoRoot() const noexcept
  {
    return (pNodesSolved > 1) ?
        ((pCompTimeMsec - pRootTimeMsec) / (pNodesSolved - 1)) : 0.0;
  }

  /// Returns the number of nodes solved
  inline int getNumNodesSolved() const noexcept { return pNodesSolved; }

  /// Returns the number of nodes sent
  inline int getNumNodesSent() const noexcept { return pNodesSent; }

  /// Returns the number of improved solutions generated by this solver
  inline int getNumImprovedIncumbent() const noexcept { return pNumImprovedIncumbent; }

  /// Returns the termination status of this node
  inline solver::TerminationState getTerminationState() const noexcept
  {
    return pTerminationState;
  }

  /// Returns the number of nodes solved without preprocess
  inline int getNumNodesSolvedWithNoPreprocess() const noexcept
  {
    return pNumSolvedWithNoPreprocesses;
  }

  /// Returns the value of the final dual bound value
  inline double getDualBoundValue() const noexcept { return pDualBound; }

  /// Sends this state to the destination using the given framework.
  /// Returns zero on success, non-zero otherwise
  virtual int send(const Framework::SPtr& fr, int destination) noexcept = 0;

  /// Uploads the content of the given network packet into this calculation state.
  /// Returns zero on success, non-zero otherwise
  virtual int upload(optnet::Packet::UPtr packet) noexcept = 0;

 protected:
  /**
   * TODO (Campeotto Federico)
   * Replace all the below class members with
   * a single protobuf object.
   */

  /// Computation time in seconds of this node
  uint64_t pCompTimeMsec{0};

  /// Computation time in seconds of the root node
  uint64_t pRootTimeMsec{0};

  /// Number of nodes solved
  int pNodesSolved{-1};

  /// Number of nodes sent
  int pNodesSent{-1};

  /// Number of improved solutions generated/found so far
  int pNumImprovedIncumbent{-1};

  /// Indicates whether this computation is in a termination state or not
  solver::TerminationState pTerminationState{solver::TerminationState::TS_TERMINATED_NORMALLY};

  /// Number of nodes solved with no pre-processes
  int pNumSolvedWithNoPreprocesses{-1};

  /// Number of simplex iterations at the root node
  int  pNumSimplexIterRoot{0};

  /// Average number of simplex iterations not at the root node
  double pAvgSimplexIter{0.0};

  /// Number of local cuts transferred from a node
  int pNumTransferredLocalCuts{0};

  /// Minimum number of local cuts transferred from a node
  int pMinTransferredLocalCuts{std::numeric_limits<int>::max()};

  /// Maximum number of local cuts transferred from a node
  int pMaxTransferredLocalCuts{std::numeric_limits<int>::lowest()};

  /// Number of restarts
  int pNumRestarts{0};

  /// Minimum sum of integer infeasibility
  double pMinInfeasibilitySum{0.0};

  /// Maximum sum of integer infeasibility
  double pMaxInfeasibilitySum{0.0};

  /// Minimum number of integer infeasibility
  int pMinInfeasibilityNum{0};

  /// Maximum number of integer infeasibility
  int pMaxInfeasibilityNum{0};

  /// Final dual bound value
  double pDualBound{std::numeric_limits<double>::lowest()};
};

}  // namespace metabb
}  // namespace optilab
