#include "meta_bb/net_framework.hpp"

#include <chrono>     // std::chrono::milliseconds
#include <stdexcept>  // for std::invalid_argument
#include <string>
#include <thread>     // for std::sleep_for
#include <utility>    // for std::move

#include <spdlog/spdlog.h>

#include "config/system_network_config.hpp"
#include "meta_bb/net_calculation_state.hpp"
#include "meta_bb/net_node.hpp"
#include "meta_bb/net_paramset.hpp"
#include "meta_bb/net_solver_state.hpp"
#include "meta_bb/net_solver_termination_state.hpp"
#include "optimizer_network/network_constants.hpp"
#include "optimizer_network/network_utilities.hpp"

namespace {

constexpr int kWaitInitCompletionMsec = 1000;

/// Utility function: returns the socket address given the socket port
std::string getSocketAddr(const int socketPort)
{
  std::string addr("tcp://");
  addr += "localhost:";
  addr += std::to_string(socketPort);
  return addr;
}
}  // namespace

namespace optilab {
namespace  metabb {

NetFramework::NetFramework(bool isRoot)
: Framework(),
  pIsRoot(isRoot),
  pIsInit(false)
{
}

NetFramework::~NetFramework()
{
  // Run teardown
  tearDown();
}

int NetFramework::initConnections() noexcept
{
  // Initialize the sockets and bing the socket connectors
  try
  {
    initSockets();
  }
  catch(...)
  {
    std::string errMsg = "NetFramework - init ";
    if (pIsRoot) errMsg += "(root): ";
    else errMsg += ": ";
    errMsg += "error in initializing the sockets";
    spdlog::error(errMsg);
    return 1;
  }

  // Initialize hub and connector
  if (pIsRoot)
  {
    // Special root framework initialization
    try
    {
      initRootFramework();
    }
    catch(...)
    {
      spdlog::error("NetFramework - init (root): error in root initialization");
      return 1;
    }

    if (!pHub || !pHub->isRunning())
    {
      spdlog::error("NetFramework - init (root): HUB not running");
      return 1;
    }
  }
  else
  {
    // Standard non-root framework initialization
    try
    {
      initFramework();
    }
    catch(...)
    {
      spdlog::error("NetFramework - init: error in initialization");
      return 1;
    }
  }

  // Framework is now initialized
  pIsInit = true;

  return 0;
}  // initConnections

int NetFramework::tearDown() noexcept
{
  if (!pIsInit) return 0;

  // If this is the root, send the abort message
  // to all other nodes in the network
  try
  {
    if (pIsRoot)
    {
      this->getComm()->abort();
    }

    // Turn down
    pCommunicator->turnDown();

    // Reset resources
    pCommunicator.reset();

    if (pIsRoot)
    {
      pHub.reset();
    }
  }
  catch(...)
  {
    spdlog::error("NetFramework - teardown: error");
    return 1;
  }

  // This framework is not initialized anymore
  pIsInit = false;
  return 0;
}  // tearDown

void NetFramework::abortConnections() const
{
  if (!pIsInit)
  {
    throw std::runtime_error("NetFramework - abort: framework has not been initialized");
  }

  pCommunicator->abort();
}  // abortConnections

void NetFramework::initRootFramework()
{
  const auto& config = SystemNetworkConfig::getInstance().getMetaBBNetworkConfig();

  // Instantiate the hub
  pHub = std::make_shared<optnet::NetworkHub>(
          config.ptpSend, config.ptpRecv, config.bcastSend, config.bcastRecv);

  // Run the hub
  pHub->run();
  if (!pHub->isRunning())
  {
    throw std::runtime_error("NetFramework - initRootFramework: not able to run the HUB");
  }

  // Instantiate the network communicator
  const bool setRoot{true};
  const bool setSynchOnInit{true};
  pCommunicator = std::make_shared<optilab::optnet::NetworkCommunicator>(
          std::move(pConnRegister.pPToPSendConn),
          std::move(pConnRegister.pPToPRecvConn),
          std::move(pConnRegister.pBcastSendConn),
          std::move(pConnRegister.pBcastRecvConn),
          std::move(pConnRegister.pContext),
          setRoot, setSynchOnInit);

  // Set the communicator on the parent class
  setNetworkCommunicator(pCommunicator);

  // Wait on this thread for other nodes to initialize before
  // sending the network synchronization message
  const auto waitBeforeSynchNetworkMsec = config.connectSynchNetworkWaitTimeMsec;
  std::this_thread::sleep_for(std::chrono::milliseconds(waitBeforeSynchNetworkMsec));

  // Initialize the network communicator
  const auto initComm = pCommunicator->init();
  if (initComm != optilab::optnet::networkconst::ERR_NO_ERROR)
  {
    throw std::runtime_error("NetFramework - initRootFramework: "
            "not able to initialized the communicator");
  }

  // Wait for initialization to complete
  std::this_thread::sleep_for(std::chrono::milliseconds(kWaitInitCompletionMsec));
}  // initRootFramework

void NetFramework::initFramework()
{
  const auto& config = SystemNetworkConfig::getInstance().getMetaBBNetworkConfig();

  // Instantiate the network communicator
  pCommunicator = std::make_shared<optilab::optnet::NetworkCommunicator>(
          std::move(pConnRegister.pPToPSendConn),
          std::move(pConnRegister.pPToPRecvConn),
          std::move(pConnRegister.pBcastSendConn),
          std::move(pConnRegister.pBcastRecvConn),
          std::move(pConnRegister.pContext));

  // Set the communicator on the parent class
  setNetworkCommunicator(pCommunicator);

  // Initialize the network communicator
  pCommunicator->init(config.connectTimeoutMsec);

  // Wait for initialization to complete
  std::this_thread::sleep_for(std::chrono::milliseconds(kWaitInitCompletionMsec));
}  // initRootFramework

void NetFramework::initSockets()
{
  const auto& config = SystemNetworkConfig::getInstance().getMetaBBNetworkConfig();

  // Create the context
  pConnRegister.pContext.reset(
          new zmq::context_t(optilab::optnet::networkconst::CONTEXT_DEFAULT_NUM_THREADS));

  // Create the sockets
  pConnRegister.pSendSocket = optnet::utils::buildPointToPointSocket(*pConnRegister.pContext);
  pConnRegister.pRecvSocket = optnet::utils::buildPointToPointSocket(*pConnRegister.pContext);
  pConnRegister.pBroadcastSendSocket = optnet::utils::buildSenderBroadcastSocket(
          *pConnRegister.pContext);
  pConnRegister.pBroadcastRecvSocket = optnet::utils::buildReceivingBroadcastSocket(
          *pConnRegister.pContext);

  // Set timeout on connections if specified
  const auto connectTimeout = config.connectTimeoutMsec;
  if (connectTimeout > 0)
  {
    pConnRegister.pSendSocket->setsockopt(
            ZMQ_CONNECT_TIMEOUT, &connectTimeout, sizeof(connectTimeout));
    pConnRegister.pRecvSocket->setsockopt(
            ZMQ_CONNECT_TIMEOUT, &connectTimeout, sizeof(connectTimeout));
    pConnRegister.pBroadcastSendSocket->setsockopt(
            ZMQ_CONNECT_TIMEOUT, &connectTimeout, sizeof(connectTimeout));
    pConnRegister.pBroadcastRecvSocket->setsockopt(
            ZMQ_CONNECT_TIMEOUT, &connectTimeout, sizeof(connectTimeout));
  }

  try
  {
    spdlog::info(std::string("NetFramework - initSockets: "
            "PToP send connect to input address ") + std::to_string(config.ptpSend));
    pConnRegister.pSendSocket->connect(getSocketAddr(config.ptpSend).c_str());

    spdlog::info(std::string("NetFramework - initSockets: "
            "PToP recv connect to input address ") + std::to_string(config.ptpRecv));
    pConnRegister.pRecvSocket->connect(getSocketAddr(config.ptpRecv).c_str());

    spdlog::info(std::string("NetFramework - initSockets: "
            "Bcast send connect to input address ") + std::to_string(config.bcastSend));
    pConnRegister.pBroadcastSendSocket->connect(getSocketAddr(config.bcastSend).c_str());

    spdlog::info(std::string("NetFramework - initSockets: "
            "Bcast recv connect to input address ") + std::to_string(config.bcastRecv));
    pConnRegister.pBroadcastRecvSocket->connect(getSocketAddr(config.bcastRecv).c_str());

    // Subscribe to all messages
    pConnRegister.pBroadcastRecvSocket->setsockopt(ZMQ_SUBSCRIBE, "", 0);
  }
  catch(...)
  {
    spdlog::error("NetFramework - initSockets: cannot connect sockets");
    throw;
  }

  // Create a new socket connector to start listening for incoming messages
  pConnRegister.pPToPSendConn.reset(new optnet::SocketConnector(pConnRegister.pSendSocket));
  pConnRegister.pPToPRecvConn.reset(new optnet::SocketConnector(pConnRegister.pRecvSocket));
  pConnRegister.pBcastSendConn.reset(
      new optnet::BroadcastSocketConnector(pConnRegister.pBroadcastSendSocket));
  pConnRegister.pBcastRecvConn.reset(
      new optnet::BroadcastSocketConnector(pConnRegister.pBroadcastRecvSocket));
}  // initSockets

std::shared_ptr<ParamSet> NetFramework::buildParamSet()
{
  return std::make_shared<NetParamSet>();
}  //buildParamSet

std::shared_ptr<CalculationState> NetFramework::buildCalculationState()
{
  return std::make_shared<NetCalculationState>();
}  // buildCalculationState

std::shared_ptr<CalculationState> NetFramework::buildCalculationState(
    uint64_t compTimeMsec,
    uint64_t rootTimeMsec,
    int numNodesSolved,
    int numNodesSent,
    int numImprovedIncumbent,
    solver::TerminationState terminationState,
    int numNodesSolverWithoutPreprocess,
    int numSimplexIterRoot,
    double avgSimplexIter,
    int numTransferredLocalCuts,
    int minTransferredLocalCuts,
    int maxTransferredLocalCuts,
    int numRestarts,
    double minInfeasibilitySum,
    double maxInfeasibilitySum,
    int minInfeasibilityNum,
    int maxInfeasibilityNum,
    double dualBound)
{
  return std::make_shared<NetCalculationState>(
          compTimeMsec,
          rootTimeMsec,
          numNodesSolved,
          numNodesSent,
          numImprovedIncumbent,
          terminationState,
          numNodesSolverWithoutPreprocess,
          numSimplexIterRoot,
          avgSimplexIter,
          numTransferredLocalCuts,
          minTransferredLocalCuts,
          maxTransferredLocalCuts,
          numRestarts,
          minInfeasibilitySum,
          maxInfeasibilitySum,
          minInfeasibilityNum,
          maxInfeasibilityNum,
          dualBound);
}  // buildCalculationState

std::shared_ptr<Node> NetFramework::buildNode()
{
  return std::make_shared<NetNode>();
}  // buildNode

std::shared_ptr<Node> NetFramework::buildNode(
    NodeId nodeId,
    NodeId generatorNodeId,
    int depth,
    double dualBoundValue,
    double originalDualBoundValue,
    double estimatedValue,
    std::shared_ptr<DiffSubproblem> diffSubproblem)
{
  return std::make_shared<NetNode>(
          nodeId,
          generatorNodeId,
          depth,
          dualBoundValue,
          originalDualBoundValue,
          estimatedValue,
          diffSubproblem);
}  // buildNode

std::shared_ptr<SolverState> NetFramework::buildSolverState()
{
  return std::make_shared<NetSolverState>();
}  // buildSolverState

std::shared_ptr<SolverState> NetFramework::buildSolverState(
        int racingStage,
        unsigned int notificationId,
        int LMId,
        int globalSubtreeId,
        long long nodesSolved,
        int nodesLeft,
        double bestDualBoundValue,
        double globalBestPrimalBoundValue,
        uint64_t timeMsec,
        double averageDualBoundGain)
{
  return std::make_shared<NetSolverState>(
          racingStage,
          notificationId,
          LMId,
          globalSubtreeId,
          nodesSolved,
          nodesLeft,
          bestDualBoundValue,
          globalBestPrimalBoundValue,
          timeMsec,
          averageDualBoundGain);
}  // buildSolverState

std::shared_ptr<SolverTerminationState> NetFramework::buildSolverTerminationState()
{
  return std::make_shared<NetSolverTerminationState>();
}  // buildSolverTerminationState

  /// Builds and returns a solver termination state with specified values
std::shared_ptr<SolverTerminationState> NetFramework::buildSolverTerminationState(
        solver::InterruptionPoint interrupted,
        int rank,
        int totalNumSolved,
        int minNumSolved,
        int maxNumSolved,
        int totalNumSent,
        int totalNumImprovedIncumbent,
        int numNodesReceived,
        int numNodesSolved,
        int numNodesSolvedAtRoot,
        int numNodesSolvedAtPreCheck,
        int numTransferredLocalCutsFromSolver,
        int minNumTransferredLocalCutsFromSolver,
        int maxNUmTransferredLocalCutsFromSolver,
        int numTotalRestarts,
        int minNumRestarts,
        int maxNumRestarts,
        int numVarTightened,
        int numIntVarTightenedInt,
        uint64_t runningTimeMsec,
        uint64_t idleTimeToFirstNodeMsec,
        uint64_t idleTimeBetweenNodesMsec,
        uint64_t idleTimeAfterLastNodeMsec,
        uint64_t idleTimeToWaitNotificationIdMsec,
        uint64_t idleTimeToWaitAckCompletionMsec,
        uint64_t idleTimeToWaitTokenMsec,
        uint64_t totalRootNodeTimeMsec,
        uint64_t minRootNodeTimeMsec,
        uint64_t maxRootNodeTimeMsec)
{
  return std::make_shared<NetSolverTerminationState>(
          interrupted,
          rank,
          totalNumSolved,
          minNumSolved,
          maxNumSolved,
          totalNumSent,
          totalNumImprovedIncumbent,
          numNodesReceived,
          numNodesSolved,
          numNodesSolvedAtRoot,
          numNodesSolvedAtPreCheck,
          numTransferredLocalCutsFromSolver,
          minNumTransferredLocalCutsFromSolver,
          maxNUmTransferredLocalCutsFromSolver,
          numTotalRestarts,
          minNumRestarts,
          maxNumRestarts,
          numVarTightened,
          numIntVarTightenedInt,
          runningTimeMsec,
          idleTimeToFirstNodeMsec,
          idleTimeBetweenNodesMsec,
          idleTimeAfterLastNodeMsec,
          idleTimeToWaitNotificationIdMsec,
          idleTimeToWaitAckCompletionMsec,
          idleTimeToWaitTokenMsec,
          totalRootNodeTimeMsec,
          minRootNodeTimeMsec,
          maxRootNodeTimeMsec);
}  // buildSolverTerminationState

}  // namespace metabb
}  // namespace optilab
