#include "scheduling_toolbox/scheduling_processor_factory.hpp"

#include <stdexcept>  // for std::invalid_argument
#include <string>

#include "scheduling_toolbox/employee_scheduling_processor.hpp"
#include "scheduling_toolbox/job_shop_processor.hpp"

namespace optilab {
namespace toolbox {

SchedulingProcessorFactory::SchedulingProcessorFactory(
        schedulingengine::SchedulingResult::SPtr result)
: pResult(result),
  pProcessorType(schedulingengine::SchedulingProcessorType::SPT_UNDEF)
{
  if (!pResult)
  {
    throw std::runtime_error("SchedulingProcessorFactory - empty result");
  }
}

SchedulingProcessor::SPtr SchedulingProcessorFactory::buildProcessor()
{
  SchedulingProcessor::SPtr proc;
  switch(pProcessorType)
  {
    case schedulingengine::SchedulingProcessorType::SPT_EMPLOYEES_SCHEDULING:
    {
      proc = std::make_shared<EmployeeSchedulingProcessor>(pResult);
      break;
    }
    case schedulingengine::SchedulingProcessorType::SPT_JOB_SHOP_SCHEDULING:
    {
      proc = std::make_shared<JobShopProcessor>(pResult);
      break;
    }
    default:
      throw std::runtime_error("SchedulingProcessorFactory - buildProcessor: "
              "unrecognized processor type " + std::to_string(static_cast<int>(pProcessorType)));
  }
  return proc;
}  // buildProcessor

}  // namespace toolbox
}  // namespace optilab
