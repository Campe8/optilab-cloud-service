#include "scheduling_toolbox/scheduling_optimizer.hpp"

#include <spdlog/spdlog.h>

#include <cassert>
#include <stdexcept>  // for std::invalid_argument
#include <utility>    // for std::move

namespace optilab {
namespace toolbox {

SchedulingOptimizer::SchedulingOptimizer(const std::string& optimizerName,
                                         const SchedulingProcessorFactory& procFactory,
                                         MetricsRegister::SPtr metricsRegister)
    : BaseClass(optimizerName),
      pSchedulingInstance(nullptr),
      pSchedulingProcessor(nullptr),
      pProcessorFactory(procFactory),
      pMetricsRegister(metricsRegister)
{
  if (!pMetricsRegister)
  {
    throw std::invalid_argument(
        "SchedulingOptimizer - empty pointer to the metrics register");
  }

  // Initialize the engine,
  // e.g., the internal pipeline
  BaseClass::init();
}

void SchedulingOptimizer::loadInstance(SchedulingInstance::UPtr instance)
{
  // Load the model into the optimizer
  assert(instance);
  pSchedulingProcessor->loadInstanceAndCreateSolver(std::move(instance));
}  // loadModel

void SchedulingOptimizer::runOptimizer()
{
  if (!isActive())
  {
    const std::string errMsg =
        "SchedulingOptimizer - runOptimizer: optimizer is not active";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  Work::SPtr work = std::make_shared<Work>(
      schedulingengine::SchedulingWork::WorkType::kRunOptimizer, pMetricsRegister);
  pushTask(work);
}  // runOptimizer

bool SchedulingOptimizer::interruptOptimizer()
{
  assert(pSchedulingProcessor);
  return pSchedulingProcessor->interruptSolver();
}  // interruptOptimizer

ProcessorPipeline::SPtr SchedulingOptimizer::buildPipeline(int)
{
  ProcessorPipeline::SPtr pipeline = std::make_shared<ProcessorPipeline>();
  pSchedulingProcessor = pProcessorFactory.buildProcessor();
  pipeline->pushBackProcessor(pSchedulingProcessor);
  return pipeline;
}  // buildPipeline

}  // namespace toolbox
}  // namespace optilab
