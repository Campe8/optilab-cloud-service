//
// Copyright OptiLab 2020. All rights reserved.
//
// Base class for a scheduling instance.
//

#pragma once

#include <memory>  // for std::unique_ptr

#include "optilab_protobuf/scheduling_model.pb.h"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace toolbox {

class SYS_EXPORT_CLASS SchedulingInstance {
 public:
  using UPtr = std::unique_ptr<SchedulingInstance>;
  using SPtr = std::shared_ptr<SchedulingInstance>;

 public:
  SchedulingInstance(const SchedulingModelProto& schedulingModel)
 : pSchedulingModel(schedulingModel)
 {
 }

  inline const SchedulingModelProto& getSchedulingModel() const noexcept
  {
    return pSchedulingModel;
  }

 private:
  /// Protobuf containing the scheduling model
  SchedulingModelProto pSchedulingModel;
};

}  // namespace toolbox
}  // namespace optilab
