//
// Copyright OptiLab 2020. All rights reserved.
//
// Constraint Programming (CP) processor for Job Shop
// problems used by scheduling engines.
// The CP processor is based on the Google OR-Tools framework
// and it uses it directly as a base solver.
// For more information, see
// https://developers.google.com/optimization/scheduling
//

#pragma once

#include <memory>   // for std::shared_ptr
#include <string>
#include <map>
#include <utility>  // for std::pair
#include <vector>

#include <boost/thread.hpp>

#include "or_tools_engine/job_shop_solver.hpp"
#include "scheduling_toolbox/scheduling_instance.hpp"
#include "scheduling_toolbox/scheduling_processor.hpp"
#include "scheduling_toolbox/scheduling_utilities.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace toolbox {

class SYS_EXPORT_CLASS JobShopProcessor : public SchedulingProcessor {
public:
  using SPtr = std::shared_ptr<JobShopProcessor>;

public:
  JobShopProcessor(schedulingengine::SchedulingResult::SPtr result);

  /// Loads the scheduling model and creates a solver to run on it.
  /// @note throw std::invalid_argument on empty instance
  void loadInstanceAndCreateSolver(SchedulingInstance::UPtr instance) override;

  /// Interrupts the internal solving process (if running).
  /// The solver remains in a non-executable state until another model is loaded
  bool interruptSolver() override;

protected:
  using Work = schedulingengine::SchedulingWork::SPtr;

  /// Lock type on the internal mutex
  using LockGuard = boost::lock_guard<boost::mutex>;

protected:
  void processWork(Work work) override;

private:
  /// ORTools solver used to solve job-shop problems
  JobShopSolver::SPtr pSolver;

  /// Pointer to the downcast VRP result instance
  schedulingengine::JobShopResult::SPtr pResult;

  /// Mutex synch.ing the state of the result
  boost::mutex pResultMutex;

  /// Utility function: runs the solver
  void runSolver(Work& work);

  /// Utility function: store solution
  void storeSolution();
};

}  // namespace toolbox
}  // namespace optilab
