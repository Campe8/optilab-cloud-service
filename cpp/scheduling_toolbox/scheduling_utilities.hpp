//
// Copyright OptiLab 2020. All rights reserved.
//
// Utilities for the scheduling toolbox.
//

#pragma once

#include <cstdint>  // for int64_t
#include <memory>   // for std::shared_ptr
#include <vector>

#include "data_structure/metrics/metrics_register.hpp"
#include "optilab_protobuf/job_shop_model.pb.h"
#include "optilab_protobuf/scheduling_model.pb.h"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace toolbox {

namespace schedulingengine {
enum SchedulingProcessorType : int {
  /// Processor based for the Employees scheduling problem
  SPT_EMPLOYEES_SCHEDULING = 0,

  /// Processor for the Job-Shop scheduling problem
  SPT_JOB_SHOP_SCHEDULING,

  /// Undefined scheduling processor type.
  /// @note this is the default if the processor type
  /// is not specified
  SPT_UNDEF
};

struct SYS_EXPORT_STRUCT SchedulingWork {
  using SPtr = std::shared_ptr<SchedulingWork>;

  enum WorkType {
    /// Runs the optimizer on the loaded model
    kRunOptimizer,
    kWorkTypeUndef
  };

  SchedulingWork(WorkType wtype, MetricsRegister::SPtr metReg)
      : workType(wtype),
        metricsRegister(metReg) {
  }

  WorkType workType;
  MetricsRegister::SPtr metricsRegister;
};

class SYS_EXPORT_CLASS SchedulingEvent {
 public:
  enum EventType {
    /// Starts the solving process
    kRunEngine,
    /// Kills any ongoing computation
    kInterruptEngine,
    /// Collects solutions
    kSolutions
  };

 public:
  explicit SchedulingEvent(EventType aType)
      : pEventType(aType) {
  }

  inline EventType getType() const noexcept {
    return pEventType;
  }
  inline void setNumSolutions(int numSolutions) noexcept {
    pNumSolutions = numSolutions;
  }
  inline int getNumSolutions() const noexcept {
    return pNumSolutions;
  }

 private:
  /// Type of this event
  EventType pEventType;

  /// Number of solutions for "kSolutions" events
  int pNumSolutions { 0 };
};

struct SYS_EXPORT_STRUCT SchedulingResult {
  /// Result status
  enum ResultStatus {
    /// Success on solution
    SUCCESS = 0,

    /// Failure in finding a solution
    FAIL,

    /// Failure in finding a solution due to timeout
    FAIL_TIMEOUT,

    /// Model proven to be invalid
    INVALID,

    /// Model not yet solved
    NOT_SOLVED
  };

  using SPtr = std::shared_ptr<SchedulingResult>;

  virtual ~SchedulingResult() = default;

  /// Name of the instance (problem that has been solved)
  std::string instanceName;
};

struct SYS_EXPORT_STRUCT EmployeesSchedulingResult : public SchedulingResult {
  using SPtr = std::shared_ptr<EmployeesSchedulingResult>;

  struct EmployeeDayShift {
    int employeeId{-1};
    int shift{-1};
    bool requestSatisfied{false};
  };

  struct EmployeeDaySchedule {
    /// Identifier for the day of the schedule
    int day{-1};

    /// Shifts of the day
    std::vector<EmployeeDayShift> shiftSchedule;
  };

  /// Status of the solver upon termination
  SchedulingResult::ResultStatus status{SchedulingResult::ResultStatus::NOT_SOLVED};

  /// Shift schedule for each day
  std::vector<EmployeeDaySchedule> daySchedule;

  /// Clear the results
  void clear() {
    daySchedule.clear();
    status = SchedulingResult::ResultStatus::NOT_SOLVED;
  }
};

struct SYS_EXPORT_STRUCT JobShopResult : public SchedulingResult {
  using SPtr = std::shared_ptr<JobShopResult>;

  /// Status of the solver upon termination
  SchedulingResult::ResultStatus status{SchedulingResult::ResultStatus::NOT_SOLVED};

  /// Protobuf Job-Shop solution
  toolbox::JsspOutputSolution jsSolution;

  /// Clear the results
  void clear() {
    status = SchedulingResult::ResultStatus::NOT_SOLVED;
  }
};

}  // namespace schedulingengine

namespace schedulingutils {

/// Builds a protobuf object representing the Employees Scheduling solution given as parameter
SYS_EXPORT_FCN SchedulingSolutionProto buildEmployeesSchedulingSolutionProto(
        schedulingengine::EmployeesSchedulingResult::SPtr res);

/// Builds a protobuf object representing the JobShop solution given as parameter
SYS_EXPORT_FCN SchedulingSolutionProto buildJobShopSolutionProto(
        schedulingengine::JobShopResult::SPtr res);

}  // namespace schedulingutils
}  // namespace toolbox

}  // namespace optilab
