#include "scheduling_toolbox/scheduling_utilities.hpp"

#include <stdexcept>  // for std::runtime_error

#include "engine/engine_constants.hpp"

namespace optilab {
namespace toolbox {

namespace schedulingutils {

SchedulingSolutionProto buildEmployeesSchedulingSolutionProto(
        schedulingengine::EmployeesSchedulingResult::SPtr res)
{
  if (!res)
  {
    throw std::runtime_error("buildEmployeesSchedulingSolutionProto - empty result");
  }

  SchedulingSolutionProto sol;
  switch (res->status)
  {
    case schedulingengine::SchedulingResult::ResultStatus::SUCCESS:
      sol.set_status(OptimizerSolutionStatusProto::OPT_SOLVER_SUCCESS);
      break;
    case schedulingengine::SchedulingResult::ResultStatus::FAIL:
      sol.set_status(OptimizerSolutionStatusProto::OPT_SOLVER_FAIL);
      break;
    case schedulingengine::SchedulingResult::ResultStatus::NOT_SOLVED:
      sol.set_status(OptimizerSolutionStatusProto::OPT_SOLVER_NOT_SOLVED);
      break;
    case schedulingengine::SchedulingResult::ResultStatus::FAIL_TIMEOUT:
      sol.set_status(OptimizerSolutionStatusProto::OPT_SOLVER_FAIL_TIMEOUT);
      break;
    case schedulingengine::SchedulingResult::ResultStatus::INVALID:
      sol.set_status(OptimizerSolutionStatusProto::OPT_SOLVER_MODEL_INVALID);
      break;
    default:
      sol.set_status(OptimizerSolutionStatusProto::OPT_SOLVER_UNKNOWN_STATUS);
      break;
  }

  // Get the Employees scheduling
  auto esched = sol.mutable_employee_scheduling_solution();
  for (const auto& dsched : res->daySchedule)
  {
    auto daySched = esched->add_day_schedule();
    daySched->set_day(dsched.day);
    for (const auto& ssched : dsched.shiftSchedule)
    {
      auto shiftSched = daySched->add_shift_schedule();
      shiftSched->set_employee_id(ssched.employeeId);
      shiftSched->set_shift(ssched.shift);
      shiftSched->set_satisfied(ssched.requestSatisfied);
    }
  }

  // Return the protobuf solution
  return sol;
}  // buildEmployeesSchedulingSolutionProto

SchedulingSolutionProto buildJobShopSolutionProto(schedulingengine::JobShopResult::SPtr res)
{
  if (!res)
  {
    throw std::runtime_error("buildJobShopSolutionProto - empty result");
  }

  SchedulingSolutionProto sol;
  return sol;
}

}  // namespace schedulingutils

}  // namespace toolbox
}  // namespace optilab
