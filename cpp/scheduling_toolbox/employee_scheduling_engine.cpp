#include "scheduling_toolbox/employee_scheduling_engine.hpp"

#include <spdlog/spdlog.h>

#include <stdexcept>  // for std::runtime_error
#include <utility>    // for std::move

#include "data_structure/json/json.hpp"
#include "engine/engine_constants.hpp"
#include "engine/engine_utils.hpp"
#include "scheduling_toolbox/scheduling_processor_factory.hpp"
#include "scheduling_toolbox/scheduling_utilities.hpp"
#include "utilities/timer.hpp"

namespace {
/// Waiting value for an idle processor
constexpr int kWaitOnIdleProcessor{-1};
}  // namespace

namespace optilab {
namespace toolbox {

EmployeesSchedulingEngine::EmployeesSchedulingEngine(
        const std::string& engineId, const EngineCallbackHandler::SPtr& handler,
        schedulingengine::SchedulingProcessorType schedulingProcessorType)
: pEngineId(engineId),
  pCallbackHandler(handler)

{
  if (engineId.empty())
  {
    const std::string errMsg = "EmployeesSchedulingEngine - empty engine identifier";
    throw std::invalid_argument(errMsg);
  }

  if (!handler)
  {
    const std::string errMsg = "EmployeesSchedulingEngine - engine callback handler";
    throw std::invalid_argument(errMsg);
  }

  timer::Timer timer;

  // Instantiate scheduling result and metrics register
  pSchedulingResult = std::make_shared<schedulingengine::EmployeesSchedulingResult>();
  pMetricsRegister = std::make_shared<MetricsRegister>();

  // Builds the optimizer running ORTools scheduling models
  buildOptimizer(schedulingProcessorType);
  assert(pOptimizer);

  // Start  the asynchronous thread in the optimizer
  pOptimizer->startUp();

  pActiveEngine = true;

  pEngineCreationTimeMsec = timer.getWallClockTimeMsec();
}

EmployeesSchedulingEngine::~EmployeesSchedulingEngine()
{
  try
  {
    turnDown();
  }
  catch (...)
  {
    // Do not throw in destructor
    spdlog::warn("EmployeesSchedulingEngine - thrown in destructor");
  }
}

void EmployeesSchedulingEngine::turnDown()
{
  if (pActiveEngine)
  {
    pActiveEngine = false;
    pOptimizer->tearDown();
    pOptimizer.reset();
  }
}  // turnDown

void EmployeesSchedulingEngine::buildOptimizer(schedulingengine::SchedulingProcessorType procType)
{
  // Create and set the factory for ORTools CP processors
  SchedulingProcessorFactory procFactory(pSchedulingResult);
  procFactory.setProcessorType(procType);
  pOptimizer = std::make_shared<SchedulingOptimizer>(pEngineId, procFactory, pMetricsRegister);
}  // buildOptimizer

void EmployeesSchedulingEngine::registerInstance(SchedulingInstance::UPtr instance)
{
  if (!instance)
  {
    throw std::runtime_error("EmployeesSchedulingEngine - registerInstance: "
        "registering an empty instance for engine: " + pEngineId);
  }

  // Start timer for loading the model
  timer::Timer timer;

  // Load the model into the optimizer
  try
  {
    const bool keepJSONFormat = true;
    pOptimizer->loadInstance(std::move(instance));
  }
  catch (std::exception& ex)
  {
    const std::string errMsg =
        "EmployeesSchedulingEngine - registerModel: "
        "error while loading the model on engine " +
        pEngineId + " " + std::string(ex.what());
    spdlog::error(errMsg);
    throw;
  }
  catch (...)
  {
    const std::string errMsg =
        "EmployeesSchedulingEngine - registerModel: "
        "undefined error while loading the model on engine " +
        pEngineId;
    spdlog::error(errMsg);
    throw;
  }

  pLoadModelLatencyMsec = timer.getWallClockTimeMsec();

  // Reset solution
  std::dynamic_pointer_cast<schedulingengine::EmployeesSchedulingResult>(pSchedulingResult)->clear();
}  // registerModel

void EmployeesSchedulingEngine::engineWait(int timeoutMsec) {
  if (pActiveEngine)
  {
    pOptimizer->waitOnTaskCompletion(timeoutMsec);
  }
}  // engineWait

void EmployeesSchedulingEngine::notifyEngine(const schedulingengine::SchedulingEvent& event)
{
  switch (event.getType())
  {
    case schedulingengine::SchedulingEvent::EventType::kRunEngine:
    {
      processRunModelEvent();
      break;
    }
    case schedulingengine::SchedulingEvent::EventType::kSolutions:
    {
      processCollectSolutionsEvent(event.getNumSolutions());
      break;
    }
    default:
    {
      assert(event.getType() ==
              schedulingengine::SchedulingEvent::EventType::kInterruptEngine);
      processInterruptEngineEvent();
      break;
    }
  }
}  // notifyEngine

void EmployeesSchedulingEngine::processRunModelEvent() {
  // Return if the engine is not active
  if (!pActiveEngine)
  {
    spdlog::warn(
        "EmployeesSchedulingEngine - processRunModelEvent: inactive engine, returning " +
        pEngineId);
    return;
  }

  timer::Timer timer;

  // Wait on previous tasks to complete.
  // For example, it waits on model registration
  engineWait(kWaitOnIdleProcessor);

  // Trigger the engine to run and return
  pOptimizer->runOptimizer();

  // Latency for running the optimizer
  pOptimizerLatencyMsec = timer.getWallClockTimeMsec();
}  // processRunModelEvent

void EmployeesSchedulingEngine::processInterruptEngineEvent()
{
  if (!pActiveEngine)
  {
    spdlog::warn(
        "EmployeesSchedulingEngine - processInterruptEngineEvent: inactive engine, returning " +
        pEngineId);
    return;
  }

  pOptimizer->interruptOptimizer();
}  // processKillEngineEvent

void EmployeesSchedulingEngine::processCollectSolutionsEvent(int numSolutions)
{
  // @note it is assumed that the processor
  // is not currently adding result into the solution
  schedulingengine::EmployeesSchedulingResult::SPtr res =
          std::dynamic_pointer_cast<schedulingengine::EmployeesSchedulingResult>(pSchedulingResult);

  // Callback method to send results back to the client
  pCallbackHandler->resultCallback(pEngineId,
                                   schedulingutils::buildEmployeesSchedulingSolutionProto(res));

  // Callback to send the metrics back to the client
  collectMetrics();
  auto jsonMetrics = engineutils::createMetricsJson(pMetricsRegister);

  // Callback method to send metrics back to the client
  pCallbackHandler->metricsCallback(pEngineId, jsonMetrics->toString());

  // Completed run
  pCallbackHandler->processCompletionCallback(pEngineId);
}  // processRunModelEvent

void EmployeesSchedulingEngine::collectMetrics()
{
  assert(pMetricsRegister);
  pMetricsRegister->setMetric(jsonmetrics::ENGINE_CREATION_TIME_MSEC,
                              pEngineCreationTimeMsec);
  pMetricsRegister->setMetric(jsonmetrics::LOAD_MODEL_LATENCY_MSEC,
                              pLoadModelLatencyMsec);
}  // collectMetrics

}  // namespace toolbox
}  // namespace optilab
