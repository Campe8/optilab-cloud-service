#include "scheduling_toolbox/employee_scheduling_processor.hpp"

#include <exception>
#include <stdexcept>   // for std::runtime_error

#include <spdlog/spdlog.h>

#include "data_structure/metrics/metrics_register.hpp"
#include "engine/engine_constants.hpp"
#include "solver/solver_model.hpp"
#include "utilities/timer.hpp"

namespace optilab {
namespace toolbox {

EmployeeSchedulingProcessor::EmployeeSchedulingProcessor(
        schedulingengine::SchedulingResult::SPtr result)
: SchedulingProcessor("EmployeeSchedulingProcessor", result)
{
  pResult = std::dynamic_pointer_cast<schedulingengine::EmployeesSchedulingResult>(getResult());
  if (!pResult)
  {
    throw std::runtime_error("EmployeeSchedulingProcessor - "
            "invalid downcast to EmployeesSchedulingResult");
  }
}

void EmployeeSchedulingProcessor::loadInstanceAndCreateSolver(SchedulingInstance::UPtr instance)
{
  if (!instance)
  {
    throw std::runtime_error("EmployeeSchedulingProcessor - loadInstanceAndCreateSolver: "
            "empty instance");
  }

  // Get the model
  if (!instance->getSchedulingModel().has_employee_scheduling_model())
  {
    const std::string errMsg = "EmployeeSchedulingProcessor - loadInstanceAndCreateSolver: "
        "invalid scheduling model for Employee Scheduling";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  // Get the scheduling model protobuf and load the model
  // in the back-end solver
  pResult->instanceName = instance->getSchedulingModel().model_id();
  const auto& emodel = instance->getSchedulingModel().employee_scheduling_model();

  // Build a the solver
  try
  {
    pSolver = std::make_shared<EmployeeSchedulingSolver>(emodel);
  }
  catch(const std::exception& ex)
  {
    const std::string errMsg = "EmployeeSchedulingProcessor - loadInstanceAndCreateSolver: "
        "cannot build the solver " + std::string(ex.what());
    spdlog::error(errMsg);
    throw;
  }
  catch(...)
  {
    const std::string errMsg = "EmployeeSchedulingProcessor - loadInstanceAndCreateSolver: "
        "cannot build the solver: unknown exception";
    spdlog::error(errMsg);
    throw;
  }
}  // setupModel

bool EmployeeSchedulingProcessor::interruptSolver()
{
  if (!pSolver)
  {
    return false;
  }

  pSolver->interruptSearchProcess();
  return true;
}  // interruptSolver

void EmployeeSchedulingProcessor::processWork(Work work)
{
  if (work->workType == schedulingengine::SchedulingWork::kRunOptimizer)
  {
    runSolver(work);
  }
  else
  {
    spdlog::warn("EmployeeSchedulingProcessor - processWork: unrecognized work type");
  }
}  // processWork

void EmployeeSchedulingProcessor::runSolver(Work& work)
{
  if (!pSolver)
  {
    const std::string errMsg = "EmployeeSchedulingProcessor - runSolver: no solver available";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  timer::Timer timer;

  // Run the solver
  pSolver->solve();

  const auto wallclockMsec = timer.getWallClockTimeMsec();
  work->metricsRegister->setMetric(jsonmetrics::OPTIMIZER_LATENCY_MSEC, wallclockMsec);

  // Store the solution found
  storeSolution();
}  // runEngine

void EmployeeSchedulingProcessor::storeSolution()
{
  LockGuard lock(pResultMutex);

  // Retrieve the solution
  pSolver->storeSolution(*pResult);
}  // storeSolution

}  // namespace toolbox
}  // namespace optilab
