//
// Copyright OptiLab 2020. All rights reserved.
//
// Processor for scheduling pipelines.
//

#pragma once

#include <memory>  // for std::shared_ptr
#include <string>

#include "engine/optimizer_processor.hpp"
#include "scheduling_toolbox/scheduling_instance.hpp"
#include "scheduling_toolbox/scheduling_utilities.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace toolbox {

class SYS_EXPORT_CLASS SchedulingProcessor
    : public OptimizerProcessor<
      schedulingengine::SchedulingWork::SPtr, schedulingengine::SchedulingWork::SPtr>
{
 public:
  using SPtr = std::shared_ptr<SchedulingProcessor>;

 public:
  SchedulingProcessor(const std::string& procName, schedulingengine::SchedulingResult::SPtr result)
   : BaseProcessor(procName), pResult(result)
  {
    if (!pResult)
    {
      throw std::runtime_error("RoutingProcessor - empty pointer to result");
    }
  }

  /// Loads the routing instance and creates a solver to run on it.
  /// @note throw std::invalid_argument on empty instance
  virtual void loadInstanceAndCreateSolver(SchedulingInstance::UPtr instance) {};

  /// Interrupts the internal solving process (if running).
  /// The solver remains in a non-executable state until another model is
  /// loaded. Returns true on success, false otherwise
  virtual bool interruptSolver() = 0;

 protected:
  inline schedulingengine::SchedulingResult::SPtr getResult() const
  {
    return pResult;
  }

 private:
  using Work = schedulingengine::SchedulingWork::SPtr;
  using BaseProcessor = OptimizerProcessor<Work, Work>;

 private:
  /// Schedules collected as a result of running the scheduler
  /// on the loaded instance
  schedulingengine::SchedulingResult::SPtr pResult;
};

}  // namespace toolbox
}  // namespace optilab
