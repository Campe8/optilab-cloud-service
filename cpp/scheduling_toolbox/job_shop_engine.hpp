//
// Copyright OptiLab 2020. All rights reserved.
//
// Engine class for solving the Job-Shop Scheduling Problem.
//

#pragma once

#include <atomic>
#include <cstdint>  // uint64_t
#include <memory>   // for std::shared_ptr
#include <string>

#include "data_structure/metrics/metrics_register.hpp"
#include "engine/engine_callback_handler.hpp"
#include "scheduling_toolbox/scheduling_engine.hpp"
#include "scheduling_toolbox/scheduling_instance.hpp"
#include "scheduling_toolbox/scheduling_optimizer.hpp"
#include "scheduling_toolbox/scheduling_utilities.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace toolbox {

class SYS_EXPORT_CLASS JobShopEngine : public SchedulingEngine
{
 public:
  using SPtr = std::shared_ptr<JobShopEngine>;

 public:
  /// Constructor: builds a new VRP engine with given processor.
  /// @note the processor must be a VRP processor
  JobShopEngine(const std::string& engineId, const EngineCallbackHandler::SPtr& handler);

  ~JobShopEngine();

  /// Registers the protobuf model given as input argument.
  /// @note this method does not run the model
  void registerInstance(SchedulingInstance::UPtr instance) override;

  /// Notifies the engine on a given EngineEvent
  void notifyEngine(const schedulingengine::SchedulingEvent& event) override;

  /// Wait for the current jobs in the queue to finish.
  /// @note if "timeoutMsec" is -1, waits for all jobs in the queue to complete
  void engineWait(int timeoutMsec) override;

  /// Turns-down this engine
  void turnDown() override;

 private:
  /// Engine identifier
  std::string pEngineId;

  /// Handler for callbacks to send back results to caller methods
  EngineCallbackHandler::SPtr pCallbackHandler;

  /// Pointer to the result instance collecting result from the optimizer
  schedulingengine::SchedulingResult::SPtr pSchedulingResult;

  /// Flag indicating whether this engine already started running
  std::atomic<bool> pActiveEngine{false};

  /// Latency in msec. to create this engine
  std::atomic<uint64_t> pEngineCreationTimeMsec{0};

  /// Latency in msec. caused by loading a model into the optimizer
  std::atomic<uint64_t> pLoadModelLatencyMsec{0};

  /// Latency in msec. caused by running the optimizer
  std::atomic<uint64_t> pOptimizerLatencyMsec{0};

  /// Register for metrics
  MetricsRegister::SPtr pMetricsRegister;

  /// Asynchronous optimizer
  SchedulingOptimizer::SPtr pOptimizer;

  // Builds the instance of a ORTools optimizer with a specified VRP processor
  void buildOptimizer(schedulingengine::SchedulingProcessorType procType);

  /// Runs the registered model
  void processRunModelEvent();

  /// Sends back using the handler all the solutions collected so far
  void processCollectSolutionsEvent(int numSolutions);

  /// Interrupts the current computation, if any, and returns
  void processInterruptEngineEvent();

  /// Collects metrics in the internal metrics register
  void collectMetrics();
};

}  // namespace toolbox
}  // namespace optilab
