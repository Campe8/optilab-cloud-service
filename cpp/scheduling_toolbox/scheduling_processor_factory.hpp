//
// Copyright OptiLab 2019. All rights reserved.
//
// Factory class for Scheduling processors.
//

#pragma once

#include "engine/engine_constants.hpp"
#include "scheduling_toolbox/scheduling_processor.hpp"
#include "scheduling_toolbox/scheduling_utilities.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace toolbox {

class SYS_EXPORT_CLASS SchedulingProcessorFactory {
public:
  explicit SchedulingProcessorFactory(schedulingengine::SchedulingResult::SPtr result);

  /// Sets the type of routing engine processor to build
  inline void setProcessorType(schedulingengine::SchedulingProcessorType ptype) noexcept
  {
    pProcessorType = ptype;
  }

  /// Returns the processor build w.r.t. the set type.
  /// See also "setProcessorType(...)".
  /// @note throw std::runtime_error on invalid processor type
  SchedulingProcessor::SPtr buildProcessor();

private:
  /// Type of processor to build
  schedulingengine::SchedulingProcessorType pProcessorType;

  /// Result used to build a scheduling processor
  schedulingengine::SchedulingResult::SPtr pResult;
};

}  // namespace toolbox
}  // namespace optilab
