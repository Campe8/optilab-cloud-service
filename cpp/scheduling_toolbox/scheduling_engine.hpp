//
// Copyright OptiLab 2020. All rights reserved.
//
// Base class for a scheduling optimizer engine.
// The goal or a scheduling problem is to find the optimal
// scheduling of resources and/or people w.r.t. some
// constraints, e.g., fairness, shifts, etc.
// Examples of scheduling problems are:
// - Nurse scheduling problem
// - Job Shop
//

#pragma once

#include "engine/engine.hpp"

#include <memory>  // for std::shared_ptr

#include "scheduling_toolbox/scheduling_instance.hpp"
#include "scheduling_toolbox/scheduling_utilities.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace toolbox {

class SYS_EXPORT_CLASS SchedulingEngine {
 public:
  using SPtr = std::shared_ptr<SchedulingEngine>;

 public:
  virtual ~SchedulingEngine() = default;

  /// Registers an instance of a routing optimization problem.
  /// @throw std::invalid argument on empty instances
  virtual void registerInstance(SchedulingInstance::UPtr instance) = 0;

  /// Notifies the optimizer on a given RoutingEvent
  virtual void notifyEngine(const schedulingengine::SchedulingEvent& event) = 0;

  /// Wait for the current jobs in the queue to finish.
  /// @note if "timeoutSec" is -1, waits for all jobs in the queue to complete
  virtual void engineWait(int timeoutMsec) = 0;

  /// Shuts down the engine
  virtual void turnDown() = 0;
};

}  // namespace toolbox
}  // namespace optilab
