#include "scheduling_toolbox/job_shop_processor.hpp"

#include <exception>
#include <stdexcept>   // for std::runtime_error

#include <spdlog/spdlog.h>

#include "data_structure/metrics/metrics_register.hpp"
#include "engine/engine_constants.hpp"
#include "solver/solver_model.hpp"
#include "utilities/timer.hpp"

namespace optilab {
namespace toolbox {

JobShopProcessor::JobShopProcessor(schedulingengine::SchedulingResult::SPtr result)
: SchedulingProcessor("JobShopProcessor", result)
{
  pResult = std::dynamic_pointer_cast<schedulingengine::JobShopResult>(getResult());
  if (!pResult)
  {
    throw std::runtime_error("JobShopProcessor - invalid downcast to JobShopProcessor");
  }
}

void JobShopProcessor::loadInstanceAndCreateSolver(SchedulingInstance::UPtr instance)
{
  if (!instance)
  {
    throw std::runtime_error("JobShopProcessor - loadInstanceAndCreateSolver: empty instance");
  }

  // Get the model
  if (!instance->getSchedulingModel().has_job_shop_model())
  {
    const std::string errMsg = "JobShopProcessor - loadInstanceAndCreateSolver: "
        "invalid scheduling model for Job-Shop";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  // Get the scheduling model protobuf and load the model
  // in the back-end solver
  pResult->instanceName = instance->getSchedulingModel().model_id();
  const auto& model = instance->getSchedulingModel().job_shop_model();

  // Build a the solver
  try
  {
    pSolver = std::make_shared<JobShopSolver>(model);
  }
  catch(const std::exception& ex)
  {
    const std::string errMsg = "JobShopProcessor - loadInstanceAndCreateSolver: "
        "cannot build the solver " + std::string(ex.what());
    spdlog::error(errMsg);
    throw;
  }
  catch(...)
  {
    const std::string errMsg = "JobShopProcessor - loadInstanceAndCreateSolver: "
        "cannot build the solver: unknown exception";
    spdlog::error(errMsg);
    throw;
  }
}  // setupModel

bool JobShopProcessor::interruptSolver()
{
  if (!pSolver)
  {
    return false;
  }

  pSolver->interruptSearchProcess();
  return true;
}  // interruptSolver

void JobShopProcessor::processWork(Work work)
{
  if (work->workType == schedulingengine::SchedulingWork::kRunOptimizer)
  {
    runSolver(work);
  }
  else
  {
    spdlog::warn("JobShopProcessor - processWork: unrecognized work type");
  }
}  // processWork

void JobShopProcessor::runSolver(Work& work)
{
  if (!pSolver)
  {
    const std::string errMsg = "JobShopProcessor - runSolver: no solver available";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  timer::Timer timer;

  // Run the solver
  pSolver->solve();

  const auto wallclockMsec = timer.getWallClockTimeMsec();
  work->metricsRegister->setMetric(jsonmetrics::OPTIMIZER_LATENCY_MSEC, wallclockMsec);

  // Store the solution found
  storeSolution();
}  // runEngine

void JobShopProcessor::storeSolution()
{
  LockGuard lock(pResultMutex);

  // Retrieve the solution
  pSolver->storeSolution(*pResult);
}  // storeSolution

}  // namespace toolbox
}  // namespace optilab
