#include "connector_engine/combinator_processor.hpp"

#include <cassert>
#include <chrono>
#include <exception>
#include <stdexcept>  // for std::invalid_argument

#include <boost/bind.hpp>
#include <boost/make_shared.hpp>

#include <spdlog/spdlog.h>

#include "config/config_constants.hpp"
#include "config/system_network_config.hpp"
#include "data_structure/json/json.hpp"
#include "engine/engine_utils.hpp"
#include "model/model_constants.hpp"
#include "utilities/random_generators.hpp"

namespace {

constexpr int kWorkerIdLength = 5;

/// Returns a random id with the following syntax:
/// prefix_id_<random_alphanumeric_string>
std::string getRandomEngineId(const std::string& prefix, const int id)
{
  std::string engineId = prefix + "_" + std::to_string(id) + "_";
  engineId += optilab::utilsrandom::buildRandomAlphaNumString(kWorkerIdLength);
  return engineId;
}  // getRandomEngineId

/// Returns the vector of connections ids
std::vector<std::string> getConnectionsIds(optilab::JSONValue::JsonObject& jsonList)
{
  std::vector<std::string> idList;
  for (optilab::JSONValue::JsonObject& iter : jsonList.GetArray())
  {
    idList.push_back(std::string(iter.GetString()));
  }

  return idList;
}  // getConnectionsIds

/// Returns the (Json) string if the object with specified id from the given Json object
void setConnectionObject(
    const std::string& id, optilab::JSONValue::JsonObject& jsonList,
    std::pair<std::pair<std::pair<optilab::EngineFrameworkType, optilab::OptimizerPkg>,
    optilab::EngineClassType>, std::string>& descriptor)
{
  for (optilab::JSONValue::JsonObject& objIter : jsonList.GetArray())
  {
    auto objId = objIter.FindMember(optilab::jsonmodel::OBJECT_ID);
    if (objId == objIter.MemberEnd())
    {
      return;
    }

    if (objId->value.GetString() == id)
    {
      // Set the descriptor
      descriptor.second = optilab::JSONValue::jsonObjectToString(objIter);

      // Set the framework type
      auto frameworkType = objIter.FindMember(optilab::jsonmodel::FRAMEWORK);
      if (frameworkType == objIter.MemberEnd())
      {
        const std::string errMsg = "AsyncCombinatorEngineProcessor - "
            "missing framework type on connection id " + id;
        spdlog::error(errMsg);
        throw std::runtime_error(errMsg);
      }
      descriptor.first.first.first = optilab::engineutils::getEngineFrameworkTypeFromString(
          frameworkType->value.GetString());

      // Set the package type
      auto packageType = objIter.FindMember(optilab::jsonmodel::PACKAGE);
      if (packageType != objIter.MemberEnd())
      {

        descriptor.first.first.second = optilab::engineutils::getEnginePackageTypeFromString(
            packageType->value.GetString());
      }
      else
      {
        descriptor.first.first.second = optilab::OptimizerPkg::OP_UNDEF;
      }

      // Set the class type
      auto classType = objIter.FindMember(optilab::jsonmodel::CLASS_TYPE);
      if (classType == objIter.MemberEnd())
      {
        const std::string errMsg = "AsyncCombinatorEngineProcessor - "
            "missing class type on connection id " + id;
        spdlog::error(errMsg);
        throw std::runtime_error(errMsg);
      }
      descriptor.first.second = optilab::engineutils::getEngineClassTypeFromString(
          classType->value.GetString());
    }
  }
}  // getConnectionObject

bool isQueueEmpty(const std::shared_ptr<std::deque<std::string>>& queue)
{
  return queue->empty();
}  // isWorkQueueEmpty

}  // namespace

namespace optilab {

CombinatorProcessor::CombinatorProcessor(const std::string& engineId)
: BaseProcessor(engineId),
  pEngineId(engineId),
  pCombinatorResults(nullptr),
  pSyncQueue(nullptr),
  pReceiverThread(nullptr)
{
}

CombinatorProcessor::~CombinatorProcessor()
{
  if (pWorkerRunner && pWorkerRunner->isRunning())
  {
    pWorkerRunner->stopRunner();
    pWorkerRunner.reset();
  }
}  // CombinatorProcessor

void CombinatorProcessor::setCombinatorResult(connectorengine::CombinatorResult::SPtr result)
{
  pCombinatorResults = result;
  if (!pCombinatorResults)
  {
    const std::string errMsg = "CombinatorProcessor - empty pointer to combinator results";
    spdlog::error(errMsg);
    throw std::invalid_argument(errMsg);
  }

  pCombinatorResults->modelName = pEngineId;
}  // setCombinatorResult

void CombinatorProcessor::setSyncQueue(connectorengine::SyncQueue::SPtr queue)
{
  pSyncQueue = queue;
  if (!pSyncQueue)
  {
    const std::string errMsg = "CombinatorProcessor - setSyncQueue: "
        "empty pointer to the synchronized queue";
    spdlog::error(errMsg);
    throw std::invalid_argument(errMsg);
  }
}

void CombinatorProcessor::loadJSONModel(const std::string& jsonModel)
{
  if (jsonModel.empty())
  {
    const std::string errMsg = "CombinatorProcessor - loadJSONModel: "
        "empty JSON model to load";
    spdlog::error(errMsg);
    throw std::invalid_argument(errMsg);
  }

  // Parse the connections as a Json object
  JSON::SPtr jsonConnections;
  try
  {
    jsonConnections = std::make_shared<JSON>(jsonModel);
  }
  catch (std::exception& ex)
  {
    const std::string errMsg = "CombinatorProcessor - loadJSONModel: "
        "cannot parse the connections JSON descriptor: " + std::string(ex.what());
    spdlog::error(errMsg);
        throw std::invalid_argument(errMsg);
  }
  catch (...)
  {
    const std::string errMsg = "CombinatorProcessor - loadJSONModel: "
        "cannot parse the connections JSON descriptor";
    spdlog::error(errMsg);
    throw std::invalid_argument(errMsg);
  }
  assert(jsonConnections);

  if (!jsonConnections->hasObject(jsoncombinatormodel::COMBINATOR_OBJECT_ID_LIST))
  {
    const std::string errMsg = "CombinatorProcessor - loadJSONModel: "
        "connection descriptor missing field " +
        std::string(jsoncombinatormodel::COMBINATOR_OBJECT_ID_LIST);
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  const auto connectionIdList = getConnectionsIds(
      jsonConnections->getObject(jsoncombinatormodel::COMBINATOR_OBJECT_ID_LIST));
  if (connectionIdList.empty())
  {
    spdlog::warn("CombinatorProcessor - loadJSONModel: "
        "empty list of connections, return");
    return;
  }

  if (jsonConnections->hasObject(jsoncombinatormodel::COMBINATOR_LOGIC))
  {
    // Set logic for collecting the combinator's results
    const auto& logicResuls = jsonConnections->getObject(jsoncombinatormodel::COMBINATOR_LOGIC);
    const std::string logic = logicResuls.GetString();
    if (logic == std::string(jsoncombinatormodel::COMBINATOR_LOGIC_FIRST_WIN))
    {
      pCombinatorContext.logic = connectorengine::CombinatorResultLogic::CRL_FIRST_WIN;
    }
    else
    {
      if (logic != std::string(jsoncombinatormodel::COMBINATOR_LOGIC_COLLECT_ALL))
      {
        const std::string errMsg = "CombinatorProcessor - loadJSONModel: "
            "unrecognized combinator logic " + logic;
        spdlog::error(errMsg);
        throw std::runtime_error(errMsg);
      }
      pCombinatorContext.logic = connectorengine::CombinatorResultLogic::CRL_COLLECT_ALL;
    }
  }

  if (!jsonConnections->hasObject(jsoncombinatormodel::COMBINATOR_OBJECT_LIST))
  {
    throw std::runtime_error("CombinatorProcessor - loadJSONModel: "
        "connection descriptor missing field " +
        std::string(jsoncombinatormodel::COMBINATOR_OBJECT_LIST));
  }
  JSONValue::JsonObject& connectionsJsonList =
      jsonConnections->getObject(jsoncombinatormodel::COMBINATOR_OBJECT_LIST);

  // For each connection id, get the corresponded (json) object,
  // and add it to the map
  for (const auto& id : connectionIdList)
  {
    // Skip duplicated ids
    if (pCombinatorContext.connectionsMap.find(id) != pCombinatorContext.connectionsMap.end())
    {
      spdlog::warn("CombinatorProcessor - loadJSONModel: "
          "duplicated connection id " + id);
      continue;
    }

    // Set the connection object, i.e., map the id of the combinator object with the
    // combinator object itself
    auto& connectionDescriptor = pCombinatorContext.connectionsMap[id];
    setConnectionObject(id, connectionsJsonList, connectionDescriptor);

    // Remove the entry from the map if the object is not present
    if (connectionDescriptor.second.empty())
    {
      pCombinatorContext.connectionsMap.erase(id);
      continue;
    }
  }
}  // setupJsonModel

void CombinatorProcessor::processWork(Work work)
{
  if (work->workType == connectorengine::ConnectorWork::kStartWorkerRunner)
  {
    startWorkerRunner();
  }
  else if (work->workType == connectorengine::ConnectorWork::kRunOptimizer)
  {
    runEnginePortfolio();
  }
  else if (work->workType == connectorengine::ConnectorWork::kStartListenerService)
  {
    startListenerService();
  }
  else if (work->workType == connectorengine::ConnectorWork::kBroadcastInterrupt)
  {
    broadcastInterrupt();
  }
  else
  {
    spdlog::warn("CombinatorProcessor - processWork: unrecognized work type");
  }
}  // processWork

void CombinatorProcessor::startWorkerRunner()
{
  auto& config = optilab::SystemNetworkConfig::getInstance();
  auto& portList = config.getPortList(optilab::configconsts::BACKEND_SERVICE_RUNNER);
  if (portList.size() < 3)
  {
    const std::string errMsg = "CombinatorProcessor - startWorkerRunner: "
        "missing ports for back-end service in the JSON configuration file";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  if (!pSyncQueue)
  {
    const std::string errMsg = "CombinatorProcessor - startWorkerRunner: "
        "empty pointer to synchronized queue";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  // Create a callback for the worker runner
  CombinatorWorkerRunnerCallback::SPtr callbackPtr =
      std::make_shared<CombinatorWorkerRunnerCallback>();

  // Set response queue and synchronization mechanism in the runner callback
  callbackPtr->setSyncQueue(pSyncQueue);

  // Create a worker runner to send requests to (sub) workers
  const int sendPort = portList[0];
  const int sinkPort = portList[1];
  const int controllerPort = portList[2];
  pWorkerRunner = std::make_shared<BackendWorkerRunner>(sendPort, sinkPort, controllerPort);

  // Set the callback into the runner
  pWorkerRunner->setResponseCallback(callbackPtr);

  // Start the runner
  pWorkerRunner->startRunner();
}  // startWorkerRunner

void CombinatorProcessor::runEnginePortfolio()
{
  if (pCombinatorContext.connectionsMap.empty())
  {
    spdlog::warn("CombinatorProcessor - runEnginePortfolio: "
        "no input connections for combinator " + pEngineId);
    return;
  }

  if (!pWorkerRunner)
  {
    const std::string errMsg = "CombinatorProcessor - runEnginePortfolio: "
        "empty worker runner " + pEngineId;
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  if (!pWorkerRunner->isRunning())
  {
    const std::string errMsg = "CombinatorProcessor - runEnginePortfolio: "
        "worker runner not started, return " + pEngineId;
    spdlog::warn(errMsg);
    return;
  }

  // Prepare the requests to send to the (sub) workers using the runner
  int requestId = 0;
  for (const auto& it : pCombinatorContext.connectionsMap)
  {
    WorkerRequestMessage request;
    request.messageId = requestId++;
    request.engineId = getRandomEngineId(pEngineId, request.messageId);
    request.frameworkType = it.second.first.first.first;
    request.packageType = it.second.first.first.second;
    request.classType = it.second.first.second;
    request.payload = it.second.second;
    request.deleteRequest = false;

    // Save the engine id used for this request/job
    pEngineIdRequestHistory.push_back(request);
  }

  // Send all the requests in a batch of jobs
  for (const auto& req : pEngineIdRequestHistory)
  {
    // Send the request to the pool of workers
    pWorkerRunner->sendRequestToWorker(req);
  }
}  // runEnginePortfolio

void CombinatorProcessor::broadcastInterrupt()
{
  // Send a shutdown message to each active engine
  spdlog::info("CombinatorProcessor - broadcastInterrupt: "
      "broadcasting interrupt to all solvers/workers");
  int requestId = pEngineIdRequestHistory.back().messageId + 1;
  for (auto& req : pEngineIdRequestHistory)
  {
    // Increase the request counter and set this request as a delete request
    req.messageId = requestId++;
    req.deleteRequest = true;

    pWorkerRunner->sendRequestToWorker(req);
  }

  // Stop the runner
  pWorkerRunner->stopRunner();
}  // broadcastInterrupt

void CombinatorProcessor::startListenerService()
{
  if (!pSyncQueue)
  {
    const std::string errMsg = "CombinatorProcessor - startListenerService: "
        "empty pointer to synchronized queue";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  // Create the task for the run loop
  boost::packaged_task<void> task(boost::bind(&CombinatorProcessor::handleIncomingMessages, this));

  // pReceiverThread =
  //    boost::make_shared<boost::thread>(&CombinatorProcessor::handleIncomingMessages, this);

  // Get the future/shared state from the packaged task
  boost::unique_future<void> readerFuture = task.get_future();

  // Run the thread on the task,
  // i.e., on the "run(...) method
  pReceiverThread.reset(new boost::thread(boost::move(task)));

  // Join the listener thread
  pReceiverThread->join();

  if (readerFuture.has_exception())
  {
    spdlog::error("CombinatorProcessor - startListenerService: "
        "exception in listener thread, stop");

    pReceiverThread->interrupt();
  }
  else
  {
    readerFuture.get();
  }

  pReceiverThread.reset();
}  // startListenerService

void CombinatorProcessor::handleIncomingMessages()
{
  int responseCounter = 0;
  while (responseCounter < pCombinatorContext.connectionsMap.size())
  {
    boost::this_thread::interruption_point();

    // Read a message from the queue
    std::string response = pSyncQueue->popFront();

    // Increase the number of responses received so far
    responseCounter++;

    // Send the solution back to the client according to the logic implemented by the
    // combinator, i.e., best solution, fast solution, etc.
    if (runCombinatorLogic(responseCounter, response))
    {
      return;
    }
  }  // while
}  // handleIncomingMessages

bool CombinatorProcessor::runCombinatorLogic(int numResponsesSoFar, const std::string& currentResp)
{
  /*
   * Implement simple "return first response" logic.
   * TODO implement strategy pattern and more logics here.
   */
  boost::this_thread::interruption_point();

  if (!pCombinatorResults)
  {
    throw std::runtime_error("CombinatorProcessor - runCombinatorLogic: "
        "empty pointer to result");
  }

  if (numResponsesSoFar <= 0 || pCombinatorContext.connectionsMap.empty())
  {
    return false;
  }

  // Set the response that will be given back to the client
  pCombinatorResults->solutionList.push_back(currentResp);

  if (pCombinatorContext.logic == connectorengine::CombinatorResultLogic::CRL_FIRST_WIN)
  {
    // First result wins and returns true to signal the caller that
    // there are enough solutions collected
    spdlog::info("CombinatorProcessor - runCombinatorLogic: "
        "found first solution, return");
    pCombinatorResults->combinatorLogic = pCombinatorContext.logic;
    return true;
  }
  else if (pCombinatorContext.logic == connectorengine::CombinatorResultLogic::CRL_COLLECT_ALL)
  {
    if (numResponsesSoFar == static_cast<int>(pCombinatorContext.connectionsMap.size()))
    {
      // First result wins and returns true to signal the caller that
      // there are enough solutions collected
      spdlog::info("CombinatorProcessor - runCombinatorLogic: "
          "all solutions found, return");
      pCombinatorResults->combinatorLogic = pCombinatorContext.logic;
      return true;
    }
  }

  return false;
}  // handleCombinatorLogic

void CombinatorProcessor::interruptProcessor()
{
  // Stop the runner
  if (pWorkerRunner)
  {
    pWorkerRunner->stopRunner();
  }

  if (pReceiverThread)
  {
    pReceiverThread->interrupt();
    pReceiverThread->join();
    pReceiverThread.reset();
  }
}  // killEngine

}  // namespace optilab
