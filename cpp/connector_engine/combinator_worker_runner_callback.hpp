//
// Copyright OptiLab 2019. All rights reserved.
//
// Runner callback for combinator engines.
//

#pragma once

#include <condition_variable>
#include <deque>
#include <memory>   // for std::shared_ptr
#include <mutex>
#include <string>

#include "connector_engine/connector_utilities.hpp"
#include "optilab_protobuf/optilab.pb.h"
#include "optimizer_service/backend_worker_runner.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS CombinatorWorkerRunnerCallback : public WorkerRunnerResponseCallback {
 public:
  using SPtr = std::shared_ptr<CombinatorWorkerRunnerCallback>;

 public:
  /// Sets the queue of responses.
  /// The response queue is the queue collecting all the responses from the sub-workers.
  /// @note throw std::invalid_argument on empty pointer
  void setSyncQueue(connectorengine::SyncQueue::SPtr queue);

  /// On reply callback, the id is the id of the request that produced the reply
  void onReply(int id, const OptiLabReplyMessage& reply) override;

  /// On error callback, the id is the id of the request that produced the error
  void onError(int id) override;

 private:
  /// Queue used to store responses
  connectorengine::SyncQueue::SPtr pSyncQueue;
};

}  // namespace optilab
