//
// Copyright OptiLab 2019. All rights reserved.
//
// Utilities for OR-tools
//

#pragma once

#include <cstddef>  // for std::size_t
#include <deque>
#include <memory>   // for std::shared_ptr
#include <string>
#include <vector>

#include <boost/bind.hpp>
#include <boost/thread.hpp>

#include "data_structure/json/json.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

namespace connectorengine {

/// Logic type used in the combinator
enum CombinatorResultLogic {
  /// First result/response from one of the workers win
  CRL_FIRST_WIN,

  /// Collect all results/responses from all workers
  CRL_COLLECT_ALL,

  /// Undefined logic
  CRL_UNDEF
};

struct SYS_EXPORT_STRUCT ConnectorWork {
using SPtr = std::shared_ptr<ConnectorWork>;

enum WorkType {
  /// Starts the worker runner
  kStartWorkerRunner,

  /// Runs the optimizer on the loaded model
  kRunOptimizer,

  /// Runs the receiver thread handling incoming messages
  kStartListenerService,

  /// Broadcast interrupt signal to all workers
  kBroadcastInterrupt,

  kWorkTypeUndef
};

explicit ConnectorWork(WorkType wtype)
: workType(wtype)
{
}

WorkType workType;
};

class SYS_EXPORT_CLASS ConnectorEvent {
 public:
  enum EventType {
    /// Starts the solving process
    kRunEngine,

    /// Interrupts any ongoing computation
    kInterruptEngine,

    /// Broadcasts the back-end engines to stop computing
    kStopBackendEngines,

    /// Collects solutions
    kSolutions
  };

 public:
  explicit ConnectorEvent(EventType aType)
  : pEventType(aType)
  {
  }

  inline EventType getType() const noexcept
  {
    return pEventType;
  }

  inline void setNumSolutions(int numSolutions) noexcept
  {
    pNumSolutions = numSolutions;
  }

  inline int getNumSolutions() const noexcept
  {
    return pNumSolutions;
  }

 private:
  /// Type of this event
  EventType pEventType;

  /// Number of solutions for "kSolutions" events
  int pNumSolutions{0};
};

class SYS_EXPORT_CLASS SyncQueue {
 public:
  using SPtr = std::shared_ptr<SyncQueue>;

 public:
  void pushBack(const std::string& msg)
  {
    UniqueLock lock(pQueueMutex);
    pDeque.push_back(msg);
    pQueueCondVar.notify_one();
  }

  std::string popFront()
  {
    std::string response;
    {
      // Critical section
      UniqueLock lock(pQueueMutex);
      if (pDeque.empty())
      {
        pQueueCondVar.wait(lock, boost::bind(&std::deque<std::string>::size, &pDeque));
      }

      // Remove one request
      response = pDeque.front();
      pDeque.pop_front();
    }
    return response;
  }

 private:
  /// Lock type on the internal mutex
  using UniqueLock = boost::unique_lock<boost::mutex>;

 private:
  /// Mutex to synchronize the access to the queue
  boost::mutex pQueueMutex;

  /// Condition variable for synchronization on
  /// reads and writes on the queue
  boost::condition_variable pQueueCondVar;

  /// Actual queue of messages
  std::deque<std::string> pDeque;
};

struct SYS_EXPORT_STRUCT ConnectorResult {
  using SPtr = std::shared_ptr<ConnectorResult>;

  /// Name of the model producing this result
  std::string modelName;
};

struct SYS_EXPORT_STRUCT CombinatorResult : public ConnectorResult {
  using SPtr = std::shared_ptr<CombinatorResult>;

  /// Logic used by the combinator to produce this result
  CombinatorResultLogic combinatorLogic{CombinatorResultLogic::CRL_UNDEF};

  std::vector<std::string> solutionList;

  inline std::size_t getNumSolutions() const noexcept { return solutionList.size(); }
  void clear()
  {
    combinatorLogic = CombinatorResultLogic::CRL_UNDEF;
    solutionList.clear();
  }

};

}  // namespace connectorengine

namespace connectorutils {

/// Builds a JSON object representing the combinator solution given as argument
SYS_EXPORT_FCN JSON::SPtr buildCombinatorSolutionJson(connectorengine::CombinatorResult::SPtr res);

}  // namespace connectorutils

}  // namespace optilab
