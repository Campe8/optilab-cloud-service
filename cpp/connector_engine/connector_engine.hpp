//
// Copyright OptiLab 2019. All rights reserved.
//
// Base class for a connector engine.
//

#pragma once

#include "engine/engine.hpp"

#include <memory>  // for std::shared_ptr
#include <string>

#include "connector_engine/connector_utilities.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS ConnectorEngine {
 public:
  using JsonModel = std::string;
  using JsonModelList = std::vector<JsonModel>;

  using SPtr = std::shared_ptr<ConnectorEngine>;

 public:
  ConnectorEngine() = default;

  virtual ~ConnectorEngine() = default;

  /// Loads the given connections.
  /// @note this method does not run the engine.
  /// @throw std::runtime_error if this model is called while the engine is running
  virtual void loadConnections(const std::string& jsonConnections) = 0;

  /// Notifies the engine on a given EngineEvent
  virtual void notifyEngine(const connectorengine::ConnectorEvent& event) = 0;

  /// Wait for the current jobs in the queue to finish.
  /// @note if "timeoutSec" is -1, waits for all jobs in the queue to complete
  virtual void engineWait(int timeoutMsec) { (void) timeoutMsec; }

  /// Turns-down this engine
  virtual void turnDown() = 0;
};

}  // namespace optilab
