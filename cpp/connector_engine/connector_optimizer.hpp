//
// Copyright OptiLab 2019. All rights reserved.
//
// An ORTools optimizer is an asynchronous engine that
// holds a pipeline containing ORTools processors.
//

#pragma once

#include <memory>  // for std::shared_ptr
#include <string>

#include "engine/async_engine.hpp"
#include "connector_engine/connector_utilities.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS ConnectorOptimizer :
public AsyncEngine<connectorengine::ConnectorWork::SPtr> {
 public:
  using SPtr = std::shared_ptr<ConnectorOptimizer>;

 public:
  ConnectorOptimizer(const std::string& engineName, int numPipelines)
 : BaseClass(engineName, numPipelines)
 {
   // Initialize the engine,
   // e.g., the internal pipeline
   // REMBER TO CALL INIT IN DERIVED CLASSES:
   // BaseClass::init();
 }

  virtual ~ConnectorOptimizer() = default;

  /// Initializes this optimizer with the given model.
  /// i.e., loads the model into the internal state and creates
  /// the optimizer processor that will solve it.
  /// @note this is a blocking call
  virtual void loadModel(const std::string& jsonModel) = 0;

  /// Runs this optimizer on the loaded model
  virtual void runOptimizer() = 0;

  /// Interrupts the solving processes on this optimizer.
  /// Returns true if the solver was successfully interrupted.
  /// Returns false otherwise
  virtual bool interruptOptimizer() = 0;

 protected:
  using Work = connectorengine::ConnectorWork;
  using BaseClass = AsyncEngine<Work::SPtr>;
};

}  // namespace optilab
