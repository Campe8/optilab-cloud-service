#include "connector_engine/connector_utilities.hpp"

#include <stdexcept>  // for std::invalid_argument

#include <spdlog/spdlog.h>

#include "engine/engine_constants.hpp"

namespace optilab {

namespace connectorutils {

JSON::SPtr buildCombinatorSolutionJson(connectorengine::CombinatorResult::SPtr res)
{
  if (!res)
  {
    throw std::invalid_argument("buildCombinatorSolutionJson - "
        "empty pointer to the combinator result");
  }

  JSON::SPtr json = std::make_shared<JSON>();
  if (res->solutionList.empty()) return json;

  if (res->combinatorLogic == connectorengine::CombinatorResultLogic::CRL_FIRST_WIN)
  {
    auto solution = json->createValue((res->solutionList)[0]);
    json->add(jsoncombinatorsolution::SOLUTION_LIST, solution);
  }
  else
  {
    if (res->combinatorLogic != connectorengine::CombinatorResultLogic::CRL_COLLECT_ALL)
    {
      throw std::runtime_error("buildCombinatorSolutionJson - unrecognized combinator logic type");
    }

    auto solutionList = json->createArray();
    for (const auto& sol : res->solutionList)
    {
      JSON jsonSol(sol);
      JSONValue::SPtr jsonVal = json->createValue();
      for (auto itr = jsonSol.getDocument().MemberBegin();
          itr != jsonSol.getDocument().MemberEnd(); ++itr)
      {
        (jsonVal->getObject()).AddMember(
            JSONValue::JsonObject(itr->name.GetString(), jsonVal->getAllocator()),
            itr->value, jsonVal->getAllocator());
      }

      solutionList->pushBack(jsonVal);
    }
    json->add(jsoncombinatorsolution::SOLUTION_LIST, solutionList);
  }

  return json;
}  // buildCombinatorSolutionJson

}  // namespace connectorutils

}  // namespace optilab
