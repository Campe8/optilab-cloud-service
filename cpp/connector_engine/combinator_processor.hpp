//
// Copyright OptiLab 2019. All rights reserved.
//
// Asynchronous processor for connector combinator engines.
//

#pragma once

#include "engine/optimizer_processor.hpp"

#include <condition_variable>
#include <deque>
#include <memory>   // for std::shared_ptr
#include <mutex>
#include <string>
#include <thread>
#include <unordered_map>
#include <utility>  // for std::pair
#include <vector>

#include <boost/shared_ptr.hpp>
#include <boost/thread.hpp>

#include "connector_engine/combinator_worker_runner_callback.hpp"
#include "connector_engine/connector_utilities.hpp"
#include "engine/engine_callback_handler.hpp"
#include "engine/engine_constants.hpp"
#include "optilab_protobuf/optilab.pb.h"
#include "optimizer_service/backend_worker_runner.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS CombinatorProcessor :
public OptimizerProcessor<connectorengine::ConnectorWork::SPtr,
connectorengine::ConnectorWork::SPtr>{
public:
  using SPtr = std::shared_ptr<CombinatorProcessor>;

  using FrameworkPackagePair = std::pair<EngineFrameworkType, OptimizerPkg>;
  using FrameworkClassTypePair = std::pair<FrameworkPackagePair, EngineClassType>;
  using FrameworkServiceDescriptorPair = std::pair<FrameworkClassTypePair, std::string>;
  using ConnectionsMap = std::unordered_map<std::string, FrameworkServiceDescriptorPair>;

  /// Shared context between processor of a combinator engine
  struct CombinatorContext {
    CombinatorContext() = default;

    /// Logic used for collecting results
    connectorengine::CombinatorResultLogic logic{
      connectorengine::CombinatorResultLogic::CRL_FIRST_WIN};

    /// Map of the connection descriptors.
    /// Each connection is mapped to its identifier
    ConnectionsMap connectionsMap;
  };

public:
  CombinatorProcessor(const std::string& engineId);

  ~CombinatorProcessor();

  /// Sets the combinator result
  void setCombinatorResult(connectorengine::CombinatorResult::SPtr result);

  /// Set the synchronized message queue
  void setSyncQueue(connectorengine::SyncQueue::SPtr queue);

  /// Sets up the Json model to be ready for the engine which will run it
  void loadJSONModel(const std::string& jsonModel);

  /// Returns the connections map
  inline const CombinatorContext& getCombinatorContext() const noexcept
  {
    return pCombinatorContext;
  }

  /// Sets/overrides the combinator context
  inline void setCombinatorContext(const CombinatorContext& ctx) noexcept
  {
    pCombinatorContext = ctx;
  }

  /// Interrupts the current processor execution (if any).
  /// The engine remains in a non-executable state until another model is loaded
  void interruptProcessor();

protected:
  using Work = connectorengine::ConnectorWork::SPtr;
  using BaseProcessor = OptimizerProcessor<Work, Work>;

protected:
  void processWork(Work work) override;

private:
  using UniqueLock = std::unique_lock<std::mutex>;
  using MutexPtr = std::shared_ptr<std::mutex>;
  using ConditionVariablePtr = std::shared_ptr<std::condition_variable>;

  using CallbackResponseQueue = std::deque<std::string>;
  using CallbackResponseQueuePtr = std::shared_ptr<CallbackResponseQueue>;

private:
  /// Id of the engine instantiating this processor
  const std::string pEngineId;

  /// Pointer to the combinator results
  connectorengine::CombinatorResult::SPtr pCombinatorResults;

  /// State of this processor
  CombinatorContext pCombinatorContext;

  /// Thread reading the responses from the worker runner
  std::shared_ptr<std::thread> pWorkerResponseReader;

  /// Worker runner to communicate with back-end (sub) workers
  BackendWorkerRunner::SPtr pWorkerRunner;

  /// Synchronized message queue
  connectorengine::SyncQueue::SPtr pSyncQueue;

  /// Map keeping track of the random engine ids and corresponding requests
  std::vector<WorkerRequestMessage> pEngineIdRequestHistory;

  /// Thread running the message receiver
  boost::shared_ptr<boost::thread> pReceiverThread;

  /// Starts the worker runner
  void startWorkerRunner();

  /// Triggers the start of the engine.
  /// @note this doesn't check whether an engine is currently running or not.
  /// It is responsibility of the caller to handle that logic
  void runEnginePortfolio();

  /// Starts the service listening from incoming messages
  void startListenerService();

  /// Handles all incoming messages from the socket
  void handleIncomingMessages();

  /// Broadcasts interrupt signal to all workers
  void broadcastInterrupt();

  /// Runs the logic of this combinator,
  /// e.g., kill all running workers after the first response has arrived.
  /// Returns true if the combinator has finished its combination tasks, false otherwise
  bool runCombinatorLogic(int numResponsesSoFar, const std::string& currentResp);
};

}  // namespace optilab
