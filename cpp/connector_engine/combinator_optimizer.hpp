//
// Copyright OptiLab 2019. All rights reserved.
//
// An ORTools optimizer is an asynchronous engine that
// holds a pipeline containing ORTools processors.
//

#pragma once

#include <memory>  // for std::shared_ptr
#include <string>
#include <vector>

#include "connector_engine/connector_optimizer.hpp"
#include "connector_engine/combinator_processor.hpp"
#include "connector_engine/connector_utilities.hpp"
#include "engine/processor_pipeline.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS CombinatorOptimizer : public ConnectorOptimizer {
 public:
  using SPtr = std::shared_ptr<CombinatorOptimizer>;

 public:
  CombinatorOptimizer(const std::string& engineName,
                      connectorengine::SyncQueue::SPtr syncQueue,
                      connectorengine::CombinatorResult::SPtr result);

  /// Waits until one result is received from one of the workers/solvers
  void waitOnReceivedResult();

  /// Broadcast shutdown to all workers
  void broadcastWorkersShutdown();

  /// Initializes this optimizer with the given model.
  /// i.e., loads the model into the internal state and creates
  /// the optimizer processor that will solve it.
  /// @note this is a blocking call.
  /// @note if "keepJSON" flag is true, it loads the model directly from the JSON string,
  /// instead of converting it first into a Model instance
  void loadModel(const std::string& jsonModel) override;

  /// Runs this optimizer on the loaded model
  void runOptimizer() override;

  /// Interrupts the solving processes on this optimizer.
  /// Returns true if the solver was successfully interrupted.
  /// Returns false otherwise
  bool interruptOptimizer() override;

 protected:
  ProcessorPipeline::SPtr buildPipeline(int pipeline = 0) override;

 private:
  /// Processor used to send outgoing messages
  CombinatorProcessor::SPtr pSendProcessor;

  /// Processor used to receive incoming messages
  CombinatorProcessor::SPtr pRecvProcessor;

  /// Synchronous queue coordinating messages between sender and receiver
  connectorengine::SyncQueue::SPtr pSyncQueue;

  /// Pointer to the combinator result
  connectorengine::CombinatorResult::SPtr pCombinatorResult;
};

}  // namespace optilab
