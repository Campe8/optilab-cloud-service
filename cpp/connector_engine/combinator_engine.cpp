// Base class
#include "connector_engine/combinator_engine.hpp"

#include <algorithm>  // for std::max
#include <memory>     // for std::make_shared
#include <sstream>
#include <stdexcept>  // for std::runtime_error
#include <vector>

#include <boost/logic/tribool.hpp>
#include <spdlog/spdlog.h>

#include "data_structure/json/json.hpp"
#include "engine/engine_constants.hpp"
#include "engine/engine_utils.hpp"
#include "utilities/timer.hpp"

namespace {
/// Waiting value for an idle processor
constexpr int kWaitOnIdleProcessor { -1 };
}  // namespace

namespace optilab {

CombinatorEngine::CombinatorEngine(const std::string& engineId,
                                   const EngineCallbackHandler::SPtr& handler)
: pEngineId(engineId),
  pCallbackHandler(handler)
{
  if (engineId.empty())
  {
    throw std::invalid_argument("CombinatorEngine - empty engine identifier");
  }

  if (!handler)
  {
    throw std::invalid_argument("CombinatorEngine - empty engine callback handler");
  }

  timer::Timer timer;

  pSyncMsgQueue = std::make_shared<connectorengine::SyncQueue>();

  pCombinatorResult = std::make_shared<connectorengine::CombinatorResult>();

  // Builds the optimizer running ORTools CP models
  buildOptimizer();
  assert(pOptimizer);

  // Start  the asynchronous thread in the optimizer
  pOptimizer->startUp();

  pActiveEngine = true;

  pEngineCreationTimeMsec = timer.getWallClockTimeMsec();
}

CombinatorEngine::~CombinatorEngine()
{
  try
  {
    turnDown();
  }
  catch(...)
  {
    // Do not throw in destructor
    spdlog::warn("CombinatorEngine - thrown in destructor");
  }
}

void CombinatorEngine::turnDown()
{
  if (pActiveEngine)
  {
    pActiveEngine = false;
    pOptimizer->tearDown();
    pOptimizer.reset();
  }
}  // turnDown

void CombinatorEngine::buildOptimizer()
{
  // Create and set the factory for ORTools CP processors
  pOptimizer = std::make_shared<CombinatorOptimizer>(pEngineId, pSyncMsgQueue, pCombinatorResult);
}  // buildOptimizer

void CombinatorEngine::loadConnections(const std::string& jsonConnections)
{
  if (jsonConnections.empty())
  {
    std::ostringstream ss;
    ss << "CombinatorEngine - loadConnections: "
        "loading an empty set of connections for engine " << pEngineId;
    spdlog::error(ss.str());
    throw std::runtime_error(ss.str());
  }

  if (!pActiveEngine)
  {
    const std::string errMsg = "CombinatorEngine - loadConnections: "
        "trying to register a model on an inactive engine " + pEngineId;
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  // Start timer for loading the model
  timer::Timer timer;

  // Load the model into the optimizer
  try
  {
    pOptimizer->loadModel(jsonConnections);
  }
  catch(std::exception& ex)
  {
    const std::string errMsg = "CombinatorEngine - loadConnections: "
        "error while loading the model on engine " + pEngineId +
        " " + std::string(ex.what());
    spdlog::error(errMsg);
    throw;
  }
  catch(...)
  {
    const std::string errMsg = "CombinatorEngine - loadConnections: "
        "undefined error while loading the model on engine " + pEngineId;
    spdlog::error(errMsg);
    throw;
  }

  pLoadModelLatencyMsec = timer.getWallClockTimeMsec();

  // Reset solution
  pCombinatorResult->clear();
}  // registerModel

void CombinatorEngine::engineWait(int timeoutMsec)
{
  if (pActiveEngine)
  {
    pOptimizer->waitOnTaskCompletion(timeoutMsec);
  }
}  // engineWait

void CombinatorEngine::notifyEngine(const connectorengine::ConnectorEvent& event)
{
  switch (event.getType()) {
    case connectorengine::ConnectorEvent::EventType::kRunEngine:
    {
      processRunModelEvent();
      break;
    }
    case connectorengine::ConnectorEvent::EventType::kSolutions:
    {
      processCollectSolutionsEvent();
      break;
    }
    case connectorengine::ConnectorEvent::EventType::kStopBackendEngines:
    {
      pOptimizer->broadcastWorkersShutdown();
      break;
    }
    default:
    {
      assert(event.getType() == connectorengine::ConnectorEvent::EventType::kInterruptEngine);
      processInterruptEngineEvent();
      break;
    }
  }
}  // notifyEngine

void CombinatorEngine::processInterruptEngineEvent()
{
  pOptimizer->interruptOptimizer();

  // Set the engine as "inactive"
  pActiveEngine = false;
}  // processKillEngineEvent

void CombinatorEngine::processCollectSolutionsEvent()
{
  timer::Timer timer;

  // Wait until the results are received
  pOptimizer->waitOnReceivedResult();

  if (pCombinatorResult->getNumSolutions() < 1)
  {
    spdlog::warn("CombinatorEngine - processCollectSolutionsEvent: "
        "solutions not found " + pEngineId);
    return;
  }

  // Latency for running the optimizer
  pOptimizerLatencyMsec = timer.getWallClockTimeMsec();

  const auto jsonRes = connectorutils::buildCombinatorSolutionJson(pCombinatorResult);

  // Callback method to send results back to the client
  pCallbackHandler->resultCallback(pEngineId, jsonRes->toString());

  // Callback to send the metrics back to the client
  auto jsonMetrics = engineutils::createMetricsJson(collectMetrics());

  // Callback method to send metrics back to the client
  pCallbackHandler->metricsCallback(pEngineId, jsonMetrics->toString());
}  // processCollectSolutionsEvent

void CombinatorEngine::processRunModelEvent()
{
  // Return if the engine is not active
  if (!pActiveEngine)
  {
    spdlog::warn("CombinatorEngine - processRunModelEvent: "
        "inactive engine, returning " + pEngineId);
    return;
  }

  // Wait on previous tasks to complete.
  // For example, it waits on model registration
  engineWait(kWaitOnIdleProcessor);

  // Trigger the engine to run and return
  pOptimizer->runOptimizer();
}  // processRunModelEvent

MetricsRegister::SPtr CombinatorEngine::collectMetrics()
{
  MetricsRegister::SPtr mreg = std::make_shared<MetricsRegister>();
  mreg->setMetric(jsonmetrics::ENGINE_CREATION_TIME_MSEC, pEngineCreationTimeMsec);
  mreg->setMetric(jsonmetrics::LOAD_MODEL_LATENCY_MSEC, pLoadModelLatencyMsec);
  mreg->setMetric(jsonmetrics::OPTIMIZER_LATENCY_MSEC, pOptimizerLatencyMsec);

  return mreg;
}  // collectMetrics

}  // namespace optilab
