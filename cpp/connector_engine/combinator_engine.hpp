//
// Copyright OptiLab 2019. All rights reserved.
//
// Class for a combinator engine.
//

#pragma once

#include "connector_engine/connector_engine.hpp"

#include <atomic>
#include <cstdint>  // uint64_t
#include <memory>   // for std::shared_ptr
#include <string>

#include "data_structure/metrics/metrics_register.hpp"
#include "connector_engine/combinator_optimizer.hpp"
#include "connector_engine/connector_utilities.hpp"
#include "engine/engine_callback_handler.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS CombinatorEngine : public ConnectorEngine {
 public:
  using JsonModel = std::string;
  using JsonModelList = std::vector<JsonModel>;

  using SPtr = std::shared_ptr<CombinatorEngine>;

 public:
  CombinatorEngine(const std::string& engineId, const EngineCallbackHandler::SPtr& handler);

  ~CombinatorEngine();

  /// Loads the given connections.
  /// @note this method does not run the engine.
  /// @throw std::runtime_error if this model is called while the engine is running
  void loadConnections(const std::string& jsonConnections) override;

  /// Notifies the engine on a given EngineEvent
  void notifyEngine(const connectorengine::ConnectorEvent& event) override;

  /// Wait for the current jobs in the queue to finish.
  /// @note if "timeoutSec" is -1, waits for all jobs in the queue to complete
  void engineWait(int timeoutMsec) override;

  /// Turns-down this engine
  void turnDown() override;

 private:
  /// Engine identifier
  std::string pEngineId;

  /// Handler for callbacks to send back results to caller methods
  EngineCallbackHandler::SPtr pCallbackHandler;

  /// Asynchronous combinator engine processor
  CombinatorOptimizer::SPtr pOptimizer;

  /// Message queue for combinator
  connectorengine::SyncQueue::SPtr pSyncMsgQueue;

  /// Combinator result
  connectorengine::CombinatorResult::SPtr pCombinatorResult;

  /// Latency in msec. to create this engine
  std::atomic<uint64_t> pEngineCreationTimeMsec{0};

  /// Latency in msec. caused by loading a model into the optimizer
  std::atomic<uint64_t> pLoadModelLatencyMsec{0};

  /// Latency in msec. caused by running the optimizer
  std::atomic<uint64_t> pOptimizerLatencyMsec{0};

  /// Flag indicating whether this engine already started running
  std::atomic<bool> pActiveEngine{false};

  // Builds the instance of a ORTools optimizer with a CP processor
  void buildOptimizer();

  /// Runs the registered model
  void processRunModelEvent();

  /// Sends back using the handler all the solutions collected so far
  void processCollectSolutionsEvent();

  /// Interrupts the current computation, if any, and return
  void processInterruptEngineEvent();

  /// Collects metrics and returns correspondent register
  MetricsRegister::SPtr collectMetrics();
};

}  // namespace optilab
