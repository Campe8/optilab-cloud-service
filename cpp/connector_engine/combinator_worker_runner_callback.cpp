#include "connector_engine/combinator_worker_runner_callback.hpp"

#include <stdexcept>  // for std::invalid_argument

#include <spdlog/spdlog.h>

#include "engine/engine_constants.hpp"

namespace optilab {

void CombinatorWorkerRunnerCallback::setSyncQueue(connectorengine::SyncQueue::SPtr queue)
{
  pSyncQueue = queue;
  if (!pSyncQueue)
  {
    const std::string errMsg = "CombinatorWorkerRunnerCallback - setSyncQueue: "
        "empty pointer to synchronization queue";
    spdlog::error(errMsg);
    throw std::invalid_argument(errMsg);
  }
}  // setSyncQueue

void CombinatorWorkerRunnerCallback::onError(int id)
{
  (void) id;
  std::string errMsg = engineconsts::ENGINE_ERROR_MSG;
  pSyncQueue->pushBack(errMsg);
}  // onError

void CombinatorWorkerRunnerCallback::onReply(int id, const OptiLabReplyMessage& reply)
{
  (void) id;

  // Get the reply message
  const auto replyType = reply.type();
  switch (replyType)
  {
    case optilab::OptiLabReplyMessage::Solution:
    {
      OptimizerSolutionRep solRep;
      reply.details().UnpackTo(&solRep);

      // Push the reply back into the queue and notify readers about a new message
      pSyncQueue->pushBack(solRep.solution());
      break;
    }
    case optilab::OptiLabReplyMessage::Metrics:
    {
      /*
      std::string errMsg = "CombinatorWorkerRunnerCallback - onReply: "
          "received metrics expected solution";
      spdlog::error(errMsg);
      throw std::runtime_error(errMsg);
      */
      // Skip back-end pool metrics
      break;
    }
    case optilab::OptiLabReplyMessage::Metadata:
    {
      /*
      std::string errMsg = "CombinatorWorkerRunnerCallback - onReply: "
          "received metadata expected solution";
      spdlog::error(errMsg);
      throw std::runtime_error(errMsg);
      */
      // Skip back-end pool meta-data
      break;
    }
    default:
    {
      std::string errMsg = "CombinatorWorkerRunnerCallback - onReply: "
          "unrecognized message reply type";
      spdlog::error(errMsg);
      throw std::runtime_error(errMsg);
    }
  }
}  // onReply

}  // namespace optilab
