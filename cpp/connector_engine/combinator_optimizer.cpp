#include "connector_engine/combinator_optimizer.hpp"

#include <cassert>
#include <stdexcept>  // for std::invalid_argument

#include <spdlog/spdlog.h>

namespace {
/// Two pipelines will be present: sender and receiver
constexpr int kNumPipelines = 2;
constexpr int kSenderPipelineId = 0;
constexpr int kReceiverPipelineId = 1;

constexpr int kWaitOnAllTaskCompletion = -1;
}  // namespace

namespace optilab {

CombinatorOptimizer::CombinatorOptimizer(const std::string& engineName,
                                         connectorengine::SyncQueue::SPtr syncQueue,
                                         connectorengine::CombinatorResult::SPtr result)
: ConnectorOptimizer(engineName, kNumPipelines),
  pSyncQueue(syncQueue),
  pCombinatorResult(result)
{
  if (!pSyncQueue)
  {
    throw std::invalid_argument("CombinatorOptimizer - empty synchronous queue");
  }

  if (!pCombinatorResult)
  {
    throw std::invalid_argument("CombinatorOptimizer - empty combinator result");
  }

  ConnectorOptimizer::init();
}

void CombinatorOptimizer::loadModel(const std::string& jsonModel)
{
  // Load the model into the optimizer with the sender processor
  assert(pSendProcessor);
  pSendProcessor->loadJSONModel(jsonModel);

  // Set the connections map into the receiver processor
  pRecvProcessor->setCombinatorContext(pSendProcessor->getCombinatorContext());

  // Start the worker runner which is used to communicate with workers
  Work::SPtr work =
      std::make_shared<Work>(connectorengine::ConnectorWork::WorkType::kStartWorkerRunner);
  pushTask(work, kSenderPipelineId);

  // Wait for the processor to completely start the runner
  waitOnTaskCompletion(kWaitOnAllTaskCompletion, kSenderPipelineId);
}  // loadModel

void CombinatorOptimizer::runOptimizer()
{
  if (!isActive())
  {
    // Both processors must be active
    const std::string errMsg = "CombinatorOptimizer - runOptimizer: optimizer is not active";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  assert(pRecvProcessor);
  assert(pSendProcessor);

  // Send a read task to the receiver processor which will start listening on incoming messages
  Work::SPtr workRecv =
      std::make_shared<Work>(connectorengine::ConnectorWork::WorkType::kStartListenerService);
  pushTask(workRecv, kReceiverPipelineId);

  // Start the worker runner that is sending the models to the (distributed) workers/solvers
  Work::SPtr workSend =
      std::make_shared<Work>(connectorengine::ConnectorWork::WorkType::kRunOptimizer);
  pushTask(workSend, kSenderPipelineId);
}  // runOptimizer

void CombinatorOptimizer::broadcastWorkersShutdown()
{
  // Broadcast shutdown message to all workers
  Work::SPtr workSend =
      std::make_shared<Work>(connectorengine::ConnectorWork::WorkType::kBroadcastInterrupt);
  pushTask(workSend, kSenderPipelineId);
}  // broadcastWorkersShutdown

void CombinatorOptimizer::waitOnReceivedResult()
{
  // Wait for the processor to completely start the runner.
  // The receiver will returns as soon as there is a message
  waitOnTaskCompletion(kWaitOnAllTaskCompletion, kReceiverPipelineId);
}  // waitOnReceivedResult

bool CombinatorOptimizer::interruptOptimizer()
{
  assert(pRecvProcessor);
  assert(pSendProcessor);

  pRecvProcessor->interruptProcessor();
  pSendProcessor->interruptProcessor();

  return true;
}  // interruptOptimizer

ProcessorPipeline::SPtr CombinatorOptimizer::buildPipeline(int pipeline)
{
  CombinatorProcessor::SPtr proc =
      std::shared_ptr<CombinatorProcessor>(new CombinatorProcessor(getEngineName()));
  ProcessorPipeline::SPtr pipelineImpl = std::make_shared<ProcessorPipeline>();

  proc->setSyncQueue(pSyncQueue);
  if (pipeline == kSenderPipelineId)
  {
    pSendProcessor = proc;
  }
  else if (pipeline == kReceiverPipelineId)
  {
    proc->setCombinatorResult(pCombinatorResult);
    pRecvProcessor = proc;
  }
  else
  {
    throw std::runtime_error("CombinatorOptimizer - buildPipeline: "
        "invalid pipeline index " + std::to_string(pipeline));
  }
  pipelineImpl->pushBackProcessor(proc);
  return pipelineImpl;
}  // buildPipeline

}  // namespace optilab
