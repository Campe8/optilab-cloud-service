//
// Copyright OptiLab 2020. All rights reserved.
//
// Processor for running custom Python code.
//

#pragma once

#include <cstdint>  // for uint64_t
#include <memory>   // for std::shared_ptr
#include <string>
#include <utility>  // for std::pair
#include <vector>

#include <boost/thread.hpp>
#include <pybind11/pybind11.h>
#include <pybind11/embed.h>

#include "engine/optimizer_processor.hpp"
#include "data_toolbox/data_instance.hpp"
#include "data_toolbox/data_utilities.hpp"
#include "data_toolbox/python_engine_utils.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace toolbox {

class SYS_EXPORT_CLASS PythonProcessor :
public OptimizerProcessor<dataengine::PythonFcnDataWork::SPtr, dataengine::PythonFcnDataWork::SPtr>{
public:
  using SPtr = std::shared_ptr<PythonProcessor>;

public:
  PythonProcessor(const std::string& procName, dataengine::DataResult::SPtr result);

  ~PythonProcessor();

  /// Loads the Python code instance
  void loadEnvironmentAndCreateSolver(DataInstance::UPtr instance);

  /// Interrupts the current engine execution (if any).
  /// The engine remains in a non-executable state until another environment is loaded.
  /// Returns true on success, false otherwise
  bool interruptSolver();

protected:
  using Work = dataengine::PythonFcnDataWork::SPtr;
  using BaseProcessor = OptimizerProcessor<Work, Work>;

protected:
  void processWork(Work work) override;

  inline dataengine::PythonFcnResult::SPtr getResult() const noexcept { return pResult; }

private:
  /// Interface data used to call the python interpreter
  pythonengineutils::PythonInterfaceData interfaceData;

  /// Pointer to the Python instance to run
  DataInstance::UPtr pPythonInstance;

  /// Python result
  dataengine::PythonFcnResult::SPtr pResult;

  /// Variable keeping track of the latency due to the initialization process in msec
  uint64_t pLatencyInitMsec;

  /// Variable keeping track of the optimization process latency in msec
  uint64_t pEvolutionLatencyMsec;

  /// Variable keeping track of the overall latency due to the optimization process in msec
  uint64_t pOptimizerLatencyMsec;

  dataengine::DataResult::OptimizerStatus pPythonProcessorStatus{
    dataengine::DataResult::OptimizerStatus::OS_UNKNOWN};

  /// Returns the status of the internal optimizer
  inline dataengine::DataResult::OptimizerStatus getStatus() const noexcept
  {
    return pPythonProcessorStatus;
  }

  /// Triggers the execution of the solver
  /// @note this doesn't check whether an engine is currently running or not.
  /// It is responsibility of the caller to handle that logic
  void runSolver(Work& work);
  void runSolverImpl();

  /// Utility function: set error, log and return
  void setErrorStatus(const std::string& errMsg);

  /// Utility function: returns true if the processor is in an error state.
  /// Returns false otherwise
  inline bool isInErrorState() const noexcept
  {
    return (pPythonProcessorStatus == dataengine::DataResult::OptimizerStatus::OS_ERROR);
  }

  /// Cleanup state
  void cleanup();
};

}  // namespace toolbox
}  // namespace optilab
