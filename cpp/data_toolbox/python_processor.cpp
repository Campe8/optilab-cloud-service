#include "data_toolbox/python_processor.hpp"

#include <unistd.h>

#include <cassert>
#include <cstdio>
#include <exception>
#include <functional>  // for std::bind
#include <stdexcept>   // for std::invalid_argument
#include <sys/wait.h>

#include <pybind11/stl.h>
#include <spdlog/spdlog.h>

#include <stdio.h>
#include <sched.h>
#include <stdlib.h>

#include "data_structure/metrics/metrics_register.hpp"
#include "engine/engine_constants.hpp"
#include "utilities/timer.hpp"

#include "data_toolbox/python_engine_utils.hpp"

#define STACK_SIZE (1024 * 1024)

namespace optilab {
namespace toolbox {

PythonProcessor::PythonProcessor(const std::string& procName, dataengine::DataResult::SPtr result)
: BaseProcessor(procName),
  pLatencyInitMsec(0),
  pEvolutionLatencyMsec(0),
  pOptimizerLatencyMsec(0)
{
  pResult = std::dynamic_pointer_cast<dataengine::PythonFcnResult>(result);
  if (!pResult)
  {
    throw std::runtime_error("PythonProcessor - empty pointer to result");
  }
}

PythonProcessor::~PythonProcessor()
{
  try
  {
    cleanup();
  }
  catch(...)
  {
    spdlog::error("PythonProcessor - exception in destructor");
  }
}

void PythonProcessor::setErrorStatus(const std::string& errMsg)
{
  pPythonProcessorStatus = dataengine::DataResult::OptimizerStatus::OS_ERROR;
  spdlog::error(errMsg);
}  // setErrorStatus

void PythonProcessor::loadEnvironmentAndCreateSolver(DataInstance::UPtr instance)
{
  // Take ownership of the instance to solve
  pPythonInstance = std::move(instance);

  auto& model = pPythonInstance->getModel();
  if (!model.has_python_model())
  {
    setErrorStatus("PythonProcessor - loadEnvironmentAndCreateSolver: "
            "model is not valid for the Python engine");
    return;
  }

  const auto& pythonModel = model.python_model();
  interfaceData.protoModel = &pythonModel;
  interfaceData.protoOutput = pResult->getPythonFcnSolutionProto();

  try
  {
    pythonengineutils::loadPythonOutputDescriptor(interfaceData.protoModel,
                                                  interfaceData.outputDescriptor);
  }
  catch(...)
  {
    setErrorStatus("PythonProcessor - loadEnvironmentAndCreateSolver: "
            "model is not valid for the Python engine - invalid output types");
    return;
  }
}  // loadEnvironmentAndCreateSolver

bool PythonProcessor::interruptSolver()
{
  return true;
}  // interruptSolver

void PythonProcessor::cleanup()
{
  // No-op
}  // cleanup

void PythonProcessor::processWork(Work work)
{
  if (work->workType == dataengine::DataWork::kRunOptimizer)
  {
    runSolver(work);
  }
  else
  {
    spdlog::warn("PythonProcessor - processWork: unrecognized work type");
  }
}  // processWork

void PythonProcessor::runSolver(Work& work)
{
  timer::Timer timer;
  if (work->dataInstance != nullptr)
  {
    loadEnvironmentAndCreateSolver(std::move(work->dataInstance));
  }

  if (isInErrorState())
  {
    return;
  }

  runSolverImpl();

  const auto wallclockMsec = timer.getWallClockTimeMsec();
  work->metricsRegister->setMetric(jsonmetrics::OPTIMIZER_LATENCY_MSEC, wallclockMsec);
}  // runSolver

void PythonProcessor::runSolverImpl()
{
  if (isInErrorState())
  {
    pResult->status = pPythonProcessorStatus;
    return;
  }

  // There can be ONLY ONE interpreter running on the same process, as per
  // https://pybind11.readthedocs.io/en/stable/advanced/embedding.html.
  // Therefore, this thread must fork another process to run the interpreter
   void* pchildStack = malloc(STACK_SIZE);

   // TODO: check the following memory allocation
   //stack = mmap(NULL, STACK_SIZE, PROT_READ | PROT_WRITE,
   //             MAP_PRIVATE | MAP_ANONYMOUS | MAP_STACK, -1, 0);
   //if (stack == MAP_FAILED)
   //    errExit("mmap");
   // where
   // #define STACK_SIZE (1024 * 1024)    /* Stack size for cloned child */
   if (pchildStack == NULL)
   {
     setErrorStatus("PythonProcessor - loadEnvironmentAndCreateSolver: "
             "failure in setting up the pipe");
     return;
   }

   // Prepare the pipe to communicate with the cloned process
   int pipefd[2];
   if (pipe(pipefd) == -1)
   {
     setErrorStatus("PythonProcessor - loadEnvironmentAndCreateSolver: "
             "failure in setting up the pipe");
     free(pchildStack);
     return;
   }

   // Set the pipe
   interfaceData.pipefd[0] = pipefd[0];
   interfaceData.pipefd[1] = pipefd[1];

   int pid = clone(pythonengineutils::callPythonInterpreter,
                   ((uint8_t*)pchildStack) + (STACK_SIZE), SIGCHLD,
                    static_cast<void*>(&interfaceData));
   if (pid < 0)
   {
     setErrorStatus("ERROR: Unable to create the child process.");
     free(pchildStack);
     return;
   }

   // Close unused write end
   close(pipefd[1]);

   // Read the proto output from the file descriptor
   interfaceData.protoOutput->ParseFromFileDescriptor(pipefd[0]);

   // Wait for child
   if (waitpid(pid, NULL, 0) == -1)
   {
     spdlog::warn("PythonProcessor - runSolverImpl: "
             "error while waiting for the cloned process");
     pPythonProcessorStatus = dataengine::DataResult::OptimizerStatus::OS_ERROR;
   }
   free(pchildStack);

   pPythonProcessorStatus = dataengine::DataResult::OptimizerStatus::OS_OKAY;
   if (interfaceData.protoOutput->interpreter_has_failed())
   {
     std::string msg = "PythonProcessor - runSolverImpl: interpreter has failed - " +
             interfaceData.protoOutput->interpreter_info_msg();
     spdlog::error(msg);

     pPythonProcessorStatus = dataengine::DataResult::OptimizerStatus::OS_ERROR;
   }
   pResult->status = pPythonProcessorStatus;

   pLatencyInitMsec = interfaceData.protoOutput->load_model_msec();

   // Optimization process latency
   pEvolutionLatencyMsec = interfaceData.protoOutput->run_model_msec();

   // Get the overall wall-clock time
   pOptimizerLatencyMsec = pLatencyInitMsec + interfaceData.runModelMsec;

   cleanup();
}  // runSolverImpl

}  // namespace toolbox
}  // namespace optilab
