//
// Copyright OptiLab 2020. All rights reserved.
//
// Base class for a evolutionary strategy instance.
//

#pragma once

#include <memory>  // for std::unique_ptr

#include "optilab_protobuf/data_model.pb.h"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace toolbox {

class SYS_EXPORT_CLASS DataInstance {
 public:
  using UPtr = std::unique_ptr<DataInstance>;
  using SPtr = std::shared_ptr<DataInstance>;

 public:
  DataInstance(const DataModelProto& dataModel)
 : pDataModel(dataModel)
 {
 }

  inline const DataModelProto& getModel() const noexcept
  {
    return pDataModel;
  }

 private:
  /// Protobuf containing the model for data and ports
  DataModelProto pDataModel;
};

}  // namespace toolbox
}  // namespace optilab
