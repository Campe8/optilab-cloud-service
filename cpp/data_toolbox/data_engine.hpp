//
// Copyright OptiLab 2020. All rights reserved.
//
// Base class for a general engine working on input/output data processing.
//

#pragma once

#include "engine/engine.hpp"

#include <memory>  // for std::shared_ptr
#include <string>

#include "data_toolbox/data_instance.hpp"
#include "data_toolbox/data_utilities.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace toolbox {

class SYS_EXPORT_CLASS DataEngine {
 public:
  using SPtr = std::shared_ptr<DataEngine>;

 public:
  virtual ~DataEngine() = default;

  /// Registers the instance of data the engine should run on/process.
  /// @throw std::runtime_error if this model is called while the engine is running
  virtual void registerInstance(DataInstance::UPtr instance) = 0;

  /// Notifies the optimizer on a given event
  virtual void notifyEngine(const dataengine::DataEvent& event) = 0;

  /// Wait for the current jobs in the queue to finish.
  /// @note if "timeoutSec" is -1, waits for all jobs in the queue to complete
  virtual void engineWait(int timeoutMsec) = 0;

  /// Shuts down the engine
  virtual void turnDown() = 0;
};

}  // namespace toolbox
}  // namespace optilab
