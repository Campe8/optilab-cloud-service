//
// Copyright OptiLab 2020. All rights reserved.
//
// Utilities for Data and Ports optimizers.
//

#pragma once


#include <string>
#include <utility>  // for std::pair
#include <vector>

#include "data_structure/metrics/metrics_register.hpp"
#include "data_toolbox/data_instance.hpp"
#include "optilab_protobuf/data_model.pb.h"
#include "utilities/random_generators.hpp"

namespace optilab {
namespace toolbox {

namespace dataengine {

enum class DataProcessorType : int {
  /// Processor for running general Python code
  DPT_PYTHON_FCN = 0,

  /// Undefined evolutionary processor type.
  /// @note this is the default if the processor type
  /// is not specified
  DPT_UNDEF
};


struct SYS_EXPORT_STRUCT DataWork {
using SPtr = std::shared_ptr<DataWork>;

enum WorkType {
  /// Runs the optimizer on the loaded model
  kRunOptimizer,
  kWorkTypeUndef
};

DataWork(WorkType wtype, MetricsRegister::SPtr metReg)
: workType(wtype), metricsRegister(metReg)
{
}

WorkType workType;
MetricsRegister::SPtr metricsRegister;
};

struct SYS_EXPORT_CLASS PythonFcnDataWork : public DataWork {
  using SPtr = std::shared_ptr<PythonFcnDataWork>;
  PythonFcnDataWork(WorkType wtype, MetricsRegister::SPtr metReg)
  : DataWork(wtype, metReg)
  {
  }

  DataInstance::UPtr dataInstance{nullptr};
};

class SYS_EXPORT_CLASS DataEvent {
 public:
  enum EventType {
    /// Starts the solving process
    kRunEngine,
    /// Kills any ongoing computation
    kInterruptEngine,
    /// Collects solutions
    kSolutions
  };

 public:
  explicit DataEvent(EventType aType)
  : pEventType(aType)
  {
  }

  inline EventType getType() const noexcept
  {
    return pEventType;
  }

  inline void setNumSolutions(int numSolutions) noexcept
  {
    pNumSolutions = numSolutions;
  }

  inline int getNumSolutions() const noexcept
  {
    return pNumSolutions;
  }

  inline void setDataInstance(DataInstance::UPtr instance) const noexcept
  {
    dataInstance = std::move(instance);
  }

  inline DataInstance::UPtr getDataInstance() const noexcept
  {
    return std::move(dataInstance);
  }

 private:
  /// Type of this event
  EventType pEventType;

  /// Number of solutions for "kSolutions" events
  int pNumSolutions{0};

  /// Data instance to solve "on-the-fly"
  mutable DataInstance::UPtr dataInstance{nullptr};
};

struct SYS_EXPORT_STRUCT DataResult {
  /// Status of the optimizer
  enum OptimizerStatus {
    /// Timeout occurred
    OS_TIME = 0,

    /// Normal run
    OS_OKAY,

    /// Other error
    OS_ERROR,

    /// Unknown
    OS_UNKNOWN
  };

  using SPtr = std::shared_ptr<DataResult>;

  /// Name of the model
  std::string modelName;

  /// Status of the solution
  OptimizerStatus status{OptimizerStatus::OS_UNKNOWN};

  /// Solution to send back to the client
  DataSolutionProto solutionProto;

  virtual ~DataResult() = default;
};

struct SYS_EXPORT_STRUCT PythonFcnResult : public DataResult {
  using SPtr = std::shared_ptr<PythonFcnResult>;

  ::optilab::toolbox::PythonFcnSolutionProto* getPythonFcnSolutionProto()
  {
    return solutionProto.mutable_python_fcn_solution();
  }

  // Clears current result
  void clear() {}
};

}  // namespace dataengine

namespace datautils {
/// Builds a protobuf object representing the data solution given as input
SYS_EXPORT_FCN DataSolutionProto buildPythonFcnSolutionProto(dataengine::DataResult::SPtr res);
}  // namespace datautils

}  // namespace toolbox
}  // namespace optilab
