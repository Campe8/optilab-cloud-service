//
// Copyright OptiLab 2020. All rights reserved.
//
// Utilities for the Python engine.
//

#pragma once

#include <cstdint>  // for uint64_t
#include <utility>  // for std::pair
#include <vector>

#include "optilab_protobuf/data_model.pb.h"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace toolbox {
namespace pythonengineutils {

enum class PythonTypeEnum {
  kTypeDouble = 0,
  kTypeFloat,
  kTypeInt,
  kTypeUInt,
  kTypeBool,
  kTypeString,
  kTypeBytes
};

/// Output description is a list of lists of output types.
/// For example, the following Python output:
/// 10, [10.2, 11.3, 80.9]
/// is described as:
/// [[1, int], [3, double]]
using OutputDescriptor = std::vector<std::pair<int, PythonTypeEnum>>;

struct SYS_EXPORT_STRUCT PythonInterfaceData {
  // Input model description as protobuf
  const ::optilab::toolbox::PythonModelProto* protoModel{nullptr};

  // Protobuf output
  ::optilab::toolbox::PythonFcnSolutionProto* protoOutput{nullptr};

  // Descriptor for the output of the Python function
  OutputDescriptor outputDescriptor;

  // Pipe used to communicate back to the host process
  int pipefd[2];

  uint64_t loadModelMsec{0};
  uint64_t runModelMsec{0};
};

SYS_EXPORT_FCN void loadPythonOutputDescriptor(
        const ::optilab::toolbox::PythonModelProto* pythonModel,
        OutputDescriptor& outputDescriptor);

SYS_EXPORT_FCN int callPythonInterpreter(void* arg);

}  // namespace pythonengineutils
}  // namespace toolbox
}  // namespace optilab
