#include "data_toolbox/python_engine_utils.hpp"

#include <vector>

#include "utilities/timer.hpp"

#include <pybind11/embed.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include <spdlog/spdlog.h>

namespace {

void logErrorOnPythonInterpreter(::optilab::toolbox::PythonFcnSolutionProto* protoOutput,
                                 const std::string& msg, int fd)
{
  spdlog::error(msg);
  protoOutput->set_interpreter_has_failed(true);
  protoOutput->set_interpreter_info_msg(msg);
  protoOutput->SerializeToFileDescriptor(fd);
}  // logErrorOnPythonInterpreter

void addOutputData(::optilab::toolbox::PythonFcnTypedValue* arg,
                   optilab::toolbox::pythonengineutils::PythonTypeEnum type,
                   const pybind11::object& obj)
{
  switch(type)
  {
    case optilab::toolbox::pythonengineutils::PythonTypeEnum::kTypeDouble:
    {
      double val = obj.cast<double>();
      arg->set_double_arg(val);
      break;
    }
    case optilab::toolbox::pythonengineutils::PythonTypeEnum::kTypeFloat:
    {
      float val = obj.cast<float>();
      arg->set_float_arg(val);
      break;
    }
    case optilab::toolbox::pythonengineutils::PythonTypeEnum::kTypeInt:
    {
      int64_t val = obj.cast<int64_t>();
      arg->set_int_arg(val);
      break;
    }
    case optilab::toolbox::pythonengineutils::PythonTypeEnum::kTypeUInt:
    {
      uint64_t val = obj.cast<uint64_t>();
      arg->set_uint_arg(val);
      break;
    }
    case optilab::toolbox::pythonengineutils::PythonTypeEnum::kTypeBool:
    {
      bool val = obj.cast<bool>();
      arg->set_bool_arg(val);
      break;
    }
    case optilab::toolbox::pythonengineutils::PythonTypeEnum::kTypeString:
    {
      std::string val = obj.cast<std::string>();
      arg->set_string_arg(val);
      break;
    }
    case optilab::toolbox::pythonengineutils::PythonTypeEnum::kTypeBytes:
    {
      std::string val = obj.cast<std::string>();
      arg->set_bytes_arg(val);
      break;
    }
    default:
    {
      throw std::runtime_error("PythonProcessor - addOutputData: "
              "unrecognized output type");
    }
  }
}  // addOutputData

void addOutputToProtoOutput(
        ::optilab::toolbox::PythonFcnArgument* protoOut,
         const pybind11::object& out,
         const optilab::toolbox::pythonengineutils::OutputDescriptor& outputDescriptor,
         int outIdx)
{
  const auto& outDescriptor = outputDescriptor.at(outIdx);
  if (outDescriptor.first == 1)
  {
    // One scalar: add it directly to the list of outputs
    auto arg = protoOut->add_argument();
    addOutputData(arg, outDescriptor.second, out);
  }
  else
  {
    // A list of values: iterate over it and add each element of the list
    for (const pybind11::handle& subOutHandle : out)
    {
      pybind11::object outData = pybind11::reinterpret_borrow<pybind11::object>(subOutHandle.ptr());
      auto arg = protoOut->add_argument();
      addOutputData(arg, outDescriptor.second, outData);
    }
  }
}  // addOutputToProtoOutput

void storePythonSolution(
        ::optilab::toolbox::PythonFcnSolutionProto* protoOutput, pybind11::object& result,
         const optilab::toolbox::pythonengineutils::OutputDescriptor& outputDescriptor)
{
  int outIdx{0};
  if (outputDescriptor.size() > 1)
  {
    for (const auto &out : result)
    {
      auto protoOut = protoOutput->add_output();
      pybind11::object outObj = pybind11::reinterpret_borrow<pybind11::object>(out.ptr());
      addOutputToProtoOutput(protoOut, outObj, outputDescriptor, outIdx++);
    }
  }
  else
  {
    auto protoOut = protoOutput->add_output();
    addOutputToProtoOutput(protoOut, result, outputDescriptor, outIdx++);
  }
}  // storePythonSolution

optilab::toolbox::pythonengineutils::PythonTypeEnum getPythonTypeFromArg(
        const ::optilab::toolbox::PythonFcnTypedValue& val)
{
  const auto type = val.argType_case();
  switch (type)
  {
    case ::optilab::toolbox::PythonFcnTypedValue::ArgTypeCase::kDoubleArg:
      return optilab::toolbox::pythonengineutils::PythonTypeEnum::kTypeDouble;
    case ::optilab::toolbox::PythonFcnTypedValue::ArgTypeCase::kFloatArg:
      return optilab::toolbox::pythonengineutils::PythonTypeEnum::kTypeFloat;
    case ::optilab::toolbox::PythonFcnTypedValue::ArgTypeCase::kIntArg:
      return optilab::toolbox::pythonengineutils::PythonTypeEnum::kTypeInt;
    case ::optilab::toolbox::PythonFcnTypedValue::ArgTypeCase::kUintArg:
      return optilab::toolbox::pythonengineutils::PythonTypeEnum::kTypeUInt;
    case ::optilab::toolbox::PythonFcnTypedValue::ArgTypeCase::kBoolArg:
      return optilab::toolbox::pythonengineutils::PythonTypeEnum::kTypeBool;
    case ::optilab::toolbox::PythonFcnTypedValue::ArgTypeCase::kStringArg:
      return optilab::toolbox::pythonengineutils::PythonTypeEnum::kTypeString;
    case ::optilab::toolbox::PythonFcnTypedValue::ArgTypeCase::kBytesArg:
      return optilab::toolbox::pythonengineutils::PythonTypeEnum::kTypeBytes;
    default:
      throw std::runtime_error("Invalid Python function argument");
  }
}  // getPythonTypeFromArg

}  // namespace

namespace optilab {
namespace toolbox {
namespace pythonengineutils {

void loadPythonOutputDescriptor(const ::optilab::toolbox::PythonModelProto* pythonModel,
                                OutputDescriptor& outputDescriptor)
{
  for (int outputIdx = 0; outputIdx < pythonModel->output_args_size(); ++outputIdx)
  {
    const auto& fcnOut = pythonModel->output_args(outputIdx);
    std::pair<int, PythonTypeEnum> outDescription;
    outDescription.first = fcnOut.argument_size();
    if (outDescription.first == 0)
    {
      continue;
    }

    outDescription.second = getPythonTypeFromArg(fcnOut.argument(0));
    outputDescriptor.push_back(outDescription);
  }
}  // loadPythonOutputs

int callPythonInterpreter(void* arg)
{
  if (arg == nullptr)
  {
    spdlog::error("Empty input argument pointer");
    return -1;
  }

  struct optilab::toolbox::pythonengineutils::PythonInterfaceData* pythonData =
          (struct optilab::toolbox::pythonengineutils::PythonInterfaceData*)arg;
  if (pythonData->protoModel == nullptr || pythonData->protoOutput == nullptr)
  {
    spdlog::error("Empty input pointers");
    return -1;
  }

  // Close unused read end
  close(pythonData->pipefd[0]);

  // Get the information the the path to add to the Python environment
  // as well as the module and function names to load
  auto environmentPath = pythonData->protoModel->python_file_path(0);
  auto pythonModule = pythonData->protoModel->entry_module_name();
  auto pythonFcn = pythonData->protoModel->entry_fcn_name();
  if (environmentPath.empty() || pythonModule.empty() || pythonFcn.empty())
  {
    logErrorOnPythonInterpreter(pythonData->protoOutput,
                                "Empty values for environment path, module, or function name",
                                pythonData->pipefd[1]);
    return -1;
  }

  // Start the Python interpreter
  pybind11::scoped_interpreter guard{};

  timer::Timer loadModelTimer;

  // Load the environment
  try
  {
    auto sys = pybind11::module::import("sys");
    auto path = sys.attr("path");
    auto appendFcn = path.attr("append");
    auto pathObj = pybind11::cast(environmentPath);
    appendFcn(pathObj);
  }
  catch (...)
  {
    logErrorOnPythonInterpreter(pythonData->protoOutput,
                                "Error while loading Python environment",
                                pythonData->pipefd[1]);
    return -1;
  }

  // Check if the module exists in the environment path
  auto implib = pybind11::module::import("importlib");
  auto loader = implib.attr("find_loader")(pythonModule.c_str());
  if (loader.is_none())
  {
    logErrorOnPythonInterpreter(pythonData->protoOutput,
                                "Error while loading Python environment: no module found",
                                pythonData->pipefd[1]);
    return -1;
  }

  // Load the callback function
  pybind11::object pythonCallback = pybind11::module::import(pythonModule.c_str()).attr(pythonFcn.c_str());

  std::vector<pybind11::object> inputArgs;
  for (int inputIdx = 0; inputIdx < pythonData->protoModel->input_args_size(); ++inputIdx)
  {
    const auto& fcnArg = pythonData->protoModel->input_args(inputIdx);
    if (fcnArg.argument_size() == 0)
    {
      continue;
    }

    if (fcnArg.argument_size() == 1)
    {
      const auto& scalar = fcnArg.argument(0);
      const auto type = scalar.argType_case();
      switch (type)
      {
        case ::optilab::toolbox::PythonFcnTypedValue::ArgTypeCase::kDoubleArg:
          inputArgs.push_back(pybind11::cast(scalar.double_arg()));
          break;
        case ::optilab::toolbox::PythonFcnTypedValue::ArgTypeCase::kFloatArg:
          inputArgs.push_back(pybind11::cast(scalar.float_arg()));
          break;
        case ::optilab::toolbox::PythonFcnTypedValue::ArgTypeCase::kIntArg:
          inputArgs.push_back(pybind11::cast(scalar.int_arg()));
          break;
        case ::optilab::toolbox::PythonFcnTypedValue::ArgTypeCase::kUintArg:
          inputArgs.push_back(pybind11::cast(scalar.uint_arg()));
          break;
        case ::optilab::toolbox::PythonFcnTypedValue::ArgTypeCase::kBoolArg:
          inputArgs.push_back(pybind11::cast(scalar.bool_arg()));
          break;
        case ::optilab::toolbox::PythonFcnTypedValue::ArgTypeCase::kStringArg:
          inputArgs.push_back(pybind11::cast(scalar.string_arg()));
          break;
        case ::optilab::toolbox::PythonFcnTypedValue::ArgTypeCase::kBytesArg:
          inputArgs.push_back(pybind11::cast(scalar.bytes_arg()));
          break;
        default:
          logErrorOnPythonInterpreter(pythonData->protoOutput,
                                      "Invalid Python function input argument type",
                                      pythonData->pipefd[1]);
          return -1;
      }
    }
    else if (fcnArg.argument_size() > 1)
    {
      const auto& arg = fcnArg.argument(0);
      const auto type =  arg.argType_case();
        switch(type)
        {
          case ::optilab::toolbox::PythonFcnTypedValue::ArgTypeCase::kDoubleArg:
          {
            std::vector<double> list;
            for (int idx = 0; idx < fcnArg.argument_size(); ++idx)
            {
              const auto& item = fcnArg.argument(idx);
              list.push_back(item.double_arg());
            }
            inputArgs.push_back(pybind11::cast(list));
            break;
          }
          case ::optilab::toolbox::PythonFcnTypedValue::ArgTypeCase::kFloatArg:
          {
            std::vector<float> list;
            for (int idx = 0; idx < fcnArg.argument_size(); ++idx)
            {
              const auto& item = fcnArg.argument(idx);
              list.push_back(item.float_arg());
            }
            inputArgs.push_back(pybind11::cast(list));
            break;
          }
          case ::optilab::toolbox::PythonFcnTypedValue::ArgTypeCase::kIntArg:
          {
            std::vector<int64_t> list;
            for (int idx = 0; idx < fcnArg.argument_size(); ++idx)
            {
              const auto& item = fcnArg.argument(idx);
              list.push_back(item.int_arg());
            }
            inputArgs.push_back(pybind11::cast(list));
            break;
          }
          case ::optilab::toolbox::PythonFcnTypedValue::ArgTypeCase::kUintArg:
          {
            std::vector<uint64_t> list;
            for (int idx = 0; idx < fcnArg.argument_size(); ++idx)
            {
              const auto& item = fcnArg.argument(idx);
              list.push_back(item.uint_arg());
            }
            inputArgs.push_back(pybind11::cast(list));
            break;
          }
          case ::optilab::toolbox::PythonFcnTypedValue::ArgTypeCase::kBoolArg:
          {
            std::vector<bool> list;
            for (int idx = 0; idx < fcnArg.argument_size(); ++idx)
            {
              const auto& item = fcnArg.argument(idx);
              list.push_back(item.bool_arg());
            }
            inputArgs.push_back(pybind11::cast(list));
            break;
          }
          case ::optilab::toolbox::PythonFcnTypedValue::ArgTypeCase::kStringArg:
          {
            std::vector<std::string> list;
            for (int idx = 0; idx < fcnArg.argument_size(); ++idx)
            {
              const auto& item = fcnArg.argument(idx);
              list.push_back(item.string_arg());
            }
            inputArgs.push_back(pybind11::cast(list));
            break;
          }
          case ::optilab::toolbox::PythonFcnTypedValue::ArgTypeCase::kBytesArg:
          {
            std::vector<std::string> list;
            for (int idx = 0; idx < fcnArg.argument_size(); ++idx)
            {
              const auto& item = fcnArg.argument(idx);
              list.push_back(item.bytes_arg());
            }
            inputArgs.push_back(pybind11::cast(list));
            break;
          }
          default:
            logErrorOnPythonInterpreter(pythonData->protoOutput,
                                        "Invalid Python function input argument list type",
                                        pythonData->pipefd[1]);
            return -1;
        }
    }
  }
  pythonData->protoOutput->set_load_model_msec(loadModelTimer.getWallClockTimeMsec());

  timer::Timer runModelTimer;

  // Run the code
  pybind11::object result;
  try
  {
    // Run the callback on the list of input arguments
    if (inputArgs.empty()) {
      result = pythonCallback();
    } else if (inputArgs.size() == 1) {
      result = pythonCallback(inputArgs[0]);
    } else if (inputArgs.size() == 2) {
      result = pythonCallback(inputArgs[0], inputArgs[1]);
    } else if (inputArgs.size() == 3) {
      result = pythonCallback(inputArgs[0], inputArgs[1], inputArgs[2]);
    } else if (inputArgs.size() == 4) {
      result = pythonCallback(inputArgs[0], inputArgs[1], inputArgs[2], inputArgs[3]);
    } else if (inputArgs.size() == 5) {
      result = pythonCallback(inputArgs[0], inputArgs[1], inputArgs[2], inputArgs[3],
                               inputArgs[4]);
    } else if (inputArgs.size() == 6) {
      result = pythonCallback(inputArgs[0], inputArgs[1], inputArgs[2], inputArgs[3],
                               inputArgs[4], inputArgs[5]);
    } else if (inputArgs.size() == 7) {
      result = pythonCallback(inputArgs[0], inputArgs[1], inputArgs[2], inputArgs[3],
                               inputArgs[4], inputArgs[5], inputArgs[6]);
    } else if (inputArgs.size() == 8) {
      result = pythonCallback(inputArgs[0], inputArgs[1], inputArgs[2], inputArgs[3],
                               inputArgs[4], inputArgs[5], inputArgs[6], inputArgs[7]);
    } else if (inputArgs.size() == 9) {
      result = pythonCallback(inputArgs[0], inputArgs[1], inputArgs[2], inputArgs[3],
                               inputArgs[4], inputArgs[5], inputArgs[6], inputArgs[7],
                               inputArgs[8]);
    } else if (inputArgs.size() == 10) {
      result = pythonCallback(inputArgs[0], inputArgs[1], inputArgs[2], inputArgs[3],
                               inputArgs[4], inputArgs[5], inputArgs[6], inputArgs[7],
                               inputArgs[8], inputArgs[9]);
    } else if (inputArgs.size() == 11) {
      result = pythonCallback(inputArgs[0], inputArgs[1], inputArgs[2], inputArgs[3],
                               inputArgs[4], inputArgs[5], inputArgs[6], inputArgs[7],
                               inputArgs[8], inputArgs[9], inputArgs[10]);
    } else if (inputArgs.size() == 12) {
      result = pythonCallback(inputArgs[0], inputArgs[1], inputArgs[2], inputArgs[3],
                               inputArgs[4], inputArgs[5], inputArgs[6], inputArgs[7],
                               inputArgs[8], inputArgs[9], inputArgs[10], inputArgs[11]);
    } else if (inputArgs.size() == 13) {
      result = pythonCallback(inputArgs[0], inputArgs[1], inputArgs[2], inputArgs[3],
                               inputArgs[4], inputArgs[5], inputArgs[6], inputArgs[7],
                               inputArgs[8], inputArgs[9], inputArgs[10], inputArgs[11],
                               inputArgs[12]);
    } else if (inputArgs.size() == 14) {
      result = pythonCallback(inputArgs[0], inputArgs[1], inputArgs[2], inputArgs[3],
                               inputArgs[4], inputArgs[5], inputArgs[6], inputArgs[7],
                               inputArgs[8], inputArgs[9], inputArgs[10], inputArgs[11],
                               inputArgs[12], inputArgs[13]);
    } else if (inputArgs.size() == 15) {
      result = pythonCallback(inputArgs[0], inputArgs[1], inputArgs[2], inputArgs[3],
                               inputArgs[4], inputArgs[5], inputArgs[6], inputArgs[7],
                               inputArgs[8], inputArgs[9], inputArgs[10], inputArgs[11],
                               inputArgs[12], inputArgs[13], inputArgs[14]);
    } else if (inputArgs.size() == 16) {
      result = pythonCallback(inputArgs[0], inputArgs[1], inputArgs[2], inputArgs[3],
                               inputArgs[4], inputArgs[5], inputArgs[6], inputArgs[7],
                               inputArgs[8], inputArgs[9], inputArgs[10], inputArgs[11],
                               inputArgs[12], inputArgs[13], inputArgs[14], inputArgs[15]);
    } else if (inputArgs.size() == 17) {
      result = pythonCallback(inputArgs[0], inputArgs[1], inputArgs[2], inputArgs[3],
                               inputArgs[4], inputArgs[5], inputArgs[6], inputArgs[7],
                               inputArgs[8], inputArgs[9], inputArgs[10], inputArgs[11],
                               inputArgs[12], inputArgs[13], inputArgs[14], inputArgs[15],
                               inputArgs[16]);
    } else if (inputArgs.size() == 18) {
      result = pythonCallback(inputArgs[0], inputArgs[1], inputArgs[2], inputArgs[3],
                               inputArgs[4], inputArgs[5], inputArgs[6], inputArgs[7],
                               inputArgs[8], inputArgs[9], inputArgs[10], inputArgs[11],
                               inputArgs[12], inputArgs[13], inputArgs[14], inputArgs[15],
                               inputArgs[16], inputArgs[17]);
    } else if (inputArgs.size() == 19) {
      result = pythonCallback(inputArgs[0], inputArgs[1], inputArgs[2], inputArgs[3],
                               inputArgs[4], inputArgs[5], inputArgs[6], inputArgs[7],
                               inputArgs[8], inputArgs[9], inputArgs[10], inputArgs[11],
                               inputArgs[12], inputArgs[13], inputArgs[14], inputArgs[15],
                               inputArgs[16], inputArgs[17], inputArgs[18]);
    } else if (inputArgs.size() == 20) {
      result = pythonCallback(inputArgs[0], inputArgs[1], inputArgs[2], inputArgs[3],
                               inputArgs[4], inputArgs[5], inputArgs[6], inputArgs[7],
                               inputArgs[8], inputArgs[9], inputArgs[10], inputArgs[11],
                               inputArgs[12], inputArgs[13], inputArgs[14], inputArgs[15],
                               inputArgs[16], inputArgs[17], inputArgs[18], inputArgs[19]);
    }
  }
  catch (...)
  {
    logErrorOnPythonInterpreter(pythonData->protoOutput,
                                "Error while running the Python function",
                                pythonData->pipefd[1]);
    return -1;
  }

  storePythonSolution(pythonData->protoOutput, result, pythonData->outputDescriptor);

  pythonData->protoOutput->set_interpreter_has_failed(false);
  pythonData->protoOutput->set_run_model_msec(runModelTimer.getWallClockTimeMsec());

  // Send the data out to the parent process
  pythonData->protoOutput->SerializeToFileDescriptor(pythonData->pipefd[1]);

  return 0;
}  // callPythonInterpreter

}  // namespace pythonengineutils
}  // namespace toolbox
}  // namespace optilab
