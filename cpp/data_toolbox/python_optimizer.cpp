#include "data_toolbox/python_optimizer.hpp"

#include <cassert>
#include <stdexcept>  // for std::invalid_argument
#include <utility>    // for std::move

#include <spdlog/spdlog.h>

namespace optilab {
namespace toolbox {

PythonOptimizer::PythonOptimizer(const std::string& engineName, dataengine::DataResult::SPtr result,
                                 MetricsRegister::SPtr metricsRegister)
: BaseClass(engineName),
  pResult(result),
  pProcessor(nullptr),
  pMetricsRegister(metricsRegister)
{
  // Initialize the engine,
  // e.g., the internal pipeline
  BaseClass::init();

  if (!pResult)
  {
    throw std::invalid_argument("PythonOptimizer - empty pointer to result");
  }

  if (!pMetricsRegister)
  {
    throw std::invalid_argument("PythonOptimizer - empty pointer to the metrics register");
  }
}

void PythonOptimizer::loadEnvironment(DataInstance::UPtr instance)
{
  // Load the environment into the optimizer
  assert(pProcessor);
  pProcessor->loadEnvironmentAndCreateSolver(std::move(instance));
}  // loadEnvironment

void PythonOptimizer::runOptimizer(DataInstance::UPtr instance)
{
  if (!isActive())
  {
    const std::string errMsg = "PythonOptimizer - runOptimizer: optimizer is not active";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  Work::SPtr work = std::make_shared<Work>(dataengine::DataWork::WorkType::kRunOptimizer,
                                           pMetricsRegister);
  if (instance)
  {
    work->dataInstance = std::move(instance);
  }
  pushTask(work);
}  // runOptimizer

bool PythonOptimizer::interruptOptimizer()
{
  assert(pProcessor);
  return pProcessor->interruptSolver();
}  // interruptOptimizer

ProcessorPipeline::SPtr PythonOptimizer::buildPipeline(int)
{
  ProcessorPipeline::SPtr pipeline = std::make_shared<ProcessorPipeline>();
  pProcessor = std::make_shared<PythonProcessor>(getEngineName(), pResult);
  pipeline->pushBackProcessor(pProcessor);
  return pipeline;
}  // buildPipeline

}  // namespace toolbox
}  // namespace optilab
