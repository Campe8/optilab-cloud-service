//
// Copyright OptiLab 2020. All rights reserved.
//
// Engine class for running custom Python code.
//

#pragma once

#include "data_toolbox/data_engine.hpp"

#include <atomic>
#include <cstdint>  // uint64_t
#include <memory>   // for std::shared_ptr
#include <string>

#include "data_structure/metrics/metrics_register.hpp"
#include "data_toolbox/data_instance.hpp"
#include "data_toolbox/data_utilities.hpp"
#include "data_toolbox/python_optimizer.hpp"
#include "engine/engine_callback_handler.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace toolbox {

class SYS_EXPORT_CLASS PythonEngine : public DataEngine {
 public:
  using SPtr = std::shared_ptr<PythonEngine>;

 public:
  PythonEngine(const std::string& engineId, const EngineCallbackHandler::SPtr& handler);

  ~PythonEngine();

  /// Registers the given instance/environment the Python engine should run on.
  /// @throw std::runtime_error if this model is called while the engine is running
  void registerInstance(DataInstance::UPtr instance) override;

  /// Notifies the engine on a given event
  void notifyEngine(const dataengine::DataEvent& event) override;

  /// Wait for the current jobs in the queue to finish.
  /// @note if "timeoutSec" is -1, waits for all jobs in the queue to complete
  void engineWait(int timeoutMsec) override;

  /// Shuts down the engine
  void turnDown() override;

 private:
  /// Engine identifier
  std::string pEngineId;

  /// Handler for callbacks to send back results to caller methods
  EngineCallbackHandler::SPtr pCallbackHandler;

  /// Pointer to the result instance collecting result from the optimizer
  dataengine::PythonFcnResult::SPtr pResult;

  /// Asynchronous optimizer
  PythonOptimizer::SPtr pOptimizer;

  /// Latency in msec. to create this engine
  std::atomic<uint64_t> pEngineCreationTimeMsec{0};

  /// Latency in msec. caused by loading an environment into the optimizer
  std::atomic<uint64_t> pLoadEnvironmentLatencyMsec{0};

  /// Latency in msec. caused by running the optimizer
  std::atomic<uint64_t> pOptimizerLatencyMsec{0};

  /// Register for metrics
  MetricsRegister::SPtr pMetricsRegister;

  /// Flag indicating whether this engine already started running
  std::atomic<bool> pActiveEngine{false};

  // Builds the instance of a GA optimizer running on a GA processor
  void buildOptimizer();

  /// Runs the registered environment
  void processRunEnvironmentEvent(DataInstance::UPtr instance);

  /// Sends back using the handler all the solutions collected so far
  void processCollectSolutionsEvent();

  /// Interrupts the current computation, if any, and return
  void processInterruptEngineEvent();

  /// Collects metrics and returns correspondent register
  void collectMetrics();
};

}  // namespace toolbox
}  // namespace optilab
