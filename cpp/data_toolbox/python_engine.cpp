#include "data_toolbox/python_engine.hpp"

#include <algorithm>  // for std::max
#include <memory>     // for std::make_shared
#include <sstream>
#include <stdexcept>  // for std::runtime_error
#include <utility>
#include <vector>

#include <spdlog/spdlog.h>

#include "data_structure/json/json.hpp"
#include "engine/engine_constants.hpp"
#include "engine/engine_utils.hpp"
#include "utilities/timer.hpp"

namespace {
/// Default Number of threads to be used in the pool
constexpr int kPoolNumThreads{1};

/// Waiting value for an idle processor
constexpr int kWaitOnIdleProcessor { -1 };
}  // namespace

namespace optilab {
namespace toolbox {

PythonEngine::PythonEngine(const std::string& engineId, const EngineCallbackHandler::SPtr& handler)
: pEngineId(engineId),
  pCallbackHandler(handler)

{
  if (engineId.empty())
  {
    throw std::invalid_argument("PythonEngine - empty engine identifier");
  }

  if (!handler)
  {
    throw std::invalid_argument("PythonEngine - empty engine callback handler");
  }

  timer::Timer timer;

  pResult = std::make_shared<dataengine::PythonFcnResult>();

  pMetricsRegister = std::make_shared<MetricsRegister>();

  // Builds the optimizer running ORTools CP models
  buildOptimizer();
  assert(pOptimizer);

  // Start  the asynchronous thread in the optimizer
  pOptimizer->startUp();

  pActiveEngine = true;

  pEngineCreationTimeMsec = timer.getWallClockTimeMsec();
}

PythonEngine::~PythonEngine()
{
  try {
    turnDown();
  }
  catch(...)
  {
    // Do not throw in destructor
    spdlog::warn("PythonEngine - thrown in destructor");
  }
}

void PythonEngine::turnDown()
{
  if (pActiveEngine)
  {
    pActiveEngine = false;
    pOptimizer->tearDown();
    pOptimizer.reset();
  }
}  // turnDown

void PythonEngine::buildOptimizer()
{
  pOptimizer = std::make_shared<PythonOptimizer>(pEngineId, pResult, pMetricsRegister);
}  // buildOptimizer

void PythonEngine::registerInstance(DataInstance::UPtr instance)
{
  if (instance == nullptr)
  {
    std::ostringstream ss;
    ss << "PythonEngine - registerEnvironment: "
        "registering an empty environment for engine " << pEngineId;
    throw std::runtime_error(ss.str());
  }

  if (!pActiveEngine)
  {
    const std::string errMsg = "PythonEngine - registerEnvironment: "
        "trying to register a model on an inactive engine " + pEngineId;
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  // Start timer for loading the model
  timer::Timer timer;

  // Load the environment into the optimizer
  try
  {
    pOptimizer->loadEnvironment(std::move(instance));
  }
  catch(std::exception& ex)
  {
    const std::string errMsg = "PythonEngine - registerEnvironment: "
        "error while loading the environment on engine " + pEngineId +
        " " + std::string(ex.what());
    spdlog::error(errMsg);
    throw;
  }
  catch(...)
  {
    const std::string errMsg = "PythonEngine - registerEnvironment: "
        "undefined error while loading the environment on engine " + pEngineId;
    spdlog::error(errMsg);
    throw;
  }

  pLoadEnvironmentLatencyMsec = timer.getWallClockTimeMsec();

  // Reset solution
  pResult->clear();
}  // registerModel

void PythonEngine::engineWait(int timeoutMsec)
{
  if (pActiveEngine)
  {
    pOptimizer->waitOnTaskCompletion(timeoutMsec);
  }
}  // engineWait

void PythonEngine::notifyEngine(const dataengine::DataEvent& event)
{
  switch (event.getType()) {
    case dataengine::DataEvent::EventType::kRunEngine:
    {
      auto instance = event.getDataInstance();
      processRunEnvironmentEvent(std::move(instance));
      break;
    }
    case dataengine::DataEvent::EventType::kSolutions:
    {
      processCollectSolutionsEvent();
      break;
    }
    default:
    {
      assert(event.getType() == dataengine::DataEvent::EventType::kInterruptEngine);
      processInterruptEngineEvent();
      break;
    }
  }
}  // notifyEngine

void PythonEngine::processRunEnvironmentEvent(DataInstance::UPtr instance)
{
  // Return if the engine is not active
  if (!pActiveEngine)
  {
    spdlog::warn("GAEngine - processRunEnvironmentEvent: "
        "inactive engine, returning " + pEngineId);
    return;
  }

  timer::Timer timer;

  // Wait on previous tasks to complete.
  // For example, it waits on model registration
  engineWait(kWaitOnIdleProcessor);

  // Trigger the engine to run and return
  pOptimizer->runOptimizer(std::move(instance));

  // Latency for running the optimizer
  pOptimizerLatencyMsec = timer.getWallClockTimeMsec();
}  // processRunModelEvent

void PythonEngine::processInterruptEngineEvent()
{
  if (!pActiveEngine)
  {
    spdlog::warn("PythonEngine - processInterruptEngineEvent: "
        "inactive engine, returning " + pEngineId);
    return;
  }

  pOptimizer->interruptOptimizer();
}  // processKillEngineEvent

void PythonEngine::processCollectSolutionsEvent()
{
  // @note it is assumed that the processor
  // is not currently adding result into the solution

  // Callback method to send results back to the client
  pCallbackHandler->resultCallback(pEngineId, datautils::buildPythonFcnSolutionProto(pResult));

  // Callback to send the metrics back to the client
  collectMetrics();
  auto jsonMetrics = engineutils::createMetricsJson(pMetricsRegister);

  // Callback method to send metrics back to the client
  pCallbackHandler->metricsCallback(pEngineId, jsonMetrics->toString());

  // Completed run
  pCallbackHandler->processCompletionCallback(pEngineId);
}  // processRunModelEvent

void PythonEngine::collectMetrics()
{
  assert(pMetricsRegister);
  pMetricsRegister->setMetric(jsonmetrics::ENGINE_CREATION_TIME_MSEC, pEngineCreationTimeMsec);
  pMetricsRegister->setMetric(jsonmetrics::LOAD_MODEL_LATENCY_MSEC, pLoadEnvironmentLatencyMsec);
}  // collectMetrics

}  // namespace toolbox
}  // namespace optilab
