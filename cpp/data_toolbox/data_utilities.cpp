#include "data_toolbox/data_utilities.hpp"

#include <stdexcept>  // for std::runtime_error

namespace optilab {
namespace toolbox {

namespace datautils {

DataSolutionProto buildPythonFcnSolutionProto(dataengine::DataResult::SPtr res)
{
  if (!res)
  {
    throw std::runtime_error("buildPythonFcnSolutionProto - empty result");
  }

  DataSolutionProto sol;
  sol.CopyFrom(res->solutionProto);

  // Set the status
  switch (res->status)
  {
    case dataengine::DataResult::OptimizerStatus::OS_ERROR:
      sol.set_status(OptimizerSolutionStatusProto::OPT_SOLVER_FAIL);
      break;
    case dataengine::DataResult::OptimizerStatus::OS_TIME:
      sol.set_status(OptimizerSolutionStatusProto::OPT_SOLVER_SUCCESS);
      break;
    case dataengine::DataResult::OptimizerStatus::OS_UNKNOWN:
      sol.set_status(OptimizerSolutionStatusProto::OPT_SOLVER_UNKNOWN_STATUS);
      break;
    case dataengine::DataResult::OptimizerStatus::OS_OKAY:
      sol.set_status(OptimizerSolutionStatusProto::OPT_SOLVER_SUCCESS);
      break;
  }

  return sol;
}  // buildGASolutionProto

}  // namespace datautils

}  // namespace toolbox
}  // namespace optilab
