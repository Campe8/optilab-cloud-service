//
// Copyright OptiLab 2020. All rights reserved.
//
// A Python optimizer is an asynchronous engine that
// holds a pipeline containing Python processors.
//

#pragma once

#include <memory>  // for std::shared_ptr
#include <string>

#include "data_structure/metrics/metrics_register.hpp"
#include "data_toolbox/data_instance.hpp"
#include "data_toolbox/python_processor.hpp"
#include "data_toolbox/data_utilities.hpp"
#include "engine/async_engine.hpp"
#include "engine/processor_pipeline.hpp"

#include "system/system_export_defs.hpp"

namespace optilab {
namespace toolbox {

class SYS_EXPORT_CLASS PythonOptimizer : public AsyncEngine<dataengine::PythonFcnDataWork::SPtr> {
 public:
  using SPtr = std::shared_ptr<PythonOptimizer>;

 public:
  PythonOptimizer(const std::string& engineName, dataengine::DataResult::SPtr result,
                  MetricsRegister::SPtr metricsRegister);

  /// Initializes this optimizer with the given model
  void loadEnvironment(DataInstance::UPtr instance);

  /// Runs this optimizer on the loaded model
  void runOptimizer(DataInstance::UPtr instance);

  /// Interrupts the solving processes on this optimizer.
  /// Returns true if the solver was successfully interrupted.
  /// Returns false otherwise
  bool interruptOptimizer();

 protected:
  ProcessorPipeline::SPtr buildPipeline(int pipeline = 0) override;

 private:
  using Work = dataengine::PythonFcnDataWork;
  using BaseClass = AsyncEngine<Work::SPtr>;

 private:
  /// Pointer to the results
  dataengine::DataResult::SPtr pResult;

  /// Pointer to the instance of the processor in the pipeline
  PythonProcessor::SPtr pProcessor;

  /// Pointer to the metrics register given to each work task processed
  /// by each processor
  MetricsRegister::SPtr pMetricsRegister;
};

}  // namespace toolbox
}  // namespace optilab
