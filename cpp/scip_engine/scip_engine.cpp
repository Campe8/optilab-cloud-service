// Base class
#include "scip_engine/scip_engine.hpp"

#include <spdlog/spdlog.h>

#include <stdexcept>  // for std::runtime_error

#include "data_structure/json/json.hpp"
#include "engine/engine_constants.hpp"
#include "engine/engine_utils.hpp"
#include "scip_engine/scip_utilities.hpp"
#include "utilities/timer.hpp"

namespace
{
/// Waiting value for an idle processor
constexpr int kWaitOnIdleProcessor{-1};
}  // namespace

namespace optilab
{
SCIPEngine::SCIPEngine(const std::string& engineId,
                       const EngineCallbackHandler::SPtr& handler)
    : pEngineId(engineId), pCallbackHandler(handler)
{
  if (pEngineId.empty())
  {
    throw std::invalid_argument("SCIPEngine - empty engine identifier");
  }

  if (!pCallbackHandler)
  {
    throw std::invalid_argument("SCIPEngine - empty engine callback handler");
  }

  timer::Timer timer;

  pMIPResult = std::make_shared<scipengine::SCIPMIPResult>();

  pMetricsRegister = std::make_shared<MetricsRegister>();

  // Builds the optimizer running ORTools CP models
  buildOptimizer();
  assert(pOptimizer);

  // Start  the asynchronous thread in the optimizer
  pOptimizer->startUp();

  pActiveEngine = true;

  pEngineCreationTimeMsec = timer.getWallClockTimeMsec();
}

SCIPEngine::~SCIPEngine()
{
  try
  {
    turnDown();
  }
  catch (...)
  {
    // Do not throw in destructor
    spdlog::warn("SCIPEngine - thrown in destructor");
  }
}

void SCIPEngine::turnDown()
{
  if (pActiveEngine)
  {
    pActiveEngine = false;
    pOptimizer->tearDown();
    pOptimizer.reset();
  }
}  // turnDown

void SCIPEngine::buildOptimizer()
{
  pOptimizer =
      std::make_shared<SCIPOptimizer>(pEngineId, pMIPResult, pMetricsRegister);
}  // buildOptimizer

void SCIPEngine::registerModel(const LinearModelSpecProto& model)
{
  if (!pActiveEngine)
  {
    const std::string errMsg =
        "SCIPEngine - registerModel: "
        "trying to register a model on an inactive engine " +
        pEngineId;
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  // Start timer for loading the model
  timer::Timer timer;

  // Load the model into the optimizer
  try
  {
    pOptimizer->loadModel(model);
  }
  catch (std::exception& ex)
  {
    const std::string errMsg =
        "SCIPEngine - registerModel: "
        "error while loading the model on engine " +
        pEngineId + " " + std::string(ex.what());
    spdlog::error(errMsg);
    throw;
  }
  catch (...)
  {
    const std::string errMsg =
        "SCIPEngine - registerModel: "
        "undefined error while loading the model on engine " +
        pEngineId;
    spdlog::error(errMsg);
    throw;
  }

  pLoadModelLatencyMsec = timer.getWallClockTimeMsec();

  // Reset solution
  std::dynamic_pointer_cast<scipengine::SCIPMIPResult>(pMIPResult)->clear();
  pMIPResult->modelName = model.model_id();
}  // registerModel

void SCIPEngine::registerModel(const std::string& model)
{
  if (model.empty())
  {
    std::ostringstream ss;
    ss << "SCIPEngine - registerModel: "
          "registering an empty model for engine "
       << pEngineId;
    throw std::runtime_error(ss.str());
  }

  if (!pActiveEngine)
  {
    const std::string errMsg =
        "SCIPEngine - registerModel: "
        "trying to register a model on an inactive engine " +
        pEngineId;
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  // Start timer for loading the model
  timer::Timer timer;

  // Load the model into the optimizer
  try
  {
    pOptimizer->loadModel(model);
  }
  catch (std::exception& ex)
  {
    const std::string errMsg =
        "SCIPEngine - registerModel: "
        "error while loading the model on engine " +
        pEngineId + " " + std::string(ex.what());
    spdlog::error(errMsg);
    throw;
  }
  catch (...)
  {
    const std::string errMsg =
        "SCIPEngine - registerModel: "
        "undefined error while loading the model on engine " +
        pEngineId;
    spdlog::error(errMsg);
    throw;
  }

  pLoadModelLatencyMsec = timer.getWallClockTimeMsec();

  // Reset solution
  std::dynamic_pointer_cast<scipengine::SCIPMIPResult>(pMIPResult)->clear();
}  // registerModel

void SCIPEngine::engineWait(int timeoutMsec)
{
  if (pActiveEngine)
  {
    pOptimizer->waitOnTaskCompletion(timeoutMsec);
  }
}  // engineWait

void SCIPEngine::notifyEngine(const scipengine::SCIPEvent& event)
{
  switch (event.getType())
  {
    case scipengine::SCIPEvent::EventType::kRunEngine:
    {
      processRunModelEvent();
      break;
    }
    case scipengine::SCIPEvent::EventType::kSolutions:
    {
      processCollectSolutionsEvent(event.getNumSolutions());
      break;
    }
    default:
    {
      assert(event.getType() ==
             scipengine::SCIPEvent::EventType::kInterruptEngine);
      processInterruptEngineEvent();
      break;
    }
  }
}  // notifyEngine

void SCIPEngine::processRunModelEvent()
{
  // Return if the engine is already active
  if (!pActiveEngine)
  {
    spdlog::warn(
        "SCIPEngine - processRunModelEvent: "
        "inactive engine, returning " +
        pEngineId);

    return;
  }

  timer::Timer timer;

  // Wait on previous tasks to complete.
  // For example, it waits on model registration
  engineWait(kWaitOnIdleProcessor);

  // Trigger the engine to run and return
  pOptimizer->runOptimizer();

  // Latency for running the optimizer
  pOptimizerLatencyMsec = timer.getWallClockTimeMsec();
}  // processRunModelEvent

void SCIPEngine::processInterruptEngineEvent()
{
  if (!pActiveEngine)
  {
    spdlog::warn(
        "SCIPEngine - processInterruptEngineEvent: "
        "inactive engine, returning " +
        pEngineId);
    return;
  }

  pOptimizer->interruptOptimizer();
}  // processKillEngineEvent

void SCIPEngine::processCollectSolutionsEvent(int numSolutions)
{
  (void)numSolutions;

  // @note it is assumed that the processor
  // is not currently adding result into the solution
  scipengine::SCIPMIPResult::SPtr res =
      std::dynamic_pointer_cast<scipengine::SCIPMIPResult>(pMIPResult);
  if (!res)
  {
    const std::string errMsg =
        "SCIPEngine - processCollectSolutionsEvent: "
        "empty pointer to result " +
        pEngineId;
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  // Callback method to send results back to the client.
  // @note this was previously done using JSON replies:
  //   auto solutionJson = sciputils::buildMIPSolutionJson(res);
  //   pCallbackHandler->resultCallback(pEngineId, solutionJson->toString());
  pCallbackHandler->resultCallback(pEngineId, sciputils::buildMIPSolutionProto(res));

  // Callback to send the metrics back to the client
  collectMetrics();
  auto jsonMetrics = engineutils::createMetricsJson(pMetricsRegister);

  // Callback method to send metrics back to the client
  pCallbackHandler->metricsCallback(pEngineId, jsonMetrics->toString());

  // Completed run
  pCallbackHandler->processCompletionCallback(pEngineId);
}  // processRunModelEvent

void SCIPEngine::collectMetrics()
{
  assert(pMetricsRegister);
  pMetricsRegister->setMetric(jsonmetrics::ENGINE_CREATION_TIME_MSEC,
                              pEngineCreationTimeMsec);
  pMetricsRegister->setMetric(jsonmetrics::LOAD_MODEL_LATENCY_MSEC,
                              pLoadModelLatencyMsec);
}  // collectMetrics

}  // namespace optilab
