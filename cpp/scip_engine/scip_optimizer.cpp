#include "scip_engine/scip_optimizer.hpp"

#include <cassert>
#include <stdexcept>  // for std::invalid_argument

#include <spdlog/spdlog.h>

namespace optilab {

SCIPOptimizer::SCIPOptimizer(const std::string& engineName,
                             scipengine::SCIPResult::SPtr result,
                             MetricsRegister::SPtr metricsRegister)
: BaseClass(engineName),
  pOptimizerModel(nullptr),
  pProcessor(nullptr),
  pResult(result),
  pMetricsRegister(metricsRegister)
{
  if (!pResult)
  {
    throw std::runtime_error("SCIPOptimizer - empty result");
  }

  if (!pMetricsRegister)
  {
    throw std::invalid_argument("SCIPOptimizer - empty pointer to the metrics register");
  }

  // Initialize the engine,
  // e.g., the internal pipeline
  BaseClass::init();
}

void SCIPOptimizer::loadModel(const LinearModelSpecProto& model)
{
  // Create the processor loading the model from the protobuf message
  pProcessor->loadModelAndCreateSolver(model);
}  // loadModel

void SCIPOptimizer::loadModel(const std::string& jsonModel)
{
  // Load the model into the optimizer
  assert(pProcessor);

  // Load the model from the given JSON
  loadModelImpl(jsonModel);
  assert(pOptimizerModel);
  pProcessor->loadModelAndCreateSolver(pOptimizerModel);
}  // loadModel

void SCIPOptimizer::loadModelImpl(const std::string& jsonModel)
{
  if (jsonModel.empty())
  {
    throw std::invalid_argument("SCIPOptimizer - loadModelImpl: empty JSON model");
  }

  pOptimizerModel = std::make_shared<Model>();
  pOptimizerModel->loadModelFromJson(jsonModel);
}  // loadModelImpl

void SCIPOptimizer::runOptimizer()
{
  if (!isActive())
  {
    const std::string errMsg = "SCIPOptimizer - runOptimizer: optimizer is not active";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  Work::SPtr work = std::make_shared<Work>(scipengine::SCIPWork::WorkType::kRunOptimizer,
                                           pMetricsRegister);
  pushTask(work);
}  // runOptimizer

bool SCIPOptimizer::interruptOptimizer()
{
  assert(pProcessor);
  return pProcessor->interruptSolver();
}  // interruptOptimizer

ProcessorPipeline::SPtr SCIPOptimizer::buildPipeline(int)
{
  ProcessorPipeline::SPtr pipeline = std::make_shared<ProcessorPipeline>();
  pProcessor = std::make_shared<SCIPProcessor>(pResult);
  pipeline->pushBackProcessor(pProcessor);
  return pipeline;
}  // buildPipeline

}  // namespace optilab
