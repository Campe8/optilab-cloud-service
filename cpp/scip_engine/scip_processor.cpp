#include "scip_engine/scip_processor.hpp"

#include <spdlog/spdlog.h>

#include <cassert>
#include <exception>
#include <functional>  // for std::bind
#include <stdexcept>   // for std::runtime_error

#include "data_structure/metrics/metrics_register.hpp"
#include "engine/engine_constants.hpp"
#include "utilities/timer.hpp"

namespace optilab
{
SCIPProcessor::SCIPProcessor(scipengine::SCIPResult::SPtr result)
    : BaseProcessor("SCIPProcessor")
{
  pMIPResult = std::dynamic_pointer_cast<scipengine::SCIPMIPResult>(result);
  if (!pMIPResult)
  {
    throw std::runtime_error("SCIPProcessor - empty pointer to result");
  }
}

void SCIPProcessor::loadModelAndCreateSolver(const LinearModelSpecProto& model)
{
  // Create a new MIP model from the given input model
  pMIPModel = std::make_shared<SCIPMIPModel>(model);

  try
  {
    pMIPModel->initializeModel(model);
  }
  catch (const std::exception& ex)
  {
    const std::string errMsg =
        "SCIPProcessor - loadModelAndCreateSolver: "
        "error parsing and initializing the protobuf model " +
        std::string(ex.what());
    spdlog::error(errMsg);
    throw;
  }
  catch (...)
  {
    const std::string errMsg =
        "SCIPProcessor - loadModelAndCreateSolver: "
        "error parsing and initializing the protobuf model";
    spdlog::error(errMsg);
    throw;
  }

  // Build a MIP solver
  buildSolver();
}  // loadModelAndCreateSolver

void SCIPProcessor::loadModelAndCreateSolver(const Model::SPtr& model)
{
  if (!model)
  {
    throw std::runtime_error(
        "SCIPProcessor - loadModelAndCreateSolver: empty model");
  }

  // Create a new MIP model from the given input model
  pMIPModel = std::make_shared<SCIPMIPModel>(model);

  // Parse and initialize the model
  try
  {
    pMIPModel->initializeModel();
  }
  catch (const std::exception& ex)
  {
    const std::string errMsg =
        "SCIPProcessor - loadModelAndCreateSolver: "
        "error parsing and initializing the model " +
        std::string(ex.what());
    spdlog::error(errMsg);
    throw;
  }
  catch (...)
  {
    const std::string errMsg =
        "SCIPProcessor - loadModelAndCreateSolver: "
        "error parsing and initializing the model";
    spdlog::error(errMsg);
    throw;
  }

  // Build a MIP solver
  buildSolver();
}  // loadModelAndCreateSolver

void SCIPProcessor::buildSolver()
{
  assert(pMIPModel);

  try
  {
    pMIPSolver = std::make_shared<SCIPSolver>(pMIPModel);
  }
  catch (const std::exception& ex)
  {
    const std::string errMsg =
        "SCIPProcessor - loadModelAndCreateSolver: "
        "cannot build the MIP solver " +
        std::string(ex.what());
    spdlog::error(errMsg);
    throw;
  }
  catch (...)
  {
    const std::string errMsg =
        "SCIPProcessor - loadModelAndCreateSolver: "
        "undefined error in building the MIP solver";
    spdlog::error(errMsg);
    throw;
  }
}  // buildSolver

bool SCIPProcessor::interruptSolver()
{
  if (!pMIPSolver)
  {
    return false;
  }

  pMIPSolver->interruptSearchProcess();
  return true;
}  // interruptSolver

void SCIPProcessor::processWork(Work work)
{
  if (work->workType == scipengine::SCIPWork::kRunOptimizer)
  {
    runSolver(work);
  }
  else
  {
    spdlog::warn("SCIPProcessor - processWork: unrecognized work type");
  }
}  // processWork

void SCIPProcessor::runSolver(Work& work)
{
  timer::Timer timer;

  // Initialize the search
  pMIPSolver->initSearch();

  // Run the solver
  pMIPSolver->solve();

  const auto wallclockMsec = timer.getWallClockTimeMsec();
  work->metricsRegister->setMetric(jsonmetrics::OPTIMIZER_LATENCY_MSEC,
                                   wallclockMsec);

  // Store the solution found
  storeSolution();

  // Cleanup the search
  pMIPSolver->cleanupSearch();
}  // runSolver

void SCIPProcessor::storeSolution()
{
  // Store solution w.r.t. to the optimization model format in input
  const auto modelFormat = pMIPModel->getModelInputFormat();
  switch (modelFormat)
  {
    case SolverModel::ModelInputFormat::INPUT_FORMAT_MPS:
      storeMPSSolution();
      break;
    case SolverModel::ModelInputFormat::INPUT_FORMAT_OPTILAB:
    case SolverModel::ModelInputFormat::INPUT_FORMAT_PROTOBUF:
      storeOptiLabSolution();
      break;
    default:
      throw std::runtime_error("SCIPProcessor - unrecognized model format");
  }
}  // storeSolution

void SCIPProcessor::storeMPSSolution()
{
  // Critical section
  LockGuard lock(pResultMutex);

  scipengine::SCIPMIPResult::Solution& solution = pMIPResult->solution.second;
  scipengine::SCIPMIPResult::ObjectiveValues& objVals =
      pMIPResult->solution.first;

  auto solver = pMIPSolver->getSolver();
  assert(solver);

  // Set variables
  for (const auto& var : solver->getVariableList())
  {
    solution[var->getName()].push_back(
        {var->getSolutionValue(), var->getSolutionValue()});
  }

  // Set objective
  objVals.push_back(solver->getObjectivePtr()->getValue());

  // Set the result status
  setResultStatus();
}  // storeMPSSolution

void SCIPProcessor::storeOptiLabSolution()
{
  // Critical section
  LockGuard lock(pResultMutex);

  scipengine::SCIPMIPResult::Solution& solution = pMIPResult->solution.second;
  scipengine::SCIPMIPResult::ObjectiveValues& objVals =
      pMIPResult->solution.first;

  // Set variables
  for (const auto& varPair : pMIPModel->getVariablesMap())
  {
    const auto& varName = varPair.first;
    if (solution.find(varName) != solution.end())
    {
      throw std::runtime_error(
          std::string("SCIPProcessor: variable assignment ") +
          std::string("already registered for variable ") + varName);
    }

    SCIPMIPVariable::SPtr var =
        std::dynamic_pointer_cast<SCIPMIPVariable>(varPair.second);
    if (!var)
    {
      const std::string errMsg =
          "SCIPProcessor - storeOptiLabSolution: "
          "invalid SCIP variable cast";
      spdlog::error(errMsg);
      throw std::runtime_error(errMsg);
    }

    if (var->isScalar())
    {
      const auto scipVar = var->getVar();
      solution[varName].push_back(
          {scipVar->getSolutionValue(), scipVar->getSolutionValue()});
    }
    else
    {
      auto& solList = solution[varName];
      solList.reserve((var->getVarList()).size());
      for (const auto& subvar : var->getVarList())
      {
        solList.push_back(
            {subvar->getSolutionValue(), subvar->getSolutionValue()});
      }
    }
  }

  // Set objectives
  for (const auto& mipObj : pMIPModel->getObjectiveList())
  {
    SCIPMIPObjective::SPtr obj =
        std::dynamic_pointer_cast<SCIPMIPObjective>(mipObj);
    if (!obj)
    {
      const std::string errMsg =
          "SCIPProcessor - storeOptiLabSolution: "
          "invalid SCIP objective cast";
      spdlog::error(errMsg);
      throw std::runtime_error(errMsg);
    }

    objVals.push_back(obj->getObj()->getValue());
  }

  // Set the result status
  setResultStatus();
}  // storeOptiLabSolution

void SCIPProcessor::setResultStatus()
{
  switch (pMIPSolver->getResultStatus())
  {
    case ortoolsengine::ORToolsMIPResult::ResultStatus::NOT_SOLVED:
      pMIPResult->resultStatus =
          scipengine::SCIPMIPResult::ResultStatus::NOT_SOLVED;
      break;
    case ortoolsengine::ORToolsMIPResult::ResultStatus::OPTIMUM:
      pMIPResult->resultStatus =
          scipengine::SCIPMIPResult::ResultStatus::OPTIMUM;
      break;
    case ortoolsengine::ORToolsMIPResult::ResultStatus::FEASIBLE:
      pMIPResult->resultStatus =
          scipengine::SCIPMIPResult::ResultStatus::FEASIBLE;
      break;
    case ortoolsengine::ORToolsMIPResult::ResultStatus::INFEASIBLE:
      pMIPResult->resultStatus =
          scipengine::SCIPMIPResult::ResultStatus::INFEASIBLE;
      break;
    case ortoolsengine::ORToolsMIPResult::ResultStatus::UNBOUNDED:
      pMIPResult->resultStatus =
          scipengine::SCIPMIPResult::ResultStatus::UNBOUNDED;
      break;
    default:
      pMIPResult->resultStatus =
          scipengine::SCIPMIPResult::ResultStatus::NOT_SOLVED;
      break;
  }
}  // setResultStatus

}  // namespace optilab
