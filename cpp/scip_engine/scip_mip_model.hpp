//
// Copyright OptiLab 2019. All rights reserved.
//
// Class encapsulating an OR-Tools MIP model.
//

#pragma once

#include <cassert>
#include <memory>  // for std::shared_ptr
#include <vector>

#include "meta_bb/linear_solver.hpp"
#include "model/model.hpp"
#include "model/model_object_constraint.hpp"
#include "model/model_object_objective.hpp"
#include "model/model_object_variable.hpp"
#include "optilab_protobuf/linear_model.pb.h"
#include "solver/mip_constraint.hpp"
#include "solver/mip_model.hpp"
#include "solver/mip_objective.hpp"
#include "solver/mip_variable.hpp"
#include "system/system_export_defs.hpp"
#include "utilities/variables.hpp"

namespace optilab
{
class SYS_EXPORT_STRUCT SCIPMIPVariable : public MIPVariable
{
 public:
  using SPtr = std::shared_ptr<SCIPMIPVariable>;

 public:
  explicit SCIPMIPVariable(metabb::MPVariable::SPtr var)
      : MIPVariable(var->getName())
  {
    pVarList.push_back(var);
  }

  SCIPMIPVariable(const std::string& name,
                  const std::vector<metabb::MPVariable::SPtr> varList)
      : MIPVariable(name, static_cast<int>(varList.size())), pVarList(varList)
  {
    assert(!pVarList.empty());
  }

  SCIPMIPVariable(const std::string& name,
                  const std::vector<metabb::MPVariable::SPtr> varList,
                  const std::vector<int>& dims)
      : MIPVariable(name, dims), pVarList(varList)
  {
    assert(!pVarList.empty());
  }

  ~SCIPMIPVariable() = default;

  inline metabb::MPVariable::SPtr getVar() const noexcept
  {
    return pVarList[0];
  }

  inline const std::vector<metabb::MPVariable::SPtr>& getVarList() const
      noexcept
  {
    return pVarList;
  }

 protected:
  MIPVariable::SPtr getVariableFromIndex(
      const std::string& varName, const std::vector<int>& subscr) override
  {
    const int idx = utilsvariables::getFlattenedIndex(getVarDims(), subscr);
    auto subVar = pVarList.at(idx);
    auto var = std::make_shared<SCIPMIPVariable>(subVar);
    var->setObjectiveCoeff(this->getObjectiveCoeff());
    return var;
  }

 private:
  /// Pointer to the variable
  std::vector<metabb::MPVariable::SPtr> pVarList;
};

class SYS_EXPORT_STRUCT SCIPMIPConstraint : public MIPConstraint
{
 public:
  using SPtr = std::shared_ptr<SCIPMIPConstraint>;

 public:
  explicit SCIPMIPConstraint(metabb::MPConstraint::SPtr con) : pCon(con) {}
  ~SCIPMIPConstraint() = default;

  inline metabb::MPConstraint::SPtr getVar() const noexcept { return pCon; }

 private:
  /// Pointer to the constraint
  metabb::MPConstraint::SPtr pCon;
};

class SYS_EXPORT_STRUCT SCIPMIPObjective : public MIPObjective
{
 public:
  using SPtr = std::shared_ptr<SCIPMIPObjective>;

 public:
  /// Constructor: creates a ne SCIP MIP objective wrapper around the given
  /// MPObjective.
  /// @note no ownership is taken by this object on the given object
  explicit SCIPMIPObjective(metabb::MPObjective* obj) : pObj(obj) {}
  ~SCIPMIPObjective() = default;

  inline metabb::MPObjective* getObj() const noexcept { return pObj; }

 private:
  /// Pointer to the objective
  metabb::MPObjective* pObj;
};

class SYS_EXPORT_CLASS SCIPMIPModel : public MIPModel
{
 public:
  using SPtr = std::shared_ptr<SCIPMIPModel>;

 public:
  /// Constructor creates a new MIP model run by SCIP MIP engine.
  /// The model is build on the given input model.
  /// @note throws std::invalid_argument on empty input arguments.
  explicit SCIPMIPModel(const Model::SPtr& model);

  /// Constructor creates a new MIP model run by SCIP MIP engine.
  /// The model is build on the given input protobuf linear model
  explicit SCIPMIPModel(const LinearModelSpecProto& model);

  /// Returns the "environment" this model is in,
  /// i.e., the "state" of the model where all model components belong to
  inline std::shared_ptr<metabb::MPSolver> getModelEnvironment() const
  {
    return pSolver;
  }

 protected:
  /// Initialize a legacy model.
  /// @note to get information about legacy models, use the following:
  /// - "getModelInputFormat(...)": to get the model format, e.g., MPS;
  /// - "getLegacyModelData(...)": to get the verbatim copy of the data;
  /// - "getLegacyModelDescriptor(...)" to get the verbatim copy of the model.
  void initializeWithLegacyModelFormat() override;

  /// Creates a MIPVariable (package-specific) given a ModelObjectVariable
  /// instance
  MIPVariable::SPtr createVariable(
      const ModelObjectVariable::SPtr& objVariable) override;

  /// Creates a MIPConstraint (package-specific) given a ModelObjectConstraint
  /// instance and the map of all the variables in the model
  MIPConstraint::SPtr createConstraint(
      const ModelObjectConstraint::SPtr& objConstraint,
      const VarMap& varMap) override;

  /// Creates a MIPObjective (package-specific) given a ModelObjectObjective
  /// instance and the map of all the variables in the model
  MIPObjective::SPtr createObjective(
      const ModelObjectObjective::SPtr& objObjective,
      const VarMap& varMap) override;

  /// Creates  MIPVariable (package-specific) given a proto variable description
  MIPVariable::SPtr createVariableFromProto(
      const OptVariableProto& protoVar) override;

  /// Creates  MIPConstraint (package-specific) given a proto constraint
  /// description
  MIPConstraint::SPtr createConstraintFromProto(
      const OptConstraintProto& protoCon, const VarMap& varMap) override;

  /// Creates a MIPObjective (package-specific) given a the protobuf model
  /// and the map of all the variables in the model
  MIPObjective::SPtr createObjectiveFromProto(const OptModelProto& model,
                                              const VarMap& varMap) override;

 private:
  using SolverPtr = std::shared_ptr<metabb::MPSolver>;

 private:
  /// The solver running this model
  SolverPtr pSolver;

  /// Flag indicating whether or not this model is infeasible
  bool pInfeasible{false};

  /// Loads an mps model into the solver
  void initializeSCIPMPSModel();
};

}  // namespace optilab
