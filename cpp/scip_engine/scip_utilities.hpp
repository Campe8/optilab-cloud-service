//
// Copyright OptiLab 2019-2020. All rights reserved.
//
// Utilities for OR-tools
//

#pragma once

#include <cstddef>  // for std::size_t
#include <memory>   // for std::shared_ptr
#include <string>
#include <vector>
#include <map>
#include <utility>  // for std::pair

#include "data_structure/json/json.hpp"
#include "data_structure/metrics/metrics_register.hpp"
#include "model/model_object_constraint.hpp"
#include "model/model_object_variable.hpp"
#include "optilab_protobuf/linear_model.pb.h"
#include "solver/cp_constraint.hpp"
#include "solver/cp_model.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

namespace scipengine {

struct SYS_EXPORT_STRUCT SCIPWork {
using SPtr = std::shared_ptr<SCIPWork>;

enum WorkType {
  /// Runs the optimizer on the loaded model
  kRunOptimizer,
  kWorkTypeUndef
};

SCIPWork(WorkType wtype, MetricsRegister::SPtr metReg)
: workType(wtype), metricsRegister(metReg)
{
}

WorkType workType;
MetricsRegister::SPtr metricsRegister;
};

class SYS_EXPORT_CLASS SCIPEvent {
 public:
  enum EventType {
    /// Starts the solving process
    kRunEngine,
    /// Kills any ongoing computation
    kInterruptEngine,
    /// Collects solutions
    kSolutions
  };

 public:
  explicit SCIPEvent(EventType aType)
  : pEventType(aType)
  {
  }

  inline EventType getType() const noexcept
  {
    return pEventType;
  }

  inline void setNumSolutions(int numSolutions) noexcept
  {
    pNumSolutions = numSolutions;
  }

  inline int getNumSolutions() const noexcept
  {
    return pNumSolutions;
  }

 private:
  /// Type of this event
  EventType pEventType;

  /// Number of solutions for "kSolutions" events
  int pNumSolutions{0};
};

struct SYS_EXPORT_STRUCT SCIPResult {
  using SPtr = std::shared_ptr<SCIPResult>;

  virtual ~SCIPResult() = default;

  /// Name of the model producing this result
  std::string modelName;
};

struct SYS_EXPORT_STRUCT SCIPMIPResult : public SCIPResult {
  LinearModelSolutionProto solutionProto;

  /// Result status
  enum ResultStatus {
    /// Solution is the global optimum
    OPTIMUM = 0,

    /// Feasible solution but not optimal
    FEASIBLE,

    /// Infeasible model, no result
    INFEASIBLE,

    /// Model proven to be unbounded
    UNBOUNDED,

    /// Model not yet solved
    NOT_SOLVED
  };

  using SPtr = std::shared_ptr<SCIPMIPResult>;

  /// Collection of solutions.
  /// @note an assignment is a list of pairs of values to a variable which may be scalar or
  /// a multi-dimensional variable
  using VarAssign = std::vector<std::pair<double, double>>;
  using Solution = std::map<std::string, VarAssign>;

  /// Vector of objective values, in the order declared in the model
  using ObjectiveValues = std::vector<double>;

  /// A MIP solution is a pair of objective values and variable assignments
  std::pair<ObjectiveValues, Solution> solution;

  /// Status of the solution
  ResultStatus resultStatus{ResultStatus::NOT_SOLVED};

  /// Clear the results
  void clear()
  {
    solutionProto.Clear();
    solutionProto.set_status(LinearModelSolutionStatusProto::SOLVER_UNKNOWN_STATUS);

    solution.first.clear();
    solution.second.clear();
    resultStatus = ResultStatus::NOT_SOLVED;
  }
};

}  // namespace ortoolsengine

namespace sciputils {
/// Builds a JSON object representing the MIP solution given as parameter
SYS_EXPORT_FCN JSON::SPtr buildMIPSolutionJson(scipengine::SCIPMIPResult::SPtr res);

/// Builds a protobuf object representing the MIP solution given as parameter
SYS_EXPORT_FCN LinearModelSolutionProto buildMIPSolutionProto(scipengine::SCIPMIPResult::SPtr res);
}  // namespace ortoolsutils

}  // namespace optilab
