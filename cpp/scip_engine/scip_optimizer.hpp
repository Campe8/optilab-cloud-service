//
// Copyright OptiLab 2019. All rights reserved.
//
// A SCIP optimizer is an asynchronous engine that
// holds a pipeline containing SCIP processors.
//

#pragma once

#include <memory>  // for std::shared_ptr
#include <string>

#include "data_structure/metrics/metrics_register.hpp"
#include "engine/async_engine.hpp"
#include "engine/processor_pipeline.hpp"
#include "optilab_protobuf/linear_model.pb.h"
#include "scip_engine/scip_processor.hpp"
#include "scip_engine/scip_utilities.hpp"
#include "model/model.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS SCIPOptimizer : public AsyncEngine<scipengine::SCIPWork::SPtr> {
 public:
  using SPtr = std::shared_ptr<SCIPOptimizer>;

 public:
  SCIPOptimizer(const std::string& engineName, scipengine::SCIPResult::SPtr result,
                MetricsRegister::SPtr metricsRegister);

  /// Initializes this optimizer with the given linear model represented
  /// as a protobuf message.
  /// In other words, it loads the model into the internal state and creates
  /// the optimizer processor that will solve it.
  /// @note this is a blocking call
  void loadModel(const LinearModelSpecProto& model);

  /// Initializes this optimizer with the given model.
  /// i.e., loads the model into the internal state and creates
  /// the optimizer processor that will solve it.
  /// @note this is a blocking call
  void loadModel(const std::string& jsonModel);

  /// Runs this optimizer on the loaded model
  void runOptimizer();

  /// Interrupts the solving processes on this optimizer.
  /// Returns true if the solver was successfully interrupted.
  /// Returns false otherwise
  bool interruptOptimizer();

 protected:
  ProcessorPipeline::SPtr buildPipeline(int pipeline = 0) override;

 private:
  using Work = scipengine::SCIPWork;
  using BaseClass = AsyncEngine<Work::SPtr>;

 private:
  /// Model solved by the optimizer
  Model::SPtr pOptimizerModel;

  /// Pointer to the instance of the processor in the pipeline
  SCIPProcessor::SPtr pProcessor;

  /// Pointer to the result instance
  scipengine::SCIPResult::SPtr pResult;

  /// Pointer to the metrics register given to each work task processed
  /// by each processor
  MetricsRegister::SPtr pMetricsRegister;

  /// Loads the model from JSON to Model instance
  void loadModelImpl(const std::string& jsonModel);
};

}  // namespace optilab
