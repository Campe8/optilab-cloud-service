//
// Copyright OptiLab 2019. All rights reserved.
//
// Base class for SCIP engines.
// SCIP engines are engines that solve MIP problems
// using the SCIP libraries.
//

#pragma once

#include <memory>  // for std::shared_ptr
#include <string>

#include "data_structure/metrics/metrics_register.hpp"
#include "engine/engine_callback_handler.hpp"
#include "optilab_protobuf/linear_model.pb.h"
#include "scip_engine/scip_optimizer.hpp"
#include "scip_engine/scip_utilities.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS SCIPEngine {
 public:
  using SPtr = std::shared_ptr<SCIPEngine>;

 public:
  SCIPEngine(const std::string& engineId, const EngineCallbackHandler::SPtr& handler);

  ~SCIPEngine();

  /// Registers the given (linear) model specified by the given protobuf message
  /// @note this method does not run the model.
  /// @throw std::runtime_error if this model is called while the engine is running
  void registerModel(const LinearModelSpecProto& model);

  /// Registers the given model.
  /// @note this method does not run the model.
  /// @throw std::runtime_error if this model is called while the engine is running
  void registerModel(const std::string& model);

  /// Notifies the engine on a given EngineEvent
  void notifyEngine(const scipengine::SCIPEvent& event);

  /// Wait for the current jobs in the queue to finish.
  /// @note if "timeoutSec" is -1, waits for all jobs in the queue to complete
  void engineWait(int timeoutMsec);

  /// Shuts down the engine
  void turnDown();

 private:
  /// Engine identifier
  std::string pEngineId;

  /// Handler for callbacks to send back results to caller methods
  EngineCallbackHandler::SPtr pCallbackHandler;

  /// Pointer to the result instance collecting result from the optimizer
  scipengine::SCIPResult::SPtr pMIPResult;

  /// Latency in msec. to create this engine
  std::atomic<uint64_t> pEngineCreationTimeMsec{0};

  /// Latency in msec. caused by loading a model into the optimizer
  std::atomic<uint64_t> pLoadModelLatencyMsec{0};

  /// Latency in msec. caused by running the optimizer
  std::atomic<uint64_t> pOptimizerLatencyMsec{0};

  /// Register for metrics
  MetricsRegister::SPtr pMetricsRegister;

  /// Flag indicating whether this engine already started running
  std::atomic<bool> pActiveEngine{false};

  /// Asynchronous optimizer
  SCIPOptimizer::SPtr pOptimizer;

  // Builds the instance of a ORTools optimizer with a MIP processor
  void buildOptimizer();

  /// Runs the registered model
  void processRunModelEvent();

  /// Sends back using the handler all the solutions collected so far
  void processCollectSolutionsEvent(int numSolutions);

  /// Interrupts the current computation, if any, and returns
  void processInterruptEngineEvent();

  /// Collects metrics in the internal metrics register
  void collectMetrics();
};

}  // namespace optilab
