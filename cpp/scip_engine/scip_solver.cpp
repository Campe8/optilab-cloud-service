#include "scip_engine/scip_solver.hpp"

#include <stdexcept>   // for std::invalid_argument

#include <spdlog/spdlog.h>

namespace optilab {

SCIPSolver::SCIPSolver(const SCIPMIPModel::SPtr& mipModel)
: pResultStatus(metabb::MPSolver::ResultStatus::NOT_SOLVED)
{
  if (!mipModel)
  {
    throw std::invalid_argument("SCIPSolver - empty pointer to a MIP model");
  }

  // Get the model environment which, in the ORTools package, corresponds to a Solver instance
  pSolver = mipModel->getModelEnvironment();
  if (!pSolver)
  {
    throw std::invalid_argument("SCIPSolver - empty environment");
  }
}

bool SCIPSolver::interruptSearchProcess()
{
  const bool isInterrupted = pSolver->interruptSolve();
  if (!isInterrupted)
  {
    // If the solver doesn't allow interruption, try to set the timeout to zero
    spdlog::warn("SCIPSolver - interruptSearchProcess: solver cannot be interrupted");
    pSolver->setTimeoutMsec(1);
  }

  return isInterrupted;
}  // interruptSearchProcess

void SCIPSolver::solve()
{
  pResultStatus = pSolver->solve();
}  // solve

double SCIPSolver::getObjectiveValue() const
{
  return pSolver->getObjectivePtr()->getValue();
}  // getObjectiveValue

ortoolsengine::ORToolsMIPResult::ResultStatus SCIPSolver::getResultStatus() const
{
  switch (pResultStatus)
  {
    case metabb::MPSolver::ResultStatus::NOT_SOLVED:
      return ortoolsengine::ORToolsMIPResult::ResultStatus::NOT_SOLVED;
    case metabb::MPSolver::ResultStatus::OPTIMAL:
      return ortoolsengine::ORToolsMIPResult::ResultStatus::OPTIMUM;
    case metabb::MPSolver::ResultStatus::FEASIBLE:
      return ortoolsengine::ORToolsMIPResult::ResultStatus::FEASIBLE;
    case metabb::MPSolver::ResultStatus::INFEASIBLE:
      return ortoolsengine::ORToolsMIPResult::ResultStatus::INFEASIBLE;
    case metabb::MPSolver::ResultStatus::UNBOUNDED:
      return ortoolsengine::ORToolsMIPResult::ResultStatus::UNBOUNDED;
    default:
      return ortoolsengine::ORToolsMIPResult::ResultStatus::NOT_SOLVED;
  }
}  // getResultStatus

}  // namespace optilab
