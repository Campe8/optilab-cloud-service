//
// Copyright OptiLab 2019. All rights reserved.
//
// Processor for SCIP MIP engines.
//

#pragma once

#include <memory>   // for std::shared_ptr
#include <string>
#include <map>
#include <utility>  // for std::pair
#include <vector>

#include "engine/optimizer_processor.hpp"
#include "model/model.hpp"
#include "optilab_protobuf/linear_model.pb.h"
#include "scip_engine/scip_mip_model.hpp"
#include "scip_engine/scip_solver.hpp"
#include "scip_engine/scip_utilities.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS SCIPProcessor :
public OptimizerProcessor<scipengine::SCIPWork::SPtr, scipengine::SCIPWork::SPtr> {
public:
  using SPtr = std::shared_ptr<SCIPProcessor>;

public:
  SCIPProcessor(scipengine::SCIPResult::SPtr result);

  /// Loads the protobuf linear model and creates a solver to run on it.
  /// @note throw std::invalid_argument on empty model
  void loadModelAndCreateSolver(const LinearModelSpecProto& model);

  /// Loads the model and creates a solver to run on it.
  /// @note throw std::invalid_argument on empty model
  void loadModelAndCreateSolver(const Model::SPtr& model);

  /// Interrupts the internal solving process (if running).
  /// The solver remains in a non-executable state until another model is loaded
  bool interruptSolver();

protected:
  using Work = scipengine::SCIPWork::SPtr;
  using BaseProcessor = OptimizerProcessor<Work, Work>;

  /// Lock type on the internal mutex
  using LockGuard = boost::lock_guard<boost::mutex>;

protected:
  void processWork(Work work) override;

private:
  /// Model to solve
  SCIPMIPModel::SPtr pMIPModel;

  /// ORTools MIPSolver used to solve instances on this engine processor
  SCIPSolver::SPtr pMIPSolver;

  /// Pointer to the downcast MIP result instance
  scipengine::SCIPMIPResult::SPtr pMIPResult;

  /// Mutex synch.ing the state of the result
  boost::mutex pResultMutex;

  /// Utility function: runs the solver
  void runSolver(Work& work);

  /// Utility function: store solution
  void storeSolution();

  /// Utility function: stores an MPS model format solution
  void storeMPSSolution();

  /// Utility function: stores a OptiLab model format solution
  void storeOptiLabSolution();

  /// Utility function: sets the result status of the solving process
  void setResultStatus();

  /// Utility function: builds the internal solver based on the
  /// initialized MIP model
  void buildSolver();
};

}  // namespace optilab
