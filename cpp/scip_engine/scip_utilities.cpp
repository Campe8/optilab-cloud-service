#include "scip_engine/scip_utilities.hpp"

#include <cassert>
#include <cstdint>    // for int64_t
#include <stdexcept>  // for std::runtime_error

#include <boost/any.hpp>
#include <sparsepp/spp.h>

#include "engine/engine_constants.hpp"
#include "engine/engine_utils.hpp"
#include "model/model_constants.hpp"
#include "model/model_object_constraint.hpp"
#include "model/model_object_intension_constraint.hpp"
#include "utilities/strings.hpp"
#include "utilities/variables.hpp"

namespace {
template<typename Assign>
optilab::JSONValueArray::SPtr createSolutionDomain(const optilab::JSON::SPtr& json,
                                                   const Assign& assign)
{
  auto domainJson = json->createArray();
  for (const auto& bounds : assign)
  {
    auto boundsJson = json->createArray();

    // Storing only one element is bounds are the same should help
    // saving some memory
    if (bounds.first == bounds.second)
    {
      auto lb = json->createValue(bounds.first);
      boundsJson->pushBack(lb);
    }
    else
    {
      auto lb = json->createValue(bounds.first);
      auto ub = json->createValue(bounds.second);
      boundsJson->pushBack(lb);
      boundsJson->pushBack(ub);
    }

    domainJson->pushBack(boundsJson);
  }

  return domainJson;
}  // createSolutionDomain

optilab::JSONValueArray::SPtr createSolution(
    const optilab::JSON::SPtr& json,
    const optilab::scipengine::SCIPMIPResult::Solution& solution)
{
  auto solutionJson = json->createArray();
  for (const auto& varMapIter : solution)
  {
    auto varAssign = json->createValue();

    // Set variable id
    auto varId = json->createValue(varMapIter.first);
    varAssign->add(optilab::jsonsolution::VAR_IDENTIFIER, varId);

    // Create domain bounds
    auto domainJson =
        createSolutionDomain<std::vector<std::pair<double, double>>>(
            json, varMapIter.second);
    varAssign->add(optilab::jsonsolution::VAR_DOMAIN_LIST, domainJson);

    // Push the current variable assignment in the solution list of variable assignments
    solutionJson->pushBack(varAssign);
  }

  return solutionJson;
}  // createSolution

optilab::JSONValueArray::SPtr createSolutionArray(
    const optilab::JSON::SPtr& json,
    const std::vector<optilab::scipengine::SCIPMIPResult::Solution> & solutionList,
    int numSolutions)
{
  auto solutionListJson = json->createArray();
  for (int solIdx = 0; solIdx < numSolutions; ++solIdx)
  {
    solutionListJson->pushBack(createSolution(json, solutionList[solIdx]));
  }

  return solutionListJson;
}  // createSolutionArray

optilab::JSONValueArray::SPtr createMIPSolutionArray(
    const optilab::JSON::SPtr& json,
    const optilab::scipengine::SCIPMIPResult::Solution& solution)
{
  auto solutionListJson = json->createArray();
  for (const auto& varMapIter : solution)
  {
    auto varAssign = json->createValue();

    // Set variable id
    auto varId = json->createValue(varMapIter.first);
    varAssign->add(optilab::jsonsolution::VAR_IDENTIFIER, varId);

    // Create domain bounds
    auto domainJson =
        createSolutionDomain<optilab::scipengine::SCIPMIPResult::VarAssign>(
            json, varMapIter.second);
    varAssign->add(optilab::jsonsolution::VAR_DOMAIN_LIST, domainJson);

    // Push the current variable assignment in the solution list of variable assignments
    solutionListJson->pushBack(varAssign);
  }

  return solutionListJson;
}  // createMIPSolutionArray

optilab::JSONValueArray::SPtr createObjectiveValuesArray(
    const optilab::JSON::SPtr& json,
    const optilab::scipengine::SCIPMIPResult::ObjectiveValues& objectiveValues)
{
  auto objectiveValuesListJson = json->createArray();
  for (int objValIdx = 0; objValIdx < objectiveValues.size(); ++objValIdx)
  {
    auto val = json->createValue(objectiveValues[objValIdx]);
    objectiveValuesListJson->pushBack(val);
  }

  return objectiveValuesListJson;
}  // createObjectiveValuesArray

}  // namespace

namespace optilab {
namespace sciputils {

JSON::SPtr buildMIPSolutionJson(scipengine::SCIPMIPResult::SPtr res)
{
  if (!res)
  {
    throw std::runtime_error("buildMIPSolutionJson - empty result");
  }

  if ((res->solution).first.empty() && (res->solution).second.empty())
  {
    throw std::runtime_error("buildMIPSolutionJson - empty solution");
  }

  JSON::SPtr json = std::make_shared<JSON>();

  // Add the model name
  auto modelId = json->createValue(res->modelName);
  json->add(jsonsolution::MODEL_ID, modelId);

  // Set the result status
  std::string resultStatusStr;
  switch (res->resultStatus)
  {
    case scipengine::SCIPMIPResult::ResultStatus::FEASIBLE:
      resultStatusStr = jsonmipsolution::RESULT_FEASIBLE;
      break;
    case scipengine::SCIPMIPResult::ResultStatus::INFEASIBLE:
      resultStatusStr = jsonmipsolution::RESULT_INFEASIBLE;
      break;
    case scipengine::SCIPMIPResult::ResultStatus::NOT_SOLVED:
      resultStatusStr = jsonmipsolution::RESULT_NOT_SOLVED;
      break;
    case scipengine::SCIPMIPResult::ResultStatus::OPTIMUM:
      resultStatusStr = jsonmipsolution::RESULT_OPTIMUM;
      break;
    default:
      assert(res->resultStatus == scipengine::SCIPMIPResult::ResultStatus::UNBOUNDED);
      resultStatusStr = jsonmipsolution::RESULT_UNBOUNDED;
      break;
  }

  auto resultStatusJson = json->createValue(resultStatusStr);
  json->add(jsonsolution::MODEL_STATUS, resultStatusJson);

  // Add objective values list
  const auto& objetiveValuesArray = createObjectiveValuesArray(json, (res->solution).first);
  json->add(jsonmipsolution::OBJECTIVE_VALUES_LIST, objetiveValuesArray);

  // Add solution
  const auto& solutionArray = createMIPSolutionArray(json, (res->solution).second);
  json->add(jsonsolution::SOLUTION_LIST, solutionArray);

  return json;
}  // buildMIPSolutionJson

LinearModelSolutionProto buildMIPSolutionProto(scipengine::SCIPMIPResult::SPtr res)
{
  if (!res)
  {
    throw std::runtime_error("buildMIPSolutionJson - empty result");
  }

  LinearModelSolutionProto sol;

  if ((res->solution).first.empty() && (res->solution).second.empty())
  {
    sol.set_status(LinearModelSolutionStatusProto::SOLVER_MODEL_INVALID);
    sol.set_status_str("SCIP: empty solution. Probably invalid model, contact Parallel support.");
    return sol;
  }

  // Set status
  switch (res->resultStatus)
  {
    case scipengine::SCIPMIPResult::ResultStatus::FEASIBLE:
      sol.set_status(LinearModelSolutionStatusProto::SOLVER_FEASIBLE);
      break;
    case scipengine::SCIPMIPResult::ResultStatus::INFEASIBLE:
      sol.set_status(LinearModelSolutionStatusProto::SOLVER_INFEASIBLE);
      break;
    case scipengine::SCIPMIPResult::ResultStatus::NOT_SOLVED:
      sol.set_status(LinearModelSolutionStatusProto::SOLVER_NOT_SOLVED);
      break;
    case scipengine::SCIPMIPResult::ResultStatus::OPTIMUM:
      sol.set_status(LinearModelSolutionStatusProto::SOLVER_OPTIMAL);
      break;
    default:
      assert(res->resultStatus == scipengine::SCIPMIPResult::ResultStatus::UNBOUNDED);
      sol.set_status(LinearModelSolutionStatusProto::SOLVER_UNBOUNDED);
      break;
  }

  // Objective value
  sol.set_objective_value((res->solution).first.at(0));

  // Variable assignment
  for (const auto& varSolution : (res->solution).second)
  {
    auto vassign = sol.add_variable_assign();
    vassign->set_var_name(varSolution.first);
    for (const auto& val : varSolution.second)
    {
      // Using only the first value since for MIP problems
      // the variables are all assigned
      vassign->add_var_value(val.first);
    }
  }

  return sol;
}  // buildMIPSolutionProto

}  // namespace sciputils
}  // namespace optilab
