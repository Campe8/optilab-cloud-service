#include "scip_engine/scip_mip_model.hpp"

#include <spdlog/spdlog.h>
#include <unistd.h>  // for write

#include <cstdio>     // for std::remove
#include <cstdlib>    // for std::mkstemp
#include <fstream>    // for std::ofstream
#include <stdexcept>  // for std::invalid_argument

#include "model/model_constants.hpp"
#include "model/model_object_mip_constraint.hpp"
#include "model/model_object_mip_objective.hpp"
#include "ortools/lp_data/mps_reader.h"
#include "utilities/file_sys.hpp"
#include "utilities/random_generators.hpp"

namespace
{
constexpr int kTempFileNameLength = 10;
}  // namespace

namespace optilab
{
SCIPMIPModel::SCIPMIPModel(const Model::SPtr& model) : MIPModel(model)
{
  const auto problemType = getProblemType();
  switch (problemType)
  {
    case ProblemType::PT_LP:
    case ProblemType::PT_IP:
    case ProblemType::PT_MIP:
      pSolver = std::make_shared<metabb::MPSolver>(
          model->getModelName(),
          metabb::MPSolver::OptimizationSolverPackageType::OSPT_SCIP);
      break;
    default:
      throw std::runtime_error(
          "ORToolsMIPModel - unrecognized optimization model problem type");
  }
}

SCIPMIPModel::SCIPMIPModel(const LinearModelSpecProto& model) : MIPModel(model)
{
  pSolver = std::make_shared<metabb::MPSolver>(
      model.model_id(),
      metabb::MPSolver::OptimizationSolverPackageType::OSPT_SCIP);
}

void SCIPMIPModel::initializeWithLegacyModelFormat()
{
  switch (getModelInputFormat())
  {
    case ModelInputFormat::INPUT_FORMAT_MPS:
    {
      initializeSCIPMPSModel();
      break;
    }
    default:
      throw std::runtime_error(
          "SCIPMIPModel - unrecognized legacy model format");
  }
}  // initializeWithLegacyModelFormat

void SCIPMIPModel::initializeSCIPMPSModel()
{
  std::string modelFileName;
  int fd;
  const auto useFileDescriptor = getModel()->useModelFileDescriptor();
  if (useFileDescriptor)
  {
    // modelFileName = utilsfilesys::getPathToLocation({getLegacyModelDescriptor()});
    modelFileName = getLegacyModelDescriptor();
    if (!utilsfilesys::doesFileExists(modelFileName))
    {
      const std::string errMsg =
          "SCIPMIPModel - file not found: " + modelFileName;
      spdlog::error(errMsg);
      throw std::runtime_error(errMsg);
    }
  }
  else
  {
    // @note this mode of loading an MPS is DEPRECATED
    // Create a temporary file to store the content of the mps model
    char templateFile[] = "/tmp/fileXXXXXX";
    fd = mkstemp(templateFile);
    if (fd == -1)
    {
      throw std::runtime_error(
          "SCIPMIPModel - cannot create temporary mps file");
    }

    // Write the string into the file
    modelFileName = std::string(templateFile);
    auto res = write(fd, getLegacyModelDescriptor().c_str(),
                     getLegacyModelDescriptor().size());
    if (res == -1)
    {
      throw std::runtime_error(
          "SCIPMIPModel - error in writing the temporary mps file");
    }
  }

  // Load the model into proto object from the file
  const int resLoad = pSolver->loadMPSModelFromFile(modelFileName);
  if (resLoad != 0)
  {
    if (resLoad == 2)
    {
      spdlog::warn("SCIPMIPModel: infeasible mps model: " + modelFileName);
      pInfeasible = true;
    }
  }

  if (!useFileDescriptor)
  {
    // Close and delete the file asap
    const int err = unlink(modelFileName.c_str());
    if (err == -1)
    {
      spdlog::warn("SCIPMIPModel: cannot unlink temporary mps file: " +
                   modelFileName);
    }
    close(fd);
  }

  // Check the status of the model
  if (pInfeasible)
  {
    return;
  }

  if (resLoad)
  {
    const std::string errMsg =
        "SCIPMIPModel - cannot load mps file, invalid model";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }
}  // initializeORToolsMPSModel

MIPVariable::SPtr SCIPMIPModel::createVariableFromProto(
    const OptVariableProto& protoVar)
{
  const std::string& name = protoVar.name();
  const bool isInt = protoVar.is_integer();
  const double objCoeff = protoVar.objective_coefficient();

  std::shared_ptr<SCIPMIPVariable> var = nullptr;
  if (isInt)
  {
    var = std::make_shared<SCIPMIPVariable>(pSolver->buildIntVar(
        protoVar.lower_bound(), protoVar.upper_bound(), name));
  }
  else
  {
    var = std::make_shared<SCIPMIPVariable>(pSolver->buildNumVar(
        protoVar.lower_bound(), protoVar.upper_bound(), name));
  }
  var->getVar()->setBranchingPriority(protoVar.branching_priority());
  var->setObjectiveCoeff(objCoeff);

  return var;
}  // createVariableFromProto

MIPVariable::SPtr SCIPMIPModel::createVariable(
    const ModelObjectVariable::SPtr& objVariable)
{
  if (!objVariable)
  {
    throw std::runtime_error(
        "ORToolsMIPModel - empty pointer to ModelObjectVariable");
  }

  const double lb = objVariable->getLowerBound();
  const double ub = objVariable->getUpperBound();
  const auto domType = objVariable->getDomainType();
  const std::string& varName = objVariable->getVariableName();
  bool isContinuous = domType == ModelObjectVariable::VarType::DOUBLE ||
                      domType == ModelObjectVariable::VarType::FLOAT;

  if (objVariable->isScalar())
  {
    if (domType == ModelObjectVariable::VarType::BOOL)
    {
      return std::make_shared<SCIPMIPVariable>(pSolver->buildBoolVar(varName));
    }
    else
    {
      auto var = pSolver->buildNumVar(lb, ub, varName);
      if (!isContinuous)
      {
        var->setInteger(true);
      }
      return std::shared_ptr<SCIPMIPVariable>(new SCIPMIPVariable(var));
    }
  }
  else
  {
    int size;
    std::vector<int> dimList;
    const int dim = static_cast<int>(objVariable->numDimensions());
    if (dim == 1)
    {
      size = objVariable->getDimension(0);
    }
    else
    {
      int size = 1;
      for (int d = 0; d < dim; ++d)
      {
        size *= objVariable->getDimension(d);
        dimList.push_back(objVariable->getDimension(d));
      }
    }

    if (domType == ModelObjectVariable::VarType::BOOL)
    {
      return dimList.empty()
                 ? std::make_shared<SCIPMIPVariable>(
                       varName, pSolver->buildBoolVarArray(size, varName))
                 : std::make_shared<SCIPMIPVariable>(
                       varName, pSolver->buildBoolVarArray(size, varName),
                       dimList);
    }
    else
    {
      if (isContinuous)
      {
        return dimList.empty()
                   ? std::make_shared<SCIPMIPVariable>(
                         varName,
                         pSolver->buildNumVarArray(size, lb, ub, varName))
                   : std::make_shared<SCIPMIPVariable>(
                         varName,
                         pSolver->buildNumVarArray(size, lb, ub, varName),
                         dimList);
      }
      else
      {
        return dimList.empty()
                   ? std::make_shared<SCIPMIPVariable>(
                         varName,
                         pSolver->buildIntVarArray(size, lb, ub, varName))
                   : std::make_shared<SCIPMIPVariable>(
                         varName,
                         pSolver->buildIntVarArray(size, lb, ub, varName),
                         dimList);
      }
    }
  }
}  // createVariable

MIPConstraint::SPtr SCIPMIPModel::createConstraint(
    const ModelObjectConstraint::SPtr& objConstraint, const VarMap& varMap)
{
  ModelObjectMIPConstraint::SPtr conObj =
      std::dynamic_pointer_cast<ModelObjectMIPConstraint>(objConstraint);
  if (!conObj)
  {
    throw std::invalid_argument(
        "SCIPMIPModel - invalid cast from ModelObjectConstraint to "
        "ModelObjectMIPConstraint");
  }

  auto con = pSolver->buildRowConstraint(conObj->getConstraintLowerBound(),
                                         conObj->getConstraintUpperBound(),
                                         conObj->getConstraintId());

  // For all variables in coeff list add the correspondent MIP variable to the
  // list of pairs <var, coef>
  for (const auto& varCoeff : *conObj)
  {
    auto it = varMap.find(varCoeff.first);
    if (it == varMap.end())
    {
      throw std::runtime_error(
          "SCIPMIPModel - createConstraint: "
          "cannot find MIP variable with id " +
          varCoeff.first);
    }

    auto var = std::dynamic_pointer_cast<SCIPMIPVariable>(it->second);
    if (!var)
    {
      throw std::runtime_error(
          "SCIPMIPModel - createConstraint: "
          "cannot convert MIP variable with id " +
          varCoeff.first);
    }
    con->setCoefficient(var->getVar(), varCoeff.second);
  }

  return std::make_shared<SCIPMIPConstraint>(con);
}  // createConstraint

MIPConstraint::SPtr SCIPMIPModel::createConstraintFromProto(
    const OptConstraintProto& protoCon, const VarMap& varMap)
{
  (void) varMap;
  auto con = pSolver->buildRowConstraint(
      protoCon.lower_bound(), protoCon.upper_bound(), protoCon.name());

  if (protoCon.var_index_size() != protoCon.coefficient_size())
  {
    throw std::runtime_error(
        "SCIPMIPModel - createConstraintFromProto: "
        "invalid size for variables and coefficients " +
        std::to_string(protoCon.var_index_size()) +
        " != " + std::to_string(protoCon.coefficient_size()));
  }

  const auto& varList = pSolver->getVariableList();
  for (int idx = 0; idx < protoCon.var_index_size(); ++idx)
  {
    const int vidx = protoCon.var_index(idx);
    if (vidx < 0 || vidx >= varList.size())
    {
      throw std::runtime_error(
          "SCIPMIPModel - createConstraintFromProto: "
          "invalid variable index " +
          std::to_string(vidx) + " (it should be in [0, " +
          std::to_string(varList.size() - 1) + "]");
    }
    con->setCoefficient(varList[vidx], protoCon.coefficient(idx));
  }
  con->setIsLazy(protoCon.is_lazy());

  return std::make_shared<SCIPMIPConstraint>(con);
}  // createConstraintFromProto

MIPObjective::SPtr SCIPMIPModel::createObjective(
    const ModelObjectObjective::SPtr& objObjective, const VarMap& varMap)
{
  ModelObjectMIPObjective::SPtr objectiveObj =
      std::dynamic_pointer_cast<ModelObjectMIPObjective>(objObjective);
  if (!objectiveObj)
  {
    throw std::invalid_argument(
        "SCIPMIPModel - createObjective: "
        "invalid cast from ModelObjectObjective to "
        "ModelObjectMIPObjective");
  }

  auto obj = pSolver->getObjectivePtr();
  for (const auto& varCoeff : *objectiveObj)
  {
    auto it = varMap.find(varCoeff.first);
    if (it == varMap.end())
    {
      throw std::runtime_error(
          "SCIPMIPModel - createObjective: "
          "cannot find MIP variable with id " +
          varCoeff.first);
    }

    auto var = std::dynamic_pointer_cast<SCIPMIPVariable>(it->second);
    if (!var)
    {
      throw std::runtime_error(
          "SCIPMIPModel - createObjective: "
          "cannot convert MIP variable with id " +
          varCoeff.first);
    }

    obj->setCoefficient(var->getVar(), varCoeff.second);
  }

  if (objectiveObj->getOptimizationDirection() ==
      ModelObjectMIPObjective::OptimizationDirection::MAXIMIZE)
  {
    obj->setMaximization();
  }
  else
  {
    obj->setMinimization();
  }

  return std::make_shared<SCIPMIPObjective>(obj);
}  // createObjective

MIPObjective::SPtr SCIPMIPModel::createObjectiveFromProto(
    const OptModelProto& model, const VarMap& varMap)
{
  auto obj = pSolver->getObjectivePtr();
  for (const auto& vname : getObjectiveVarList())
  {
    auto varPtr = varMap.at(vname);
    auto var = std::dynamic_pointer_cast<SCIPMIPVariable>(varPtr);
    if (!var)
    {
      throw std::runtime_error(
          "SCIPMIPModel - createObjectiveFromProto: "
          "cannot convert MIP variable with id " +
          vname);
    }
    obj->setCoefficient(var->getVar(), var->getObjectiveCoeff());
  }

  obj->setOffset(model.objective_offset());
  if (model.maximize())
  {
    obj->setMaximization();
  }
  else
  {
    obj->setMinimization();
  }

  return std::make_shared<SCIPMIPObjective>(obj);
}  // createObjectiveFromProto

}  // namespace optilab
