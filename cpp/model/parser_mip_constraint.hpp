//
// Copyright OptiLab 2019. All rights reserved.
//
// Class encapsulating a parser for a constraint model object.
//

#pragma once

#include "model/parser.hpp"

#include <string>

#include "data_structure/json/json.hpp"
#include "model/model_object.hpp"

namespace optilab {

class SYS_EXPORT_CLASS ParserMIPConstraint : public Parser {
 public:
  using SPtr = std::shared_ptr<ParserMIPConstraint>;

 public:
  ParserMIPConstraint();

  virtual ~ParserMIPConstraint() = default;

  ModelObject::SPtr parse(JSONValue::JsonObject& jsonObject) override;

 protected:
  ModelObject::SPtr parseDescription(const std::string& description) override;
};

}  // namespace optilab
