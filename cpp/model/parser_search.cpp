#include "model/parser_search.hpp"

#include <cstddef>    // for std::size_t
#include <cassert>
#include <stdexcept>  // for std::runtime_error
#include <vector>

#include <spdlog/spdlog.h>

#include "model/model_constants.hpp"
#include "model/model_object_search.hpp"
#include "model/parser_utilities.hpp"
#include "model/primitive_search_object.hpp"

namespace {

std::string getVariableSelectionStrategyFromJson(optilab::JSONValue::JsonObject& jsonObject)
{
  auto varStrategy = jsonObject.FindMember(optilab::jsonmodel::VARIABLE_SELECTION_STRATEGY);
  if (varStrategy == jsonObject.MemberEnd())
  {
    throw std::runtime_error("Missing variable selection strategy");
  }

  return varStrategy->value.GetString();
}  // getVariableSelectionStrategyFromJson

std::string getValueSelectionStrategyFromJson(optilab::JSONValue::JsonObject& jsonObject)
{
  auto varStrategy = jsonObject.FindMember(optilab::jsonmodel::VALUE_SELECTION_STRATEGY);
  if (varStrategy == jsonObject.MemberEnd())
  {
    throw std::runtime_error("Missing variable selection strategy");
  }

  return varStrategy->value.GetString();
}  // getValueSelectionStrategyFromJson

int getRandomSeedFromJson(optilab::JSONValue::JsonObject& jsonObject)
{
  auto rndSeed = jsonObject.FindMember(optilab::jsonmodel::RANDOM_SEED);
  if (rndSeed == jsonObject.MemberEnd())
  {
    return -1;
  }

  return rndSeed->value.GetInt();
}  // getRandomSeedFromJson

optilab::BaseSearchObject::BranchingVar getBranchingVariable(
    optilab::JSONValue::JsonObject& jsonObject)
{
  optilab::BaseSearchObject::BranchingVar branchingVar;

  // Get branching variable identifier
  auto varId = jsonObject.FindMember(optilab::jsonmodel::VARIABLE_ID);
  if (varId == jsonObject.MemberEnd())
  {
    throw std::runtime_error("Missing branching variable identifier");
  }
  branchingVar.first = varId->value.GetString();

  // Get branching variable indices
  auto varIdx = jsonObject.FindMember(optilab::jsonmodel::BRANCHING_VARIABLE_INDICES);
  if (varIdx == jsonObject.MemberEnd())
  {
    throw std::runtime_error("Missing branching variable indices");
  }

  for (auto& idx : varIdx->value.GetArray())
  {
    branchingVar.second.push_back(idx.GetInt());
  }

  return branchingVar;
}  // getBranchingVariable

optilab::BaseSearchObject::BranchingVarsList getBranchingVariablesListFromJson(
    optilab::JSONValue::JsonObject& jsonObject)
{
  optilab::BaseSearchObject::BranchingVarsList branchingVars;

  auto branchingVarList = jsonObject.FindMember(optilab::jsonmodel::BRANCHING_VARIABLES_LIST);
  if (branchingVarList == jsonObject.MemberEnd())
  {
    throw std::runtime_error("Missing branching variables list");
  }

  for (auto& var : branchingVarList->value.GetArray())
  {
    branchingVars.push_back(getBranchingVariable(var));
  }

  return branchingVars;
}  // getBranchingVariablesListFromJson

}  // namespace

namespace optilab {

ParserSearch::ParserSearch()
: Parser(ModelObject::ModelObjectType::SEARCH)
{
}

void ParserSearch::parseSearchObject(JSONValue::JsonObject& jsonObject)
{
  auto jsonObjId = jsonObject.FindMember(optilab::jsonmodel::SEARCH_OBJECT_ID);
  if (jsonObjId == jsonObject.MemberEnd())
  {
    throw std::runtime_error("ParserSearch - Missing search object id");
  }
  const std::string objId = jsonObjId->value.GetString();

  auto jsonObjCombinatorType =
      jsonObject.FindMember(optilab::jsonmodel::SEARCH_OBJECT_COMBINATOR_TYPE);
  if (jsonObjCombinatorType == jsonObject.MemberEnd())
  {
    throw std::runtime_error("ParserSearch - Missing search object combinator type");
  }
  const std::string combinatorType = jsonObjCombinatorType->value.GetString();

  SearchObject::SPtr searchObj;
  if (combinatorType == optilab::jsonmodel::SEARCH_OBJECT_COMBINATOR_TYPE_BASE_SEARCH)
  {
    searchObj = parseBaseSearchObject(objId, jsonObject);
  }
  else if (combinatorType == optilab::jsonmodel::SEARCH_OBJECT_COMBINATOR_TYPE_OR)
  {
    searchObj = parseOrSearchObject(objId, jsonObject);
  }
  else if (combinatorType == optilab::jsonmodel::SEARCH_OBJECT_COMBINATOR_TYPE_AND)
  {
    searchObj = parseAndSearchObject(objId, jsonObject);
  }
  else
  {
    throw std::runtime_error("ParserSearch - Combinator type not recognized: " + combinatorType);
  }
  assert(searchObj);

  pSearchObjectsList.push_back(searchObj);
}  // parseSearchObject

SearchObject::SPtr ParserSearch::parseBaseSearchObject(const std::string& objId,
                                                       JSONValue::JsonObject& jsonObject)
{
  std::shared_ptr<BaseSearchObject> searchObj = std::make_shared<BaseSearchObject>(objId);

  // Get and set variable selection strategy
  const auto varStrategy = getVariableSelectionStrategyFromJson(jsonObject);
  if (varStrategy.empty())
  {
    throw std::runtime_error("ParserSearch - BaseSearch: empty variable selection strategy");
  }
  searchObj->setVariableSelectionStrategy(varStrategy);

  // Get and set value selection strategy
  const auto valStrategy = getValueSelectionStrategyFromJson(jsonObject);
  if (valStrategy.empty())
  {
    throw std::runtime_error("ParserSearch - BaseSearch: empty value selection strategy");
  }
  searchObj->setValueSelectionStrategy(valStrategy);

  const auto randomSeed = getRandomSeedFromJson(jsonObject);
  if (randomSeed >= 0)
  {
    searchObj->setRandomSeed(static_cast<std::size_t>(randomSeed));
  }

  // Get and set the branching variables list
  auto varList = getBranchingVariablesListFromJson(jsonObject);
  for (const auto& varPair : varList)
  {
    if (varPair.first.empty())
    {
      throw std::runtime_error("ParserSearch - BaseSearch: branching variable name");
    }
    searchObj->addBranchingVariable(varPair);
  }

  return searchObj;
}  // parseBaseSearchObject

SearchObject::SPtr ParserSearch::parseOrSearchObject(const std::string& objId,
                                                     JSONValue::JsonObject& jsonObject)
{
  std::shared_ptr<OrObject> searchObj = std::make_shared<OrObject>(objId);

  auto objList = jsonObject.FindMember(optilab::jsonmodel::SEARCH_OBJECT_COMBINATORS_LIST);
  if (objList == jsonObject.MemberEnd())
  {
    throw std::runtime_error("ParserSearch - OrSearch: no search object list");
  }

  for (auto& objId : objList->value.GetArray())
  {
    searchObj->addSearchObjectIdToList(objId.GetString());
  }

  return searchObj;
}  // parseOrSearchObject

SearchObject::SPtr ParserSearch::parseAndSearchObject(const std::string& objId,
                                                     JSONValue::JsonObject& jsonObject)
{
  std::shared_ptr<AndObject> searchObj = std::make_shared<AndObject>(objId);

  auto objList = jsonObject.FindMember(optilab::jsonmodel::SEARCH_OBJECT_COMBINATORS_LIST);
  if (objList == jsonObject.MemberEnd())
  {
    throw std::runtime_error("ParserSearch - AndSearch: no search object list");
  }

  for (auto& objId : objList->value.GetArray())
  {
    searchObj->addSearchObjectIdToList(objId.GetString());
  }

  return searchObj;
}  // parseAndSearchObject

ModelObject::SPtr ParserSearch::parse(JSONValue::JsonObject& jsonObject)
{
  ModelObjectSearch::SPtr ptr;

  auto jsonObjSearchType =
      jsonObject.FindMember(optilab::jsonmodel::SEARCH_TYPE);
  if (jsonObjSearchType == jsonObject.MemberEnd())
  {
    throw std::runtime_error("ParserSearch - Missing search type");
  }
  const std::string searchType = jsonObjSearchType->value.GetString();

  // Instantiate a new model object search
  if (searchType == optilab::jsonmodel::SEARCH_TYPE_GLOBAL)
  {
    ptr = std::make_shared<ModelObjectSearch>(ModelObjectSearch::SearchType::GLOBAL);
  }
  else
  {
    throw std::runtime_error("ParserSearch - unrecognized search type: " + searchType);
  }

  // Get the root object id
  auto jsonObjId = jsonObject.FindMember(optilab::jsonmodel::SEARCH_ID);
  if (jsonObjId == jsonObject.MemberEnd())
  {
    throw std::runtime_error("ParserSearch - Missing search id");
  }
  const std::string objId = jsonObjId->value.GetString();

  // Set the root object id
  ptr->setRootSearchObjectId(objId);

  // Get number of solutions to find
  auto jsonObjNumSol = jsonObject.FindMember(optilab::jsonmodel::NUMBER_OF_SOLUTIONS);

  int numSol;
  if (jsonObjNumSol == jsonObject.MemberEnd())
  {
    spdlog::warn("ParserSearch - number of solutions not found, default to 1");
    numSol = 1;
  }
  else
  {
    numSol = jsonObjNumSol->value.GetInt();
  }
  ptr->setNumberOfSolutionsToFind(numSol);

  // Add all the primitive search objects from the list
  for (const auto& objPtr : pSearchObjectsList)
  {
    ptr->addSearchObject(objPtr);
  }

  // Return the search object
  return ptr;
}  // parse

ModelObject::SPtr ParserSearch::parseDescription(const std::string&)
{
  throw std::runtime_error("ParserSearch - parseDescription() is no longer supported");
}  // parseDescription

}  // namespace optilab
