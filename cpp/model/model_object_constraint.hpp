//
// Copyright OptiLab 2019. All rights reserved.
//
// Class encapsulating a constraint model object.
//

#pragma once

#include <cstddef>  // for std::size_t
#include <memory>   // for std::shared_ptr
#include <string>
#include <vector>

#include "model/model_object.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS ModelObjectConstraint : public ModelObject {
 public:
  enum ConstraintSemantic {
    CS_INTENSION = 0,
    CS_EXTENSION,
    CS_GLOBAL
  };

  using SPtr = std::shared_ptr<ModelObjectConstraint>;

public:
  ModelObjectConstraint(ConstraintSemantic semantic);
  ~ModelObjectConstraint() = default;

  static bool isa(const ModelObject* modelObject);
  static ModelObjectConstraint* cast(ModelObject* modelObject);
  static const ModelObjectConstraint* cast(const ModelObject* modelObject);

  /// Get/set semantic
  inline ConstraintSemantic getSemantic() const { return pSemantic; }

  /// Get/Set the constraint type
  inline void setType(const std::string& type) { pType = type; }
  inline const std::string& getType() const { return pType; }

  /// Get/Set constraint name
  inline void setName(const std::string& name) { pName = name; }
  inline const std::string& getName() const { return pName; }

  /// Get/Set constraint propagation type.
  /// @note by default, propagation type is set to "default"
  inline void setPropagationType(const std::string& pPropType) { pPropagationType = pPropType; }
  inline const std::string& getPropagationType() const { return pPropagationType; }

private:
  /// Semantic of the constraint, e.g., intension, extension, global, etc.
  ConstraintSemantic pSemantic;

  /// Type of this constraint: bool, int, float, etc.
  std::string pType;

  /// Name of this constraint, e.g., 2int, 2float, abs, acos, etc.
  std::string pName;

  /// Propagation type, e.g., default, domain, bounds, etc.
  /// This is usually a constraint annotation
  std::string pPropagationType;
};

}  // namespace optilab
