#include "model/parser_variable.hpp"

#include <stdexcept>  // for std::runtime_error
#include <utility>    // for std::pair
#include <vector>

#include "model/model_constants.hpp"
#include "model/model_object_variable.hpp"
#include "model/parser_utilities.hpp"

namespace {

bool domainDefinedOnBounds(optilab::ModelObjectVariable::VarType type)
{
  switch(type)
  {
    case optilab::ModelObjectVariable::VarType::INT:
    case optilab::ModelObjectVariable::VarType::BOOL:
    case optilab::ModelObjectVariable::VarType::FLOAT:
    case optilab::ModelObjectVariable::VarType::DOUBLE:
      return true;
    case optilab::ModelObjectVariable::VarType::SET:
    case optilab::ModelObjectVariable::VarType::LIST:
    case optilab::ModelObjectVariable::VarType::RANGE_LIST:
      return false;
    default:
      throw std::runtime_error("Unrecognized domain type");
  }
}  // domainDefinedOnBounds

optilab::ModelObjectVariable::VarType parseVariableType(const std::string& type)
{
  if (type == optilab::parser::VAR_INT)
  {
    return optilab::ModelObjectVariable::VarType::INT;
  }
  else if (type == optilab::parser::VAR_FLOAT)
  {
    return optilab::ModelObjectVariable::VarType::FLOAT;
  }
  else if (type == optilab::parser::VAR_BOOL)
  {
    return optilab::ModelObjectVariable::VarType::BOOL;
  }
  else if (type == optilab::parser::VAR_DOUBLE)
  {
    return optilab::ModelObjectVariable::VarType::DOUBLE;
  }
  else if (type == optilab::parser::VAR_SET)
  {
    return optilab::ModelObjectVariable::VarType::SET;
  }
  else if (type == optilab::parser::VAR_LIST)
  {
    return optilab::ModelObjectVariable::VarType::LIST;
  }
  else if (type == optilab::parser::VAR_RANGE_LIST)
  {
    return optilab::ModelObjectVariable::VarType::RANGE_LIST;
  }
  else
  {
    throw std::runtime_error(std::string("ParserVariable - invalid variable format type: ") + type);
  }
}  // parseVariableType

std::string getVariableIdFromJson(optilab::JSONValue::JsonObject& jsonObject)
{
  auto varId = jsonObject.FindMember(optilab::jsonmodel::VARIABLE_ID);
  if (varId == jsonObject.MemberEnd())
  {
    throw std::runtime_error("Missing variable identifier");
  }

  return varId->value.GetString();
}  // getVariableIdFromJson

std::string getVariableTypeFromJson(optilab::JSONValue::JsonObject& jsonObject)
{
  auto vartype = jsonObject.FindMember(optilab::jsonmodel::VARIABLE_TYPE);
  if (vartype == jsonObject.MemberEnd())
  {
    throw std::runtime_error("Missing variable type");
  }

  return vartype->value.GetString();
}  // getVariableTypeFromJson

std::pair<double, double> getDomainBoundsFromJson(optilab::JSONValue::JsonObject& jsonObject)
{
  auto lb = jsonObject.FindMember(optilab::jsonmodel::DOMAIN_LOWER_BOUND);
  auto ub = jsonObject.FindMember(optilab::jsonmodel::DOMAIN_UPPER_BOUND);
  if (lb == jsonObject.MemberEnd() || ub == jsonObject.MemberEnd())
  {
    throw std::runtime_error("Missing domain bounds");
  }

  return {lb->value.GetDouble(), ub->value.GetDouble()};
}  // getVariableTypeFromJson

std::vector<int> getVariableDimensionsFromJson(optilab::JSONValue::JsonObject& jsonObject)
{
  std::vector<int> dims;
  auto dimsList = jsonObject.FindMember(optilab::jsonmodel::VARIABLE_DIMENSIONS);
  if (dimsList == jsonObject.MemberEnd())
  {
    throw std::runtime_error("Missing variable dimensions");
  }

  for (auto& dimIter : dimsList->value.GetArray())
  {
    dims.push_back(dimIter.GetInt());
  }

  return dims;
}  // getVariableDimensionsFromJson

void setDomainOnType(optilab::JSONValue::JsonObject& jsonObject,
                     optilab::ModelObjectVariable::SPtr ptr)
{
  const auto varType = ptr->getDomainType();

  std::vector<double> vals;
  if (varType == optilab::ModelObjectVariable::VarType::LIST ||
      varType == optilab::ModelObjectVariable::VarType::SET)
  {
    const auto fieldName = (varType == optilab::ModelObjectVariable::VarType::LIST) ?
        std::string(optilab::parser::VAR_LIST) :
        std::string(optilab::parser::VAR_SET);
    auto list = jsonObject.FindMember(fieldName.c_str());
    if (list == jsonObject.MemberEnd())
    {
      throw std::runtime_error("Missing variable field: " + fieldName);
    }

    for (auto& listIter : list->value.GetArray())
    {
      vals.push_back(static_cast<double>(listIter.GetInt64()));
    }
  }
  else if (varType == optilab::ModelObjectVariable::VarType::RANGE_LIST)
  {
    auto list = jsonObject.FindMember(optilab::parser::VAR_RANGE_LIST);
    if (list == jsonObject.MemberEnd())
    {
      throw std::runtime_error("Missing variable field: " +
                               std::string(optilab::parser::VAR_RANGE_LIST));
    }

    for (auto& listIter : list->value.GetArray())
    {
      for (auto& obj : listIter.GetArray())
      {
        vals.push_back(static_cast<double>(obj.GetInt64()));
      }
    }
  }
  else
  {
    throw std::runtime_error("Unrecognized domain type");
  }

  ptr->setListOfDomainValues(vals);
}  // setDomainOnType

}  // namespace

namespace optilab {

ParserVariable::ParserVariable()
: Parser(ModelObject::ModelObjectType::VARIABLE)
{
}

ModelObject::SPtr ParserVariable::parse(JSONValue::JsonObject& jsonObject)
{
  ModelObjectVariable::SPtr ptr = std::make_shared<ModelObjectVariable>();

  // Get and set variable name
  const auto varId = getVariableIdFromJson(jsonObject);
  if (varId.empty())
  {
    throw std::runtime_error("Empty variable identifier");
  }
  ptr->setVariableName(varId);

  // Get variable type
  const auto varType = getVariableTypeFromJson(jsonObject);
  const auto domainType = parseVariableType(varType);
  ptr->setDomainType(domainType);

  // Get domain bounds/values
  if (domainDefinedOnBounds(domainType))
  {
    const auto domainBounds = getDomainBoundsFromJson(jsonObject);
    ptr->setDomainBounds(domainBounds.first, domainBounds.second);
  }
  else
  {
    // Set the domain w.r.t. the type of variable
    setDomainOnType(jsonObject, ptr);
  }

  // Get variable dimensions
  const auto varDims = getVariableDimensionsFromJson(jsonObject);
  ptr->setVariableDimensions(varDims);

  return ptr;
}  // parse

ModelObject::SPtr ParserVariable::parseDescription(const std::string& description)
{
  // @note DEPRECATED
  ModelObjectVariable::SPtr ptr;
  return ptr;
}  // parseDescription

}  // namespace optilab
