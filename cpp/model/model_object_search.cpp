#include "model/model_object_search.hpp"

#include <stdexcept>  // for std::invalid_argument

namespace optilab {

ModelObjectSearch::ModelObjectSearch(SearchType searchType)
: ModelObject(ModelObjectType::SEARCH),
  pSearchType(searchType)
{
}

void ModelObjectSearch::addSearchObject(SearchObject::SPtr searchObject)
{
  if (!searchObject)
  {
    throw std::invalid_argument("ModelObjectSearch - empty SearchObject pointer");
  }

  const auto& objectId = searchObject->getObjectId();
  if (pSearchObjectMap.find(objectId) != pSearchObjectMap.end())
  {
    throw std::invalid_argument("ModelObjectSearch - SearchObject with id " + objectId +
                                " already registered in the map");
  }
  pSearchObjectMap[objectId] = searchObject;
}  // addSearchObject

SearchObject::SPtr ModelObjectSearch::getSearchObject(const std::string& objectId) const
{
  if (pSearchObjectMap.find(objectId) == pSearchObjectMap.end())
  {
    SearchObject::SPtr emptyPtr;
    return emptyPtr;
  }
  return pSearchObjectMap.at(objectId);
}  // getSearchObject

}  // namespace optilab
