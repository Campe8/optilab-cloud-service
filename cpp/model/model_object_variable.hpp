//
// Copyright OptiLab 2019. All rights reserved.
//
// Class encapsulating a variable model object.
// This class encapsulates raw strings/data and the minimum amount
// of information for the back-end to be able to reconstruct a variable object
//

#pragma once

#include "model/model_object.hpp"

#include <cstddef>  // for std::size_t
#include <limits>   // for std::numeric_limits
#include <memory>
#include <string>
#include <vector>

#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS ModelObjectVariable : public ModelObject {
 public:
  using SPtr = std::shared_ptr<ModelObjectVariable>;

  enum VarType {
    /// Integer range variable.
    /// For example: x[1, 10]
    INT = 0,

    /// Boolean range variable.
    /// For example: x[false, true]
    BOOL,

    /// Float range variable.
    /// For example: x[10.2f, 20.5f]
    FLOAT,

    /// Double range variable.
    /// For example: x[10.2, 20.5]
    DOUBLE,

    /// Set variable.
    /// For example: x{{}, {1}, {2}, {3}, {1, 2}, ..., {1, 2, 3}}.
    /// Its domain is specified with the list x[1, 2, 3]
    SET,

    /// Variable over a list of values.
    /// For example: x{1, 20, 3, 50, 10, 30}
    LIST,

    /// Variable over a list of ranges.
    /// For example: x[[1, 10], [20, 30], [50, 100]]
    RANGE_LIST,

    /// Undefined type
    UNDEF
  };

public:
  /// Constructor: creates a new unbounded variable
  ModelObjectVariable();
  ~ModelObjectVariable() = default;

  static bool isa(const ModelObject* modelObject);
  static ModelObjectVariable* cast(ModelObject* modelObject);
  static const ModelObjectVariable* cast(const ModelObject* modelObject);

  void setVariableName(const std::string& varName) { pName = varName; }
  const std::string& getVariableName() const { return pName; }

  inline void setVariableDimensions(const std::vector<int>& dims) { pVarDims = dims; }
  inline bool isScalar() const { return pVarDims.empty(); }
  inline std::size_t numDimensions() const { return pVarDims.size(); }
  int getDimension(const std::size_t idx);

  /// Sets domain bounds.
  /// @note this method has semantic meaning for INT/BOOL/FLOAT, and DOUBLE types only,
  /// i.e., on types that are defined on a single range
  void setDomainBounds(double lowerBound, double upperBound);
  bool isUpperBoundInf() const;
  bool isLowerBoundInf() const;

  inline double getLowerBound() const { return pDomValues.front(); }
  inline double getUpperBound() const { return pDomValues.back(); }

  /// Sets the give list of domain values as values for this object variable.
  /// @note this method sorts the given list of values.
  /// @note this method has semantic meaning for all types but it doesn't check
  /// the size of the given list. This means that the caller can pass a list of values
  /// with 3 elements for an INT type variable, but only the lower and higher members will
  /// be considered as lower bound and upper bound respectively
  void setListOfDomainValues(const std::vector<double>& valueList);
  const std::vector<double>& getListOfDomainValues() const { return pDomValues; }

  inline void setDomainType(VarType varType) { pType = varType; }
  inline VarType getDomainType() const { return pType; }

private:
  /// Name of this variable
  std::string pName;

  /// Type of this variable, i.e., its domain type
  VarType pType{VarType::UNDEF};

  /// Vector of variable dimensions
  std::vector<int> pVarDims;

  /// Vector containing the domain values
  std::vector<double> pDomValues;
};

}  // namespace optilab
