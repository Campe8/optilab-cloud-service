//
// Copyright OptiLab 2019. All rights reserved.
//
// Class encapsulating an intension constraint model object.
// @note intension constraints are usually binary or "simple"
// constraints, for arithmetic and logic relations.
//

#pragma once

#include <string>
#include <vector>

#include "model/model_object_constraint.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS ModelObjectIntensionConstraint : public ModelObjectConstraint {
public:
  using ArgList = std::vector<std::vector<std::string>>;

public:
  ModelObjectIntensionConstraint() : ModelObjectConstraint(ConstraintSemantic::CS_INTENSION) {}
  ~ModelObjectIntensionConstraint() = default;

  /// Adds an argument to the intension constraint.
  /// @note an argument can be an array, therefore, the argument is represented as a vector
  inline void addArgument(const std::vector<std::string>& arg) { pArgsList.push_back(arg); }

  /// Returns the number of coefficients in the constraint
  inline int numArguments() const { return static_cast<int>(pArgsList.size()); }

  /// Returns the list of coefficients
  inline const ArgList& getArgumentsList() const { return pArgsList; }

private:
  /// List of constraint arguments
  ArgList pArgsList;
};

}  // namespace optilab
