//
// Copyright OptiLab 2019. All rights reserved.
//
// Constants for Model(s).
//

#pragma once

namespace optilab {

namespace model {

extern const char FRAMEWORK_CP[];
extern const char FRAMEWORK_GA[];
extern const char FRAMEWORK_IP[];
extern const char FRAMEWORK_LP[];
extern const char FRAMEWORK_MIP[];
extern const char FRAMEWORK_SCHEDULING[];
extern const char FRAMEWORK_VRP[];
extern const char PARAMETER_PKG_GECODE[];
extern const char PARAMETER_PKG_OPEN_GA[];
extern const char PARAMETER_PKG_OR_TOOLS[];
extern const char PARAMETER_PKG_SCIP[];

}  // namespace model

namespace jsonmodel {

extern const char BRANCHING_VARIABLE_INDICES[];
extern const char BRANCHING_VARIABLES_LIST[];
extern const char CLASS_TYPE[];
extern const char CONSTRAINT_ARGUMENTS_LIST[];
extern const char CONSTRAINT_ID[];
extern const char CONSTRAINT_NAME[];
extern const char CONSTRAINT_PROPAGATION_TYPE[];
extern const char CONSTRAINT_PROPAGATION_TYPE_BOUNDS[];
extern const char CONSTRAINT_PROPAGATION_TYPE_DEFAULT[];
extern const char CONSTRAINT_PROPAGATION_TYPE_DOMAIN[];
extern const char CONSTRAINT_SEMANTIC[];
extern const char CONSTRAINT_SEMANTIC_EXTENSION[];
extern const char CONSTRAINT_SEMANTIC_GLOBAL[];
extern const char CONSTRAINT_SEMANTIC_INTENSION[];
extern const char CONSTRAINT_TYPE[];
extern const char CONSTRAINTS_LIST[];
extern const int DEFAULT_NUMBER_OF_SOLUTIONS;
extern const char DOMAIN_LOWER_BOUND[];
extern const char DOMAIN_UPPER_BOUND[];
extern const char FLATZINC_MODEL_FORMAT[];
extern const char FRAMEWORK[];
extern const char MODEL_DATA[];
extern const char MODEL_DESCRIPTOR[];
extern const char MODEL_FILE_DESCRIPTOR[];
extern const char MODEL_FORMAT[];
extern const char MODEL_NAME[];
extern const char MPS_MODEL_FORMAT[];
extern const char MPS_MODEL_FORMAT_EXT[];
extern const char NUMBER_OF_SOLUTIONS[];
extern const char OBJECT_ID[];
extern const char OBJECTIVES_LIST[];
extern const char OPTILAB_MODEL_FORMAT[];
extern const char PACKAGE[];
extern const char RANDOM_SEED[];
extern const char SEARCH_LIST[];
extern const char SEARCH_ID[];
extern const char SEARCH_TYPE[];
extern const char SEARCH_TYPE_GLOBAL[];
extern const char SEARCH_OBJECT_ID[];
extern const char SEARCH_OBJECT_COMBINATORS_LIST[];
extern const char SEARCH_OBJECT_COMBINATOR_TYPE[];
extern const char SEARCH_OBJECT_COMBINATOR_TYPE_BASE_SEARCH[];
extern const char SEARCH_OBJECT_COMBINATOR_TYPE_OR[];
extern const char SEARCH_OBJECT_COMBINATOR_TYPE_AND[];
extern const char SEARCH_OBJECTS_LIST[];
extern const char USE_FILE_DESCRIPTORS[];
extern const char VALUE_SELECTION_STRATEGY[];
extern const char VARIABLE_DIMENSIONS[];
extern const char VARIABLE_ID[];
extern const char VARIABLE_SELECTION_STRATEGY[];
extern const char VARIABLE_TYPE[];
extern const char VARIABLES_LIST[];

}  // jsonmodel

namespace jsonmipmodel {

extern const char CONSTRAINT_VAR_COEFF_LIST[];
extern const char CONSTRAINT_LOWER_BOUND[];
extern const char CONSTRAINT_UPPER_BOUND[];
extern const char OBJECTIVE_VAR_COEFF_LIST[];
extern const char OPTIMIZATION_DIRECTION[];
extern const char OPTIMIZATION_DIRECTION_MAXIMIZE[];
extern const char OPTIMIZATION_DIRECTION_MINIMIZE[];
extern const char VARIABLE_COEFF[];

}  // jsonmipmodel

namespace jsonschedulingmodel {

extern const char NUM_DAYS[];
extern const char NUM_PERSONNEL[];
extern const char NUM_SHIFTS[];
extern const char SHIFTS_REQUESTS[];

}  // jsonschedulingmodel

namespace jsonvrpmodel {

extern const char DEMANDS[];
extern const char DEPOT[];
extern const char DISTANCE_MATRIX[];
extern const char NUM_VEHICLES[];
extern const char VEHICLE_CAPACITIES[];

}  // jsonvrpmodel

namespace jsoncombinatormodel {

extern const char COMBINATOR_LOGIC[];
extern const char COMBINATOR_LOGIC_COLLECT_ALL[];
extern const char COMBINATOR_LOGIC_FIRST_WIN[];
extern const char COMBINATOR_OBJECT_ID_LIST[];
extern const char COMBINATOR_OBJECT_LIST[];

}  // jsoncombinatormodel

namespace parser {

extern const char VAR_BOOL[];
extern const char VAR_DOUBLE[];
extern const char VAR_FLOAT[];
extern const char VAR_INT[];
extern const char VAR_LIST[];
extern const char VAR_RANGE_LIST[];
extern const char VAR_SET[];
extern const char VAR_LB_INF[];
extern const char VAR_UB_INF[];

}  // namespace parser

namespace ortoolsconstants {

extern const char CHOOSE_FIRST_UNBOUND[];
extern const char CHOOSE_HIGHEST_MAX[];
extern const char CHOOSE_LOWEST_MIN[];
extern const char CHOOSE_MIN_SIZE_HIGHEST_MAX[];
extern const char CHOOSE_MIN_SIZE_HIGHEST_MIN[];
extern const char CHOOSE_MIN_SIZE_LOWEST_MAX[];
extern const char CHOOSE_MIN_SIZE_LOWEST_MIN[];
extern const char CHOOSE_PATH[];
extern const char CHOOSE_RANDOM[];
extern const char INT_VAR_DEFAULT[];
extern const char INT_VAR_SIMPLE[];
extern const char CHOOSE_MAX_REGRET_ON_MIN[];
extern const char CHOOSE_MAX_SIZE[];
extern const char CHOOSE_MIN_SIZE[];

extern const char ASSIGN_CENTER_VALUE[];
extern const char ASSIGN_MAX_VALUE[];
extern const char ASSIGN_MIN_VALUE[];
extern const char ASSIGN_RANDOM_VALUE[];
extern const char INT_VALUE_DEFAULT[];
extern const char INT_VALUE_SIMPLE[];
extern const char SPLIT_LOWER_HALF[];
extern const char SPLIT_UPPER_HALF[];

}  // ortools

namespace gecodeconstants {

extern const char BOOL_VAR_ACTION_MAX[];
extern const char BOOL_VAR_ACTION_MIN[];
extern const char BOOL_VAR_AFC_MAX[];
extern const char BOOL_VAR_AFC_MIN[];
extern const char BOOL_VAR_CHB_MAX[];
extern const char BOOL_VAR_CHB_MIN[];
extern const char BOOL_VAR_DEGREE_MAX[];
extern const char BOOL_VAR_DEGREE_MIN[];
extern const char BOOL_VAR_MERIT_MAX[];
extern const char BOOL_VAR_MERIT_MIN[];
extern const char BOOL_VAR_NONE[];
extern const char BOOL_VAR_RND[];

extern const char BOOL_VAL_MAX[];
extern const char BOOL_VAL_MIN[];
extern const char BOOL_VAL_RND[];
extern const char BOOL_VAL[];

extern const char INT_VAR_ACTION_MAX[];
extern const char INT_VAR_ACTION_MIN[];
extern const char INT_VAR_ACTION_SIZE_MAX[];
extern const char INT_VAR_ACTION_SIZE_MIN[];
extern const char INT_VAR_AFC_MAX[];
extern const char INT_VAR_AFC_MIN[];
extern const char INT_VAR_AFC_SIZE_MAX[];
extern const char INT_VAR_AFC_SIZE_MIN[];
extern const char INT_VAR_CHB_MAX[];
extern const char INT_VAR_CHB_MIN[];
extern const char INT_VAR_CHB_SIZE_MAX[];
extern const char INT_VAR_CHB_SIZE_MIN[];
extern const char INT_VAR_DEGREE_MAX[];
extern const char INT_VAR_DEGREE_MIN[];
extern const char INT_VAR_DEGREE_SIZE_MAX[];
extern const char INT_VAR_DEGREE_SIZE_MIN[];
extern const char INT_VAR_MAX_MAX[];
extern const char INT_VAR_MAX_MIN[];
extern const char INT_VAR_MERIT_MAX[];
extern const char INT_VAR_MERIT_MIN[];
extern const char INT_VAR_MIN_MAX[];
extern const char INT_VAR_MIN_MIN[];
extern const char INT_VAR_NONE[];
extern const char INT_VAR_REGRET_MAX_MAX[];
extern const char INT_VAR_REGRET_MAX_MIN[];
extern const char INT_VAR_REGRET_MIN_MAX[];
extern const char INT_VAR_REGRET_MIN_MIN[];
extern const char INT_VAR_RND[];
extern const char INT_VAR_SIZE_MAX[];
extern const char INT_VAR_SIZE_MIN[];

extern const char INT_VAL_MAX[];
extern const char INT_VAL_MED[];
extern const char INT_VAL_MIN[];
extern const char INT_VAL_RANGE_MAX[];
extern const char INT_VAL_RANGE_MIN[];
extern const char INT_VAL_RND[];
extern const char INT_VAL_SPLIT_MAX[];
extern const char INT_VAL_SPLIT_MIN[];
extern const char INT_VAL[];
extern const char INT_VALUES_MAX[];
extern const char INT_VALUES_MIN[];

extern const char SET_VAR_ACTION_MAX[];
extern const char SET_VAR_ACTION_MIN[];
extern const char SET_VAR_ACTION_SIZE_MAX[];
extern const char SET_VAR_ACTION_SIZE_MIN[];
extern const char SET_VAR_AFC_MAX[];
extern const char SET_VAR_AFC_MIN[];
extern const char SET_VAR_AFC_SIZE_MAX[];
extern const char SET_VAR_AFC_SIZE_MIN[];
extern const char SET_VAR_CHB_MAX[];
extern const char SET_VAR_CHB_MIN[];
extern const char SET_VAR_CHB_SIZE_MAX[];
extern const char SET_VAR_CHB_SIZE_MIN[];
extern const char SET_VAR_DEGREE_MAX[];
extern const char SET_VAR_DEGREE_MIN[];
extern const char SET_VAR_DEGREE_SIZE_MAX[];
extern const char SET_VAR_DEGREE_SIZE_MIN[];
extern const char SET_VAR_MAX_MAX[];
extern const char SET_VAR_MAX_MIN[];
extern const char SET_VAR_MERIT_MAX[];
extern const char SET_VAR_MERIT_MIN[];
extern const char SET_VAR_MIN_MAX[];
extern const char SET_VAR_MIN_MIN[];
extern const char SET_VAR_NONE[];
extern const char SET_VAR_RND[];
extern const char SET_VAR_SIZE_MAX[];
extern const char SET_VAR_SIZE_MIN[];

extern const char SET_VAL_MAX_EXC[];
extern const char SET_VAL_MAX_INC[];
extern const char SET_VAL_MED_EXC[];
extern const char SET_VAL_MED_INC[];
extern const char SET_VAL_MIN_EXC[];
extern const char SET_VAL_MIN_INC[];
extern const char SET_VAL_RND_EXC[];
extern const char SET_VAL_RND_INC[];
extern const char SET_VAL[];

extern const char FLOAT_VAR_ACTION_MAX[];
extern const char FLOAT_VAR_ACTION_MIN[];
extern const char FLOAT_VAR_ACTION_SIZE_MAX[];
extern const char FLOAT_VAR_ACTION_SIZE_MIN[];
extern const char FLOAT_VAR_AFC_MAX[];
extern const char FLOAT_VAR_AFC_MIN[];
extern const char FLOAT_VAR_AFC_SIZE_MAX[];
extern const char FLOAT_VAR_AFC_SIZE_MIN[];
extern const char FLOAT_VAR_CHB_MAX[];
extern const char FLOAT_VAR_CHB_MIN[];
extern const char FLOAT_VAR_CHB_SIZE_MAX[];
extern const char FLOAT_VAR_CHB_SIZE_MIN[];
extern const char FLOAT_VAR_DEGREE_MAX[];
extern const char FLOAT_VAR_DEGREE_MIN[];
extern const char FLOAT_VAR_DEGREE_SIZE_MAX[];
extern const char FLOAT_VAR_DEGREE_SIZE_MIN[];
extern const char FLOAT_VAR_MAX_MAX[];
extern const char FLOAT_VAR_MAX_MIN[];
extern const char FLOAT_VAR_MERIT_MAX[];
extern const char FLOAT_VAR_MERIT_MIN[];
extern const char FLOAT_VAR_MIN_MAX[];
extern const char FLOAT_VAR_MIN_MIN[];
extern const char FLOAT_VAR_NONE[];
extern const char FLOAT_VAR_RND[];
extern const char FLOAT_VAR_SIZE_MAX[];
extern const char FLOAT_VAR_SIZE_MIN[];

extern const char FLOAT_VAL_SPLIT_MAX[];
extern const char FLOAT_VAL_SPLIT_MIN[];
extern const char FLOAT_VAL_SPLIT_RND[];
extern const char FLOAT_VAL[];

}  // gecodeconstants

}  // namespace optilab


