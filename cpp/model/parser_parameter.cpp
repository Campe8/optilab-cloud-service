#include "model/parser_parameter.hpp"

namespace optilab {

ParserParameter::ParserParameter()
: Parser(ModelObject::ModelObjectType::PARAMETER)
{
}

ModelObject::SPtr ParserParameter::parseDescription(const std::string& description)
{
  ModelObject::SPtr ptr;
  return ptr;
}  // parseDescription

ModelObject::SPtr ParserParameter::parse(JSONValue::JsonObject&)
{
  ModelObject::SPtr ptr;
  return ptr;
}  // parse

}  // namespace optilab
