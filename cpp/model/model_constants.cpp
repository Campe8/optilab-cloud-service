#include "model/model_constants.hpp"

namespace optilab {

namespace model {

const char FRAMEWORK_CP[] = "CP";
const char FRAMEWORK_GA[] = "GA";
const char FRAMEWORK_IP[] = "IP";
const char FRAMEWORK_LP[] = "LP";
const char FRAMEWORK_MIP[] = "MIP";
const char FRAMEWORK_SCHEDULING[] = "SCHEDULING";
const char FRAMEWORK_VRP[] = "VRP";
const char PARAMETER_PKG_GECODE[] = "GECODE";
const char PARAMETER_PKG_OPEN_GA[] = "OPEN-GA";
const char PARAMETER_PKG_OR_TOOLS[] = "OR-TOOLS";
const char PARAMETER_PKG_SCIP[] = "SCIP";

}  // namespace model

namespace jsonmodel {

const char BRANCHING_VARIABLE_INDICES[] = "idx";
const char BRANCHING_VARIABLES_LIST[] = "branchingList";
const char CLASS_TYPE[] = "classType";
const char CONSTRAINT_ARGUMENTS_LIST[] = "args_list";
const char CONSTRAINT_ID[] = "id";
const char CONSTRAINT_NAME[] = "name";
const char CONSTRAINT_PROPAGATION_TYPE[] = "propagation_type";
const char CONSTRAINT_PROPAGATION_TYPE_BOUNDS[] = "bounds";
const char CONSTRAINT_PROPAGATION_TYPE_DEFAULT[] = "default";
const char CONSTRAINT_PROPAGATION_TYPE_DOMAIN[] = "domain";
const char CONSTRAINT_SEMANTIC[] = "semantic";
const char CONSTRAINT_SEMANTIC_EXTENSION[] = "extension";
const char CONSTRAINT_SEMANTIC_GLOBAL[] = "global";
const char CONSTRAINT_SEMANTIC_INTENSION[] = "intension";
const char CONSTRAINT_TYPE[] = "type";
const char CONSTRAINTS_LIST[] = "constraintsList";
const int DEFAULT_NUMBER_OF_SOLUTIONS = 1;
const char DOMAIN_LOWER_BOUND[] = "lb";
const char DOMAIN_UPPER_BOUND[] = "ub";
const char FLATZINC_MODEL_FORMAT[] = "FLATZINC";
const char FRAMEWORK[] = "framework";
const char MODEL_DATA[] = "model_data";
const char MODEL_DESCRIPTOR[] = "model_descriptor";
const char MODEL_FILE_DESCRIPTOR[] = "model_descriptor";
const char MODEL_FORMAT[] = "model_format";
const char MPS_MODEL_FORMAT_EXT[] = "mps";
const char MODEL_NAME[] = "model";
const char MPS_MODEL_FORMAT[] = "MPS";
const char NUMBER_OF_SOLUTIONS[] = "numSolutions";
const char OBJECT_ID[] = "id";
const char OBJECTIVES_LIST[] = "objectivesList";
const char OPTILAB_MODEL_FORMAT[] = "OptiLab";
const char PACKAGE[] = "package";
const char RANDOM_SEED[] = "randomSeed";
const char SEARCH_LIST[] = "searchList";
const char SEARCH_ID[] = "searchId";
const char SEARCH_TYPE[] = "searchType";
const char SEARCH_TYPE_GLOBAL[] = "global";
const char SEARCH_OBJECT_ID[] = "id";
const char SEARCH_OBJECT_COMBINATORS_LIST[] = "searchObjectList";
const char SEARCH_OBJECT_COMBINATOR_TYPE[] = "combinatorType";
const char SEARCH_OBJECT_COMBINATOR_TYPE_BASE_SEARCH[] = "baseSearch";
const char SEARCH_OBJECT_COMBINATOR_TYPE_OR[] = "or";
const char SEARCH_OBJECT_COMBINATOR_TYPE_AND[] = "and";
const char SEARCH_OBJECTS_LIST[] = "searchObjects";
const char USE_FILE_DESCRIPTORS[] = "use_file_descriptors";
const char VALUE_SELECTION_STRATEGY[] = "valSelection";
const char VARIABLE_DIMENSIONS[] = "dims";
const char VARIABLE_ID[] = "id";
const char VARIABLE_SELECTION_STRATEGY[] = "varSelection";
const char VARIABLE_TYPE[] = "type";
const char VARIABLES_LIST[] = "variablesList";

}  // jsonmodel


namespace jsonmipmodel {

const char CONSTRAINT_VAR_COEFF_LIST[] = "coefficientsList";
const char CONSTRAINT_LOWER_BOUND[] = "lb";
const char CONSTRAINT_UPPER_BOUND[] = "ub";
const char OBJECTIVE_VAR_COEFF_LIST[] = "coefficientsList";
const char OPTIMIZATION_DIRECTION[] = "optimizationDirection";
const char OPTIMIZATION_DIRECTION_MAXIMIZE[] = "maximize";
const char OPTIMIZATION_DIRECTION_MINIMIZE[] = "minimize";
const char VARIABLE_COEFF[] = "coeff";

}  // jsonmipmodel

namespace jsonschedulingmodel {

const char NUM_DAYS[] = "num_days";
const char NUM_PERSONNEL[] = "num_personnel";
const char NUM_SHIFTS[] = "num_shifts";
const char SHIFTS_REQUESTS[] = "shift_requests";

}  // jsonschedulingmodel

namespace jsonvrpmodel {

const char DEMANDS[] = "demands";
const char DEPOT[] = "depot";
const char DISTANCE_MATRIX[] = "distance_matrix";
const char NUM_VEHICLES[] = "num_vehicles";
const char VEHICLE_CAPACITIES[] = "vehicle_capacities";

}  // jsonvrpmodel

namespace jsoncombinatormodel {

const char COMBINATOR_LOGIC[] = "combinatorLogic";
const char COMBINATOR_LOGIC_COLLECT_ALL[] = "collect_all";
const char COMBINATOR_LOGIC_FIRST_WIN[] = "first_win";
const char COMBINATOR_OBJECT_ID_LIST[] = "combinatorObjects";
const char COMBINATOR_OBJECT_LIST[] = "objectsList";

}  // jsoncombinatormodel

namespace parser {

const char VAR_BOOL[] = "bool";
const char VAR_DOUBLE[] = "double";
const char VAR_FLOAT[] = "float";
const char VAR_INT[] = "int";
const char VAR_LIST[] = "list";
const char VAR_RANGE_LIST[] = "range_list";
const char VAR_SET[] = "set";
const char VAR_LB_INF[] = "inf";
const char VAR_UB_INF[] = "inf";

}  // namespace parser

namespace ortoolsconstants {

const char CHOOSE_FIRST_UNBOUND[] = "first_unbound";
const char CHOOSE_HIGHEST_MAX[] = "highest_max";
const char CHOOSE_LOWEST_MIN[] = "lowest_min";
const char CHOOSE_MIN_SIZE_HIGHEST_MAX[] = "min_size_highest_max";
const char CHOOSE_MIN_SIZE_HIGHEST_MIN[] = "min_size_highest_min";
const char CHOOSE_MIN_SIZE_LOWEST_MAX[] = "min_size_lowest_max";
const char CHOOSE_MIN_SIZE_LOWEST_MIN[] = "min_size_lowest_min";
const char CHOOSE_PATH[] = "path";
const char CHOOSE_RANDOM[] = "random";
const char CHOOSE_MAX_REGRET_ON_MIN[] = "max_regret_on_min";
const char CHOOSE_MAX_SIZE[] = "max_size";
const char CHOOSE_MIN_SIZE[] = "min_size";
const char INT_VAR_DEFAULT[] = "default";
const char INT_VAR_SIMPLE[] = "simple";

const char ASSIGN_CENTER_VALUE[] = "center_value";
const char ASSIGN_MAX_VALUE[] = "max_value";
const char ASSIGN_MIN_VALUE[] = "min_value";
const char ASSIGN_RANDOM_VALUE[] = "random_value";
const char INT_VALUE_DEFAULT[] = "default";
const char INT_VALUE_SIMPLE[] = "simple";
const char SPLIT_LOWER_HALF[] = "split_lower_half";
const char SPLIT_UPPER_HALF[] = "split_upper_half";

}  // ortoolsconstants

namespace gecodeconstants {

const char BOOL_VAR_ACTION_MAX[] = "bool_var_action_max";
const char BOOL_VAR_ACTION_MIN[] = "bool_var_action_min";
const char BOOL_VAR_AFC_MAX[] = "bool_var_afc_max";
const char BOOL_VAR_AFC_MIN[] = "bool_var_afc_min";
const char BOOL_VAR_CHB_MAX[] = "bool_var_chb_max";
const char BOOL_VAR_CHB_MIN[] = "bool_var_chb_min";
const char BOOL_VAR_DEGREE_MAX[] = "bool_var_degree_max";
const char BOOL_VAR_DEGREE_MIN[] = "bool_var_degree_min";
const char BOOL_VAR_MERIT_MAX[] = "bool_var_merit_max";
const char BOOL_VAR_MERIT_MIN[] = "bool_var_merit_min";
const char BOOL_VAR_NONE[] = "bool_var_none";
const char BOOL_VAR_RND[] = "bool_var_rnd";

const char BOOL_VAL_MAX[] = "bool_var_max";
const char BOOL_VAL_MIN[] = "bool_var_min";
const char BOOL_VAL_RND[] = "bool_var_rnd";
const char BOOL_VAL[] = "bool_val";

const char INT_VAR_ACTION_MAX[] = "int_var_action_max";
const char INT_VAR_ACTION_MIN[] = "int_var_action_min";
const char INT_VAR_ACTION_SIZE_MAX[] = "int_var_action_size_min";
const char INT_VAR_ACTION_SIZE_MIN[] = "int_var_action_size_max";;
const char INT_VAR_AFC_MAX[] = "int_var_afc_max";
const char INT_VAR_AFC_MIN[] = "int_var_afc_min";
const char INT_VAR_AFC_SIZE_MAX[] = "int_var_afc_size_max";
const char INT_VAR_AFC_SIZE_MIN[] = "int_var_afc_size_min";
const char INT_VAR_CHB_MAX[] = "int_var_chb_max";
const char INT_VAR_CHB_MIN[] = "int_var_chb_min";
const char INT_VAR_CHB_SIZE_MAX[] = "int_var_chb_size_max";
const char INT_VAR_CHB_SIZE_MIN[] = "int_var_chb_size_min";
const char INT_VAR_DEGREE_MAX[] = "int_var_degree_max";
const char INT_VAR_DEGREE_MIN[] = "int_var_degree_min";
const char INT_VAR_DEGREE_SIZE_MAX[] = "int_var_degree_size_max";
const char INT_VAR_DEGREE_SIZE_MIN[] = "int_var_degree_size_min";
const char INT_VAR_MAX_MAX[] = "int_var_max_max";
const char INT_VAR_MAX_MIN[] = "int_var_max_min";
const char INT_VAR_MERIT_MAX[] = "int_var_merit_max";
const char INT_VAR_MERIT_MIN[] = "int_var_merit_min";
const char INT_VAR_MIN_MAX[] = "int_var_min_max";
const char INT_VAR_MIN_MIN[] = "int_var_min_min";
const char INT_VAR_NONE[] = "int_var_none";
const char INT_VAR_REGRET_MAX_MAX[] = "int_var_regret_max_max";
const char INT_VAR_REGRET_MAX_MIN[] = "int_var_regret_max_min";
const char INT_VAR_REGRET_MIN_MAX[] = "int_var_regret_min_max";
const char INT_VAR_REGRET_MIN_MIN[] = "int_var_regret_min_min";
const char INT_VAR_RND[] = "int_var_rnd";
const char INT_VAR_SIZE_MAX[] = "int_var_size_max";
const char INT_VAR_SIZE_MIN[] = "int_var_size_min";

const char INT_VAL_MAX[] = "int_val_max";
const char INT_VAL_MED[] = "int_val_med";
const char INT_VAL_MIN[] = "int_val_min";
const char INT_VAL_RANGE_MAX[] = "int_val_range_max";
const char INT_VAL_RANGE_MIN[] = "int_val_range_min";
const char INT_VAL_RND[] = "int_val_rnd";
const char INT_VAL_SPLIT_MAX[] = "int_val_split_max";
const char INT_VAL_SPLIT_MIN[] = "int_val_split_min";
const char INT_VAL[] = "int_val";
const char INT_VALUES_MAX[] = "int_values_max";
const char INT_VALUES_MIN[] = "int_values_min";

const char SET_VAR_ACTION_MAX[] = "set_var_action_max";
const char SET_VAR_ACTION_MIN[] = "set_var_action_min";
const char SET_VAR_ACTION_SIZE_MAX[] = "set_var_action_size_max";
const char SET_VAR_ACTION_SIZE_MIN[] = "set_var_action_size_min";
const char SET_VAR_AFC_MAX[] = "set_var_afc_max";
const char SET_VAR_AFC_MIN[] = "set_var_afc_min";
const char SET_VAR_AFC_SIZE_MAX[] = "set_var_afc_size_max";
const char SET_VAR_AFC_SIZE_MIN[] = "set_var_afc_size_min";
const char SET_VAR_CHB_MAX[] = "set_var_chb_max";
const char SET_VAR_CHB_MIN[] = "set_var_chb_min";
const char SET_VAR_CHB_SIZE_MAX[] = "set_var_chb_size_max";
const char SET_VAR_CHB_SIZE_MIN[] = "set_var_chb_size_min";
const char SET_VAR_DEGREE_MAX[] = "set_var_degree_max";
const char SET_VAR_DEGREE_MIN[] = "set_var_degree_min";
const char SET_VAR_DEGREE_SIZE_MAX[] = "set_var_degree_size_max";
const char SET_VAR_DEGREE_SIZE_MIN[] = "set_var_degree_size_min";
const char SET_VAR_MAX_MAX[] = "set_var_max_max";
const char SET_VAR_MAX_MIN[] = "set_var_max_min";
const char SET_VAR_MERIT_MAX[] = "set_var_merit_max";
const char SET_VAR_MERIT_MIN[] = "set_var_merit_min";
const char SET_VAR_MIN_MAX[] = "set_var_min_max";
const char SET_VAR_MIN_MIN[] = "set_var_min_min";
const char SET_VAR_NONE[] = "set_var_none";
const char SET_VAR_RND[] = "set_var_rnd";
const char SET_VAR_SIZE_MAX[] = "set_var_size_max";
const char SET_VAR_SIZE_MIN[] = "set_var_size_min";

const char SET_VAL_MAX_EXC[] = "set_val_max_exc";
const char SET_VAL_MAX_INC[] = "set_val_max_inc";
const char SET_VAL_MED_EXC[] = "set_val_med_exc";
const char SET_VAL_MED_INC[] = "set_val_med_inc";
const char SET_VAL_MIN_EXC[] = "set_val_min_exc";
const char SET_VAL_MIN_INC[] = "set_val_min_inc";
const char SET_VAL_RND_EXC[] = "set_val_rnd_exc";
const char SET_VAL_RND_INC[] = "set_val_rnd_inc";
const char SET_VAL[] = "set_val";

const char FLOAT_VAR_ACTION_MAX[] = "float_var_action_max";
const char FLOAT_VAR_ACTION_MIN[] = "float_var_action_min";
const char FLOAT_VAR_ACTION_SIZE_MAX[] = "float_var_action_size_max";
const char FLOAT_VAR_ACTION_SIZE_MIN[] = "float_var_action_size_min";
const char FLOAT_VAR_AFC_MAX[] = "float_var_afc_max";
const char FLOAT_VAR_AFC_MIN[] = "float_var_afc_min";
const char FLOAT_VAR_AFC_SIZE_MAX[] = "float_var_afc_size_max";
const char FLOAT_VAR_AFC_SIZE_MIN[] = "float_var_afc_size_min";
const char FLOAT_VAR_CHB_MAX[] = "float_var_chb_max";
const char FLOAT_VAR_CHB_MIN[] = "float_var_chb_min";
const char FLOAT_VAR_CHB_SIZE_MAX[] = "float_var_chb_size_max";
const char FLOAT_VAR_CHB_SIZE_MIN[] = "float_var_chb_size_min";
const char FLOAT_VAR_DEGREE_MAX[] = "float_var_degree_max";
const char FLOAT_VAR_DEGREE_MIN[] = "float_var_degree_min";
const char FLOAT_VAR_DEGREE_SIZE_MAX[] = "float_var_degree_size_max";
const char FLOAT_VAR_DEGREE_SIZE_MIN[] = "float_var_degree_size_min";
const char FLOAT_VAR_MAX_MAX[] = "float_var_max_max";
const char FLOAT_VAR_MAX_MIN[] = "float_var_max_min";
const char FLOAT_VAR_MERIT_MAX[] = "float_var_merit_max";
const char FLOAT_VAR_MERIT_MIN[] = "float_var_merit_min";
const char FLOAT_VAR_MIN_MAX[] = "float_var_min_max";
const char FLOAT_VAR_MIN_MIN[] = "float_var_min_min";
const char FLOAT_VAR_NONE[] = "float_var_none";
const char FLOAT_VAR_RND[] = "float_var_rnd";
const char FLOAT_VAR_SIZE_MAX[] = "float_var_size_max";
const char FLOAT_VAR_SIZE_MIN[] = "float_var_size_min";

const char FLOAT_VAL_SPLIT_MAX[] = "float_val_split_max";
const char FLOAT_VAL_SPLIT_MIN[] = "float_val_split_min";
const char FLOAT_VAL_SPLIT_RND[] = "float_val_split_rnd";
const char FLOAT_VAL[] = "float_val";

}  // gecodeconstants

}  // namespace optilab
