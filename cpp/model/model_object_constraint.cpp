#include "model/model_object_constraint.hpp"

#include "model/model_constants.hpp"

namespace optilab {

ModelObjectConstraint::ModelObjectConstraint(ConstraintSemantic semantic)
: ModelObject(ModelObjectType::CONSTRAINT),
  pSemantic(semantic),
  pPropagationType(jsonmodel::CONSTRAINT_PROPAGATION_TYPE_DEFAULT)
{
}

bool ModelObjectConstraint::isa(const ModelObject* modelObject)
{
  return modelObject &&
      (modelObject->getType() == ModelObjectType::CONSTRAINT);
}  // isa

ModelObjectConstraint* ModelObjectConstraint::cast(ModelObject* modelObject)
{
  if (!isa(modelObject)) return nullptr;
  return static_cast<ModelObjectConstraint*>(modelObject);
}  // cast

const ModelObjectConstraint* ModelObjectConstraint::cast(const ModelObject* modelObject)
{
  if (!isa(modelObject)) return nullptr;
  return static_cast<const ModelObjectConstraint*>(modelObject);
}  // cast

}  // namespace optilab

