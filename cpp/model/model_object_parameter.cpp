#include "model/model_object_parameter.hpp"

namespace optilab {

ModelObjectParameter::ModelObjectParameter()
: ModelObject(ModelObjectType::PARAMETER)
{
}

bool ModelObjectParameter::isa(const ModelObject* modelObject)
{
  return modelObject &&
      (modelObject->getType() == ModelObjectType::PARAMETER);
}  // isa

ModelObjectParameter* ModelObjectParameter::cast(ModelObject* modelObject)
{
  if (!isa(modelObject)) return nullptr;
  return static_cast<ModelObjectParameter*>(modelObject);
}  // cast

const ModelObjectParameter* ModelObjectParameter::cast(const ModelObject* modelObject)
{
  if (!isa(modelObject)) return nullptr;
  return static_cast<const ModelObjectParameter*>(modelObject);
}  // cast

}  // namespace optilab

