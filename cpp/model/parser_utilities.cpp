#include "model/parser_utilities.hpp"

#include <algorithm>   // for std::find_if
#include <functional>  // for std::not1
#include <stdexcept>   // for std::runtime_error
#include <sstream>

namespace optilab {
namespace parserutils {

std::string getStringObjectOrThrow(const std::string& objKey,
                                   optilab::JSONValue::JsonObject& jsonObject)
{
  auto strObj = jsonObject.FindMember(objKey.c_str());
  if (strObj == jsonObject.MemberEnd())
  {
    throw std::runtime_error("Json has no object with key " + objKey);
  }

  return strObj->value.GetString();
}  // getStringObjectOrThrow

double getDoubleObjectOrThrow(const std::string& objKey, optilab::JSONValue::JsonObject& jsonObject)
{
  auto strObj = jsonObject.FindMember(objKey.c_str());
  if (strObj == jsonObject.MemberEnd())
  {
    throw std::runtime_error("Json has no object with key " + objKey);
  }

  return strObj->value.GetDouble();
}  // getDoubleObjectOrThrow

}  // namespace parserutils
}  // namespace optilab
