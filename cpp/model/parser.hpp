//
// Copyright OptiLab 2019. All rights reserved.
//
// Class encapsulating a parser for a model object.
//

#pragma once

#include <string>

#include "data_structure/json/json.hpp"
#include "model/model_object.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS Parser {
 public:
  using SPtr = std::shared_ptr<Parser>;

 public:
  virtual ~Parser() = default;

  ModelObject::SPtr parse(const std::string& line);
  virtual ModelObject::SPtr parse(JSONValue::JsonObject& jsonObject) = 0;

  inline ModelObject::ModelObjectType getParsedObjectType() const { return pObjectType; }

 protected:
  /// Constructor creates a new model object parser for the give object type
  Parser(ModelObject::ModelObjectType objectType);

  virtual ModelObject::SPtr parseDescription(const std::string& description) = 0;

 private:
  /// Type of object parsed by this parser
  ModelObject::ModelObjectType pObjectType;
};

}  // namespace optilab
