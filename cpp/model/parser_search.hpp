//
// Copyright OptiLab 2019. All rights reserved.
//
// Class encapsulating a parser for a search model object.
//

#pragma once

#include "model/parser.hpp"

#include <string>
#include <vector>

#include "data_structure/json/json.hpp"
#include "model/model_object.hpp"
#include "model/primitive_search_object.hpp"

namespace optilab {

class SYS_EXPORT_CLASS ParserSearch : public Parser {
 public:
  using SPtr = std::shared_ptr<ParserSearch>;

 public:
  ParserSearch();

  virtual ~ParserSearch() = default;

  ModelObject::SPtr parse(JSONValue::JsonObject& jsonObject) override;

  /// Parses the base/primitive search object, i.e., combinator
  void parseSearchObject(JSONValue::JsonObject& jsonObject);

 protected:
  ModelObject::SPtr parseDescription(const std::string& description) override;

 private:
  /// List of SearchObjects
  std::vector<SearchObject::SPtr> pSearchObjectsList;

  /// Parses a BaseSearch object type and returns the correspondent SearchObject instance
  SearchObject::SPtr parseBaseSearchObject(const std::string& objId,
                                           JSONValue::JsonObject& jsonObject);

  /// Parses a BaseSearch object type and returns the correspondent SearchObject instance
  SearchObject::SPtr parseOrSearchObject(const std::string& objId,
                                         JSONValue::JsonObject& jsonObject);

  /// Parses a BaseSearch object type and returns the correspondent SearchObject instance
  SearchObject::SPtr parseAndSearchObject(const std::string& objId,
                                          JSONValue::JsonObject& jsonObject);
};

}  // namespace optilab
