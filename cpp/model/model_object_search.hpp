//
// Copyright OptiLab 2019. All rights reserved.
//
// Model search object.
// It holds the collection of basic search objects (i.e., combinators).
//

#pragma once

#include "model/model_object.hpp"

#include <cstdint>  // for int64_t
#include <memory>   // for std::shared_ptr
#include <string>
#include <unordered_map>

#include "model/primitive_search_object.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS ModelObjectSearch : public ModelObject {
 public:
  enum SearchType {
    GLOBAL = 0,
    LOCAL
  };

  using SPtr = std::shared_ptr<ModelObjectSearch>;

 public:
  ModelObjectSearch(SearchType searchType);

  ~ModelObjectSearch() = default;

  /// Returns the type of the search
  inline SearchType getSearchType() const { return pSearchType; }

  /// Sets the number of solutions to find.
  /// @note a negative value means "all solutions"
  inline void setNumberOfSolutionsToFind(int64_t numSol) { pNumSolutions = numSol; }

  /// Returns the number of solutions to find.
  /// @note a negative value means "all solutions"
  inline int64_t getNumberOfSolutionsToFind() const { return pNumSolutions; }

  /// Sets the id of the root SearchObject
  inline void setRootSearchObjectId(const std::string& rootObjectId)
  {
    pRootSearchObjectId = rootObjectId;
  }

  /// Returns the id of the root search object
  inline const std::string& getRootModelSearchObjectId() const { return pRootSearchObjectId; }

  /// Adds a search object to the internal map.
  /// @note throws std::invalid_argument if the given pointer is empty
  /// @note throws std::runtime_error if the map already contains an object with same id
  void addSearchObject(SearchObject::SPtr searchObject);

  /// Returns the pointer to the SearchObject with specified id.
  /// @note returns nullptr if no such id is found in the internal map of objects
  SearchObject::SPtr getSearchObject(const std::string& objectId) const;

 private:
  using SearchObjectMap = std::unordered_map<std::string, SearchObject::SPtr>;

 private:
  /// Type of the search
  SearchType pSearchType;

  /// Id of the root search object
  std::string pRootSearchObjectId;

  /// Number of solutions to find
  int64_t pNumSolutions;

  /// Map of ID to search object pointers
  SearchObjectMap pSearchObjectMap;
};

}  // namespace optilab
