//
// Copyright OptiLab 2019. All rights reserved.
//
// Class encapsulating a model object.
//

#pragma once

#include <memory>
#include <string>

#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS ModelObject {
 public:
  enum ModelObjectType : int {
    VARIABLE = 0,
    CONSTRAINT,
    PARAMETER,
    SEARCH,
    OBJECTIVE
  };

  using SPtr = std::shared_ptr<ModelObject>;

public:
  virtual ~ModelObject() = default;

  /// Returns the type of this object
  inline ModelObjectType getType() const { return pType; }

  /// Sets the string describing this object as parsed from the model
  inline void setObjectDescriptionString(const std::string& description)
  {
    pDescription = description;
  }

  /// Returns the model object description string of this object
  inline const std::string& getObjectDescriptionString() const { return pDescription; }

protected:
  ModelObject(ModelObjectType objectType) : pType(objectType) {}

private:
  /// Type of this object
  ModelObjectType pType;

  /// String describing this object as parsed from the model
  std::string pDescription;
};

}  // namespace optilab
