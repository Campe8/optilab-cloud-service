//
// Copyright OptiLab 2019. All rights reserved.
//
// Class encapsulating an objective model object.
//

#pragma once

#include <memory>

#include "model/model_object.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS ModelObjectObjective : public ModelObject {
 public:
  using SPtr = std::shared_ptr<ModelObjectObjective>;

public:
  ModelObjectObjective();

  ~ModelObjectObjective() = default;

  static bool isa(const ModelObject* modelObject);
  static ModelObjectObjective* cast(ModelObject* modelObject);
  static const ModelObjectObjective* cast(const ModelObject* modelObject);
};

}  // namespace optilab
