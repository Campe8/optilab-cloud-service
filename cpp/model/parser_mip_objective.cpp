#include "model/parser_mip_objective.hpp"

#include <cassert>
#include <utility>  // for std::pair

#include <spdlog/spdlog.h>

#include "model/model_constants.hpp"
#include "model/model_object_mip_objective.hpp"
#include "model/parser_utilities.hpp"

namespace {

std::pair<std::string, double> getVarCoeffPair(optilab::JSONValue::JsonObject& jsonObject)
{
  // Get variable identifier
  const std::string varId = optilab::parserutils::getStringObjectOrThrow(
      optilab::jsonmodel::VARIABLE_ID, jsonObject);
  const double varCoeff = optilab::parserutils::getDoubleObjectOrThrow(
      optilab::jsonmipmodel::VARIABLE_COEFF, jsonObject);

  return {varId, varCoeff};
}  // getVarCoeffPair

}  // namespace

namespace optilab {

ParserMIPObjective::ParserMIPObjective()
: Parser(ModelObject::ModelObjectType::OBJECTIVE)
{
}

ModelObject::SPtr ParserMIPObjective::parseDescription(const std::string&)
{
  ModelObject::SPtr ptr;
  return ptr;
}  // parseDescription

ModelObject::SPtr ParserMIPObjective::parse(JSONValue::JsonObject& jsonObject)
{
  ModelObjectMIPObjective::SPtr ptr = std::make_shared<ModelObjectMIPObjective>();

  // Get and set optimization direction
  const auto optDir = parserutils::getStringObjectOrThrow(jsonmipmodel::OPTIMIZATION_DIRECTION,
                                                          jsonObject);
  if (optDir == jsonmipmodel::OPTIMIZATION_DIRECTION_MAXIMIZE)
  {
    ptr->setOptimizationDirection(ModelObjectMIPObjective::OptimizationDirection::MAXIMIZE);
  }
  else
  {
    assert(optDir == jsonmipmodel::OPTIMIZATION_DIRECTION_MINIMIZE);
    ptr->setOptimizationDirection(ModelObjectMIPObjective::OptimizationDirection::MINIMIZE);
  }

  // Get and set coefficients list
  auto varCoeffListJson = jsonObject.FindMember(optilab::jsonmipmodel::OBJECTIVE_VAR_COEFF_LIST);
  if (varCoeffListJson == jsonObject.MemberEnd())
  {
    throw std::runtime_error("ParserMIPObjective - variable coefficients list not found");
  }

  for (auto& varCoeff : varCoeffListJson->value.GetArray())
  {
    const auto varCoeffPair = getVarCoeffPair(varCoeff);
    ptr->addVarCoeff(varCoeffPair.first, varCoeffPair.second);
  }

  return ptr;
}  // parse

}  // namespace optilab
