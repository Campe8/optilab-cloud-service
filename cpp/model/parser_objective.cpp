#include "model/parser_objective.hpp"

namespace optilab {

ParserObjective::ParserObjective()
: Parser(ModelObject::ModelObjectType::OBJECTIVE)
{
}

ModelObject::SPtr ParserObjective::parseDescription(const std::string& description)
{
  ModelObject::SPtr ptr;
  return ptr;
}  // parseDescription

ModelObject::SPtr ParserObjective::parse(JSONValue::JsonObject&)
{
  ModelObject::SPtr ptr;
  return ptr;
}  // parse

}  // namespace optilab
