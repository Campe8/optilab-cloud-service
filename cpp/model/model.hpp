//
// Copyright OptiLab 2019. All rights reserved.
//
// Class encapsulating an OptiLab model.
// This is a MODEL representation for OR frameworks.
// In other words, this model only encapsulates high level information
// about CP, MIP, LP, etc. models.
//

#pragma once

#include <cstddef>  // for std::size_t
#include <iostream>
#include <memory>  // for std::shared_ptr
#include <string>
#include <unordered_map>
#include <vector>

#include "data_structure/json/json.hpp"
#include "model_object.hpp"
#include "model_object_parameter.hpp"
#include "model_object_variable.hpp"
#include "parser.hpp"
#include "system/system_export_defs.hpp"

namespace optilab
{
class SYS_EXPORT_CLASS Model
{
 public:
  enum ClassType
  {
    CP = 0,
    IP,
    LP,
    MIP,
    UNDEF_TYPE
  };

  enum FrameworkPkg
  {
    OR_TOOLS = 0,
    GECODE,
    SCIP,
    UNDEF_PKG
  };

  using SPtr = std::shared_ptr<Model>;

  /// Map used for fast variable look-up.
  /// Each variable is identified by its unique string id
  using VariableMap =
      std::unordered_map<std::string, ModelObjectVariable::SPtr>;

  /// List of model objects
  using ModelObjectList = std::vector<ModelObject::SPtr>;

 public:
  Model();

  ~Model() = default;

  /// Loads a model from the given Json string.
  /// @note throws std::invalid_argument if the input json string is empty.
  /// @note throws std::invalid_argument if the input json is not properly
  /// structured
  void loadModelFromJson(const std::string& jsonModel);

  /// Returns true if the model has a legacy format (e.g., MPS) rather than
  /// standard OptiLab Json model format
  bool hasLegacyModelFormat() const;

  /// Returns the model format
  inline const std::string& getModelFormat() const { return pModelFormat; }

  /// Returns the model data
  inline const std::string& getModelData() const { return pModelData; }

  /// Sets the legacy model descriptor,
  /// e.g., the path to the file describing the model
  inline void setLegacyModelDescriptor(const std::string& descriptor)
  {
    pModelDescriptor = descriptor;
    pUseFileDescriptorForLegacyModels = true;
  }

  /// Returns the model descriptor.
  /// @note this is non-empty only if the model has a legacy model format.
  /// See also the "hasLegacyModelFormat(...)" method
  inline const std::string& getModelDescriptor() const
  {
    return pModelDescriptor;
  }

  /// Returns true if the model (legacy) is stored in a file rather than being
  /// copied verbatim into the JSON. Either way, both descriptors (path to file
  /// or model) can be retrieved with the function "getModelDescriptor(...)"
  inline bool useModelFileDescriptor() const
  {
    return pUseFileDescriptorForLegacyModels;
  }

  inline const std::string& getModelName() const { return pModelName; }

  inline const VariableMap& getVariableMap() const { return pVariableMap; }

  /// Returns the class type of this model
  inline ClassType getModelClassType() const { return pFrameworkClassType; }

  /// Returns the list of search objects
  const ModelObjectList& getSearchList() const;

  /// Returns the list of model objects in this model of specified type
  const ModelObjectList& getObjectList(
      ModelObject::ModelObjectType objectType) const;

 private:
  struct EnumClassHash
  {
    template <typename T>
    std::size_t operator()(T t) const
    {
      return static_cast<std::size_t>(t);
    }
  };

  /// Map of all the objects in the model
  using ModelObjectMap = std::unordered_map<ModelObject::ModelObjectType,
                                            ModelObjectList, EnumClassHash>;

  /// Map used for fast parameter look-up.
  /// Each parameter is identified by its unique string id
  using ParameterMap =
      std::unordered_map<std::string, ModelObjectParameter::SPtr>;

  /// Map of parsers addressed by the framework they are supposed to parse for
  using FrameworkParserMap =
      std::unordered_map<ClassType, Parser::SPtr, EnumClassHash>;

  /// Map of model object parsers
  using ModelObjectParserMap =
      std::unordered_map<ModelObject::ModelObjectType,
                         std::shared_ptr<FrameworkParserMap>, EnumClassHash>;

 private:
  /// Type of the framework's block this model has to run on
  ClassType pFrameworkClassType{ClassType::UNDEF_TYPE};

  /// Package running the framewor.
  /// @note actual packages and software running this model
  FrameworkPkg pFrameworkPackage{FrameworkPkg::UNDEF_PKG};

  /// Name of the model
  std::string pModelName;

  /// Format of this model
  std::string pModelFormat;

  /// Variables for legacy model formats: model data
  std::string pModelData;

  /// Variables for legacy model formats
  std::string pModelDescriptor;

  /// Indicates if non OptiLab models need to be loaded from a file or
  /// are copied verbatim in the JSON model
  bool pUseFileDescriptorForLegacyModels{true};

  /// Map of model objects
  mutable ModelObjectMap pModelObjectMap;

  /// Map of model object parser parser
  ModelObjectParserMap pParserMap;

  /// Map of all the variables in the model
  VariableMap pVariableMap;

  /// Map of all parameters in the model
  ParameterMap pParameterMap;

  /// Utility function: initialize this model.
  /// For example, initialize all the model object parsers
  void initializeModel();

  /// Utility function: load Json model format, i.e., model described in Json
  /// format
  void loadJsonModelFormat(JSON::SPtr modelJSON);

  /// Utility function: load any other non-json/legacy model format
  void loadLegacyModelFormat(JSON::SPtr modelJSON);

  /// Utility function: returns the parser to parse the given object type.
  /// @note the parser for the same model object type can be different w.r.t.
  /// the model package
  Parser* getModelObjectParser(ModelObject::ModelObjectType objectType);

  /// Utility function: adds the given object to the model map
  void addModelObjectToModel(const ModelObject::SPtr& modelObject);

  /// Utility function: adds a model object variable to the model.
  /// @note throws std::runtime_error if a variable with same identifier is
  /// already been registered
  void addVariableToModel(const ModelObject::SPtr& modelObject);

  /// Utility function: adds a model object variable to the model.
  /// @note throws std::runtime_error if a parameter with same identifier is
  /// already been registered
  void addParameterToModel(const ModelObject::SPtr& modelObject);

  /// Utility function: sets the framework's class type
  void setFrameworkClassType(const std::string& classType);

  /// Utility function: sets the package type
  void setPackageType(const std::string& packageType);

  /// Parses the list of given model object types in the given json object and
  /// adds them to the current model
  void parseJsonListOfModelObjectTypesAndAddToModel(
      ModelObject::ModelObjectType objectType,
      JSONValue::JsonObject& objectJsonList);

  /// Parses the JSON search specification and creates a search model object
  /// adding it to the model
  void parseJsonSearchAndAddSearchObjectToModel(
      JSONValue::JsonObject& searchList,
      JSONValue::JsonObject& searchObjectList);

  /// Utility function: resets the internal state of the model
  void resetModelState();
};

}  // namespace optilab
