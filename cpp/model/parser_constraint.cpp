#include "model/parser_constraint.hpp"

#include <vector>

#include "model/model_constants.hpp"
#include "model/model_object_constraint.hpp"
#include "model/model_object_intension_constraint.hpp"

namespace {

std::string getConstraintSemanticFromJson(optilab::JSONValue::JsonObject& jsonObject)
{
  auto semantic = jsonObject.FindMember(optilab::jsonmodel::CONSTRAINT_SEMANTIC);
  if (semantic == jsonObject.MemberEnd())
  {
    throw std::runtime_error("ParserConstraint - missing constraint semantic");
  }

  return semantic->value.GetString();
}  // getConstraintSemanticFromJson

std::string getConstraintTypeFromJson(optilab::JSONValue::JsonObject& jsonObject)
{
  auto type = jsonObject.FindMember(optilab::jsonmodel::CONSTRAINT_TYPE);
  if (type == jsonObject.MemberEnd())
  {
    throw std::runtime_error("ParserConstraint - missing constraint type");
  }

  return type->value.GetString();
}  // getConstraintTypeFromJson

std::string getConstraintNameFromJson(optilab::JSONValue::JsonObject& jsonObject)
{
  auto name = jsonObject.FindMember(optilab::jsonmodel::CONSTRAINT_NAME);
  if (name == jsonObject.MemberEnd())
  {
    throw std::runtime_error("ParserConstraint - missing constraint name");
  }

  return name->value.GetString();
}  // getConstraintNameFromJson

std::string getConstraintPropagationTypeFromJson(optilab::JSONValue::JsonObject& jsonObject)
{
  auto propType = jsonObject.FindMember(optilab::jsonmodel::CONSTRAINT_PROPAGATION_TYPE);
  if (propType == jsonObject.MemberEnd())
  {
    throw std::runtime_error("ParserConstraint - missing constraint propagation type");
  }

  return propType->value.GetString();
}  // getConstraintPropagationTypeeFromJson

void addArgumentsFromJson(optilab::ModelObjectIntensionConstraint* con,
                          optilab::JSONValue::JsonObject& jsonObject)
{
  auto argsList = jsonObject.FindMember(optilab::jsonmodel::CONSTRAINT_ARGUMENTS_LIST);
  if (argsList == jsonObject.MemberEnd())
  {
    throw std::runtime_error("ParserConstraint - missing arguments list");
  }

  for (auto& listIter : argsList->value.GetArray())
  {
    std::vector<std::string> arg;
    for (auto& obj : listIter.GetArray())
    {
      arg.push_back(obj.GetString());
    }
    con->addArgument(arg);
  }
}  // addArgumentsFromJson

optilab::ModelObject::SPtr buildExtensionConstraint(optilab::JSONValue::JsonObject&)
{
  /// TODO
  optilab::ModelObject::SPtr ptr;
  return ptr;
}  // buildExtensionConstraint

optilab::ModelObject::SPtr buildGlobalConstraint(optilab::JSONValue::JsonObject&)
{
  /// TODO
  optilab::ModelObject::SPtr ptr;
  return ptr;
}  // buildExtensionConstraint

optilab::ModelObject::SPtr buildIntensionConstraint(optilab::JSONValue::JsonObject& jsonObject)
{
  optilab::ModelObjectIntensionConstraint* con = new optilab::ModelObjectIntensionConstraint();

  con->setType(getConstraintTypeFromJson(jsonObject));
  con->setName(getConstraintNameFromJson(jsonObject));
  con->setPropagationType(getConstraintPropagationTypeFromJson(jsonObject));

  addArgumentsFromJson(con, jsonObject);

  // Wrap the shared pointer around the constraint and return
  optilab::ModelObjectConstraint::SPtr ptr(con);
  return ptr;
}  // buildExtensionConstraint

}  // namespace

namespace optilab {

ParserConstraint::ParserConstraint()
: Parser(ModelObject::ModelObjectType::CONSTRAINT)
{
}

ModelObject::SPtr ParserConstraint::parseDescription(const std::string& description)
{
  // @note DEPRECATED
  ModelObject::SPtr ptr;
  return ptr;
}  // parseDescription

ModelObject::SPtr ParserConstraint::parse(JSONValue::JsonObject& jsonObject)
{
  // Get and create the constraint object according to its semantic
  const auto semantic = getConstraintSemanticFromJson(jsonObject);
  if (semantic.empty())
  {
    throw std::runtime_error("ParserConstraint - empty constraint semantic");
  }

  if (semantic == optilab::jsonmodel::CONSTRAINT_SEMANTIC_EXTENSION)
  {
    return buildExtensionConstraint(jsonObject);
  }
  else if (semantic == optilab::jsonmodel::CONSTRAINT_SEMANTIC_GLOBAL)
  {
    return buildGlobalConstraint(jsonObject);
  }
  else if (semantic == optilab::jsonmodel::CONSTRAINT_SEMANTIC_INTENSION)
  {
    return buildIntensionConstraint(jsonObject);
  }
  else
  {
    throw std::runtime_error("ParserConstraint - invalid constraint semantic: " + semantic);
  }
}  // parse

}  // namespace optilab
