//
// Copyright OptiLab 2019. All rights reserved.
//
// Utility functions for parser
//

#pragma once

#include <string>
#include <vector>

#include "data_structure/json/json.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace parserutils {

/// @note throws std::runtime_error if no such object is found
SYS_EXPORT_FCN std::string getStringObjectOrThrow(const std::string& objKey,
                                                  optilab::JSONValue::JsonObject& jsonObject);

/// Returns the double object with given key from the given Json.
/// @note throws std::runtime_error if no such object is found
SYS_EXPORT_FCN double getDoubleObjectOrThrow(const std::string& objKey,
                                             optilab::JSONValue::JsonObject& jsonObject);

}  // namespace parserutils
}  // namespace optilab
