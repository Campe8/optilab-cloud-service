#include "model/model.hpp"

#include <cassert>
#include <fstream>
#include <stdexcept>  // for std::runtime_error
#include <sstream>
#include <string>
#include <utility>    // for std::pair

#include <spdlog/spdlog.h>

#include "model/model_constants.hpp"
#include "model/parser_search.hpp"
#include "model/parser_constraint.hpp"
#include "model/parser_mip_constraint.hpp"
#include "model/parser_mip_objective.hpp"
#include "model/parser_objective.hpp"
#include "model/parser_parameter.hpp"
#include "model/parser_utilities.hpp"
#include "model/parser_variable.hpp"

namespace optilab {

Model::Model()
{
  initializeModel();
}

void Model::initializeModel()
{

  // === Constraint parsers === //
  auto constraintParsersMap = std::make_shared<FrameworkParserMap>();
  (*constraintParsersMap)[ClassType::CP] = std::make_shared<ParserConstraint>();
  (*constraintParsersMap)[ClassType::MIP] = std::make_shared<ParserMIPConstraint>();

  // === Objective parsers === //
  auto objectiveParsersMap = std::make_shared<FrameworkParserMap>();
  (*objectiveParsersMap)[ClassType::CP] = std::make_shared<ParserObjective>();
  (*objectiveParsersMap)[ClassType::MIP] = std::make_shared<ParserMIPObjective>();

  // === Parameter parsers === //
  auto parameterParsersMap = std::make_shared<FrameworkParserMap>();
  (*parameterParsersMap)[ClassType::CP] = std::make_shared<ParserParameter>();
  (*parameterParsersMap)[ClassType::MIP] = std::make_shared<ParserParameter>();

  // === Search parsers === //
  auto searchParsersMap = std::make_shared<FrameworkParserMap>();
  (*searchParsersMap)[ClassType::CP] = std::make_shared<ParserSearch>();
  (*searchParsersMap)[ClassType::MIP] = std::make_shared<ParserSearch>();

  // === Variable parsers === //
  auto variableParsersMap = std::make_shared<FrameworkParserMap>();
  (*variableParsersMap)[ClassType::CP] = std::make_shared<ParserVariable>();
  (*variableParsersMap)[ClassType::MIP] = std::make_shared<ParserVariable>();

  pParserMap[ModelObject::ModelObjectType::CONSTRAINT] = constraintParsersMap;
  pParserMap[ModelObject::ModelObjectType::OBJECTIVE] = objectiveParsersMap;
  pParserMap[ModelObject::ModelObjectType::PARAMETER] = parameterParsersMap;
  pParserMap[ModelObject::ModelObjectType::SEARCH] = searchParsersMap;
  pParserMap[ModelObject::ModelObjectType::VARIABLE] = variableParsersMap;
}  // initializeModel

void Model::resetModelState()
{
  pFrameworkClassType = ClassType::UNDEF_TYPE;
  pFrameworkPackage = FrameworkPkg::UNDEF_PKG;
  pModelName.clear();
  pModelObjectMap.clear();
  pVariableMap.clear();
  pParameterMap.clear();
}  // resetModelState

bool Model::hasLegacyModelFormat() const
{
  return !pModelFormat.empty() &&
      pModelFormat != jsonmodel::OPTILAB_MODEL_FORMAT;
}  // hasLegacyModelFormat

void Model::loadModelFromJson(const std::string& jsonModel)
{
  if (jsonModel.empty())
  {
    throw std::invalid_argument("Model - Empty input JSON model");
  }

  // Reset the state of the model
  resetModelState();

  JSON::SPtr modelJSON = std::make_shared<JSON>(jsonModel);

  // Get and set the framework the model should run in
  if (!modelJSON->hasObject(jsonmodel::CLASS_TYPE))
  {
    throw std::runtime_error("Model - Missing block type specification in model");
  }
  auto& frameworkClass = modelJSON->getObject(jsonmodel::CLASS_TYPE);
  setFrameworkClassType(frameworkClass.GetString());

  // Get and set the package the model should run in
  if (!modelJSON->hasObject(jsonmodel::PACKAGE))
  {
    throw std::runtime_error("Missing package specification in model");
  }
  auto& package = modelJSON->getObject(jsonmodel::PACKAGE);
  setPackageType(package.GetString());

  // Get and set the model name
  if (!modelJSON->hasObject(jsonmodel::MODEL_NAME))
  {
    throw std::runtime_error("Model - Missing model name specification in model");
  }

  auto& modelName = modelJSON->getObject(jsonmodel::MODEL_NAME);
  pModelName = modelName.GetString();
  if (pModelName.empty())
  {
    throw std::runtime_error("Model - Empty model name specification in model");
  }

  // Determine whether this is a verbatim copy of a model in a specific format.
  // If so, store model and data and return.
  // Otherwise, parse the entries as Json Objects
  if (modelJSON->hasObject(jsonmodel::MODEL_FORMAT))
  {
    auto& modelFormat = modelJSON->getObject(jsonmodel::MODEL_FORMAT);
    pModelFormat = modelFormat.GetString();

    // Check whether or not the model format is specified and it is non-OptiLab (i.e., non legacy)
    if (pModelFormat != jsonmodel::OPTILAB_MODEL_FORMAT)
    {
      loadLegacyModelFormat(modelJSON);
    }
    else
    {
      loadJsonModelFormat(modelJSON);
    }
  }
  else
  {
    // It is assumed that the model has a Json/OptiLab description.
    // Set OptiLab as standard/non-legacy model format
    pModelFormat = jsonmodel::OPTILAB_MODEL_FORMAT;
    loadJsonModelFormat(modelJSON);
  }
}  // loadModelFromJson

void Model::loadLegacyModelFormat(JSON::SPtr modelJSON)
{
  // Model data
  if (modelJSON->hasObject(jsonmodel::MODEL_DATA))
  {
    auto& modelData = modelJSON->getObject(jsonmodel::MODEL_DATA);
    pModelData = modelData.GetString();
  }

  // A model can be represented as a string (verbatim copy of the model into the JSON wrapper),
  // or as a path to the file containing the model in the user's environment
  pUseFileDescriptorForLegacyModels = true;
  if (modelJSON->hasObject(jsonmodel::USE_FILE_DESCRIPTORS))
  {
    auto& useFilesDescriptorsJson = modelJSON->getObject(jsonmodel::USE_FILE_DESCRIPTORS);
    pUseFileDescriptorForLegacyModels = useFilesDescriptorsJson.GetBool();
  }

  if (!pUseFileDescriptorForLegacyModels)
  {
    spdlog::warn("Model - loadLegacyModelFormat: using verbatim model in JSON is deprecated");

    // Model descriptor.
    // @note this way of describing a model (verbatim) is DEPRECATED.
    // Use file descriptors instead
    if (!modelJSON->hasObject(jsonmodel::MODEL_DESCRIPTOR))
    {
      const std::string errMsg = "Model - loadLegacyModelFormat: missing model descriptor \"" +
          std::string(jsonmodel::MODEL_DESCRIPTOR) + "\"";
      spdlog::error(errMsg);
      throw std::runtime_error(errMsg);
    }

    auto& modelDescriptor = modelJSON->getObject(jsonmodel::MODEL_DESCRIPTOR);
    pModelDescriptor = modelDescriptor.GetString();
  }
  else
  {
    // Get the file describing the model
    if (!modelJSON->hasObject(jsonmodel::MODEL_FILE_DESCRIPTOR))
    {
      const std::string errMsg = "Model - loadLegacyModelFormat: missing model file descriptor " +
          std::string(jsonmodel::MODEL_FILE_DESCRIPTOR);
      spdlog::error(errMsg);
      throw std::runtime_error(errMsg);
    }

    auto& modelFileDescriptor = modelJSON->getObject(jsonmodel::MODEL_FILE_DESCRIPTOR);
    pModelDescriptor = modelFileDescriptor.GetString();
  }
}  // loadLegacyModelFormat

void Model::loadJsonModelFormat(JSON::SPtr modelJSON)
{
  // Get the list of variable objects in the model
  if (!modelJSON->hasObject(jsonmodel::VARIABLES_LIST))
  {
    throw std::runtime_error("Model - Missing variables list specification in model");
  }
  parseJsonListOfModelObjectTypesAndAddToModel(ModelObject::ModelObjectType::VARIABLE,
                                               modelJSON->getObject(jsonmodel::VARIABLES_LIST));

  // Get the list of constraint objects in the model
  if (modelJSON->hasObject(jsonmodel::CONSTRAINTS_LIST))
  {
    parseJsonListOfModelObjectTypesAndAddToModel(ModelObject::ModelObjectType::CONSTRAINT,
                                                 modelJSON->getObject(jsonmodel::CONSTRAINTS_LIST));
  }

  // Get the list of objective objects in the model
  if (modelJSON->hasObject(jsonmodel::OBJECTIVES_LIST))
  {
    parseJsonListOfModelObjectTypesAndAddToModel(ModelObject::ModelObjectType::OBJECTIVE,
                                                 modelJSON->getObject(jsonmodel::OBJECTIVES_LIST));
  }

  // TODO add parameter parser

  // Search is mandatory only for CP frameworks
  if (pFrameworkClassType == ClassType::CP)
  {
    // Get the list of search objects in the model
    if (!modelJSON->hasObject(jsonmodel::SEARCH_LIST))
    {
      throw std::runtime_error("Model - Missing search list specification in model");
    }

    if (!modelJSON->hasObject(jsonmodel::SEARCH_OBJECTS_LIST))
    {
      throw std::runtime_error("Model - Missing search object list specification in model");
    }
    parseJsonSearchAndAddSearchObjectToModel(modelJSON->getObject(jsonmodel::SEARCH_LIST),
                                             modelJSON->getObject(jsonmodel::SEARCH_OBJECTS_LIST));
  }
  else
  {
    // Otherwise search is optional
    if (modelJSON->hasObject(jsonmodel::SEARCH_LIST))
    {
      parseJsonListOfModelObjectTypesAndAddToModel(ModelObject::ModelObjectType::SEARCH,
                                                   modelJSON->getObject(jsonmodel::SEARCH_LIST));
    }

  }
}  // loadJsonModelFormat

void Model::parseJsonSearchAndAddSearchObjectToModel(JSONValue::JsonObject& searchList,
                                                     JSONValue::JsonObject& searchObjectList)
{
  auto jsonParser = getModelObjectParser(ModelObject::ModelObjectType::SEARCH);
  ParserSearch* parser = static_cast<ParserSearch*>(jsonParser);

  /// First parse the list of search object
  for (JSONValue::JsonObject& iter : searchObjectList.GetArray())
  {
    parser->parseSearchObject(iter);
  }

  /// Then create search objects with the initialized parser containing all the search objects
  for (JSONValue::JsonObject& iter : searchList.GetArray())
  {
    auto object = parser->parse(iter);

    // Add the object to the model
    addModelObjectToModel(object);
  }
}  // parseJsonSearchAndAddSearchObjectToModel

void Model::parseJsonListOfModelObjectTypesAndAddToModel(ModelObject::ModelObjectType objectType,
                                                         JSONValue::JsonObject& objectJsonList)
{
  // Get the parser for the given object type.
  // @note the returned parser is "framework-aware"
  auto jsonParser = getModelObjectParser(objectType);
  for (JSONValue::JsonObject& iter : objectJsonList.GetArray())
  {
    // Parse the json and get the corresponding model object
    auto object = jsonParser->parse(iter);

    // Add the object to the model
    addModelObjectToModel(object);
  }
}  // parseJsonListOfModelObjectTypesAndAddToModel

void Model::setFrameworkClassType(const std::string& classType)
{
  if (classType == model::FRAMEWORK_CP)
  {
    pFrameworkClassType = ClassType::CP;
  }
  else if (classType == model::FRAMEWORK_LP)
  {
    pFrameworkClassType = ClassType::LP;
  }
  else if (classType == model::FRAMEWORK_IP)
  {
    pFrameworkClassType = ClassType::IP;
  }
  else if (classType == model::FRAMEWORK_MIP)
  {
    pFrameworkClassType = ClassType::MIP;
  }
  else
  {
    throw std::runtime_error(std::string("Model - Unrecognized framework class type: ") +
                             std::to_string(static_cast<int>(pFrameworkClassType)));
  }
}  // setFrameworkClassType

void Model::setPackageType(const std::string& packageType)
{
  if (packageType == model::PARAMETER_PKG_OR_TOOLS)
  {
    pFrameworkPackage = FrameworkPkg::OR_TOOLS;
  }
  else if (packageType == model::PARAMETER_PKG_GECODE)
  {
    pFrameworkPackage = FrameworkPkg::GECODE;
  }
  else if (packageType == model::PARAMETER_PKG_SCIP)
  {
    pFrameworkPackage = FrameworkPkg::SCIP;
  }
  else
  {
    throw std::runtime_error(std::string("Model - Unrecognized package: ") + packageType);
  }
}  // setPackageType

Parser* Model::getModelObjectParser(ModelObject::ModelObjectType objectType)
{
  if (pParserMap.find(objectType) == pParserMap.end())
  {
    throw std::runtime_error("Model - Parser not found");
  }

  // Get the parser the specific framework
  auto frameworkParser = pParserMap.at(objectType);
  if (frameworkParser->find(pFrameworkClassType) == frameworkParser->end())
  {
    throw std::runtime_error("Model - Parser for specified framework not found");
  }

  return frameworkParser->at(pFrameworkClassType).get();
}  // getModelObjectParser

const Model::ModelObjectList& Model::getObjectList(ModelObject::ModelObjectType objectType) const
{
  const auto& it = pModelObjectMap.find(objectType);
  if (it == pModelObjectMap.end())
  {
    throw std::runtime_error("Model - cannot find list of objects of specified type");
  }

  return it->second;
}  // getObjectList

void Model::addModelObjectToModel(const ModelObject::SPtr& modelObject)
{
  assert(modelObject);

  const auto objectType = modelObject->getType();
  switch (objectType) {
    case ModelObject::ModelObjectType::VARIABLE:
    {
      addVariableToModel(modelObject);
      break;
    }
    case ModelObject::ModelObjectType::PARAMETER:
    {
      addParameterToModel(modelObject);
      break;
    }
    default:
    {
      break;
    }
  }

  // Add the object to the object map
  pModelObjectMap[objectType].push_back(modelObject);
}  // addModelObjectToModel

void Model::addVariableToModel(const ModelObject::SPtr& modelObject)
{
  auto variable = ModelObjectVariable::cast(modelObject.get());
  assert(variable);

  const auto& variableName = variable->getVariableName();
  if (pVariableMap.find(variableName) != pVariableMap.end())
  {
    throw std::runtime_error(std::string("Model - Variable with id ") + variableName +
                             std::string(" already present in the map"));
  }

  pVariableMap[variableName] = std::dynamic_pointer_cast<ModelObjectVariable>(modelObject);
}  // addVariableToModel

void Model::addParameterToModel(const ModelObject::SPtr& modelObject)
{
  auto parameter = ModelObjectParameter::cast(modelObject.get());
  assert(parameter);

  const auto& parameterName = parameter->getParameterName();
  if (pVariableMap.find(parameterName) != pVariableMap.end())
  {
    throw std::runtime_error(std::string("Model - Parameter with id ") + parameterName +
                             std::string(" already present in the map"));
  }

  pParameterMap[parameterName] = std::dynamic_pointer_cast<ModelObjectParameter>(modelObject);
}  // addParameterToModel

const Model::ModelObjectList& Model::getSearchList() const
{
  if (pModelObjectMap.find(ModelObject::ModelObjectType::SEARCH) == pModelObjectMap.end())
  {
    return pModelObjectMap[ModelObject::ModelObjectType::SEARCH];
  }
  return pModelObjectMap.at(ModelObject::ModelObjectType::SEARCH);
}  // getSearchList

}  // namespace optilab
