//
// Copyright OptiLab 2019. All rights reserved.
//
// Class encapsulating a parser for a variable model object.
//

#pragma once

#include "model/parser.hpp"

#include <string>

#include "data_structure/json/json.hpp"
#include "model/model_object.hpp"

namespace optilab {

class SYS_EXPORT_CLASS ParserVariable : public Parser {
 public:
  using SPtr = std::shared_ptr<ParserVariable>;

 public:
  ParserVariable();
  virtual ~ParserVariable() = default;

  ModelObject::SPtr parse(JSONValue::JsonObject& jsonObject) override;

 protected:
  ModelObject::SPtr parseDescription(const std::string& description) override;
};

}  // namespace optilab
