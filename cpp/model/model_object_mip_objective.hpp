//
// Copyright OptiLab 2019. All rights reserved.
//
// Class encapsulating a MIP objective model object.
//

#pragma once

#include <memory>  // // for std::shared_ptr
#include <string>
#include <utility>  // for std::pair
#include <vector>

#include "model/model_object_objective.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS ModelObjectMIPObjective : public ModelObjectObjective {
 public:
  enum OptimizationDirection {
    MINIMIZE = 0,
    MAXIMIZE
  };

  using SPtr = std::shared_ptr<ModelObjectMIPObjective>;

  /// Variable coefficients
  using VarCoeff = std::pair<std::string, double>;
  using VarCoeffList = std::vector<VarCoeff>;
  using Iter = VarCoeffList::iterator;

public:
  ModelObjectMIPObjective() = default;

  ~ModelObjectMIPObjective() = default;

  inline void setOptimizationDirection(OptimizationDirection dir) { pOptimizationDir = dir; }
  inline OptimizationDirection getOptimizationDirection() const { return pOptimizationDir; }

  inline void addVarCoeff(const std::string& varId, double coeff)
  {
    pCoefficientsList.push_back({varId, coeff});
  }
  inline const VarCoeffList& getVarCoeffList() const { return pCoefficientsList; }

  /// Iterators used by the for-range loop
  Iter begin() { return pCoefficientsList.begin(); }
  Iter end() { return pCoefficientsList.end(); }

private:
  /// Direction of the optimization
  OptimizationDirection pOptimizationDir;

  /// List of variable/coefficients pairs
  VarCoeffList pCoefficientsList;
};

}  // namespace optilab
