#include "model/model_object_variable.hpp"

#include <algorithm>  // for std::sort
#include <stdexcept>  // for std::runtime_error

namespace optilab {

ModelObjectVariable::ModelObjectVariable()
: ModelObject(ModelObjectType::VARIABLE)
{
  pDomValues.push_back(std::numeric_limits<double>::infinity());
  pDomValues.push_back(-std::numeric_limits<double>::infinity());
}

bool ModelObjectVariable::isa(const ModelObject* modelObject)
{
  return modelObject &&
      (modelObject->getType() == ModelObjectType::VARIABLE);
}  // isa

ModelObjectVariable* ModelObjectVariable::cast(ModelObject* modelObject)
{
  if (!isa(modelObject)) return nullptr;
  return static_cast<ModelObjectVariable*>(modelObject);
}  // cast

const ModelObjectVariable* ModelObjectVariable::cast(const ModelObject* modelObject)
{
  if (!isa(modelObject)) return nullptr;
  return static_cast<const ModelObjectVariable*>(modelObject);
}  // cast

int ModelObjectVariable::getDimension(const std::size_t idx)
{
  if (idx >= pVarDims.size())
  {
    throw std::runtime_error(std::string("Invalid variable dimension index: ") +
                             std::to_string(idx));
  }

  return pVarDims[idx];
}  // getDimension

void ModelObjectVariable::setDomainBounds(double lowerBound, double upperBound)
{
  pDomValues.front() = lowerBound;
  pDomValues.back() = upperBound;
}  // setDomainBounds

bool ModelObjectVariable::isUpperBoundInf() const
{
  static double inf = std::numeric_limits<double>::infinity();
  return getUpperBound() == inf;
}  // isUpperBoundInf

bool ModelObjectVariable::isLowerBoundInf() const
{
   static double inf = std::numeric_limits<double>::infinity();
   return getLowerBound() == -inf;
}  // isLowerBoundInf

void ModelObjectVariable::setListOfDomainValues(const std::vector<double>& valueList)
{
  pDomValues = valueList;
  std::sort(pDomValues.begin(), pDomValues.end());
} // setListOfDomainValues

}  // namespace optilab

