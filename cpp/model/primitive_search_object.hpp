//
// Copyright OptiLab 2019. All rights reserved.
//
// Set of primitive search objects.
//

#pragma once

#include <cstddef>  // for std::size_t
#include <memory>   // for std::shared_ptr
#include <string>
#include <vector>

#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS SearchObject {
public:
  enum SearchObjectType {
    BASE_SEARCH = 0,
    OR,
    AND
  };

  using SPtr = std::shared_ptr<SearchObject>;

public:
  /// Returns the Id of this object
  inline const std::string& getObjectId() const { return pObjectId; }

  /// Returns the search object type of this object
  inline SearchObjectType getSearchObjectType() const { return pObjectType; }

  virtual ~SearchObject() = default;

protected:
  SearchObject(const std::string& objectId, SearchObjectType objectType)
    : pObjectId(objectId), pObjectType(objectType) {}

private:
  const std::string pObjectId;
  SearchObjectType pObjectType;
};

class SYS_EXPORT_CLASS BaseSearchObject : public SearchObject {
 public:
  /// A branching variable is described by its id and the vector subscription indices in case
  /// of n-dimensional variables
  using BranchingVar = std::pair<std::string, std::vector<int>>;

  /// List of branching variables
  using BranchingVarsList = std::vector<BranchingVar>;

  /// Iterator used for range-based for-loop
  using ObjectIter = BranchingVarsList::iterator;

 public:
  BaseSearchObject(const std::string& objectId)
   : SearchObject(objectId, SearchObjectType::BASE_SEARCH), pRandomSeed(0.0) {}

  inline void addBranchingVariable(const BranchingVar& branchingVar)
  {
    pBranchingVariableList.push_back(branchingVar);
  }

  inline const BranchingVarsList& getBranchingVarList() const { return pBranchingVariableList; }

  inline void setVariableSelectionStrategy(const std::string& varStrategy)
  {
    pVarSelectionStrategy = varStrategy;
  }

  inline void setValueSelectionStrategy(const std::string& valStrategy)
  {
    pValSelectionStrategy = valStrategy;
  }

  inline const std::string& getVariableSelectionStrategy() const { return pVarSelectionStrategy; }
  inline const std::string& getValueSelectionStrategy() const { return pValSelectionStrategy; }

  inline void setRandomSeed(std::size_t seed) { pRandomSeed = seed; }
  inline std::size_t getRandomSeed() const { return pRandomSeed; }

  ObjectIter begin() { return pBranchingVariableList.begin(); }
  ObjectIter end() { return pBranchingVariableList.end(); }

 private:
  /// Variable selection strategy
  std::string pVarSelectionStrategy;

  /// Value selection strategy
  std::string pValSelectionStrategy;

  /// Seed to be used for random var/val selection strategies
  std::size_t pRandomSeed;

  /// List of branching variables
  BranchingVarsList pBranchingVariableList;
};

class SYS_EXPORT_CLASS OrObject : public SearchObject {
 public:
  OrObject(const std::string& objectId) : SearchObject(objectId, SearchObjectType::OR) {}

  /// Add an object id to the list of search objects of the OR combinator
  inline void addSearchObjectIdToList(const std::string& objectId)
  {
    pSearchObjectList.push_back(objectId);
  }

  /// Returns the list of object ids of the OR combinator
  inline const std::vector<std::string>& getSearchObjectList() const { return pSearchObjectList; }

 private:
  std::vector<std::string> pSearchObjectList;
};

class SYS_EXPORT_CLASS AndObject : public SearchObject {
 public:
  AndObject(const std::string& objectId) : SearchObject(objectId, SearchObjectType::AND) {}

  /// Add an object id to the list of search objects of the AND combinator
  inline void addSearchObjectIdToList(const std::string& objectId)
  {
    pSearchObjectList.push_back(objectId);
  }

  /// Returns the list of object ids of the AND combinator
  inline const std::vector<std::string>& getSearchObjectList() const { return pSearchObjectList; }

 private:
  std::vector<std::string> pSearchObjectList;
};

}  // namespace optilab
