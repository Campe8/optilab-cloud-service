#include "model/parser_mip_constraint.hpp"

#include <utility>  // for std::pair

#include <spdlog/spdlog.h>

#include "model/model_constants.hpp"
#include "model/model_object_mip_constraint.hpp"
#include "model/parser_utilities.hpp"

namespace {

std::pair<std::string, double> getVarCoeffPair(optilab::JSONValue::JsonObject& jsonObject)
{
  // Get variable identifier
  const std::string varId = optilab::parserutils::getStringObjectOrThrow(
      optilab::jsonmodel::VARIABLE_ID, jsonObject);
  const double varCoeff = optilab::parserutils::getDoubleObjectOrThrow(
      optilab::jsonmipmodel::VARIABLE_COEFF, jsonObject);

  return {varId, varCoeff};
}  // getVarCoeffPair

}  // namespace

namespace optilab {

ParserMIPConstraint::ParserMIPConstraint()
: Parser(ModelObject::ModelObjectType::CONSTRAINT)
{
}

ModelObject::SPtr ParserMIPConstraint::parseDescription(const std::string&)
{
  ModelObject::SPtr ptr;
  return ptr;
}  // parseDescription

ModelObject::SPtr ParserMIPConstraint::parse(JSONValue::JsonObject& jsonObject)
{
  ModelObjectMIPConstraint::SPtr ptr = std::make_shared<ModelObjectMIPConstraint>();

  // Get and set constraint id.
  // @note constraint id is not required
  try
  {
    const auto cid = parserutils::getStringObjectOrThrow(jsonmodel::CONSTRAINT_ID, jsonObject);
    ptr->setConstraintId(cid);
  }
  catch(...)
  {
    spdlog::warn("ParserMIPConstraint - constraint ID not set");
  }

  // Get and set constraint lower and upper bounds
  const auto clb = parserutils::getDoubleObjectOrThrow(
      jsonmipmodel::CONSTRAINT_LOWER_BOUND, jsonObject);
  const auto cub = parserutils::getDoubleObjectOrThrow(
      jsonmipmodel::CONSTRAINT_UPPER_BOUND, jsonObject);
  ptr->setConstraintLowerBound(clb);
  ptr->setConstraintUpperBound(cub);

  // Get and set coefficients list
  auto varCoeffListJson = jsonObject.FindMember(optilab::jsonmipmodel::CONSTRAINT_VAR_COEFF_LIST);
  if (varCoeffListJson == jsonObject.MemberEnd())
  {
    throw std::runtime_error("ParserMIPConstraint - variable coefficients list not found");
  }

  for (auto& varCoeff : varCoeffListJson->value.GetArray())
  {
    const auto varCoeffPair = getVarCoeffPair(varCoeff);
    ptr->addVarCoeff(varCoeffPair.first, varCoeffPair.second);
  }

  return ptr;
}  // parse

}  // namespace optilab
