//
// Copyright OptiLab 2019. All rights reserved.
//
// Class encapsulating a parameter model object.
//

#pragma once

#include <memory>
#include <string>

#include "model/model_object.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS ModelObjectParameter : public ModelObject {
 public:
  using SPtr = std::shared_ptr<ModelObjectParameter>;

public:
  ModelObjectParameter();

  ~ModelObjectParameter() = default;

  static bool isa(const ModelObject* modelObject);
  static ModelObjectParameter* cast(ModelObject* modelObject);
  static const ModelObjectParameter* cast(const ModelObject* modelObject);

  const std::string& getParameterName() const { return pName; }

private:
  /// Name of this parameter
  std::string pName;
};

}  // namespace optilab
