#include "model/parser.hpp"

#include "utilities/strings.hpp"

namespace optilab {

Parser::Parser(ModelObject::ModelObjectType objectType)
: pObjectType(objectType)
{
}

ModelObject::SPtr Parser::parse(const std::string& line)
{
  auto trimmedLine = utilsstrings::trimmed(line);
  ModelObject::SPtr object = parseDescription(trimmedLine);

  // Set the description in the object
  object->setObjectDescriptionString(line);

  // Return the parsed object
  return object;
}  // parse

}  // namespace optilab

