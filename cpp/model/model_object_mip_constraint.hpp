//
// Copyright OptiLab 2019. All rights reserved.
//
// Class encapsulating a MIP constraint model object.
//

#pragma once

#include <limits>   // for std::numeric_limits
#include <memory>   // for std::shared_ptr
#include <string>
#include <utility>  // for std::pair
#include <vector>

#include "model/model_object_constraint.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS ModelObjectMIPConstraint : public ModelObjectConstraint {
 public:
  using SPtr = std::shared_ptr<ModelObjectMIPConstraint>;

  /// Variable coefficients
  using VarCoeff = std::pair<std::string, double>;
  using VarCoeffList = std::vector<VarCoeff>;
  using Iter = VarCoeffList::iterator;

public:
  /// Constructor: creates a MIP constraint model object with -INF/+INF default
  /// constraint bounds
  ModelObjectMIPConstraint()
  : ModelObjectConstraint(ConstraintSemantic::CS_INTENSION),
    pConstraintLB(-std::numeric_limits<double>::infinity()),
    pConstraintUB(std::numeric_limits<double>::infinity())
  {
  }

  ~ModelObjectMIPConstraint() = default;

  inline void setConstraintId(const std::string& id) { pConstraintId = id; }
  inline const std::string& getConstraintId() const { return pConstraintId; }

  inline void setConstraintLowerBound(double lb) { pConstraintLB = lb; }
  inline void setConstraintUpperBound(double ub) { pConstraintUB = ub; }

  inline double getConstraintLowerBound() const { return pConstraintLB; }
  inline double getConstraintUpperBound() const { return pConstraintUB; }

  inline void addVarCoeff(const std::string& varId, double coeff)
  {
    pCoefficientsList.push_back({varId, coeff});
  }
  inline const VarCoeffList& getVarCoeffList() const { return pCoefficientsList; }

  /// Iterators used by the for-range loop
  Iter begin() { return pCoefficientsList.begin(); }
  Iter end() { return pCoefficientsList.end(); }

private:
  /// Constraint id
  std::string pConstraintId;

  /// Constraint lower bound
  double pConstraintLB;

  /// Constraint upper bound
  double pConstraintUB;

  /// List of variable/coefficients pairs
  VarCoeffList pCoefficientsList;
};

}  // namespace optilab
