#include "model/model_object_objective.hpp"

namespace optilab {

ModelObjectObjective::ModelObjectObjective()
: ModelObject(ModelObjectType::OBJECTIVE)
{
}

bool ModelObjectObjective::isa(const ModelObject* modelObject)
{
  return modelObject &&
      (modelObject->getType() == ModelObjectType::OBJECTIVE);
}  // isa

ModelObjectObjective* ModelObjectObjective::cast(ModelObject* modelObject)
{
  if (!isa(modelObject)) return nullptr;
  return static_cast<ModelObjectObjective*>(modelObject);
}  // cast

const ModelObjectObjective* ModelObjectObjective::cast(const ModelObject* modelObject)
{
  if (!isa(modelObject)) return nullptr;
  return static_cast<const ModelObjectObjective*>(modelObject);
}  // cast

}  // namespace optilab

