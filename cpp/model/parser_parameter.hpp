//
// Copyright OptiLab 2019. All rights reserved.
//
// Class encapsulating a parser for a parameter model object.
//

#pragma once

#include "model/parser.hpp"

#include <string>

#include "data_structure/json/json.hpp"
#include "model/model_object.hpp"

namespace optilab {

class SYS_EXPORT_CLASS ParserParameter : public Parser {
 public:
  using SPtr = std::shared_ptr<ParserParameter>;

 public:
  ParserParameter();
  virtual ~ParserParameter() = default;

  ModelObject::SPtr parse(JSONValue::JsonObject& jsonObject) override;

 protected:
  ModelObject::SPtr parseDescription(const std::string& description) override;
};

}  // namespace optilab
