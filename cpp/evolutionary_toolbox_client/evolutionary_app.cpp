//
// Copyright OptiLab 2020. All rights reserved.
//
// Entry point for the OptiLab evolutionary
// toolbox application.
//

#include <getopt.h>
#include <signal.h>

#include <cstdint>  // for uint32_t
#include <iostream>
#include <limits>   // for std::numeric_limits
#include <memory>   // for std::shared_ptr
#include <string>
#include <vector>

#include "evolutionary_toolbox_client/client_ga_processor.hpp"

extern int optind;

namespace {

const std::string kfoundryPath_1 =
        "/home/federicocampeotto/personal/evolutionary_data/ga_foundry.hpp";
const std::string kfoundryPath_2 =
        "/home/federicocampeotto/personal/evolutionary_data/custom_environment.cpp";
const uint32_t kNumVars = 1000;
const double kLowerBound = 0.0;
const double kUpperBound = 15.0;
const bool kIsInt = true;
const bool kMultiThread = false;
const uint32_t kPopSize = 20;
const uint32_t knumGen = 100;
const double kMutationRate = 0.2;
const double kCrossoverFraction = 0.7;
const uint32_t kTimeoutSec = 3600;
const double kStallBest = 1e-6;
const uint32_t kStallMax = 10;
const int kSeed = 0;

void printHelp(const std::string& programName) {
  std::cerr << "Usage: " << programName << " [options]"
      << std::endl
      << "options:" << std::endl
      << "  --genetic|-g        Execute genetic algorithm." << std::endl
      << "  --help|-h           Print this help message."
      << std::endl;
}  // printHelp

void runGeneticAlgorithm(const std::string& name)
{
  using namespace optilab;
  using namespace toolbox;

  // Prepare a model
  std::cout << "Single GA processor - create processor..." << std::endl;
  auto proc = std::make_shared<ClientGAProcessor>(name);
  std::cout << "...done" << std::endl;

  // Create the model

  std::cout << "Single GA processor - load model..." << std::endl;
  proc->loadCustomEnvironment(name,
                              kNumVars,
                              kLowerBound,
                              kUpperBound,
                              kIsInt,
                              kMultiThread,
                              kPopSize,
                              knumGen,
                              kMutationRate,
                              kCrossoverFraction,
                              kTimeoutSec,
                              kStallBest,
                              kStallMax,
                              kSeed,
                              {kfoundryPath_1, kfoundryPath_2});
  std::cout << "...done" << std::endl;

  std::cout << "Single GA processor - run model..." << std::endl;
  proc->runProcessor();
  std::cout << "...done" << std::endl;

  // Delete processor instance
  proc.reset();
}  // runGeneticAlgorithm

}  // namespace

int main(int argc, char* argv[]) {

  char optString[] = "gh";
  struct option longOptions[] =
  {
      { "genetic", no_argument, NULL, 'g' },
      { "help", no_argument, NULL, 'h' },
      { 0, 0, 0, 0 }
  };

  // Parse options
  int opt;
  bool runGenetic{false};
  while (-1 != (opt = getopt_long(argc, argv, optString, longOptions, NULL)))
  {
    switch (opt)
    {
      case 'g':
      {
        runGenetic = true;
        break;
      }
      case 'h':
      default:
        printHelp(argv[0]);
        return 0;
    }
  }  // while

  const std::string modelName{"GeneticAlgorithm"};
  if (runGenetic)
  {
    try
    {
      std::cout << "=== Running Genetic algorithm ===" << std::endl;
      runGeneticAlgorithm(modelName);
      std::cout << "=== Done running Genetic algorithm ===" << std::endl;
    }
    catch(...)
    {
      std::cerr << "Error while running Genetic algorithm" << std::endl;
      return 1;
    }
  }

  return 0;
}
