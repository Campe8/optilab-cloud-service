//
// Copyright OptiLab 2020. All rights reserved.
//
// Test processor for Genetic Algorithm strategies.
//

#pragma once

#include <cstdint>  // for uint32_t
#include <memory>   // for std::shared_ptr
#include <vector>

#include "evolutionary_toolbox/ga_processor.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace toolbox {

class SYS_EXPORT_CLASS ClientGAProcessor : public GAProcessor {
public:
  using SPtr = std::shared_ptr<ClientGAProcessor>;

public:
  ClientGAProcessor(const std::string& procName = "ClientGAProcessor");
  ~ClientGAProcessor() override = default;

  void loadCustomEnvironment(const std::string& envName,
                             uint32_t numVars,
                             double lowerBound,
                             double upperBound,
                             bool isInt,
                             bool multiThread,
                             uint32_t popSize,
                             uint32_t numGen,
                             double mutationRate,
                             double crossoverFraction,
                             uint32_t timeoutSec,
                             double stallBest,
                             uint32_t stallMaxGen,
                             int seed,
                             const std::vector<std::string>& pathToEnvironment);

  void runProcessor();
};

}  // namespace toolbox
}  // namespace optilab
