#include "evolutionary_toolbox_client/client_ga_processor.hpp"

#include <chrono>
#include <stdexcept>   // for std::invalid_argument

#include <spdlog/spdlog.h>

#include "data_structure/metrics/metrics_register.hpp"
#include "evolutionary_toolbox/evolutionary_instance.hpp"
#include "evolutionary_toolbox/evolutionary_utilities.hpp"
#include "optilab_protobuf/evolutionary_model.pb.h"
#include "utilities/timer.hpp"

namespace optilab {
namespace toolbox {

ClientGAProcessor::ClientGAProcessor(const std::string& procName)
: GAProcessor(procName, std::make_shared<evolengine::GeneticAlgorithmResult>())
{
}

void ClientGAProcessor::loadCustomEnvironment(const std::string& envName,
                                              uint32_t numVars,
                                              double lowerBound,
                                              double upperBound,
                                              bool isInt,
                                              bool multiThread,
                                              uint32_t popSize,
                                              uint32_t numGen,
                                              double mutationRate,
                                              double crossoverFraction,
                                              uint32_t timeoutSec,
                                              double stallBest,
                                              uint32_t stallMaxGen,
                                              int seed,
                                              const std::vector<std::string>& pathToEnvironment)
{
  EvolutionaryModelProto model;
  model.set_model_id(envName);
  auto ga = model.mutable_genetic_model();
  ga->set_environment_name(envName);
  ga->set_multithread(multiThread);
  ga->set_population_size(popSize);
  ga->set_num_generations(numGen);
  ga->set_mutation_rate(mutationRate);
  ga->set_crossover_fraction(crossoverFraction);
  ga->set_timeout_sec(timeoutSec);
  ga->set_stall_best(stallBest);
  ga->set_stall_max_gen(stallMaxGen);
  ga->set_random_seed(seed);
  auto env = ga->mutable_custom_environment();
  for (const auto& path : pathToEnvironment)
  {
    env->add_environment_path(path);
  }

  auto chr = ga->mutable_chromosome();
  chr->set_lower_bound(lowerBound);
  chr->set_upper_bound(upperBound);
  if (isInt)
  {
    chr->set_type(ChromosomeProto_ChromosomeType::ChromosomeProto_ChromosomeType_CHROMOSOME_INT);
  }
  else
  {
    chr->set_type(ChromosomeProto_ChromosomeType::ChromosomeProto_ChromosomeType_CHROMOSOME_DOUBLE);
  }
  chr->add_dimensions(numVars);

  GAProcessor::loadEnvironmentAndCreateSolver(
          EvolutionaryInstance::UPtr(new EvolutionaryInstance(model)));
}  // loadCustomEnvironment

void ClientGAProcessor::runProcessor() {
  using namespace std::chrono;

  std::cout << "Start solving..." << std::endl;
  auto tic = high_resolution_clock::now();

  MetricsRegister::SPtr mreg = std::make_shared<MetricsRegister>();
  evolengine::EvolWork::SPtr work = std::make_shared<evolengine::EvolWork>(
          evolengine::EvolWork::WorkType::kRunOptimizer, mreg);
  processWork(work);
  GAProcessor::waitForTaskToComplete();

  auto toc = high_resolution_clock::now();
  auto solvingSec = static_cast<uint64_t>(std::chrono::duration_cast<std::chrono::seconds>(
          toc - tic).count());
  std::cout << "Optimized environment in " << solvingSec << " sec." << std::endl;

  auto env = getEnvironment();
  std::cout << "Objective value " << env->getObjectiveValue() <<std::endl;
}  // runProcessor

}  // namespace toolbox
}  // namespace optilab
