#include "optimizer_api/routing_api.hpp"

#include <spdlog/spdlog.h>

#include <cassert>
#include <exception>
#include <utility>  // for std::move

#include "optimizer_api/framework_api_macro.hpp"
#include "routing_toolbox/vrp_engine.hpp"

namespace
{
constexpr int kWaitAllTaskToCompletion = -1;
constexpr int kAllSolutions = -1;

}  // namespace

namespace optilab {

SolverError RoutingApi::createEngine(const std::string& engineId,
                                     toolbox::routingengine::RoutingProcessorType procType) noexcept
{
  ASSERT_AND_RETURN_ERROR(!engineId.empty());
  {
    // Lock mutex on critical section
    LockGuard lock(pMutex);
    if (pEngineMap.find(engineId) != pEngineMap.end())
    {
      // Registering twice the same engine is not allowed
      spdlog::error("RoutingApi - createEngine: engine already registered " +
                    engineId);
      return SolverError::kError;
    }

    toolbox::RoutingEngine::SPtr engine;
    try
    {
      switch (procType)
      {
        case toolbox::routingengine::RoutingProcessorType::RPT_CP_TSP:
        case toolbox::routingengine::RoutingProcessorType::RPT_CP_VRP:
          engine = std::make_shared<toolbox::VRPEngine>(engineId, pEngineHandler, procType);
          break;
        default:
          spdlog::error("RoutingApi - createEngine: unrecognized processor type " +
                        std::to_string(static_cast<int>(procType)));
          throw std::runtime_error("RoutingApi - createEngine: engine processor not available");
      }
    }
    catch (const std::exception& ex)
    {
      spdlog::error("RoutingApi - createEngine on engine id " + engineId +
                    ": " + std::string(ex.what()));
      return SolverError::kError;
    }
    catch (...)
    {
      spdlog::error("RoutingApi - createEngine: undefined error on engine id " +
                    engineId);
      return SolverError::kError;
    }
    assert(engine);

    pEngineMap[engineId] = engine;
  }

  return SolverError::kNoError;
}  // createEngine

SolverError RoutingApi::deleteEngine(const std::string& engineId) noexcept
{
  ASSERT_AND_RETURN_ERROR(!engineId.empty());
  {
    // Lock mutex on critical section
    LockGuard lock(pMutex);

    // Lookup the instance on the map
    auto iter = pEngineMap.find(engineId);
    if (iter != pEngineMap.end())
    {
      toolbox::RoutingEngine::SPtr engine = iter->second;
      pEngineMap.erase(iter);
      engine.reset();

      return SolverError::kNoError;
    }
  }

  // If the flow reached this point, the engine wasn't registered,
  // by default the engine has been trivially deleted
  return SolverError::kNoError;
}  // deleteEngine

SolverError RoutingApi::loadModelInstance(const std::string& engineId,
                                          toolbox::RoutingInstance::UPtr instance) noexcept
{
  ASSERT_AND_RETURN_ERROR(!engineId.empty());

  auto engine = getEngine(engineId);
  ASSERT_AND_RETURN_ERROR(engine);

  if (!instance)
  {
    spdlog::error("RoutingApi - loadModelInstance: empty model instance for engine id " +
                  engineId);
    return SolverError::kError;
  }

  try
  {
    engine->registerInstance(std::move(instance));
  }
  catch (const std::exception& ex)
  {
    spdlog::error("RoutingApi - loadModelInstance on engine id " + engineId + ": " +
                  std::string(ex.what()));
    return SolverError::kError;
  }
  catch (...)
  {
    spdlog::error("RoutingApi - loadModelInstance: undefined error on engine id " +
                  engineId);
    return SolverError::kError;
  }

  return SolverError::kNoError;
}  // loadModel

SolverError RoutingApi::runEngine(const std::string& engineId) noexcept
{
  ASSERT_AND_RETURN_ERROR(!engineId.empty());

  auto engine = getEngine(engineId);
  ASSERT_AND_RETURN_ERROR(engine);

  try
  {
    toolbox::routingengine::RoutingEvent event = toolbox::routingengine::RoutingEvent(
        toolbox::routingengine::RoutingEvent::EventType::kRunEngine);
    engine->notifyEngine(event);
  }
  catch (const std::exception& ex)
  {
    spdlog::error("RoutingApi - runEngine on engine id " + engineId + ": " +
                  std::string(ex.what()));
    return SolverError::kError;
  }
  catch (...)
  {
    spdlog::error("RoutingApi - runEngine: undefined error on engine id " +
                  engineId);
    return SolverError::kError;
  }

  return SolverError::kNoError;
}  // runModel

SolverError RoutingApi::interruptEngine(const std::string& engineId) noexcept
{
  ASSERT_AND_RETURN_ERROR(!engineId.empty());

  auto engine = getEngine(engineId);

  // Without engine, it is trivially satisfied
  if (!engine) return SolverError::kNoError;
  ASSERT_AND_RETURN_ERROR(engine);

  try
  {
    toolbox::routingengine::RoutingEvent event = toolbox::routingengine::RoutingEvent(
        toolbox::routingengine::RoutingEvent::EventType::kInterruptEngine);
    engine->notifyEngine(event);
  }
  catch (const std::exception& ex)
  {
    spdlog::error("RoutingApi - force interrupt on engine id " + engineId +
                  ": " + std::string(ex.what()));
    return SolverError::kError;
  }
  catch (...)
  {
    spdlog::error(
        "RoutingApi - force interruption: undefined error on engine id " +
        engineId);
    return SolverError::kError;
  }

  return SolverError::kNoError;
}  // forceTerminationModelRun

SolverError RoutingApi::collectSolutions(const std::string& engineId, int numSol) noexcept
{
  ASSERT_AND_RETURN_ERROR(!engineId.empty());

  auto engine = getEngine(engineId);
  ASSERT_AND_RETURN_ERROR(engine);

  try
  {
    toolbox::routingengine::RoutingEvent event = toolbox::routingengine::RoutingEvent(
        toolbox::routingengine::RoutingEvent::EventType::kSolutions);
    event.setNumSolutions(numSol);
    engine->notifyEngine(event);
  }
  catch (const std::exception& ex)
  {
    spdlog::error("RoutingApi - collectSolutions on engine id " + engineId +
                  ": " + std::string(ex.what()));
    return SolverError::kError;
  }
  catch (...)
  {
    spdlog::error(
        "RoutingApi - collectSolutions: undefined error on engine id " +
        engineId);
    return SolverError::kError;
  }

  return SolverError::kNoError;
}  // collectSolutions

SolverError RoutingApi::engineWait(const std::string& engineId, int timeoutMsec) noexcept
{
  ASSERT_AND_RETURN_ERROR(!engineId.empty());

  auto engine = getEngine(engineId);
  ASSERT_AND_RETURN_ERROR(engine);

  try
  {
    engine->engineWait(timeoutMsec);
  }
  catch (const std::exception& ex)
  {
    spdlog::error("RoutingApi - engineWait on engine id " + engineId + ": " +
                  std::string(ex.what()));
    return SolverError::kError;
  }
  catch (...)
  {
    spdlog::error("RoutingApi - engineWait: undefined error on engine id " +
                  engineId);
    return SolverError::kError;
  }

  return SolverError::kNoError;
}  // engineWait

void RoutingApi::setEngineHandler(const EngineCallbackHandler::SPtr& handler) noexcept
{
  pEngineHandler = handler;
}  // setEngineHandler

EngineCallbackHandler::SPtr RoutingApi::getEngineHandler() const noexcept
{
  return pEngineHandler;
}  // getEngineHandler

toolbox::RoutingEngine::SPtr RoutingApi::getEngine(const std::string& engineId)
{
  {
    // Lock mutex on critical section
    LockGuard lock(pMutex);

    // Lookup the instance on the map
    if (pEngineMap.find(engineId) != pEngineMap.end())
    {
      return pEngineMap[engineId];
    }
  }

  spdlog::warn("RoutingApi - getEngine: return empty engine pointer");
  toolbox::RoutingEngine::SPtr emptyPtr;
  return emptyPtr;
}  // getEngine

}  // namespace optilab
