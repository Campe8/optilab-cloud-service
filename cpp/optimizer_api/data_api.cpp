#include "optimizer_api/data_api.hpp"

#include <spdlog/spdlog.h>

#include <cassert>
#include <exception>
#include <utility>  // for std::move

#include "optimizer_api/framework_api_macro.hpp"
#include "data_toolbox/python_engine.hpp"

namespace
{
constexpr int kWaitAllTaskToCompletion = -1;
constexpr int kAllSolutions = -1;

}  // namespace

namespace optilab {

SolverError DataApi::createEngine(
        const std::string& engineId,
        toolbox::dataengine::DataProcessorType procType) noexcept
{
  ASSERT_AND_RETURN_ERROR(!engineId.empty());
  {
    // Lock mutex on critical section
    LockGuard lock(pMutex);
    if (pEngineMap.find(engineId) != pEngineMap.end())
    {
      // Registering twice the same engine is not allowed
      spdlog::error("DataApi - createEngine: engine already registered " +
                    engineId);
      return SolverError::kError;
    }

    toolbox::DataEngine::SPtr engine;
    try
    {
      switch (procType)
      {
        case toolbox::dataengine::DataProcessorType::DPT_PYTHON_FCN:
          engine = std::make_shared<toolbox::PythonEngine>(engineId, pEngineHandler);
          break;
        default:
          spdlog::error("DataApi - createEngine: unrecognized processor type " +
                        std::to_string(static_cast<int>(procType)));
          throw std::runtime_error("DataApi - createEngine: engine processor not available");
      }
    }
    catch (const std::exception& ex)
    {
      spdlog::error("DataApi - createEngine on engine id " + engineId +
                    ": " + std::string(ex.what()));
      return SolverError::kError;
    }
    catch (...)
    {
      spdlog::error("DataApi - createEngine: undefined error on engine id " +
                    engineId);
      return SolverError::kError;
    }
    assert(engine);

    pEngineMap[engineId] = engine;
  }

  return SolverError::kNoError;
}  // createEngine

SolverError DataApi::deleteEngine(const std::string& engineId) noexcept
{
  ASSERT_AND_RETURN_ERROR(!engineId.empty());
  {
    // Lock mutex on critical section
    LockGuard lock(pMutex);

    // Lookup the instance on the map
    auto iter = pEngineMap.find(engineId);
    if (iter != pEngineMap.end())
    {
      toolbox::DataEngine::SPtr engine = iter->second;
      pEngineMap.erase(iter);
      engine.reset();

      return SolverError::kNoError;
    }
  }

  // If the flow reached this point, the engine wasn't registered,
  // by default the engine has been trivially deleted
  return SolverError::kNoError;
}  // deleteEngine

SolverError DataApi::loadModelInstance(
        const std::string& engineId, toolbox::DataInstance::UPtr instance) noexcept
{
  ASSERT_AND_RETURN_ERROR(!engineId.empty());

  auto engine = getEngine(engineId);
  ASSERT_AND_RETURN_ERROR(engine);

  if (!instance)
  {
    spdlog::error("DataApi - loadModelInstance: empty model instance for engine id " +
                  engineId);
    return SolverError::kError;
  }

  try
  {
    engine->registerInstance(std::move(instance));
  }
  catch (const std::exception& ex)
  {
    spdlog::error("DataApi - loadModelInstance on engine id " + engineId + ": " +
                  std::string(ex.what()));
    return SolverError::kError;
  }
  catch (...)
  {
    spdlog::error("DataApi - loadModelInstance: undefined error on engine id " +
                  engineId);
    return SolverError::kError;
  }

  return SolverError::kNoError;
}  // loadModel

SolverError DataApi::runEngine(const std::string& engineId,
                               toolbox::DataInstance::UPtr instance) noexcept
{
  ASSERT_AND_RETURN_ERROR(!engineId.empty());

  auto engine = getEngine(engineId);
  ASSERT_AND_RETURN_ERROR(engine);

  try
  {
    toolbox::dataengine::DataEvent event = toolbox::dataengine::DataEvent(
            toolbox::dataengine::DataEvent::EventType::kRunEngine);
    if (instance != nullptr)
    {
      event.setDataInstance(std::move(instance));
    }
    engine->notifyEngine(event);
  }
  catch (const std::exception& ex)
  {
    spdlog::error("DataApi - runEngine on engine id " + engineId + ": " +
                  std::string(ex.what()));
    return SolverError::kError;
  }
  catch (...)
  {
    spdlog::error("DataApi - runEngine: undefined error on engine id " +
                  engineId);
    return SolverError::kError;
  }

  return SolverError::kNoError;
}  // runModel

SolverError DataApi::interruptEngine(const std::string& engineId) noexcept
{
  ASSERT_AND_RETURN_ERROR(!engineId.empty());

  auto engine = getEngine(engineId);

  // Without engine, it is trivially satisfied
  if (!engine) return SolverError::kNoError;
  ASSERT_AND_RETURN_ERROR(engine);

  try
  {
    toolbox::dataengine::DataEvent event = toolbox::dataengine::DataEvent(
            toolbox::dataengine::DataEvent::EventType::kInterruptEngine);
    engine->notifyEngine(event);
  }
  catch (const std::exception& ex)
  {
    spdlog::error("DataApi - force interrupt on engine id " + engineId +
                  ": " + std::string(ex.what()));
    return SolverError::kError;
  }
  catch (...)
  {
    spdlog::error(
        "DataApi - force interruption: undefined error on engine id " +
        engineId);
    return SolverError::kError;
  }

  return SolverError::kNoError;
}  // forceTerminationModelRun

SolverError DataApi::collectSolutions(const std::string& engineId, int numSol) noexcept
{
  ASSERT_AND_RETURN_ERROR(!engineId.empty());

  auto engine = getEngine(engineId);
  ASSERT_AND_RETURN_ERROR(engine);

  try
  {
    toolbox::dataengine::DataEvent event = toolbox::dataengine::DataEvent(
            toolbox::dataengine::DataEvent::EventType::kSolutions);
    event.setNumSolutions(numSol);
    engine->notifyEngine(event);
  }
  catch (const std::exception& ex)
  {
    spdlog::error("DataApi - collectSolutions on engine id " + engineId +
                  ": " + std::string(ex.what()));
    return SolverError::kError;
  }
  catch (...)
  {
    spdlog::error(
        "DataApi - collectSolutions: undefined error on engine id " +
        engineId);
    return SolverError::kError;
  }

  return SolverError::kNoError;
}  // collectSolutions

SolverError DataApi::engineWait(const std::string& engineId, int timeoutMsec) noexcept
{
  ASSERT_AND_RETURN_ERROR(!engineId.empty());

  auto engine = getEngine(engineId);
  ASSERT_AND_RETURN_ERROR(engine);

  try
  {
    engine->engineWait(timeoutMsec);
  }
  catch (const std::exception& ex)
  {
    spdlog::error("DataApi - engineWait on engine id " + engineId + ": " +
                  std::string(ex.what()));
    return SolverError::kError;
  }
  catch (...)
  {
    spdlog::error("DataApi - engineWait: undefined error on engine id " +
                  engineId);
    return SolverError::kError;
  }

  return SolverError::kNoError;
}  // engineWait

void DataApi::setEngineHandler(const EngineCallbackHandler::SPtr& handler) noexcept
{
  pEngineHandler = handler;
}  // setEngineHandler

EngineCallbackHandler::SPtr DataApi::getEngineHandler() const noexcept
{
  return pEngineHandler;
}  // getEngineHandler

toolbox::DataEngine::SPtr DataApi::getEngine(const std::string& engineId)
{
  {
    // Lock mutex on critical section
    LockGuard lock(pMutex);

    // Lookup the instance on the map
    if (pEngineMap.find(engineId) != pEngineMap.end())
    {
      return pEngineMap[engineId];
    }
  }

  spdlog::warn("DataApi - getEngine: return empty engine pointer");
  toolbox::DataEngine::SPtr emptyPtr;
  return emptyPtr;
}  // getEngine

}  // namespace optilab
