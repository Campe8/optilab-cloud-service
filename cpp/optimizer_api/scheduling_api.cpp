#include "optimizer_api/scheduling_api.hpp"

#include <spdlog/spdlog.h>

#include <cassert>
#include <exception>
#include <utility>  // for std::move

#include "optimizer_api/framework_api_macro.hpp"
#include "scheduling_toolbox/employee_scheduling_engine.hpp"

namespace
{
constexpr int kWaitAllTaskToCompletion = -1;
constexpr int kAllSolutions = -1;

}  // namespace

namespace optilab {

SolverError SchedulingApi::createEngine(
        const std::string& engineId,
        toolbox::schedulingengine::SchedulingProcessorType procType) noexcept
{
  ASSERT_AND_RETURN_ERROR(!engineId.empty());
  {
    // Lock mutex on critical section
    LockGuard lock(pMutex);
    if (pEngineMap.find(engineId) != pEngineMap.end())
    {
      // Registering twice the same engine is not allowed
      spdlog::error("SchedulingApi - createEngine: engine already registered " +
                    engineId);
      return SolverError::kError;
    }

    toolbox::SchedulingEngine::SPtr engine;
    try
    {
      switch (procType)
      {
        case toolbox::schedulingengine::SchedulingProcessorType::SPT_EMPLOYEES_SCHEDULING:
          engine = std::make_shared<toolbox::EmployeesSchedulingEngine>(
                  engineId, pEngineHandler, procType);
          break;
        default:
          spdlog::error("SchedulingApi - createEngine: unrecognized processor type " +
                        std::to_string(static_cast<int>(procType)));
          throw std::runtime_error("SchedulingApi - createEngine: engine processor not available");
      }
    }
    catch (const std::exception& ex)
    {
      spdlog::error("SchedulingApi - createEngine on engine id " + engineId +
                    ": " + std::string(ex.what()));
      return SolverError::kError;
    }
    catch (...)
    {
      spdlog::error("SchedulingApi - createEngine: undefined error on engine id " +
                    engineId);
      return SolverError::kError;
    }
    assert(engine);

    pEngineMap[engineId] = engine;
  }

  return SolverError::kNoError;
}  // createEngine

SolverError SchedulingApi::deleteEngine(const std::string& engineId) noexcept
{
  ASSERT_AND_RETURN_ERROR(!engineId.empty());
  {
    // Lock mutex on critical section
    LockGuard lock(pMutex);

    // Lookup the instance on the map
    auto iter = pEngineMap.find(engineId);
    if (iter != pEngineMap.end())
    {
      toolbox::SchedulingEngine::SPtr engine = iter->second;
      pEngineMap.erase(iter);
      engine.reset();

      return SolverError::kNoError;
    }
  }

  // If the flow reached this point, the engine wasn't registered,
  // by default the engine has been trivially deleted
  return SolverError::kNoError;
}  // deleteEngine

SolverError SchedulingApi::loadModelInstance(const std::string& engineId,
                                             toolbox::SchedulingInstance::UPtr instance) noexcept
{
  ASSERT_AND_RETURN_ERROR(!engineId.empty());

  auto engine = getEngine(engineId);
  ASSERT_AND_RETURN_ERROR(engine);

  if (!instance)
  {
    spdlog::error("SchedulingApi - loadModelInstance: empty model instance for engine id " +
                  engineId);
    return SolverError::kError;
  }

  try
  {
    engine->registerInstance(std::move(instance));
  }
  catch (const std::exception& ex)
  {
    spdlog::error("SchedulingApi - loadModelInstance on engine id " + engineId + ": " +
                  std::string(ex.what()));
    return SolverError::kError;
  }
  catch (...)
  {
    spdlog::error("SchedulingApi - loadModelInstance: undefined error on engine id " +
                  engineId);
    return SolverError::kError;
  }

  return SolverError::kNoError;
}  // loadModel

SolverError SchedulingApi::runEngine(const std::string& engineId) noexcept
{
  ASSERT_AND_RETURN_ERROR(!engineId.empty());

  auto engine = getEngine(engineId);
  ASSERT_AND_RETURN_ERROR(engine);

  try
  {
    auto event = toolbox::schedulingengine::SchedulingEvent(
            toolbox::schedulingengine::SchedulingEvent::EventType::kRunEngine);
    engine->notifyEngine(event);
  }
  catch (const std::exception& ex)
  {
    spdlog::error("SchedulingApi - runEngine on engine id " + engineId + ": " +
                  std::string(ex.what()));
    return SolverError::kError;
  }
  catch (...)
  {
    spdlog::error("SchedulingApi - runEngine: undefined error on engine id " +
                  engineId);
    return SolverError::kError;
  }

  return SolverError::kNoError;
}  // runModel

SolverError SchedulingApi::interruptEngine(const std::string& engineId) noexcept
{
  ASSERT_AND_RETURN_ERROR(!engineId.empty());

  auto engine = getEngine(engineId);

  // Without engine, it is trivially satisfied
  if (!engine) return SolverError::kNoError;
  ASSERT_AND_RETURN_ERROR(engine);

  try
  {
    auto event = toolbox::schedulingengine::SchedulingEvent(
            toolbox::schedulingengine::SchedulingEvent::EventType::kInterruptEngine);
    engine->notifyEngine(event);
  }
  catch (const std::exception& ex)
  {
    spdlog::error("SchedulingApi - force interrupt on engine id " + engineId +
                  ": " + std::string(ex.what()));
    return SolverError::kError;
  }
  catch (...)
  {
    spdlog::error(
        "SchedulingApi - force interruption: undefined error on engine id " +
        engineId);
    return SolverError::kError;
  }

  return SolverError::kNoError;
}  // forceTerminationModelRun

SolverError SchedulingApi::collectSolutions(const std::string& engineId, int numSol) noexcept
{
  ASSERT_AND_RETURN_ERROR(!engineId.empty());

  auto engine = getEngine(engineId);
  ASSERT_AND_RETURN_ERROR(engine);

  try
  {
    auto event = toolbox::schedulingengine::SchedulingEvent(
            toolbox::schedulingengine::SchedulingEvent::EventType::kSolutions);
    event.setNumSolutions(numSol);
    engine->notifyEngine(event);
  }
  catch (const std::exception& ex)
  {
    spdlog::error("SchedulingApi - collectSolutions on engine id " + engineId +
                  ": " + std::string(ex.what()));
    return SolverError::kError;
  }
  catch (...)
  {
    spdlog::error(
        "SchedulingApi - collectSolutions: undefined error on engine id " +
        engineId);
    return SolverError::kError;
  }

  return SolverError::kNoError;
}  // collectSolutions

SolverError SchedulingApi::engineWait(const std::string& engineId, int timeoutMsec) noexcept
{
  ASSERT_AND_RETURN_ERROR(!engineId.empty());

  auto engine = getEngine(engineId);
  ASSERT_AND_RETURN_ERROR(engine);

  try
  {
    engine->engineWait(timeoutMsec);
  }
  catch (const std::exception& ex)
  {
    spdlog::error("SchedulingApi - engineWait on engine id " + engineId + ": " +
                  std::string(ex.what()));
    return SolverError::kError;
  }
  catch (...)
  {
    spdlog::error("SchedulingApi - engineWait: undefined error on engine id " +
                  engineId);
    return SolverError::kError;
  }

  return SolverError::kNoError;
}  // engineWait

void SchedulingApi::setEngineHandler(const EngineCallbackHandler::SPtr& handler) noexcept
{
  pEngineHandler = handler;
}  // setEngineHandler

EngineCallbackHandler::SPtr SchedulingApi::getEngineHandler() const noexcept
{
  return pEngineHandler;
}  // getEngineHandler

toolbox::SchedulingEngine::SPtr SchedulingApi::getEngine(const std::string& engineId)
{
  {
    // Lock mutex on critical section
    LockGuard lock(pMutex);

    // Lookup the instance on the map
    if (pEngineMap.find(engineId) != pEngineMap.end())
    {
      return pEngineMap[engineId];
    }
  }

  spdlog::warn("SchedulingApi - getEngine: return empty engine pointer");
  toolbox::SchedulingEngine::SPtr emptyPtr;
  return emptyPtr;
}  // getEngine

}  // namespace optilab
