//
// Copyright OptiLab 2019. All rights reserved.
//
// API interface for a connector.
//

#pragma once

#include <memory>  // for std::shared_ptr
#include <string>
#include <unordered_map>

#include "engine/engine_constants.hpp"
#include "optimizer_api/framework_api.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS ConnectorApi : public FrameworkApi {
 public:
  using SPtr = std::shared_ptr<ConnectorApi>;

 public:
  virtual ~ConnectorApi() = default;

  /// Returns the framework type of this API
  EngineFrameworkType getFrameworkAPIType() const noexcept override
  {
    return EngineFrameworkType::CONNECTORS_FRAMEWORK;
  }

  /// Creates and registers a new instance of a connector engine for the specified connector type
  /// and with given identifier. A connector engine is a back-end component that satisfies the
  /// requests addressed to it and its behavior depends on the type of connector.
  /// Returns a SolverError type.
  /// @note this method is thread-safe.
  /// @note this method returns an error if the same engine is registered twice.
  /// @note this method returns an error is there is no handler previously registered.
  /// @note this method never throws
  virtual SolverError createConnector(const std::string& connectorId,
                                      EngineClassType connectorType) noexcept = 0;

  /// Deletes the connector engine instance "connectorId" from the internal map.
  /// Returns a SolverError type.
  /// @note this method is thread-safe.
  /// @note returns an error on deleting an engine that is not registered.
  /// @note this method never throws
  virtual SolverError deleteConnector(const std::string& connectorId) noexcept = 0;

  /// Loads the connections attached to the connector engine with given identifier.
  /// The connections are expressed as json models.
  /// @note this method is thread-safe.
  /// @note returns SolverError::kError if the engine with given identifier is not registered
  virtual SolverError loadConnections(const std::string& connectorId,
                                      const std::string& jsonConnections) noexcept = 0;

  /// Runs the connector engine "connectorId" on its loaded connections and returns asap.
  /// The engine will run the connections it has as input connections either in parallel or
  /// sequentially and it will callback the results at the end of the run according to the
  /// logic implemented by the connector.
  /// @note this method DOES NOT wait for the engine to complete running.
  /// @note this method is thread-safe.
  /// @note returns SolverError::kError if the engine with given identifier is not registered
  /// or on any other exception thrown by the engine (e.g., no model is loaded)
  virtual SolverError runConnector(const std::string& connectorId) noexcept = 0;

  /// Forces the given connector engine to terminate its run (if running on the input connections).
  /// @note returns SolverError::kError if the engine with given identifier is not registered
  virtual SolverError forceConnectorTermination(const std::string& connectorId) noexcept = 0;

  /// Waits for the engine with specified identifier to complete
  /// all the tasks in its queue of tasks.
  /// If "timeoutSec" is -1, wait for all the queue to be empty, otherwise wait
  /// for "timeoutSec" seconds.
  /// @note returns SolverError::kError if the engine with given identifier is not registered
  virtual SolverError engineWait(const std::string& connectorId, int timeoutSec) noexcept = 0;

  /// Collects the solutions produced by the engines connected to this connector engine.
  /// @note this method is thread-safe.
  /// @note returns SolverError::kError if the engine with given identifier is not registered.
  /// @note by default this is a no-op since not all connector may necessary return a solution.
  /// For example, a transformer connector may transform an input into a given output and
  /// pass it over to the next engine without returning any solution
  virtual SolverError collectSolutions(const std::string& connectorId) noexcept
  {
    (void)connectorId;
    return SolverError::kNoError;
  }

};

}  // namespace optilab
