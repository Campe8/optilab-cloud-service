#include "optimizer_api/cp_api.hpp"

#include <spdlog/spdlog.h>

#include <cassert>
#include <exception>
#include <utility>  // for std::move

#include "optimizer_api/framework_api_macro.hpp"
#include "cp_toolbox/cp_sat_engine.hpp"

namespace
{
constexpr int kWaitAllTaskToCompletion = -1;
constexpr int kAllSolutions = -1;
}  // namespace

namespace optilab {

SolverError CPApi::createEngine(const std::string& engineId,
                                toolbox::cpengine::ConstraintProcessorType procType) noexcept
{
  ASSERT_AND_RETURN_ERROR(!engineId.empty());
  {
    // Lock mutex on critical section
    LockGuard lock(pMutex);
    if (pEngineMap.find(engineId) != pEngineMap.end())
    {
      // Registering twice the same engine is not allowed
      spdlog::error("CPApi - createEngine: engine already registered " + engineId);
      return SolverError::kError;
    }

    toolbox::CPEngine::SPtr engine;
    try
    {
      switch (procType)
      {
        case toolbox::cpengine::ConstraintProcessorType::CP_CP_SAT:
          engine = std::make_shared<toolbox::CPSatEngine>(engineId, pEngineHandler);
          break;
        default:
          spdlog::error("CPApi - createEngine: unrecognized processor type " +
                        std::to_string(static_cast<int>(procType)));
          throw std::runtime_error("CPApi - createEngine: engine processor not available");
      }
    }
    catch (const std::exception& ex)
    {
      spdlog::error("CPApi - createEngine on engine id " + engineId +
                    ": " + std::string(ex.what()));
      return SolverError::kError;
    }
    catch (...)
    {
      spdlog::error("CPApi - createEngine: undefined error on engine id " +
                    engineId);
      return SolverError::kError;
    }
    assert(engine);

    pEngineMap[engineId] = engine;
  }

  return SolverError::kNoError;
}  // createEngine

SolverError CPApi::deleteEngine(const std::string& engineId) noexcept
{
  ASSERT_AND_RETURN_ERROR(!engineId.empty());
  {
    // Lock mutex on critical section
    LockGuard lock(pMutex);

    // Lookup the instance on the map
    auto iter = pEngineMap.find(engineId);
    if (iter != pEngineMap.end())
    {
      toolbox::CPEngine::SPtr engine = iter->second;
      pEngineMap.erase(iter);
      engine.reset();

      return SolverError::kNoError;
    }
  }

  // If the flow reached this point, the engine wasn't registered,
  // by default the engine has been trivially deleted
  return SolverError::kNoError;
}  // deleteEngine

SolverError CPApi::loadModelInstance(const std::string& engineId,
                                     toolbox::CPInstance::UPtr instance) noexcept
{
  ASSERT_AND_RETURN_ERROR(!engineId.empty());

  auto engine = getEngine(engineId);
  ASSERT_AND_RETURN_ERROR(engine);

  if (!instance)
  {
    spdlog::error("CPApi - loadModelInstance: empty model instance for engine id " +
                  engineId);
    return SolverError::kError;
  }

  try
  {
    engine->registerInstance(std::move(instance));
  }
  catch (const std::exception& ex)
  {
    spdlog::error("CPApi - loadModelInstance on engine id " + engineId + ": " +
                  std::string(ex.what()));
    return SolverError::kError;
  }
  catch (...)
  {
    spdlog::error("CPApi - loadModelInstance: undefined error on engine id " +
                  engineId);
    return SolverError::kError;
  }

  return SolverError::kNoError;
}  // loadModel

SolverError CPApi::runEngine(const std::string& engineId,
                             toolbox::CPInstance::UPtr instance) noexcept
{
  ASSERT_AND_RETURN_ERROR(!engineId.empty());

  auto engine = getEngine(engineId);
  ASSERT_AND_RETURN_ERROR(engine);

  try
  {
    toolbox::cpengine::CPEvent event = toolbox::cpengine::CPEvent(
            toolbox::cpengine::CPEvent::EventType::kRunEngine);
    if (instance != nullptr)
    {
      event.setInstance(std::move(instance));
    }
    engine->notifyEngine(event);
  }
  catch (const std::exception& ex)
  {
    spdlog::error("CPApi - runEngine on engine id " + engineId + ": " +
                  std::string(ex.what()));
    return SolverError::kError;
  }
  catch (...)
  {
    spdlog::error("CPApi - runEngine: undefined error on engine id " +
                  engineId);
    return SolverError::kError;
  }

  return SolverError::kNoError;
}  // runModel

SolverError CPApi::interruptEngine(const std::string& engineId) noexcept
{
  ASSERT_AND_RETURN_ERROR(!engineId.empty());

  auto engine = getEngine(engineId);

  // Without engine, it is trivially satisfied
  if (!engine) return SolverError::kNoError;
  ASSERT_AND_RETURN_ERROR(engine);

  try
  {
    toolbox::cpengine::CPEvent event = toolbox::cpengine::CPEvent(
            toolbox::cpengine::CPEvent::EventType::kInterruptEngine);
    engine->notifyEngine(event);
  }
  catch (const std::exception& ex)
  {
    spdlog::error("CPApi - force interrupt on engine id " + engineId +
                  ": " + std::string(ex.what()));
    return SolverError::kError;
  }
  catch (...)
  {
    spdlog::error(
        "CPApi - force interruption: undefined error on engine id " +
        engineId);
    return SolverError::kError;
  }

  return SolverError::kNoError;
}  // forceTerminationModelRun

SolverError CPApi::collectSolutions(const std::string& engineId, int numSol) noexcept
{
  ASSERT_AND_RETURN_ERROR(!engineId.empty());

  auto engine = getEngine(engineId);
  ASSERT_AND_RETURN_ERROR(engine);

  try
  {
    toolbox::cpengine::CPEvent event = toolbox::cpengine::CPEvent(
            toolbox::cpengine::CPEvent::EventType::kSolutions);
    event.setNumSolutions(numSol);
    engine->notifyEngine(event);
  }
  catch (const std::exception& ex)
  {
    spdlog::error("CPApi - collectSolutions on engine id " + engineId +
                  ": " + std::string(ex.what()));
    return SolverError::kError;
  }
  catch (...)
  {
    spdlog::error(
        "CPApi - collectSolutions: undefined error on engine id " +
        engineId);
    return SolverError::kError;
  }

  return SolverError::kNoError;
}  // collectSolutions

SolverError CPApi::engineWait(const std::string& engineId, int timeoutMsec) noexcept
{
  ASSERT_AND_RETURN_ERROR(!engineId.empty());

  auto engine = getEngine(engineId);
  ASSERT_AND_RETURN_ERROR(engine);

  try
  {
    engine->engineWait(timeoutMsec);
  }
  catch (const std::exception& ex)
  {
    spdlog::error("CPApi - engineWait on engine id " + engineId + ": " +
                  std::string(ex.what()));
    return SolverError::kError;
  }
  catch (...)
  {
    spdlog::error("CPApi - engineWait: undefined error on engine id " +
                  engineId);
    return SolverError::kError;
  }

  return SolverError::kNoError;
}  // engineWait

void CPApi::setEngineHandler(const EngineCallbackHandler::SPtr& handler) noexcept
{
  pEngineHandler = handler;
}  // setEngineHandler

EngineCallbackHandler::SPtr CPApi::getEngineHandler() const noexcept
{
  return pEngineHandler;
}  // getEngineHandler

toolbox::CPEngine::SPtr CPApi::getEngine(const std::string& engineId)
{
  {
    // Lock mutex on critical section
    LockGuard lock(pMutex);

    // Lookup the instance on the map
    if (pEngineMap.find(engineId) != pEngineMap.end())
    {
      return pEngineMap[engineId];
    }
  }

  spdlog::warn("CPApi - getEngine: return empty engine pointer");
  toolbox::CPEngine::SPtr emptyPtr;
  return emptyPtr;
}  // getEngine

}  // namespace optilab
