#include "optimizer_api/async_connector_api.hpp"

#include <spdlog/spdlog.h>

#include <cassert>
#include <exception>

#include "connector_engine/combinator_engine.hpp"
#include "connector_engine/connector_utilities.hpp"
#include "optimizer_api/framework_api_macro.hpp"

namespace
{
constexpr int kWaitAllTaskToCompletion = -1;

}  // namespace

namespace optilab
{
SolverError AsyncConnectorApi::createBackendServiceEngine(
    const std::string& engineId, EngineClassType classType) noexcept
{
  return createConnector(engineId, classType);
}  // createBackendServiceEngine

SolverError AsyncConnectorApi::interruptBackendServiceEngine(
    const std::string& engineId) noexcept
{
  return forceConnectorTermination(engineId);
}  // interruptBackendServiceEngine

SolverError AsyncConnectorApi::deleteBackendServiceEngine(
    const std::string& engineId) noexcept
{
  // First wait on unfinished tasks
  const auto err = engineWait(engineId, kWaitAllTaskToCompletion);
  CHECK_ERROR_AND_RETURN(err);

  // Then delete the engine
  spdlog::info("AsyncConnectorApi - delete backend engine " + engineId);
  return deleteConnector(engineId);
}  // deleteBackendServiceEngine

SolverError AsyncConnectorApi::runBackendServiceEngine(
    const std::string& engineId, const std::string& serviceDescriptor) noexcept
{
  // Wait on unfinished tasks
  auto err = engineWait(engineId, kWaitAllTaskToCompletion);
  CHECK_ERROR_AND_RETURN(err);

  // Load the connections into the engine
  err = loadConnections(engineId, serviceDescriptor);
  CHECK_ERROR_AND_RETURN(err);

  err = engineWait(engineId, kWaitAllTaskToCompletion);
  CHECK_ERROR_AND_RETURN(err);

  // Run the connector
  err = runConnector(engineId);
  CHECK_ERROR_AND_RETURN(err);

  // Wait for the model run to be finished
  err = engineWait(engineId, kWaitAllTaskToCompletion);
  return err;
}  // runBackendServiceEngine

SolverError AsyncConnectorApi::createConnector(
    const std::string& connectorId, EngineClassType connectorType) noexcept
{
  ASSERT_AND_RETURN_ERROR(!connectorId.empty());
  {
    // Lock mutex on critical section
    LockGuard lock(pMutex);
    if (pEngineMap.find(connectorId) != pEngineMap.end())
    {
      // Registering twice the same engine is not allowed
      return SolverError::kError;
    }

    ConnectorEngine::SPtr engine;
    try
    {
      switch (connectorType)
      {
        case EngineClassType::EC_COMBINATOR:
          engine = std::shared_ptr<CombinatorEngine>(
              new CombinatorEngine(connectorId, pEngineHandler));
          break;
        default:
          throw std::runtime_error("Connector engine not available");
      }
    }
    catch (const std::exception& ex)
    {
      spdlog::error("AsynchConnectorApi - createEngine on engine id " +
                    connectorId + ": " + std::string(ex.what()));
      return SolverError::kError;
    }
    catch (...)
    {
      spdlog::error(
          "AsynchConnectorApi - createEngine: undefined error on engine id " +
          connectorId);
      return SolverError::kError;
    }
    assert(engine);

    pEngineMap[connectorId] = engine;
  }

  return SolverError::kNoError;
}  // createConnector

SolverError AsyncConnectorApi::deleteConnector(
    const std::string& connectorId) noexcept
{
  ASSERT_AND_RETURN_ERROR(!connectorId.empty());
  {
    // Lock mutex on critical section
    LockGuard lock(pMutex);

    // Lookup the instance on the map
    auto iter = pEngineMap.find(connectorId);
    if (iter != pEngineMap.end())
    {
      ConnectorEngine::SPtr engine = iter->second;
      pEngineMap.erase(iter);
      engine.reset();

      return SolverError::kNoError;
    }
  }

  // If the flow reached this point, the engine wasn't registered,
  // by default the engine has been trivially deleted
  return SolverError::kNoError;
}  // deleteConnector

SolverError AsyncConnectorApi::loadConnections(
    const std::string& connectorId, const std::string& jsonConnections) noexcept
{
  ASSERT_AND_RETURN_ERROR(!connectorId.empty());
  ASSERT_AND_RETURN_ERROR(!jsonConnections.empty());

  auto engine = getEngine(connectorId);
  ASSERT_AND_RETURN_ERROR(engine);

  try
  {
    engine->loadConnections(jsonConnections);
  }
  catch (const std::exception& ex)
  {
    spdlog::error("AsynchConnectorApi - loadConnections on engine id " +
                  connectorId + ": " + std::string(ex.what()));
    return SolverError::kError;
  }
  catch (...)
  {
    spdlog::error(
        "AsynchConnectorApi - loadConnections: undefined error on engine id " +
        connectorId);
    return SolverError::kError;
  }

  return SolverError::kNoError;
}  // loadConnections

SolverError AsyncConnectorApi::runConnector(
    const std::string& connectorId) noexcept
{
  ASSERT_AND_RETURN_ERROR(!connectorId.empty());

  auto engine = getEngine(connectorId);
  ASSERT_AND_RETURN_ERROR(engine);

  try
  {
    connectorengine::ConnectorEvent eventRun = connectorengine::ConnectorEvent(
        connectorengine::ConnectorEvent::EventType::kRunEngine);
    engine->notifyEngine(eventRun);
  }
  catch (const std::exception& ex)
  {
    spdlog::error("AsynchConnectorApi - runConnector on engine id " +
                  connectorId + ": " + std::string(ex.what()));
    return SolverError::kError;
  }
  catch (...)
  {
    spdlog::error(
        "AsynchConnectorApi - runConnector: "
        "undefined error on engine id " +
        connectorId);
    return SolverError::kError;
  }

  return SolverError::kNoError;
}  // runConnector

SolverError AsyncConnectorApi::collectSolutions(
    const std::string& connectorId) noexcept
{
  ASSERT_AND_RETURN_ERROR(!connectorId.empty());

  auto engine = getEngine(connectorId);
  ASSERT_AND_RETURN_ERROR(engine);

  try
  {
    connectorengine::ConnectorEvent eventSol = connectorengine::ConnectorEvent(
        connectorengine::ConnectorEvent::EventType::kSolutions);
    engine->notifyEngine(eventSol);

    connectorengine::ConnectorEvent eventBroadcast =
        connectorengine::ConnectorEvent(
            connectorengine::ConnectorEvent::EventType::kStopBackendEngines);
    engine->notifyEngine(eventBroadcast);
  }
  catch (const std::exception& ex)
  {
    spdlog::error("AsynchConnectorApi - collectSolutions on engine id " +
                  connectorId + ": " + std::string(ex.what()));
    return SolverError::kError;
  }
  catch (...)
  {
    spdlog::error(
        "AsynchConnectorApi - collectSolutions: "
        "undefined error on engine id " +
        connectorId);
    return SolverError::kError;
  }

  return SolverError::kNoError;
}  // collectSolutions

SolverError AsyncConnectorApi::forceConnectorTermination(
    const std::string& connectorId) noexcept
{
  ASSERT_AND_RETURN_ERROR(!connectorId.empty());

  auto engine = getEngine(connectorId);

  // No connector found, nothing to force termination on
  if (!engine) return SolverError::kNoError;
  ASSERT_AND_RETURN_ERROR(engine);

  try
  {
    connectorengine::ConnectorEvent event = connectorengine::ConnectorEvent(
        connectorengine::ConnectorEvent::EventType::kInterruptEngine);
    engine->notifyEngine(event);
  }
  catch (const std::exception& ex)
  {
    spdlog::error("AsynchConnectorApi - force termination on engine id " +
                  connectorId + ": " + std::string(ex.what()));
    return SolverError::kError;
  }
  catch (...)
  {
    spdlog::error(
        "AsynchConnectorApi - force termination: undefined error on engine "
        "id " +
        connectorId);
    return SolverError::kError;
  }

  return SolverError::kNoError;
}  // forceConnectorTermination

SolverError AsyncConnectorApi::engineWait(const std::string& engineId,
                                          int timeoutMsec) noexcept
{
  ASSERT_AND_RETURN_ERROR(!engineId.empty());

  auto engine = getEngine(engineId);
  ASSERT_AND_RETURN_ERROR(engine);

  try
  {
    engine->engineWait(timeoutMsec);
  }
  catch (const std::exception& ex)
  {
    spdlog::error("AsyncConnectorApi - engineWait on engine id " + engineId +
                  ": " + std::string(ex.what()));
    return SolverError::kError;
  }
  catch (...)
  {
    spdlog::error(
        "AsyncConnectorApi - engineWait: undefined error on engine id " +
        engineId);
    return SolverError::kError;
  }

  return SolverError::kNoError;
}  // engineWait

void AsyncConnectorApi::setEngineHandler(
    const EngineCallbackHandler::SPtr& handler) noexcept
{
  pEngineHandler = handler;
}  // setEngineHandler

EngineCallbackHandler::SPtr AsyncConnectorApi::getEngineHandler() const noexcept
{
  return pEngineHandler;
}  // getEngineHandler

ConnectorEngine::SPtr AsyncConnectorApi::getEngine(
    const std::string& connectorId)
{
  {
    // Lock mutex on critical section
    LockGuard lock(pMutex);

    // Lookup the instance on the map
    if (pEngineMap.find(connectorId) != pEngineMap.end())
    {
      return pEngineMap[connectorId];
    }
  }

  spdlog::warn("AsynchConnectorApi - getEngine: return empty engine pointer");
  ConnectorEngine::SPtr emptyPtr;
  return emptyPtr;
}  // getEngine

}  // namespace optilab
