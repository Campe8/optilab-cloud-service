//
// Copyright OptiLab 2019. All rights reserved.
//
// Asynchronous API for the connector engines.
//

#pragma once

#include <memory>  // for std::shared_ptr
#include <mutex>
#include <unordered_map>
#include <vector>

#include "connector_engine/connector_engine.hpp"
#include "engine/engine_callback_handler.hpp"
#include "optimizer_api/connector_api.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS AsyncConnectorApi : public ConnectorApi {
 public:
  using SPtr = std::shared_ptr<AsyncConnectorApi>;

 public:
  /// Creates a back-end service engine with given identifier and of specific class type
  SolverError createBackendServiceEngine(const std::string& engineId,
                                         EngineClassType classType) noexcept override;

  /// Interrupts the run of back-end service engine with given identifier
  SolverError interruptBackendServiceEngine(const std::string& engineId) noexcept override;

  /// Deletes the back-end service engine with given identifier
  SolverError deleteBackendServiceEngine(const std::string& engineId) noexcept override;

  /// Runs the back-end service engine with the given identifier on the given service descriptor
  SolverError runBackendServiceEngine(const std::string& engineId,
                                      const std::string& serviceDescriptor) noexcept override;

  /// Creates and registers a new instance of a connector engine for the specified connector type
  /// and with given identifier. A connector engine is a back-end component that satisfies the
  /// requests addressed to it and its behavior depends on the type of connector.
  /// Returns a SolverError type.
  /// @note this method is thread-safe.
  /// @note this method returns an error if the same engine is registered twice.
  /// @note this method returns an error is there is no handler previously registered.
  /// @note this method never throws
  SolverError createConnector(const std::string& connectorId,
                              EngineClassType connectorType) noexcept override;

  /// Deletes the connector engine instance "connectorId" from the internal map.
  /// Returns a SolverError type.
  /// @note this method is thread-safe.
  /// @note returns an error on deleting an engine that is not registered.
  /// @note this method never throws
  SolverError deleteConnector(const std::string& connectorId) noexcept override;

  /// Loads the connections attached to the connector engine with given identifier.
  /// The connections are expressed as json models.
  /// @note this method is thread-safe.
  /// @note returns SolverError::kError if the engine with given identifier is not registered
  SolverError loadConnections(const std::string& connectorId,
                              const std::string& jsonConnections) noexcept override;

  /// Runs the connector engine "connectorId" on its loaded connections and returns asap.
  /// The engine will run the connections it has as input connections either in parallel or
  /// sequentially and it will callback the results at the end of the run according to the
  /// logic implemented by the connector.
  /// @note this method DOES NOT wait for the engine to complete running.
  /// @note this method is thread-safe.
  /// @note returns SolverError::kError if the engine with given identifier is not registered
  /// or on any other exception thrown by the engine (e.g., no model is loaded)
  SolverError runConnector(const std::string& connectorId) noexcept override;

  /// Collects the solutions produced by the engines connected to this connector engine.
  /// @note this method is thread-safe.
  /// @note returns SolverError::kError if the engine with given identifier is not registered.
  /// @note this is a blocking call waiting for the solutions from the connections
  SolverError collectSolutions(const std::string& connectorId) noexcept override;

  /// Forces the given connector engine to terminate its run (if running on the input connections).
  /// @note returns SolverError::kError if the engine with given identifier is not registered
  SolverError forceConnectorTermination(const std::string& connectorId) noexcept override;

  /// Waits for the engine with specified identifier to complete
  /// all the tasks in its queue of tasks.
  /// If "timeoutSec" is -1, wait for all the queue to be empty, otherwise wait
  /// for "timeoutSec" seconds.
  /// @note returns SolverError::kError if the engine with given identifier is not registered
  virtual SolverError engineWait(const std::string& connectorId, int timeoutSec) noexcept override;

  /// Sets the given handler as engine handler
  void setEngineHandler(const EngineCallbackHandler::SPtr& handler) noexcept override;

  /// Returns the engine handler used by this API
  EngineCallbackHandler::SPtr getEngineHandler() const noexcept override;

 private:
  /// Map of connector engines used by this combinator API.
  /// Each combinator has a list of input json models, one for each input connection
  using EngineMap = std::unordered_map<std::string, ConnectorEngine::SPtr>;

  /// Lock type on the internal mutex
  using LockGuard = std::lock_guard<std::recursive_mutex>;

 private:
  /// Map storing the instances of the combinator engines
  EngineMap pEngineMap;

  /// Mutex synchronizing on the internal map
  std::recursive_mutex pMutex;

  /// Callback handler used to send back messages to the caller of this API
  EngineCallbackHandler::SPtr pEngineHandler;

  /// Utility function: returns the instance of the connector engine registered in the map
  /// with given identifier. Returns an empty instance if no such engine is found.
  /// @note this method is thread-safe
  ConnectorEngine::SPtr getEngine(const std::string& connectorId);
};

}  // namespace optilab
