#include "optimizer_api/scip_api.hpp"

#include <spdlog/spdlog.h>

#include <cassert>
#include <exception>

#include "optilab_protobuf/linear_model.pb.h"
#include "optimizer_api/framework_api_macro.hpp"
#include "scip_engine/scip_utilities.hpp"

namespace
{
constexpr int kWaitAllTaskToCompletion = -1;
constexpr int kAllSolutions = -1;

}  // namespace

namespace optilab
{
OptimizerPkg SCIPApi::getOptimizerAPIPkg() const noexcept
{
  return OptimizerPkg::OP_SCIP;
}  // getOptimizerAPIPkg

SolverError SCIPApi::createBackendServiceEngine(
    const std::string& engineId, EngineClassType classType) noexcept
{
  return createEngine(engineId, classType);
}  // createBackendServiceEngine

SolverError SCIPApi::interruptBackendServiceEngine(
    const std::string& engineId) noexcept
{
  return forceTerminationModelRun(engineId);
}  // interruptBackendServiceEngine

SolverError SCIPApi::deleteBackendServiceEngine(
    const std::string& engineId) noexcept
{
  // First wait on unfinished tasks
  const auto err = engineWait(engineId, kWaitAllTaskToCompletion);
  CHECK_ERROR_AND_RETURN(err);

  // Then delete the engine
  spdlog::info("SCIPApi - delete backend engine " + engineId);
  return deleteEngine(engineId);
}  // deleteBackendServiceEngine

SolverError SCIPApi::runBackendServiceEngine(
    const std::string& engineId, const std::string& serviceDescriptor) noexcept
{
  // Wait on unfinished tasks
  auto err = engineWait(engineId, kWaitAllTaskToCompletion);
  CHECK_ERROR_AND_RETURN(err);

  // Load the model/descriptor into the engine
  err = loadModel(engineId, serviceDescriptor);
  CHECK_ERROR_AND_RETURN(err);

  // Wait for the model run to be finished
  err = engineWait(engineId, kWaitAllTaskToCompletion);
  CHECK_ERROR_AND_RETURN(err);

  // Run the model
  err = runModel(engineId);
  CHECK_ERROR_AND_RETURN(err);

  // Wait for the model run to be finished
  err = engineWait(engineId, kWaitAllTaskToCompletion);
  CHECK_ERROR_AND_RETURN(err);

  // Collect all the solutions found by engine as described by the model
  err = collectSolutions(engineId, kAllSolutions);
  CHECK_ERROR_AND_RETURN(err);

  // Wait for the model run to be finished
  err = engineWait(engineId, kWaitAllTaskToCompletion);

  return err;
}  // runBackendServiceEngine

SolverError SCIPApi::createEngine(const std::string& engineId,
                                  EngineClassType framework) noexcept
{
  ASSERT_AND_RETURN_ERROR(!engineId.empty());
  {
    // Lock mutex on critical section
    LockGuard lock(pMutex);
    if (pEngineMap.find(engineId) != pEngineMap.end())
    {
      // Registering twice the same engine is not allowed
      spdlog::error("SCIPApi - createEngine: engine already registered " +
                    engineId);
      return SolverError::kError;
    }

    SCIPEngine::SPtr engine;
    try
    {
      switch (framework)
      {
        case EngineClassType::EC_MIP:
          engine = std::make_shared<SCIPEngine>(engineId, pEngineHandler);
          break;
        default:
          spdlog::error("SCIPApi - createEngine: unrecognized framework");
          throw std::runtime_error("Engine framework not available");
      }
    }
    catch (const std::exception& ex)
    {
      spdlog::error("SCIPApi - createEngine on engine id " + engineId + ": " +
                    std::string(ex.what()));
      return SolverError::kError;
    }
    catch (...)
    {
      spdlog::error("SCIPApi - createEngine: undefined error on engine id " +
                    engineId);
      return SolverError::kError;
    }
    assert(engine);

    pEngineMap[engineId] = engine;
  }

  return SolverError::kNoError;
}  // createEngine

SolverError SCIPApi::deleteEngine(const std::string& engineId) noexcept
{
  ASSERT_AND_RETURN_ERROR(!engineId.empty());
  {
    // Lock mutex on critical section
    LockGuard lock(pMutex);

    // Lookup the instance on the map
    auto iter = pEngineMap.find(engineId);
    if (iter != pEngineMap.end())
    {
      SCIPEngine::SPtr engine = iter->second;
      pEngineMap.erase(iter);
      engine.reset();

      return SolverError::kNoError;
    }
  }

  // If the flow reached this point, the engine wasn't registered,
  // by default the engine has been trivially deleted
  return SolverError::kNoError;
}  // deleteEngine

SolverError SCIPApi::loadModel(const std::string& engineId,
                               const OptimizerModel& protoModel) noexcept
{
  ASSERT_AND_RETURN_ERROR(!engineId.empty());

  auto engine = getEngine(engineId);
  ASSERT_AND_RETURN_ERROR(engine);

  // SCIP engines only accept linear models
  if (!protoModel.has_linear_model())
  {
    spdlog::error(
        "SCIPApi - loadModel on engine id " + engineId +
        ": unrecognized protobuf model message (needed LinearModelSpecProto)");
    return SolverError::kError;
  }

  try
  {
    engine->registerModel(protoModel.linear_model());
  }
  catch (const std::exception& ex)
  {
    spdlog::error("SCIPApi - loadModel on engine id " + engineId + ": " +
                  std::string(ex.what()));
    return SolverError::kError;
  }
  catch (...)
  {
    spdlog::error("SCIPApi - loadModel: undefined error on engine id " +
                  engineId);
    return SolverError::kError;
  }

  return SolverError::kNoError;
}  // loadModel

SolverError SCIPApi::loadModel(const std::string& engineId,
                               const std::string& model) noexcept
{
  ASSERT_AND_RETURN_ERROR(!engineId.empty());
  ASSERT_AND_RETURN_ERROR(!model.empty());

  auto engine = getEngine(engineId);
  ASSERT_AND_RETURN_ERROR(engine);

  try
  {
    engine->registerModel(model);
  }
  catch (const std::exception& ex)
  {
    spdlog::error("SCIPApi - loadModel on engine id " + engineId + ": " +
                  std::string(ex.what()));
    return SolverError::kError;
  }
  catch (...)
  {
    spdlog::error("SCIPApi - loadModel: undefined error on engine id " +
                  engineId);
    return SolverError::kError;
  }

  return SolverError::kNoError;
}  // loadModel

SolverError SCIPApi::runModel(const std::string& engineId) noexcept
{
  ASSERT_AND_RETURN_ERROR(!engineId.empty());

  auto engine = getEngine(engineId);
  ASSERT_AND_RETURN_ERROR(engine);

  try
  {
    scipengine::SCIPEvent event =
        scipengine::SCIPEvent(scipengine::SCIPEvent::EventType::kRunEngine);
    engine->notifyEngine(event);
  }
  catch (const std::exception& ex)
  {
    spdlog::error("SCIPApi - runModel on engine id " + engineId + ": " +
                  std::string(ex.what()));
    return SolverError::kError;
  }
  catch (...)
  {
    spdlog::error("SCIPApi - runModel: undefined error on engine id " +
                  engineId);
    return SolverError::kError;
  }

  return SolverError::kNoError;
}  // runModel

SolverError SCIPApi::forceTerminationModelRun(
    const std::string& engineId) noexcept
{
  ASSERT_AND_RETURN_ERROR(!engineId.empty());

  auto engine = getEngine(engineId);

  // Without engine, it is trivially satisfied
  if (!engine) return SolverError::kNoError;
  ASSERT_AND_RETURN_ERROR(engine);

  try
  {
    scipengine::SCIPEvent event = scipengine::SCIPEvent(
        scipengine::SCIPEvent::EventType::kInterruptEngine);
    engine->notifyEngine(event);
  }
  catch (const std::exception& ex)
  {
    spdlog::error("SCIPApi - force termination on engine id " + engineId +
                  ": " + std::string(ex.what()));
    return SolverError::kError;
  }
  catch (...)
  {
    spdlog::error(
        "SCIPApi - force interruption: undefined error on engine id " +
        engineId);
    return SolverError::kError;
  }

  return SolverError::kNoError;
}  // forceTerminationModelRun

SolverError SCIPApi::collectSolutions(const std::string& engineId,
                                      int numSol) noexcept
{
  ASSERT_AND_RETURN_ERROR(!engineId.empty());

  auto engine = getEngine(engineId);
  ASSERT_AND_RETURN_ERROR(engine);

  try
  {
    scipengine::SCIPEvent event =
        scipengine::SCIPEvent(scipengine::SCIPEvent::EventType::kSolutions);
    event.setNumSolutions(numSol);
    engine->notifyEngine(event);
  }
  catch (const std::exception& ex)
  {
    spdlog::error("SCIPApi - collectSolutions on engine id " + engineId + ": " +
                  std::string(ex.what()));
    return SolverError::kError;
  }
  catch (...)
  {
    spdlog::error("SCIPApi - collectSolutions: undefined error on engine id " +
                  engineId);
    return SolverError::kError;
  }

  return SolverError::kNoError;
}  // collectSolutions

SolverError SCIPApi::engineWait(const std::string& engineId,
                                int timeoutMsec) noexcept
{
  ASSERT_AND_RETURN_ERROR(!engineId.empty());

  auto engine = getEngine(engineId);
  ASSERT_AND_RETURN_ERROR(engine);

  try
  {
    engine->engineWait(timeoutMsec);
  }
  catch (const std::exception& ex)
  {
    spdlog::error("SCIPApi - engineWait on engine id " + engineId + ": " +
                  std::string(ex.what()));
    return SolverError::kError;
  }
  catch (...)
  {
    spdlog::error("SCIPApi - engineWait: undefined error on engine id " +
                  engineId);
    return SolverError::kError;
  }

  return SolverError::kNoError;
}  // engineWait

void SCIPApi::setEngineHandler(
    const EngineCallbackHandler::SPtr& handler) noexcept
{
  pEngineHandler = handler;
}  // setEngineHandler

EngineCallbackHandler::SPtr SCIPApi::getEngineHandler() const noexcept
{
  return pEngineHandler;
}  // getEngineHandler

SCIPEngine::SPtr SCIPApi::getEngine(const std::string& engineId)
{
  {
    // Lock mutex on critical section
    LockGuard lock(pMutex);

    // Lookup the instance on the map
    if (pEngineMap.find(engineId) != pEngineMap.end())
    {
      return pEngineMap[engineId];
    }
  }

  spdlog::warn("SCIPApi - getEngine: return empty engine pointer");
  SCIPEngine::SPtr emptyPtr;
  return emptyPtr;
}  // getEngine

}  // namespace optilab
