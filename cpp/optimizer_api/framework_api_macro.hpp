//
// Copyright OptiLab 2019. All rights reserved.
//
// Collection of macros for the APIs.
//

#pragma once

#define UNUSED_ARG(x) (void)(x)

#define ASSERT_AND_RETURN_ERROR(expr)         \
  do                                          \
  {                                           \
    if (!(expr)) return SolverError::kError;  \
  } while (0)

#define CHECK_ERROR_AND_RETURN(err)      \
  do                                     \
  {                                      \
    if ((err) != SolverError::kNoError)  \
    {                                    \
      return err;                        \
    }                                    \
  } while (0)
