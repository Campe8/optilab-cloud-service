//
// Copyright OptiLab 2019. All rights reserved.
//
// OR-Tools API for the solver.
//

#pragma once

#include <memory>  // for std::shared_ptr
#include <mutex>
#include <string>
#include <unordered_map>

#include "engine/engine_callback_handler.hpp"
#include "engine/engine_constants.hpp"
#include "optilab_protobuf/optimizer_model.pb.h"
#include "optimizer_api/solver_api.hpp"
#include "or_tools_engine/or_tools_engine.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

/**
 * General architecture for the OR-Tools solver API is as follows
 *
 *             +-----+
 *             | API |
 *             +-----+           +------------------+
 *                ^              | ORToolsLPEngine  |<>--+
 *                |              +------------------+    |
 *          +------------+       +------------------+    |  +------------------+    +-----------+
 *          | ORToolsAPI |<>-----| ORToolsCPEngine  |<>--+--| OrToolsOptimizer |<>--| Processor |
 *          +------------+       +------------------+    |  +------------------+    +-----------+
 *                               +------------------+    |
 *                               | ORToolsMIPEngine |<>--+
 *                               +------------------+
 *                                       ...
 *
 *
 */

class SYS_EXPORT_CLASS ORToolsApi : public SolverApi {
 public:
  using SPtr = std::shared_ptr<ORToolsApi>;

 public:
  /// Returns this API package
  OptimizerPkg getOptimizerAPIPkg() const noexcept override;

  /// Creates a back-end service engine with given identifier and of specific class type
  SolverError createBackendServiceEngine(const std::string& engineId,
                                         EngineClassType classType) noexcept override;

  /// Interrupts the run of back-end service engine with given identifier
  SolverError interruptBackendServiceEngine(const std::string& engineId) noexcept override;

  /// Deletes the back-end service engine with given identifier
  SolverError deleteBackendServiceEngine(const std::string& engineId) noexcept override;

  /// Runs the back-end service engine with the given identifier on the given service descriptor
  SolverError runBackendServiceEngine(const std::string& engineId,
                                      const std::string& serviceDescriptor) noexcept override;

  /// Creates and registers new instance of an engine for the specified framework
  /// (e.g., CP, MIP, etc.) and with given identifier.
  /// Returns a SolverError type.
  /// @note this method is thread-safe.
  /// @note this method returns an error if the same engine is registered twice.
  /// @note this method returns an error is there is no handler previously registered.
  /// @note this method never throws
  SolverError createEngine(const std::string& engineId,
                           EngineClassType framework) noexcept override;

  /// Deletes the engine instance "engineId" from the internal map.
  /// Returns a SolverError type.
  /// @note this method is thread-safe.
  /// @note returns an error on deleting an engine that is not registered.
  /// @note this method never throws
  SolverError deleteEngine(const std::string& engineId) noexcept override;

  /// Loads the given model represented as a protobuf message into the engine with given identifier.
  /// @note this method is thread-safe.
  /// @note returns SolverError::kError if the engine with given identifier is not registered.
  SolverError loadModel(const std::string& engineId,
                        const OptimizerModel& protoModel) noexcept override;

  /// Loads the model into the engine with given identifier.
  /// @note this method is thread-safe.
  /// @note returns SolverError::kError if the engine with given identifier is not registered.
  SolverError loadModel(const std::string& engineId, const std::string& model) noexcept override;

  /// Runs the engine "aEngineId" on its loaded model and returns asap.
  /// @note this method DOES NOT wait for the engine to complete running.
  /// @note this method is thread-safe.
  /// @note returns SolverError::kError if the engine with given identifier is not registered
  /// or on any other exception thrown by the engine (e.g., no model is loaded)
  SolverError runModel(const std::string& engineId) noexcept override;

  /// Forces the given engine to terminate its run (if running on a model).
  /// @note returns SolverError::kError if the engine with given identifier is not registered
  SolverError forceTerminationModelRun(const std::string& engineId) noexcept override;

  /// Collects "numSol" solutions produced by the engine with given identifier.
  /// @note if "numSol" is greater than the actual number of solutions, return the
  /// actual number of solutions.
  /// @note if "numSol" is negative, returns all solutions.
  /// @note this method DOES NOT wait for the engine to complete running.
  /// Use "engineWait(...)" method to wait for the engine to finish running.
  /// @note this method is thread-safe.
  /// @note returns SolverError::kError if the engine with given identifier is not registered
  SolverError collectSolutions(const std::string& engineId, int numSol) noexcept override;

  /// Waits for the engine with specified identifier to complete
  /// all the tasks in its queue of tasks.
  /// If "timeoutSec" is -1, wait for all the queue to be empty, otherwise wait
  /// for "timeoutSec" seconds.
  /// @note returns SolverError::kError if the engine with given identifier is not registered
  SolverError engineWait(const std::string& engineId, int timeoutSec) noexcept override;

  /// Sets the given handler as engine handler
  void setEngineHandler(const EngineCallbackHandler::SPtr& handler) noexcept override;

  /// Returns the engine handler used by this API
  EngineCallbackHandler::SPtr getEngineHandler() const noexcept override;

 private:
  /// Map of engines used by this solver API.
  /// TODO split this map into multiple sub-maps, one for each ORTools engine type
  using EngineMap = std::unordered_map<std::string, ORToolsEngine::SPtr>;

  /// Lock type on the internal mutex
  using LockGuard = std::lock_guard<std::recursive_mutex>;

 private:
  /// Map storing the instances of the engines
  EngineMap pEngineMap;

  /// Mutex synchronizing on the internal map
  std::recursive_mutex pMutex;

  /// Callback handler used to send back messages to the caller of this API
  EngineCallbackHandler::SPtr pEngineHandler;

  /// Utility function: returns the instance of the engine registered in the map
  /// with given identifier. Returns an empty instance if no such engine is found.
  /// @note this method is thread-safe
  ORToolsEngine::SPtr getEngine(const std::string& engineId);
};

}  // namespace optilab
