#include "optimizer_api/or_tools_api.hpp"

#include <spdlog/spdlog.h>

#include <cassert>
#include <exception>

#include "optimizer_api/framework_api_macro.hpp"
#include "or_tools_engine/or_tools_cp_engine.hpp"
#include "or_tools_engine/or_tools_mip_engine.hpp"
#include "or_tools_engine/or_tools_utilities.hpp"
#include "or_tools_engine/or_tools_vrp_engine.hpp"

namespace
{
constexpr int kWaitAllTaskToCompletion = -1;
constexpr int kAllSolutions = -1;

}  // namespace

namespace optilab
{
OptimizerPkg ORToolsApi::getOptimizerAPIPkg() const noexcept
{
  return OptimizerPkg::OP_OR_TOOLS;
}  // getOptimizerAPIPkg

SolverError ORToolsApi::createBackendServiceEngine(
    const std::string& engineId, EngineClassType classType) noexcept
{
  return createEngine(engineId, classType);
}  // createBackendServiceEngine

SolverError ORToolsApi::interruptBackendServiceEngine(
    const std::string& engineId) noexcept
{
  return forceTerminationModelRun(engineId);
}  // interruptBackendServiceEngine

SolverError ORToolsApi::deleteBackendServiceEngine(
    const std::string& engineId) noexcept
{
  // First wait on unfinished tasks
  const auto err = engineWait(engineId, kWaitAllTaskToCompletion);
  CHECK_ERROR_AND_RETURN(err);

  // Then delete the engine
  spdlog::info("ORToolsApi - delete backend engine " + engineId);
  return deleteEngine(engineId);
}  // deleteBackendServiceEngine

SolverError ORToolsApi::runBackendServiceEngine(
    const std::string& engineId, const std::string& serviceDescriptor) noexcept
{
  // Wait on unfinished tasks
  auto err = engineWait(engineId, kWaitAllTaskToCompletion);
  CHECK_ERROR_AND_RETURN(err);

  // Load the model/descriptor into the engine
  err = loadModel(engineId, serviceDescriptor);
  CHECK_ERROR_AND_RETURN(err);

  // Wait for the model run to be finished
  err = engineWait(engineId, kWaitAllTaskToCompletion);
  CHECK_ERROR_AND_RETURN(err);

  // Run the model
  err = runModel(engineId);
  CHECK_ERROR_AND_RETURN(err);

  // Wait for the model run to be finished
  err = engineWait(engineId, kWaitAllTaskToCompletion);
  CHECK_ERROR_AND_RETURN(err);

  // Collect all the solutions found by engine as described by the model
  err = collectSolutions(engineId, kAllSolutions);
  CHECK_ERROR_AND_RETURN(err);

  // Wait for the model run to be finished
  err = engineWait(engineId, kWaitAllTaskToCompletion);

  return err;
}  // runBackendServiceEngine

SolverError ORToolsApi::createEngine(const std::string& engineId,
                                     EngineClassType framework) noexcept
{
  ASSERT_AND_RETURN_ERROR(!engineId.empty());
  {
    // Lock mutex on critical section
    LockGuard lock(pMutex);
    if (pEngineMap.find(engineId) != pEngineMap.end())
    {
      // Registering twice the same engine is not allowed
      spdlog::error("ORToolsApi - createEngine: engine already registered " +
                    engineId);
      return SolverError::kError;
    }

    ORToolsEngine::SPtr engine;
    try
    {
      switch (framework)
      {
        case EngineClassType::EC_CP:
          engine = std::make_shared<ORToolsCPEngine>(engineId, pEngineHandler);
          break;
        case EngineClassType::EC_MIP:
          engine = std::make_shared<ORToolsMIPEngine>(engineId, pEngineHandler);
          break;
        case EngineClassType::EC_VRP:
          engine = std::make_shared<ORToolsVRPEngine>(engineId, pEngineHandler);
          break;
        default:
          spdlog::error("ORToolsApi - createEngine: unrecognized framework");
          throw std::runtime_error("Engine framework not available");
      }
    }
    catch (const std::exception& ex)
    {
      spdlog::error("ORToolsApi - createEngine on engine id " + engineId +
                    ": " + std::string(ex.what()));
      return SolverError::kError;
    }
    catch (...)
    {
      spdlog::error("ORToolsApi - createEngine: undefined error on engine id " +
                    engineId);
      return SolverError::kError;
    }
    assert(engine);

    pEngineMap[engineId] = engine;
  }

  return SolverError::kNoError;
}  // createEngine

SolverError ORToolsApi::deleteEngine(const std::string& engineId) noexcept
{
  ASSERT_AND_RETURN_ERROR(!engineId.empty());
  {
    // Lock mutex on critical section
    LockGuard lock(pMutex);

    // Lookup the instance on the map
    auto iter = pEngineMap.find(engineId);
    if (iter != pEngineMap.end())
    {
      ORToolsEngine::SPtr engine = iter->second;
      pEngineMap.erase(iter);
      engine.reset();

      return SolverError::kNoError;
    }
  }

  // If the flow reached this point, the engine wasn't registered,
  // by default the engine has been trivially deleted
  return SolverError::kNoError;
}  // deleteEngine

SolverError ORToolsApi::loadModel(const std::string& engineId,
                                  const OptimizerModel& protoModel) noexcept
{
  ASSERT_AND_RETURN_ERROR(!engineId.empty());

  auto engine = getEngine(engineId);
  ASSERT_AND_RETURN_ERROR(engine);

  try
  {
    engine->registerModel(protoModel);
  }
  catch (const std::exception& ex)
  {
    spdlog::error("ORToolsApi - loadModel on engine id " + engineId + ": " +
                  std::string(ex.what()));
    return SolverError::kError;
  }
  catch (...)
  {
    spdlog::error("ORToolsApi - loadModel: undefined error on engine id " +
                  engineId);
    return SolverError::kError;
  }

  return SolverError::kNoError;
}  // loadModel

SolverError ORToolsApi::loadModel(const std::string& engineId,
                                  const std::string& model) noexcept
{
  ASSERT_AND_RETURN_ERROR(!engineId.empty());
  ASSERT_AND_RETURN_ERROR(!model.empty());

  auto engine = getEngine(engineId);
  ASSERT_AND_RETURN_ERROR(engine);

  try
  {
    engine->registerModel(model);
  }
  catch (const std::exception& ex)
  {
    spdlog::error("ORToolsApi - loadModel on engine id " + engineId + ": " +
                  std::string(ex.what()));
    return SolverError::kError;
  }
  catch (...)
  {
    spdlog::error("ORToolsApi - loadModel: undefined error on engine id " +
                  engineId);
    return SolverError::kError;
  }

  return SolverError::kNoError;
}  // loadModel

SolverError ORToolsApi::runModel(const std::string& engineId) noexcept
{
  ASSERT_AND_RETURN_ERROR(!engineId.empty());

  auto engine = getEngine(engineId);
  ASSERT_AND_RETURN_ERROR(engine);

  try
  {
    ortoolsengine::ORToolsEvent event = ortoolsengine::ORToolsEvent(
        ortoolsengine::ORToolsEvent::EventType::kRunEngine);
    engine->notifyEngine(event);
  }
  catch (const std::exception& ex)
  {
    spdlog::error("ORToolsApi - runModel on engine id " + engineId + ": " +
                  std::string(ex.what()));
    return SolverError::kError;
  }
  catch (...)
  {
    spdlog::error("ORToolsApi - runModel: undefined error on engine id " +
                  engineId);
    return SolverError::kError;
  }

  return SolverError::kNoError;
}  // runModel

SolverError ORToolsApi::forceTerminationModelRun(
    const std::string& engineId) noexcept
{
  ASSERT_AND_RETURN_ERROR(!engineId.empty());

  auto engine = getEngine(engineId);

  // Without engine, it is trivially satisfied
  if (!engine) return SolverError::kNoError;
  ASSERT_AND_RETURN_ERROR(engine);

  try
  {
    ortoolsengine::ORToolsEvent event = ortoolsengine::ORToolsEvent(
        ortoolsengine::ORToolsEvent::EventType::kInterruptEngine);
    engine->notifyEngine(event);
  }
  catch (const std::exception& ex)
  {
    spdlog::error("ORToolsApi - force termination on engine id " + engineId +
                  ": " + std::string(ex.what()));
    return SolverError::kError;
  }
  catch (...)
  {
    spdlog::error(
        "ORToolsApi - force interruption: undefined error on engine id " +
        engineId);
    return SolverError::kError;
  }

  return SolverError::kNoError;
}  // forceTerminationModelRun

SolverError ORToolsApi::collectSolutions(const std::string& engineId,
                                         int numSol) noexcept
{
  ASSERT_AND_RETURN_ERROR(!engineId.empty());

  auto engine = getEngine(engineId);
  ASSERT_AND_RETURN_ERROR(engine);

  try
  {
    ortoolsengine::ORToolsEvent event = ortoolsengine::ORToolsEvent(
        ortoolsengine::ORToolsEvent::EventType::kSolutions);
    event.setNumSolutions(numSol);
    engine->notifyEngine(event);
  }
  catch (const std::exception& ex)
  {
    spdlog::error("ORToolsApi - collectSolutions on engine id " + engineId +
                  ": " + std::string(ex.what()));
    return SolverError::kError;
  }
  catch (...)
  {
    spdlog::error(
        "ORToolsApi - collectSolutions: undefined error on engine id " +
        engineId);
    return SolverError::kError;
  }

  return SolverError::kNoError;
}  // collectSolutions

SolverError ORToolsApi::engineWait(const std::string& engineId,
                                   int timeoutMsec) noexcept
{
  ASSERT_AND_RETURN_ERROR(!engineId.empty());

  auto engine = getEngine(engineId);
  ASSERT_AND_RETURN_ERROR(engine);

  try
  {
    engine->engineWait(timeoutMsec);
  }
  catch (const std::exception& ex)
  {
    spdlog::error("ORToolsApi - engineWait on engine id " + engineId + ": " +
                  std::string(ex.what()));
    return SolverError::kError;
  }
  catch (...)
  {
    spdlog::error("ORToolsApi - engineWait: undefined error on engine id " +
                  engineId);
    return SolverError::kError;
  }

  return SolverError::kNoError;
}  // engineWait

void ORToolsApi::setEngineHandler(
    const EngineCallbackHandler::SPtr& handler) noexcept
{
  pEngineHandler = handler;
}  // setEngineHandler

EngineCallbackHandler::SPtr ORToolsApi::getEngineHandler() const noexcept
{
  return pEngineHandler;
}  // getEngineHandler

ORToolsEngine::SPtr ORToolsApi::getEngine(const std::string& engineId)
{
  {
    // Lock mutex on critical section
    LockGuard lock(pMutex);

    // Lookup the instance on the map
    if (pEngineMap.find(engineId) != pEngineMap.end())
    {
      return pEngineMap[engineId];
    }
  }

  spdlog::warn("ORToolsApi - getEngine: return empty engine pointer");
  ORToolsEngine::SPtr emptyPtr;
  return emptyPtr;
}  // getEngine

}  // namespace optilab
