//
// Copyright OptiLab 2019. All rights reserved.
//
// Base class for all APIs used by the optimizer.
// Each framework has its own set of APIs
//

#pragma once

#include <memory>
#include <unordered_map>
#include <vector>

#include "engine/engine_callback_handler.hpp"
#include "engine/engine_constants.hpp"
#include "optimizer_api/framework_api_macro.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

enum SolverError {
  kNoError = 0,
  kError
};

class SYS_EXPORT_CLASS FrameworkApi {
 public:
  using SPtr = std::shared_ptr<FrameworkApi>;
  using FrameworkApiMap = std::unordered_map<int, std::vector<FrameworkApi::SPtr>>;
  using FrameworkApiMapSPtr = std::shared_ptr<FrameworkApiMap>;

 public:
  virtual ~FrameworkApi() = default;

  /**
   * The following set of APIs are used for back-end services,
   * i.e., for sub-workers to run work descriptors received from the
   * "main workers" that need to run parallel services.
   * @note although the following APIs don't specify whether the behavior should
   * be asynchronous or not, the derived APIs implementing these methods should
   * take into consideration that the FrameworkApi doesn't have any synchronization method.
   * In other words, the general "usage" of the following APIs -- from the client/
   * request resolver point of view -- is the following:
   *
   * void runRequest() {
   *   // Create the engine for the given class
   *   createBackendServiceEngine(...);
   *
   *   // Run the engine just created
   *   runBackendServiceEngine(...);
   *
   *   (// If needed, interrupt the running engine
   *    interruptBackendServiceEngine(...))
   *
   *   // Delete the engine
   *   deleteBackendServiceEngine(...);
   * }
   *
   * Note that there is no wait between call to the run and the call to the delete engine.
   * This means that the implementation of the APIs must either wait on the run delete
   * before returning to the caller or queue the request and return.
   * The callback to the handler is asynchronous and it will send back the results
   * when ready.
   */

  /// Creates a back-end service engine with given identifier and of specific class type
  virtual SolverError createBackendServiceEngine(const std::string& engineId,
                                                 EngineClassType classType) noexcept
  {
    UNUSED_ARG(engineId);
    UNUSED_ARG(classType);
    return SolverError::kNoError;
  }

  /// Interrupts the run of back-end service engine with given identifier
  virtual SolverError interruptBackendServiceEngine(const std::string& engineId) noexcept
  {
    UNUSED_ARG(engineId);
    return SolverError::kNoError;
  }

  /// Deletes the back-end service engine with given identifier
  virtual SolverError deleteBackendServiceEngine(const std::string& engineId) noexcept
  {
    UNUSED_ARG(engineId);
    return SolverError::kNoError;
  }

  /// Runs the back-end service engine with the given identifier on the given service descriptor
  virtual SolverError runBackendServiceEngine(const std::string& engineId,
                                              const std::string& serviceDescriptor) noexcept
  {
    UNUSED_ARG(engineId);
    UNUSED_ARG(serviceDescriptor);
    return SolverError::kNoError;
  }

  /**
   * The following set of APIs are generic APIs that each sub-framework API
   * must implement.
   */

  /// Returns the framework type of this API
  virtual EngineFrameworkType getFrameworkAPIType() const noexcept = 0;

  /// Sets the given handler as engine handler
  virtual void setEngineHandler(const EngineCallbackHandler::SPtr& handler) noexcept = 0;

  /// Returns the engine handler used by this API
  virtual EngineCallbackHandler::SPtr getEngineHandler() const noexcept = 0;
};

}  // namespace optilab
