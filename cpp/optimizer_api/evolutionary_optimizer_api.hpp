//
// Copyright OptiLab 2019. All rights reserved.
//
// API interface for a generic evolutionary computation optimizer.
// @note "engine" and "optimizer" have the same meaning in this context.
//

#pragma once

#include "optimizer_api/framework_api.hpp"

#include <memory>  // for std::shared_ptr
#include <string>
#include <unordered_map>

#include "engine/engine_constants.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS EvolutionaryOptimizerApi : public FrameworkApi {
 public:
  using SPtr = std::shared_ptr<EvolutionaryOptimizerApi>;
  using OptimizerApiMap = std::unordered_map<int, EvolutionaryOptimizerApi::SPtr>;
  using OptimizerApiMapSPtr = std::shared_ptr<OptimizerApiMap>;

 public:
  virtual ~EvolutionaryOptimizerApi() = default;

  /// Returns the framework type of this API
  EngineFrameworkType getFrameworkAPIType() const noexcept override
  {
    return EngineFrameworkType::EC_FRAMEWORK;
  }

  /// Returns this API package
  virtual OptimizerPkg getOptimizerAPIPkg() const noexcept = 0;

  /// Creates and registers new instance of an evolutionary optimizer for the specified framework
  /// (e.g., GA, Ant Colony, etc.) and with given identifier.
  /// Returns a SolverError type.
  /// @note this method is thread-safe.
  /// @note this method returns an error if the same optimizer is registered twice.
  /// @note this method returns an error is there is no handler previously registered.
  /// @note this method never throws
  virtual SolverError createOptimizer(const std::string& optId,
                                      EngineClassType framework) noexcept = 0;

  /// Deletes the optimizer instance "engineId" from the internal map.
  /// Returns a SolverError type.
  /// @note this method is thread-safe.
  /// @note returns an error on deleting an optimizer that is not registered.
  /// @note this method never throws
  virtual SolverError deleteOptimizer(const std::string& optId) noexcept = 0;

  /// Loads the given environment represented as a JSON string into the
  /// optimizer with given identifier.
  /// @note this method is thread-safe.
  /// @note the environment represents the "model" (in OR terms) the optimizer will run on.
  /// @note returns SolverError::kError if the optimizer with given identifier is not registered
  virtual SolverError loadEnvironment(const std::string& optId,
                                      const std::string& env) noexcept = 0;

  /// Runs the optimizer "optId" on its loaded environment and returns asap.
  /// @note this method DOES NOT wait for the optimizer to complete running.
  /// @note this method is thread-safe.
  /// @note returns SolverError::kError if the engine with given identifier is not registered
  /// or on any other exception thrown by the engine (e.g., no model is loaded)
  virtual SolverError runEnvironment(const std::string& optId) noexcept = 0;

  /// Forces the given optimizer to terminate its run (if running on an environment).
  /// @note returns SolverError::kError if the optimizer with given identifier is not registered
  virtual SolverError forceTerminationEnvironmentRun(const std::string& optId) noexcept = 0;

  /// Collects the optimized solution produced by the optimizer with given identifier.
  /// @note this method DOES NOT wait for the optimizer to complete running.
  /// Use "engineWait(...)" method to wait for the engine to finish running.
  /// @note this method is thread-safe.
  /// @note returns SolverError::kError if the engine with given identifier is not registered
  virtual SolverError collectSolutions(const std::string& optId) noexcept = 0;

  /// Waits for the optimizer with specified identifier to complete
  /// all the tasks in its queue of tasks.
  /// If "timeoutSec" is -1, wait for all the queue to be empty, otherwise wait
  /// for "timeoutSec" seconds.
  /// @note returns SolverError::kError if the optimizer with given identifier is not registered
  virtual SolverError engineWait(const std::string& optId, int timeoutSec) noexcept = 0;
};

}  // namespace optilab
