//
// Copyright OptiLab 2020. All rights reserved.
//
// Data and ports API for the solvers.
//

#pragma once

#include <memory>  // for std::shared_ptr
#include <string>
#include <unordered_map>

#include <boost/thread.hpp>

#include "engine/engine_callback_handler.hpp"
#include "engine/engine_constants.hpp"
#include "optimizer_api/framework_api.hpp"
#include "data_toolbox/data_engine.hpp"
#include "data_toolbox/data_instance.hpp"
#include "data_toolbox/data_utilities.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

/**
 * General architecture for the Evolutionary API is as follows
 *
 *             +-----+
 *             | API |
 *             +-----+        +-------------+
 *                ^           | ExcelEngine |<>---+
 *                |           +-------------+     |
 *          +---------+       +--------------+    |
 *          | DataAPI |<>-----| PythonEngine |<>--+
 *          +---------+       +--------------+    |
 *
 *                                         ...        ...
 *
 *
 */

class SYS_EXPORT_CLASS DataApi : public FrameworkApi {
 public:
  using SPtr = std::shared_ptr<DataApi>;

 public:
  /// Returns the framework type of this API
  EngineFrameworkType getFrameworkAPIType() const noexcept override
  {
    return EngineFrameworkType::DATA_AND_PORTS_TOOLBOX;
  }

  /// Creates and registers new instance of an engine running the specified processor type
  /// and with given identifier.
  /// Returns a SolverError type.
  /// @note this method is thread-safe.
  /// @note this method returns an error if the same engine is registered twice.
  /// @note this method returns an error is there is no handler previously registered.
  /// @note this method never throws
  SolverError createEngine(const std::string& engineId,
                           toolbox::dataengine::DataProcessorType procType) noexcept;

  /// Deletes the engine instance "engineId" from the internal map.
  /// Returns a SolverError type.
  /// @note this method is thread-safe.
  /// @note returns an error on deleting an engine that is not registered.
  /// @note this method never throws
  SolverError deleteEngine(const std::string& engineId) noexcept;

  /// Loads the given model (problem) instance represented as a protocol buffer message
  /// into the engine with given identifier.
  /// @note this method is thread-safe.
  /// @note returns SolverError::kError if the engine with given identifier is not registered.
  SolverError loadModelInstance(const std::string& engineId,
                                toolbox::DataInstance::UPtr instance) noexcept;

  /// Runs the engine "engineId" on its loaded model instance and returns asap.
  /// The caller can specify and instance to run on-the-fly.
  /// @note this method DOES NOT wait for the engine to complete the search.
  /// @note this method is thread-safe.
  /// @note returns SolverError::kError if the engine with given identifier is not registered
  /// or on any other exception thrown by the engine (e.g., no model is loaded)
  SolverError runEngine(const std::string& engineId,
                        toolbox::DataInstance::UPtr instance = nullptr) noexcept;

  /// Forces the given engine to terminate its run (if running on a model instance).
  /// @note returns SolverError::kError if the engine with given identifier is not registered
  SolverError interruptEngine(const std::string& engineId) noexcept;

  /// Collects "numSol" solutions produced by the engine with given identifier.
  /// @note if "numSol" is greater than the actual number of solutions, return the
  /// actual number of solutions.
  /// @note if "numSol" is negative, returns all solutions.
  /// @note this method DOES NOT wait for the engine to complete running.
  /// Use "engineWait(...)" method to wait for the engine to finish running.
  /// @note this method is thread-safe.
  /// @note returns SolverError::kError if the engine with given identifier is not registered
  SolverError collectSolutions(const std::string& engineId, int numSol) noexcept;

  /// Waits for the engine with specified identifier to complete
  /// all the tasks in its queue of tasks.
  /// If "timeoutSec" is -1, wait for all the queue to be empty, otherwise wait
  /// for "timeoutSec" seconds.
  /// @note returns SolverError::kError if the engine with given identifier is not registered
  SolverError engineWait(const std::string& engineId, int timeoutSec) noexcept;

  /// Sets the given handler as engine handler
  void setEngineHandler(const EngineCallbackHandler::SPtr& handler) noexcept override;

  /// Returns the engine handler used by this API
  EngineCallbackHandler::SPtr getEngineHandler() const noexcept override;

 private:
  /// Map of engines used by this solver API
  using EngineMap = std::unordered_map<std::string, toolbox::DataEngine::SPtr>;

  /// Lock type on the internal mutex
  using LockGuard = boost::lock_guard<boost::recursive_mutex>;

 private:
  /// Map storing the instances of the engines
  EngineMap pEngineMap;

  /// Mutex synchronizing on the internal map
  boost::recursive_mutex pMutex;

  /// Callback handler used to send back messages to the caller of this API
  EngineCallbackHandler::SPtr pEngineHandler;

  /// Utility function: returns the instance of the engine registered in the map
  /// with given identifier. Returns an empty instance if no such engine is found.
  /// @note this method is thread-safe
  toolbox::DataEngine::SPtr getEngine(const std::string& engineId);
};

}  // namespace optilab
