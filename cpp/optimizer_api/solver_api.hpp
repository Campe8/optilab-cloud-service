//
// Copyright OptiLab 2019. All rights reserved.
//
// API interface for a solver.
//

#pragma once

#include <memory>
#include <string>
#include <unordered_map>

#include "engine/engine_constants.hpp"
#include "optilab_protobuf/optimizer_model.pb.h"
#include "optimizer_api/framework_api.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS SolverApi : public FrameworkApi {
 public:
  using SPtr = std::shared_ptr<SolverApi>;
  using OptimizerApiMap = std::unordered_map<int, SolverApi::SPtr>;
  using OptimizerApiMapSPtr = std::shared_ptr<OptimizerApiMap>;

 public:
  virtual ~SolverApi() = default;

  /// Returns the framework type of this API
  EngineFrameworkType getFrameworkAPIType() const noexcept override
  {
    return EngineFrameworkType::OR_FRAMEWORK;
  }

  /// Returns this API package
  virtual OptimizerPkg getOptimizerAPIPkg() const noexcept = 0;

  /// Creates and registers new instance of an engine for the specified framework
  /// (e.g., CP, MIP, etc.) and with given identifier.
  /// Returns a SolverError type.
  /// @note this method is thread-safe.
  /// @note this method returns an error if the same engine is registered twice.
  /// @note this method returns an error is there is no handler previously registered.
  /// @note this method never throws
  virtual SolverError createEngine(const std::string& engineId,
                                   EngineClassType framework) noexcept = 0;

  /// Deletes the engine instance "engineId" from the internal map.
  /// Returns a SolverError type.
  /// @note this method is thread-safe.
  /// @note returns an error on deleting an engine that is not registered.
  /// @note this method never throws
  virtual SolverError deleteEngine(const std::string& engineId) noexcept = 0;

  /// Loads the given model represented as a protobuf message into the engine with given identifier.
  /// @note this method is thread-safe.
  /// @note returns SolverError::kError if the engine with given identifier is not registered.
  /// @note this method is specific for this API
  virtual SolverError loadModel(const std::string& engineId,
                                const OptimizerModel& protoModel) noexcept = 0;

  /// Loads the given model represented as a JSON string into the engine with given identifier.
  /// @note this method is thread-safe.
  /// @note returns SolverError::kError if the engine with given identifier is not registered
  virtual SolverError loadModel(const std::string& engineId, const std::string& model) noexcept = 0;

  /// Runs the engine "aEngineId" on its loaded model and returns asap.
  /// @note this method DOES NOT wait for the engine to complete running.
  /// @note this method is thread-safe.
  /// @note returns SolverError::kError if the engine with given identifier is not registered
  /// or on any other exception thrown by the engine (e.g., no model is loaded)
  virtual SolverError runModel(const std::string& engineId) noexcept = 0;

  /// Forces the given engine to terminate its run (if running on a model).
  /// @note returns SolverError::kError if the engine with given identifier is not registered
  virtual SolverError forceTerminationModelRun(const std::string& engineId) noexcept = 0;

  /// Collects "numSol" solutions produced by the engine with given identifier.
  /// @note if "numSol" is greater than the actual number of solutions, return the
  /// actual number of solutions.
  /// @note if "numSol" is negative, returns all solutions.
  /// @note this method DOES NOT wait for the engine to complete running.
  /// Use "engineWait(...)" method to wait for the engine to finish running.
  /// @note this method is thread-safe.
  /// @note returns SolverError::kError if the engine with given identifier is not registered
  virtual SolverError collectSolutions(const std::string& engineId, int numSol) noexcept = 0;

  /// Waits for the engine with specified identifier to complete
  /// all the tasks in its queue of tasks.
  /// If "timeoutSec" is -1, wait for all the queue to be empty, otherwise wait
  /// for "timeoutSec" seconds.
  /// @note returns SolverError::kError if the engine with given identifier is not registered
  virtual SolverError engineWait(const std::string& engineId, int timeoutSec) noexcept = 0;
};

}  // namespace optilab
