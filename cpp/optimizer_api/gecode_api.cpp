#include "optimizer_api/gecode_api.hpp"

#include <spdlog/spdlog.h>

#include <cassert>
#include <exception>

#include "gecode_engine/gecode_utilities.hpp"
#include "optimizer_api/framework_api_macro.hpp"

namespace
{
constexpr int kWaitAllTaskToCompletion = -1;
constexpr int kAllSolutions = -1;

}  // namespace

namespace optilab
{
OptimizerPkg GecodeApi::getOptimizerAPIPkg() const noexcept
{
  return OptimizerPkg::OP_GECODE;
}  // getOptimizerAPIPkg

SolverError GecodeApi::createBackendServiceEngine(
    const std::string& engineId, EngineClassType classType) noexcept
{
  return createEngine(engineId, classType);
}  // createBackendServiceEngine

SolverError GecodeApi::interruptBackendServiceEngine(
    const std::string& engineId) noexcept
{
  return forceTerminationModelRun(engineId);
}  // interruptBackendServiceEngine

SolverError GecodeApi::deleteBackendServiceEngine(
    const std::string& engineId) noexcept
{
  // First wait on unfinished tasks
  const auto err = engineWait(engineId, kWaitAllTaskToCompletion);
  CHECK_ERROR_AND_RETURN(err);

  // Then delete the engine
  spdlog::info("GecodeApi - delete backend engine " + engineId);
  return deleteEngine(engineId);
}  // deleteBackendServiceEngine

SolverError GecodeApi::runBackendServiceEngine(
    const std::string& engineId, const std::string& serviceDescriptor) noexcept
{
  // Wait on unfinished tasks
  auto err = engineWait(engineId, kWaitAllTaskToCompletion);
  CHECK_ERROR_AND_RETURN(err);

  // Load the model/descriptor into the engine
  err = loadModel(engineId, serviceDescriptor);
  CHECK_ERROR_AND_RETURN(err);

  // Wait for the model run to be finished
  err = engineWait(engineId, kWaitAllTaskToCompletion);
  CHECK_ERROR_AND_RETURN(err);

  // Run the model
  err = runModel(engineId);
  CHECK_ERROR_AND_RETURN(err);

  // Wait for the model run to be finished
  err = engineWait(engineId, kWaitAllTaskToCompletion);
  CHECK_ERROR_AND_RETURN(err);

  // Collect all the solutions found by engine as described by the model
  err = collectSolutions(engineId, kAllSolutions);
  CHECK_ERROR_AND_RETURN(err);

  // Wait for the model run to be finished
  err = engineWait(engineId, kWaitAllTaskToCompletion);

  return err;
}  // runBackendServiceEngine

SolverError GecodeApi::createEngine(const std::string& engineId,
                                    EngineClassType framework) noexcept
{
  ASSERT_AND_RETURN_ERROR(!engineId.empty());
  {
    // Lock mutex on critical section
    LockGuard lock(pMutex);
    if (pEngineMap.find(engineId) != pEngineMap.end())
    {
      // Registering twice the same engine is not allowed
      spdlog::error("ORToolsApi - createEngine: engine already registered " +
                    engineId);
      return SolverError::kError;
    }

    GecodeEngine::SPtr engine;
    try
    {
      switch (framework)
      {
        case EngineClassType::EC_CP:
          engine = std::make_shared<GecodeEngine>(engineId, pEngineHandler);
          break;
        default:
          spdlog::error("GecodeApi - createEngine: unrecognized framework");
          throw std::runtime_error("Engine framework not available");
      }
    }
    catch (const std::exception& ex)
    {
      spdlog::error("GecodeApi - createEngine on engine id " + engineId + ": " +
                    std::string(ex.what()));
      return SolverError::kError;
    }
    catch (...)
    {
      spdlog::error("GecodeApi - createEngine: undefined error on engine id " +
                    engineId);
      return SolverError::kError;
    }
    assert(engine);

    pEngineMap[engineId] = engine;
  }

  return SolverError::kNoError;
}  // createEngine

SolverError GecodeApi::deleteEngine(const std::string& engineId) noexcept
{
  ASSERT_AND_RETURN_ERROR(!engineId.empty());
  {
    // Lock mutex on critical section
    LockGuard lock(pMutex);

    // Lookup the instance on the map
    auto iter = pEngineMap.find(engineId);
    if (iter != pEngineMap.end())
    {
      GecodeEngine::SPtr engine = iter->second;
      pEngineMap.erase(iter);
      engine.reset();

      return SolverError::kNoError;
    }
  }

  // If the flow reached this point, the engine wasn't registered,
  // by default the engine has been trivially deleted
  return SolverError::kNoError;
}  // deleteEngine

SolverError GecodeApi::loadModel(const std::string& engineId,
                                 const OptimizerModel& protoModel) noexcept
{
  (void)engineId;
  (void)protoModel;
  spdlog::error(
      "GecodeApi - loadModel: loading a protobuf model is not yet supported");
  return SolverError::kError;
}  // loadModel

SolverError GecodeApi::loadModel(const std::string& engineId,
                                 const std::string& model) noexcept
{
  ASSERT_AND_RETURN_ERROR(!engineId.empty());
  ASSERT_AND_RETURN_ERROR(!model.empty());

  auto engine = getEngine(engineId);
  ASSERT_AND_RETURN_ERROR(engine);

  try
  {
    engine->registerModel(model);
  }
  catch (const std::exception& ex)
  {
    spdlog::error("GecodeApi - loadModel on engine id " + engineId + ": " +
                  std::string(ex.what()));
    return SolverError::kError;
  }
  catch (...)
  {
    spdlog::error("GecodeApi - loadModel: undefined error on engine id " +
                  engineId);
    return SolverError::kError;
  }

  return SolverError::kNoError;
}  // loadModel

SolverError GecodeApi::runModel(const std::string& engineId) noexcept
{
  ASSERT_AND_RETURN_ERROR(!engineId.empty());

  auto engine = getEngine(engineId);
  ASSERT_AND_RETURN_ERROR(engine);

  try
  {
    gecodeengine::GecodeEvent event = gecodeengine::GecodeEvent(
        gecodeengine::GecodeEvent::EventType::kRunEngine);
    engine->notifyEngine(event);
  }
  catch (const std::exception& ex)
  {
    spdlog::error("GecodeApi - runModel on engine id " + engineId + ": " +
                  std::string(ex.what()));
    return SolverError::kError;
  }
  catch (...)
  {
    spdlog::error("GecodeApi - runModel: undefined error on engine id " +
                  engineId);
    return SolverError::kError;
  }

  return SolverError::kNoError;
}  // runModel

SolverError GecodeApi::forceTerminationModelRun(
    const std::string& engineId) noexcept
{
  ASSERT_AND_RETURN_ERROR(!engineId.empty());

  auto engine = getEngine(engineId);

  // Without engine, it is trivially satisfied
  if (!engine) return SolverError::kNoError;
  ASSERT_AND_RETURN_ERROR(engine);

  try
  {
    gecodeengine::GecodeEvent event = gecodeengine::GecodeEvent(
        gecodeengine::GecodeEvent::EventType::kInterruptEngine);
    engine->notifyEngine(event);
  }
  catch (const std::exception& ex)
  {
    spdlog::error("GecodeApi - force termination on engine id " + engineId +
                  ": " + std::string(ex.what()));
    return SolverError::kError;
  }
  catch (...)
  {
    spdlog::error(
        "GecodeApi - force termination: undefined error on engine id " +
        engineId);
    return SolverError::kError;
  }

  return SolverError::kNoError;
}  // forceTerminationModelRun

SolverError GecodeApi::collectSolutions(const std::string& engineId,
                                        int numSol) noexcept
{
  ASSERT_AND_RETURN_ERROR(!engineId.empty());

  auto engine = getEngine(engineId);
  ASSERT_AND_RETURN_ERROR(engine);

  try
  {
    gecodeengine::GecodeEvent event = gecodeengine::GecodeEvent(
        gecodeengine::GecodeEvent::EventType::kSolutions);
    event.setNumSolutions(numSol);
    engine->notifyEngine(event);
  }
  catch (const std::exception& ex)
  {
    spdlog::error("GecodeApi - collectSolutions on engine id " + engineId +
                  ": " + std::string(ex.what()));
    return SolverError::kError;
  }
  catch (...)
  {
    spdlog::error(
        "GecodeApi - collectSolutions: undefined error on engine id " +
        engineId);
    return SolverError::kError;
  }

  return SolverError::kNoError;
}  // collectSolutions

SolverError GecodeApi::engineWait(const std::string& engineId,
                                  int timeoutMsec) noexcept
{
  ASSERT_AND_RETURN_ERROR(!engineId.empty());

  auto engine = getEngine(engineId);
  ASSERT_AND_RETURN_ERROR(engine);

  try
  {
    engine->engineWait(timeoutMsec);
  }
  catch (const std::exception& ex)
  {
    spdlog::error("GecodeApi - engineWait on engine id " + engineId + ": " +
                  std::string(ex.what()));
    return SolverError::kError;
  }
  catch (...)
  {
    spdlog::error("GecodeApi - engineWait: undefined error on engine id " +
                  engineId);
    return SolverError::kError;
  }

  return SolverError::kNoError;
}  // engineWait

void GecodeApi::setEngineHandler(
    const EngineCallbackHandler::SPtr& handler) noexcept
{
  pEngineHandler = handler;
}  // setEngineHandler

EngineCallbackHandler::SPtr GecodeApi::getEngineHandler() const noexcept
{
  return pEngineHandler;
}  // getEngineHandler

GecodeEngine::SPtr GecodeApi::getEngine(const std::string& engineId)
{
  {
    // Lock mutex on critical section
    LockGuard lock(pMutex);

    // Lookup the instance on the map
    if (pEngineMap.find(engineId) != pEngineMap.end())
    {
      return pEngineMap[engineId];
    }
  }

  spdlog::warn("GecodeEngine - getEngine: return empty engine pointer");
  GecodeEngine::SPtr emptyPtr;
  return emptyPtr;
}  // getEngine

}  // namespace optilab
