//
// Copyright OptiLab 2020. All rights reserved.
//
// Utilities class for a CSV reader.
//

#pragma once

#include <string>
#include <vector>

#include "system/system_export_defs.hpp"

namespace optilab {
namespace utilsreader {

class SYS_EXPORT_CLASS CSVReader final {
 public:
  using CSVData = std::vector<std::vector<std::string>>;

public:
  CSVReader(const std::string& filename, std::string delim = ",");

  ~CSVReader() = default;

  /// Returns data from a CSV File
  CSVData getData();

private:
  std::string pFileName;
  std::string pDelimiter;
};

}  // namespace utilsreader
}  // optilab
