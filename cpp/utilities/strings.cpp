#include "utilities/strings.hpp"

#include <algorithm>   // for std::find_if
#include <cmath>       // for HUGE_VAL
#include <cstddef>     // for std::size_t
#include <cstdlib>     // for strtod
#include <functional>  // for std::ptr_fun
#include <sstream>

namespace {

const char kBoolTrueVal[] = "true";
const char kBoolFalseVal[] = "false";

static std::string cleanArgument(const std::string& aArg)
{
  std::size_t startIdx{ 0 };
  for (auto& c : aArg)
  {
    if ((c == ',' || c == ' '))
    {
      startIdx++;
      continue;
    }
    break;
  }

  return optilab::utilsstrings::trimmed(aArg.substr(startIdx));
}//cleanArgument

}  // namespace

namespace optilab {

namespace utilsstrings {

void ltrim(std::string& aString)
{
  aString.erase(aString.begin(),
                std::find_if(aString.begin(), aString.end(), [](int ch)
                             {
    return !std::isspace(ch);
                             }
  ));
}//ltrim

void rtrim(std::string& aString)
{
  aString.erase(std::find_if(aString.rbegin(), aString.rend(), [](int ch)
                             {
    return !std::isspace(ch);
                             }
  ).base(), aString.end());
}//rtrim

void trim(std::string& str)
{
  ltrim(str);
  rtrim(str);
}  // trim

std::string ltrimmed(std::string str)
{
  ltrim(str);
  return str;
}  // ltrimmed

std::string rtrimmed(std::string str)
{
  rtrim(str);
  return str;
}  // rtrimmed

std::string trimmed(std::string str)
{
  trim(str);
  return str;
}  // trimmed

std::vector<std::string> tokenizeStringOnSymbol(const std::string& str, char chr)
{
  std::vector<std::string> tokens;
  if (!str.empty()) {
    std::istringstream ss(str);
    std::string token;
    while (std::getline(ss, token, chr)) {
      tokens.push_back(token);
    }
  }
  return tokens;
}

std::vector<std::string> splitArguments(const std::string& aArgs)
{
  // Vector of fcn arguments to return
  std::vector<std::string> args;
  if (aArgs.empty()) { return args; }

  // Stack for counting parentheses
  int stack{ 0 };

  // Set 2 indexes for substr:
  // start and length of string to be copied
  std::size_t start{ 0 };
  std::size_t len{ 0 };
  for (auto& c : aArgs)
  {
    if (c == ',' && stack == 0)
    {
      auto cleanArg = cleanArgument(aArgs.substr(start, len));
      if (!cleanArg.empty())
      {
        args.push_back(cleanArg);
      }
      start += len;
      len = 0;
    }
    else if (c == '[')
    {
      stack++;
    }
    else if (c == ']')
    {
      stack--;
      if (stack < 0) { stack = 0; }
    }
    len++;
  }

  // Push last argument in the list
  auto cleanArg = cleanArgument(aArgs.substr(start, len));
  if (!cleanArg.empty())
  {
    args.push_back(cleanArg);
  }

  return args;
}  // splitArguments

bool isBool(const std::string& val)
{
  return val == kBoolTrueVal || val == kBoolFalseVal;
}  // isBool

bool isInt(const std::string& val)
{
  char* p;
  strtol(val.c_str(), &p, 10);
  if (*p) return false;
  return true;
}  // isInt

bool isDouble(const std::string& val)
{
  char* end = 0;
  double dval = strtod(val.c_str(), &end);
  return end != val.c_str() && *end == '\0' && dval != HUGE_VAL;
}  // isDouble

int64_t stringToInt(const std::string& val)
{
  if (isBool(val))
  {
    if (val == kBoolTrueVal) return 1;
    if (val == kBoolFalseVal) return 0;
  }
  else if (isInt(val))
  {
    return static_cast<int64_t>(std::stol(val));
  }

  throw std::runtime_error("Cannot convert the string to an integer: " + val);
}  // stringToInt

double stringToDouble(const std::string& val)
{
  if (isBool(val) || isInt(val))
  {
    return 1.0 * stringToInt(val);
  }
  else if (isDouble(val))
  {
    return std::stod(val);
  }

  throw std::runtime_error("Cannot convert the string to a double: " + val);
}  // stringToDouble

boost::any stringToAny(const std::string& val)
{
  boost::any anyVal;
  if (val == kBoolTrueVal)
  {
    anyVal = true;
  }
  else if (val == kBoolFalseVal)
  {
    anyVal = false;
  }
  else if (isInt(val))
  {
    anyVal = std::stol(val);
  }
  else if (isDouble(val))
  {
    anyVal = std::stod(val);
  }

  return anyVal;
}  // stringToAny

}  // namespace utilsstrings

}  // namespace optilab
