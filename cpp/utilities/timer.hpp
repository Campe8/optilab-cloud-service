//
// Copyright OptiLab 2019. All rights reserved.
//
// Utilities for timers.
//

#pragma once

#include <chrono>
#include <cstdint>  // for uint64_t
#include <memory>   // for std::shared_ptr

#include "system/system_export_defs.hpp"

// Forward declarations
namespace optilab {

namespace timer {

class SYS_EXPORT_CLASS Timer {
 public:
  using SPtr = std::shared_ptr<Timer>;

 public:
  /// Constructor, if "start" is true (default value), starts the timer
  explicit Timer(bool start = true);

  ~Timer() = default;

  /// Starts the timer
  void start();

  /// Stops the timer
  void stop();

  /// Resets the timer and stops it
  void reset();

  /// Returns true if this timer is running, false otherwise
  inline bool isRunning() const
  {
    return pRunning;
  }

  /// Returns the elapsed time (msec) between the start of the timer and now.
  /// @note this value is the cumulative running time, i.e., it does not count the
  /// time when the timer wasn't running.
  /// @note time is expressed in milliseconds
  uint64_t getWallClockTimeMsec();

  /// Utility method: returns the wallclock time in seconds
  inline uint64_t getWallClockTimeSec()
  {
    return getWallClockTimeMsec() / 1000;
  }

 private:
  using Clock = std::chrono::high_resolution_clock;
  using TimePoint = Clock::time_point;
  using MSec = std::chrono::milliseconds;

 private:
  /// Flag indicating whether the timer is running
  bool pRunning;

  /// Counter of elapsed time between start and update
  MSec pElapsedTime;

  /// Point in time when the timer started
  TimePoint pTimePointStart;

  /// Current point in time
  TimePoint pTimePointCurrent;

  /// Updates the counter for the elapsed time
  void updateTimeCounter();

};

}  // timer

}  // optilab
