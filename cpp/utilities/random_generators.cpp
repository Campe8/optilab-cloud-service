#include "utilities/random_generators.hpp"

#include <chrono>
#include <cstdint>    // for uint64_t
#include <cstdlib>    // for rand
#include <stdexcept>  // for std::runtime_error

namespace optilab {

namespace utilsrandom {

std::string buildRandomAlphaNumString(int len)
{
  std::string randomString;
  static const char alphanum[] = "0123456789"
      "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
      "abcdefghijklmnopqrstuvwxyz";

  for (int idx = 0; idx < len; ++idx)
  {
    randomString += alphanum[rand() % (sizeof(alphanum) - 1)];
  }

  return randomString;
}  // buildRandomAlphaNumString

RandomGenerator::RandomGenerator(int aSeed)
    : pSeed(aSeed),
      pGenerator(nullptr),
      unif01(0.0, 1.0)
{
  // Instantiate random generator
  if (aSeed < 0)
  {
    uint64_t timeSeed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
    std::seed_seq ss { uint32_t(timeSeed & 0xffffffff), uint32_t(timeSeed >> 32) };
    pGenerator.reset(new std::mt19937_64(ss));
  }
  else
  {
    pGenerator.reset(new std::mt19937_64(static_cast<std::uint32_t>(pSeed)));
  }
}

int RandomGenerator::randInt(int lb, int ub) const
{
  // Decrement the upper bound to keep it consistent with the real distribution,
  // i.e., a distribution over [lb, ub)
  ub--;

  // Check boundaries
  if (lb > ub)
  {
    throw std::runtime_error("RandomGenerator - invalid lb/ub: " + std::to_string(lb) + " " +
                             std::to_string(ub));
  }

  // Initialize the distribution
  std::uniform_int_distribution<int> unif(lb, ub);

  // Return a random sample
  return unif(generator());
}  //randInt

double RandomGenerator::randReal(double lb, double ub) const
{
  if (lb > ub)
  {
    throw std::runtime_error("RandomGenerator - invalid lb/ub: " + std::to_string(lb) + " " +
                             std::to_string(ub));
  }

  // Initialize the distribution
  if (lb == 0 && ub == 1.0)
  {
    return unif01(generator());
  }
  else
  {
    std::uniform_real_distribution<double> unif(lb, ub);

    // Return a random sample
    return unif(generator());
  }
}  //randReal

}  // namespace utilsrandom

}  // namespace optilab
