//
// Copyright OptiLab 2019. All rights reserved.
//
// Utilities class for variables.
//

#pragma once

#include <string>
#include <utility>    // for std::pair
#include <vector>

#include "model/model_object_variable.hpp"
#include "solver/cp_variable.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

namespace utilsvariables {

/// Returns the "flattened" index.
/// In general, give Original[HEIGHT, WIDTH, DEPTH] and Flat[HEIGHT * WIDTH * DEPTH],
/// returns the index idx such that:
/// Flat[idx] = Original[x, y, z]
/// where
/// idx = x + WIDTH * (y + DEPTH * z)
SYS_EXPORT_FCN int getFlattenedIndex(const std::vector<int>& dims, const std::vector<int> idxList);

/// Returns CPDomain object representation from the given object variable.
/// @note throws std::invalid_argument if the given pointer is empty
SYS_EXPORT_FCN CPDomain buildCPDomainFromObjectVariable(const ModelObjectVariable::SPtr& var);

/// Given a variable identifier that may be followed by a subscript operator, return the pair
/// <variable_id, list of indices>
SYS_EXPORT_FCN std::pair<std::string, std::vector<int>> extractVariableIdAndIdxFromString(
    const std::string& id);

}  // namespace utilsvariables

}  // namespace optilab
