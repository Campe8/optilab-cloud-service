//
// Copyright OptiLab 2019. All rights reserved.
//
// Utilities class for strings and string manipulation.
//

#pragma once

#include <cstdint>    // for int64_t
#include <string>
#include <vector>

#include <boost/any.hpp>

#include "system/system_export_defs.hpp"

namespace optilab {

namespace utilsstrings {

/// Trims left end of "aString"
SYS_EXPORT_FCN void ltrim(std::string& str);
SYS_EXPORT_FCN std::string ltrimmed(std::string str);

/// Trims right end of "aString"S
SYS_EXPORT_FCN void rtrim(std::string& str);
SYS_EXPORT_FCN std::string rtrimmed(std::string str);

/// Trims "aString" on both ends
SYS_EXPORT_FCN void trim(std::string& aString);
SYS_EXPORT_FCN std::string trimmed(std::string str);

/// Given a string containing the arguments of a fcn call, returns a vector
/// containing each argument separately.
/// For example:
/// "x, t[x[1], m, 2], string_arg"
/// returns the following 3 arguments
/// 1) "x"
/// 2) "t[x[1], m, 2]"
/// 3) "string_arg"
SYS_EXPORT_FCN std::vector<std::string> splitArguments(const std::string& args);

/// Tokenizes "aString" based on delimiter character "aChar".
/// For example, if aString = 1,2,3 and aChar = ',', returns the
/// vector ["1", "2", "3"]
SYS_EXPORT_FCN std::vector<std::string> tokenizeStringOnSymbol(const std::string& str,
                                                               char chr = ' ');

/// Returns true if the given string represents a valid Boolean value, returns false otherwise
SYS_EXPORT_FCN bool isBool(const std::string& val);

/// Returns true if the given string represents a valid integer number, returns false otherwise
SYS_EXPORT_FCN bool isInt(const std::string& val);

/// Returns true if the given string represents a valid double number, returns false otherwise
SYS_EXPORT_FCN bool isDouble(const std::string& val);

/// Convert a string integer (or Boolean) to the corresponding integer value.
/// @note throws std::runtime_error if "isBool(...)" and "isInt(...)" return false
SYS_EXPORT_FCN int64_t stringToInt(const std::string& val);

/// Convert a string representing a Boolean, integer, or double to the corresponding double value
SYS_EXPORT_FCN double stringToDouble(const std::string& val);

/// Convert the value "val" into a boost::any type.
/// The argument van be any of the following types:
/// - Boolean
/// - Integer
/// - Double
SYS_EXPORT_FCN boost::any stringToAny(const std::string& val);

}  // namespace utilsstrings

}  // namespace optilab
