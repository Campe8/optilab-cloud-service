#include "utilities/file_sys.hpp"

#include <array>
#include <cstdio>
#include <iostream>
#include <stdexcept>  // for std::runtime_error
#include <memory>     // for std::unique_ptr
#include <unistd.h>   // for write

#include <boost/dll.hpp>
#include <boost/filesystem.hpp>

namespace {

constexpr int kMaxFileLen = 20;
const char kCppFileExt[] = ".cpp";

}  // namespace

namespace optilab {

namespace utilsfilesys {

std::string getPathToProgramLocation()
{
  return boost::dll::program_location().parent_path().string();
}  // getPathToProgramLocation

std::string getFullPathToLocation(const std::vector<std::string>& pathList)
{
  if (pathList.empty()) return "";

  boost::filesystem::path path(pathList[0]);
  for (int idx = 1; idx < static_cast<int>(pathList.size()); ++idx)
  {
    path = path / pathList[idx];
  }

  return path.string();
}  // getFullPathToLocation

std::string getPathToLocation(const std::vector<std::string>& pathList)
{
  boost::filesystem::path path(getPathToProgramLocation());
  for (const auto& p : pathList) path = path / p;
  return path.string();
}  // getPathToLocation

bool doesFileExists(const std::string& pathToFile)
{
  boost::filesystem::path path(pathToFile);
  return boost::filesystem::exists(path);
}  // doesFileExists

std::pair<int, std::string> createAndOpenTemporaryFile(const std::string& content,
                                                       const std::string& ext)
{
  std::pair<int, std::string> file;

  // Create and open a file with temporary file name
  // "/tmp/fileXXXXXX.cpp"; "fileXXXXXX.cpp"
  static const std::string baseTemplate = "tmpXXXXXX.";
  const std::string fileName = baseTemplate + ext;
  if (static_cast<int>(fileName.size()) >= kMaxFileLen)
  {
    throw std::runtime_error("createAndOpenTemporaryFile - invalid file name: " + fileName);
  }
  char templateFile[kMaxFileLen] =  {'\0'};

  int idx = 0;
  for (auto c : fileName) templateFile[idx++] = c;
  int fd = mkstemps(templateFile, 1 + static_cast<int>(ext.size()));
  if (fd == -1)
  {
    const std::string errMsg = "createAndOpenHPPFile - cannot create temporary hpp file";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  // Write into the file
  std::string tempFileName(templateFile);
  auto res = write(fd, content.c_str(), content.size());
  if (res == -1)
  {
    throw std::runtime_error("createAndOpenHPPFile - error in writing the temporary hpp file");
  }

  file.first = fd;
  file.second = tempFileName;

  return file;
}  // createAndOpenTemporaryFile

void closeAndRemoveFile(const std::pair<int, std::string>& file)
{
  // Close and delete the file asap
  const int err = unlink(file.second.c_str());
  if (err == -1)
  {
    spdlog::warn("removeAndCloseHPPFile: cannot unlink temporary hpp file: " + file.second);
  }
  close(file.first);
}  // closeAndRemoveFile

std::string execCmd(const char* cmd)
{
  std::array<char, 128> buffer;
  std::string result;
  std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd, "r"), pclose);
  if (!pipe)
  {
    throw std::runtime_error("popen() failed!");
  }
   while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr)
   {
       result += buffer.data();
   }
   return result;
}  // execCmd

std::string compileAndCreateDynamicLib(const std::string& fileName,
                                       const std::string& dynLibName,
                                       const std::string& compilerName,
                                       const std::string& includePath)
{
  std::vector<std::string> fileList = { fileName };
  return compileAndCreateDynamicLib(fileList, dynLibName, compilerName, includePath);
}  // compileAndCreateDynamicLib

std::string compileAndCreateDynamicLib(const std::vector<std::string>& fileList,
                                       const std::string& dynLibName,
                                       const std::string& compilerName,
                                       const std::string& includePath)
{
  static const std::string compilerCmd = compilerName +
      " -std=c++11 -shared -fPIC -Wfatal-errors " + includePath + " -O3 ";

  if (fileList.empty())
  {
    throw std::runtime_error("compileAndCreateDynamicLib - empty file list");
  }

  bool emptyFileList = true;
  std::string exeCommand = compilerCmd;
  for (const auto& file : fileList)
  {
    // Skip hpp files
    if (file.size() > 4 && (file.substr(file.size() - 4) != std::string(kCppFileExt)))
    {
      continue;
    }
    emptyFileList = false;
    exeCommand += file + " ";
  }

  if (emptyFileList)
  {
    throw std::runtime_error("compileAndCreateDynamicLib - no files to compile");
  }

  exeCommand += "-o " + dynLibName;
  return execCmd(exeCommand.c_str());
}  // compileAndCreateDynamicLib

}  // namespace utilsfilesys

}  // namespace optilab
