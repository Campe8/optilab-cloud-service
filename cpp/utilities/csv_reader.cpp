#include "utilities/csv_reader.hpp"

#include <fstream>  // for std::ifstream

#include <boost/algorithm/string.hpp>

namespace optilab {
namespace utilsreader {

CSVReader::CSVReader(const std::string& filename, std::string delim)
: pFileName(filename)
, pDelimiter(delim)
{
}

CSVReader::CSVData CSVReader::getData() {

  std::ifstream file(pFileName);

  CSVReader::CSVData dataList;

  std::string line = "";

  // Iterate through each line and split the content using delimeter
  while (getline(file, line))
  {
    std::vector<std::string> vec;
    boost::algorithm::split(vec, line, boost::is_any_of(pDelimiter));
    dataList.push_back(vec);
  }

  // Close the File
  file.close();

  // Returns the list of data
  return dataList;
}

}  // namespace utilsreader
}  // optilab
