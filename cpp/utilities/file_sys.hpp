//
// Copyright OptiLab 2019. All rights reserved.
//
// Utilities for file system manipulation.
//

#pragma once

#include <dlfcn.h>
#include <iostream>
#include <memory>  // for std::shared_ptr
#include <string>
#include <utility> // for std::pair
#include <vector>

#include <spdlog/spdlog.h>

#include "system/system_export_defs.hpp"

namespace optilab {

namespace utilsfilesys {

template<class T>
class DLClass
{

 public:
  DLClass(std::string moduleName)
 : pModule(moduleName),
   pShared(std::make_shared<shared_obj>())
 {
 }
  ~DLClass()
  {
    //close_module();
  }

  template<typename ... Args>
  std::shared_ptr<T> make_obj(Args ... args)
  {
    if(!pShared->create || !pShared->destroy)
    {
        if(!pShared->open_module(pModule))
        {
          return std::shared_ptr<T>(NULL);
        }
    }

    //  auto create_args = ((T* (*)(Args...))create);
    std::shared_ptr<shared_obj> my_shared = pShared;
    return std::shared_ptr<T>(pShared->create(args...),
                              [my_shared](T* p){ my_shared->destroy(p); });
  }

 private:
  struct shared_obj
  {
    typename T::create_t* create = nullptr;
    typename T::destroy_t* destroy = nullptr;
    void * dll_handle = nullptr;

    ~shared_obj()
    {
      close_module();
    }

    bool open_module(std::string module)
    {
      dll_handle = dlopen(module.c_str(), RTLD_LAZY);

      if (!dll_handle)
      {
        const std::string errMsg = "Failed to open library: " + module + " due to " +
            std::string(dlerror());
        spdlog::error(errMsg);
        return false;
      }

      // Reset errors
      dlerror();

      create = (typename T::create_t*) dlsym(dll_handle, "create");
      const char * err = dlerror();
      if(err)
      {
        const std::string errMsg = "Failed to create symbol from library: " + module +
            " on symbol \"create\" due to " + std::string(err);
        spdlog::error(errMsg);
        close_module();
        return false;
      }

      destroy = (typename T::destroy_t*) dlsym(dll_handle, "destroy");
      err = dlerror();
      if(err)
      {
        const std::string errMsg = "Failed to load destroy symbol rom library: " + module +
            " on symbol \"destroy\" due to " + std::string(err);
        spdlog::error(errMsg);
        close_module();
        return false;
      }

      return true;
    }

    void close_module()
    {
      if (dll_handle)
      {
        dlclose(dll_handle);
        dll_handle = nullptr;
      }

      if (create) create = nullptr;
      if (destroy) destroy = nullptr;
    }
  };

  std::string pModule;
  std::shared_ptr<shared_obj> pShared;
};

/// Returns the full path to the location where the executable (the one containing the main)
/// is currently located
SYS_EXPORT_FCN std::string getPathToProgramLocation();

/// Returns the full path as follows:
/// pathList[0] / pathList[1] / ...
SYS_EXPORT_FCN std::string getFullPathToLocation(const std::vector<std::string>& pathList);

/// Returns the full path as follows:
/// full path to executable + specified path
SYS_EXPORT_FCN std::string getPathToLocation(const std::vector<std::string>& pathList);

/// Checks and returns whether the specified (path to) file exists or not
SYS_EXPORT_FCN bool doesFileExists(const std::string& pathToFile);

/// Creates a (unique) temporary file with given extension storing in it
/// the data given as input argument "content".
/// Returns the file descriptor to the open/created file and the name of the file.
/// @note this function is supposed to go in tandem with "removeAndCloseHPPFile(...)"
SYS_EXPORT_FCN std::pair<int, std::string> createAndOpenTemporaryFile(const std::string& content,
                                                                      const std::string& ext);

/// Unlinks the open file with given name and closes the correspondent file descritor.
/// @note this function is supposed to go in tandem with "createAndOpenTemporaryFile(...)"
SYS_EXPORT_FCN void closeAndRemoveFile(const std::pair<int, std::string>& file);

/// Executes the given Linux command (e.g., "ls") and returns the output (as output stream).
/// @note this function uses "popen(...)" behind the scenes
SYS_EXPORT_FCN std::string execCmd(const char* cmd);

/// Compiles and creates a dynamic library given the path to the file to compile and the name
/// of the dynamic library (.so) to build.
/// Returns the output of the compiler.
/// @note the library name MUST have the ".so" extension.
/// @note it is possible to specify the compiler to use
/// For example:
/// g++ -shared -fPIC <filename.hpp> -o <libname.so>
SYS_EXPORT_FCN std::string compileAndCreateDynamicLib(const std::string& fileName,
                                                      const std::string& dynLibName,
                                                      const std::string& compilerName = "gcc",
                                                      const std::string& includePath = "-I.");

/// Compiles and creates a dynamic library given the list of paths to the files to
/// compile include in the dynamic library (.so) to build.
/// The library will have the name provided as "dynLibName".
/// Returns the output of the compiler.
/// @note the library name MUST have the ".so" extension.
/// @note it is possible to specify the compiler to use
/// For example:
/// g++ -shared -fPIC <filename.hpp> -o <libname.so>
SYS_EXPORT_FCN std::string compileAndCreateDynamicLib(const std::vector<std::string>& fileList,
                                                      const std::string& dynLibName,
                                                      const std::string& compilerName = "gcc",
                                                      const std::string& includePath = "-I.");
}  // namespace utilsfilesys

}  // namespace optilab
