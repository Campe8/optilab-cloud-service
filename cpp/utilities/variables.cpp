#include "utilities/variables.hpp"

#include <algorithm>  // for std::max
#include <cstdint>    // for int64_t
#include <limits>     // for std::numeric_limits
#include <stdexcept>  // for std::invalid_argument
#include <utility>    // for std::pair

#include "utilities/strings.hpp"

namespace {

const char kOpenSquareBracket[] = "[";
const char kCloseSquareBracket[] = "]";

template<typename T>
T castToType(double val)
{
  double castVal = std::max<double>(val, static_cast<double>(std::numeric_limits<T>::min()));
  castVal = std::min<double>(castVal, static_cast<double>(std::numeric_limits<T>::max()));
  return static_cast<T>(castVal);
}  // castToType

}  // namespace

namespace optilab {

namespace utilsvariables {

int getFlattenedIndex(const std::vector<int>& dims, const std::vector<int> idxList)
{
  if (idxList.empty())
  {
    throw std::runtime_error("Empty index list");
  }

  if (dims.empty())
  {
    throw std::runtime_error("Empty dimension list");
  }

  if (dims.size() != idxList.size())
  {
    throw std::runtime_error("Dimension list and index list must agree on size");
  }

  int dim = 0;
  int pos = 1;
  int idx = idxList.back();
  int flatIdx = dim + pos * idx;
  for (int ctr = dims.size() - 1; ctr > 0; --ctr)
  {
    flatIdx = dim + pos * idx;

    idx = flatIdx;
    pos = dims[ctr];
    dim = idxList[ctr - 1];
  }

  return flatIdx;
}  // getFlattenedIndex

CPDomain buildCPDomainFromObjectVariable(const ModelObjectVariable::SPtr& var)
{
  if (!var)
  {
    throw std::invalid_argument("Empty pointer to the model object variable");
  }

  const auto type = var->getDomainType();
  if (type == ModelObjectVariable::VarType::INT)
  {
    int64_t lb, ub;
    lb = static_cast<int64_t>(
        std::max<double>(var->getLowerBound(),
                         static_cast<double>(std::numeric_limits<int64_t>::min())));

    ub = static_cast<int64_t>(
        std::min<double>(var->getUpperBound(),
                         static_cast<double>(std::numeric_limits<int64_t>::max())));
    return CPDomain::buildIntDomain(lb, ub);
  }
  else if (type == ModelObjectVariable::VarType::BOOL)
  {
    if (var->getLowerBound() == var->getUpperBound())
    {
      return CPDomain::buildBooleanDomain(var->getLowerBound() != 0);
    }
    else
    {
      return CPDomain::buildBooleanDomain();
    }
  }
  else if (type == ModelObjectVariable::VarType::FLOAT ||
      type == ModelObjectVariable::VarType::DOUBLE)
  {
    return CPDomain::buildContinuousDomain(var->getLowerBound(), var->getUpperBound());
  }
  else if (type == ModelObjectVariable::VarType::SET)
  {
    std::vector<int64_t> setVals;
    for (auto v : var->getListOfDomainValues())
    {
      setVals.push_back(castToType<int64_t>(v));
    }
    return CPDomain::buildSetDomain(setVals);
  }
  else if (type == ModelObjectVariable::VarType::LIST)
  {
    std::vector<int64_t> listVals;
    for (auto v : var->getListOfDomainValues())
    {
      listVals.push_back(castToType<int64_t>(v));
    }
    return CPDomain::buildListOfValuesDomain(listVals);
  }
  else if (type == ModelObjectVariable::VarType::RANGE_LIST)
  {
    std::vector<std::pair<int64_t, int64_t>> listPairs;
    for (int idx = 1; idx < static_cast<int>((var->getListOfDomainValues()).size()) - 1;
        idx = idx + 2)
    {
      std::pair<int64_t, int64_t> pair;
      pair.first = castToType<int64_t>((var->getListOfDomainValues()).at(idx));
      pair.second = castToType<int64_t>((var->getListOfDomainValues()).at(idx + 1));
      listPairs.push_back(pair);
    }
    return CPDomain::buildListOfRangesDomain(listPairs);
  }

  throw std::runtime_error("Undefined model object variable type");
}  // buildCPDomainFromObjectVariable

std::pair<std::string, std::vector<int>> extractVariableIdAndIdxFromString(const std::string& id)
{
  std::pair<std::string, std::vector<int>> idPair;
  auto itOpen = id.find_first_of(kOpenSquareBracket);
  auto itClose = id.find_last_of(kCloseSquareBracket);

  if (itOpen == std::string::npos || itClose == std::string::npos)
  {
    // Only the id
    idPair.first = id;
  }
  else
  {
    // The id is until the first open bracket
    idPair.first = id.substr(0, itOpen);
    auto indices = id.substr(itOpen + 1, itClose - itOpen - 1);
    auto indicesTokens = utilsstrings::splitArguments(indices);
    for (const auto& token : indicesTokens)
    {
      if (utilsstrings::isInt(token))
      {
        throw std::runtime_error("Invalid conversion from string to int: " + token);
      }
      idPair.second.push_back(std::stoi(token));
    }
  }

  return idPair;
}  // extractVariableIdAndIdxFromString

}  // namespace utilsvariables

}  // namespace optilab
