//
// Copyright OptiLab 2019. All rights reserved.
//
// Utilities class for random generators.
//

#pragma once

#include <memory>  // for std::unique_ptr
#include <random>
#include <string>

#include "system/system_export_defs.hpp"

namespace optilab {

namespace utilsrandom {

/// Returns a random alphanumeric string of given length
SYS_EXPORT_FCN std::string buildRandomAlphaNumString(int len);

class SYS_EXPORT_CLASS RandomGenerator {
public:
  /// Constructor for random number generator.
  /// If "aSeed" is less than 0, it uses the current time as random seed.
  /// @note default value for the seed is "-1", i.e., the current time
  /// is used as random seed
  RandomGenerator(int aSeed=-1);

  ~RandomGenerator() = default;

  inline int getSeed() const { return pSeed; }

  /// Generate a random integer number in [aLB, aUB) with uniform distribution
  int randInt(int aLB, int aUB) const;

  /// Generate a random double number in [aLB, aUB) with uniform distribution.
  /// @note default interval is [0.0, 1.0)
  double randReal(double aLB = 0.0, double aUB = 1.0) const;

  /// Utility function: returns the reference to the generator
  inline std::mt19937_64& generator() const { return *pGenerator; }

private:
  /// Random seed used to create the generator
  int pSeed;

  /// Random generator
  mutable std::unique_ptr<std::mt19937_64> pGenerator;

  /// Uniform in [0, 1)
  mutable std::uniform_real_distribution<double> unif01;
};

}  // namespace utilsrandom

}  // namespace optilab
