//
// Copyright OptiLab 2019. All rights reserved.
//
// Entry point for the OptiLab client.
//

#include <getopt.h>

#include <exception>
#include <iostream>
#include <string>
#include <vector>

#include "config/config_constants.hpp"
#include "config/system_network_config.hpp"
#include "optimizer_client/optimizer_client.hpp"

extern int optind;

namespace {

void printHelp(const std::string& programName) {
  std::cerr << "Usage: " << programName << " [options]"
      << std::endl
      << "options:" << std::endl
      << "  --framework|-f     Specifies the framework to be used: "
          "CP, MIP, COMB, MPS (default CP).\n"
      << "  --help|-h          Print this help message."
      << std::endl;
}  // printHelp

}  // namespace

int main(int argc, char* argv[]) {

  char optString[] = "hf:";
  struct option longOptions[] =
  {
      { "framework", required_argument, NULL, 'f' },
      { "help", no_argument, NULL, 'h' },
      { 0, 0, 0, 0 }
  };

  // Parse options
  int opt;
  std::string framework = "CP";
  while (-1 != (opt = getopt_long(argc, argv, optString, longOptions, NULL)))
  {
    switch (opt)
    {
      case 'f':
      {
        framework = std::string(optarg);
        break;
      }
      case 'h':
      default:
        printHelp(argv[0]);
        return 0;
    }
  }  // while

  // Get the port number to run the service on
  auto& config = optilab::SystemNetworkConfig::getInstance();
  auto& portList = config.getPortList(optilab::configconsts::FRONT_END_CLIENT);
  if (portList.empty())
  {
    std::cerr << "Empty front-end client port list in configuration file\n";
    return 1;
  }

  int optilabFrontEndPort = portList[0];
  try
  {
    // Create a new optimizer broker and run it
    optilab::OptimizerClient optClient(optilabFrontEndPort, framework);
    return optClient.run();
  }
  catch (const std::exception& e)
  {
    std::cerr << e.what() << std::endl;
    return 1;
  }
  catch (...)
  {
    std::cerr << "Undefined error" << std::endl;
    return 2;
  }

  return 0;
}
