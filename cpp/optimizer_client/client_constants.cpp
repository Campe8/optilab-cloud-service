#include "optimizer_client/client_constants.hpp"

namespace optilab {

namespace optimizerclient {

const char CLIENT_LOGIN_MESSAGE[] = "login";
const char CLIENT_LOGOFF_MESSAGE[] = "logoff";
const char CLIENT_LOGOFF_SUCCESS_MESSAGE[] = "logoff_success";
const char CLIENT_PREFIX_TCP_NETWORK_ADDR[] = "tcp://";
const int DEFAULT_CLIENT_CONTEXT_NUM_THREADS = 1;
const char LOCAL_HOST_NAME[] = "localhost";
const int MAX_SERVICE_PORT_NUMBER = 65535;

}  // optimizerclient

namespace optimizerclienttest {

const char kDummyMPSORToolsModel[] =
    R"(
{
  "framework": "OR",
  "classType": "LP",
  "package": "OR-TOOLS",
  "model": "test_lp_mps",
  "model_format": "MPS",
  "model_data": "",
  "model_descriptor":
  "NAME          TESTPROB\r\nROWS\r\n N  COST\r\n L  LIM1\r\n G  LIM2\r\n E  MYEQN\r\nCOLUMNS\r\n    XONE      COST                 1   LIM1                 1\r\n    XONE      LIM2                 1\r\n    YTWO      COST                 4   LIM1                 1\r\n    YTWO      MYEQN               -1\r\n    ZTHREE    COST                 9   LIM2                 1\r\n    ZTHREE    MYEQN                1\r\nRHS\r\n    RHS1      LIM1                 5   LIM2                10\r\n    RHS1      MYEQN                7\r\nBOUNDS\r\n UP BND1      XONE                 4\r\n LO BND1      YTWO                -1\r\n UP BND1      YTWO                 1\r\nENDATA\r\n"
}
)";

const char kDummyFlatZincGecodeModel[] =
    R"(
{
  "framework": "OR",
  "classType": "CP",
  "package": "GECODE",
  "model": "queens",
  "model_format": "FLATZINC",
  "model_data": "",
  "model_descriptor":
  "array [1..4] of var 1..4: q :: output_array([1..4]);\r\nvar int: dq01 :: output_var;\r\nvar int: dq02 :: output_var;\r\nvar int: dq03 :: output_var;\r\nvar int: dq12 :: output_var;\r\nvar int: dq13 :: output_var;\r\nvar int: dq23 :: output_var;\r\nconstraint int_plus(dq01, q[2], q[1]);\r\nconstraint int_plus(dq02, q[3], q[1]);\r\nconstraint int_plus(dq03, q[4], q[1]);\r\nconstraint int_plus(dq12, q[3], q[2]);\r\nconstraint int_plus(dq13, q[4], q[2]);\r\nconstraint int_plus(dq23, q[4], q[3]);\r\nconstraint int_ne(q[1], q[2]);\r\nconstraint int_ne(q[1], q[3]);\r\nconstraint int_ne(q[1], q[4]);\r\nconstraint int_ne(q[2], q[3]);\r\nconstraint int_ne(q[2], q[4]);\r\nconstraint int_ne(q[3], q[4]);\r\nconstraint int_ne(dq01, -1);\r\nconstraint int_ne(dq01,  1);\r\nconstraint int_ne(dq02, -2);\r\nconstraint int_ne(dq02,  2);\r\nconstraint int_ne(dq03, -3);\r\nconstraint int_ne(dq03,  3);\r\nconstraint int_ne(dq12, -1);\r\nconstraint int_ne(dq12,  1);\r\nconstraint int_ne(dq13, -2);\r\nconstraint int_ne(dq13,  2);\r\nconstraint int_ne(dq23, -1);\r\nconstraint int_ne(dq23,  1);\r\nsolve satisfy;\r\n"
}
)";

const char kDummyCPGecodeModel[] =
    R"(
{
  "framework":"OR",
  "classType": "CP",
  "package":"GECODE",
  "model":"simple_model",
  "variablesList":[{
    "id":"x",
    "dims":[],
    "lb":0,
    "ub":2,
    "type":"int"
  },{
    "id":"y",
    "dims":[],
    "lb":0,
    "ub":2,
    "type":"int"
  }],
  "searchList":[{
    "searchId": "s1",
    "searchType": "global",
    "numSolutions": 1
  }],
  "searchObjects":[{
    "id": "s1",
    "combinatorType":"baseSearch",
    "varSelection": "int_var_size_min",
    "valSelection": "int_val_min",
    "branchingList": [{
      "id": "x",
      "idx": []
    },{
      "id": "y",
      "idx": []
    }]
  }]
})";

const char kDummyCPORToolsModel[] =
    R"(
{
  "framework":"OR",
  "classType": "CP",
  "package":"OR-TOOLS",
  "model":"simple_model",
  "variablesList":[{
    "id":"x",
    "dims":[],
    "lb":0,
    "ub":2,
    "type":"int"
  },{
    "id":"y",
    "dims":[],
    "lb":0,
    "ub":2,
    "type":"int"
  }],
  "searchList":[{
    "searchId": "s1",
    "searchType": "global",
    "numSolutions": 1
  }],
  "searchObjects":[{
    "id": "s1",
    "combinatorType":"baseSearch",
    "varSelection": "first_unbound",
    "valSelection": "min_value",
    "branchingList": [{
      "id": "x",
      "idx": []
    },{
      "id": "y",
      "idx": []
    }]
  }]
})";

const char kDummyMIPORToolsModel[] =
    R"(
{
  "framework": "OR",
  "classType": "MIP",
  "package": "OR-TOOLS",
  "model": "simple_model",
  "variablesList": [{
    "id": "x",
    "dims": [],
    "lb": 0.0,
    "ub": 1.79769e+308,
    "type": "int"
  }, {
    "id": "y",
    "dims": [],
    "lb": 0.0,
    "ub": 1.79769e+308,
    "type": "int"
  }],
  "constraintsList":[
    {
      "id": "c0",
      "lb": -1.79769e+308,
      "ub": 17.5,
      "coefficientsList": [
        {
          "id": "x",
          "coeff": 1
        },
        {
          "id": "y",
          "coeff": 7
        }
      ]
    },
    {
      "id": "c1",
      "lb": -1.79769e+308,
      "ub": 3.5,
      "coefficientsList": [
        {
          "id": "x",
          "coeff": 1
        },
        {
          "id": "y",
          "coeff": 0
        }
      ]
    }
  ],
  "objectivesList":[
    {
      "optimizationDirection": "maximize",
      "coefficientsList": [
        {
          "id": "x",
          "coeff": 1
        },
        {
          "id": "y",
          "coeff": 10
        }
      ]
    }
  ]
}
)";

const char kDummyCombinatorModel[] =
    R"(
{
  "framework": "Connectors",
  "classType": "Combinator",
  "id": "combinator",
  "combinatorObjects":["simple_model_cp", "simple_model_mip"],
  "objectsList": [
    {
    "framework": "OR",
    "classType": "MIP",
    "id": "simple_model_mip",
    "package": "OR-TOOLS",
    "model": "simple_model_mip",
    "variablesList": [{
      "id": "x",
      "dims": [],
      "lb": 0.0,
      "ub": 1.79769e+308,
      "type": "double"
    }, {
      "id": "y",
      "dims": [],
      "lb": 0.0,
      "ub": 1.79769e+308,
      "type": "double"
    }],
    "constraintsList":[
      {
        "id": "c0",
        "lb": -1.79769e+308,
        "ub": 17.5,
        "coefficientsList": [
          {
            "id": "x",
            "coeff": 1
          },
          {
            "id": "y",
            "coeff": 7
          }
        ]
      },
      {
        "id": "c1",
        "lb": -1.79769e+308,
        "ub": 3.5,
        "coefficientsList": [
          {
            "id": "x",
            "coeff": 1
          },
          {
            "id": "y",
            "coeff": 0
          }
        ]
      }
    ],
    "objectivesList":[
      {
        "optimizationDirection": "maximize",
        "coefficientsList": [
          {
            "id": "x",
            "coeff": 1
          },
          {
            "id": "y",
            "coeff": 10
          }
        ]
      }
    ]
  },
  {
    "framework": "OR",
    "classType": "CP",
    "id": "simple_model_cp",
    "package": "OR-TOOLS",
    "model": "simple_model",
    "variablesList": [{
      "id": "x",
      "dims": [],
      "lb": 0,
      "ub": 2,
      "type": "int"
    }, {
      "id": "y",
      "dims": [],
      "lb": 0,
      "ub": 2,
      "type": "int"
    }],
    "searchList":[
      {
        "searchId": "s1",
        "searchType": "global",
        "numSolutions": 1
      }
    ],
    "searchObjects":[
      {
        "id": "s1",
        "combinatorType":"baseSearch",
        "varSelection": "first_unbound",
        "valSelection": "min_value",
        "branchingList": [{
          "id": "x",
          "idx": []
        }, {
          "id": "y",
          "idx": []
        }]
      }
    ]
  }
 ]
}
)";

}  // optimizerclienttest

}  // namespace optilab
