//
// Copyright OptiLab 2019. All rights reserved.
//
// Utilities class for clients.
//

#pragma once

#include <string>

#include "optilab_protobuf/optilab.pb.h"
#include "system/system_export_defs.hpp"

namespace optilab {

namespace clientutils {

/// Builds and returns a create a CP optimizer request message using OR-Tools package
SYS_EXPORT_FCN OptilabRequestMessage buildCreateCPOptimizerMessage(const std::string& engineId,
                                                                   optilab::PkgType pkgType);

/// Builds and returns a create a MIP optimizer request message
SYS_EXPORT_FCN OptilabRequestMessage buildCreateMIPOptimizerMessage(const std::string& engineId);

/// Builds and returns a create a connector request message
SYS_EXPORT_FCN OptilabRequestMessage buildCreateConnectorMessage(const std::string& engineId);

/// Builds and returns a delete optimizer request message
SYS_EXPORT_FCN OptilabRequestMessage buildDeleteOptimizerMessage(const std::string& engineId,
                                                                 optilab::PkgType pkgType);

/// Builds and returns a delete connector request message
SYS_EXPORT_FCN OptilabRequestMessage buildDeleteConnectorMessage(const std::string& engineId);

/// Builds and returns a load model request message
SYS_EXPORT_FCN OptilabRequestMessage buildLoadModelMessage(const std::string& engineId,
                                                           const std::string& jsonModel,
                                                           optilab::PkgType pkgType);

/// Builds and returns a load connections request message
SYS_EXPORT_FCN OptilabRequestMessage buildLoadConnectionsMessage(const std::string& engineId,
                                                                 const std::string& jsonModel);

/// Builds and returns a run model request message
SYS_EXPORT_FCN OptilabRequestMessage buildRunModelMessage(const std::string& engineId,
                                                          optilab::PkgType pkgType);

/// Builds and returns a run connector request message
SYS_EXPORT_FCN OptilabRequestMessage buildRunConnectorMessage(const std::string& engineId);

/// Builds and returns a model solutions request message
SYS_EXPORT_FCN OptilabRequestMessage buildModelSolutionsMessage(const std::string& engineId,
                                                                int numSol,
                                                                optilab::PkgType pkgType);

/// Builds and returns an optimizer wait request message
SYS_EXPORT_FCN OptilabRequestMessage buildOptimizerWaitMessage(const std::string& engineId,
                                                               optilab::PkgType pkgType,
                                                               int waitSec = -1);

/// Builds and returns a connector wait request message
SYS_EXPORT_FCN OptilabRequestMessage buildConnectorWaitMessage(const std::string& engineId,
                                                               int waitSec = -1);

}  // namespace clientutils

}  // namespace optilab
