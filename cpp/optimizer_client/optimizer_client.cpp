#include "optimizer_client/optimizer_client.hpp"

#include <cassert>
#include <functional>  // for std::ref
#include <chrono>
#include <stdexcept>   // for std::invalid_argument

#include <spdlog/spdlog.h>

#include "optimizer_client/client_constants.hpp"
#include "optimizer_client/client_utilities.hpp"
#include "optilab_protobuf/optilab.pb.h"
#include "optimizer_service/service_constants.hpp"

namespace {

constexpr int kLogoffSleepSimulationTimeSec = 2;
constexpr int kCleanupWaitTimeSec = 2;
constexpr int kMessageId = 1000;
const char FRAMEWORK_CP[] = "CP";
const char FRAMEWORK_CP_GECODE[] = "CPGC";
const char FRAMEWORK_FZ[] = "FZ";
const char FRAMEWORK_MIP[] = "MIP";
const char FRAMEWORK_COMB[] = "COMB";
const char FRAMEWORK_MPS[] = "MPS";

std::string getSocketAddr(const int socketPort, const std::string& socketAddr)
{
  std::string addr = optilab::optimizerclient::CLIENT_PREFIX_TCP_NETWORK_ADDR;
  if (socketAddr.empty())
  {
    throw std::invalid_argument("Empty TCP address");

  }
  addr += socketAddr + ":";
  addr += std::to_string(socketPort);

  return addr;
}  // getSocketAddr

bool checkOKReply(const std::unordered_set<int>& idSet, int id)
{
  return idSet.find(id) != idSet.end();
}  // checkOKReply

}  // namespace

namespace optilab {

OptimizerClient::OptimizerClient(const int frontendPort, const std::string& framework,
                                 const std::string& frontendAddr)
: pFrontendPort(frontendPort),
  pFrameworkType(framework),
  pFrontendAddr(frontendAddr.empty() ? optimizerclient::LOCAL_HOST_NAME : frontendAddr)
{
  if (pFrontendPort < 1 || pFrontendPort > optimizerclient::MAX_SERVICE_PORT_NUMBER)
  {
    throw std::invalid_argument("OptimizerClient - invalid front-end port number");
  }
}

int OptimizerClient::run()
{
  // Initialize the context for the client
  std::string clientConnectId;
  std::shared_ptr<zmq::context_t> context;
  std::shared_ptr<zmq::socket_t> socket;
  try
  {
    context = std::make_shared<zmq::context_t>(optimizerclient::DEFAULT_CLIENT_CONTEXT_NUM_THREADS);
    socket = std::make_shared<zmq::socket_t>(*context, ZMQ_DEALER);

    // Set client ID and connect
    clientConnectId = s_set_id(*socket);
    spdlog::info("OptimizerClient - create new client connection id " + clientConnectId);

    // Get the front-end/back-end addresses to bind to
    const std::string frontendAddr = getSocketAddr(pFrontendPort, pFrontendAddr);
    spdlog::info("OptimizerClient - client " + clientConnectId +
                 " connecting to address " + frontendAddr);
    socket->connect(frontendAddr.c_str());

    // Login into the broker
    spdlog::info("OptimizerClient - client " + clientConnectId +
                 " send login message " + std::string(optimizerclient::CLIENT_LOGIN_MESSAGE));
    s_send(*socket, optimizerclient::CLIENT_LOGIN_MESSAGE);
  }
  catch (...)
  {
    throw;
  }

  // Perform startup
  startupClient(socket);

  // Send requests to service
  const int errorStatus = sendRequestToService(socket);

  // Simulate log-off/cleanup with a wait
  spdlog::info("OptimizerClient - simulating log off with " +
               std::to_string(kLogoffSleepSimulationTimeSec) + " sec. sleep...");
  std::this_thread::sleep_for(std::chrono::seconds(kLogoffSleepSimulationTimeSec));
  spdlog::info("OptimizerClient - ...done");

  // Perform cleanup
  cleanupClient(socket);

  // Cleanup sockets and context
  socket.reset();
  context.reset();

  return errorStatus;
}  // run

void OptimizerClient::startupClient(const SocketSPtr& socket)
{
  spdlog::info("OptimizerClient - start message reader");
  pMessageReader = std::make_shared<std::thread>(
      &OptimizerClient::handleIncomingMessages, this, socket);
}  // startupClient

void OptimizerClient::cleanupClient(const SocketSPtr& socket)
{
  // Logoff from the broker
  spdlog::info("OptimizerClient - sending log off message");
  s_send(*socket, optimizerclient::CLIENT_LOGOFF_MESSAGE);

  // Wait some time to receive the log-off success message.
  // If not received, force log-off
  spdlog::info("OptimizerClient - waiting cleanup time...");
  std::this_thread::sleep_for(std::chrono::seconds(kCleanupWaitTimeSec));
  spdlog::info("OptimizerClient - ...done");

  if (!pLogoffSuccess)
  {
    // Force shutdown of the reader.
    // @note this detaches the thread which will throw since the (socket) context is destroyed.
    // The try-catch will terminate the thread
    spdlog::warn("Detach reader thread");
    pMessageReader->detach();
  }
  else
  {
    // Join the receiver thread
    pMessageReader->join();
  }
}  // cleanupClient

int OptimizerClient::sendRequestToService(const SocketSPtr& socket)
{
  spdlog::info("OptimizerClient - start communication on socket");

  // Error status, zero means no error
  int errorStatus = 0;
  if (!socket)
  {
    const std::string errMsg = "OptimizerClient - "
        "empty socket while sending a request to the service";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  // Ask the service to solve a simple OR-Tools model.
  // The request is made as follows:
  // 1 - create an OR-Tools engine;
  // 2 - load the model;
  // 3 - run model;
  // 4 - collect solutions;
  // 5 - delete engine;
  // Between each request, send an engine wait request;
  std::string engineId = "engine_id";
  std::string modelRequest;
  if (pFrameworkType == std::string(FRAMEWORK_CP))
  {
    engineId += "_cp";
    modelRequest = std::string(optimizerclienttest::kDummyCPORToolsModel);
    return sendORRequestToService(engineId, modelRequest, socket);
  }
  else if (pFrameworkType == std::string(FRAMEWORK_MIP))
  {
    engineId += "_mip";
    modelRequest = std::string(optimizerclienttest::kDummyMIPORToolsModel);
    return sendORRequestToService(engineId, modelRequest, socket);
  }
  else if (pFrameworkType == std::string(FRAMEWORK_FZ))
  {
    engineId += "_fz";
    modelRequest = std::string(optimizerclienttest::kDummyFlatZincGecodeModel);
    return sendORRequestToService(engineId, modelRequest, socket);
  }
  else if (pFrameworkType == std::string(FRAMEWORK_CP_GECODE))
  {
    engineId += "_cp_gecode";
    modelRequest = std::string(optimizerclienttest::kDummyCPGecodeModel);
    return sendORRequestToService(engineId, modelRequest, socket);
  }
  else if (pFrameworkType == std::string(FRAMEWORK_MPS))
  {
    engineId += "_mps";
    modelRequest = std::string(optimizerclienttest::kDummyMPSORToolsModel);
    return sendORRequestToService(engineId, modelRequest, socket);
  }
  else if (pFrameworkType == std::string(FRAMEWORK_COMB))
  {
    engineId += "_comb";
    modelRequest = std::string(optimizerclienttest::kDummyCombinatorModel);
    return sendConnectorRequestToService(engineId, modelRequest, socket);
  }
  else
  {
    const std::string errMsg = "OptimizerClient - "
        "unrecognized framework: " + pFrameworkType;
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }
}  // sendRequestToService

int OptimizerClient::sendConnectorRequestToService(const std::string& engineId,
                                                   const std::string& modelRequest,
                                                   const SocketSPtr& socket)
{
  // Error status, zero means no error
  int errorStatus = 0;

  spdlog::info("OptimizerClient - client send connector creation request");
  errorStatus = sendCreateConnectorRequest(engineId, socket);
  if (errorStatus != 0)
  {
    spdlog::error("Engine creation request failed");
    return errorStatus;
  }

  spdlog::info("OptimizerClient - client send engine wait request");
  errorStatus = sendConnectorWaitRequst(engineId, socket);
  if (errorStatus != 0)
  {
    spdlog::error("Engine wait request failed");
    return errorStatus;
  }

  spdlog::info("OptimizerClient - client send load connections request");
  errorStatus = sendLoadConnectionsRequest(engineId, modelRequest, socket);
  if (errorStatus != 0)
  {
    spdlog::error("Engine load model request failed");
    return errorStatus;
  }

  spdlog::info("OptimizerClient - client send engine wait request");
  errorStatus = sendConnectorWaitRequst(engineId, socket);
  if (errorStatus != 0)
  {
    spdlog::error("Engine wait request failed");
    return errorStatus;
  }

  spdlog::info("OptimizerClient - client run connector request");
  errorStatus = sendRunConnectorRequest(engineId, socket);
  if (errorStatus != 0)
  {
    spdlog::error("Engine run model request failed");
    return errorStatus;
  }

  spdlog::info("OptimizerClient - client send engine wait request");
  errorStatus = sendConnectorWaitRequst(engineId, socket);
  if (errorStatus != 0)
  {
    spdlog::error("Engine wait request failed");
    return errorStatus;
  }

  spdlog::info("OptimizerClient - client send delete connector request");
  errorStatus = sendDeleteConnectorRequest(engineId, socket);
  if (errorStatus != 0)
  {
    spdlog::error("Engine deletion request failed");
    return errorStatus;
  }

  return errorStatus;
}  // sendConnectorRequestToService

int OptimizerClient::sendORRequestToService(const std::string& engineId,
                                            const std::string& modelRequest,
                                            const SocketSPtr& socket)
{
  // Error status, zero means no error
  int errorStatus = 0;

  spdlog::info("OptimizerClient - client send engine creation request");
  errorStatus = sendCreateOptimizerRequest(engineId, socket);
  if (errorStatus != 0)
  {
    spdlog::error("Engine creation request failed");
    return errorStatus;
  }

  spdlog::info("OptimizerClient - client send engine wait request");
  errorStatus = sendOptimizerWaitRequst(engineId, socket);
  if (errorStatus != 0)
  {
    spdlog::error("Engine wait request failed");
    return errorStatus;
  }

  spdlog::info("OptimizerClient - client send load model request");
  errorStatus = sendLoadModelRequest(engineId, modelRequest, socket);
  if (errorStatus != 0)
  {
    spdlog::error("Engine load model request failed");
    return errorStatus;
  }

  spdlog::info("OptimizerClient - client send engine wait request");
  errorStatus = sendOptimizerWaitRequst(engineId, socket);
  if (errorStatus != 0)
  {
    spdlog::error("Engine wait request failed");
    return errorStatus;
  }

  spdlog::info("OptimizerClient - client run model request");
  errorStatus = sendRunModelRequest(engineId, socket);
  if (errorStatus != 0)
  {
    spdlog::error("Engine run model request failed");
    return errorStatus;
  }

  spdlog::info("OptimizerClient - client send engine wait request");
  errorStatus = sendOptimizerWaitRequst(engineId, socket);
  if (errorStatus != 0)
  {
    spdlog::error("Engine wait request failed");
    return errorStatus;
  }

  spdlog::info("OptimizerClient - client send get solution request");
  errorStatus = sendGetSolutionRequest(engineId, 1, socket);
  if (errorStatus != 0)
  {
    spdlog::error("Engine get solution request failed");
    return errorStatus;
  }

  spdlog::info("OptimizerClient - client send engine wait request");
  errorStatus = sendOptimizerWaitRequst(engineId, socket);
  if (errorStatus != 0)
  {
    spdlog::error("Engine wait request failed");
    return errorStatus;
  }

  spdlog::info("OptimizerClient - client send delete optimizer request");
  errorStatus = sendDeleteOptimizerRequest(engineId, socket);
  if (errorStatus != 0)
  {
    spdlog::error("Engine deletion request failed");
    return errorStatus;
  }

  return errorStatus;
}  // sendORRequestToService


void OptimizerClient::handleReplyMessage(const OptiLabReplyMessage& repMsg)
{
  /*
  const auto replyType = repMsg.type();
  switch (replyType)
  {
    case optilab::OptiLabReplyMessage::Solution:
    {
      OptimizerSolutionRep solRep = repMsg.GetExtension(OptimizerSolutionRep::id);
      const auto& sol = solRep.solution();
      spdlog::info("OptimizerClient - SOLUTION:\n" + sol);
      break;
    }
    default:
    {
      std::string errMsg = "Unrecognized message reply type";
      spdlog::error(errMsg);
      throw std::runtime_error(errMsg);
    }
  }*/
  throw std::runtime_error("Deprecated");
}  // handleReplyMessage

void OptimizerClient::handleIncomingMessages(optilab::OptimizerClient::SocketSPtr socket)
{
  /*
  bool runReaciverLoop = true;
  while (runReaciverLoop)
  {
    std::string reply;
    try
    {
      reply = s_recv(*socket);
    }
    catch (...)
    {
      spdlog::error("Error reading from the socket");
      break;
    }

    OptiLabReplyMessage repMsg;
    repMsg.ParseFromString(reply);

    if (repMsg.replystatus() != optilab::ReplyStatus::OK)
    {
      spdlog::error("Received error from a reply message");
      pErrorResponse = true;
      break;
    }

    // Reply was okay, add the reply message id to the set of OK replies.
    // @note this should happen only after being successfully logged into the service
    if (loginSuccess)
    {
      UniqueLock lock(pRequestResponseStateMutex);
      pOKReplaySet.insert(kMessageId);
      pRequestResponseStateConditionVariable.notify_all();
    }

    const auto replyType = repMsg.type();

    switch (replyType)
    {
      case optilab::OptiLabReplyMessage::InformationalMessage:
      {
        // Do not perform any action
        break;
      }
      case optilab::OptiLabReplyMessage::ServiceLogin:
      {
        ServiceLoginRep loginRep = repMsg.GetExtension(ServiceLoginRep::id);
        if (loginRep.login() == ServiceLoginRep::SUCCESS)
        {
          // Initialize the OK set with the id of the first message to let the client
          // send the first message
          spdlog::info("OptimizerClient - login success");

          UniqueLock lock(pRequestResponseStateMutex);
          loginSuccess = true;
          pOKReplaySet.insert(kMessageId);
          pRequestResponseStateConditionVariable.notify_all();
        }
        else
        {
          spdlog::error("OptimizerClient - failed login");
          pErrorResponse = true;
          runReaciverLoop = false;
        }
        break;
      }
      case optilab::OptiLabReplyMessage::ServiceLogoff:
      {
        ServiceLogoffRep logoffRep = repMsg.GetExtension(ServiceLogoffRep::id);
        if (logoffRep.logoff() != ServiceLogoffRep::SUCCESS)
        {
          spdlog::error("OptimizerClient - failed log off");
        }

        pLogoffSuccess = true;
        runReaciverLoop = false;

        break;
      }
      default:
      {
        handleReplyMessage(repMsg);
        break;
      }
    }
  }  // while
  */
  throw std::runtime_error("Deprecated");
}  // handleIncomingMessages

int OptimizerClient::sendCreateOptimizerRequest(const std::string& optimizerId,
                                                const SocketSPtr& socket)
{
  OptilabRequestMessage createEngineReq;
  if (pFrameworkType == std::string(FRAMEWORK_CP))
  {
    spdlog::info("OptimizerClient - sending a request for CP OR-TOOLS optimizer creation");
    createEngineReq = clientutils::buildCreateCPOptimizerMessage(optimizerId,
                                                                 optilab::PkgType::OR_TOOLS);
  }
  else if (pFrameworkType == std::string(FRAMEWORK_FZ) ||
      pFrameworkType == std::string(FRAMEWORK_CP_GECODE))
  {
    spdlog::info("OptimizerClient - sending a request for CP Gecode optimizer creation");
    createEngineReq = clientutils::buildCreateCPOptimizerMessage(optimizerId,
                                                                 optilab::PkgType::GECODE);
  }
  else if (pFrameworkType == std::string(FRAMEWORK_MIP) ||
      pFrameworkType == std::string(FRAMEWORK_MPS))
  {
    spdlog::info("OptimizerClient - sending a request for MIP optimizer creation");
    createEngineReq = clientutils::buildCreateMIPOptimizerMessage(optimizerId);
  }
  else
  {
    spdlog::warn("OptimizerClient - unrecognized framework, defaulting to CP OR-TOOLS");
    spdlog::info("OptimizerClient - sending a request for CP optimizer creation");
    createEngineReq = clientutils::buildCreateCPOptimizerMessage(optimizerId,
                                                                 optilab::PkgType::OR_TOOLS);
  }

  return sendRequestMessageToService(createEngineReq, socket);
}  // sendCreateOptimizerRequest

int OptimizerClient::sendCreateConnectorRequest(const std::string& optimizerId,
                                                const SocketSPtr& socket)
{
  OptilabRequestMessage createEngineReq;
  createEngineReq = clientutils::buildCreateConnectorMessage(optimizerId);

  return sendRequestMessageToService(createEngineReq, socket);
}  // sendCreateConnectorRequest

int OptimizerClient::sendDeleteOptimizerRequest(const std::string& optimizerId,
                                                const SocketSPtr& socket)
{
  optilab::PkgType pgkType = (pFrameworkType == std::string(FRAMEWORK_CP)) ?
      optilab::PkgType::OR_TOOLS :
      optilab::PkgType::GECODE;
  const auto deleteEngineReq = clientutils::buildDeleteOptimizerMessage(optimizerId, pgkType);
  return sendRequestMessageToService(deleteEngineReq, socket);
}  // sendCreateOptimizerRequest

int OptimizerClient::sendDeleteConnectorRequest(const std::string& optimizerId,
                                                const SocketSPtr& socket)
{
  const auto deleteEngineReq = clientutils::buildDeleteConnectorMessage(optimizerId);
  return sendRequestMessageToService(deleteEngineReq, socket);
}  // sendDeleteConnectorRequest

int OptimizerClient::sendOptimizerWaitRequst(const std::string& optimizerId,
                                             const SocketSPtr& socket, int timoutSec)
{
  optilab::PkgType pgkType = (pFrameworkType == std::string(FRAMEWORK_CP)) ?
      optilab::PkgType::OR_TOOLS :
      optilab::PkgType::GECODE;
  const auto waitReq = clientutils::buildOptimizerWaitMessage(optimizerId, pgkType,timoutSec);
  return sendRequestMessageToService(waitReq, socket);
}  // sendOptimizerWaitRequst

int OptimizerClient::sendConnectorWaitRequst(const std::string& optimizerId,
                                             const SocketSPtr& socket, int timoutSec)
{
  const auto waitReq = clientutils::buildConnectorWaitMessage(optimizerId, timoutSec);
  return sendRequestMessageToService(waitReq, socket);
}  // sendConnectorWaitRequst

int OptimizerClient::sendLoadModelRequest(const std::string& optimizerId,
                                          const std::string jsonModel, const SocketSPtr& socket)
{
  optilab::PkgType pgkType = (pFrameworkType == std::string(FRAMEWORK_CP)) ?
      optilab::PkgType::OR_TOOLS :
      optilab::PkgType::GECODE;
  const auto loadModelReq = clientutils::buildLoadModelMessage(optimizerId, jsonModel, pgkType);
  return sendRequestMessageToService(loadModelReq, socket);
}  // sendCreateOptimizerRequest

int OptimizerClient::sendLoadConnectionsRequest(const std::string& optimizerId,
                                                const std::string jsonModel,
                                                const SocketSPtr& socket)
{
  const auto loadModelReq = clientutils::buildLoadConnectionsMessage(optimizerId, jsonModel);
  return sendRequestMessageToService(loadModelReq, socket);
}  // sendCreateOptimizerRequest

int OptimizerClient::sendRunModelRequest(const std::string& optimizerId, const SocketSPtr& socket)
{
  optilab::PkgType pgkType = (pFrameworkType == std::string(FRAMEWORK_CP)) ?
      optilab::PkgType::OR_TOOLS :
      optilab::PkgType::GECODE;
  const auto runModelReq = clientutils::buildRunModelMessage(optimizerId, pgkType);
  return sendRequestMessageToService(runModelReq, socket);
}  // sendCreateOptimizerRequest

int OptimizerClient::sendRunConnectorRequest(const std::string& optimizerId, const SocketSPtr& socket)
{
  const auto runModelReq = clientutils::buildRunConnectorMessage(optimizerId);
  return sendRequestMessageToService(runModelReq, socket);
}  // sendCreateOptimizerRequest

int OptimizerClient::sendGetSolutionRequest(const std::string& optimizerId, int numSol,
                                            const SocketSPtr& socket)
{
  optilab::PkgType pgkType = (pFrameworkType == std::string(FRAMEWORK_CP)) ?
      optilab::PkgType::OR_TOOLS :
      optilab::PkgType::GECODE;
  const auto getSolReq = clientutils::buildModelSolutionsMessage(optimizerId, numSol, pgkType);
  return sendRequestMessageToService(getSolReq, socket);
}  // sendGetSolutionRequest

int OptimizerClient::sendRequestMessageToService(const optilab::OptilabRequestMessage& req,
                                                 const optilab::OptimizerClient::SocketSPtr& socket)
{
  assert(socket);

  // Error status, zero means no error
  int errorStatus = 0;

  // Serialize the message to send back to the client
  std::string reqMsg;
  try
  {
    reqMsg = req.SerializeAsString();
  }
  catch(...)
  {
    errorStatus = 1;
    return errorStatus;
  }

  // Before sending a request check if there is any error from some previous responses
  if (pErrorResponse)
  {
    spdlog::error("OptimizerClient - cannot send request to the client due to error in some"
        " previous response");
    errorStatus = 1;
    return errorStatus;
  }

  // Check if the client received the OK response for the previous message, if so continue.
  // If not, put this thread on old until an OK response is received or a timeout is expired
  {
    UniqueLock lock(pRequestResponseStateMutex);
    if (!checkOKReply(pOKReplaySet, kMessageId))
    {
      pRequestResponseStateConditionVariable.wait(
          lock, [&]{ return checkOKReply(std::ref(pOKReplaySet), kMessageId); });
    }

    // Remove this message OK id
    pOKReplaySet.erase(kMessageId);
  }

  // Send the message back to the client
  s_send(*socket, reqMsg);

  return errorStatus;
}  // sendRequestMessageToService

}  // namespace optilab
