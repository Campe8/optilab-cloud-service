//
// Copyright OptiLab 2019. All rights reserved.
//
// Optimizer client.
// It is the front-end client sending requests to the broker.
//

#pragma once

#include <atomic>
#include <condition_variable>
#include <memory>   // for std::shared_ptr
#include <mutex>
#include <string>
#include <thread>
#include <unordered_set>

#include "zeromq/zhelpers.hpp"

#include "optimizer_service/socket_connector.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS OptimizerClient {
public:
  using SPtr = std::shared_ptr<OptimizerClient>;
  using SocketSPtr = std::shared_ptr<zmq::socket_t>;

public:
  /// Constructor: creates a new front-end client connecting to the broker on the given
  /// port and address.
  /// The "framework" argument specifies which simple model to run, e.g., CP, MIP, etc.
  /// @note if the addresses are empty, "localhost" will be used.
  /// @note throws std::invalid_argument if the ports are less than 1 or greater than
  /// optimizerservice::MAX_SERVICE_PORT_NUMBER
  OptimizerClient(const int frontendPort, const std::string& framework,
                  const std::string& frontendAddr = std::string());

  virtual ~OptimizerClient() = default;

  /// Runs the client: it connects broker and sends a "dummy" model.
  /// @note this method is used mostly for testing
  int run();

protected:
  /// Prepares the client and internal state to start sending requests to the service
  virtual void startupClient(const SocketSPtr& socket);

  /// Sends a request to the service (e.g., solve model).
  /// Returns zero on success, non-zero otherwise
  virtual int sendRequestToService(const SocketSPtr& socket);

  /// Sends OR request to the service (e.g., solve model)
  virtual int sendORRequestToService(const std::string& engineId, const std::string& modelRequest,
                                     const SocketSPtr& socket);

  /// Sends CONNECTOR request to the service (e.g., solve model)
  virtual int sendConnectorRequestToService(const std::string& engineId,
                                            const std::string& modelRequest,
                                            const SocketSPtr& socket);

  /// Cleans up the client and internal state
  virtual void cleanupClient(const SocketSPtr& socket);

  /// Send a create optimizer request to the service.
  /// Returns zero on success, non-zero otherwise
  int sendCreateOptimizerRequest(const std::string& optimizerId, const SocketSPtr& socket);

  /// Send a create connector request to the service.
  /// Returns zero on success, non-zero otherwise
  int sendCreateConnectorRequest(const std::string& optimizerId, const SocketSPtr& socket);

  /// Send a delete optimizer request to the service.
  /// Returns zero on success, non-zero otherwise
  int sendDeleteOptimizerRequest(const std::string& optimizerId, const SocketSPtr& socket);

  /// Send a delete connector request to the service.
  /// Returns zero on success, non-zero otherwise
  int sendDeleteConnectorRequest(const std::string& optimizerId, const SocketSPtr& socket);

  /// Send an optimizer wait request to the service.
  /// Returns zero on success, non-zero otherwise
  int sendOptimizerWaitRequst(const std::string& optimizerId, const SocketSPtr& socket,
                              int timoutSec = -1);

  /// Send an connector wait request to the service.
  /// Returns zero on success, non-zero otherwise
  int sendConnectorWaitRequst(const std::string& optimizerId, const SocketSPtr& socket,
                              int timoutSec = -1);

  /// Send a load model request to the service.
  /// Returns zero on success, non-zero otherwise
  int sendLoadModelRequest(const std::string& optimizerId, const std::string jsonModel,
                           const SocketSPtr& socket);

  /// Send a load connections request to the service.
  /// Returns zero on success, non-zero otherwise
  int sendLoadConnectionsRequest(const std::string& optimizerId, const std::string jsonModel,
                                 const SocketSPtr& socket);

  /// Send a run model request to the service.
  /// Returns zero on success, non-zero otherwise
  int sendRunModelRequest(const std::string& optimizerId, const SocketSPtr& socket);

  /// Send a run connector request to the service.
  /// Returns zero on success, non-zero otherwise
  int sendRunConnectorRequest(const std::string& optimizerId, const SocketSPtr& socket);

  /// Send a get solutions request to the service.
  /// Returns zero on success, non-zero otherwise
  int sendGetSolutionRequest(const std::string& optimizerId, int numSol, const SocketSPtr& socket);

private:
  using UniqueLock = std::unique_lock<std::mutex>;

private:
  /// Port the client connects to
  const int pFrontendPort;

  /// Framework of the model to run
  const std::string pFrameworkType;

  /// Address the client connects to
  const std::string pFrontendAddr;

  /// Flag indicating whether the client was able to successfully login into the service or not
  bool loginSuccess{false};

  /// Flag indicating an error from the backend
  std::atomic<bool> pErrorResponse{false};

  /// Flag used to check whether the log-off was successful of not
  std::atomic<bool> pLogoffSuccess{false};

  /// Thread running the front-end reading process
  std::shared_ptr<std::thread> pMessageReader;

  /// Set of ids of messages that got an OK response from the service
  std::unordered_set<int> pOKReplaySet;

  /// Mutex used to synchronize requests/replies messages
  std::mutex pRequestResponseStateMutex;

  /// Condition variable used to synchronize requests/replies messages
  std::condition_variable pRequestResponseStateConditionVariable;

  /// Handles all incoming messages from the socket
  void handleIncomingMessages(optilab::OptimizerClient::SocketSPtr socket);

  /// Handles a reply message
  void handleReplyMessage(const OptiLabReplyMessage& repMsg);

  /// Sends the given request to the service using the given socket.
  /// Returns zero on success, non-zero otherwise
  int sendRequestMessageToService(const optilab::OptilabRequestMessage& req,
                                  const optilab::OptimizerClient::SocketSPtr& socket);
};

}  // namespace optilab
