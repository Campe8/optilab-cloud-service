#include "optimizer_client/client_utilities.hpp"

#include <memory>  // for std::make_shared
#include <stdexcept>

namespace {

optilab::OptilabRequestMessage buildCreateOptimizerMessage(
    const std::string& engineId, optilab::CreateOptimizerReq::EngineType engineType,
    optilab::PkgType pkgType)
{
  /*
  optilab::OptilabRequestMessage req;
  optilab::CreateOptimizerReq optReq;
  optReq.set_enginetype(engineType);
  auto mutExt = (req.MutableExtension(optilab::CreateOptimizerReq::id));
  mutExt->CopyFrom(optReq);

  req.set_type(optilab::OptilabRequestMessage::Type::OptilabRequestMessage_Type_EngineEvent);
  req.set_pkgtype(pkgType);
  req.set_requesttype(optilab::RequestType::POST);
  req.set_frameworktype(optilab::FrameworkType::OR);
  req.set_messageinfo(engineId);
  return req;
  */
  throw std::runtime_error("Deprecated functionality");
}  // buildCreateEngineMessage

}  // namespace

namespace optilab {

namespace clientutils {

OptilabRequestMessage buildCreateCPOptimizerMessage(const std::string& engineId,
                                                    optilab::PkgType pkgType)
{
  return buildCreateOptimizerMessage(
      engineId, optilab::CreateOptimizerReq::EngineType::CreateOptimizerReq_EngineType_CP,
      pkgType);
}  // buildCreateCPOptimizerMessage

OptilabRequestMessage buildCreateMIPOptimizerMessage(const std::string& engineId)
{
  return buildCreateOptimizerMessage(
      engineId, optilab::CreateOptimizerReq::EngineType::CreateOptimizerReq_EngineType_MIP,
      optilab::PkgType::OR_TOOLS);
}  // buildCreateMIPOptimizerMessage

OptilabRequestMessage buildCreateConnectorMessage(const std::string& engineId)
{
  /*
  optilab::CreateConnectorReq optReq;
  optReq.set_connectortype(
      optilab::CreateConnectorReq::ConnectorType::CreateConnectorReq_ConnectorType_COMBINATOR);

  optilab::OptilabRequestMessage req;
  auto mutExt = (req.MutableExtension(optilab::CreateConnectorReq::id));
  mutExt->CopyFrom(optReq);

  req.set_type(optilab::OptilabRequestMessage::Type::OptilabRequestMessage_Type_EngineEvent);
  req.set_requesttype(optilab::RequestType::POST);
  req.set_frameworktype(optilab::FrameworkType::CONNECTORS);
  req.set_messageinfo(engineId);

  return req;
  */

  throw std::runtime_error("Deprecated functionality");
}  // buildCreateConnectorMessage

OptilabRequestMessage buildDeleteOptimizerMessage(const std::string& engineId,
                                                  optilab::PkgType pkgType)
{
  /*
  CreateOptimizerReq optReq;

  OptilabRequestMessage req;
  auto mutExt = (req.MutableExtension(CreateOptimizerReq::id));
  mutExt->CopyFrom(optReq);

  req.set_type(OptilabRequestMessage::Type::OptilabRequestMessage_Type_EngineEvent);
  req.set_pkgtype(pkgType);
  req.set_requesttype(RequestType::DELETE);
  req.set_frameworktype(optilab::FrameworkType::OR);
  req.set_messageinfo(engineId);

  return req;
  */

  throw std::runtime_error("Deprecated functionality");
}  // buildCreateEngineMessage

OptilabRequestMessage buildDeleteConnectorMessage(const std::string& engineId)
{
  /*
  CreateConnectorReq optReq;

  OptilabRequestMessage req;
  auto mutExt = (req.MutableExtension(CreateConnectorReq::id));
  mutExt->CopyFrom(optReq);

  req.set_type(OptilabRequestMessage::Type::OptilabRequestMessage_Type_EngineEvent);
  req.set_requesttype(RequestType::DELETE);
  req.set_frameworktype(optilab::FrameworkType::CONNECTORS);
  req.set_messageinfo(engineId);

  return req;
  */

  throw std::runtime_error("Deprecated functionality");
}  // buildDeleteConnectorMessage


OptilabRequestMessage buildLoadModelMessage(const std::string& engineId,
                                            const std::string& jsonModel,
                                            optilab::PkgType pkgType)
{
  /*
  RunModelReq optReq;
  optReq.set_jsonmodel(jsonModel);

  OptilabRequestMessage req;
  auto mutExt = (req.MutableExtension(RunModelReq::id));
  mutExt->CopyFrom(optReq);

  req.set_type(OptilabRequestMessage::Type::OptilabRequestMessage_Type_EngineEvent);
  req.set_pkgtype(pkgType);
  req.set_requesttype(RequestType::POST);
  req.set_frameworktype(optilab::FrameworkType::OR);
  req.set_messageinfo(engineId);

  return req;
  */

  throw std::runtime_error("Deprecated functionality");
}  // buildLoadModelMessage

OptilabRequestMessage buildLoadConnectionsMessage(const std::string& engineId,
                                                  const std::string& jsonModel)
{
  /*
  ExecuteConnectorReq optReq;
  optReq.set_connectionsdescriptor(jsonModel);

  OptilabRequestMessage req;
  auto mutExt = (req.MutableExtension(ExecuteConnectorReq::id));
  mutExt->CopyFrom(optReq);

  req.set_type(OptilabRequestMessage::Type::OptilabRequestMessage_Type_EngineEvent);
  req.set_requesttype(RequestType::POST);
  req.set_frameworktype(optilab::FrameworkType::CONNECTORS);
  req.set_messageinfo(engineId);

  return req;
  */

  throw std::runtime_error("Deprecated functionality");
}  // buildLoadConnectionsMessage

OptilabRequestMessage buildRunModelMessage(const std::string& engineId, optilab::PkgType pkgType)
{
  /*
  RunModelReq optReq;

  OptilabRequestMessage req;
  auto mutExt = (req.MutableExtension(RunModelReq::id));
  mutExt->CopyFrom(optReq);

  req.set_type(OptilabRequestMessage::Type::OptilabRequestMessage_Type_EngineEvent);
  req.set_pkgtype(pkgType);
  req.set_requesttype(RequestType::PUT);
  req.set_frameworktype(optilab::FrameworkType::OR);
  req.set_messageinfo(engineId);

  return req;
  */

  throw std::runtime_error("Deprecated functionality");
}  // buildLoadModelMessage

OptilabRequestMessage buildRunConnectorMessage(const std::string& engineId)
{
  /*
  ExecuteConnectorReq optReq;

  OptilabRequestMessage req;
  auto mutExt = (req.MutableExtension(ExecuteConnectorReq::id));
  mutExt->CopyFrom(optReq);

  req.set_type(OptilabRequestMessage::Type::OptilabRequestMessage_Type_EngineEvent);
  req.set_requesttype(RequestType::PUT);
  req.set_frameworktype(optilab::FrameworkType::CONNECTORS);
  req.set_messageinfo(engineId);

  return req;
  */

  throw std::runtime_error("Deprecated functionality");
}  // buildRunConnectorMessage

OptilabRequestMessage buildModelSolutionsMessage(const std::string& engineId, int numSol,
                                                 optilab::PkgType pkgType)
{
  /*
  ModelSolutionsReq optReq;
  optReq.set_numsolutions(numSol);

  OptilabRequestMessage req;
  auto mutExt = (req.MutableExtension(ModelSolutionsReq::id));
  mutExt->CopyFrom(optReq);

  req.set_type(OptilabRequestMessage::Type::OptilabRequestMessage_Type_EngineEvent);
  req.set_pkgtype(pkgType);
  req.set_requesttype(RequestType::GET);
  req.set_frameworktype(optilab::FrameworkType::OR);
  req.set_messageinfo(engineId);

  return req;
  */

  throw std::runtime_error("Deprecated functionality");
}  // buildModelSolutionsMessage

OptilabRequestMessage buildOptimizerWaitMessage(const std::string& engineId,
                                                optilab::PkgType pkgType, int waitSec)
{
  /*
  OptimizerWaitReq optReq;
  optReq.set_waittimeout(waitSec);

  OptilabRequestMessage req;
  auto mutExt = (req.MutableExtension(OptimizerWaitReq::id));
  mutExt->CopyFrom(optReq);

  req.set_type(OptilabRequestMessage::Type::OptilabRequestMessage_Type_EngineEvent);
  req.set_pkgtype(pkgType);
  req.set_requesttype(RequestType::POST);
  req.set_frameworktype(optilab::FrameworkType::OR);
  req.set_messageinfo(engineId);

  return req;
  */

  throw std::runtime_error("Deprecated functionality");
}  // buildModelSolutionsMessage

OptilabRequestMessage buildConnectorWaitMessage(const std::string& engineId, int waitSec)
{
  /*
  OptimizerWaitReq optReq;
  optReq.set_waittimeout(waitSec);

  OptilabRequestMessage req;
  auto mutExt = (req.MutableExtension(OptimizerWaitReq::id));
  mutExt->CopyFrom(optReq);

  req.set_type(OptilabRequestMessage::Type::OptilabRequestMessage_Type_EngineEvent);
  req.set_requesttype(RequestType::POST);
  req.set_frameworktype(optilab::FrameworkType::CONNECTORS);
  req.set_messageinfo(engineId);

  return req;
  */

  throw std::runtime_error("Deprecated functionality");
}  // buildConnectorWaitMessage

}  // namespace clientutils

}  // namespace optilab
