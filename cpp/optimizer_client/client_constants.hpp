//
// Copyright OptiLab 2019. All rights reserved.
//
// Constants for the front-end client.
//

#pragma once

namespace optilab {

namespace optimizerclient {

extern const char CLIENT_LOGIN_MESSAGE[];
extern const char CLIENT_LOGOFF_MESSAGE[];
extern const char CLIENT_LOGOFF_SUCCESS_MESSAGE[];
extern const char CLIENT_PREFIX_TCP_NETWORK_ADDR[];
extern const int DEFAULT_CLIENT_CONTEXT_NUM_THREADS;
extern const char LOCAL_HOST_NAME[];
extern const int MAX_SERVICE_PORT_NUMBER;

}  // optimizerclient

// Test Json models
namespace optimizerclienttest {

extern const char kDummyMPSORToolsModel[];
extern const char kDummyCPORToolsModel[];
extern const char kDummyCPGecodeModel[];
extern const char kDummyCombinatorModel[];
extern const char kDummyMIPORToolsModel[];
extern const char kDummyFlatZincGecodeModel[];

}  // optimizerclienttest

}  // namespace optilab
