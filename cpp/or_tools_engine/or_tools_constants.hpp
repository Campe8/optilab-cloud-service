//
// Copyright OptiLab 2019. All rights reserved.
//
// Constants for OR-Tools engine
//

#pragma once

#include <cstdint>  // for int64_t

namespace optilab {

namespace cpconstants {
extern const int64_t INT_VAR_LB_INF;
extern const int64_t INT_VAR_UB_INF;
}  // namespace cpconstants

}  // namespace optilab
