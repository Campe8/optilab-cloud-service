//
// Copyright OptiLab 2019. All rights reserved.
//
// CPSolver for OR Tools package.
//

#pragma once

#include <memory>   // for std::shared_ptr

#include "ortools/constraint_solver/constraint_solveri.h"
#include "or_tools_engine/or_tools_cp_model.hpp"
#include "solver/cp_solver.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS ORToolsCPSolver : public CPSolver {
 public:
  using SPtr = std::shared_ptr<ORToolsCPSolver>;
  using SolverPtr = std::shared_ptr<operations_research::Solver>;

 public:
  /// Constructor.
  /// @note throws std::invalid_argument on empty argument pointer
  ORToolsCPSolver(const ORToolsCPModel::SPtr& cpModel);

  virtual ~ORToolsCPSolver() = default;

  /// Initializes the solver for the search
  void initSearch() override;

  /// Executes cleanup code after search is completed
  void cleanupSearch() override;

  /// Performs constraint propagation only.
  /// Returns true if the current variable assignment satisfies all constraints.
  /// Returns false otherwise
  bool constraintPropagation() override;

  /// Runs the solver until a solution (next solution) is found.
  /// Returns true when there is a "next" solution, returns false otherwise.
  /// This method is supposed to be called within a loop, for example:
  /// while (nextSolution())
  /// {
  ///    use solution;
  /// }
  bool nextSolution() override;

  /// Stops the (ongoing) search process and return
  void interruptSearchProcess() override;

  /// Returns the internal ORTools solver instance
  inline const SolverPtr& getSolver() const { return pSolver; }

  /// Sets the search heuristic used by this solver to explore the search space.
  /// @note throws std::invalid_argument on nullptr pointer
  void setSearchHeuristic(operations_research::DecisionBuilder* huristic);

 private:
  using MonitorsList = std::vector<operations_research::SearchMonitor*>;

 private:
  /// The actual ORTools solver
  SolverPtr pSolver;

  /// Flag indicating whether the solver should be running or not
  bool pSolverActive{true};

  /// Flag indicating whether the solver has been initialized or not
  bool pSolverInitialized{false};

  /// Search heuristic used by this solver
  operations_research::DecisionBuilder* pHeuristic{nullptr};

  /// List of all the monitors of this model
  MonitorsList pSearchMonitorsList;

  /// Returns true if the solver should stop running. Returns false otherwise
  bool checkWhetherSolverShouldStop();

  /// Sets the status of the CPSolver w.r.t. the status of the ORTools solver
  void setCPSolverStatusOnORToolsSolverState();
};

}  // namespace optilab
