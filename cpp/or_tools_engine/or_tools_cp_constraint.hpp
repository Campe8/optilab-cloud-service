//
// Copyright OptiLab 2019. All rights reserved.
//
// Class encapsulating an OR-Tools CP constraint.
//

#pragma once

#include <cstdint>  // for int64_t
#include <memory>   // for std::shared_ptr
#include <stdexcept>
#include <string>
#include <vector>

#include "ortools/constraint_solver/constraint_solveri.h"
#include "solver/cp_constraint.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS ORToolsCPConstraint : public CPConstraint {
 public:
  using SPtr = std::shared_ptr<ORToolsCPConstraint>;

 public:
  /// Creates a OR-Tools CP constraint.
  /// @note throws std::invalid_argument if the pointer to the OR-Tools solver is nullptr
  ORToolsCPConstraint(const std::string& name, const std::vector<CPArgument>& args,
                      PropagationType propType, operations_research::Solver* solver)
 : CPConstraint(name, args, propType),
   pSolver(solver)
 {
    if (!pSolver)
    {
      throw std::invalid_argument("ORToolsCPConstraint - NULL pointer to solver");
    }
 }

  /// Returns the solver registering this constraint
 inline operations_research::Solver* getSolver() const { return pSolver; }

 private:
  /// Pointer to the solver used to create this instance of constraint
  operations_research::Solver* pSolver;
};

}  // namespace optilab
