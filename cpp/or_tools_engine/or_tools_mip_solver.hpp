//
// Copyright OptiLab 2019. All rights reserved.
//
// MIPSolver for OR Tools package.
//

#pragma once

#include <memory>   // for std::shared_ptr

#include "or_tools_engine/or_tools_mip_model.hpp"
#include "or_tools_engine/or_tools_utilities.hpp"
#include "ortools/linear_solver/linear_solver.h"
#include "ortools/linear_solver/linear_solver.pb.h"
#include "solver/mip_solver.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS ORToolsMIPSolver : public MIPSolver {
 public:
  using SPtr = std::shared_ptr<ORToolsMIPSolver>;
  using SolverPtr = std::shared_ptr<operations_research::MPSolver>;

 public:
  /// Constructor.
  /// @note throws std::invalid_argument on empty argument pointer
  ORToolsMIPSolver(const ORToolsMIPModel::SPtr& mipModel);

  virtual ~ORToolsMIPSolver() = default;

  /// Runs the solver until a solution is found or a given timeout is reached
  void solve() override;

  /// Returns the current status of the search result
  ortoolsengine::ORToolsMIPResult::ResultStatus getResultStatus() const override;

  /// Returns the ORTools protobuf solution response.
  /// @note this solution is a valid solution only if the "solve(...)" method
  /// has been already called
  operations_research::MPSolutionResponse getORToolsProtobufResult() const;

  /// Returns the value of the objective.
  /// @note this method is supposed to be called after the "solver(...)" method
  double getObjectiveValue() const;

  /// Stops the (ongoing) search process and returns true if the solver
  /// was actually interrupted, returns false otherwise.
  /// @note depending on the package in use, some solvers don't provide
  /// interruption functionalities
  bool interruptSearchProcess() override;

  /// Returns the internal ORTools solver instance
  inline const SolverPtr& getSolver() const { return pSolver; }

 private:
  /// The actual ORTools MIP solver
  SolverPtr pSolver;

  /// Flag indicating whether the solver should be running or not
  bool pSolverActive{true};

  /// Flag indicating whether the solver has been initialized or not
  bool pSolverInitialized{false};

  /// Status of the search result
  operations_research::MPSolver::ResultStatus pResultStatus;
};

}  // namespace optilab
