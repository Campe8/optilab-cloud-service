//
// Copyright OptiLab 2019. All rights reserved.
//
// Class encapsulating an OR-Tools MIP variable.
//

#pragma once

#include <memory>   // for std::shared_ptr
#include <string>
#include <vector>

#include "ortools/linear_solver/linear_solver.h"
#include "solver/mip_variable.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_STRUCT ORToolsMIPVariable : public MIPVariable {
 public:
  using SPtr = std::shared_ptr<ORToolsMIPVariable>;

 public:
  /// Creates a scalar variable with given name and bounds.
  /// @note if "isContinuous" is true, the variable is a continuous variable, otherwise the
  /// variable is an Integer variable
  ORToolsMIPVariable(const std::string& name, double min, double max, bool isContinuous,
                     operations_research::MPSolver* solver);

  /// Creates an array of integer variables according to the given list of dimensions.
  /// @note if "isContinuous" is true, the variable is a continuous variable, otherwise the
  /// variable is an Integer variable
  ORToolsMIPVariable(const std::string& name, double min, double max, bool isContinuous, int len,
                     operations_research::MPSolver* solver);

  /// Creates an n-dimensional matrix of integer variables according to the given
  /// list of dimensions.
  /// @note if "isContinuous" is true, the variable is a continuous variable, otherwise the
  /// variable is an Integer variable
  /// @note dims size must be at least 2
  ORToolsMIPVariable(const std::string& name, double min, double max, bool isContinuous,
                     const std::vector<int>& dims, operations_research::MPSolver* solver);

  /// Creates a scalar Boolean variable with given name
  ORToolsMIPVariable(const std::string& name, operations_research::MPSolver* solver);

  /// Creates an array of Boolean variables according to the given list of dimensions
  ORToolsMIPVariable(const std::string& name, int len, operations_research::MPSolver* solver);

  /// Creates an n-dimensional matrix of Boolean variables according to the given list
  /// of dimensions.
  /// @note dims size must be at least 2
  ORToolsMIPVariable(const std::string& name, const std::vector<int>& dims,
                     operations_research::MPSolver* solver);

  /// Returns the actual variable (if scalar) or the first variable of an array/matrix
  inline operations_research::MPVariable* getVariable() const
  {
    return pVariableList.at(0);
  }

  /// Returns the vectorized list of variables
  inline const std::vector<operations_research::MPVariable*>& getVariableList() const
  {
    return pVariableList;
  }

  /// Returns the variable at the given subscript index.
  /// @note throws std::runtime_error on scalar variables.
  /// @note throws std::runtime_error on wrong indexing
  operations_research::MPVariable* getVariable(const std::vector<int>& subscr);

 protected:
  MIPVariable::SPtr getVariableFromIndex(const std::string& varName,
                                         const std::vector<int>& subscr) override;

 private:
  enum VarType {
    INT_VAR = 0,
    BOOL_VAR,
    CONTINUOUS_VAR
  };

 private:
  /// Type of this variable
  VarType pType;

  /// Lower bound of the variable's domain
  double pLowerBound;

  /// Upper bound of the variable's domain
  double pUpperBound;

  /// Pointer to the solver used to create this instance of variable
  operations_research::MPSolver* pSolver;

  /// List of all variables (scalar, array, matrix) encapsulated by this instance
  std::vector<operations_research::MPVariable*> pVariableList;
};

}  // namespace optilab
