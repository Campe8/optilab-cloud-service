//
// Copyright OptiLab 2019. All rights reserved.
//
// Class encapsulating an OR-Tools MIP objective.
//

#pragma once

#include <memory>  // for std::shared_ptr
#include <string>
#include <vector>

#include "model/model_object_mip_objective.hpp"
#include "ortools/linear_solver/linear_solver.h"
#include "solver/mip_objective.hpp"
#include "solver/mip_variable.hpp"
#include "system/system_export_defs.hpp"

namespace optilab
{
class SYS_EXPORT_STRUCT ORToolsMIPObjective : public MIPObjective
{
 public:
  using SPtr = std::shared_ptr<ORToolsMIPObjective>;

 public:
  explicit ORToolsMIPObjective(operations_research::MPObjective* obj);

  ORToolsMIPObjective(
      ModelObjectMIPObjective::SPtr mipObjectiveObject,
      const std::unordered_map<std::string, MIPVariable::SPtr>& varMap,
      operations_research::MPSolver* solver);

  /// Returns the instance of the ORTools objective stored in this class
  inline const operations_research::MPObjective* getObjective() const
  {
    return pObjective;
  }

 private:
  /// Actual ORTools objective
  operations_research::MPObjective* pObjective;
};

}  // namespace optilab
