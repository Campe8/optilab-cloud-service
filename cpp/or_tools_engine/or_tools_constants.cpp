#include "or_tools_engine/or_tools_constants.hpp"

#include <limits>

namespace optilab {

namespace cpconstants {

const int64_t INT_VAR_LB_INF = std::numeric_limits<int64_t>::min();
const int64_t INT_VAR_UB_INF = std::numeric_limits<int64_t>::max();

}  // namespace cpconstants

}  // namespace optilab
