#include "or_tools_engine/or_tools_utilities.hpp"

#include <cassert>
#include <cstdint>    // for int64_t
#include <stdexcept>  // for std::runtime_error

#include <boost/any.hpp>
#include <sparsepp/spp.h>

#include "engine/engine_constants.hpp"
#include "engine/engine_utils.hpp"
#include "model/model_constants.hpp"
#include "model/model_object_constraint.hpp"
#include "model/model_object_intension_constraint.hpp"
#include "or_tools_engine/or_tools_constants.hpp"
#include "utilities/strings.hpp"
#include "utilities/variables.hpp"

namespace {

template<class From, class To>
To* castSmartPointer(const From& val)
{
  auto dynCast = std::dynamic_pointer_cast<To>(val);
  if (!dynCast)
  {
    throw std::runtime_error("Invalid dynamic cast to ORTools type");
  }

  return dynCast.get();
}  // castSmartPointer

optilab::CPConstraint::PropagationType getConstraintPropagationTypeFromString(
    const std::string& propType)
{
  if (propType == optilab::jsonmodel::CONSTRAINT_PROPAGATION_TYPE_DEFAULT)
  {
    return optilab::CPConstraint::PropagationType::PT_DEFAULT;
  }
  else if (propType == optilab::jsonmodel::CONSTRAINT_PROPAGATION_TYPE_BOUNDS)
  {
    return optilab::CPConstraint::PropagationType::PT_BOUNDS;
  }
  else if (propType == optilab::jsonmodel::CONSTRAINT_PROPAGATION_TYPE_DOMAIN)
  {
    return optilab::CPConstraint::PropagationType::PT_DOMAIN;
  }
  else
  {
    throw std::runtime_error("Constraint propagation type not recognized: " + propType);
  }
}  // getConstraintPropagationTypeFromString

optilab::CPArgument getConstraintArgumentFromArgList(const std::vector<std::string>& args,
                                                     optilab::CPModel* model)
{
  if (args.empty())
  {
    throw std::runtime_error("Empty list of arguments");
  }

  // Create the argument according to the type of the (first) element in the vector
  // with the exception that one single double type cast all the other elements to double
  bool isDouble = false;
  bool isInt = false;
  bool isBool = false;
  for (const auto& arg : args)
  {
    if (optilab::utilsstrings::isDouble(arg))
    {
      isDouble = true;
    }
    if (optilab::utilsstrings::isInt(arg))
    {
      isInt = true;
    }
    if (optilab::utilsstrings::isBool(arg))
    {
      isBool = true;
    }
  }

  // Create the argument
  optilab::CPArgument cpArg;

  // At least one string has the following syntax: <num>.<num>
  if (isDouble && !isInt)
  {
    std::vector<double> doubleArgs;
    for (const auto& arg : args)
    {
      doubleArgs.push_back(optilab::utilsstrings::stringToDouble(arg));
    }
    cpArg = optilab::CPArgument::buildDoubleValueArrayArg(doubleArgs);
  }
  else if ((isInt || isBool) && !isDouble)
  {
    // All arguments are int (or Boolean)
    std::vector<int64_t> intArgs;
    for (const auto& arg : args)
    {
      intArgs.push_back(optilab::utilsstrings::stringToInt(arg));
    }
    cpArg = optilab::CPArgument::buildIntValueArrayArg(intArgs);
  }
  else
  {
    // All arguments are variables
    std::vector<optilab::CPVariable::SPtr> varArgs;
    const auto& varMap = model->getVariablesMap();
    for (const auto& arg : args)
    {
      // Var id can be either:
      // - id, for example "x"
      // - subscript, for example "x[1, 2]"
      auto idAndIdx = optilab::utilsvariables::extractVariableIdAndIdxFromString(arg);
      auto it = varMap.find(idAndIdx.first);
      if (it == varMap.end())
      {
        throw std::runtime_error("Constraint argument (variable) not found in the model: " +
                                 idAndIdx.first);
      }

      auto var = it->second;
      if (idAndIdx.second.empty())
      {
        varArgs.push_back(var);
      }
      else
      {
        // Get the variable at the specified index
        if (var->isScalar())
        {
          throw std::runtime_error("Constraint argument: trying to use "
              "the subscript operator on a scalar variable");
        }
        varArgs.push_back(var->getSubVariable(idAndIdx.second));
      }
      cpArg = optilab::CPArgument::buildVarRefArrayArg(varArgs);
    }
  }

  // Return the argument
  return cpArg;
}  // getConstraintArgumentFromArgList

std::vector<optilab::CPArgument> getConstraintArgumentsFromIntensionConstraint(
    optilab::ModelObjectIntensionConstraint* intCon, optilab::CPModel* model)
{
  std::vector<optilab::CPArgument> argList;

  for (const auto& argStrList : intCon->getArgumentsList())
  {
    argList.push_back(getConstraintArgumentFromArgList(argStrList, model));
  }

  return argList;
}  // getConstraintArgumentsFromIntensionConstraint

template<typename Assign>
optilab::JSONValueArray::SPtr createSolutionDomain(const optilab::JSON::SPtr& json,
                                                   const Assign& assign)
{
  auto domainJson = json->createArray();
  for (const auto& bounds : assign)
  {
    auto boundsJson = json->createArray();

    // Storing only one element is bounds are the same should help
    // saving some memory
    if (bounds.first == bounds.second)
    {
      auto lb = json->createValue(bounds.first);
      boundsJson->pushBack(lb);
    }
    else
    {
      auto lb = json->createValue(bounds.first);
      auto ub = json->createValue(bounds.second);
      boundsJson->pushBack(lb);
      boundsJson->pushBack(ub);
    }

    domainJson->pushBack(boundsJson);
  }

  return domainJson;
}  // createSolutionDomain

optilab::JSONValueArray::SPtr createSolution(
    const optilab::JSON::SPtr& json,
    const optilab::ortoolsengine::ORToolsCPResult::Solution& solution)
{
  auto solutionJson = json->createArray();
  for (const auto& varMapIter : solution)
  {
    auto varAssign = json->createValue();

    // Set variable id
    auto varId = json->createValue(varMapIter.first);
    varAssign->add(optilab::jsonsolution::VAR_IDENTIFIER, varId);

    // Create domain bounds
    auto domainJson =
        createSolutionDomain<std::vector<std::pair<int64, int64>>>(
            json, varMapIter.second);
    varAssign->add(optilab::jsonsolution::VAR_DOMAIN_LIST, domainJson);

    // Push the current variable assignment in the solution list of variable assignments
    solutionJson->pushBack(varAssign);
  }

  return solutionJson;
}  // createSolution

optilab::JSONValueArray::SPtr createSolutionArray(
    const optilab::JSON::SPtr& json,
    const std::vector<optilab::ortoolsengine::ORToolsCPResult::Solution> & solutionList,
    int numSolutions)
{
  auto solutionListJson = json->createArray();
  for (int solIdx = 0; solIdx < numSolutions; ++solIdx)
  {
    solutionListJson->pushBack(createSolution(json, solutionList[solIdx]));
  }

  return solutionListJson;
}  // createSolutionArray

optilab::JSONValueArray::SPtr createMIPSolutionArray(
    const optilab::JSON::SPtr& json,
    const optilab::ortoolsengine::ORToolsMIPResult::Solution& solution)
{
  auto solutionListJson = json->createArray();
  for (const auto& varMapIter : solution)
  {
    auto varAssign = json->createValue();

    // Set variable id
    auto varId = json->createValue(varMapIter.first);
    varAssign->add(optilab::jsonsolution::VAR_IDENTIFIER, varId);

    // Create domain bounds
    auto domainJson =
        createSolutionDomain<optilab::ortoolsengine::ORToolsMIPResult::VarAssign>(
            json, varMapIter.second);
    varAssign->add(optilab::jsonsolution::VAR_DOMAIN_LIST, domainJson);

    // Push the current variable assignment in the solution list of variable assignments
    solutionListJson->pushBack(varAssign);
  }

  return solutionListJson;
}  // createMIPSolutionArray

optilab::JSONValueArray::SPtr createObjectiveValuesArray(
    const optilab::JSON::SPtr& json,
    const optilab::ortoolsengine::ORToolsMIPResult::ObjectiveValues& objectiveValues)
{
  auto objectiveValuesListJson = json->createArray();
  for (int objValIdx = 0; objValIdx < objectiveValues.size(); ++objValIdx)
  {
    auto val = json->createValue(objectiveValues[objValIdx]);
    objectiveValuesListJson->pushBack(val);
  }

  return objectiveValuesListJson;
}  // createObjectiveValuesArray

optilab::ORToolsCPConstraint::SPtr getIntensionCPCon(
    optilab::ModelObjectIntensionConstraint* intCon, operations_research::Solver* solver,
    optilab::CPModel* model)
{
  auto conTypeName = intCon->getType() + "_" + intCon->getName();
  auto propType = getConstraintPropagationTypeFromString(intCon->getPropagationType());
  auto argumentsList = getConstraintArgumentsFromIntensionConstraint(intCon, model);


  return std::make_shared<optilab::ORToolsCPConstraint>(conTypeName, argumentsList, propType,
                                                        solver);
}  // getIntensionCPCon

}  // namespace

namespace optilab {

namespace ortoolsutils {

optilab::ORToolsMIPVariable::SPtr getMIPVar(const optilab::ModelObjectVariable::SPtr& varPtr,
                                            operations_research::MPSolver* solver)
{
  const double lb = varPtr->getLowerBound();
  const double ub = varPtr->getUpperBound();
  const auto domType = varPtr->getDomainType();
  const std::string& varName = varPtr->getVariableName();
  bool isContinuos = domType == ModelObjectVariable::VarType::DOUBLE ||
      domType == ModelObjectVariable::VarType::FLOAT;

  // Force continuous variables if the optimization problem type is LP
  if (!solver->IsMIP())
  {
    isContinuos = true;
  }

  if (varPtr->isScalar())
  {
    if (domType == ModelObjectVariable::VarType::BOOL)
    {
      return std::make_shared<optilab::ORToolsMIPVariable>(varName, solver);
    }
    else
    {
      return std::make_shared<optilab::ORToolsMIPVariable>(varName, lb, ub, isContinuos, solver);
    }
  }
  else
  {
    const int numDim = static_cast<int>(varPtr->numDimensions());
    if (numDim == 1)
    {
      if (domType == ModelObjectVariable::VarType::BOOL)
      {
        return std::make_shared<optilab::ORToolsMIPVariable>(varName, varPtr->getDimension(0),
                                                             solver);
      }
      else
      {
        return std::make_shared<optilab::ORToolsMIPVariable>(varName, lb, ub, isContinuos,
                                                             varPtr->getDimension(0), solver);
      }
    }
    else
    {
      std::vector<int> dims;
      for (int dim = 0; dim < numDim; ++dim)
      {
        dims.push_back(varPtr->getDimension(dim));
      }

      if (domType == ModelObjectVariable::VarType::BOOL)
      {
        return std::make_shared<optilab::ORToolsMIPVariable>(varName, dims, solver);
      }
      else
      {
        return std::make_shared<optilab::ORToolsMIPVariable>(
            varName, lb, ub, isContinuos, dims, solver);
      }
    }
  }
}  // getMIPVar

optilab::ORToolsCPVariable::SPtr getCPVar(const optilab::ModelObjectVariable::SPtr& varPtr,
                                          operations_research::Solver* solver)
{
  // Get the name of the variable
  const std::string& varName = varPtr->getVariableName();

  // Get the domain from the model object variable
  auto domain = utilsvariables::buildCPDomainFromObjectVariable(varPtr);

  // Create the variable
  if (varPtr->isScalar())
  {
    return std::make_shared<ORToolsCPVariable>(varName, domain, solver);
  }
  else
  {
    const int numDim = static_cast<int>(varPtr->numDimensions());
    if (numDim == 1)
    {
      return std::make_shared<ORToolsCPVariable>(varName, domain, varPtr->getDimension(0), solver);
    }
    else
    {
      std::vector<int> dims;
      for (int dim = 0; dim < numDim; ++dim)
      {
        dims.push_back(varPtr->getDimension(dim));
      }
      return std::make_shared<ORToolsCPVariable>(varName, domain, dims, solver);
    }
  }
}  // getCPVar

optilab::ORToolsCPConstraint::SPtr getCPCon(const optilab::ModelObjectConstraint::SPtr& conPtr,
                                            operations_research::Solver* solver, CPModel* model)
{
  auto conSemantic = conPtr->getSemantic();
  if (conSemantic == optilab::ModelObjectConstraint::ConstraintSemantic::CS_INTENSION)
  {
    auto intConst = static_cast<optilab::ModelObjectIntensionConstraint*>(conPtr.get());
    return getIntensionCPCon(intConst, solver, model);
  }
  throw std::runtime_error("OR-Tools constraint semantic not supported");
}  // getCPCon

operations_research::Solver::IntVarStrategy getVarSelectionStrategyFromString(
    const std::string& varStrategy)
{

  if (varStrategy == ortoolsconstants::CHOOSE_FIRST_UNBOUND)
  {
    return operations_research::Solver::IntVarStrategy::CHOOSE_FIRST_UNBOUND;
  }
  else if (varStrategy == ortoolsconstants::CHOOSE_HIGHEST_MAX)
  {
    return operations_research::Solver::IntVarStrategy::CHOOSE_HIGHEST_MAX;
  }
  else if (varStrategy == ortoolsconstants::CHOOSE_LOWEST_MIN)
  {
    return operations_research::Solver::IntVarStrategy::CHOOSE_LOWEST_MIN;
  }
  else if (varStrategy == ortoolsconstants::CHOOSE_MIN_SIZE_HIGHEST_MAX)
  {
    return operations_research::Solver::IntVarStrategy::CHOOSE_MIN_SIZE_HIGHEST_MAX;
  }
  else if (varStrategy == ortoolsconstants::CHOOSE_MIN_SIZE_HIGHEST_MIN)
  {
    return operations_research::Solver::IntVarStrategy::CHOOSE_MIN_SIZE_HIGHEST_MIN;
  }
  else if (varStrategy == ortoolsconstants::CHOOSE_MIN_SIZE_LOWEST_MAX)
  {
    return operations_research::Solver::IntVarStrategy::CHOOSE_MIN_SIZE_LOWEST_MAX;
  }
  else if (varStrategy == ortoolsconstants::CHOOSE_MIN_SIZE_LOWEST_MIN)
  {
    return operations_research::Solver::IntVarStrategy::CHOOSE_MIN_SIZE_LOWEST_MIN;
  }
  else if (varStrategy == ortoolsconstants::CHOOSE_PATH)
  {
    return operations_research::Solver::IntVarStrategy::CHOOSE_PATH;
  }
  else if (varStrategy == ortoolsconstants::CHOOSE_RANDOM)
  {
    return operations_research::Solver::IntVarStrategy::CHOOSE_RANDOM;
  }
  else if (varStrategy == ortoolsconstants::CHOOSE_MAX_REGRET_ON_MIN)
  {
    return operations_research::Solver::IntVarStrategy::CHOOSE_MAX_REGRET_ON_MIN;
  }
  else if (varStrategy == ortoolsconstants::CHOOSE_MAX_SIZE)
  {
    return operations_research::Solver::IntVarStrategy::CHOOSE_MAX_SIZE;
  }
  else if (varStrategy == ortoolsconstants::CHOOSE_MIN_SIZE)
  {
    return operations_research::Solver::IntVarStrategy::CHOOSE_MIN_SIZE;
  }
  else if (varStrategy == ortoolsconstants::INT_VAR_DEFAULT)
  {
    return operations_research::Solver::IntVarStrategy::INT_VAR_DEFAULT;
  }
  else if (varStrategy == ortoolsconstants::INT_VAR_SIMPLE)
  {
    return operations_research::Solver::IntVarStrategy::INT_VAR_SIMPLE;
  }
  else
  {
    throw std::runtime_error("OR-Tools: variable selection strategy not recognized " + varStrategy);
  }
}  // getVarSelectionStrategyFromString

operations_research::Solver::IntValueStrategy getValSelectionStrategyFromString(
    const std::string& valStrategy)
{
  if (valStrategy == ortoolsconstants::ASSIGN_CENTER_VALUE)
  {
    return operations_research::Solver::IntValueStrategy::ASSIGN_CENTER_VALUE;
  }
  else if (valStrategy == ortoolsconstants::ASSIGN_MAX_VALUE)
  {
    return operations_research::Solver::IntValueStrategy::ASSIGN_MAX_VALUE;
  }
  else if (valStrategy == ortoolsconstants::ASSIGN_MIN_VALUE)
  {
    return operations_research::Solver::IntValueStrategy::ASSIGN_MIN_VALUE;
  }
  else if (valStrategy == ortoolsconstants::ASSIGN_RANDOM_VALUE)
  {
    return operations_research::Solver::IntValueStrategy::ASSIGN_RANDOM_VALUE;
  }
  else if (valStrategy == ortoolsconstants::INT_VALUE_DEFAULT)
  {
    return operations_research::Solver::IntValueStrategy::INT_VALUE_DEFAULT;
  }
  else if (valStrategy == ortoolsconstants::INT_VALUE_SIMPLE)
  {
    return operations_research::Solver::IntValueStrategy::INT_VALUE_SIMPLE;
  }
  else if (valStrategy == ortoolsconstants::SPLIT_LOWER_HALF)
  {
    return operations_research::Solver::IntValueStrategy::SPLIT_LOWER_HALF;
  }
  else if (valStrategy == ortoolsconstants::SPLIT_UPPER_HALF)
  {
    return operations_research::Solver::IntValueStrategy::SPLIT_UPPER_HALF;
  }
  else
  {
    throw std::runtime_error("OR-Tools: value selection strategy not recognized " + valStrategy);
  }
}  // getValSelectionStrategy

JSON::SPtr buildCPSolutionJson(ortoolsengine::ORToolsCPResult::SPtr res, int numSolutions)
{
  if (!res)
  {
    throw std::runtime_error("buildCPSolutionJson - empty result");
  }

  JSON::SPtr json = std::make_shared<JSON>();

  // Check for the number of solutions to send back to the caller
  const auto& modelName = res->modelName;
  const auto numResSol = res->getNumSolutions();
  if (numResSol == 0)
  {
    // Handle no solutions found
    if (res->isSatModel)
    {
      // No solutions but satisfiable model (e.g., a model without branching variables)
      json = engineutils::createCPSatSolutionJson(modelName);
    }
    else if (!res->isSatModel)
    {
      // No solutions and an unsatisfiable model
      json = engineutils::createCPUnsatSolutionJson(modelName);
    }
    else
    {
      // The engine did not run
      throw std::runtime_error("The engine did not run");
    }
    return json;
  }

  if (numResSol < numSolutions)
  {
    numSolutions = static_cast<int>(numResSol);
  }

  auto modelId = json->createValue(modelName);
  auto numSol = json->createValue(numSolutions);
  auto status = json->createValue(std::string(jsonsolution::STATUS_SATISFIED));
  json->add(jsonsolution::MODEL_ID, modelId);
  json->add(jsonsolution::NUM_SOLUTIONS, numSol);
  json->add(jsonsolution::MODEL_STATUS, status);

  // Set the list of solutions
  const auto& solutionArray = createSolutionArray(json, res->solutionList, numSolutions);
  json->add(jsonsolution::SOLUTION_LIST, solutionArray);

  return json;
}  // buildCPSolutionJson

JSON::SPtr buildMIPSolutionJson(ortoolsengine::ORToolsMIPResult::SPtr res)
{
  if (!res)
  {
    throw std::runtime_error("buildMIPSolutionJson - empty result");
  }

  if ((res->solution).first.empty() && (res->solution).second.empty())
  {
    throw std::runtime_error("buildMIPSolutionJson - empty solution");
  }

  JSON::SPtr json = std::make_shared<JSON>();

  // Add the model name
  auto modelId = json->createValue(res->modelName);
  json->add(jsonsolution::MODEL_ID, modelId);

  // Set the result status
  std::string resultStatusStr;
  switch (res->resultStatus)
  {
    case ortoolsengine::ORToolsMIPResult::ResultStatus::FEASIBLE:
      resultStatusStr = jsonmipsolution::RESULT_FEASIBLE;
      break;
    case ortoolsengine::ORToolsMIPResult::ResultStatus::INFEASIBLE:
      resultStatusStr = jsonmipsolution::RESULT_INFEASIBLE;
      break;
    case ortoolsengine::ORToolsMIPResult::ResultStatus::NOT_SOLVED:
      resultStatusStr = jsonmipsolution::RESULT_NOT_SOLVED;
      break;
    case ortoolsengine::ORToolsMIPResult::ResultStatus::OPTIMUM:
      resultStatusStr = jsonmipsolution::RESULT_OPTIMUM;
      break;
    default:
      assert(res->resultStatus == ortoolsengine::ORToolsMIPResult::ResultStatus::UNBOUNDED);
      resultStatusStr = jsonmipsolution::RESULT_UNBOUNDED;
      break;
  }

  auto resultStatusJson = json->createValue(resultStatusStr);
  json->add(jsonsolution::MODEL_STATUS, resultStatusJson);

  // Add objective values list
  const auto& objetiveValuesArray = createObjectiveValuesArray(json, (res->solution).first);
  json->add(jsonmipsolution::OBJECTIVE_VALUES_LIST, objetiveValuesArray);

  // Add solution
  const auto& solutionArray = createMIPSolutionArray(json, (res->solution).second);
  json->add(jsonsolution::SOLUTION_LIST, solutionArray);

  return json;
}  // buildMIPSolutionJson

LinearModelSolutionProto buildMIPSolutionProto(ortoolsengine::ORToolsMIPResult::SPtr res)
{
  if (!res)
  {
    throw std::runtime_error("buildMIPSolutionProto - empty result");
  }

  LinearModelSolutionProto sol;
  if ((res->solution).first.empty() && (res->solution).second.empty())
    {
      sol.set_status(LinearModelSolutionStatusProto::SOLVER_MODEL_INVALID);
      sol.set_status_str("SCIP: empty solution. Probably invalid model, contact Parallel support.");
      return sol;
    }

    // Set status
    switch (res->resultStatus)
    {
      case ortoolsengine::ORToolsMIPResult::ResultStatus::FEASIBLE:
        sol.set_status(LinearModelSolutionStatusProto::SOLVER_FEASIBLE);
        break;
      case ortoolsengine::ORToolsMIPResult::ResultStatus::INFEASIBLE:
        sol.set_status(LinearModelSolutionStatusProto::SOLVER_INFEASIBLE);
        break;
      case ortoolsengine::ORToolsMIPResult::ResultStatus::NOT_SOLVED:
        sol.set_status(LinearModelSolutionStatusProto::SOLVER_NOT_SOLVED);
        break;
      case ortoolsengine::ORToolsMIPResult::ResultStatus::OPTIMUM:
        sol.set_status(LinearModelSolutionStatusProto::SOLVER_OPTIMAL);
        break;
      default:
        assert(res->resultStatus == ortoolsengine::ORToolsMIPResult::ResultStatus::UNBOUNDED);
        sol.set_status(LinearModelSolutionStatusProto::SOLVER_UNBOUNDED);
        break;
    }

    // Objective value
    sol.set_objective_value((res->solution).first.at(0));

    // Variable assignment
    for (const auto& varSolution : (res->solution).second)
    {
      auto vassign = sol.add_variable_assign();
      vassign->set_var_name(varSolution.first);
      for (const auto& val : varSolution.second)
      {
        // Using only the first value since for MIP problems
        // the variables are all assigned
        vassign->add_var_value(val.first);
      }
    }

  return sol;
}  // buildMIPSolutionProto

JSON::SPtr buildVRPSolutionJson(ortoolsengine::ORToolsVRPResult::SPtr res)
{
  if (!res)
  {
    throw std::runtime_error("buildVRPSolutionJson - empty result");
  }

  JSON::SPtr json = std::make_shared<JSON>();

  // Add the model name
  auto modelId = json->createValue(res->modelName);
  json->add(jsonsolution::MODEL_ID, modelId);

  // Set the result status
  std::string resultStatusStr;
  switch (res->status)
  {
    case operations_research::RoutingModel::Status::ROUTING_SUCCESS:
      resultStatusStr = jsonsolution::STATUS_SATISFIED;
      break;
    case operations_research::RoutingModel::Status::ROUTING_FAIL:
      resultStatusStr = jsonsolution::STATUS_UNSATISFIED;
      break;
    case operations_research::RoutingModel::Status::ROUTING_NOT_SOLVED:
      resultStatusStr = jsonsolution::STATUS_SATISFIED;
      break;
    case operations_research::RoutingModel::Status::ROUTING_FAIL_TIMEOUT:
      resultStatusStr = jsonsolution::STATUS_TIMEOUT;
            break;
    case operations_research::RoutingModel::Status::ROUTING_INVALID:
      resultStatusStr = jsonsolution::STATUS_INVALID;
      break;
    default:
      resultStatusStr = jsonsolution::STATUS_UNSATISFIED;
      break;
  }
  auto resultStatusJson = json->createValue(resultStatusStr);
  json->add(jsonsolution::MODEL_STATUS, resultStatusJson);

  auto totDist = json->createValue(res->totDistance);
  json->add(jsonvrpsolution::TOT_DISTANCE_ALL_ROUTES, totDist);

  auto totLoad = json->createValue(res->totLoad);
  json->add(jsonvrpsolution::TOT_LOAD_ALL_ROUTES, totLoad);

  auto routeList = json->createArray();
  for (const auto& route : res->routeList)
  {
    auto jsonRoute = json->createValue();

    // Set vehicle identifier
    auto vehicleId = json->createValue(route.vehicleId);
    jsonRoute->add(jsonvrpsolution::VEHICLE_ID, vehicleId);

    // Set total distance and load
    auto totRouteDist = json->createValue(route.totDistance);
    auto totRouteLoad = json->createValue(route.totLoad);
    jsonRoute->add(jsonvrpsolution::TOT_ROUTE_DISTANCE, totRouteDist);
    jsonRoute->add(jsonvrpsolution::TOT_ROUTE_LOAD, totRouteLoad);

    // Set route and load
    auto routeStops = json->createArray();
    for (auto stop : route.route)
    {
      routeStops->pushBack(json->createValue(stop));
    }
    jsonRoute->add(jsonvrpsolution::ROUTE, routeStops);

    auto routeLoads = json->createArray();
    for (auto stopLoad : route.load)
    {
      routeLoads->pushBack(json->createValue(stopLoad));
    }
    jsonRoute->add(jsonvrpsolution::LOAD, routeLoads);
    routeList->pushBack(jsonRoute);
  }
  json->add(jsonmipsolution::OBJECTIVE_VALUES_LIST, routeList);

  return json;
}  // buildVRPSolutionJson

}  // namespace ortoolsutils

}  // namespace optilab
