#include "or_tools_engine/job_shop_solver.hpp"

#include <algorithm>  // for std::max
#include <cassert>
#include <stdexcept>  // for std::invalid_argument

#include <spdlog/spdlog.h>

#include "optilab_protobuf/job_shop_model.pb.h"

namespace {
operations_research::sat::DecisionStrategyProto::VariableSelectionStrategy
getVariableSelectionStrategy(::optilab::toolbox::SearchStrategy_VariableSelection varSelection)
{
  switch(varSelection)
  {
    case ::optilab::toolbox::SearchStrategy_VariableSelection::
    SearchStrategy_VariableSelection_CHOOSE_HIGHEST_MAX:
      return operations_research::sat::DecisionStrategyProto::VariableSelectionStrategy::
              DecisionStrategyProto_VariableSelectionStrategy_CHOOSE_HIGHEST_MAX;
    case ::optilab::toolbox::SearchStrategy_VariableSelection::
    SearchStrategy_VariableSelection_CHOOSE_LOWEST_MIN:
      return operations_research::sat::DecisionStrategyProto::VariableSelectionStrategy::
              DecisionStrategyProto_VariableSelectionStrategy_CHOOSE_LOWEST_MIN;
    case ::optilab::toolbox::SearchStrategy_VariableSelection::
    SearchStrategy_VariableSelection_CHOOSE_MAX_DOMAIN_SIZE:
      return operations_research::sat::DecisionStrategyProto::VariableSelectionStrategy::
              DecisionStrategyProto_VariableSelectionStrategy_CHOOSE_MAX_DOMAIN_SIZE;
    case ::optilab::toolbox::SearchStrategy_VariableSelection::
    SearchStrategy_VariableSelection_CHOOSE_MIN_DOMAIN_SIZE:
      return  operations_research::sat::DecisionStrategyProto::VariableSelectionStrategy::
              DecisionStrategyProto_VariableSelectionStrategy_CHOOSE_MIN_DOMAIN_SIZE;
    default:
      return operations_research::sat::DecisionStrategyProto::VariableSelectionStrategy::
              DecisionStrategyProto_VariableSelectionStrategy_CHOOSE_LOWEST_MIN;
  }

}

operations_research::sat::DecisionStrategyProto::DomainReductionStrategy getValueSelectionStrategy(
        ::optilab::toolbox::SearchStrategy_ValueSelection valSelection)
{
  switch(valSelection)
  {
    case ::optilab::toolbox::SearchStrategy_ValueSelection::
    SearchStrategy_ValueSelection_SELECT_LOWER_HALF:
      return operations_research::sat::DecisionStrategyProto::DomainReductionStrategy::
              DecisionStrategyProto_DomainReductionStrategy_SELECT_LOWER_HALF;
    case ::optilab::toolbox::SearchStrategy_ValueSelection::
    SearchStrategy_ValueSelection_SELECT_MAX_VALUE:
      return operations_research::sat::DecisionStrategyProto::DomainReductionStrategy::
              DecisionStrategyProto_DomainReductionStrategy_SELECT_MAX_VALUE;
    case ::optilab::toolbox::SearchStrategy_ValueSelection::
    SearchStrategy_ValueSelection_SELECT_MEDIAN_VALUE:
      return operations_research::sat::DecisionStrategyProto::DomainReductionStrategy::
              DecisionStrategyProto_DomainReductionStrategy_SELECT_MEDIAN_VALUE;
    case ::optilab::toolbox::SearchStrategy_ValueSelection::
    SearchStrategy_ValueSelection_SELECT_MIN_VALUE:
      return operations_research::sat::DecisionStrategyProto::DomainReductionStrategy::
              DecisionStrategyProto_DomainReductionStrategy_SELECT_MIN_VALUE;
    case ::optilab::toolbox::SearchStrategy_ValueSelection::
    SearchStrategy_ValueSelection_SELECT_UPPER_HALF:
      return operations_research::sat::DecisionStrategyProto::DomainReductionStrategy::
              DecisionStrategyProto_DomainReductionStrategy_SELECT_UPPER_HALF;
    default:
      return operations_research::sat::DecisionStrategyProto::DomainReductionStrategy::
              DecisionStrategyProto_DomainReductionStrategy_SELECT_MIN_VALUE;
  }
}
}  // namespace

namespace optilab {

JobShopSolver::JobShopSolver(const toolbox::JobShopModelProto& model)
: pSATModelBuilder(nullptr)
{
  // Set default variable selection strategy
  pVariableStrategy = operations_research::sat::DecisionStrategyProto::CHOOSE_LOWEST_MIN;

  // Set default value selection strategy
  pValueStrategy = operations_research::sat::DecisionStrategyProto::SELECT_MIN_VALUE;

  // Initialize the solver
  initSolver(model);
}

void JobShopSolver::initSolver(const toolbox::JobShopModelProto& model)
{
  // Instantiate a the scheduling builder
  pSATModelBuilder = std::make_shared<ModelBuilder>();

  if (model.has_search_strategy())
  {
    const auto& strat = model.search_strategy();
    pVariableStrategy = getVariableSelectionStrategy(strat.variable_selection_type());
    pValueStrategy = getValueSelectionStrategy(strat.value_selection_type());
  }

  // Initialize the model
  initJobShopModel(model.job_shop_model());
}  // initSolver

void JobShopSolver::initJobShopModel(const toolbox::JsspInputProblem& problem)
{
  const int numJobs = problem.jobs_size();
  const int numMachines = problem.machines_size();
  const int64_t horizon = computeHorizon(problem);

  std::vector<int> starts;
  std::vector<int> ends;
  const operations_research::Domain allHorizon(0, horizon);

  //== Variables ==//
  // Makespan
  const operations_research::sat::IntVar makespan = pSATModelBuilder->NewIntVar(allHorizon);

  std::vector<std::vector<operations_research::sat::IntervalVar>> machineToIntervals(numMachines);
  std::vector<std::vector<int>> machineToJobs(numMachines);

  // All task's start variables for each machine
  std::vector<std::vector<operations_research::sat::IntVar>> machineToStarts(numMachines);

  // All task's end variables for each machine
  std::vector<std::vector<operations_research::sat::IntVar>> machineToEnds(numMachines);
  std::vector<std::vector<operations_research::sat::BoolVar>> machineToPresences(numMachines);

  int64_t objectiveOffset{0};
  std::vector<operations_research::sat::IntVar> objectiveVars;
  std::vector<int64_t> objectiveCoeffs;

  pJobStarts.resize(numJobs);
  pJobEnds.resize(numJobs);
  pJobToTaskStarts.resize(numJobs);
  for (int j{0}; j < numJobs; ++j)
  {
    const auto& job = problem.jobs(j);

    operations_research::sat::IntVar previousEnd;
    const int64_t hardStart = job.earliest_start();
    const int64_t hardEnd = horizon;

    // Create the variables for each task of the current job
    for (int t{0}; t < job.tasks_size(); ++t)
    {
      const toolbox::Task& task = job.tasks(t);
      const int numAlternatives = task.machine_size();
      assert(numAlternatives == task.duration_size());

      // Add the "main" task interval. It will encapsulate all the alternative ones
      // if there is many, or be a normal task otherwise.
      int64_t minDuration = task.duration(0);
      int64_t maxDuration = task.duration(0);
      for (int i = 1; i < numAlternatives; ++i)
      {
        minDuration = std::min(minDuration, task.duration(i));
        maxDuration = std::max(maxDuration, task.duration(i));
      }
      const auto start = pSATModelBuilder->NewIntVar(
              operations_research::Domain(hardStart, hardEnd));
      const auto duration =
              pSATModelBuilder->NewIntVar(operations_research::Domain(minDuration, maxDuration));
      const auto end =
              pSATModelBuilder->NewIntVar(operations_research::Domain(hardStart, hardEnd));

      // Create the interval variable in [start, end] of given duration
      const auto interval = pSATModelBuilder->NewIntervalVar(start, duration, end);

      // Store starts and ends of jobs for precedences.
      if (t == 0)
      {
        pJobStarts[j] = start;
      }

      if (t == job.tasks_size() - 1)
      {
        pJobEnds[j] = end;
      }

      // Store the start variable for this task
      pJobToTaskStarts[j].push_back(pTaskStarts.size());
      pTaskStarts.push_back(start);

      // Chain the task belonging to the same job.
      if (t > 0)
      {
        pSATModelBuilder->AddLessOrEqual(previousEnd, start);
      }
      previousEnd = end;

      if (numAlternatives == 1)
      {
        // Usual case -> one task can go only on one machine
        const int m = task.machine(0);
        // Store this task's interval into the machine's intervals
        machineToIntervals[m].push_back(interval);
        // Store this job's id into the machine's jobs
        machineToJobs[m].push_back(j);
        machineToStarts[m].push_back(start);
        machineToEnds[m].push_back(end);
        machineToPresences[m].push_back(pSATModelBuilder->TrueVar());
        if (task.cost_size() > 0)
        {
          objectiveOffset += task.cost(0);
        }
      }
      else
      {
        // Case where a task can go on being executed in more than one machine
        std::vector<operations_research::sat::BoolVar> presences;
        for (int a = 0; a < numAlternatives; ++a)
        {
          // Create a new variable for each alternative machine this task can go on
          const auto presence = pSATModelBuilder->NewBoolVar();
          const auto localStart =
                  pSATModelBuilder->NewIntVar(operations_research::Domain(hardStart, hardEnd));
          const auto localDuration = pSATModelBuilder->NewConstant(task.duration(a));
          const auto localEnd =
                  pSATModelBuilder->NewIntVar(operations_research::Domain(hardStart, hardEnd));
          const auto localInterval =pSATModelBuilder->NewOptionalIntervalVar(
                  localStart, localDuration, localEnd, presence);

          // Link local and global variables using the Boolean variable presence
          pSATModelBuilder->AddEquality(start, localStart).OnlyEnforceIf(presence);
          pSATModelBuilder->AddEquality(end, localEnd).OnlyEnforceIf(presence);

          // TODO Experiment with the following implication
          pSATModelBuilder->AddEquality(duration, localDuration).OnlyEnforceIf(presence);

          // Record relevant variables for later use
          const int m = task.machine(a);
          machineToIntervals[m].push_back(localInterval);
          machineToJobs[m].push_back(j);
          machineToStarts[m].push_back(localStart);
          machineToEnds[m].push_back(localEnd);
          machineToPresences[m].push_back(presence);

          // Add cost if present
          if (task.cost_size() > 0)
          {
            objectiveVars.push_back(presence);
            objectiveCoeffs.push_back(task.cost(a));
          }

          // Collect presence variables
          presences.push_back(presence);
        }

        // Exactly one alternative interval is present
        pSATModelBuilder->AddEquality(
                operations_research::sat::LinearExpr::BooleanSum(presences), 1);
      }  // else
    }  // for all tasks "t" on current job

    // The makespan will be greater than the end of each job.
    if (problem.makespan_cost_per_time_unit() != 0L)
    {
      pSATModelBuilder->AddLessOrEqual(previousEnd, makespan);
    }

    const int64_t latenessPenalty = job.lateness_cost_per_time_unit();

    // Lateness cost
    if (latenessPenalty != 0L)
    {
      const int64_t dueDate = job.late_due_date();
      if (dueDate == 0)
      {
        objectiveVars.push_back(previousEnd);
        objectiveCoeffs.push_back(latenessPenalty);
      }
      else
      {
        const auto shiftedVar =
                pSATModelBuilder->NewIntVar(
                        operations_research::Domain(-dueDate, horizon - dueDate));
        pSATModelBuilder->AddEquality(
                shiftedVar,
                operations_research::sat::LinearExpr(previousEnd).AddConstant(-dueDate));
        const auto latenessVar = pSATModelBuilder->NewIntVar(allHorizon);
        pSATModelBuilder->AddMaxEquality(latenessVar,
                                         {pSATModelBuilder->NewConstant(0), shiftedVar});
        objectiveVars.push_back(latenessVar);
        objectiveCoeffs.push_back(latenessPenalty);
      }
    }  // latenessPenalty

    const int64 earlinessPenalty = job.earliness_cost_per_time_unit();

    // Earliness cost
    if (earlinessPenalty != 0L)
    {
      const int64 dueDate = job.early_due_date();
      if (dueDate > 0)
      {
        const auto shiftedVar =
                pSATModelBuilder->NewIntVar(
                        operations_research::Domain(dueDate - horizon, dueDate));
        pSATModelBuilder->AddEquality(operations_research::sat::LinearExpr::Sum({shiftedVar, previousEnd}),
                                      dueDate);
        const auto earlinessVar = pSATModelBuilder->NewIntVar(allHorizon);
        pSATModelBuilder->AddMaxEquality(earlinessVar,
                                         {pSATModelBuilder->NewConstant(0), shiftedVar});
        objectiveVars.push_back(earlinessVar);
        objectiveCoeffs.push_back(earlinessPenalty);
      }
    }  // earliness_penalty
  }  // for each job "j"

  //== Constraint ==//

  // Add one no_overlap constraint per machine
  for (int m = 0; m < numMachines; ++m)
  {
    // Add availability time
    if (problem.machines(m).has_availability_matrix())
    {
      const toolbox::AvailabilityMatrix& availability = problem.machines(m).availability_matrix();
      const int numIntervals = availability.availability_time().size() / 2;

      // The availability of the machine is set as the time the machine can "run".
      // In other words, set the intervals in which the machine cannot be used
      int64_t startInterval{0};
      for (int i = 0; i < numIntervals; ++i)
      {
        const int64_t endAvail = availability.availability_time(2*i);
        const auto nonAvailInterval =pSATModelBuilder->NewIntervalVar(
                pSATModelBuilder->NewConstant(startInterval),
                pSATModelBuilder->NewConstant(endAvail - startInterval),
                pSATModelBuilder->NewConstant(endAvail));
        startInterval = availability.availability_time(2*i + 1);

        // Add the non overlapping interval
        machineToIntervals[m].push_back(nonAvailInterval);
      }
    }

    // Add the non-overlapping constraints
    pSATModelBuilder->AddNoOverlap(machineToIntervals[m]);

    // Add transition times
    if (problem.machines(m).has_transition_time_matrix())
    {
      const toolbox::TransitionTimeMatrix& transitions =
              problem.machines(m).transition_time_matrix();
      const int numIntervals = machineToIntervals[m].size();

      // Create circuit constraint on a machine.
      // Node 0 and num_intervals + 1 are source and sink.
      auto circuit = pSATModelBuilder->AddCircuitConstraint();
      for (int i = 0; i < numIntervals; ++i)
      {
        const int job_i = machineToJobs[m][i];

        // Source to nodes
        circuit.AddArc(0, i + 1, pSATModelBuilder->NewBoolVar());

        // Node to sink
        circuit.AddArc(i + 1, 0, pSATModelBuilder->NewBoolVar());

        // Node to node
        for (int j = 0; j < numIntervals; ++j)
        {
          if (i == j)
          {
            circuit.AddArc(i + 1, i + 1, Not(machineToPresences[m][i]));
          }
          else
          {
            const int job_j = machineToJobs[m][j];
            const int64_t transition = transitions.transition_time(job_i * numJobs + job_j);
            const operations_research::sat::BoolVar lit = pSATModelBuilder->NewBoolVar();
            const operations_research::sat::IntVar start = machineToStarts[m][j];
            const operations_research::sat::IntVar end = machineToEnds[m][i];
            circuit.AddArc(i + 1, j + 1, lit);

            // Push the new start with an extra transition
            pSATModelBuilder->AddLessOrEqual(
                    operations_research::sat::LinearExpr(end)
            .AddConstant(transition), start).OnlyEnforceIf(lit);
          }
        }
      }
    }
  }  // no overlap constraint on machines

  // Add job precedences
  for (const auto& precedence : problem.precedences())
  {
    const operations_research::sat::IntVar start = pJobStarts[precedence.second_job_index()];
    const operations_research::sat::IntVar end = pJobEnds[precedence.first_job_index()];
    pSATModelBuilder->AddLessOrEqual(
            operations_research::sat::LinearExpr(end).AddConstant(precedence.min_delay()), start);
  }

  //== Objective ==//
  if (problem.makespan_cost_per_time_unit() != 0L)
  {
    objectiveCoeffs.push_back(problem.makespan_cost_per_time_unit());
    objectiveVars.push_back(makespan);
  }

  // Add objective only if this is an optimization problem
  if (problem.is_optimization())
  {
    pIsOptimizationModel = true;
    pSATModelBuilder->Minimize(
            operations_research::sat::LinearExpr::ScalProd(
                    objectiveVars, objectiveCoeffs).AddConstant(objectiveOffset));

    if (problem.scaling_factor() != 1.0)
    {
      pSATModelBuilder->ScaleObjectiveBy(problem.scaling_factor());
    }
  }

  //== Decision strategy ==//
  pSATModelBuilder->AddDecisionStrategy(
          pTaskStarts,
          pVariableStrategy,
          pValueStrategy);
}

int64 JobShopSolver::computeHorizon(const toolbox::JsspInputProblem& problem)
{
  int64_t sumOfDurations{0};
  int64_t maxLatestEnd{0};
  int64_t maxEarliestStart{0};
  for (const toolbox::Job &job : problem.jobs())
  {
    maxLatestEnd = std::max(maxLatestEnd, job.latest_end());
    maxEarliestStart = std::max(maxEarliestStart, job.earliest_start());

    // Compute the sum of all durations
    for (const toolbox::Task &task : job.tasks())
    {
      int64 maxDuration{0};
      for (int64 d : task.duration())
      {
        maxDuration = std::max(maxDuration, d);
      }
      sumOfDurations += maxDuration;
    }
  }

  // For each machine, take into account the transition matrix time
  const int numJobs = problem.jobs_size();
  int64 sumOfTransitions{0};
  for (const toolbox::Machine& machine : problem.machines())
  {
    if (!machine.has_transition_time_matrix())
    {
      continue;
    }
    const toolbox::TransitionTimeMatrix& matrix = machine.transition_time_matrix();
    for (int i{0}; i < numJobs; ++i)
    {
      int64 maxTransition{0};
      for (int j{0}; j < numJobs; ++j)
      {
        maxTransition = std::max(maxTransition, matrix.transition_time(i * numJobs + j));
      }
      sumOfTransitions += maxTransition;
    }
  }
  return std::min(maxLatestEnd, sumOfDurations + sumOfTransitions + maxEarliestStart);
}

bool JobShopSolver::interruptSearchProcess()
{
  return false;
}  // interruptSearchProcess

void JobShopSolver::solve()
{
  pSolution = operations_research::sat::Solve(pSATModelBuilder->Build());
}  // solve

void JobShopSolver::storeSolution(toolbox::schedulingengine::JobShopResult& sol)
{
  // Set status of the solution
  switch (pSolution.status())
  {
    case ::operations_research::sat::CpSolverStatus::FEASIBLE:
      sol.status = toolbox::schedulingengine::SchedulingResult::ResultStatus::SUCCESS;
      break;
    case ::operations_research::sat::CpSolverStatus::INFEASIBLE:
      sol.status = toolbox::schedulingengine::SchedulingResult::ResultStatus::FAIL;
      break;
    case ::operations_research::sat::CpSolverStatus::MODEL_INVALID:
      sol.status = toolbox::schedulingengine::SchedulingResult::ResultStatus::INVALID;
      break;
    case ::operations_research::sat::CpSolverStatus::OPTIMAL:
      sol.status = toolbox::schedulingengine::SchedulingResult::ResultStatus::SUCCESS;
      break;
    case ::operations_research::sat::CpSolverStatus::UNKNOWN:
    default:
      sol.status = toolbox::schedulingengine::SchedulingResult::ResultStatus::NOT_SOLVED;
      break;
  }

  if (sol.status == toolbox::schedulingengine::SchedulingResult::ResultStatus::SUCCESS)
  {
    auto& jssolution = sol.jsSolution;

    if(pIsOptimizationModel)
    {
      jssolution.set_makespan_cost(static_cast<int64_t>(pSolution.objective_value()));
    }

    // Set job start times
    for (const auto& job : pJobToTaskStarts)
    {
      auto assignedJob = jssolution.add_jobs();
      for (const auto& taskStart : job)
      {
        auto task = assignedJob->add_tasks();
        task->set_start_time(operations_research::sat::SolutionIntegerValue(
                pSolution, pTaskStarts[taskStart]));
      }
    }
  }
}  // storeSolution

}  // namespace optilab
