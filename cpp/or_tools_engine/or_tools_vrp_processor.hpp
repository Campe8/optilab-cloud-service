//
// Copyright OptiLab 2019. All rights reserved.
//
// Processor for OR-Tools VRP engines.
//

#pragma once

#include <memory>   // for std::shared_ptr
#include <string>
#include <map>
#include <utility>  // for std::pair
#include <vector>

#include <boost/thread.hpp>

#include "model/model.hpp"
#include "or_tools_engine/or_tools_processor.hpp"
#include "or_tools_engine/or_tools_vrp_model.hpp"
#include "or_tools_engine/or_tools_vrp_solver.hpp"
#include "or_tools_engine/or_tools_utilities.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS OrToolsVRPProcessor : public ORToolsProcessor {
public:
  using SPtr = std::shared_ptr<OrToolsVRPProcessor>;

public:
  OrToolsVRPProcessor(ortoolsengine::ORToolsResult::SPtr result);

  /// Loads the JSON model and creates a solver to run on it.
  /// @note throw std::invalid_argument on empty model
  void loadJSONModelAndCreateSolver(const std::string& jsonModel) override;

  /// Interrupts the internal solving process (if running).
  /// The solver remains in a non-executable state until another model is loaded
  bool interruptSolver() override;

protected:
  using Work = ortoolsengine::ORToolsWork::SPtr;

  /// Lock type on the internal mutex
  using LockGuard = boost::lock_guard<boost::mutex>;

protected:
  void processWork(Work work) override;

private:
  /// VRP model to solve
  ORToolsVRPModel::SPtr pVRPModel;

  /// ORTools solver used to solve VRP problems
  ORToolsVRPSolver::SPtr pVRPSolver;

  /// Pointer to the downcast VRP result instance
  ortoolsengine::ORToolsVRPResult::SPtr pVRPResult;

  /// Mutex synch.ing the state of the result
  boost::mutex pResultMutex;

  /// Utility function: runs the solver
  void runSolver(Work& work);

  /// Utility function: store solution
  void storeSolution();
};

}  // namespace optilab
