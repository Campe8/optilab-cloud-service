#include "or_tools_engine/or_tools_search_combinator_builder.hpp"

#include <stdexcept>  // for std::invalid_argument

#include "model/model_constants.hpp"
#include "or_tools_engine/or_tools_cp_variable.hpp"
#include "or_tools_engine/or_tools_search_combinator.hpp"
#include "or_tools_engine/or_tools_utilities.hpp"

namespace optilab {

ORToolsSearchCombinatorBuilder::ORToolsSearchCombinatorBuilder(
    std::shared_ptr<operations_research::Solver> solver)
: pSolver(solver)
{
  if (!pSolver)
  {
    throw std::invalid_argument("ORToolsSearchCombinatorBuilder - pointer to Solver is empty");
  }
}

std::string ORToolsSearchCombinatorBuilder::getDefaultVariableSelectionStrategy() const
{
  return ortoolsconstants::INT_VAR_DEFAULT;
}  // getDefaultVariableSelectionStrategy

std::string ORToolsSearchCombinatorBuilder::getDefaultValueSelectionStrategy() const
{
  return ortoolsconstants::INT_VALUE_DEFAULT;
}  // getDefaultValueSelectionStrategy

SearchCombinator::SPtr ORToolsSearchCombinatorBuilder::buildBaseSearchCombinator(
    const std::vector<CPVariable::SPtr>& variablesList, const std::string& varSelectionStrategy,
    const std::string& valSelectionStrategy)
{

  std::vector<operations_research::IntVar*> orToolsVarList;
  orToolsVarList.reserve(variablesList.size());

  for (const auto& varPtr : variablesList)
  {
    ORToolsCPVariable::SPtr orToolsCPVar = std::dynamic_pointer_cast<ORToolsCPVariable>(varPtr);
    if (!orToolsCPVar)
    {
      throw std::runtime_error("ORToolsSearchCombinatorBuilder - buildBaseSearchCombinator: "
          "invalid pointer cast to ORTools CPVariables");
    }

    for (auto var : orToolsCPVar->getVariableList())
    {
      orToolsVarList.push_back(var);
    }
  }

  const auto varStrategy = ortoolsutils::getVarSelectionStrategyFromString(varSelectionStrategy);
  const auto valStrategy = ortoolsutils::getValSelectionStrategyFromString(valSelectionStrategy);
  auto decisionBuilder = pSolver->MakePhase(orToolsVarList, varStrategy, valStrategy);

  return std::make_shared<ORToolsSearchCombinator>(decisionBuilder);
}  // buildBaseSearchCombinator

SearchCombinator::SPtr ORToolsSearchCombinatorBuilder::buildAndCombinator(
    const std::vector<SearchCombinator::SPtr>& combList)
{
  if (combList.empty())
  {
    throw std::runtime_error("ORToolsSearchCombinatorBuilder - buildAndCombinator: "
              "empty list of combinators");
  }

  std::vector<operations_research::DecisionBuilder*> composeDB;
  composeDB.reserve(combList.size());

  for (const auto& combPtr : combList)
  {
    ORToolsSearchCombinator::SPtr orToolComp =
        std::dynamic_pointer_cast<ORToolsSearchCombinator>(combPtr);
    if (!orToolComp)
    {
      throw std::runtime_error("ORToolsSearchCombinatorBuilder - buildAndCombinator: "
          "invalid pointer cast to ORTools combinator");
    }

    composeDB.push_back(orToolComp->getDecisionBuilder());
  }

  auto decisionBuilder = pSolver->Compose(composeDB);
  return std::make_shared<ORToolsSearchCombinator>(decisionBuilder);
}  // buildAndCombinator

SearchCombinator::SPtr ORToolsSearchCombinatorBuilder::buildOrCombinator(
    const std::vector<SearchCombinator::SPtr>& combList)
{
  if (combList.empty())
  {
    throw std::runtime_error("ORToolsSearchCombinatorBuilder - buildOrCombinator: "
              "empty list of combinators");
  }

  std::vector<operations_research::DecisionBuilder*> tryDB;
  tryDB.reserve(combList.size());

  for (const auto& combPtr : combList)
  {
    ORToolsSearchCombinator::SPtr orToolComp =
        std::dynamic_pointer_cast<ORToolsSearchCombinator>(combPtr);
    if (!orToolComp)
    {
      throw std::runtime_error("ORToolsSearchCombinatorBuilder - buildOrCombinator: "
          "invalid pointer cast to ORTools combinator");
    }

    tryDB.push_back(orToolComp->getDecisionBuilder());
  }

  auto decisionBuilder = pSolver->Try(tryDB);
  return std::make_shared<ORToolsSearchCombinator>(decisionBuilder);
}  // buildOrCombinator

}  // namespace optilab
