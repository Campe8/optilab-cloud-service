#include "or_tools_engine/or_tools_cp_model.hpp"

#include <cassert>
#include <stdexcept>  // for std::invalid_argument

#include "model/model_object_variable.hpp"
#include "or_tools_engine/or_tools_constraint_posters.hpp"
#include "or_tools_engine/or_tools_utilities.hpp"

namespace optilab {

ORToolsCPModel::ORToolsCPModel(const Model::SPtr& model)
: CPModel(model),
  pSolver(std::make_shared<operations_research::Solver>(model->getModelName()))
{
}

CPVariable::SPtr ORToolsCPModel::createVariable(const ModelObjectVariable::SPtr& objVariable)
{
  if (!objVariable)
  {
    throw std::runtime_error("ORToolsCPModel - empty pointer to ModelObjectVariable");
  }

  const auto domType = objVariable->getDomainType();
  if (domType != ModelObjectVariable::VarType::INT &&
      domType != ModelObjectVariable::VarType::BOOL)
  {
    throw std::invalid_argument("ORToolsCPModel: invalid variable type");
  }

  return ortoolsutils::getCPVar(objVariable, pSolver.get());
}  // createVariable

CPConstraint::SPtr ORToolsCPModel::createConstraint(
    const ModelObjectConstraint::SPtr& objConstraint)
{
  if (!objConstraint)
  {
    throw std::runtime_error("ORToolsCPModel - empty pointer to ModelObjectConstraint");
  }

  return ortoolsutils::getCPCon(objConstraint, pSolver.get(), this);
}  // createConstraint

void ORToolsCPModel::registerConstraints()
{
  registerConstraint("array_bool_and", ortoolsconstraintposters::p_arrayBoolAnd);
  registerConstraint("array_bool_element", ortoolsconstraintposters::p_arrayIntElement);
  registerConstraint("array_bool_or", ortoolsconstraintposters::p_arrayBoolOr);
  registerConstraint("array_bool_xor", ortoolsconstraintposters::p_arrayBoolXor);
  registerConstraint("array_int_element", ortoolsconstraintposters::p_arrayIntElement);
  registerConstraint("array_var_bool_element", ortoolsconstraintposters::p_arrayVarIntElement);
  registerConstraint("array_var_int_element", ortoolsconstraintposters::p_arrayVarIntElement);
  registerConstraint("bool_and", ortoolsconstraintposters::p_boolAnd);
  registerConstraint("bool_clause", ortoolsconstraintposters::p_boolClause);
  registerConstraint("bool_eq", ortoolsconstraintposters::p_intEq);
  registerConstraint("bool_2int", ortoolsconstraintposters::p_intEq);
  registerConstraint("bool2int", ortoolsconstraintposters::p_intEq);
  registerConstraint("bool_2_int", ortoolsconstraintposters::p_intEq);
  registerConstraint("bool_eq_reif", ortoolsconstraintposters::p_intEqReif);
  registerConstraint("bool_ge", ortoolsconstraintposters::p_intGe);
  registerConstraint("bool_ge_reif", ortoolsconstraintposters::p_intGeReif);
  registerConstraint("bool_gt", ortoolsconstraintposters::p_intGt);
  registerConstraint("bool_gt_reif", ortoolsconstraintposters::p_intGtReif);
  registerConstraint("bool_le", ortoolsconstraintposters::p_intLe);
  registerConstraint("bool_le_reif", ortoolsconstraintposters::p_intLeReif);
  registerConstraint("bool_left_imp", ortoolsconstraintposters::p_intLe);
  registerConstraint("bool_lin_eq", ortoolsconstraintposters::p_intLinEq);
  registerConstraint("bool_lin_le", ortoolsconstraintposters::p_intLinLe);
  registerConstraint("bool_lt", ortoolsconstraintposters::p_intLt);
  registerConstraint("bool_lt_reif", ortoolsconstraintposters::p_intLtReif);
  registerConstraint("bool_ne", ortoolsconstraintposters::p_intNe);
  registerConstraint("bool_ne_reif", ortoolsconstraintposters::p_intNeReif);
  registerConstraint("bool_not", ortoolsconstraintposters::p_boolNot);
  registerConstraint("bool_or", ortoolsconstraintposters::p_boolOr);
  registerConstraint("bool_right_imp", ortoolsconstraintposters::p_intGe);
  registerConstraint("bool_xor", ortoolsconstraintposters::p_boolXor);
  registerConstraint("int_abs", ortoolsconstraintposters::p_intAbs);
  registerConstraint("int_div", ortoolsconstraintposters::p_intDiv);
  registerConstraint("int_eq", ortoolsconstraintposters::p_intEq);
  registerConstraint("int_eq_reif", ortoolsconstraintposters::p_intEqReif);
  registerConstraint("int_ge", ortoolsconstraintposters::p_intGe);
  registerConstraint("int_ge_reif", ortoolsconstraintposters::p_intGeReif);
  registerConstraint("int_gt", ortoolsconstraintposters::p_intGt);
  registerConstraint("int_gt_reif", ortoolsconstraintposters::p_intGtReif);
  registerConstraint("int_le", ortoolsconstraintposters::p_intLe);
  registerConstraint("int_le_reif", ortoolsconstraintposters::p_intLeReif);
  registerConstraint("int_lin_eq", ortoolsconstraintposters::p_intLinEq);
  registerConstraint("int_lin_eq_reif", ortoolsconstraintposters::p_intLinEqReif);
  registerConstraint("int_lin_ge", ortoolsconstraintposters::p_intLinGe);
  registerConstraint("int_lin_ge_reif", ortoolsconstraintposters::p_intLinGeReif);
  registerConstraint("int_lin_le", ortoolsconstraintposters::p_intLinLe);
  registerConstraint("int_lin_le_reif", ortoolsconstraintposters::p_intLinLeReif);
  registerConstraint("int_lin_ne", ortoolsconstraintposters::p_intLinNe);
  registerConstraint("int_lin_ne_reif", ortoolsconstraintposters::p_intLinNeReif);
  registerConstraint("int_lt", ortoolsconstraintposters::p_intLt);
  registerConstraint("int_lt_reif", ortoolsconstraintposters::p_intLtReif);
  registerConstraint("int_max", ortoolsconstraintposters::p_intMax);
  registerConstraint("int_min", ortoolsconstraintposters::p_intMin);
  registerConstraint("int_minus", ortoolsconstraintposters::p_intMinus);
  registerConstraint("int_mod", ortoolsconstraintposters::p_intMod);
  registerConstraint("int_ne", ortoolsconstraintposters::p_intNe);
  registerConstraint("int_ne_reif", ortoolsconstraintposters::p_intNeReif);
  registerConstraint("int_negate", ortoolsconstraintposters::p_intNegate);
  registerConstraint("int_plus", ortoolsconstraintposters::p_intPlus);
  registerConstraint("int_times", ortoolsconstraintposters::p_intTimes);
}  // registerConstraint

}  // namespace optilab
