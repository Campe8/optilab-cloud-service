//
// Copyright OptiLab 2020. All rights reserved.
//
// CP Sat solver class for Constraint Programming.
//

#pragma once

#include <memory>  // for std::shared_ptr

#include "cp_toolbox/cp_solver.hpp"
#include "cp_toolbox/cp_utilities.hpp"

#include "optilab_protobuf/constraint_model.pb.h"

namespace optilab {
namespace ortools {

class SYS_EXPORT_CLASS CPSatSolver : public toolbox::CPSolver{
 public:
  using SPtr = std::shared_ptr<CPSatSolver>;

 public:
  CPSatSolver() = default;

  ~CPSatSolver() = default;

  /// Returns the status of the solver
  toolbox::cpengine::CPResult::OptimizerStatus getStatus() noexcept override;

  /// Runs the solver on the given model and returns the status
  toolbox::cpengine::CPResult::OptimizerStatus solve(
          const toolbox::ConstraintModelProto& model) override;

  const toolbox::ConstraintSolutionProto& getSolution() noexcept override;

 private:
  /// Variable storing the current solver's status
  toolbox::cpengine::CPResult::OptimizerStatus pStatus{
    toolbox::cpengine::CPResult::OptimizerStatus::NOT_SOLVED};

  /// Variable storing the current solver's solution
  toolbox::ConstraintSolutionProto pSolution;
};

}  // namespace ortools
}  // namespace optilab
