//
// Copyright OptiLab 2019. All rights reserved.
//
// Search combinator for ORTools CP solvers.
//

#pragma once

#include <memory>   // for std::shared_ptr

#include "ortools/constraint_solver/constraint_solveri.h"
#include "solver/cp_variable.hpp"
#include "solver/search_combinator.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS ORToolsSearchCombinator : public SearchCombinator {
 public:
  using SPtr = std::shared_ptr<ORToolsSearchCombinator>;

 public:
  /// Constructor.
  /// @note throws std::invalid_argument if the given decision builder pointer is nullptr
  ORToolsSearchCombinator(operations_research::DecisionBuilder* searchDecisionBuilder);

  ~ORToolsSearchCombinator() = default;

  /// Returns the DecisionBuilder instance represented by this combinator
  operations_research::DecisionBuilder* getDecisionBuilder() const { return pSearchDecider; }

 private:
  /// Decision builder encapsulating the search strategy the solver runs.
  /// In other words, this is the "combinator" implementation
  operations_research::DecisionBuilder* pSearchDecider;
};

}  // namespace optilab
