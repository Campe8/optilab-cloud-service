//
// Copyright OptiLab 2019. All rights reserved.
//
// Class encapsulating an OR-Tools CP model.
//

#pragma once

#include <memory>   // for std::shared_ptr
#include <string>
#include <unordered_map>

#include "model/model.hpp"
#include "model/model_object_variable.hpp"
#include "ortools/constraint_solver/constraint_solveri.h"
#include "or_tools_engine/or_tools_cp_constraint.hpp"
#include "solver/cp_model.hpp"
#include "solver/cp_variable.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS ORToolsCPModel : public CPModel {
 public:
  using SPtr = std::shared_ptr<ORToolsCPModel>;

 public:
  /// Constructor creates a new CP model run by OR-Tools CP engine.
  /// The model is build on the given input model.
  /// @note throws std::invalid_argument on empty input arguments.
  explicit ORToolsCPModel(const Model::SPtr& model);

  /// Returns the "environment" this model is in,
  /// i.e., the "state" of the model where all model components belong to
  inline std::shared_ptr<operations_research::Solver> getModelEnvironment() const
  {
    return pSolver;
  }

 protected:
  CPVariable::SPtr createVariable(const ModelObjectVariable::SPtr& objVariable) override;

  CPConstraint::SPtr createConstraint(const ModelObjectConstraint::SPtr& objConstraint) override;

  void registerConstraints() override;

 private:
  using SolverPtr = std::shared_ptr<operations_research::Solver>;

 private:
  /// The solver running this model
  SolverPtr pSolver;
};

}  // namespace optilab
