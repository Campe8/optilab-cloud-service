//
// Copyright OptiLab 2019. All rights reserved.
//
// An ORTools optimizer is an asynchronous engine that
// holds a pipeline containing ORTools processors.
//

#pragma once

#include <memory>  // for std::shared_ptr
#include <string>

#include "data_structure/metrics/metrics_register.hpp"
#include "engine/async_engine.hpp"
#include "engine/processor_pipeline.hpp"
#include "model/model.hpp"
#include "optilab_protobuf/optimizer_model.pb.h"
#include "or_tools_engine/or_tools_processor.hpp"
#include "or_tools_engine/or_tools_processor_factory.hpp"
#include "or_tools_engine/or_tools_utilities.hpp"
#include "system/system_export_defs.hpp"

namespace optilab
{
class SYS_EXPORT_CLASS ORToolsOptimizer
    : public AsyncEngine<ortoolsengine::ORToolsWork::SPtr>
{
 public:
  using SPtr = std::shared_ptr<ORToolsOptimizer>;

 public:
  ORToolsOptimizer(const std::string& engineName,
                   const ORToolsProcessorFactory& procFactory,
                   MetricsRegister::SPtr metricsRegister);

  /// Initializes this optimizer with the given protobuf model.
  /// i.e., loads the model into the internal state and creates
  /// the optimizer processor that will solve it.
  /// @note this is a blocking call
  void loadModel(const OptimizerModel& protoModel);

  /// Initializes this optimizer with the given model.
  /// i.e., loads the model into the internal state and creates
  /// the optimizer processor that will solve it.
  /// @note this is a blocking call.
  /// @note if "keepJSON" flag is true, it loads the model directly from the
  /// JSON string, instead of converting it first into a Model instance
  void loadModel(const std::string& jsonModel, bool keepJSON = false);

  /// Runs this optimizer on the loaded model
  void runOptimizer();

  /// Interrupts the solving processes on this optimizer.
  /// Returns true if the solver was successfully interrupted.
  /// Returns false otherwise
  bool interruptOptimizer();

  /// Pointer to the internal model
  inline const Model::SPtr getModel() const noexcept { return pOptimizerModel; }

 protected:
  ProcessorPipeline::SPtr buildPipeline(int pipeline = 0) override;

 private:
  using Work = ortoolsengine::ORToolsWork;
  using BaseClass = AsyncEngine<Work::SPtr>;

 private:
  /// Model solved by the optimizer
  Model::SPtr pOptimizerModel;

  /// Pointer to the instance of the processor in the pipeline
  ORToolsProcessor::SPtr pORToolsProcessor;

  /// Instance of the factory builder for problem-specific processors
  ORToolsProcessorFactory pProcessorFactory;

  /// Pointer to the metrics register given to each work task processed
  /// by each processor
  MetricsRegister::SPtr pMetricsRegister;

  /// Loads the model from JSON to Model instance
  void loadModelImpl(const std::string& jsonModel);
};

}  // namespace optilab
