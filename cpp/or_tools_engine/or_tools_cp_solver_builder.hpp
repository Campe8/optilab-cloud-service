//
// Copyright OptiLab 2019. All rights reserved.
//
// CPSolver builder for OR Tools package.
//

#pragma once

#include <memory>   // for std::shared_ptr

#include "or_tools_engine/or_tools_cp_solver.hpp"
#include "solver/cp_model.hpp"
#include "solver/cp_search.hpp"
#include "solver/cp_solver.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS ORToolsCPSolverBuilder {
 public:
  using SPtr = std::shared_ptr<ORToolsCPSolverBuilder>;

 public:
  /// Constructor.
  /// @note throws std::invalid_argument if the input argument is an empty pointer
  ORToolsCPSolverBuilder(CPModel::SPtr cpModel);

  virtual ~ORToolsCPSolverBuilder() = default;

  /// Builds a (ORTools) CPSolver instance running according to the model and
  /// search strategy provided by the builder
  CPSolver::SPtr build();

 private:
  /// Model to build the solver upon
  CPModel::SPtr pModel;

  /// Builds and returns a CP solver performing global search
  void setGlobalSearchOnSolver(const ORToolsCPSolver::SPtr& solver,
                               const CPSearch::SPtr& searchStrategy);
};

}  // namespace optilab
