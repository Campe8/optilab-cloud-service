//
// Copyright OptiLab 2019. All rights reserved.
//
// VRPSolver for OR Tools package.
//

#pragma once

#include <memory>   // for std::shared_ptr
#include <vector>

#include "or_tools_engine/or_tools_vrp_model.hpp"
#include "ortools/constraint_solver/routing.h"
#include "ortools/constraint_solver/routing_index_manager.h"
#include "ortools/constraint_solver/routing_parameters.h"
#include "or_tools_engine/or_tools_utilities.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS ORToolsVRPSolver {
 public:
  using SPtr = std::shared_ptr<ORToolsVRPSolver>;

 public:
  /// Constructor.
  /// @note throws std::invalid_argument on empty argument pointer
  explicit ORToolsVRPSolver(const ORToolsVRPModel::SPtr& vrpModel);

  ~ORToolsVRPSolver();

  /// Runs the solver until a solution is found or a given timeout is reached
  void solve();

  /// Returns the current status of the solving process
  inline operations_research::RoutingModel::Status getResultStatus() const noexcept
  {
    return pRoutingModel->status();
  }

  /// Stores the solution of the solving process.
  /// @note throws std::runtime_error if the solver has not being invoked,
  /// see also "solve(...)" method
  void storeSolution(ortoolsengine::ORToolsVRPResult& sol);

  /// Stops the (ongoing) search process and returns true if the solver
  /// was actually interrupted, returns false otherwise
  bool interruptSearchProcess();

 private:
  using RoutingIndexManagerPtr = std::shared_ptr<operations_research::RoutingIndexManager>;
  using RoutingModelPtr = std::shared_ptr<operations_research::RoutingModel>;
  using RoutingSearchParams = operations_research::RoutingSearchParameters;

 private:
  /// Pointer to the VRP model
  ORToolsVRPModel::SPtr pVRPModel;

  /// Routing index manager
  RoutingIndexManagerPtr pRIManager;

  /// Pointer to the routing model
  RoutingModelPtr pRoutingModel;

  /// Search parameters
  RoutingSearchParams pSearchParams;

  /// Status of the search solving process
  operations_research::RoutingModel::Status pSearchStatus{
    operations_research::RoutingModel::Status::ROUTING_NOT_SOLVED};

  /// Pointer to the final solution
  const operations_research::Assignment* pSolution{nullptr};

  /// Utility function: initializes this solver w.r.t. the internal VRP model
  void initSolver();
};

}  // namespace optilab
