//
// Copyright OptiLab 2020. All rights reserved.
//
// Solver for the Job-Shop problem written
// using the Google OR-Tools package.
// For more information, see
// https://developers.google.com/optimization/scheduling/job_shop
//

#pragma once

#include <cstdint>  // for uint32_t
#include <memory>   // for std::shared_ptr
#include <utility>  // for std::pair
#include <vector>

#include "optilab_protobuf/scheduling_model.pb.h"
#include "ortools/sat/cp_model.h"
#include "ortools/sat/model.h"
#include "ortools/sat/sat_parameters.pb.h"
#include "scheduling_toolbox/scheduling_utilities.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS JobShopSolver {
 public:
  using SPtr = std::shared_ptr<JobShopSolver>;

 public:
  /// Constructor.
  /// @note throws std::invalid_argument on empty argument pointer
  explicit JobShopSolver(const toolbox::JobShopModelProto& model);

  virtual ~JobShopSolver() = default;

  /// Runs the solver until a solution is found or a given timeout is reached
  void solve();

  /// Returns the current status of the solving process
  inline operations_research::sat::CpSolverStatus getResultStatus() const noexcept
  {
    return pSearchStatus;
  }

  /// Stores the solution of the solving process.
  /// @note throws std::runtime_error if the solver has not being invoked,
  /// see also "solve(...)" method
  void storeSolution(toolbox::schedulingengine::JobShopResult& sol);

  /// Stops the (ongoing) search process and returns true if the solver
  /// was actually interrupted, returns false otherwise
  bool interruptSearchProcess();

 private:
  using ModelBuilder = operations_research::sat::CpModelBuilder;
  using ModelBuilderPtr = std::shared_ptr<ModelBuilder>;

 private:
  /// Model builder for the CP SAT solver
  ModelBuilderPtr pSATModelBuilder;

  /// Flag indicating whether or not this is an optimization problem
  bool pIsOptimizationModel{false};

  /// Status of the search solving process
  operations_research::sat::CpSolverStatus pSearchStatus{
    operations_research::sat::CpSolverStatus::UNKNOWN};

  /// Pointer to the final solution
  operations_research::sat::CpSolverResponse pSolution;

  /// Start variables for each job
  std::vector<operations_research::sat::IntVar> pJobStarts;

  /// End variables for each job
  std::vector<operations_research::sat::IntVar> pJobEnds;

  /// All task's starts
  std::vector<operations_research::sat::IntVar> pTaskStarts;

  /// Variable selection strategy
  operations_research::sat::DecisionStrategyProto::VariableSelectionStrategy pVariableStrategy;

  /// Value selection strategy
  operations_research::sat::DecisionStrategyProto::DomainReductionStrategy pValueStrategy;

  /// Mapping between jobs and task start variables
  std::vector<std::vector<uint32_t>> pJobToTaskStarts;

  /// Utility function: initializes this solver w.r.t. the internal scheduling model
  void initSolver(const toolbox::JobShopModelProto& model);

  /// Initialize the internal solver on the given JS problem
  void initJobShopModel(const toolbox::JsspInputProblem& problem);

  /// Computes a valid horizon for the given problem
  int64 computeHorizon(const toolbox::JsspInputProblem& problem);
};

}  // namespace optilab
