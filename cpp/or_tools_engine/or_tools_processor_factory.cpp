#include "or_tools_engine/or_tools_processor_factory.hpp"

#include <stdexcept>  // for std::invalid_argument
#include <string>

#include "or_tools_engine/or_tools_cp_processor.hpp"
#include "or_tools_engine/or_tools_mip_processor.hpp"
#include "or_tools_engine/or_tools_vrp_processor.hpp"

namespace optilab {

ORToolsProcessorFactory::ORToolsProcessorFactory(ortoolsengine::ORToolsResult::SPtr result)
: pResult(result),
  pProcType(EngineClassType::EC_UNDEF)
{
  if (!pResult)
  {
    throw std::runtime_error("ORToolsProcessorFactory - empty result");
  }
}

ORToolsProcessor::SPtr ORToolsProcessorFactory::buildProcessor()
{
  ORToolsProcessor::SPtr proc;
  switch(pProcType)
  {
    case EngineClassType::EC_CP:
    {
      proc = std::make_shared<OrToolsCPProcessor>(pResult);
      break;
    }
    case EngineClassType::EC_MIP:
    {
      proc = std::make_shared<OrToolsMIPProcessor>(pResult);
      break;
    }
    case EngineClassType::EC_VRP:
    {
      proc = std::make_shared<OrToolsVRPProcessor>(pResult);
      break;
    }
    default:
      throw std::runtime_error("ORToolsProcessorFactory - buildProcessor: "
          "unrecognized processor type " + std::to_string(static_cast<int>(pProcType)));
  }
  return proc;
}  // buildProcessor

}  // namespace optilab
