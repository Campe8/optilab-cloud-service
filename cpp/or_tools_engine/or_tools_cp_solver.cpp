#include "or_tools_engine/or_tools_cp_solver.hpp"

#include <functional>  // for std::function
#include <stdexcept>   // for std::invalid_argument

#include <spdlog/spdlog.h>

#include "or_tools_engine/or_tools_search_combinator.hpp"
#include "solver/cp_global_search.hpp"

namespace optilab {

ORToolsCPSolver::ORToolsCPSolver(const ORToolsCPModel::SPtr& cpModel)
{
  if (!cpModel)
  {
    throw std::invalid_argument("ORToolsCPSolver - empty pointer to a CP model");
  }

  // Get the model environment which, in the ORTools package, corresponds to a Solver instance.
  // In other packages, the model environment could mean different things,
  // for example, in the Gecode package, the environment is represented by an instance
  // of a "Space" object
  pSolver = cpModel->getModelEnvironment();
  if (!pSolver)
  {
    throw std::invalid_argument("ORToolsCPSolver - empty environment");
  }
}

void ORToolsCPSolver::setSearchHeuristic(operations_research::DecisionBuilder* huristic)
{
  if (!huristic)
  {
    throw std::invalid_argument("ORToolsCPSolver - pointer to search heuristic is NULL");
  }

  pHeuristic = huristic;
}  // setSearchHeuristic

bool ORToolsCPSolver::checkWhetherSolverShouldStop()
{
  // The solver should stop when it is NOT active
  return !pSolverActive;
}  // checkWhetherSolverShouldStop

void ORToolsCPSolver::interruptSearchProcess()
{
  pSolverActive = false;
}  // interruptSearchProcess

void ORToolsCPSolver::initSearch()
{
  // Create the monitor used to kill the engine given the kill event
  std::function<bool()> killEngineLimit =
      std::bind(&ORToolsCPSolver::checkWhetherSolverShouldStop, this);
  auto searchLimit = pSolver->MakeCustomLimit(killEngineLimit);

  // Add the custom monitor to the list of search monitors
  pSearchMonitorsList.push_back(searchLimit);

  // Create a new search in the solver
  if (!pHeuristic)
  {
    const std::string errMsg = "ORToolsCPSolver - initSearch: heuristic not set";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }
  pSolver->NewSearch(pHeuristic, pSearchMonitorsList);

  // Set solver as initialized
  pSolverInitialized = true;
}  // initSearch

void ORToolsCPSolver::cleanupSearch()
{
  // Terminate the search
  pSolver->EndSearch();
}  // cleanupSearch

bool ORToolsCPSolver::constraintPropagation()
{
  if (!pSolverInitialized)
  {
    const std::string errMsg = "ORToolsCPSolver - constraintPropagation: solver has not "
        "been initialized";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  // Run the solver once
  const bool satModel = pSolver->NextSolution();

  // Set the status of the solver
  setCPSolverStatusOnORToolsSolverState();

  // Return the result of the propagation
  return satModel;
}  // constraintPropagation

bool ORToolsCPSolver::nextSolution()
{
  // Get next solution
  const bool hasNextSolution = pSolver->NextSolution();

  // Set the status of the solver
  setCPSolverStatusOnORToolsSolverState();

  // Return the solution
  return hasNextSolution;
}  // nextSolution

void ORToolsCPSolver::setCPSolverStatusOnORToolsSolverState()
{
  if (pSolver->state() ==
      operations_research::Solver::SolverState::NO_MORE_SOLUTIONS)
  {
    setSolverStatus(SolverStatus::SS_OPT);
  }
  else if (pSolver->state() == operations_research::Solver::SolverState::AT_SOLUTION)
  {
    setSolverStatus(SolverStatus::SS_SAT);
  }
  else if (pSolver->state() ==
      operations_research::Solver::SolverState::PROBLEM_INFEASIBLE)
  {
    setSolverStatus(SolverStatus::SS_UNSAT);
  }
  else
  {
    setSolverStatus(SolverStatus::SS_UNKNOWN);
  }
}  // setCPSolverStatusOnORToolsSolverState


}  // namespace optilab
