//
// Copyright OptiLab 2019. All rights reserved.
//
// Class encapsulating an OR-Tools CP variable
//

#pragma once

#include <cstdint>  // for int64_t
#include <memory>   // for std::shared_ptr
#include <string>
#include <vector>

#include "ortools/constraint_solver/constraint_solveri.h"
#include "solver/cp_variable.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS ORToolsCPVariable : public CPVariable {
 public:
  using SPtr = std::shared_ptr<ORToolsCPVariable>;

 public:
  /// Creates a scalar variable with given name and domain.
  /// @note throws std::invalid_argument if the pointer to the OR-Tools space is nullptr
  ORToolsCPVariable(const std::string& name, const CPDomain& domain,
                    operations_research::Solver* solver);

  /// Creates an array of variables of given size and with given name and domain.
  /// @note throws std::invalid_argument if the pointer to the OR-Tools space is nullptr
  ORToolsCPVariable(const std::string& name, const CPDomain& domain, int len,
                    operations_research::Solver* solver);

  /// Creates an n-dimensional matrix of variables of given dimensions, with given name and domain
  /// @note dims size must be at least two.
  /// @note throws std::invalid_argument if the pointer to the OR-Tools space is nullptr
  ORToolsCPVariable(const std::string& name, const CPDomain& domain, const std::vector<int>& dims,
                    operations_research::Solver* solver);

  /// Returns the actual variable (if scalar) or the first variable of an array/matrix
  inline operations_research::IntVar* getVariable() const
  {
    return pVariableList.at(0);
  }

  /// Returns the vectorized list of variables
  inline const std::vector<operations_research::IntVar*>& getVariableList() const
  {
    return pVariableList;
  }

  /// Returns the variable at the given subscript index.
  /// @note the identifier of the returned variable is the following:
  ///       <this_variable_name>_dim1_dim2_..._dim_n
  /// @note throws std::runtime_error on scalar variables.
  /// @note throws std::runtime_error on wrong indexing
  CPVariable::SPtr getSubVariable(const std::vector<int>& subscr) override;

 protected:
  /// Base constructor
  ORToolsCPVariable(const std::string& name, operations_research::Solver* solver);

 private:
  /// Pointer to the solver used to create this instance of variable
  operations_research::Solver* pSolver;

  /// List of all variables (scalar, array, matrix) encapsulated by this instance
  std::vector<operations_research::IntVar*> pVariableList;

  /// Utility function: builds a variable with given domain and adds it to the solver
  /// of this ORToolsCPVariable.
  /// Returns the pointer of the variable in the solver
  operations_research::IntVar* buildVariableAndAddItToSolver(const CPDomain& domain);
};

}  // namespace optilab
