#include "or_tools_engine/or_tools_search_combinator.hpp"

#include <stdexcept>  // for std::invalid_argument

namespace optilab {

ORToolsSearchCombinator::ORToolsSearchCombinator(
    operations_research::DecisionBuilder* searchDecisionBuilder)
: pSearchDecider(searchDecisionBuilder)
{
  if (!pSearchDecider)
  {
    throw std::invalid_argument("ORToolsSearchCombinator - pointer to DecisionBuilder is NULL");
  }
}

}  // namespace optilab
