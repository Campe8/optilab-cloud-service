#include "or_tools_engine/cp_sat_solver.hpp"

#include "ortools/sat/cp_model.h"

namespace optilab {
namespace ortools {

toolbox::cpengine::CPResult::OptimizerStatus CPSatSolver::getStatus() noexcept
{
  return pStatus;
}

toolbox::cpengine::CPResult::OptimizerStatus CPSatSolver::solve(
        const toolbox::ConstraintModelProto& model)
{
  const auto response = operations_research::sat::Solve(model.cp_sat_model().cp_model());

  // Get and clear current solution (if any)
  auto cpSatSolution = pSolution.mutable_cp_sat_solution();
  cpSatSolution->Clear();

  auto solverResponse = cpSatSolution->mutable_cp_solver_solution();
  solverResponse->CopyFrom(response);

  // Set and returns the status of the search
  switch(response.status())
  {
    case operations_research::sat::CpSolverStatus::FEASIBLE:
    {
      pStatus = toolbox::cpengine::CPResult::OptimizerStatus::FEASIBLE;
      pSolution.set_status(toolbox::OptimizerSolutionStatusProto::OPT_SOLVER_SUCCESS);
      break;
    }
    case operations_research::sat::CpSolverStatus::OPTIMAL:
    {
      pStatus = toolbox::cpengine::CPResult::OptimizerStatus::OPTIMUM;
      pSolution.set_status(toolbox::OptimizerSolutionStatusProto::OPT_SOLVER_SUCCESS);
      break;
    }
    case operations_research::sat::CpSolverStatus::INFEASIBLE:
      pStatus = toolbox::cpengine::CPResult::OptimizerStatus::INFEASIBLE;
      pSolution.set_status(toolbox::OptimizerSolutionStatusProto::OPT_SOLVER_FAIL);
      break;
    case operations_research::sat::CpSolverStatus::MODEL_INVALID:
    {
      pStatus = toolbox::cpengine::CPResult::OptimizerStatus::MODEL_INVALID;
      pSolution.set_status(toolbox::OptimizerSolutionStatusProto::OPT_SOLVER_MODEL_INVALID);
      break;
    }
    default:
    {
      pStatus = toolbox::cpengine::CPResult::OptimizerStatus::UNKNOWN;
      pSolution.set_status(toolbox::OptimizerSolutionStatusProto::OPT_SOLVER_UNKNOWN_STATUS);
      break;
    }
  }

  // Return the status of the solver
  return pStatus;
}

const toolbox::ConstraintSolutionProto& CPSatSolver::getSolution() noexcept
{
  return pSolution;
}

}  // namespace ortools
}  // namespace optilab
