//
// Copyright OptiLab 2019. All rights reserved.
//
// Processor for OR-Tools MIP engines.
//

#pragma once

#include <map>
#include <memory>  // for std::shared_ptr
#include <string>
#include <utility>  // for std::pair
#include <vector>

#include "model/model.hpp"
#include "optilab_protobuf/optimizer_model.pb.h"
#include "or_tools_engine/or_tools_mip_model.hpp"
#include "or_tools_engine/or_tools_mip_solver.hpp"
#include "or_tools_engine/or_tools_processor.hpp"
#include "or_tools_engine/or_tools_utilities.hpp"
#include "solver/mip_solver.hpp"
#include "system/system_export_defs.hpp"

namespace optilab
{
class SYS_EXPORT_CLASS OrToolsMIPProcessor : public ORToolsProcessor
{
 public:
  using SPtr = std::shared_ptr<OrToolsMIPProcessor>;

 public:
  OrToolsMIPProcessor(ortoolsengine::ORToolsResult::SPtr result);

  /// Loads the protobuf model and creates a solver to run on it.
  /// @note throw std::invalid_argument on empty model
  void loadModelAndCreateSolver(const OptimizerModel& protoModel) override;

  /// Loads the model and creates a solver to run on it.
  /// @note throw std::invalid_argument on empty model
  void loadModelAndCreateSolver(const Model::SPtr& model) override;

  /// Interrupts the internal solving process (if running).
  /// The solver remains in a non-executable state until another model is loaded
  bool interruptSolver() override;

 protected:
  using Work = ortoolsengine::ORToolsWork::SPtr;

  /// Lock type on the internal mutex
  using LockGuard = boost::lock_guard<boost::mutex>;

 protected:
  void processWork(Work work) override;

 private:
  /// Model to solve
  ORToolsMIPModel::SPtr pMIPModel;

  /// ORTools MIPSolver used to solve instances on this engine processor
  ORToolsMIPSolver::SPtr pMIPSolver;

  /// Pointer to the downcast MIP result instance
  ortoolsengine::ORToolsMIPResult::SPtr pMIPResult;

  /// Mutex synch.ing the state of the result
  boost::mutex pResultMutex;

  /// Utility function: runs the solver
  void runSolver(Work& work);

  /// Utility function: store solution
  void storeSolution();

  /// Utility function: store an mps model format solution
  void storeMPSSolution();

  /// Utility function: store a OptiLab model format solution
  void storeOptiLabSolution();

  /// Utility function: builds the internal solver based on the
  /// initialized MIP model
  void buildSolver();
};

}  // namespace optilab
