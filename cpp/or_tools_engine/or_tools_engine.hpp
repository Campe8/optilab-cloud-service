//
// Copyright OptiLab 2019. All rights reserved.
//
// Base class for ORTools engines.
// ORTools engines are engines that solve satisfaction and optimization
// problems using the ORTools libraries.
//

#pragma once

#include <memory>  // for std::shared_ptr
#include <string>

#include "optilab_protobuf/optimizer_model.pb.h"
#include "or_tools_engine/or_tools_utilities.hpp"
#include "system/system_export_defs.hpp"

namespace optilab
{
class SYS_EXPORT_CLASS ORToolsEngine
{
 public:
  using SPtr = std::shared_ptr<ORToolsEngine>;

 public:
  virtual ~ORToolsEngine() = default;

  /// Registers the given optimizer model specified by the given protobuf
  /// message
  /// @note this method does not run the model.
  /// @throw std::runtime_error if this model is called while the engine is
  /// running
  virtual void registerModel(const OptimizerModel& protoModel) = 0;

  /// Registers the given model.
  /// @note this method does not run the model.
  /// @throw std::runtime_error if this model is called while the engine is
  /// running
  virtual void registerModel(const std::string& model) = 0;

  /// Notifies the engine on a given EngineEvent
  virtual void notifyEngine(const ortoolsengine::ORToolsEvent& event) = 0;

  /// Wait for the current jobs in the queue to finish.
  /// @note if "timeoutSec" is -1, waits for all jobs in the queue to complete
  virtual void engineWait(int timeoutMsec) = 0;

  /// Shuts down the engine
  virtual void turnDown() = 0;
};

}  // namespace optilab
