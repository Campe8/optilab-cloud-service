#include "or_tools_engine/or_tools_vrp_model.hpp"

#include <stdexcept>  // for std::invalid_argument

#include <spdlog/spdlog.h>

#include "engine/engine_constants.hpp"
#include "model/model_constants.hpp"

namespace {

constexpr int kMaxNumVehicles = 1000000000;

}  // namespace

namespace optilab {

ORToolsVRPModel::ORToolsVRPModel(const std::string& jsonModel)
: pNumVehicles(0),
  pNumCities(-1)
{
  if (jsonModel.empty())
  {
    throw std::invalid_argument("ORToolsVRPModel - empty JSON model");
  }

  initModel(jsonModel);
}

void ORToolsVRPModel::initModel(const std::string& json)
{
  JSON::SPtr jsonModel;
  try
  {
    jsonModel = std::make_shared<JSON>(json);
  }
  catch(...)
  {
    const std::string errMsg = "ORToolsVRPModel - initModel: invalid parsing of input JSON model";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  // Get and set the framework the model should run in
  if (!jsonModel->hasObject(jsonmodel::CLASS_TYPE))
  {
    throw std::runtime_error("ORToolsVRPModel - initModel: "
        "missing field \"" + std::string(jsonmodel::CLASS_TYPE) + "\"");
  }

  const std::string classType = (jsonModel->getObject(jsonmodel::CLASS_TYPE)).GetString();
  if (classType != std::string(engineconsts::ENGINE_CLASS_TYPE_VRP))
  {
    throw std::runtime_error("ORToolsVRPModel - initModel: "
        "invalid model class type " + classType);
  }

  if (!jsonModel->hasObject(jsonmodel::MODEL_NAME))
  {
    throw std::runtime_error("ORToolsVRPModel - initModel: "
        "missing field \"" + std::string(jsonmodel::MODEL_NAME) + "\"");
  }
  pModelName = (jsonModel->getObject(jsonmodel::MODEL_NAME)).GetString();

  if (!jsonModel->hasObject(jsonvrpmodel::DISTANCE_MATRIX))
  {
    throw std::runtime_error("ORToolsVRPModel - initModel: "
        "missing field \"" + std::string(jsonvrpmodel::DISTANCE_MATRIX) + "\"");
  }
  initDistanceMatrix(jsonModel->getObject(jsonvrpmodel::DISTANCE_MATRIX));

  if (!jsonModel->hasObject(jsonvrpmodel::DEMANDS))
  {
    throw std::runtime_error("ORToolsVRPModel - initModel: "
        "missing field \"" + std::string(jsonvrpmodel::DEMANDS) + "\"");
  }
  initDemands(jsonModel->getObject(jsonvrpmodel::DEMANDS));

  if (!jsonModel->hasObject(jsonvrpmodel::VEHICLE_CAPACITIES))
  {
    throw std::runtime_error("ORToolsVRPModel - initModel: "
        "missing field \"" + std::string(jsonvrpmodel::VEHICLE_CAPACITIES) + "\"");
  }
  initVehicleCapacities(jsonModel->getObject(jsonvrpmodel::VEHICLE_CAPACITIES));

  if (!jsonModel->hasObject(jsonvrpmodel::NUM_VEHICLES))
  {
    throw std::runtime_error("ORToolsVRPModel - initModel: "
        "missing field \"" + std::string(jsonvrpmodel::NUM_VEHICLES) + "\"");
  }

  pNumVehicles = (jsonModel->getObject(jsonvrpmodel::NUM_VEHICLES)).GetInt();
  if (pNumVehicles < 1 || pNumVehicles > kMaxNumVehicles)
  {
    throw std::runtime_error("ORToolsVRPModel - initModel: "
        "invalid number of vehicles: " + classType);
  }

  if (!jsonModel->hasObject(jsonvrpmodel::DEPOT))
  {
    throw std::runtime_error("ORToolsVRPModel - initModel: "
        "missing field \"" + std::string(jsonvrpmodel::DEPOT) + "\"");
  }

  pDepot = (jsonModel->getObject(jsonvrpmodel::DEPOT)).GetInt();
  if (pDepot < 0 || pDepot > (getNumCities() - 1))
  {
    throw std::runtime_error("ORToolsVRPModel - initModel: "
        "invalid depot index: " + classType);
  }
}  // initModel

void ORToolsVRPModel::initDistanceMatrix(JSONValue::JsonObject& mat)
{
  /// First parse the list of search object
  int numCities = -1;
  for (JSONValue::JsonObject& iter : mat.GetArray())
  {
    std::vector<int64> dlist;
    for (JSONValue::JsonObject& dist : iter.GetArray())
    {
      dlist.push_back(dist.GetInt64());
    }
    pDistanceMatrix.push_back(dlist);

    int currNumCities = static_cast<int>(pDistanceMatrix.back().size());
    if (numCities == -1)
    {
      numCities = currNumCities;
    }

    if (currNumCities != numCities)
    {
      throw std::runtime_error("ORToolsVRPModel - invalid distance matrix: not a square matrix");
    }
  }

  if (static_cast<int>(pDistanceMatrix.size()) != numCities)
  {
    throw std::runtime_error("ORToolsVRPModel - invalid distance matrix: not a square matrix");
  }

  // Set the number of cities as the size of the matrix
  pNumCities = numCities;
}  // initDistanceMatrix

void ORToolsVRPModel::initDemands(JSONValue::JsonObject& demands)
{
  for (JSONValue::JsonObject& iter : demands.GetArray())
  {
    pDemandList.push_back(iter.GetInt64());
  }
}  // initDemands

void ORToolsVRPModel::initVehicleCapacities(JSONValue::JsonObject& vcap)
{
  for (JSONValue::JsonObject& iter : vcap.GetArray())
  {
    pVehicleCapacityList.push_back(iter.GetInt64());
  }
}  // initVehicleCapacities

}  // namespace optilab
