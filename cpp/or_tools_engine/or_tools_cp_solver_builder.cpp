#include "or_tools_engine/or_tools_cp_solver_builder.hpp"

#include <cassert>
#include <stdexcept>  // for std::invalid_argument

#include "or_tools_engine/or_tools_cp_model.hpp"
#include "or_tools_engine/or_tools_search_combinator.hpp"
#include "or_tools_engine/or_tools_search_combinator_builder.hpp"
#include "solver/cp_global_search.hpp"
#include "solver/cp_search_builder.hpp"

namespace optilab {

ORToolsCPSolverBuilder::ORToolsCPSolverBuilder(CPModel::SPtr cpModel)
: pModel(cpModel)
{
  if (!pModel)
  {
    throw std::invalid_argument("ORToolsCPSolverBuilder - empty CPModel pointer");
  }
}

CPSolver::SPtr ORToolsCPSolverBuilder::build()
{
  // Get the ORToolsCPModel used to instantiate the solver
  ORToolsCPModel::SPtr orToolCPModel = std::dynamic_pointer_cast<ORToolsCPModel>(pModel);
  if (!orToolCPModel)
  {
    throw std::runtime_error("ORToolsCPSolverBuilder - CPModel is not an ORTools CPModel");
  }

  // Create a new instance of a solver
  ORToolsCPSolver::SPtr solver = std::make_shared<ORToolsCPSolver>(orToolCPModel);

  // Create a builder for search strategies
  ORToolsSearchCombinatorBuilder::SPtr combinatorBuilder =
      std::make_shared<ORToolsSearchCombinatorBuilder>(solver->getSolver());
  CPSearchBuilder::SPtr searchBuilder = std::make_shared<CPSearchBuilder>(pModel,
                                                                          combinatorBuilder);

  // Build the search strategy
  CPSearch::SPtr searchStrategy = searchBuilder->build();

  // Set the search strategy into the solver
  const auto searchType = searchStrategy->getSearchType();
  switch (searchType)
  {
    case CPSearch::SearchType::GLOBAL_SEARCH:
    {
      setGlobalSearchOnSolver(solver, searchStrategy);
      break;
    }
    default:
    {
      assert(searchType == CPSearch::SearchType::LOCAL_SEARCH);
      throw std::runtime_error("ORToolsCPSolverBuilder - local search strategy not supported");
    }
  }

  // Return the CP solver
  return solver;
}  // build

void ORToolsCPSolverBuilder::setGlobalSearchOnSolver(const ORToolsCPSolver::SPtr& solver,
                                                     const CPSearch::SPtr& searchStrategy)
{
  assert(CPGlobalSearchStrategy::isa(searchStrategy.get()));
  auto globalSearch = CPGlobalSearchStrategy::cast(searchStrategy.get());

  // Get the search combinator describing this search heuristic
  auto heuristic =
      std::dynamic_pointer_cast<ORToolsSearchCombinator>(globalSearch->getSearchHeuristic());
  if (!heuristic)
  {
    throw std::runtime_error("ORToolsCPSolverBuilder - search heuristic combinator is not "
        "an ORTools search combinator");
  }

  // Set the search heuristic to be used by the solver to explore the search space
  solver->setSearchHeuristic(heuristic->getDecisionBuilder());
}  // setGlobalSearchOnSolver

}  // namespace optilab
