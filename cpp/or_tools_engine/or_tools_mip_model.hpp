//
// Copyright OptiLab 2019. All rights reserved.
//
// Class encapsulating an OR-Tools MIP model.
//

#pragma once

#include <memory>  // for std::shared_ptr

#include "model/model.hpp"
#include "model/model_object_constraint.hpp"
#include "model/model_object_objective.hpp"
#include "model/model_object_variable.hpp"
#include "optilab_protobuf/linear_model.pb.h"
#include "ortools/linear_solver/linear_solver.h"
#include "ortools/linear_solver/linear_solver.pb.h"
#include "solver/mip_constraint.hpp"
#include "solver/mip_model.hpp"
#include "solver/mip_objective.hpp"
#include "solver/mip_variable.hpp"
#include "system/system_export_defs.hpp"

namespace optilab
{
class SYS_EXPORT_CLASS ORToolsMIPModel : public MIPModel
{
 public:
  using SPtr = std::shared_ptr<ORToolsMIPModel>;

 public:
  /// Constructor creates a new MIP model run by OR-Tools MIP engine.
  /// The model is build on the given input model.
  /// @note throws std::invalid_argument on empty input arguments.
  explicit ORToolsMIPModel(const Model::SPtr& model);

  /// Constructor creates a new MIP model run by OR-Tools MIP engine.
  /// The model is build on the given input model.
  /// @note throws std::invalid_argument on empty input arguments.
  explicit ORToolsMIPModel(const LinearModelSpecProto& model);

  /// Returns the "environment" this model is in,
  /// i.e., the "state" of the model where all model components belong to
  inline std::shared_ptr<operations_research::MPSolver> getModelEnvironment()
      const
  {
    return pSolver;
  }

  /// Returns the ORTools protobuf description of this model.
  /// @note this is valid on legacy/non-OptiLab model formats
  const operations_research::MPModelProto& getModelProto() const
  {
    return pModelProto;
  }

 protected:
  /// Initialize a legacy model.
  /// @note to get information about legacy models, use the following:
  /// - "getModelInputFormat(...)": to get the model format, e.g., MPS;
  /// - "getLegacyModelData(...)": to get the verbatim copy of the data;
  /// - "getLegacyModelDescriptor(...)" to get the verbatim copy of the model.
  void initializeWithLegacyModelFormat() override;

  /// Creates a MIPVariable (package-specific) given a ModelObjectVariable
  /// instance
  MIPVariable::SPtr createVariable(
      const ModelObjectVariable::SPtr& objVariable) override;

  /// Creates a MIPConstraint (package-specific) given a ModelObjectConstraint
  /// instance and the map of all the variables in the model
  MIPConstraint::SPtr createConstraint(
      const ModelObjectConstraint::SPtr& objConstraint,
      const VarMap& varMap) override;

  /// Creates a MIPObjective (package-specific) given a ModelObjectObjective
  /// instance and the map of all the variables in the model
  MIPObjective::SPtr createObjective(
      const ModelObjectObjective::SPtr& objObjective,
      const VarMap& varMap) override;

  /// Creates  MIPVariable (package-specific) given a proto variable description
  MIPVariable::SPtr createVariableFromProto(
      const OptVariableProto& protoVar) override;

  /// Creates  MIPConstraint (package-specific) given a proto constraint
  /// description
  MIPConstraint::SPtr createConstraintFromProto(
      const OptConstraintProto& protoCon, const VarMap& varMap) override;

  /// Creates a MIPObjective (package-specific) given a the protobuf model
  /// and the map of all the variables in the model
  MIPObjective::SPtr createObjectiveFromProto(const OptModelProto& model,
                                              const VarMap& varMap) override;

 private:
  using SolverPtr = std::shared_ptr<operations_research::MPSolver>;

 private:
  /// The solver running this model
  SolverPtr pSolver;

  /// Instance of the ORTools protobuf model used on legacy formats
  operations_research::MPModelProto pModelProto;

  /// Loads an mps model into the solver
  void initializeORToolsMPSModel();
};

}  // namespace optilab
