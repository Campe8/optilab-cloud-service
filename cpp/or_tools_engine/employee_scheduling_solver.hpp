//
// Copyright OptiLab 2020. All rights reserved.
//
// Solver for the Employees Scheduling problem written
// using the Google OR-Tools package.
// For more information, see
// https://developers.google.com/optimization/scheduling/employee_scheduling
//

#pragma once

#include <memory>   // for std::shared_ptr
#include <unordered_map>
#include <utility>  // for std::pair
#include <vector>

#include "optilab_protobuf/scheduling_model.pb.h"
#include "ortools/sat/cp_model.h"
#include "ortools/sat/model.h"
#include "ortools/sat/sat_parameters.pb.h"
#include "scheduling_toolbox/scheduling_utilities.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS EmployeeSchedulingSolver {
 public:
  using SPtr = std::shared_ptr<EmployeeSchedulingSolver>;

 public:
  /// Constructor.
  /// @note throws std::invalid_argument on empty argument pointer
  explicit EmployeeSchedulingSolver(const toolbox::EmployeeSchedulingModelProto& smodel);

  virtual ~EmployeeSchedulingSolver() = default;

  /// Runs the solver until a solution is found or a given timeout is reached
  void solve();

  /// Returns the current status of the solving process
  inline operations_research::sat::CpSolverStatus getResultStatus() const noexcept
  {
    return pSearchStatus;
  }

  /// Stores the solution of the solving process.
  /// @note throws std::runtime_error if the solver has not being invoked,
  /// see also "solve(...)" method
  void storeSolution(toolbox::schedulingengine::EmployeesSchedulingResult& sol);

  /// Stops the (ongoing) search process and returns true if the solver
  /// was actually interrupted, returns false otherwise
  bool interruptSearchProcess();

 private:
  using ModelBuilder = operations_research::sat::CpModelBuilder;
  using ModelBuilderPtr = std::shared_ptr<ModelBuilder>;

 private:
  /// Model builder for the CP SAT solver
  ModelBuilderPtr pSATModelBuilder;

  /// Matrix of variables for shifts
  std::vector<std::vector<std::vector<operations_research::sat::BoolVar>>> pShiftMatrix;

  /// Model data
  int pNumPersonnel{-1};
  int pNumDays{-1};
  int pNumShifts{-1};

  /// Map of employees preferences.
  /// The map stores:
  /// - key: employee id
  /// - value: list of [day, shift] preferences
  std::unordered_map<int, std::vector<std::pair<int, int>>> pPreferenceMap;

  /// Status of the search solving process
  operations_research::sat::CpSolverStatus pSearchStatus{
    operations_research::sat::CpSolverStatus::UNKNOWN};

  /// Pointer to the final solution
  operations_research::sat::CpSolverResponse pSolution;

  /// Utility function: initializes this solver w.r.t. the internal scheduling model
  void initSolver(const toolbox::EmployeeSchedulingModelProto& smodel);

  /// Utility function: returns true if the employee with given id,
  /// day, and shift requested the shift, returns false otherwise
  bool employeeRequestedDayAndShift(int id, int day, int shift);
};

}  // namespace optilab
