//
// Copyright OptiLab 2019. All rights reserved.
//
// Class encapsulating an OR-Tools VRP model.
//

#pragma once

#include <memory>  // for std::shared_ptr
#include <string>
#include <vector>

#include "data_structure/json/json.hpp"
#include "ortools/constraint_solver/routing_index_manager.h"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS ORToolsVRPModel {
 public:
  using SPtr = std::shared_ptr<ORToolsVRPModel>;

  /// Distance matrix representation
  using DistanceMatrix = std::vector<std::vector<int64>>;
  using VehicleCapacities = std::vector<int64>;

 public:
  /// Constructor creates a new VRP model run by the OR-Tools VRP engine.
  /// The model is build on the given input JSON model.
  /// @note throws std::invalid_argument on empty input arguments.
  explicit ORToolsVRPModel(const std::string& jsonModel);

  /// Returns the model's name
  inline const std::string& getModelName() const noexcept { return pModelName; }

  /// Returns the total number of cities
  inline int getNumCities() const noexcept { return pNumCities; }

  /// Returns the number of vehicles
  inline int getNumVehicles() const noexcept { return pNumVehicles; }

  /// Returns the distance matrix
  inline const DistanceMatrix& getDistanceMatrix() const noexcept { return pDistanceMatrix; }

  /// Returns the demands list
  inline const std::vector<int64>& getDemands() const noexcept { return pDemandList; }

  /// Returns the vehicle capacities list
  inline const std::vector<int64>& getVehicleCapacities() const noexcept
  {
    return pVehicleCapacityList;
  }

  /// Returns the depot index
  inline const operations_research::RoutingIndexManager::NodeIndex& getDepot() const noexcept
  {
    return pDepot;
  }

 private:
  /// Model's name
  std::string pModelName;

  /// Total number of cities in the model
  int pNumCities;

  /// Distance matrix for the VRP problem
  DistanceMatrix pDistanceMatrix;

  /// Total number of vehicles.
  /// @note this number must be greater than zero.
  /// @note if the number of vehicles is one,
  /// the problem reduces to TSP (with capacity constraints)
  int pNumVehicles;

  /// List of demands.
  /// @note the list can be empty.
  /// @note if not empty, the size should match the number of cities
  std::vector<int64> pDemandList;

  /// List of vehicle capacities.
  /// @note the list can be empty.
  /// @note if not empty, the size should match the number of vehicles
  std::vector<int64> pVehicleCapacityList;

  /// Depot, i.e., starting point
  operations_research::RoutingIndexManager::NodeIndex pDepot;

  /// Utility function: initializes this model from the give JSON string
  void initModel(const std::string& json);

  /// Initializes the distance matrix
  void initDistanceMatrix(JSONValue::JsonObject& mat);

  /// Initializes demands list
  void initDemands(JSONValue::JsonObject& demands);

  /// Initializes vehicle capacities list
  void initVehicleCapacities(JSONValue::JsonObject& vcap);
};

}  // namespace optilab
