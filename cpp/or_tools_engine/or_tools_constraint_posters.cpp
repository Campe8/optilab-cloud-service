#include "or_tools_engine/or_tools_constraint_posters.hpp"

#include <algorithm>  // for std::max
#include <cassert>
#include <cstdint>    // for int64_t
#include <stdexcept>  // for std::runtime_error
#include <string>
#include <vector>

#include <boost/any.hpp>
#include <sparsepp/spp.h>

#include "engine/engine_constants.hpp"

#include "ortools/constraint_solver/constraint_solver.h"
#include "ortools/constraint_solver/constraint_solveri.h"
#include "or_tools_engine/flatzinc_constraints.hpp"
#include "or_tools_engine/or_tools_constants.hpp"
#include "or_tools_engine/or_tools_cp_constraint.hpp"
#include "or_tools_engine/or_tools_cp_variable.hpp"
#include "utilities/strings.hpp"
#include "utilities/variables.hpp"

#define CON_CAST(c) castSmartPointer<optilab::CPConstraint::SPtr, optilab::ORToolsCPConstraint>(c)
#define VAR_CAST(v) castSmartPointer<optilab::CPVariable::SPtr, optilab::ORToolsCPVariable>(v)

namespace {

template<class From, class To>
To* castSmartPointer(const From& val)
{
  auto dynCast = std::dynamic_pointer_cast<To>(val);
  if (!dynCast)
  {
    throw std::runtime_error("Invalid dynamic cast to ORTools type");
  }

  return dynCast.get();
}  // castSmartPointer

bool isAllDifferent(const std::vector<optilab::CPVariable::SPtr>& array)
{
  std::unordered_set<optilab::CPVariable::SPtr> set(array.begin(), array.end());
  return set.size() == array.size();
}  // isAllDifferent

std::vector<operations_research::IntVar*> extractVariables(
    const std::vector<optilab::CPVariable::SPtr>& array)
{
  std::vector<operations_research::IntVar*> variables;
  for (const auto& var : array)
  {
    auto v = VAR_CAST(var);
    for (auto subvar : v->getVariableList())
    {
      variables.push_back(subvar);
    }
  }
  return variables;
}  // extractVariables

operations_research::IntExpr* getOrCreateExpression(const optilab::CPArgument& arg,
                                                    operations_research::Solver* const solver)
{
  if (arg.hasOneIntValue())
  {
    return solver->MakeIntConst(arg.getIntValue());
  }
  else if (arg.argType == optilab::CPArgument::AT_VAR ||
      arg.argType == optilab::CPArgument::AT_VAR_LIST)
  {
    auto v = VAR_CAST(arg.varList[0]);
    return v->getVariable();
  }
  else
  {
    throw std::runtime_error("Cannot extract an IntExpr from the given argument");
  }
}  // getOrCreateExpression

void postBooleanSumInRange(operations_research::Solver* solver,
                           const std::vector<operations_research::IntVar*>& variables,
                           int64_t range_min, int64_t range_max)
{
  const int64_t size = variables.size();
  range_min = std::max(static_cast<int64_t>(0), range_min);
  range_max = std::min(static_cast<int64_t>(size), range_max);
  int true_vars = 0;
  std::vector<operations_research::IntVar*> alt;
  for (int i = 0; i < size; ++i) {
    if (!variables[i]->Bound()) {
      alt.push_back(variables[i]);
    } else if (variables[i]->Min() == 1) {
      true_vars++;
    }
  }
  const int possible_vars = alt.size();
  range_min -= true_vars;
  range_max -= true_vars;

  if (range_max < 0 || range_min > possible_vars)
  {
    operations_research::Constraint* const ct = solver->MakeFalseConstraint();
    solver->AddConstraint(ct);
  }
  else
  {
    operations_research::Constraint* const ct =
        operations_research::MakeBooleanSumInRange(solver, alt, range_min, range_max);
    solver->AddConstraint(ct);
  }
}  // postBooleanSumInRange

bool areAllExtractedAsVariables(const std::vector<optilab::CPVariable::SPtr>& vars)
{
  for (const auto cpvar : vars) {
    auto v = VAR_CAST(cpvar);
    if (!v->getVariable()->IsVar()) return false;
  }
  return true;
}

bool areAllVariablesBoolean(optilab::ORToolsCPConstraint* ct) {
  for (const auto& cpvar : (ct->getArgumentList())[1].varList) {
    auto v = VAR_CAST(cpvar);
    auto orvar = v->getVariable();
    if (orvar->Min() < 0 || orvar->Max() > 1) {
      return false;
    }
  }
  return true;
}

bool extractLinAsShort(optilab::ORToolsCPConstraint* ct)
{
  const int size = static_cast<int>((ct->getArgumentList())[0].valIntList.size());
  if ((ct->getArgumentList())[1].varList.empty()) {
    // Constant linear scal-prods will be treated correctly by ParseShortLin.
    return true;
  }
  switch (size) {
    case 0:
      return true;
    case 1:
      return true;
    case 2:
    case 3: {
      if (operations_research::AreAllOnes((ct->getArgumentList())[0].valIntList) &&
          areAllExtractedAsVariables((ct->getArgumentList())[1].varList) &&
          areAllVariablesBoolean(ct)) {
        return false;
      } else {
        return true;
      }
    }
    default: {
      return false;
    }
  }
}  // extractLinAsShort

void parseShortIntLin(optilab::ORToolsCPConstraint* ct, operations_research::IntExpr** left,
                      operations_research::IntExpr** right,
                      operations_research::Solver* const solver)
{
  const auto& vars = (ct->getArgumentList())[1].varList;
  const std::vector<int64_t>& coefficients = (ct->getArgumentList())[0].valIntList;
  int64_t rhs = (ct->getArgumentList())[2].getIntValue();
  const int size = static_cast<int>(coefficients.size());
  *left = nullptr;
  *right = nullptr;

  if (vars.empty() && size != 0) {
    // We have a constant array
    if (static_cast<int>((ct->getArgumentList())[1].valIntList.size()) != size)
    {
      throw std::runtime_error("Invalid number of values in int_lin constraint");
    }
    int64_t result = 0;
    for (int i = 0; i < size; ++i) {
      result += coefficients[i] * (ct->getArgumentList())[1].valIntList[i];
    }
    *left = solver->MakeIntConst(result);
    *right = solver->MakeIntConst(rhs);
    return;
  }

  switch (size) {
    case 0: {
      *left = solver->MakeIntConst(0);
      *right = solver->MakeIntConst(rhs);
      break;
    }
    case 1: {
      auto v = VAR_CAST(vars[0]);
      *left = solver->MakeProd(v->getVariable(), coefficients[0]);
      *right = solver->MakeIntConst(rhs);
      break;
    }
    case 2: {
      auto v1 = VAR_CAST(vars[0]);
      auto v2 = VAR_CAST(vars[1]);
      operations_research::IntExpr* const e1 = v1->getVariable();
      operations_research::IntExpr* const e2 = v2->getVariable();
      const int64 c1 = coefficients[0];
      const int64 c2 = coefficients[1];
      if (c1 > 0) {
        if (c2 > 0) {
          *left = solver->MakeProd(e1, c1);
          *right = solver->MakeDifference(rhs, solver->MakeProd(e2, c2));
        } else {
          *left = solver->MakeProd(e1, c1);
          *right = solver->MakeSum(solver->MakeProd(e2, -c2), rhs);
        }
      } else if (c2 > 0) {
        *left = solver->MakeProd(e2, c2);
        *right = solver->MakeSum(solver->MakeProd(e1, -c1), rhs);
      } else {
        *left = solver->MakeDifference(-rhs, solver->MakeProd(e2, -c2));
        *right = solver->MakeProd(e1, -c1);
      }
      break;
    }
    case 3: {
      auto v1 = VAR_CAST(vars[0]);
      auto v2 = VAR_CAST(vars[1]);
      auto v3 = VAR_CAST(vars[2]);
      operations_research::IntExpr* const e1 = v1->getVariable();
      operations_research::IntExpr* const e2 = v2->getVariable();
      operations_research::IntExpr* const e3 = v3->getVariable();
      const int64 c1 = coefficients[0];
      const int64 c2 = coefficients[1];
      const int64 c3 = coefficients[2];
      if (c1 > 0 && c2 > 0 && c3 > 0) {
        *left =
            solver->MakeSum(solver->MakeProd(e1, c1), solver->MakeProd(e2, c2));
        *right = solver->MakeDifference(rhs, solver->MakeProd(e3, c3));

      } else if (c1 < 0 && c2 > 0 && c3 > 0) {
        *left =
            solver->MakeSum(solver->MakeProd(e2, c2), solver->MakeProd(e3, c3));
        *right = solver->MakeSum(solver->MakeProd(e1, -c1), rhs);
      } else if (c1 > 0 && c2 < 0 && c3 < 0) {
        *left = solver->MakeSum(solver->MakeProd(e1, c1), -rhs);
        *right = solver->MakeSum(solver->MakeProd(e2, -c2),
                                 solver->MakeProd(e3, -c3));
      } else if (c1 > 0 && c2 < 0 && c3 > 0) {
        *left =
            solver->MakeSum(solver->MakeProd(e1, c1), solver->MakeProd(e3, c3));
        *right = solver->MakeSum(solver->MakeProd(e2, -c2), rhs);
      } else if (c1 > 0 && c2 > 0 && c3 < 0) {
        *left =
            solver->MakeSum(solver->MakeProd(e1, c1), solver->MakeProd(e2, c2));
        *right = solver->MakeSum(solver->MakeProd(e3, -c3), rhs);
      } else if (c1 < 0 && c2 < 0 && c3 > 0) {
        *left = solver->MakeSum(solver->MakeProd(e3, c3), -rhs);
        *right = solver->MakeSum(solver->MakeProd(e1, -c1),
                                 solver->MakeProd(e2, -c2));
      } else if (c1 < 0 && c2 > 0 && c3 < 0) {
        *left = solver->MakeSum(solver->MakeProd(e2, c2), -rhs);
        *right = solver->MakeSum(solver->MakeProd(e1, -c1),
                                 solver->MakeProd(e3, -c3));
      } else {
        if (c1 > 0 || c2 > 2 || c3 > 0)
        {
          throw std::runtime_error("parseShortIntLin - invalid coefficients");
        }
        *left = solver->MakeDifference(-rhs, solver->MakeProd(e3, -c3));
        *right = solver->MakeSum(solver->MakeProd(e1, -c1),
                                 solver->MakeProd(e2, -c2));
      }
      break;
    }
    default: {
      throw std::runtime_error("int_lin - Too many terms");
    }
  }
}  // parseShortIntLin

void parseLongIntLin(optilab::ORToolsCPConstraint* ct,
                     std::vector<operations_research::IntVar*>* vars,
                     std::vector<int64_t>* coeffs,
                     int64* rhs) {
  if (!vars || !coeffs || !rhs)
  {
    throw std::runtime_error("parseLongIntLin - NULL input pointers");
  }

  const auto& cpvars = (ct->getArgumentList())[1].varList;
  const std::vector<int64_t>& coefficients = (ct->getArgumentList())[0].valIntList;
  *rhs = static_cast<int64>((ct->getArgumentList())[2].valIntList[0]);
  const int size = static_cast<int>(cpvars.size());
  for (int i = 0; i < size; ++i) {
    const int64_t coef = coefficients[i];
    auto v = VAR_CAST(cpvars[i]);
    operations_research::IntVar* const var = v->getVariable();
    if (coef != 0 && (var->Min() != 0 || var->Max() != 0)) {
      if (var->Bound()) {
        (*rhs) -= var->Min() * coef;
      } else {
        coeffs->push_back(coef);
        vars->push_back(var);
      }
    }
  }
}  // parseLongIntLin

}  // namespace

namespace optilab {

namespace ortoolsconstraintposters {

void p_arrayBoolAnd(CPConstraint::SPtr con)
{
  // array_bool_and(as[], r)
  // 2 arguments where:
  // for all i in 1..n as[i] <-> r
  // where n is the length of as
  auto c = CON_CAST(con);
  if (c->getArgumentList().size() != 3)
  {
    throw std::runtime_error("p_arrayBoolAnd - invalid number of arguments: " +
                             std::to_string(c->getArgumentList().size()));
  }

  auto solver = c->getSolver();
  std::vector<operations_research::IntVar*> variables;
  spp::sparse_hash_set<operations_research::IntExpr*> added;
  const auto& varArgs = (c->getArgumentList())[0];
  const auto& valArgs = (c->getArgumentList())[1];

  if (!varArgs.isArgVar())
  {
    throw std::runtime_error("p_arrayBoolAnd - invalid variable argument");
  }

  for (const auto& var : varArgs.varList)
  {
    auto v = VAR_CAST(var);
    for (auto subvar : v->getVariableList())
    {
      if (added.find(subvar) == added.end() && subvar->Min() != 1)
      {
        variables.push_back(subvar);
        added.insert(subvar);
      }
    }
  }

  if (valArgs.hasOneIntValue())
  {
    if (valArgs.getIntValue() == 0)
    {
      // At least one of the variables must be false
      operations_research::Constraint* const constraint =
          solver->MakeSumLessOrEqual(variables, variables.size() - 1);
      solver->AddConstraint(constraint);
    }
    else
    {
      // All variables must be true
      operations_research::Constraint* const constraint =
          solver->MakeSumEquality(variables, variables.size());
      solver->AddConstraint(constraint);
    }
  }
  else
  {
    // The second argument is a variable
    if (!((valArgs.varList.back())->isScalar()))
    {
      throw std::runtime_error("p_arrayBoolAnd - the right-hand side variable is not scalar");
    }

    // The min value on the array must be the same as the right value,
    // i.e., either both are true or there is at least one false
    auto v = VAR_CAST(valArgs.varList.back());
    operations_research::Constraint* const constraint =
        solver->MakeMinEquality(variables, v->getVariable());
    solver->AddConstraint(constraint);
  }
}  // p_arrayBoolAnd

void p_arrayIntElement(CPConstraint::SPtr con)
{
  // array_int_element(b, as[], c)
  // 3 arguments where:
  // b in 1..n as[b] = c
  // where n is the length of as, as is an array of integers, b and c are var int
  auto ortoolsCon = CON_CAST(con);
  if (ortoolsCon->getArgumentList().size() != 3)
  {
    throw std::runtime_error("p_arrayIntElement - invalid number of arguments: " +
                             std::to_string(ortoolsCon->getArgumentList().size()));
  }

  auto solver = ortoolsCon->getSolver();
  const auto& b = ortoolsCon->getArgumentList()[0];
  const auto& as = ortoolsCon->getArgumentList()[1];
  const auto& c = ortoolsCon->getArgumentList()[2];

  operations_research::IntExpr* const index = getOrCreateExpression(b, solver);
  const std::vector<int64_t>& values = as.valIntList;
  const int64_t imin =
      std::max<int64_t>(static_cast<int64_t>(index->Min()), static_cast<int64_t>(1));
  const int64_t imax =
      std::min<int64_t>(static_cast<int64_t>(index->Max()), static_cast<int64_t>(values.size()));
  operations_research::IntVar* const shiftedIndex = solver->MakeSum(index, -imin)->Var();

  const int64_t size = imax - imin + 1;
  std::vector<int64_t> coefficients(size);
  for (int idx = 0; idx < size; ++idx)
  {
    coefficients[idx] = values[idx + imin - 1];
  }

  operations_research::IntVar* const target = getOrCreateExpression(c, solver)->Var();
  operations_research::Constraint* const constraint =
      solver->MakeElementEquality(coefficients, shiftedIndex, target);
  solver->AddConstraint(constraint);
}  // p_arrayIntElement

void p_arrayBoolOr(CPConstraint::SPtr con)
{
  // array_bool_or(as[], r)
  // 2 arguments where:
  // Exists i in 1..n as[i] <-> r
  // where n is the length of as
  auto c = CON_CAST(con);
  if (c->getArgumentList().size() != 2)
  {
    throw std::runtime_error("p_arrayBoolOr - invalid number of arguments: " +
                             std::to_string(c->getArgumentList().size()));
  }

  auto solver = c->getSolver();
  std::vector<operations_research::IntVar*> variables;
  spp::sparse_hash_set<operations_research::IntExpr*> added;
  const auto& varArgs = (c->getArgumentList())[0];
  const auto& valArgs = (c->getArgumentList())[1];

  if (!varArgs.isArgVar())
  {
    throw std::runtime_error("p_arrayBoolOr - invalid variable argument");
  }

  for (const auto& var : varArgs.varList)
  {
    auto v = VAR_CAST(var);
    for (auto subvar : v->getVariableList())
    {
      if (added.find(subvar) == added.end() && subvar->Max() != 0)
      {
        variables.push_back(subvar);
        added.insert(subvar);
      }
    }
  }

  if (valArgs.hasOneIntValue())
  {
    if (valArgs.getIntValue() == 1)
    {
      // At least one of the variables must be true
      operations_research::Constraint* const constraint =
          solver->MakeSumGreaterOrEqual(variables, 1);
      solver->AddConstraint(constraint);
    }
    else
    {
      // All variables must be false
      operations_research::Constraint* const constraint =
          solver->MakeSumEquality(variables, static_cast<int64_t>(0));
      solver->AddConstraint(constraint);
    }
  }
  else
  {
    // The second argument is a variable
    if (!((valArgs.varList.back())->isScalar()))
    {
      throw std::runtime_error("p_arrayBoolOr - the right-hand side variable is not scalar");
    }

    // The min value on the array must be the same as the right value,
    // i.e., either both are true or there is at least one false
    auto v = VAR_CAST(valArgs.varList.back());
    operations_research::Constraint* const constraint =
        solver->MakeMaxEquality(variables, v->getVariable());
    solver->AddConstraint(constraint);
  }
}  // p_arrayBoolOr

void p_arrayBoolXor(CPConstraint::SPtr con)
{
  // array_bool_or(as[])
  // 1 argument where:
  // (Exists i in 1..n as[i] mod 2) = 1
  // where n is the length of as
  auto c = CON_CAST(con);
  if (c->getArgumentList().size() != 1)
  {
    throw std::runtime_error("p_arrayBoolXor - invalid number of arguments: " +
                             std::to_string(c->getArgumentList().size()));
  }

  auto solver = c->getSolver();
  const auto& varArgs = (c->getArgumentList())[0];
  std::vector<operations_research::IntVar*> variables;

  bool even = true;
  for (const auto& var : varArgs.varList)
  {
    auto v = VAR_CAST(var);
    for (auto subvar : v->getVariableList())
    {
      if (subvar->Max() == 1) {
        if (subvar->Min() == 1) {
          even = !even;
        } else {
          variables.push_back(subvar);
        }
      }
    }
  }

  switch (variables.size()) {
    case 0:
    {
      operations_research::Constraint* const constraint =
          even ? solver->MakeFalseConstraint() : solver->MakeTrueConstraint();
      solver->AddConstraint(constraint);
      break;
    }
    case 1:
    {
      operations_research::Constraint* const constraint =
          solver->MakeEquality(variables.front(), even);
      solver->AddConstraint(constraint);
      break;
    }
    case 2:
    {
      if (even)
      {
        postBooleanSumInRange(solver, variables, 1, 1);
      }
      else
      {
        variables.push_back(solver->MakeIntConst(1));
        operations_research::Constraint* const constraint =
            operations_research::MakeBooleanSumOdd(solver, variables);
        solver->AddConstraint(constraint);
      }
      break;
    }
    default:
    {
      if (!even)
      {
        variables.push_back(solver->MakeIntConst(1));
      }
      operations_research::Constraint* const constraint =
          operations_research::MakeBooleanSumOdd(solver, variables);
      solver->AddConstraint(constraint);
    }
  }
}  // p_arrayBoolOr

void p_arrayVarIntElement(CPConstraint::SPtr con)
{
  // array_var_int_element(b, as[], val)
  // 3 arguments where:
  // b in 1..n as[b] = val
  // where n is the length of as, as is an array of var integers, b and c are var int
  auto c = CON_CAST(con);
  operations_research::Solver* const solver = c->getSolver();
  const auto& b = (c->getArgumentList())[0];
  const auto& as = (c->getArgumentList())[1];
  const auto& val = (c->getArgumentList())[2];

  operations_research::IntExpr* const index = getOrCreateExpression(b, solver);
  const int64_t array_size = as.varList.size();
  const int64_t imin =
      std::max<int64_t>(static_cast<int64_t>(index->Min()), static_cast<int64_t>(1));
  const int64_t imax = std::min<int64_t>(static_cast<int64_t>(index->Max()), array_size);
  operations_research::IntVar* const shifted_index = solver->MakeSum(index, -imin)->Var();

  std::vector<operations_research::IntVar*> vars;
  for (const auto& var : as.varList)
  {
    // @note take the full multi-dimensional variable and not subvariables
    // for the var list
    auto v = VAR_CAST(var);
    vars.push_back(v->getVariable());
  }
  const int64_t size = imax - imin + 1;
  std::vector<operations_research::IntVar*> var_array(size);
  for (int i = 0; i < size; ++i) {
    var_array[i] = vars[i + imin - 1];
  }

  operations_research::Constraint* constraint = nullptr;
  if (val.hasOneIntValue())
  {
    const int64_t target = val.getIntValue();
    if (isAllDifferent(as.varList))
    {
      constraint =
          solver->MakeIndexOfConstraint(var_array, shifted_index, target);
    } else {
      constraint =
          solver->MakeElementEquality(var_array, shifted_index, target);
    }
  } else {
    operations_research::IntVar* const target = getOrCreateExpression(val, solver)->Var();
    constraint =
        solver->MakeElementEquality(var_array, shifted_index, target);
  }

  solver->AddConstraint(constraint);
}  // p_arrayVarIntElement

void p_boolAnd(CPConstraint::SPtr con)
{
  // bool_and(a, b, r)
  // (a /\ b) <-> r
  auto c = CON_CAST(con);
  operations_research::Solver* const solver = c->getSolver();
  const auto& a = (c->getArgumentList())[0];
  const auto& b = (c->getArgumentList())[1];
  const auto& r = (c->getArgumentList())[2];

  operations_research::IntExpr* const left = getOrCreateExpression(a, solver);
  operations_research::IntExpr* const right = getOrCreateExpression(b, solver);
  operations_research::IntExpr* const target = getOrCreateExpression(r, solver);

  operations_research::Constraint* const constraint =
      solver->MakeEquality(solver->MakeMin(left, right), target);
  solver->AddConstraint(constraint);
}  // p_boolAnd

void p_boolClause(CPConstraint::SPtr con)
{
  // bool_clause(as[], bs[])
  // (exist i s.t. as[i]) \/ (exist i s.t. !bs[i])
  auto c = CON_CAST(con);
  operations_research::Solver* const solver = c->getSolver();
  const auto& as = (c->getArgumentList())[0];
  const auto& bs = (c->getArgumentList())[1];

  std::vector<operations_research::IntVar*> positive_variables = extractVariables(as.varList);
  std::vector<operations_research::IntVar*> negative_variables = extractVariables(bs.varList);

  std::vector<operations_research::IntVar*> vars;
  for (operations_research::IntVar* const var : positive_variables) {
    if (var->Bound() && var->Min() == 1) {  // True constraint
      solver->AddConstraint(solver->MakeTrueConstraint());
      return;
    } else if (!var->Bound()) {
      vars.push_back(var);
    }
  }

  for (operations_research::IntVar* const var : negative_variables) {
    if (var->Bound() && var->Max() == 0) {  // True constraint
      solver->AddConstraint(solver->MakeTrueConstraint());
      return;
    } else if (!var->Bound()) {
      vars.push_back(solver->MakeDifference(1, var)->Var());
    }
  }

  operations_research::Constraint* const constraint = solver->MakeSumGreaterOrEqual(vars, 1);
  solver->AddConstraint(constraint);
}  // p_boolClause

void p_intEq(CPConstraint::SPtr con)
{
  // int_eq(a, b)
  // a = b
  auto c = CON_CAST(con);
  operations_research::Solver* const solver = c->getSolver();
  const auto& a = (c->getArgumentList())[0];
  const auto& b = (c->getArgumentList())[1];

  if (a.argType == CPArgument::ArgumentType::AT_VAR_LIST ||
      a.argType == CPArgument::ArgumentType::AT_VAR)
  {
    operations_research::IntExpr* const left = getOrCreateExpression(a, solver);
    if (b.argType == CPArgument::ArgumentType::AT_VAR_LIST ||
        b.argType == CPArgument::ArgumentType::AT_VAR)
    {
      operations_research::IntExpr* const right = getOrCreateExpression(b, solver);
      solver->AddConstraint(solver->MakeEquality(left, right));
    }
    else
    {
      const int64 right = static_cast<int64>(b.getIntValue());
      solver->AddConstraint(solver->MakeEquality(left, right));
    }
  } else {
    const int64 left = static_cast<int64>(a.getIntValue());
    if (b.argType == CPArgument::ArgumentType::AT_VAR_LIST ||
        b.argType == CPArgument::ArgumentType::AT_VAR)
    {
      operations_research::IntExpr* const right = getOrCreateExpression(b, solver);
      solver->AddConstraint(solver->MakeEquality(right, left));
    } else {
      const int64 right = static_cast<int64>(b.getIntValue());
      if (left != right) {
        solver->AddConstraint(solver->MakeFalseConstraint());
      }
    }
  }
}  // p_intEq

void p_intEqReif(CPConstraint::SPtr con)
{
  // int_eq_reif(a, b, r)
  // (a = b) <-> r
  auto c = CON_CAST(con);
  operations_research::Solver* const solver = c->getSolver();
  const auto& a = (c->getArgumentList())[0];
  const auto& b = (c->getArgumentList())[1];
  const auto& r = (c->getArgumentList())[2];

  operations_research::IntVar* const boolvar = getOrCreateExpression(r, solver)->Var();
  if (b.hasOneIntValue())
  {
    operations_research::IntExpr* const left = getOrCreateExpression(a, solver);
    const int64 value = static_cast<int64>(b.getIntValue());
    operations_research::Constraint* const constraint =
            solver->MakeIsEqualCstCt(left, value, boolvar);
    solver->AddConstraint(solver->MakeFalseConstraint());
  }
  else if (a.hasOneIntValue())
  {
    operations_research::IntExpr* const right = getOrCreateExpression(b, solver);
    const int64 value = static_cast<int64>(a.getIntValue());
    operations_research::Constraint* const constraint =
            solver->MakeIsEqualCstCt(right, value, boolvar);
    solver->AddConstraint(solver->MakeFalseConstraint());
  }
  else
  {
    operations_research::IntExpr* const left = getOrCreateExpression(a, solver);
    operations_research::IntExpr* const right = getOrCreateExpression(b, solver)->Var();
    operations_research::Constraint* const constraint =
              solver->MakeIsEqualCt(left, right, boolvar);
    solver->AddConstraint(solver->MakeFalseConstraint());
  }
}  // p_intEqReif

void p_intGe(CPConstraint::SPtr con)
{
  // int_ge(a, b)
  // a >= b
  auto c = CON_CAST(con);
  operations_research::Solver* const solver = c->getSolver();
  const auto& a = (c->getArgumentList())[0];
  const auto& b = (c->getArgumentList())[1];

  if (a.argType == CPArgument::ArgumentType::AT_VAR_LIST ||
      a.argType == CPArgument::ArgumentType::AT_VAR)
  {
    operations_research::IntExpr* const left = getOrCreateExpression(a, solver);
    if (b.argType == CPArgument::ArgumentType::AT_VAR_LIST ||
        b.argType == CPArgument::ArgumentType::AT_VAR)
    {
      operations_research::IntExpr* const right = getOrCreateExpression(b, solver);
      solver->AddConstraint(solver->MakeGreaterOrEqual(left, right));
    }
    else
    {
      const int64 right = static_cast<int64>(b.getIntValue());
      solver->AddConstraint(solver->MakeGreaterOrEqual(left, right));
    }
  } else {
    const int64 left = static_cast<int64>(a.getIntValue());
    if (b.argType == CPArgument::ArgumentType::AT_VAR_LIST ||
        b.argType == CPArgument::ArgumentType::AT_VAR)
    {
      operations_research::IntExpr* const right = getOrCreateExpression(b, solver);
      solver->AddConstraint(solver->MakeLessOrEqual(right, left));
    } else {
      const int64 right = static_cast<int64>(b.getIntValue());
      if (left < right) {
        solver->AddConstraint(solver->MakeFalseConstraint());
      }
    }
  }
}  // p_intGe

#define EXTRACT_INT_XX_REIF(Op, Rev)                                        \
  auto c = CON_CAST(con);                                                   \
  operations_research::Solver* const solver = c->getSolver();               \
  const auto& a = (c->getArgumentList())[0];                                \
  const auto& b = (c->getArgumentList())[1];                                \
  const auto& r = (c->getArgumentList())[2];                                \
  operations_research::IntVar* boolvar = nullptr;                           \
  operations_research::Constraint* constraint = nullptr;                    \
  if (a.hasOneIntValue()) {                                                 \
    const int64 left = static_cast<int64>(a.getIntValue());                 \
    operations_research::IntExpr* const right =                             \
      getOrCreateExpression(b, solver);                                     \
    if (right->IsVar()) {                                                   \
      boolvar = solver->MakeIs##Rev##CstVar(right, left);                   \
    } else {                                                                \
      boolvar = getOrCreateExpression(r, solver)->Var();                    \
      constraint = solver->MakeIs##Rev##CstCt(right, left, boolvar);        \
    }                                                                       \
  } else if (b.hasOneIntValue()) {                                          \
    operations_research::IntExpr* const left =                              \
      getOrCreateExpression(a, solver);                                     \
    const int64 right = static_cast<int64>(b.getIntValue());                \
    if (left->IsVar()) {                                                    \
      boolvar = solver->MakeIs##Op##CstVar(left, right);                    \
    } else {                                                                \
      boolvar = getOrCreateExpression(r, solver)->Var();                    \
      constraint = solver->MakeIs##Op##CstCt(left, right, boolvar);         \
    }                                                                       \
  } else {                                                                  \
    operations_research::IntExpr* const left =                              \
      getOrCreateExpression(a, solver);                                     \
    operations_research::IntExpr* const right =                             \
      getOrCreateExpression(b, solver);                                     \
    boolvar = getOrCreateExpression(r, solver)->Var();                      \
    constraint = solver->MakeIs##Op##Ct(left, right, boolvar);              \
  }                                                                         \
  if (constraint != nullptr) {                                              \
    solver->AddConstraint(constraint);                                      \
  } else {                                                                  \
    operations_research::IntVar* const previous =                           \
      getOrCreateExpression(r, solver)->Var();                              \
    operations_research::Constraint* const constraint =                     \
      solver->MakeEquality(boolvar, previous);                              \
    solver->AddConstraint(constraint);                                      \
  }

void p_intGeReif(CPConstraint::SPtr con)
{
  // int_ge_reif(a, b, r)
  // (a >= b) <-> r
  EXTRACT_INT_XX_REIF(GreaterOrEqual, LessOrEqual)
}  // p_intGeReif

void p_intGt(CPConstraint::SPtr con)
{
  // int_gt(a, b)
  // a > b
  auto c = CON_CAST(con);
  operations_research::Solver* const solver = c->getSolver();
  const auto& a = (c->getArgumentList())[0];
  const auto& b = (c->getArgumentList())[1];

  if (a.argType == CPArgument::ArgumentType::AT_VAR_LIST ||
      a.argType == CPArgument::ArgumentType::AT_VAR)
  {
    operations_research::IntExpr* const left = getOrCreateExpression(a, solver);
    if (b.argType == CPArgument::ArgumentType::AT_VAR_LIST ||
        b.argType == CPArgument::ArgumentType::AT_VAR)
    {
      operations_research::IntExpr* const right = getOrCreateExpression(b, solver);
      solver->AddConstraint(solver->MakeGreater(left, right));
    }
    else
    {
      const int64 right = static_cast<int64>(b.getIntValue());
      solver->AddConstraint(solver->MakeGreater(left, right));
    }
  } else {
    const int64 left = static_cast<int64>(a.getIntValue());
    if (b.argType == CPArgument::ArgumentType::AT_VAR_LIST ||
        b.argType == CPArgument::ArgumentType::AT_VAR)
    {
      operations_research::IntExpr* const right = getOrCreateExpression(b, solver);
      solver->AddConstraint(solver->MakeLess(right, left));
    } else {
      const int64 right = static_cast<int64>(b.getIntValue());
      if (left <= right) {
        solver->AddConstraint(solver->MakeFalseConstraint());
      }
    }
  }
}  // p_intGt

void p_intGtReif(CPConstraint::SPtr con)
{
  // int_gt_reif(a, b, r)
  // (a > b) <-> r
  EXTRACT_INT_XX_REIF(Greater, Less);
}  // p_intGtReif

void p_intLe(CPConstraint::SPtr con)
{
  // int_le(a, b)
  // a <= b
  auto c = CON_CAST(con);
  operations_research::Solver* const solver = c->getSolver();
  const auto& a = (c->getArgumentList())[0];
  const auto& b = (c->getArgumentList())[1];

  if (a.argType == CPArgument::ArgumentType::AT_VAR_LIST ||
      a.argType == CPArgument::ArgumentType::AT_VAR)
  {
    operations_research::IntExpr* const left = getOrCreateExpression(a, solver);
    if (b.argType == CPArgument::ArgumentType::AT_VAR_LIST ||
        b.argType == CPArgument::ArgumentType::AT_VAR)
    {
      operations_research::IntExpr* const right = getOrCreateExpression(b, solver);
      solver->AddConstraint(solver->MakeLessOrEqual(left, right));
    }
    else
    {
      const int64 right = static_cast<int64>(b.getIntValue());
      solver->AddConstraint(solver->MakeLessOrEqual(left, right));
    }
  } else {
    const int64 left = static_cast<int64>(a.getIntValue());
    if (b.argType == CPArgument::ArgumentType::AT_VAR_LIST ||
        b.argType == CPArgument::ArgumentType::AT_VAR)
    {
      operations_research::IntExpr* const right = getOrCreateExpression(b, solver);
      solver->AddConstraint(solver->MakeGreaterOrEqual(right, left));
    } else {
      const int64 right = static_cast<int64>(b.getIntValue());
      if (left > right) {
        solver->AddConstraint(solver->MakeFalseConstraint());
      }
    }
  }
}  // p_intLe

void p_intLeReif(CPConstraint::SPtr con)
{
  // int_gt_reif(a, b, r)
  // (a > b) <-> r
  EXTRACT_INT_XX_REIF(LessOrEqual, GreaterOrEqual)
}  // p_intLeReif

void p_intLt(CPConstraint::SPtr con)
{
  // int_lt(a, b)
  // a < b
  auto c = CON_CAST(con);
  operations_research::Solver* const solver = c->getSolver();
  const auto& a = (c->getArgumentList())[0];
  const auto& b = (c->getArgumentList())[1];

  if (a.argType == CPArgument::ArgumentType::AT_VAR_LIST ||
      a.argType == CPArgument::ArgumentType::AT_VAR)
  {
    operations_research::IntExpr* const left = getOrCreateExpression(a, solver);
    if (b.argType == CPArgument::ArgumentType::AT_VAR_LIST ||
        b.argType == CPArgument::ArgumentType::AT_VAR)
    {
      operations_research::IntExpr* const right = getOrCreateExpression(b, solver);
      solver->AddConstraint(solver->MakeLess(left, right));
    }
    else
    {
      const int64 right = static_cast<int64>(b.getIntValue());
      solver->AddConstraint(solver->MakeLess(left, right));
    }
  } else {
    const int64 left = static_cast<int64>(a.getIntValue());
    if (b.argType == CPArgument::ArgumentType::AT_VAR_LIST ||
        b.argType == CPArgument::ArgumentType::AT_VAR)
    {
      operations_research::IntExpr* const right = getOrCreateExpression(b, solver);
      solver->AddConstraint(solver->MakeGreater(right, left));
    } else {
      const int64 right = static_cast<int64>(b.getIntValue());
      if (left > right) {
        solver->AddConstraint(solver->MakeFalseConstraint());
      }
    }
  }
}  // p_intLt

void p_intLtReif(CPConstraint::SPtr con)
{
  // int_lt_reif(a, b, r)
  // (a < b) <-> r
  EXTRACT_INT_XX_REIF(Less, Greater)
}  // p_intLeReif

#undef EXTRACT_INT_XX_REIF

void p_intLinEq(CPConstraint::SPtr con)
{
  // exists i in 1..n as[i] * bs[i] = val
  // int_lin_eq(as[], bs[], val)
  // where as is a list of int coefficients and bs a list of int variables
  auto c = CON_CAST(con);
  operations_research::Solver* const solver = c->getSolver();
  int64 rhs = static_cast<int64>((c->getArgumentList())[2].getIntValue());
  operations_research::Constraint* constraint = nullptr;
  if (extractLinAsShort(c)) {
    operations_research::IntExpr* left = nullptr;
    operations_research::IntExpr* right = nullptr;
    parseShortIntLin(c, &left, &right, solver);
    constraint = solver->MakeEquality(left, right);
  } else {
    std::vector<operations_research::IntVar*> vars;
    std::vector<int64_t> coeffs;
    parseLongIntLin(c, &vars, &coeffs, &rhs);
    constraint = solver->MakeScalProdEquality(vars, coeffs, rhs);
  }
  solver->AddConstraint(constraint);
}  // p_intLinEq

void p_intLinLe(CPConstraint::SPtr con)
{
  // exists i in 1..n as[i] * bs[i] <= val
  // int_lin_le(as[], bs[], val)
  // where as is a list of int coefficients and bs a list of int variables
  auto c = CON_CAST(con);
  operations_research::Solver* const solver = c->getSolver();
  operations_research::Constraint* constraint = nullptr;
  if (extractLinAsShort(c)) {
    operations_research::IntExpr* left = nullptr;
    operations_research::IntExpr* right = nullptr;
    parseShortIntLin(c, &left, &right, solver);
    parseShortIntLin(c, &left, &right, solver);
    constraint = solver->MakeLessOrEqual(left, right);
  } else {
    std::vector<operations_research::IntVar*> vars;
    std::vector<int64_t> coeffs;
    int64 rhs = 0;
    parseLongIntLin(c, &vars, &coeffs, &rhs);
    constraint = solver->MakeScalProdLessOrEqual(vars, coeffs, rhs);
    solver->AddConstraint(constraint);
  }
}  // p_intLinLe

void p_intNe(CPConstraint::SPtr con)
{
  // int_ne(a, b)
  // a != b
  auto c = CON_CAST(con);
  operations_research::Solver* const solver = c->getSolver();
  const auto& a = (c->getArgumentList())[0];
  const auto& b = (c->getArgumentList())[1];

  if (a.argType == CPArgument::ArgumentType::AT_VAR_LIST ||
      a.argType == CPArgument::ArgumentType::AT_VAR)
  {
    operations_research::IntExpr* const left = getOrCreateExpression(a, solver);
    if (b.argType == CPArgument::ArgumentType::AT_VAR_LIST ||
        b.argType == CPArgument::ArgumentType::AT_VAR)
    {
      operations_research::IntExpr* const right = getOrCreateExpression(b, solver);
      solver->AddConstraint(solver->MakeNonEquality(left, right));
    }
    else
    {
      const int64 right = static_cast<int64>(b.getIntValue());
      solver->AddConstraint(solver->MakeNonEquality(left, right));
    }
  } else {
    const int64 left = static_cast<int64>(a.getIntValue());
    if (b.argType == CPArgument::ArgumentType::AT_VAR_LIST ||
        b.argType == CPArgument::ArgumentType::AT_VAR)
    {
      operations_research::IntExpr* const right = getOrCreateExpression(b, solver);
      solver->AddConstraint(solver->MakeNonEquality(right, left));
    } else {
      const int64 right = static_cast<int64>(b.getIntValue());
      if (left == right) {
        solver->AddConstraint(solver->MakeFalseConstraint());
      }
    }
  }
}  // p_intNe

void p_intNeReif(CPConstraint::SPtr con)
{
  // int_ne_reif(a, b, r)
  // (a != b) <-> r
  auto c = CON_CAST(con);
  operations_research::Solver* const solver = c->getSolver();
  const auto& a = (c->getArgumentList())[0];
  const auto& b = (c->getArgumentList())[1];
  const auto& r = (c->getArgumentList())[2];
  operations_research::IntExpr* const left = getOrCreateExpression(a, solver);
  operations_research::IntExpr* const right = getOrCreateExpression(b, solver);
  operations_research::IntVar* const boolvar = getOrCreateExpression(r, solver)->Var();
  operations_research::Constraint* const constraint =
      solver->MakeIsDifferentCt(left, right, boolvar);
  solver->AddConstraint(constraint);
}  // p_intNeReif

void p_intAbs(CPConstraint::SPtr con)
{
  // int_abs(a, b)
  // |a| = b
  auto c = CON_CAST(con);
  operations_research::Solver* const solver = c->getSolver();
  const auto& a = (c->getArgumentList())[0];
  const auto& b = (c->getArgumentList())[1];

  operations_research::IntExpr* const left = getOrCreateExpression(a, solver);
  if (b.argType == CPArgument::ArgumentType::AT_VAR_LIST ||
      b.argType == CPArgument::ArgumentType::AT_VAR)
  {
    operations_research::IntExpr* const target = getOrCreateExpression(b, solver);
    solver->AddConstraint(solver->MakeAbsEquality(left->Var(), target->Var()));
  }
  else
  {
    const int64_t value = b.getIntValue();
    std::vector<int64_t> values(2);
    values[0] = value;
    values[1] = -value;
    solver->AddConstraint(solver->MakeMemberCt(left, values));
  }
}  // p_intDiv

void p_intDiv(CPConstraint::SPtr con)
{
  // int_ne_reif(a, b, r)
  // (a != b) <-> r
  auto c = CON_CAST(con);
  operations_research::Solver* const solver = c->getSolver();
  const auto& a = (c->getArgumentList())[0];
  const auto& b = (c->getArgumentList())[1];
  const auto& r = (c->getArgumentList())[2];
  operations_research::IntExpr* const left = getOrCreateExpression(a, solver);
  operations_research::IntExpr* const target = getOrCreateExpression(r, solver);
  if (b.argType == CPArgument::ArgumentType::AT_VAR_LIST ||
      b.argType == CPArgument::ArgumentType::AT_VAR)
  {
    operations_research::IntExpr* const right = getOrCreateExpression(b, solver);
    solver->AddConstraint(solver->MakeEquality(solver->MakeDiv(left, right), target));
  }
  else
  {
    const int64 right = static_cast<int64>(b.getIntValue());
    solver->AddConstraint(solver->MakeEquality(solver->MakeDiv(left, right), target));
  }
}  // p_intDiv

void p_boolNot(CPConstraint::SPtr con) {
  // bool_not(a, b)
  // !a = b
  auto c = CON_CAST(con);
  operations_research::Solver* const solver = c->getSolver();
  const auto& a = (c->getArgumentList())[0];
  const auto& b = (c->getArgumentList())[1];
  operations_research::IntExpr* const left = getOrCreateExpression(a, solver);
  operations_research::IntExpr* const right = getOrCreateExpression(b, solver);
  operations_research::Constraint* const constraint =
      solver->MakeEquality(solver->MakeDifference(1, left), right);
  solver->AddConstraint(constraint);
}  // p_boolNot

void p_boolOr(CPConstraint::SPtr con) {
  // bool_or(a, b, r)
  // (a \/ b) <-> r
  auto c = CON_CAST(con);
  operations_research::Solver* const solver = c->getSolver();
  const auto& a = (c->getArgumentList())[0];
  const auto& b = (c->getArgumentList())[1];
  const auto& r = (c->getArgumentList())[2];
  operations_research::IntExpr* const left = getOrCreateExpression(a, solver);
  operations_research::IntExpr* const right = getOrCreateExpression(b, solver);
  operations_research::IntExpr* const target = getOrCreateExpression(r, solver);
  operations_research::Constraint* const constraint =
      solver->MakeEquality(solver->MakeMax(left, right), target);
  solver->AddConstraint(constraint);
}  // p_boolOr

void p_boolXor(CPConstraint::SPtr con) {
  // bool_xor(a, b, r)
  // (a != b) <-> r
  auto c = CON_CAST(con);
  operations_research::Solver* const solver = c->getSolver();
  const auto& a = (c->getArgumentList())[0];
  const auto& b = (c->getArgumentList())[1];
  const auto& r = (c->getArgumentList())[2];
  operations_research::IntExpr* const left = getOrCreateExpression(a, solver);
  operations_research::IntExpr* const right = getOrCreateExpression(b, solver);
  operations_research::IntVar* const target = getOrCreateExpression(r, solver)->Var();
  operations_research::Constraint* const constraint =
      solver->MakeIsEqualCstCt(solver->MakeSum(left, right), 1, target);
  solver->AddConstraint(constraint);
}  // p_boolXor

void p_boolAbs(CPConstraint::SPtr con) {
  // int_abs(a, b)
  // |a| = b
  auto c = CON_CAST(con);
  operations_research::Solver* const solver = c->getSolver();
  const auto& a = (c->getArgumentList())[0];
  const auto& b = (c->getArgumentList())[1];
  operations_research::IntExpr* const left = getOrCreateExpression(a, solver);
  if (b.hasOneIntValue())
  {
    const int64_t value = b.getIntValue();
    std::vector<int64_t> values(2);
    values[0] = -value;
    values[1] = value;
    operations_research::Constraint* const constraint = solver->MakeMemberCt(left, values);
    solver->AddConstraint(constraint);
  }
  else
  {
    operations_research::IntExpr* const target = getOrCreateExpression(b, solver);
    operations_research::Constraint* const constraint =
        solver->MakeAbsEquality(left->Var(), target->Var());
    solver->AddConstraint(constraint);
  }
}  // p_boolAbs

void p_boolDiv(CPConstraint::SPtr con) {
  // int_div(a, b, r)
  // a/b = r rounding towards zero
  auto c = CON_CAST(con);
  operations_research::Solver* const solver = c->getSolver();
  const auto& a = (c->getArgumentList())[0];
  const auto& b = (c->getArgumentList())[1];
  const auto& r = (c->getArgumentList())[2];
  operations_research::IntExpr* const left = getOrCreateExpression(a, solver);
  operations_research::IntExpr* const target = getOrCreateExpression(r, solver);
  if (b.hasOneIntValue())
  {
    const int64_t value = b.getIntValue();
    operations_research::Constraint* const constraint =
        solver->MakeEquality(solver->MakeDiv(left, value), target);
    solver->AddConstraint(constraint);
  }
  else
  {
    operations_research::IntExpr* const right = getOrCreateExpression(b, solver);
    operations_research::Constraint* const constraint =
        solver->MakeEquality(solver->MakeDiv(left, right), target);
    solver->AddConstraint(constraint);
  }
}  // p_boolDiv

void p_intLinEqReif(CPConstraint::SPtr con)
{
  // (exists i in 1..n as[i] * bs[i] = val) <-> r
  // int_lin_eq_reif(as[], bs[], val, r)
  // where as is a list of int coefficients and bs a list of int variables
  auto c = CON_CAST(con);
  operations_research::Solver* const solver = c->getSolver();
  operations_research::Constraint* constraint = nullptr;
  if (extractLinAsShort(c)) {
    operations_research::IntExpr* left = nullptr;
    operations_research::IntExpr* right = nullptr;
    parseShortIntLin(c, &left, &right, solver);
    parseShortIntLin(c, &left, &right, solver);

    const auto& r = (c->getArgumentList())[3];
    operations_research::IntVar* const boolvar =
        getOrCreateExpression(r, solver)->Var();
    operations_research::Constraint* const constraint =
        solver->MakeIsEqualCt(left, right, boolvar);
    solver->AddConstraint(constraint);
  } else {
    std::vector<operations_research::IntVar*> vars;
    std::vector<int64_t> coeffs;
    int64 rhs = 0;
    parseLongIntLin(c, &vars, &coeffs, &rhs);

    const auto& r = (c->getArgumentList())[3];
    operations_research::IntVar* const boolvar =
        getOrCreateExpression(r, solver)->Var();
    operations_research::Constraint* const constraint = solver->MakeIsEqualCstCt(
              solver->MakeScalProd(vars, coeffs), rhs, boolvar);
    solver->AddConstraint(constraint);
  }
}  // p_intLinEqReif

void p_intLinGe(CPConstraint::SPtr con)
{
  // exists i in 1..n as[i] * bs[i] >= val
  // int_lin_eq_reif(as[], bs[], val)
  // where as is a list of int coefficients and bs a list of int variables
  auto c = CON_CAST(con);
  operations_research::Solver* const solver = c->getSolver();
  const auto& as = (c->getArgumentList())[0];
  const int size = static_cast<int>(as.valIntList.size());
  if (extractLinAsShort(c)) {
    operations_research::IntExpr* left = nullptr;
    operations_research::IntExpr* right = nullptr;
    parseShortIntLin(c, &left, &right, solver);
    solver->AddConstraint(solver->MakeGreaterOrEqual(left, right));
  } else {
    std::vector<operations_research::IntVar*> vars;
    std::vector<int64_t> coeffs;
    int64 rhs = 0;
    parseLongIntLin(c, &vars, &coeffs, &rhs);
    solver->AddConstraint(solver->MakeScalProdGreaterOrEqual(vars, coeffs, rhs));
  }
}  // p_intLinGe

void p_intLinGeReif(CPConstraint::SPtr con)
{
  // (exists i in 1..n as[i] * bs[i] >= val) <-> r
  // int_lin_eq_reif(as[], bs[], val, r)
  // where as is a list of int coefficients and bs a list of int variables
  auto c = CON_CAST(con);
  operations_research::Solver* const solver = c->getSolver();
  if (extractLinAsShort(c)) {
    operations_research::IntExpr* left = nullptr;
    operations_research::IntExpr* right = nullptr;
    parseShortIntLin(c, &left, &right, solver);
    const auto& r = (c->getArgumentList())[3];
    operations_research::IntVar* const boolvar =
        getOrCreateExpression(r, solver)->Var();
    operations_research::Constraint* const constraint =
        solver->MakeIsGreaterOrEqualCt(left, right, boolvar);
    solver->AddConstraint(constraint);
  } else {
    std::vector<operations_research::IntVar*> vars;
    std::vector<int64_t> coeffs;
    int64 rhs = 0;
    parseLongIntLin(c, &vars, &coeffs, &rhs);
    const auto& r = (c->getArgumentList())[3];
    operations_research::IntVar* const boolvar =
        getOrCreateExpression(r, solver)->Var();
    operations_research::Constraint* const constraint = solver->MakeIsGreaterOrEqualCstCt(
                solver->MakeScalProd(vars, coeffs), rhs, boolvar);
    solver->AddConstraint(constraint);
  }
}  // p_intLinGeReif

void p_intLinLeReif(CPConstraint::SPtr con)
{
  // (exists i in 1..n as[i] * bs[i] <= val) <-> r
  // int_lin_le_reif(as[], bs[], val, r)
  // where as is a list of int coefficients and bs a list of int variables
  auto c = CON_CAST(con);
  operations_research::Solver* const solver = c->getSolver();
  if (extractLinAsShort(c)) {
    operations_research::IntExpr* left = nullptr;
    operations_research::IntExpr* right = nullptr;
    parseShortIntLin(c, &left, &right, solver);
    const auto& r = (c->getArgumentList())[3];
    operations_research::IntVar* const boolvar =
        getOrCreateExpression(r, solver)->Var();
    operations_research::Constraint* const constraint =
        solver->MakeIsLessOrEqualCt(left, right, boolvar);
    solver->AddConstraint(constraint);
  } else {
    std::vector<operations_research::IntVar*> vars;
    std::vector<int64_t> coeffs;
    int64 rhs = 0;
    parseLongIntLin(c, &vars, &coeffs, &rhs);
    const auto& r = (c->getArgumentList())[3];
    operations_research::IntVar* const boolvar =
        getOrCreateExpression(r, solver)->Var();
    operations_research::Constraint* const constraint = solver->MakeIsLessOrEqualCstCt(
                solver->MakeScalProd(vars, coeffs), rhs, boolvar);
    solver->AddConstraint(constraint);
  }
}  // p_intLinLeReif

void p_intLinNe(CPConstraint::SPtr con)
{
  // exists i in 1..n as[i] * bs[i] != val
  // int_lin_ne(as[], bs[], val)
  // where as is a list of int coefficients and bs a list of int variables
  auto c = CON_CAST(con);
  operations_research::Solver* const solver = c->getSolver();
  if (extractLinAsShort(c)) {
    operations_research::IntExpr* left = nullptr;
    operations_research::IntExpr* right = nullptr;
    parseShortIntLin(c, &left, &right, solver);
    solver->AddConstraint(solver->MakeNonEquality(left, right));
  } else {
    std::vector<operations_research::IntVar*> vars;
    std::vector<int64_t> coeffs;
    int64 rhs = 0;
    parseLongIntLin(c, &vars, &coeffs, &rhs);
    solver->AddConstraint(
        solver->MakeNonEquality(solver->MakeScalProd(vars, coeffs), rhs));
  }
}  // p_intLinNe

void p_intLinNeReif(CPConstraint::SPtr con)
{
  // (exists i in 1..n as[i] * bs[i] != val) <-> r
  // int_lin_ne_reif(as[], bs[], val, r)
  // where as is a list of int coefficients and bs a list of int variables
  auto c = CON_CAST(con);
  operations_research::Solver* const solver = c->getSolver();
  if (extractLinAsShort(c)) {
    operations_research::IntExpr* left = nullptr;
    operations_research::IntExpr* right = nullptr;
    parseShortIntLin(c, &left, &right, solver);
    const auto& r = (c->getArgumentList())[3];
    operations_research::IntVar* const boolvar =
        getOrCreateExpression(r, solver)->Var();
    operations_research::Constraint* const constraint =
        solver->MakeIsDifferentCt(left, right, boolvar);
    solver->AddConstraint(constraint);
  } else {
    std::vector<operations_research::IntVar*> vars;
    std::vector<int64_t> coeffs;
    int64 rhs = 0;
    parseLongIntLin(c, &vars, &coeffs, &rhs);
    const auto& r = (c->getArgumentList())[3];
    operations_research::IntVar* const boolvar =
        getOrCreateExpression(r, solver)->Var();
    operations_research::Constraint* const constraint = solver->MakeIsDifferentCstCt(
        solver->MakeScalProd(vars, coeffs), rhs, boolvar);
    solver->AddConstraint(constraint);
  }
}  // p_intLinNeReif

void p_intMax(CPConstraint::SPtr con) {
  // max(a, b) = m
  // int_max(a, b, m)
  auto c = CON_CAST(con);
  operations_research::Solver* const solver = c->getSolver();
  const auto& a = (c->getArgumentList())[0];
  const auto& b = (c->getArgumentList())[1];
  const auto& m = (c->getArgumentList())[2];
  operations_research::IntExpr* const left = getOrCreateExpression(a, solver);
  operations_research::IntExpr* const right = getOrCreateExpression(b, solver);
  operations_research::IntExpr* const target = getOrCreateExpression(m, solver);
  operations_research::Constraint* const constraint =
      solver->MakeEquality(solver->MakeMax(left, right), target);
  solver->AddConstraint(constraint);
}  // p_intMax

void p_intMin(CPConstraint::SPtr con) {
  // min(a, b) = m
  // int_max(a, b, m)
  auto c = CON_CAST(con);
  operations_research::Solver* const solver = c->getSolver();
  const auto& a = (c->getArgumentList())[0];
  const auto& b = (c->getArgumentList())[1];
  const auto& m = (c->getArgumentList())[2];
  operations_research::IntExpr* const left = getOrCreateExpression(a, solver);
  operations_research::IntExpr* const right = getOrCreateExpression(b, solver);
  operations_research::IntExpr* const target = getOrCreateExpression(m, solver);
  operations_research::Constraint* const constraint =
      solver->MakeEquality(solver->MakeMin(left, right), target);
  solver->AddConstraint(constraint);
}  // p_intMin

void p_intMinus(CPConstraint::SPtr con) {
  // a - b = m
  // int_minus(a, b, m)
  auto c = CON_CAST(con);
  operations_research::Solver* const solver = c->getSolver();
  const auto& a = (c->getArgumentList())[0];
  const auto& b = (c->getArgumentList())[1];
  const auto& m = (c->getArgumentList())[2];
  operations_research::IntExpr* const left = getOrCreateExpression(a, solver);
  operations_research::IntExpr* const right = getOrCreateExpression(b, solver);
  operations_research::IntExpr* const target = getOrCreateExpression(m, solver);
  operations_research::Constraint* const constraint =
      solver->MakeEquality(solver->MakeDifference(left, right), target);
  solver->AddConstraint(constraint);
}  // p_intMinus

void p_intMod(CPConstraint::SPtr con) {
  // a - x * b = m where x = a/b rounding towards zero
  // int_mod(a, b, m)
  auto c = CON_CAST(con);
  operations_research::Solver* const solver = c->getSolver();
  const auto& a = (c->getArgumentList())[0];
  const auto& b = (c->getArgumentList())[1];
  const auto& m = (c->getArgumentList())[2];
  operations_research::IntExpr* const left = getOrCreateExpression(a, solver);
  if (m.hasOneIntValue())
  {
    const int64 target = m.getIntValue();
    if (!b.hasOneIntValue())
    {
      operations_research::IntExpr* const mod = getOrCreateExpression(b, solver);
      operations_research::Constraint* const constraint =
          operations_research::MakeFixedModulo(solver, left->Var(), mod->Var(), target);
      solver->AddConstraint(constraint);
    }
    else
    {
      const int64 mod = b.getIntValue();
      operations_research::Constraint* constraint = nullptr;
      if (mod == 2) {
        switch (target) {
          case 0: {
            constraint = MakeVariableEven(solver, left->Var());
            break;
          }
          case 1: {
            constraint = MakeVariableOdd(solver, left->Var());
            break;
          }
          default: {
            constraint = solver->MakeFalseConstraint();
            break;
          }
        }
      } else {
        constraint =
            solver->MakeEquality(solver->MakeModulo(left, mod), target);
      }
      solver->AddConstraint(constraint);
    }
  }
  else
  {
    operations_research::IntExpr* const target = getOrCreateExpression(m, solver);
    if (!b.hasOneIntValue())
    {
      operations_research::IntExpr* const mod = getOrCreateExpression(b, solver);
      operations_research::Constraint* const constraint =
          solver->MakeEquality(solver->MakeModulo(left, mod), target);
      solver->AddConstraint(constraint);
    }
    else
    {
      const int64 mod = b.getIntValue();
      operations_research::Constraint* const constraint =
          solver->MakeEquality(solver->MakeModulo(left, mod), target);
      solver->AddConstraint(constraint);
    }
  }
}  // p_intMod

void p_intPlus(CPConstraint::SPtr con) {
  // a + b = m
  // int_plus(a, b, m)
  auto c = CON_CAST(con);
  operations_research::Solver* const solver = c->getSolver();
  const auto& a = (c->getArgumentList())[0];
  const auto& b = (c->getArgumentList())[1];
  const auto& m = (c->getArgumentList())[2];
  operations_research::IntExpr* const left = getOrCreateExpression(a, solver);
  operations_research::IntExpr* const right = getOrCreateExpression(b, solver);
  operations_research::IntExpr* const target = getOrCreateExpression(m, solver);
  operations_research::Constraint* const constraint =
      solver->MakeEquality(solver->MakeSum(left, right), target);
  solver->AddConstraint(constraint);
}  // p_intPlus

void p_intNegate(CPConstraint::SPtr con) {
  // b = -a
  // int_negate(a, b)
  auto c = CON_CAST(con);
  operations_research::Solver* const solver = c->getSolver();
  const auto& a = (c->getArgumentList())[0];
  const auto& b = (c->getArgumentList())[1];
  operations_research::IntExpr* const left = getOrCreateExpression(a, solver);
  operations_research::IntExpr* const target = getOrCreateExpression(b, solver);
  operations_research::Constraint* const constraint =
      solver->MakeEquality(solver->MakeOpposite(left), target);
  solver->AddConstraint(constraint);
}  // p_intNegate

void p_intTimes(CPConstraint::SPtr con) {
  // a * b = m
  // int_times(a, b, m)
  auto c = CON_CAST(con);
  operations_research::Solver* const solver = c->getSolver();
  const auto& a = (c->getArgumentList())[0];
  const auto& b = (c->getArgumentList())[1];
  const auto& m = (c->getArgumentList())[2];
  operations_research::IntExpr* const left = getOrCreateExpression(a, solver);
  operations_research::IntExpr* const right = getOrCreateExpression(b, solver);
  operations_research::IntExpr* const target = getOrCreateExpression(m, solver);
  operations_research::Constraint* const constraint =
      solver->MakeEquality(solver->MakeProd(left, right), target);
  solver->AddConstraint(constraint);
}  // p_intTimes

}  // ortoolsconstraintposters

}  // namespace optilab
