//
// Copyright OptiLab 2019. All rights reserved.
//
// Processor for OR-Tools pipelines.
//

#pragma once

#include <memory>  // for std::shared_ptr
#include <string>

#include "engine/optimizer_processor.hpp"
#include "optilab_protobuf/optimizer_model.pb.h"
#include "or_tools_engine/or_tools_utilities.hpp"
#include "system/system_export_defs.hpp"

namespace optilab
{
class SYS_EXPORT_CLASS ORToolsProcessor
    : public OptimizerProcessor<ortoolsengine::ORToolsWork::SPtr,
                                ortoolsengine::ORToolsWork::SPtr>
{
 public:
  using SPtr = std::shared_ptr<ORToolsProcessor>;

 public:
  ORToolsProcessor(const std::string& procName,
                   ortoolsengine::ORToolsResult::SPtr result)
      : BaseProcessor(procName), pResult(result)
  {
    if (!pResult)
    {
      throw std::runtime_error("OrToolsProcessor - empty pointer to result");
    }
  }

  /// Loads the protobuf model and creates a solver to run on it.
  /// @note throw std::invalid_argument on empty model
  virtual void loadModelAndCreateSolver(const OptimizerModel& protoModel){};

  /// Loads the model and creates a solver to run on it.
  /// @note throw std::invalid_argument on empty model
  virtual void loadModelAndCreateSolver(const Model::SPtr& model){};

  /// Loads the JSON model and creates a solver to run on it.
  /// @note throw std::invalid_argument on empty model
  virtual void loadJSONModelAndCreateSolver(const std::string& jsonModel){};

  /// Interrupts the internal solving process (if running).
  /// The solver remains in a non-executable state until another model is
  /// loaded. Returns true on success, false otherwise
  virtual bool interruptSolver() = 0;

 protected:
  inline ortoolsengine::ORToolsResult::SPtr getResult() const
  {
    return pResult;
  }

 private:
  using Work = ortoolsengine::ORToolsWork::SPtr;
  using BaseProcessor = OptimizerProcessor<Work, Work>;

 private:
  /// CP result, i.e., the actual assignment of variables
  ortoolsengine::ORToolsResult::SPtr pResult;
};

}  // namespace optilab
