#include "or_tools_engine/or_tools_mip_processor.hpp"

#include <spdlog/spdlog.h>

#include <exception>
#include <stdexcept>  // for std::runtime_error

#include "data_structure/metrics/metrics_register.hpp"
#include "engine/engine_constants.hpp"
#include "optilab_protobuf/linear_model.pb.h"
#include "or_tools_engine/or_tools_mip_objective.hpp"
#include "or_tools_engine/or_tools_mip_variable.hpp"
#include "solver/solver_model.hpp"
#include "utilities/timer.hpp"

namespace optilab
{
OrToolsMIPProcessor::OrToolsMIPProcessor(
    ortoolsengine::ORToolsResult::SPtr result)
    : ORToolsProcessor("OrToolsMIPProcessor", result),
      pMIPModel(nullptr),
      pMIPSolver(nullptr)
{
  pMIPResult =
      std::dynamic_pointer_cast<ortoolsengine::ORToolsMIPResult>(getResult());
  if (!pMIPResult)
  {
    throw std::runtime_error(
        "OrToolsMIPProcessor - invalid downcast to ORToolsMIPResult");
  }
}

void OrToolsMIPProcessor::loadModelAndCreateSolver(
    const OptimizerModel& protoModel)
{
  if (!protoModel.has_linear_model())
  {
    const std::string errMsg =
        "OrToolsMIPProcessor - loadModelAndCreateSolver: "
        "protobuf linear model expected";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  // Create a new MIP model from the given input model
  pMIPModel = std::make_shared<ORToolsMIPModel>(protoModel.linear_model());
  try
  {
    pMIPModel->initializeModel(protoModel.linear_model());
  }
  catch (const std::exception& ex)
  {
    const std::string errMsg =
        "OrToolsMIPProcessor - loadModelAndCreateSolver: "
        "error parsing and initializing the protobuf model " +
        std::string(ex.what());
    spdlog::error(errMsg);
    throw;
  }
  catch (...)
  {
    const std::string errMsg =
        "pMIPModel - loadModelAndCreateSolver: "
        "error parsing and initializing the protobuf model";
    spdlog::error(errMsg);
    throw;
  }

  // Build a MIP solver
  buildSolver();

}  // loadModelAndCreateSolver

void OrToolsMIPProcessor::loadModelAndCreateSolver(const Model::SPtr& model)
{
  if (!model)
  {
    throw std::runtime_error("OrToolsMIPProcessor - loadModel: empty model");
  }

  // Create a new OR-Tools MIP model from the given input model
  pMIPModel = std::make_shared<ORToolsMIPModel>(model);

  // Set the name of the model
  pMIPResult->modelName = pMIPModel->getModelName();

  // Parse and initialize the model
  try
  {
    pMIPModel->initializeModel();
  }
  catch (const std::exception& ex)
  {
    const std::string errMsg =
        "OrToolsMIPProcessor - loadModelAndCreateSolver: "
        "error parsing and initializing the model " +
        std::string(ex.what());
    spdlog::error(errMsg);
    throw;
  }
  catch (...)
  {
    const std::string errMsg =
        "OrToolsMIPProcessor - loadModelAndCreateSolver: "
        "undefined error parsing and initializing the model";
    spdlog::error(errMsg);
    throw;
  }

  // Build a MIP solver
  buildSolver();
}  // loadModelAndCreateSolver

void OrToolsMIPProcessor::buildSolver()
{
  assert(pMIPModel);

  try
  {
    pMIPSolver = std::make_shared<ORToolsMIPSolver>(pMIPModel);
  }
  catch (const std::exception& ex)
  {
    const std::string errMsg =
        "OrToolsMIPProcessor - loadModelAndCreateSolver: "
        "cannot build the MIP solver " +
        std::string(ex.what());
    spdlog::error(errMsg);
    throw;
  }
  catch (...)
  {
    const std::string errMsg =
        "OrToolsMIPProcessor - loadModelAndCreateSolver: "
        "undefined error in building the MIP solver";
    spdlog::error(errMsg);
    throw;
  }
}  // buildSolver

bool OrToolsMIPProcessor::interruptSolver()
{
  if (!pMIPSolver)
  {
    return false;
  }

  return pMIPSolver->interruptSearchProcess();
}  // interruptSolver

void OrToolsMIPProcessor::processWork(Work work)
{
  if (work->workType == ortoolsengine::ORToolsWork::kRunOptimizer)
  {
    runSolver(work);
  }
  else
  {
    spdlog::warn("OrToolsMIPProcessor - processWork: unrecognized work type");
  }
}  // processWork

void OrToolsMIPProcessor::runSolver(Work& work)
{
  timer::Timer timer;

  // Initialize the search
  pMIPSolver->initSearch();

  // Run the solver
  pMIPSolver->solve();

  const auto wallclockMsec = timer.getWallClockTimeMsec();
  work->metricsRegister->setMetric(jsonmetrics::OPTIMIZER_LATENCY_MSEC,
                                   wallclockMsec);

  // Store the solution found
  storeSolution();

  // Cleanup the search
  pMIPSolver->cleanupSearch();
}  // runSolver

void OrToolsMIPProcessor::storeSolution()
{
  // Store solution w.r.t. to the optimization model format in input
  const auto modelFormat = pMIPModel->getModelInputFormat();
  switch (modelFormat)
  {
    case SolverModel::ModelInputFormat::INPUT_FORMAT_MPS:
      storeMPSSolution();
      break;
    case SolverModel::ModelInputFormat::INPUT_FORMAT_OPTILAB:
    case SolverModel::ModelInputFormat::INPUT_FORMAT_PROTOBUF:
      storeOptiLabSolution();
      break;
    default:
      throw std::runtime_error(
          "OrToolsMIPProcessor - unrecognized model format");
  }
}  // storeSolution

void OrToolsMIPProcessor::storeOptiLabSolution()
{
  // Critical section
  LockGuard lock(pResultMutex);

  ortoolsengine::ORToolsMIPResult::Solution& solution =
      pMIPResult->solution.second;
  ortoolsengine::ORToolsMIPResult::ObjectiveValues& objVals =
      pMIPResult->solution.first;

  // Set variables
  for (const auto& varPair : pMIPModel->getVariablesMap())
  {
    const auto& varName = varPair.first;
    if (solution.find(varName) != solution.end())
    {
      throw std::runtime_error(
          std::string("OrToolsMIPProcessor: variable assignment ") +
          std::string("already registered for variable ") + varName);
    }

    ORToolsMIPVariable::SPtr var =
        std::dynamic_pointer_cast<ORToolsMIPVariable>(varPair.second);
    if (!var)
    {
      const std::string errMsg =
          "OrToolsMIPProcessor - storeOptiLabSolution: "
          "invalid ORTools variable";
      spdlog::error(errMsg);
      throw std::runtime_error(errMsg);
    }
    if (var->isScalar())
    {
      const auto ORToolsVar = var->getVariable();
      solution[varName].push_back(
          {ORToolsVar->solution_value(), ORToolsVar->solution_value()});
    }
    else
    {
      auto& solList = solution[varName];
      solList.reserve((var->getVariableList()).size());
      for (const auto& subVars : var->getVariableList())
      {
        solList.push_back(
            {subVars->solution_value(), subVars->solution_value()});
      }
    }
  }

  // Set objectives
  for (const auto& mipObj : pMIPModel->getObjectiveList())
  {
    ORToolsMIPObjective::SPtr obj =
        std::dynamic_pointer_cast<ORToolsMIPObjective>(mipObj);
    if (!obj)
    {
      const std::string errMsg =
          "AsyncOrToolsMIPEngineProcessor - storeOptiLabSolution: "
          "invalid ORTools objective";
      spdlog::error(errMsg);
      throw std::runtime_error(errMsg);
    }

    objVals.push_back(obj->getObjective()->Value());
  }

  // Set result status
  pMIPResult->resultStatus = pMIPSolver->getResultStatus();
}  // storeOptiLabSolution

void OrToolsMIPProcessor::storeMPSSolution()
{
  // Critical section
  LockGuard lock(pResultMutex);
  ortoolsengine::ORToolsMIPResult::Solution& solution =
      pMIPResult->solution.second;
  ortoolsengine::ORToolsMIPResult::ObjectiveValues& objVals =
      pMIPResult->solution.first;

  operations_research::MPSolutionResponse result =
      pMIPSolver->getORToolsProtobufResult();
  const auto& protoModel = pMIPModel->getModelProto();
  for (int idx = 0; idx < result.variable_value_size(); ++idx)
  {
    solution[protoModel.variable(idx).name()].push_back(
        {result.variable_value(idx), result.variable_value(idx)});
  }

  objVals.push_back(pMIPSolver->getObjectiveValue());

  // Set result status
  pMIPResult->resultStatus = pMIPSolver->getResultStatus();
}  // storeMPSSolution

}  // namespace optilab
