//
// Copyright OptiLab 2019. All rights reserved.
//
// Search combinator builder for ORTools CP solvers.
//

#pragma once

#include <memory>   // for std::shared_ptr
#include <string>
#include <vector>

#include "ortools/constraint_solver/constraint_solveri.h"
#include "solver/cp_variable.hpp"
#include "solver/search_combinator_builder.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS ORToolsSearchCombinatorBuilder : public SearchCombinatorBuilder {
 public:
  using SPtr = std::shared_ptr<ORToolsSearchCombinatorBuilder>;

 public:
  /// Constructor.
  /// @note the give solver is used to build all the combinator of this class.
  /// When one of the methods has one or more combinators as input, they must built using this
  /// combinator builder instance.
  /// @note throws std::invalid_argument if the given solver pointer is nullptr
  ORToolsSearchCombinatorBuilder(std::shared_ptr<operations_research::Solver> solver);

  ~ORToolsSearchCombinatorBuilder() = default;

  std::string getDefaultVariableSelectionStrategy() const override;

  std::string getDefaultValueSelectionStrategy() const override;

  SearchCombinator::SPtr buildBaseSearchCombinator(
      const std::vector<CPVariable::SPtr>& variablesList, const std::string& varSelectionStrategy,
      const std::string& valSelectionStrategy) override;

  SearchCombinator::SPtr buildAndCombinator(
      const std::vector<SearchCombinator::SPtr>& combList) override;

  SearchCombinator::SPtr buildOrCombinator(
      const std::vector<SearchCombinator::SPtr>& combList) override;

 private:
  /// ORTools solver instance used to build combinators
  std::shared_ptr<operations_research::Solver> pSolver;
};

}  // namespace optilab
