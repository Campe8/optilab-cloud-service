//
// Copyright OptiLab 2019. All rights reserved.
//
// Class encapsulating an OR-Tools MIP constraint.
//

#pragma once

#include <memory>  // for std::shared_ptr
#include <string>
#include <vector>

#include "model/model_object_mip_constraint.hpp"
#include "ortools/linear_solver/linear_solver.h"
#include "solver/mip_constraint.hpp"
#include "solver/mip_variable.hpp"
#include "system/system_export_defs.hpp"

namespace optilab
{
class SYS_EXPORT_STRUCT ORToolsMIPConstraint : public MIPConstraint
{
 public:
  using SPtr = std::shared_ptr<ORToolsMIPConstraint>;

 public:
  explicit ORToolsMIPConstraint(operations_research::MPConstraint*);

  ORToolsMIPConstraint(
      ModelObjectMIPConstraint::SPtr mipConstraintObject,
      const std::unordered_map<std::string, MIPVariable::SPtr>& varMap,
      operations_research::MPSolver* solver);

  /// Returns the instance of the ORTools constraint stored in this class
  inline const operations_research::MPConstraint* getConstraint() const
  {
    return pConstraint;
  }

 private:
  /// Actual ORTools constraint
  operations_research::MPConstraint* pConstraint;
};

}  // namespace optilab
