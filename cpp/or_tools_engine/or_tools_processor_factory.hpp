//
// Copyright OptiLab 2019. All rights reserved.
//
// Factory class for ORTools processors.
//

#pragma once

#include "engine/engine_constants.hpp"
#include "or_tools_engine/or_tools_processor.hpp"
#include "or_tools_engine/or_tools_utilities.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS ORToolsProcessorFactory {
public:
  explicit ORToolsProcessorFactory(ortoolsengine::ORToolsResult::SPtr result);

  /// Sets the type of ORTools processor to build
  inline void setProcessorType(EngineClassType ptype) noexcept { pProcType = ptype; }

  /// Returns the processor build w.r.t. the set type.
  /// See also "setProcessorType(...)".
  /// @note throw std::runtime_error on invalid processor type
  ORToolsProcessor::SPtr buildProcessor();


private:
  /// Type of processor to build
  EngineClassType pProcType;

  /// Result used to build an ORTools processor
  ortoolsengine::ORToolsResult::SPtr pResult;
};

}  // namespace optilab
