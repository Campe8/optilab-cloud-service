#include "or_tools_engine/or_tools_vrp_processor.hpp"

#include <exception>
#include <stdexcept>   // for std::runtime_error

#include <spdlog/spdlog.h>

#include "data_structure/metrics/metrics_register.hpp"
#include "engine/engine_constants.hpp"
#include "solver/solver_model.hpp"
#include "utilities/timer.hpp"

namespace optilab {

OrToolsVRPProcessor::OrToolsVRPProcessor(ortoolsengine::ORToolsResult::SPtr result)
: ORToolsProcessor("OrToolsVRPProcessor", result),
  pVRPModel(nullptr),
  pVRPSolver(nullptr)
{
  pVRPResult = std::dynamic_pointer_cast<ortoolsengine::ORToolsVRPResult>(getResult());
  if (!pVRPResult)
  {
    throw std::runtime_error("OrToolsVRPProcessor - invalid downcast to ORToolsVRPResult");
  }
}

void OrToolsVRPProcessor::loadJSONModelAndCreateSolver(const std::string& jsonModel)
{
  if (jsonModel.empty())
  {
    throw std::runtime_error("OrToolsVRPProcessor - loadJSONModelAndCreateSolver: "
        "empty JSON model");
  }

  // Create a new OR-Tools VRP model from the given input JSON model
  try
  {
    pVRPModel = std::make_shared<ORToolsVRPModel>(jsonModel);
  }
  catch(const std::exception& ex)
  {
    const std::string errMsg = "OrToolsVRPProcessor - loadJSONModelAndCreateSolver: "
        "error parsing and initializing the model " + std::string(ex.what());
    spdlog::error(errMsg);
    throw;
  }
  catch(...)
  {
    const std::string errMsg = "OrToolsVRPProcessor - loadJSONModelAndCreateSolver: "
        "error parsing and initializing the model";
    spdlog::error(errMsg);
    throw;
  }

  // Set the name of the model
  pVRPResult->modelName = pVRPModel->getModelName();

  // Build a MIP solver
  try
  {
    pVRPSolver = std::make_shared<ORToolsVRPSolver>(pVRPModel);
  }
  catch(const std::exception& ex)
  {
    const std::string errMsg = "OrToolsVRPProcessor - loadJSONModelAndCreateSolver: "
        "cannot build the VRP solver " + std::string(ex.what());
    spdlog::error(errMsg);
    throw;
  }
  catch(...)
  {
    const std::string errMsg = "OrToolsVRPProcessor - loadJSONModelAndCreateSolver: "
        "cannot build the VRP solver";
    spdlog::error(errMsg);
    throw;
  }
}  // setupModel

bool OrToolsVRPProcessor::interruptSolver()
{
  if (!pVRPSolver)
  {
    return false;
  }

  pVRPSolver->interruptSearchProcess();
  return true;
}  // interruptSolver

void OrToolsVRPProcessor::processWork(Work work)
{
  if (work->workType == ortoolsengine::ORToolsWork::kRunOptimizer)
  {
    runSolver(work);
  }
  else
  {
    spdlog::warn("OrToolsVRPProcessor - processWork: unrecognized work type");
  }
}  // processWork

void OrToolsVRPProcessor::runSolver(Work& work)
{
  if (!pVRPModel)
  {
    const std::string errMsg = "OrToolsVRPProcessor - runSolver: no model loaded";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  if (!pVRPSolver)
  {
    const std::string errMsg = "OrToolsVRPProcessor - runSolver: no solver available";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  timer::Timer timer;

  // Run the solver
  pVRPSolver->solve();

  const auto wallclockMsec = timer.getWallClockTimeMsec();
  work->metricsRegister->setMetric(jsonmetrics::OPTIMIZER_LATENCY_MSEC, wallclockMsec);

  // Store the solution found
  storeSolution();
}  // runEngine

void OrToolsVRPProcessor::storeSolution()
{
  LockGuard lock(pResultMutex);

  // Retrieve the solution
  pVRPSolver->storeSolution(*pVRPResult);
}  // storeSolution

}  // namespace optilab
