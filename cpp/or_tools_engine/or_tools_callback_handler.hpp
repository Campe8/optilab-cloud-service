//
// Copyright OptiLab 2019. All rights reserved.
//
// Callback handler for OR-Tools engines.
//

#pragma once

#include "engine/engine_callback_handler.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS ORToolsCallbackHandler : public EngineCallbackHandler {
 public:
  ORToolsCallbackHandler() = default;

  void resultCallback(const std::string& engineId, const std::string& jsonResult) override;
};

}  // namespace optilab
