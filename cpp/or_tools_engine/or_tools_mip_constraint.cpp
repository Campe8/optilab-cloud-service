#include "or_tools_engine/or_tools_mip_constraint.hpp"

#include <iostream>
#include <stdexcept>  // for std::invalid_argument

#include "or_tools_engine/or_tools_mip_variable.hpp"

namespace optilab
{
ORToolsMIPConstraint::ORToolsMIPConstraint(
    operations_research::MPConstraint* con)
    : MIPConstraint(), pConstraint(con)
{
}

ORToolsMIPConstraint::ORToolsMIPConstraint(
    ModelObjectMIPConstraint::SPtr mipConstraintObject,
    const std::unordered_map<std::string, MIPVariable::SPtr>& varMap,
    operations_research::MPSolver* solver)
    : MIPConstraint(mipConstraintObject, varMap)
{
  // Instantiate the constraint
  if (!solver)
  {
    throw std::invalid_argument(
        "ORToolsMIPConstraint - pointer to solver instance is NULL");
  }

  if (getConstraintId().empty())
  {
    pConstraint = solver->MakeRowConstraint(getConstraintLowerBound(),
                                            getConstraintUpperBound());
  }
  else
  {
    pConstraint =
        solver->MakeRowConstraint(getConstraintLowerBound(),
                                  getConstraintUpperBound(), getConstraintId());
  }

  // Add the coefficients
  for (const auto& varCoeffPair : getVarCoeffList())
  {
    ORToolsMIPVariable::SPtr var =
        std::dynamic_pointer_cast<ORToolsMIPVariable>(varCoeffPair.first);
    if (!var)
    {
      throw std::runtime_error(
          "ORToolsMIPConstraint - invalid cast to ORToolsMIPVariable for "
          "variables from the var-coeff list");
    }

    pConstraint->SetCoefficient(var->getVariable(), varCoeffPair.second);
  }
}

}  // namespace optilab
