#include "or_tools_engine/or_tools_vrp_solver.hpp"

#include <stdexcept>   // for std::invalid_argument

#include <spdlog/spdlog.h>

namespace optilab {

ORToolsVRPSolver::ORToolsVRPSolver(const ORToolsVRPModel::SPtr& vrpModel)
: pVRPModel(vrpModel)
{
  if (!pVRPModel)
  {
    throw std::invalid_argument("ORToolsVRPSolver - empty pointer to a VRP model");
  }

  initSolver();
}

ORToolsVRPSolver::~ORToolsVRPSolver()
{
  pVRPModel.reset();
  pRIManager.reset();
  pRoutingModel.reset();
}

void ORToolsVRPSolver::initSolver()
{
  // Instantiate a nre routing manager
  pRIManager =
      std::make_shared<operations_research::RoutingIndexManager>(
          pVRPModel->getNumCities(), pVRPModel->getNumVehicles(), pVRPModel->getDepot());

  // Instantiate a new routing model from the routing manager
  pRoutingModel = std::make_shared<operations_research::RoutingModel>(*pRIManager);

  // Register a transit callback to specify the cost of each path
  const int transitCallbackIndex = pRoutingModel->RegisterTransitCallback(
      [this](int64 fromIndex, int64 toIndex) -> int64 {
       // Convert from routing variable Index to distance matrix NodeIndex.
        int fromNode = pRIManager->IndexToNode(fromIndex).value();
        int toNode = pRIManager->IndexToNode(toIndex).value();
        return pVRPModel->getDistanceMatrix().at(fromNode).at(toNode);
  });

  // Define the cost of each arc
  pRoutingModel->SetArcCostEvaluatorOfAllVehicles(transitCallbackIndex);

  // Check is capacity constraints need to be added
  if (!(pVRPModel->getDemands()).empty())
  {
    const int demandCallbackIndex = pRoutingModel->RegisterUnaryTransitCallback(
        [this](int64 fromIndex) -> int64 {
         // Convert from routing variable Index to demand NodeIndex.
        int fromNode = pRIManager->IndexToNode(fromIndex).value();
        return pVRPModel->getDemands().at(fromNode);
    });

    pRoutingModel->AddDimensionWithVehicleCapacity(
        demandCallbackIndex,                // transit callback index
        int64{0},                           // null capacity slack
        pVRPModel->getVehicleCapacities(),  // vehicle maximum capacities
        true,                               // start cumul to zero
        "Capacity");
  }

  // Set search strategy
  pSearchParams = operations_research::DefaultRoutingSearchParameters();
  pSearchParams.set_first_solution_strategy(
      operations_research::FirstSolutionStrategy::PATH_CHEAPEST_ARC);
}  // initSolver

bool ORToolsVRPSolver::interruptSearchProcess()
{
  pRoutingModel->CloseModel();
  return true;
}  // interruptSearchProcess

void ORToolsVRPSolver::solve()
{
  pSolution = pRoutingModel->SolveWithParameters(pSearchParams);
}  // solve

void ORToolsVRPSolver::storeSolution(ortoolsengine::ORToolsVRPResult& sol)
{
  if (!pSolution)
  {
    throw std::runtime_error("ORToolsVRPSolver - getSolution: the solver did not run");
  }

  // Set status of the solution
  sol.status = getResultStatus();

  int64 totalDistance{0};
  int64 totalLoad{0};
  bool hasLoad = !(pVRPModel->getDemands()).empty();
  for (int vehicleId = 0; vehicleId < pVRPModel->getNumVehicles(); ++vehicleId)
  {
    ortoolsengine::ORToolsVRPResult::VRPRoute vrpRoute;
    vrpRoute.vehicleId = vehicleId;

    int64 index = pRoutingModel->Start(vehicleId);
    int64 routeDistance{0};
    int64 routeLoad{0};
    while (pRoutingModel->IsEnd(index) == false)
    {
      int64 nodeIndex = pRIManager->IndexToNode(index).value();

      vrpRoute.route.push_back(nodeIndex);

      if (hasLoad)
      {
        routeLoad += pVRPModel->getDemands().at(nodeIndex);
        vrpRoute.load.push_back(routeLoad);
      }

      int64 previousIndex = index;
      index = pSolution->Value(pRoutingModel->NextVar(index));
      routeDistance += pRoutingModel->GetArcCostForVehicle(previousIndex, index, int64{vehicleId});
    }

    // Add last index of the route
    vrpRoute.route.push_back(pRIManager->IndexToNode(index).value());
    vrpRoute.totDistance = routeDistance;
    vrpRoute.totLoad = routeLoad;

    // Push back the route
    sol.routeList.push_back(vrpRoute);

    // Accumulate the distances and loads
    totalDistance += routeDistance;
    totalLoad += routeLoad;
  }

  // Set total distance and load
  sol.totDistance = totalDistance;
  sol.totLoad = totalLoad;
}  // getSolution

}  // namespace optilab
