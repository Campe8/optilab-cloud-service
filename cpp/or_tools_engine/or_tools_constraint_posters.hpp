//
// Copyright OptiLab 2019. All rights reserved.
//
// Constraint posters for OR-Tools constraints
//

#pragma once

#include "solver/cp_constraint.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

namespace ortoolsconstraintposters {

SYS_EXPORT_FCN void p_arrayBoolAnd(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_arrayBoolOr(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_arrayBoolXor(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_arrayIntElement(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_arrayVarIntElement(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_boolAnd(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_boolClause(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_boolNot(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_boolOr(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_boolXor(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_intEq(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_intEqReif(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_intGe(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_intGeReif(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_intGt(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_intGtReif(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_intLe(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_intLeReif(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_intLt(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_intLtReif(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_intLinEq(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_intLinLe(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_intNe(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_intNeReif(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_intAbs(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_intDiv(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_intLinEqReif(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_intLinGe(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_intLinGeReif(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_intLinLeReif(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_intLinNe(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_intLinNeReif(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_intMax(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_intMin(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_intMinus(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_intMod(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_intNegate(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_intPlus(CPConstraint::SPtr con);
SYS_EXPORT_FCN void p_intTimes(CPConstraint::SPtr con);

}  // ortoolsconstraintposters

}  // namespace optilab
