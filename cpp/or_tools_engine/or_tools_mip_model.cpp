#include "or_tools_engine/or_tools_mip_model.hpp"

#include <spdlog/spdlog.h>
#include <unistd.h>  // for write

#include <cstdio>     // for std::remove
#include <cstdlib>    // for std::mkstemp
#include <fstream>    // for std::ofstream
#include <stdexcept>  // for std::invalid_argument

#include "model/model_constants.hpp"
#include "model/model_object_mip_constraint.hpp"
#include "model/model_object_mip_objective.hpp"
#include "or_tools_engine/or_tools_mip_constraint.hpp"
#include "or_tools_engine/or_tools_mip_objective.hpp"
#include "or_tools_engine/or_tools_utilities.hpp"
#include "ortools/lp_data/mps_reader.h"
#include "utilities/file_sys.hpp"
#include "utilities/random_generators.hpp"

namespace
{
constexpr int kTempFileNameLength = 10;

}  // namespace

namespace optilab
{
ORToolsMIPModel::ORToolsMIPModel(const Model::SPtr& model) : MIPModel(model)
{
  const auto problemType = getProblemType();
  switch (problemType)
  {
    case ProblemType::PT_LP:
      pSolver = std::make_shared<operations_research::MPSolver>(
          model->getModelName(),
          operations_research::MPSolver::CLP_LINEAR_PROGRAMMING);
      break;
    case ProblemType::PT_MIP:
      pSolver = std::make_shared<operations_research::MPSolver>(
          model->getModelName(),
          operations_research::MPSolver::CBC_MIXED_INTEGER_PROGRAMMING);
      break;
    case ProblemType::PT_IP:
      pSolver = std::make_shared<operations_research::MPSolver>(
          model->getModelName(),
          operations_research::MPSolver::GLOP_LINEAR_PROGRAMMING);
      break;
    default:
      throw std::runtime_error(
          "ORToolsMIPModel - unrecognized optimization model problem type");
  }
}

ORToolsMIPModel::ORToolsMIPModel(const LinearModelSpecProto& model)
    : MIPModel(model)
{
  switch (model.package_type())
  {
    case LinearModelSpecProto_PackageType::LinearModelSpecProto_PackageType_OR_TOOLS_CLP:
    {
      pSolver = std::make_shared<operations_research::MPSolver>(
          model.model_id(),
          operations_research::MPSolver::CLP_LINEAR_PROGRAMMING);
      break;
    }
    case LinearModelSpecProto_PackageType::LinearModelSpecProto_PackageType_OR_TOOLS_GLPK:
    {
      if (model.class_type() == LinearModelSpecProto_ClassType::LinearModelSpecProto_ClassType_MIP)
      {
        pSolver = std::make_shared<operations_research::MPSolver>(
            model.model_id(),
            operations_research::MPSolver::GLPK_MIXED_INTEGER_PROGRAMMING);
      }
      else
      {
        pSolver = std::make_shared<operations_research::MPSolver>(
            model.model_id(),
            operations_research::MPSolver::GLPK_LINEAR_PROGRAMMING);
      }
      break;
    }
    case LinearModelSpecProto_PackageType::LinearModelSpecProto_PackageType_OR_TOOLS_GLOP:
    {
      pSolver = std::make_shared<operations_research::MPSolver>(
          model.model_id(),
          operations_research::MPSolver::GLOP_LINEAR_PROGRAMMING);
      break;
    }
    case LinearModelSpecProto_PackageType::LinearModelSpecProto_PackageType_OR_TOOLS_CBC:
    {
      pSolver = std::make_shared<operations_research::MPSolver>(
          model.model_id(),
          operations_research::MPSolver::CBC_MIXED_INTEGER_PROGRAMMING);
      break;
    }
    default:
      throw std::runtime_error(
          "ORToolsMIPModel - unrecognized optimization model problem type");
  }
}

void ORToolsMIPModel::initializeWithLegacyModelFormat()
{
  switch (getModelInputFormat())
  {
    case ModelInputFormat::INPUT_FORMAT_MPS:
    {
      initializeORToolsMPSModel();
      break;
    }
    default:
      throw std::runtime_error(
          "ORToolsMIPModel - unrecognized legacy model format");
  }
}  // initializeWithLegacyModelFormat

void ORToolsMIPModel::initializeORToolsMPSModel()
{
  std::string modelFileName;
  int fd;
  const auto useFileDescriptor = getModel()->useModelFileDescriptor();
  if (useFileDescriptor)
  {
    // modelFileName = utilsfilesys::getPathToLocation({getLegacyModelDescriptor()});
    modelFileName = getLegacyModelDescriptor();
    if (!utilsfilesys::doesFileExists(modelFileName))
    {
      const std::string errMsg =
          "ORToolsMIPModel - file not found: " + modelFileName;
      spdlog::error(errMsg);
      throw std::runtime_error(errMsg);
    }
  }
  else
  {
    // Create a temporary file to store the content of the mps model
    char templateFile[] = "/tmp/fileXXXXXX";
    fd = mkstemp(templateFile);
    if (fd == -1)
    {
      throw std::runtime_error(
          "ORToolsMIPModel - cannot create temporary mps file");
    }

    // Write the string into the file
    modelFileName = std::string(templateFile);
    auto res = write(fd, getLegacyModelDescriptor().c_str(),
                     getLegacyModelDescriptor().size());
    if (res == -1)
    {
      throw std::runtime_error(
          "ORToolsMIPModel - error in writing the temporary mps file");
    }
  }

  // Load the model into proto object from the file
  auto loadStatus = operations_research::glop::MPSReader().ParseFile(
      modelFileName, &pModelProto);

  if (!useFileDescriptor)
  {
    // Close and delete the file asap
    const int err = unlink(modelFileName.c_str());
    if (err == -1)
    {
      spdlog::warn("ORToolsMIPModel: cannot unlink temporary mps file: " +
                   modelFileName);
    }
    close(fd);
  }

  // Check the status of the model
  if (!loadStatus.ok())
  {
    const std::string errMsg =
        "ORToolsMIPModel - cannot load mps file: " + loadStatus.ToString();
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  // Load the model into the solver
  std::string loadErrMsg;
  const operations_research::MPSolverResponseStatus status =
      pSolver->LoadModelFromProtoWithUniqueNamesOrDie(pModelProto, &loadErrMsg);

  if (status != operations_research::MPSOLVER_MODEL_IS_VALID)
  {
    std::string errMsg =
        MPSolverResponseStatus_Name(status) + ": " + loadErrMsg;
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }
}  // initializeORToolsMPSModel

MIPVariable::SPtr ORToolsMIPModel::createVariableFromProto(
    const OptVariableProto& protoVar)
{
  const std::string& name = protoVar.name();
  const bool isInt = protoVar.is_integer();
  const double objCoeff = protoVar.objective_coefficient();

  std::shared_ptr<ORToolsMIPVariable> var = nullptr;
  var = std::make_shared<ORToolsMIPVariable>(name, protoVar.lower_bound(),
                                             protoVar.upper_bound(), !isInt,
                                             pSolver.get());
  var->setObjectiveCoeff(objCoeff);
  return var;
}  // createVariableFromProto

MIPVariable::SPtr ORToolsMIPModel::createVariable(
    const ModelObjectVariable::SPtr& objVariable)
{
  if (!objVariable)
  {
    throw std::runtime_error(
        "ORToolsMIPModel - empty pointer to ModelObjectVariable");
  }

  return ortoolsutils::getMIPVar(objVariable, pSolver.get());
}  // createVariable

MIPConstraint::SPtr ORToolsMIPModel::createConstraint(
    const ModelObjectConstraint::SPtr& objConstraint, const VarMap& varMap)
{
  ModelObjectMIPConstraint::SPtr conObj =
      std::dynamic_pointer_cast<ModelObjectMIPConstraint>(objConstraint);
  if (!conObj)
  {
    throw std::invalid_argument(
        "ORToolsMIPModel - invalid cast from ModelObjectConstraint to "
        "ModelObjectMIPConstraint");
  }

  return std::make_shared<ORToolsMIPConstraint>(conObj, varMap, pSolver.get());
}  // createConstraint

MIPConstraint::SPtr ORToolsMIPModel::createConstraintFromProto(
    const OptConstraintProto& protoCon, const VarMap& varMap)
{
  auto con = pSolver->MakeRowConstraint(
      protoCon.lower_bound(), protoCon.upper_bound(), protoCon.name());

  if (protoCon.var_index_size() != protoCon.coefficient_size())
  {
    throw std::runtime_error(
        "ORToolsMIPModel - createConstraintFromProto: "
        "invalid size for variables and coefficients " +
        std::to_string(protoCon.var_index_size()) +
        " != " + std::to_string(protoCon.coefficient_size()));
  }

  const auto& varList = pSolver->variables();
  for (int idx = 0; idx < protoCon.var_index_size(); ++idx)
  {
    const int vidx = protoCon.var_index(idx);
    if (vidx < 0 || vidx >= varList.size())
    {
      throw std::runtime_error(
          "ORToolsMIPModel - createConstraintFromProto: "
          "invalid variable index " +
          std::to_string(vidx) + " (it should be in [0, " +
          std::to_string(varList.size() - 1) + "]");
    }
    con->SetCoefficient(varList[vidx], protoCon.coefficient(idx));
  }
  con->set_is_lazy(protoCon.is_lazy());

  return std::make_shared<ORToolsMIPConstraint>(con);
}  // createConstraintFromProto

MIPObjective::SPtr ORToolsMIPModel::createObjective(
    const ModelObjectObjective::SPtr& objObjective, const VarMap& varMap)
{
  ModelObjectMIPObjective::SPtr objectiveObj =
      std::dynamic_pointer_cast<ModelObjectMIPObjective>(objObjective);
  if (!objectiveObj)
  {
    throw std::invalid_argument(
        "ORToolsMIPModel - invalid cast from ModelObjectObjective to "
        "ModelObjectMIPObjective");
  }

  return std::make_shared<ORToolsMIPObjective>(objectiveObj, varMap,
                                               pSolver.get());
}  // createObjective

MIPObjective::SPtr ORToolsMIPModel::createObjectiveFromProto(
    const OptModelProto& model, const VarMap& varMap)
{
  auto obj = pSolver->MutableObjective();
  for (const auto& vname : getObjectiveVarList())
  {
    auto varPtr = varMap.at(vname);
    auto var = std::dynamic_pointer_cast<ORToolsMIPVariable>(varPtr);
    if (!var)
    {
      throw std::runtime_error(
          "ORToolsMIPModel - createObjectiveFromProto: "
          "cannot convert MIP variable with id " +
          vname);
    }
    obj->SetCoefficient(var->getVariable(), var->getObjectiveCoeff());
  }

  obj->SetOffset(model.objective_offset());
  if (model.maximize())
  {
    obj->SetMaximization();
  }
  else
  {
    obj->SetMinimization();
  }

  return std::make_shared<ORToolsMIPObjective>(obj);
}  // createObjectiveFromProto

}  // namespace optilab
