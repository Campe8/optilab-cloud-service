#include "or_tools_engine/or_tools_cp_processor.hpp"

#include <stdexcept>  // for std::runtime_error

#include <spdlog/spdlog.h>

#include "data_structure/metrics/metrics_register.hpp"
#include "engine/engine_constants.hpp"
#include "or_tools_engine/or_tools_cp_solver_builder.hpp"
#include "or_tools_engine/or_tools_cp_variable.hpp"
#include "utilities/timer.hpp"

namespace optilab {

OrToolsCPProcessor::OrToolsCPProcessor(ortoolsengine::ORToolsResult::SPtr result)
: ORToolsProcessor("OrToolsCPProcessor", result),
  pCPModel(nullptr),
  pCPSolver(nullptr)
{
  pCPResult = std::dynamic_pointer_cast<ortoolsengine::ORToolsCPResult>(getResult());
  if (!pCPResult)
  {
    throw std::runtime_error("OrToolsCPProcessor - invalid downcast to ORToolsCPResult");
  }
}

void OrToolsCPProcessor::loadModelAndCreateSolver(const Model::SPtr& model)
{
  if (!model)
  {
    throw std::runtime_error("OrToolsCPProcessor - loadModel: empty model");
  }

  // Create a new OR-Tools CP model from the given input model
  pCPModel = std::make_shared<ORToolsCPModel>(model);

  // Set the name of the model
  pCPResult->modelName = pCPModel->getModelName();

  // Parse and initialize the model
  try
  {
    pCPModel->initializeModel();
  }
  catch(const std::exception& ex)
  {
    const std::string errMsg = "OrToolsCPProcessor - loadModel: "
        "error parsing and initializing the model: " + std::string(ex.what());
    spdlog::error(errMsg);
    throw;
  }
  catch(...)
  {
    const std::string errMsg = "OrToolsCPProcessor - loadModel: "
        "error parsing and initializing the model";
    spdlog::error(errMsg);
    throw;
  }

  // Build the CP Solver builder
  ORToolsCPSolverBuilder::SPtr solverBuilder;
  try
  {
    solverBuilder = std::make_shared<ORToolsCPSolverBuilder>(pCPModel);
  }
  catch(const std::exception& ex)
  {
    const std::string errMsg = "OrToolsCPProcessor - loadModel: "
        "cannot build the solver builder: " + std::string(ex.what());
    spdlog::error(errMsg);
    throw;
  }
  catch(...)
  {
    const std::string errMsg = "OrToolsCPProcessor - loadModel: "
        "cannot build the solver builder";
    spdlog::error(errMsg);
    throw;
  }

  // Build the solver
  try
  {
    CPSolver::SPtr solver = solverBuilder->build();
    pCPSolver = std::dynamic_pointer_cast<ORToolsCPSolver>(solver);
    if (!pCPSolver)
    {
      const std::string errMsg = "OrToolsCPProcessor - loadModel: "
          "solver builder built a wrong type of solver";
      spdlog::error(errMsg);
      throw std::runtime_error(errMsg);
    }
  }
  catch(const std::exception& ex)
  {
    const std::string errMsg = "OrToolsCPProcessor - loadModel: "
        "cannot build the solver: " + std::string(ex.what());
    spdlog::error(errMsg);
    throw;
  }
  catch(...)
  {
    const std::string errMsg = "OrToolsCPProcessor - loadModel: "
        "cannot build the solver";
    spdlog::error(errMsg);
    throw;
  }
}  // loadModelAndCreateSolver

bool OrToolsCPProcessor::interruptSolver()
{
  if (!pCPSolver)
  {
    return false;
  }

  pCPSolver->interruptSearchProcess();
  return true;
}  // interruptSolver

void OrToolsCPProcessor::processWork(Work work)
{
  if (work->workType == ortoolsengine::ORToolsWork::kRunOptimizer)
  {
    runSolver(work);
  }
  else
  {
    spdlog::warn("OrToolsCPProcessor - processWork: unrecognized work type");
  }
}  // processWork

void OrToolsCPProcessor::runSolver(Work& work)
{
  if (!pCPModel)
  {
    const std::string errMsg = "OrToolsCPProcessor - runSolver: no model loaded";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  if (!pCPSolver)
  {
    const std::string errMsg = "OrToolsCPProcessor - runSolver: no solver available";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  timer::Timer timer;

  const auto numSol = pCPModel->getNumberOfSolutionsToFind();
  if (pCPModel->isPropagationOnlyModel() || numSol == 0)
  {
    const bool satModel = runConstraintPropagationOnly();
    if (!satModel)
    {
      storeUnsatModel();
    }
    else
    {
      storeSatModel();
    }
  }
  else
  {
    // Otherwise run the model to find solutions
    runSolver(numSol);
  }

  const auto wallclockMsec = timer.getWallClockTimeMsec();
  work->metricsRegister->setMetric(jsonmetrics::OPTIMIZER_LATENCY_MSEC, wallclockMsec);
}  // runEngine

bool OrToolsCPProcessor::runConstraintPropagationOnly()
{
  // Initialize the search
  pCPSolver->initSearch();

  // Run the solver for constraint propagation only
  const bool satModel = pCPSolver->constraintPropagation();

  // Cleanup the search
  pCPSolver->cleanupSearch();

  // Return the result of the propagation
  return satModel;
}  // runConstraintPropagationOnly

void OrToolsCPProcessor::runSolver(int numSol)
{
  assert(numSol != 0);

  // Initialize the search
  pCPSolver->initSearch();

  // Check for all solutions
  bool findAllSolutions = numSol < 0;

  int solCtr = 0;
  while(((solCtr < numSol) || findAllSolutions) && pCPSolver->nextSolution())
  {
    storeSolution();
    solCtr++;
  }

  // Check and set this model status w.r.t. the solver status
  const auto status = pCPSolver->getStatus();
  if (status == CPSolver::SolverStatus::SS_SAT || status == CPSolver::SolverStatus::SS_OPT)
  {
    // Keep track of the fact that the model is satisfiable
    storeSatModel();
  }
  else
  {
    storeUnsatModel();
  }

  // Cleanup the search
  pCPSolver->cleanupSearch();
}  // runSolver

inline ortoolsengine::ORToolsCPResult OrToolsCPProcessor::getCurrentResult()
{
  LockGuard lock(pResultMutex);
  return *pCPResult;
}  // getCurrentResult

void OrToolsCPProcessor::storeUnsatModel()
{
  pSatModel = false;

  LockGuard lock(pResultMutex);
  pCPResult->isSatModel = pSatModel;
}  // storeUnsatModel

void OrToolsCPProcessor::storeSatModel()
{
  pSatModel = true;

  LockGuard lock(pResultMutex);
  pCPResult->isSatModel = pSatModel;
}  // storeSatModel

void OrToolsCPProcessor::storeSolution()
{
  // Critical section
  LockGuard lock(pResultMutex);

  // Store the solution
  ortoolsengine::ORToolsCPResult::Solution solution;
  for (const auto& varPair : pCPModel->getVariablesMap())
  {
    const auto& varName = varPair.first;
    if (solution.find(varName) != solution.end())
    {
      throw std::runtime_error("OrToolsCPProcessor - storeSolution: "
          "variable assignment already registered for variable " + varName);
    }

    // Skip non-output variables
    if (!varPair.second->isOutput()) continue;

    ORToolsCPVariable::SPtr var = std::dynamic_pointer_cast<ORToolsCPVariable>(varPair.second);
    if (!var)
    {
      const std::string errMsg = "AsyncOrToolsCPEngineProcessor - storeSolution: "
          "invalid ORTools variable";
      spdlog::error(errMsg);
      throw std::runtime_error(errMsg);
    }
    if (var->isScalar())
    {
      const auto ORToolsVar = var->getVariable();
      solution[varName].push_back({ORToolsVar->Min(), ORToolsVar->Max()});
    }
    else
    {
      auto& solList = solution[varName];
      solList.reserve((var->getVariableList()).size());
      for (const auto& subVars : var->getVariableList())
      {
        solList.push_back({subVars->Min(), subVars->Max()});
      }
    }
  }

  // Store the solution
  pCPResult->solutionList.push_back(solution);
}  // storeSolution

}  // namespace optilab
