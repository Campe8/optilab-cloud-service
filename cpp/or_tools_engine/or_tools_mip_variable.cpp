#include "or_tools_engine/or_tools_mip_variable.hpp"

#include <cassert>
#include <stdexcept>  // for std::invalid_argument

#include "or_tools_engine/or_tools_utilities.hpp"
#include "utilities/variables.hpp"

namespace optilab
{
ORToolsMIPVariable::ORToolsMIPVariable(const std::string& name, double min,
                                       double max, bool isContinuous,
                                       operations_research::MPSolver* solver)
    : MIPVariable(name),
      pType(isContinuous ? VarType::CONTINUOUS_VAR : VarType::INT_VAR),
      pLowerBound(min),
      pUpperBound(max),
      pSolver(solver)
{
  if (!solver)
  {
    throw std::invalid_argument(
        "ORToolsMIPVariable - pointer to solver is NULL");
  }

  if (pLowerBound > pUpperBound)
  {
    throw std::invalid_argument(
        "ORToolsMIPVariable - invalid variable bounds: " + getVarName() + "[" +
        std::to_string(pLowerBound) + ", " + std::to_string(pUpperBound) + "]");
  }

  if (isContinuous)
  {
    pVariableList.push_back(
        solver->MakeNumVar(pLowerBound, pUpperBound, getVarName()));
  }
  else
  {
    pVariableList.push_back(
        solver->MakeIntVar(pLowerBound, pUpperBound, getVarName()));
  }
}

ORToolsMIPVariable::ORToolsMIPVariable(const std::string& name, double min,
                                       double max, bool isContinuous, int len,
                                       operations_research::MPSolver* solver)
    : MIPVariable(name, len),
      pType(isContinuous ? VarType::CONTINUOUS_VAR : VarType::INT_VAR),
      pLowerBound(min),
      pUpperBound(max),
      pSolver(solver)
{
  if (!solver)
  {
    throw std::invalid_argument(
        "ORToolsMIPVariable - pointer to solver is NULL");
  }

  if (pLowerBound > pUpperBound)
  {
    throw std::invalid_argument(
        "ORToolsMIPVariable - invalid variable bounds: " + getVarName() + "[" +
        std::to_string(pLowerBound) + ", " + std::to_string(pUpperBound) + "]");
  }

  if (isContinuous)
  {
    solver->MakeNumVarArray(len, pLowerBound, pUpperBound, getVarName(),
                            &pVariableList);
  }
  else
  {
    solver->MakeIntVarArray(len, pLowerBound, pUpperBound, getVarName(),
                            &pVariableList);
  }
}

ORToolsMIPVariable::ORToolsMIPVariable(const std::string& name, double min,
                                       double max, bool isContinuous,
                                       const std::vector<int>& dims,
                                       operations_research::MPSolver* solver)
    : MIPVariable(name, dims),
      pType(isContinuous ? VarType::CONTINUOUS_VAR : VarType::INT_VAR),
      pLowerBound(min),
      pUpperBound(max),
      pSolver(solver)
{
  if (!solver)
  {
    throw std::invalid_argument(
        "ORToolsMIPVariable - pointer to solver is NULL");
  }

  if (pLowerBound > pUpperBound)
  {
    throw std::invalid_argument(
        "ORToolsMIPVariable - invalid variable bounds: " + getVarName() + "[" +
        std::to_string(pLowerBound) + ", " + std::to_string(pUpperBound) + "]");
  }

  if (getNumDim() < 2)
  {
    throw std::runtime_error(
        "ORToolsMIPVariable - Invalid number of dimensions for matrix");
  }

  int numvars = 1;
  for (auto d : getVarDims())
  {
    numvars *= d;
  }

  if (isContinuous)
  {
    solver->MakeNumVarArray(numvars, pLowerBound, pUpperBound, getVarName(),
                            &pVariableList);
  }
  else
  {
    solver->MakeIntVarArray(numvars, pLowerBound, pUpperBound, getVarName(),
                            &pVariableList);
  }
}

ORToolsMIPVariable::ORToolsMIPVariable(const std::string& name,
                                       operations_research::MPSolver* solver)
    : MIPVariable(name),
      pType(VarType::BOOL_VAR),
      pLowerBound(0),
      pUpperBound(1),
      pSolver(solver)
{
  if (!solver)
  {
    throw std::invalid_argument(
        "ORToolsMIPVariable - pointer to solver is NULL");
  }

  pVariableList.push_back(solver->MakeBoolVar(getVarName()));
}

ORToolsMIPVariable::ORToolsMIPVariable(const std::string& name, int len,
                                       operations_research::MPSolver* solver)
    : MIPVariable(name, len),
      pType(VarType::BOOL_VAR),
      pLowerBound(0),
      pUpperBound(1),
      pSolver(solver)
{
  if (!solver)
  {
    throw std::invalid_argument(
        "ORToolsMIPVariable - pointer to solver is NULL");
  }

  solver->MakeBoolVarArray(len, getVarName(), &pVariableList);
}

ORToolsMIPVariable::ORToolsMIPVariable(const std::string& name,
                                       const std::vector<int>& dims,
                                       operations_research::MPSolver* solver)
    : MIPVariable(name, dims),
      pType(VarType::BOOL_VAR),
      pLowerBound(0),
      pUpperBound(1),
      pSolver(solver)
{
  if (!solver)
  {
    throw std::invalid_argument(
        "ORToolsMIPVariable - pointer to solver is NULL");
  }

  if (getNumDim() < 2)
  {
    throw std::runtime_error(
        "ORToolsMIPVariable - Invalid number of dimensions for matrix");
  }

  int numvars = 1;
  for (auto d : getVarDims())
  {
    numvars *= d;
  }

  solver->MakeBoolVarArray(numvars, getVarName(), &pVariableList);
}

operations_research::MPVariable* ORToolsMIPVariable::getVariable(
    const std::vector<int>& subscr)
{
  if (isScalar())
  {
    throw std::runtime_error(
        "ORToolsMIPVariable - invalid operand on scalar variable");
  }

  const int idx = utilsvariables::getFlattenedIndex(getVarDims(), subscr);
  if (idx < 0 || idx >= pVariableList.size())
  {
    throw std::runtime_error("ORToolsMIPVariable - incorrect array index " +
                             std::to_string(idx));
  }

  return pVariableList.at(idx);
}  // getVariable

MIPVariable::SPtr ORToolsMIPVariable::getVariableFromIndex(
    const std::string& varName, const std::vector<int>& subscr)
{
  auto orToolsVar = getVariable(subscr);
  ORToolsMIPVariable::SPtr var = nullptr;
  switch (pType)
  {
    case VarType::INT_VAR:
      var = std::make_shared<ORToolsMIPVariable>(varName, pLowerBound,
                                                 pUpperBound, false, pSolver);
      break;
    case VarType::CONTINUOUS_VAR:
      var = std::make_shared<ORToolsMIPVariable>(varName, pLowerBound,
                                                 pUpperBound, true, pSolver);
      break;
    default:
      assert(pType == VarType::BOOL_VAR);
      var = std::make_shared<ORToolsMIPVariable>(varName, pSolver);
      break;
  }
  var->setObjectiveCoeff(this->getObjectiveCoeff());
  return var;
}  // getVariableFromIndex

}  // namespace optilab
