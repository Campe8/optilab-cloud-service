#include "or_tools_engine/or_tools_vrp_engine.hpp"

#include <spdlog/spdlog.h>

#include <stdexcept>  // for std::runtime_error

#include "data_structure/json/json.hpp"
#include "engine/engine_constants.hpp"
#include "engine/engine_utils.hpp"
#include "or_tools_engine/or_tools_utilities.hpp"
#include "utilities/timer.hpp"

namespace
{
/// Waiting value for an idle processor
constexpr int kWaitOnIdleProcessor{-1};
}  // namespace

namespace optilab
{
ORToolsVRPEngine::ORToolsVRPEngine(const std::string& engineId,
                                   const EngineCallbackHandler::SPtr& handler)
    : pEngineId(engineId), pCallbackHandler(handler)

{
  if (engineId.empty())
  {
    const std::string errMsg = "ORToolsVRPEngine - empty engine identifier";
    throw std::invalid_argument(errMsg);
  }

  if (!handler)
  {
    const std::string errMsg = "ORToolsVRPEngine - engine callback handler";
    throw std::invalid_argument(errMsg);
  }

  timer::Timer timer;

  pVRPResult = std::make_shared<ortoolsengine::ORToolsVRPResult>();

  pMetricsRegister = std::make_shared<MetricsRegister>();

  // Builds the optimizer running ORTools VRP models
  buildOptimizer();
  assert(pOptimizer);

  // Start  the asynchronous thread in the optimizer
  pOptimizer->startUp();

  pActiveEngine = true;

  pEngineCreationTimeMsec = timer.getWallClockTimeMsec();
}

ORToolsVRPEngine::~ORToolsVRPEngine()
{
  try
  {
    turnDown();
  }
  catch (...)
  {
    // Do not throw in destructor
    spdlog::warn("ORToolsCPEngine - thrown in destructor");
  }
}

void ORToolsVRPEngine::turnDown()
{
  if (pActiveEngine)
  {
    pActiveEngine = false;
    pOptimizer->tearDown();
    pOptimizer.reset();
  }
}  // turnDown

void ORToolsVRPEngine::buildOptimizer()
{
  // Create and set the factory for ORTools CP processors
  ORToolsProcessorFactory procFactory(pVRPResult);
  procFactory.setProcessorType(EngineClassType::EC_VRP);
  pOptimizer = std::make_shared<ORToolsOptimizer>(pEngineId, procFactory,
                                                  pMetricsRegister);
}  // buildOptimizer

void ORToolsVRPEngine::registerModel(const OptimizerModel& protoModel)
{
  const std::string errMsg =
      "ORToolsVRPEngine - registerModel: "
      "registering a protobuf CP model not yet supported ";
  spdlog::error(errMsg);
  throw std::runtime_error(errMsg);
}  // registerModel

void ORToolsVRPEngine::registerModel(const std::string& vrpModel)
{
  if (vrpModel.empty())
  {
    throw std::runtime_error(
        "ORToolsVRPEngine - "
        "registering an empty model for engine: " +
        pEngineId);
  }

  // Start timer for loading the model
  timer::Timer timer;

  // Load the model into the optimizer
  try
  {
    const bool keepJSONFormat = true;
    pOptimizer->loadModel(vrpModel, keepJSONFormat);
  }
  catch (std::exception& ex)
  {
    const std::string errMsg =
        "ORToolsVRPEngine - registerModel: "
        "error while loading the model on engine " +
        pEngineId + " " + std::string(ex.what());
    spdlog::error(errMsg);
    throw;
  }
  catch (...)
  {
    const std::string errMsg =
        "ORToolsVRPEngine - registerModel: "
        "undefined error while loading the model on engine " +
        pEngineId;
    spdlog::error(errMsg);
    throw;
  }

  pLoadModelLatencyMsec = timer.getWallClockTimeMsec();

  // Reset solution
  std::dynamic_pointer_cast<ortoolsengine::ORToolsVRPResult>(pVRPResult)
      ->clear();
}  // registerModel

void ORToolsVRPEngine::engineWait(int timeoutMsec)
{
  if (pActiveEngine)
  {
    pOptimizer->waitOnTaskCompletion(timeoutMsec);
  }
}  // engineWait

void ORToolsVRPEngine::notifyEngine(const ortoolsengine::ORToolsEvent& event)
{
  switch (event.getType())
  {
    case ortoolsengine::ORToolsEvent::EventType::kRunEngine:
    {
      processRunModelEvent();
      break;
    }
    case ortoolsengine::ORToolsEvent::EventType::kSolutions:
    {
      processCollectSolutionsEvent(event.getNumSolutions());
      break;
    }
    default:
    {
      assert(event.getType() ==
             ortoolsengine::ORToolsEvent::EventType::kInterruptEngine);
      processInterruptEngineEvent();
      break;
    }
  }
}  // notifyEngine

void ORToolsVRPEngine::processRunModelEvent()
{
  // Return if the engine is not active
  if (!pActiveEngine)
  {
    spdlog::warn(
        "ORToolsVRPEngine - processRunModelEvent: "
        "inactive engine, returning " +
        pEngineId);
    return;
  }

  timer::Timer timer;

  // Wait on previous tasks to complete.
  // For example, it waits on model registration
  engineWait(kWaitOnIdleProcessor);

  // Trigger the engine to run and return
  pOptimizer->runOptimizer();

  // Latency for running the optimizer
  pOptimizerLatencyMsec = timer.getWallClockTimeMsec();
}  // processRunModelEvent

void ORToolsVRPEngine::processInterruptEngineEvent()
{
  if (!pActiveEngine)
  {
    spdlog::warn(
        "ORToolsVRPEngine - processInterruptEngineEvent: "
        "inactive engine, returning " +
        pEngineId);
    return;
  }

  pOptimizer->interruptOptimizer();
}  // processKillEngineEvent

void ORToolsVRPEngine::processCollectSolutionsEvent(int numSolutions)
{
  // @note it is assumed that the processor
  // is not currently adding result into the solution
  ortoolsengine::ORToolsVRPResult::SPtr res =
      std::dynamic_pointer_cast<ortoolsengine::ORToolsVRPResult>(pVRPResult);

  // Get the solution in JSON format
  auto jsonSolution = ortoolsutils::buildVRPSolutionJson(res);

  // Callback method to send results back to the client
  pCallbackHandler->resultCallback(pEngineId, jsonSolution->toString());

  // Callback to send the metrics back to the client
  collectMetrics();
  auto jsonMetrics = engineutils::createMetricsJson(pMetricsRegister);

  // Callback method to send metrics back to the client
  pCallbackHandler->metricsCallback(pEngineId, jsonMetrics->toString());
}  // processRunModelEvent

void ORToolsVRPEngine::collectMetrics()
{
  assert(pMetricsRegister);
  pMetricsRegister->setMetric(jsonmetrics::ENGINE_CREATION_TIME_MSEC,
                              pEngineCreationTimeMsec);
  pMetricsRegister->setMetric(jsonmetrics::LOAD_MODEL_LATENCY_MSEC,
                              pLoadModelLatencyMsec);
}  // collectMetrics

}  // namespace optilab
