// Base class
#include "or_tools_engine/or_tools_mip_engine.hpp"

#include <spdlog/spdlog.h>

#include <stdexcept>  // for std::runtime_error

#include "data_structure/json/json.hpp"
#include "engine/engine_constants.hpp"
#include "engine/engine_utils.hpp"
#include "or_tools_engine/or_tools_utilities.hpp"
#include "utilities/timer.hpp"

namespace
{
/// Waiting value for an idle processor
constexpr int kWaitOnIdleProcessor{-1};
}  // namespace

namespace optilab
{
ORToolsMIPEngine::ORToolsMIPEngine(const std::string& engineId,
                                   const EngineCallbackHandler::SPtr& handler)
    : pEngineId(engineId), pCallbackHandler(handler)

{
  if (engineId.empty())
  {
    throw std::invalid_argument("ORToolsMIPEngine - empty engine identifier");
  }

  if (!handler)
  {
    throw std::invalid_argument(
        "ORToolsMIPEngine - empty engine callback handler");
  }

  timer::Timer timer;

  pMIPResult = std::make_shared<ortoolsengine::ORToolsMIPResult>();

  pMetricsRegister = std::make_shared<MetricsRegister>();

  // Builds the optimizer running ORTools CP models
  buildOptimizer();
  assert(pOptimizer);

  // Start  the asynchronous thread in the optimizer
  pOptimizer->startUp();

  pActiveEngine = true;

  pEngineCreationTimeMsec = timer.getWallClockTimeMsec();
}

ORToolsMIPEngine::~ORToolsMIPEngine()
{
  try
  {
    turnDown();
  }
  catch (...)
  {
    // Do not throw in destructor
    spdlog::warn("ORToolsMIPEngine - thrown in destructor");
  }
}

void ORToolsMIPEngine::turnDown()
{
  if (pActiveEngine)
  {
    pActiveEngine = false;
    pOptimizer->tearDown();
    pOptimizer.reset();
  }
}  // turnDown

void ORToolsMIPEngine::buildOptimizer()
{
  // Create and set the factory for ORTools CP processors
  ORToolsProcessorFactory procFactory(pMIPResult);
  procFactory.setProcessorType(EngineClassType::EC_MIP);
  pOptimizer = std::make_shared<ORToolsOptimizer>(pEngineId, procFactory,
                                                  pMetricsRegister);
}  // buildOptimizer

void ORToolsMIPEngine::registerModel(const OptimizerModel& protoModel)
{
  if (!pActiveEngine)
  {
    const std::string errMsg =
        "ORToolsMIPEngine - registerModel: "
        "trying to register a model on an inactive engine " +
        pEngineId;
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  // OR-TOOLs MIP engines only accept linear models
  if (!protoModel.has_linear_model())
  {
    const std::string errMsg =
        "ORToolsMIPEngine - loadModel on engine id " + pEngineId +
        ": unrecognized protobuf model message (needed LinearModelSpecProto)";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  // Start timer for loading the model
  timer::Timer timer;

  // Load the model into the optimizer
  try
  {
    pOptimizer->loadModel(protoModel);
  }
  catch (std::exception& ex)
  {
    const std::string errMsg =
        "ORToolsMIPEngine - registerModel: "
        "error while loading the protobuf model on engine " +
        pEngineId + " " + std::string(ex.what());
    spdlog::error(errMsg);
    throw;
  }
  catch (...)
  {
    const std::string errMsg =
        "ORToolsMIPEngine - registerModel: "
        "undefined error while loading the protobuf model on engine " +
        pEngineId;
    spdlog::error(errMsg);
    throw;
  }

  pLoadModelLatencyMsec = timer.getWallClockTimeMsec();

  // Reset solution
  std::dynamic_pointer_cast<ortoolsengine::ORToolsMIPResult>(pMIPResult)
      ->clear();
  pMIPResult->modelName = protoModel.linear_model().model_id();
}  // registerModel

void ORToolsMIPEngine::registerModel(const std::string& model)
{
  if (model.empty())
  {
    std::ostringstream ss;
    ss << "ORToolsMIPEngine - registerModel: "
          "registering an empty model for engine "
       << pEngineId;
    throw std::runtime_error(ss.str());
  }

  if (!pActiveEngine)
  {
    const std::string errMsg =
        "ORToolsMIPEngine - registerModel: "
        "trying to register a model on an inactive engine " +
        pEngineId;
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  // Start timer for loading the model
  timer::Timer timer;

  // Load the model into the optimizer
  try
  {
    pOptimizer->loadModel(model);
  }
  catch (std::exception& ex)
  {
    const std::string errMsg =
        "ORToolsMIPEngine - registerModel: "
        "error while loading the model on engine " +
        pEngineId + " " + std::string(ex.what());
    spdlog::error(errMsg);
    throw;
  }
  catch (...)
  {
    const std::string errMsg =
        "ORToolsMIPEngine - registerModel: "
        "undefined error while loading the model on engine " +
        pEngineId;
    spdlog::error(errMsg);
    throw;
  }

  pLoadModelLatencyMsec = timer.getWallClockTimeMsec();

  // Reset solution
  std::dynamic_pointer_cast<ortoolsengine::ORToolsMIPResult>(pMIPResult)
      ->clear();
  pMIPResult->modelName = pOptimizer->getModel()->getModelName();
}  // registerModel

void ORToolsMIPEngine::engineWait(int timeoutMsec)
{
  if (pActiveEngine)
  {
    pOptimizer->waitOnTaskCompletion(timeoutMsec);
  }
}  // engineWait

void ORToolsMIPEngine::notifyEngine(const ortoolsengine::ORToolsEvent& event)
{
  switch (event.getType())
  {
    case ortoolsengine::ORToolsEvent::EventType::kRunEngine:
    {
      processRunModelEvent();
      break;
    }
    case ortoolsengine::ORToolsEvent::EventType::kSolutions:
    {
      processCollectSolutionsEvent(event.getNumSolutions());
      break;
    }
    default:
    {
      assert(event.getType() ==
             ortoolsengine::ORToolsEvent::EventType::kInterruptEngine);
      processInterruptEngineEvent();
      break;
    }
  }
}  // notifyEngine

void ORToolsMIPEngine::processRunModelEvent()
{
  // Return if the engine is already active
  if (!pActiveEngine)
  {
    spdlog::warn(
        "ORToolsMIPEngine - processRunModelEvent: "
        "inactive engine, returning " +
        pEngineId);

    return;
  }

  timer::Timer timer;

  // Wait on previous tasks to complete.
  // For example, it waits on model registration
  engineWait(kWaitOnIdleProcessor);

  // Trigger the engine to run and return
  pOptimizer->runOptimizer();

  // Latency for running the optimizer
  pOptimizerLatencyMsec = timer.getWallClockTimeMsec();
}  // processRunModelEvent

void ORToolsMIPEngine::processInterruptEngineEvent()
{
  if (!pActiveEngine)
  {
    spdlog::warn(
        "ORToolsMIPEngine - processInterruptEngineEvent: "
        "inactive engine, returning " +
        pEngineId);
    return;
  }

  pOptimizer->interruptOptimizer();
}  // processKillEngineEvent

void ORToolsMIPEngine::processCollectSolutionsEvent(int numSolutions)
{
  // @note it is assumed that the processor
  // is not currently adding result into the solution
  ortoolsengine::ORToolsMIPResult::SPtr res =
      std::dynamic_pointer_cast<ortoolsengine::ORToolsMIPResult>(pMIPResult);
  if (!res)
  {
    const std::string errMsg =
        "ORToolsMIPEngine - processCollectSolutionsEvent: "
        "empty pointer to result " +
        pEngineId;
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  // Callback method to send results back to the client.
  // @note this was previously done using JSON replies:
  //   auto solutionJson = sciputils::buildMIPSolutionJson(res);
  //   pCallbackHandler->resultCallback(pEngineId, solutionJson->toString());
  pCallbackHandler->resultCallback(pEngineId, ortoolsutils::buildMIPSolutionProto(res));

  // Callback to send the metrics back to the client
  collectMetrics();
  auto jsonMetrics = engineutils::createMetricsJson(pMetricsRegister);

  // Callback method to send metrics back to the client
  pCallbackHandler->metricsCallback(pEngineId, jsonMetrics->toString());

  // Completed run
  pCallbackHandler->processCompletionCallback(pEngineId);
}  // processRunModelEvent

void ORToolsMIPEngine::collectMetrics()
{
  assert(pMetricsRegister);
  pMetricsRegister->setMetric(jsonmetrics::ENGINE_CREATION_TIME_MSEC,
                              pEngineCreationTimeMsec);
  pMetricsRegister->setMetric(jsonmetrics::LOAD_MODEL_LATENCY_MSEC,
                              pLoadModelLatencyMsec);
}  // collectMetrics

}  // namespace optilab
