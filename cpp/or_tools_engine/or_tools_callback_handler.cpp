#include "or_tools_engine/or_tools_callback_handler.hpp"

#include <iostream>

namespace optilab {

void ORToolsCallbackHandler::resultCallback(const std::string& engineId,
                                            const std::string& jsonResult)
{
  (void)engineId;
  std::cout << jsonResult << std::endl;
}  // solutionCallback

}  // namespace optilab
