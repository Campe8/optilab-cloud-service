#include "or_tools_engine/or_tools_mip_solver.hpp"

#include <stdexcept>   // for std::invalid_argument

#include <spdlog/spdlog.h>

namespace optilab {

ORToolsMIPSolver::ORToolsMIPSolver(const ORToolsMIPModel::SPtr& mipModel)
: pResultStatus(operations_research::MPSolver::ResultStatus::NOT_SOLVED)
{
  if (!mipModel)
  {
    throw std::invalid_argument("ORToolsMIPSolver - empty pointer to a MIP model");
  }

  // Get the model environment which, in the ORTools package, corresponds to a Solver instance
  pSolver = mipModel->getModelEnvironment();
  if (!pSolver)
  {
    throw std::invalid_argument("ORToolsMIPSolver - empty environment");
  }
}

bool ORToolsMIPSolver::interruptSearchProcess()
{
  const bool isInterrupted = pSolver->InterruptSolve();
  if (!isInterrupted)
  {
    // If the solver doesn't allow interruption, try to set the timeout to zero
    spdlog::warn("ORToolsMIPSolver - interruptSearchProcess: solver cannot be interrupted");
    pSolver->set_time_limit(1);
  }

  return isInterrupted;
}  // interruptSearchProcess

void ORToolsMIPSolver::solve()
{
  pResultStatus = pSolver->Solve();
}  // solve

operations_research::MPSolutionResponse ORToolsMIPSolver::getORToolsProtobufResult() const
{
  operations_research::MPSolutionResponse result;
  pSolver->FillSolutionResponseProto(&result);
  return result;
}  // getORToolsProtobufResult

double ORToolsMIPSolver::getObjectiveValue() const
{
  return (pSolver->Objective()).Value();
}  // getObjectiveValue

ortoolsengine::ORToolsMIPResult::ResultStatus ORToolsMIPSolver::getResultStatus() const
{
  switch (pResultStatus)
  {
    case operations_research::MPSolver::ResultStatus::NOT_SOLVED:
      return ortoolsengine::ORToolsMIPResult::ResultStatus::NOT_SOLVED;
    case operations_research::MPSolver::ResultStatus::OPTIMAL:
      return ortoolsengine::ORToolsMIPResult::ResultStatus::OPTIMUM;
    case operations_research::MPSolver::ResultStatus::FEASIBLE:
      return ortoolsengine::ORToolsMIPResult::ResultStatus::FEASIBLE;
    case operations_research::MPSolver::ResultStatus::INFEASIBLE:
      return ortoolsengine::ORToolsMIPResult::ResultStatus::INFEASIBLE;
    case operations_research::MPSolver::ResultStatus::UNBOUNDED:
      return ortoolsengine::ORToolsMIPResult::ResultStatus::UNBOUNDED;
    default:
      return ortoolsengine::ORToolsMIPResult::ResultStatus::NOT_SOLVED;
  }
}  // getResultStatus

}  // namespace optilab
