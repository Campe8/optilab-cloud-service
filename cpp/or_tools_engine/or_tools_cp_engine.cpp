// Base class
#include "or_tools_engine/or_tools_cp_engine.hpp"

#include <spdlog/spdlog.h>

#include <algorithm>  // for std::max
#include <cassert>
#include <memory>  // for std::make_shared
#include <sstream>
#include <stdexcept>  // for std::runtime_error
#include <vector>

#include "data_structure/json/json.hpp"
#include "engine/engine_constants.hpp"
#include "engine/engine_utils.hpp"
#include "or_tools_engine/or_tools_processor_factory.hpp"
#include "or_tools_engine/or_tools_utilities.hpp"
#include "utilities/timer.hpp"

namespace
{
/// Waiting value for an idle processor
constexpr int kWaitOnIdleProcessor{-1};
}  // namespace

namespace optilab
{
ORToolsCPEngine::ORToolsCPEngine(const std::string& engineId,
                                 const EngineCallbackHandler::SPtr& handler)
    : pEngineId(engineId), pCallbackHandler(handler)

{
  if (engineId.empty())
  {
    throw std::invalid_argument("ORToolsCPEngine - empty engine identifier");
  }

  if (!handler)
  {
    throw std::invalid_argument(
        "ORToolsCPEngine - empty engine callback handler");
  }

  timer::Timer timer;

  pCPResult = std::make_shared<ortoolsengine::ORToolsCPResult>();

  pMetricsRegister = std::make_shared<MetricsRegister>();

  // Builds the optimizer running ORTools CP models
  buildOptimizer();
  assert(pOptimizer);

  // Start  the asynchronous thread in the optimizer
  pOptimizer->startUp();

  pActiveEngine = true;

  pEngineCreationTimeMsec = timer.getWallClockTimeMsec();
}

ORToolsCPEngine::~ORToolsCPEngine()
{
  try
  {
    turnDown();
  }
  catch (...)
  {
    // Do not throw in destructor
    spdlog::warn("ORToolsCPEngine - thrown in destructor");
  }
}

void ORToolsCPEngine::turnDown()
{
  if (pActiveEngine)
  {
    pActiveEngine = false;
    pOptimizer->tearDown();
    pOptimizer.reset();
  }
}  // turnDown

void ORToolsCPEngine::buildOptimizer()
{
  // Create and set the factory for ORTools CP processors
  ORToolsProcessorFactory procFactory(pCPResult);
  procFactory.setProcessorType(EngineClassType::EC_CP);
  pOptimizer = std::make_shared<ORToolsOptimizer>(pEngineId, procFactory,
                                                  pMetricsRegister);
}  // buildOptimizer

void ORToolsCPEngine::registerModel(const OptimizerModel& protoModel)
{
  const std::string errMsg =
      "ORToolsCPEngine - registerModel: "
      "registering a protobuf CP model not yet supported ";
  spdlog::error(errMsg);
  throw std::runtime_error(errMsg);
}  // registerModel

void ORToolsCPEngine::registerModel(const std::string& model)
{
  if (model.empty())
  {
    std::ostringstream ss;
    ss << "ORToolsCPEngine - registerModel: "
          "registering an empty model for engine "
       << pEngineId;
    throw std::runtime_error(ss.str());
  }

  if (!pActiveEngine)
  {
    const std::string errMsg =
        "ORToolsCPEngine - registerModel: "
        "trying to register a model on an inactive engine " +
        pEngineId;
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  // Start timer for loading the model
  timer::Timer timer;

  // Load the model into the optimizer
  try
  {
    pOptimizer->loadModel(model);
  }
  catch (std::exception& ex)
  {
    const std::string errMsg =
        "ORToolsCPEngine - registerModel: "
        "error while loading the model on engine " +
        pEngineId + " " + std::string(ex.what());
    spdlog::error(errMsg);
    throw;
  }
  catch (...)
  {
    const std::string errMsg =
        "ORToolsCPEngine - registerModel: "
        "undefined error while loading the model on engine " +
        pEngineId;
    spdlog::error(errMsg);
    throw;
  }

  pLoadModelLatencyMsec = timer.getWallClockTimeMsec();

  // Reset solution
  std::dynamic_pointer_cast<ortoolsengine::ORToolsCPResult>(pCPResult)->clear();
}  // registerModel

void ORToolsCPEngine::engineWait(int timeoutMsec)
{
  if (pActiveEngine)
  {
    pOptimizer->waitOnTaskCompletion(timeoutMsec);
  }
}  // engineWait

void ORToolsCPEngine::notifyEngine(const ortoolsengine::ORToolsEvent& event)
{
  switch (event.getType())
  {
    case ortoolsengine::ORToolsEvent::EventType::kRunEngine:
    {
      processRunModelEvent();
      break;
    }
    case ortoolsengine::ORToolsEvent::EventType::kSolutions:
    {
      processCollectSolutionsEvent(event.getNumSolutions());
      break;
    }
    default:
    {
      assert(event.getType() ==
             ortoolsengine::ORToolsEvent::EventType::kInterruptEngine);
      processInterruptEngineEvent();
      break;
    }
  }
}  // notifyEngine

void ORToolsCPEngine::processRunModelEvent()
{
  // Return if the engine is not active
  if (!pActiveEngine)
  {
    spdlog::warn(
        "ORToolsCPEngine - processRunModelEvent: "
        "inactive engine, returning " +
        pEngineId);
    return;
  }

  timer::Timer timer;

  // Wait on previous tasks to complete.
  // For example, it waits on model registration
  engineWait(kWaitOnIdleProcessor);

  // Trigger the engine to run and return
  pOptimizer->runOptimizer();

  // Latency for running the optimizer
  pOptimizerLatencyMsec = timer.getWallClockTimeMsec();
}  // processRunModelEvent

void ORToolsCPEngine::processInterruptEngineEvent()
{
  if (!pActiveEngine)
  {
    spdlog::warn(
        "ORToolsCPEngine - processInterruptEngineEvent: "
        "inactive engine, returning " +
        pEngineId);
    return;
  }

  pOptimizer->interruptOptimizer();
}  // processInterruptEngineEvent

void ORToolsCPEngine::processCollectSolutionsEvent(int numSolutions)
{
  // @note it is assumed that the processor
  // is not currently adding result into the solution
  auto jsonSolution = ortoolsutils::buildCPSolutionJson(
      std::dynamic_pointer_cast<ortoolsengine::ORToolsCPResult>(pCPResult),
      numSolutions);

  // Callback method to send results back to the client
  pCallbackHandler->resultCallback(pEngineId, jsonSolution->toString());

  // Callback to send the metrics back to the client
  collectMetrics();
  auto jsonMetrics = engineutils::createMetricsJson(pMetricsRegister);

  // Callback method to send metrics back to the client
  pCallbackHandler->metricsCallback(pEngineId, jsonMetrics->toString());

  // Completed run
  pCallbackHandler->processCompletionCallback(pEngineId);
}  // processRunModelEvent

void ORToolsCPEngine::collectMetrics()
{
  assert(pMetricsRegister);
  pMetricsRegister->setMetric(jsonmetrics::ENGINE_CREATION_TIME_MSEC,
                              pEngineCreationTimeMsec);
  pMetricsRegister->setMetric(jsonmetrics::LOAD_MODEL_LATENCY_MSEC,
                              pLoadModelLatencyMsec);
}  // collectMetrics

}  // namespace optilab
