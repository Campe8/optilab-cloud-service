#include "or_tools_engine/employee_scheduling_solver.hpp"

#include <stdexcept>   // for std::invalid_argument

#include <spdlog/spdlog.h>

namespace {

constexpr int kSolutionShiftPersonIdx = 0;
constexpr int kSolutionShiftTurnIdx = 1;
constexpr int kSolutionShiftRequestIdx = 2;

}  // namespace

namespace optilab {

EmployeeSchedulingSolver::EmployeeSchedulingSolver(
        const toolbox::EmployeeSchedulingModelProto& smodel)
: pSATModelBuilder(nullptr)
{
  initSolver(smodel);
}

void EmployeeSchedulingSolver::initSolver(const toolbox::EmployeeSchedulingModelProto& smodel)
{
  // Instantiate a the scheduling builder
  pSATModelBuilder = std::make_shared<ModelBuilder>();

  // Create the internal CP/SAT model
  pNumPersonnel = smodel.num_employees();
  pNumDays = smodel.num_days();
  pNumShifts = smodel.num_shift_per_day();

  // Create a matrix to collect all variables
  pShiftMatrix.reserve(pNumPersonnel);
  for (int pers = 0; pers < pNumPersonnel; ++pers)
  {
    std::vector<std::vector<operations_research::sat::BoolVar>> daysList;
    for (int day = 0; day < pNumDays; ++day)
    {
      std::vector<operations_research::sat::BoolVar> shiftList;
      for (int shift = 0; shift < pNumShifts; ++shift)
      {
        const std::string varName = "shift_" +
            std::to_string(pers) + "_" +
            std::to_string(day) + "_" +
            std::to_string(shift);
        shiftList.push_back((pSATModelBuilder->NewBoolVar()).WithName(varName));
      }
      daysList.push_back(shiftList);
    }
    pShiftMatrix.push_back(daysList);
  }

  // Add constraints.
  // 1) Each shift is assigned to one person
  for (int day = 0; day < pNumDays; ++day)
  {
    for (int shift = 0; shift < pNumShifts; ++shift)
    {
      std::vector<operations_research::sat::BoolVar> shiftPerPerson;
      shiftPerPerson.resize(pNumPersonnel);
      for (int pers = 0; pers < pNumPersonnel; ++pers)
      {
        shiftPerPerson[pers] = pShiftMatrix[pers][day][shift];
      }
      operations_research::sat::LinearExpr expr =
          operations_research::sat::LinearExpr::BooleanSum(shiftPerPerson);
      pSATModelBuilder->AddEquality(expr, 1);
    }
  }

  // 2) Each person works at most one shift per day
  for (int pers = 0; pers < pNumPersonnel; ++pers)
  {
    for (int day = 0; day < pNumDays; ++day)
    {
      std::vector<operations_research::sat::BoolVar> shiftPerDay;
      shiftPerDay.resize(pNumShifts);
      for (int shift = 0; shift < pNumShifts; ++shift)
      {
        shiftPerDay[shift] = pShiftMatrix[pers][day][shift];
      }
      operations_research::sat::LinearExpr expr =
          operations_research::sat::LinearExpr::BooleanSum(shiftPerDay);
      pSATModelBuilder->AddLessOrEqual(expr, 1);
    }
  }

  const int totNumShifts = pNumShifts * pNumDays;
  const int minShiftPerPerson = (pNumShifts * pNumDays) / pNumPersonnel;
  const int maxShiftPerPerson = minShiftPerPerson + 1;
  for (int pers = 0; pers < pNumPersonnel; ++pers)
  {
    std::vector<operations_research::sat::BoolVar> allShifts;
    allShifts.resize(totNumShifts);
    for (int day = 0; day < pNumDays; ++day)
    {
      for (int shift = 0; shift < pNumShifts; ++shift)
      {
        allShifts[shift + (day * pNumShifts)] = pShiftMatrix[pers][day][shift];
      }
    }

    // Each person has to have assigned at least "minShiftPerPerson",
    // i.e., the total number of shifts divided by the number of personnel
    operations_research::sat::LinearExpr expr =
        operations_research::sat::LinearExpr::BooleanSum(allShifts);
    pSATModelBuilder->AddLessOrEqual(minShiftPerPerson, expr);

    // Each person can work at most "minShiftPerPerson" + 1,
    // since the number of shifts may not divide equally all the personnel
    pSATModelBuilder->AddLessOrEqual(expr, maxShiftPerPerson);
  }

  // Add shift requests if any
  if (smodel.shift_preference_list_size() > 0)
  {
    const int totSize = pNumPersonnel * pNumDays * pNumShifts;
    std::vector<operations_research::sat::BoolVar> requests;
    std::vector<int64> requestsVals;
    requests.reserve(totSize);
    requestsVals.resize(totSize, 0);

    for (int idx = 0; idx < smodel.shift_preference_list_size(); ++idx)
    {
      const auto& pref = smodel.shift_preference_list(idx);
      requestsVals[pref.employee_id() * pNumDays * pNumShifts + pref.day() * pNumShifts + pref.shift()] = 1;
      pPreferenceMap[pref.employee_id()].push_back({pref.day(), pref.shift()});
    }

    for (int pers = 0; pers < pNumPersonnel; ++pers)
    {
      for (int day = 0; day < pNumDays; ++day)
      {
        for (int shift = 0; shift < pNumShifts; ++shift)
        {
          requests.push_back(pShiftMatrix[pers][day][shift]);
        }
      }
    }

    // Create the objective
    operations_research::sat::LinearExpr expr =
        operations_research::sat::LinearExpr::BooleanScalProd(requests, requestsVals);

    // Add the maximization objective
    pSATModelBuilder->Maximize(expr);
  }
}  // initSolver

bool EmployeeSchedulingSolver::interruptSearchProcess()
{
  return false;
}  // interruptSearchProcess

void EmployeeSchedulingSolver::solve()
{
  pSolution = operations_research::sat::Solve(pSATModelBuilder->Build());
}  // solve

bool EmployeeSchedulingSolver::employeeRequestedDayAndShift(int id, int day, int shift)
{
  if (pPreferenceMap.find(id) == pPreferenceMap.end()) return false;

  const auto& entry = pPreferenceMap[id];
  for (const auto& pref : entry)
  {
    if (pref.first == day && pref.second == shift) return true;
  }
  return false;
}  // employeeRequestedDayAndShift

void EmployeeSchedulingSolver::storeSolution(
        toolbox::schedulingengine::EmployeesSchedulingResult& sol)
{
  // Set status of the solution
  switch (pSolution.status())
  {
    case ::operations_research::sat::CpSolverStatus::FEASIBLE:
      sol.status = toolbox::schedulingengine::SchedulingResult::ResultStatus::SUCCESS;
      break;
    case ::operations_research::sat::CpSolverStatus::INFEASIBLE:
      sol.status = toolbox::schedulingengine::SchedulingResult::ResultStatus::FAIL;
      break;
    case ::operations_research::sat::CpSolverStatus::MODEL_INVALID:
      sol.status = toolbox::schedulingengine::SchedulingResult::ResultStatus::INVALID;
      break;
    case ::operations_research::sat::CpSolverStatus::OPTIMAL:
      sol.status = toolbox::schedulingengine::SchedulingResult::ResultStatus::SUCCESS;
      break;
    case ::operations_research::sat::CpSolverStatus::UNKNOWN:
    default:
      sol.status = toolbox::schedulingengine::SchedulingResult::ResultStatus::NOT_SOLVED;
      break;
  }

  if (sol.status == toolbox::schedulingengine::SchedulingResult::ResultStatus::SUCCESS)
  {
    // Set solution
    for (int day = 0; day < pNumDays; ++day)
    {
      toolbox::schedulingengine::EmployeesSchedulingResult::EmployeeDaySchedule sched;
      sched.day = day;
      for (int eid = 0; eid < pNumPersonnel; ++eid)
      {
        for (int shift = 0; shift < pNumShifts; ++shift)
        {
          const bool turn = operations_research::sat::SolutionBooleanValue(
                  pSolution, pShiftMatrix[eid][day][shift]);
          if (turn)
          {
            // The employee "eid" has the turn on this "day" for this "shift"
            toolbox::schedulingengine::EmployeesSchedulingResult::EmployeeDayShift edayshift;
            edayshift.employeeId = eid;
            edayshift.shift = shift;
            edayshift.requestSatisfied = employeeRequestedDayAndShift(eid, day, shift);
            sched.shiftSchedule.push_back(edayshift);
          }
        }
      }
      sol.daySchedule.push_back(sched);
    }  // day
  }
}  // storeSolution

}  // namespace optilab
