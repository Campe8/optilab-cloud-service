//
// Copyright OptiLab 2019. All rights reserved.
//
// Utilities for OR-tools
//

#pragma once

#include <cstddef>  // for std::size_t
#include <memory>   // for std::shared_ptr
#include <string>
#include <vector>
#include <map>
#include <utility>  // for std::pair

#include <boost/logic/tribool.hpp>

#include "data_structure/json/json.hpp"
#include "data_structure/metrics/metrics_register.hpp"
#include "model/model_object_constraint.hpp"
#include "model/model_object_variable.hpp"
#include "optilab_protobuf/linear_model.pb.h"
#include "or_tools_engine/or_tools_cp_constraint.hpp"
#include "or_tools_engine/or_tools_cp_variable.hpp"
#include "or_tools_engine/or_tools_mip_variable.hpp"
#include "ortools/constraint_solver/constraint_solveri.h"
#include "ortools/constraint_solver/routing.h"
#include "ortools/linear_solver/linear_solver.h"
#include "ortools/sat/cp_model.h"
#include "solver/cp_constraint.hpp"
#include "solver/cp_model.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

namespace ortoolsengine {

struct SYS_EXPORT_STRUCT ORToolsWork {
using SPtr = std::shared_ptr<ORToolsWork>;

enum WorkType {
  /// Runs the optimizer on the loaded model
  kRunOptimizer,
  kWorkTypeUndef
};

ORToolsWork(WorkType wtype, MetricsRegister::SPtr metReg)
: workType(wtype), metricsRegister(metReg)
{
}

WorkType workType;
MetricsRegister::SPtr metricsRegister;
};

class SYS_EXPORT_CLASS ORToolsEvent {
 public:
  enum EventType {
    /// Starts the solving process
    kRunEngine,
    /// Kills any ongoing computation
    kInterruptEngine,
    /// Collects solutions
    kSolutions
  };

 public:
  explicit ORToolsEvent(EventType aType)
  : pEventType(aType)
  {
  }

  inline EventType getType() const noexcept
  {
    return pEventType;
  }

  inline void setNumSolutions(int numSolutions) noexcept
  {
    pNumSolutions = numSolutions;
  }

  inline int getNumSolutions() const noexcept
  {
    return pNumSolutions;
  }

 private:
  /// Type of this event
  EventType pEventType;

  /// Number of solutions for "kSolutions" events
  int pNumSolutions{0};
};

struct SYS_EXPORT_STRUCT ORToolsResult {
  using SPtr = std::shared_ptr<ORToolsResult>;

  virtual ~ORToolsResult() = default;

  /// Name of the model producing this result
  std::string modelName;
};

struct SYS_EXPORT_STRUCT ORToolsCPResult : public ORToolsResult {
  using SPtr = std::shared_ptr<ORToolsCPResult>;

  /// A solution is a map between variables (variable id.) and a list
  /// of assignments.
  /// Each variable has a list of assignments since the variable can have more than
  /// on dimension. In turn, an assignment is a pair of values [lower_bound, upper_bound]
  using Solution = std::map<std::string, std::vector<std::pair<int64, int64>>>;

  /// A result can contain 0, 1, ..., n solutions
  std::vector<Solution> solutionList;

  /// Flag indicating whether or not this model is satisfiable
  boost::logic::tribool isSatModel{boost::logic::indeterminate};

  inline std::size_t getNumSolutions() const noexcept { return solutionList.size(); }

  /// Clear the results
  void clear()
  {
    solutionList.clear();
    isSatModel = boost::logic::indeterminate;
  }
};

struct SYS_EXPORT_STRUCT ORToolsMIPResult : public ORToolsResult {
  /// Result status
  enum ResultStatus {
    /// Solution is the global optimum
    OPTIMUM = 0,

    /// Feasible solution but not optimal
    FEASIBLE,

    /// Infeasible model, no result
    INFEASIBLE,

    /// Model proven to be unbounded
    UNBOUNDED,

    /// Model not yet solved
    NOT_SOLVED
  };

  using SPtr = std::shared_ptr<ORToolsMIPResult>;

  /// Collection of solutions.
  /// @note an assignment is a list of pairs of values to a variable which may be scalar or
  /// a multi-dimensional variable
  using VarAssign = std::vector<std::pair<double, double>>;
  using Solution = std::map<std::string, VarAssign>;

  /// Vector of objective values, in the order declared in the model
  using ObjectiveValues = std::vector<double>;

  /// A MIP solution is a pair of objective values and variable assignments
  std::pair<ObjectiveValues, Solution> solution;

  /// Status of the solution
  ResultStatus resultStatus{ResultStatus::NOT_SOLVED};

  /// Clear the results
  void clear()
  {
    solution.first.clear();
    solution.second.clear();
    resultStatus = ResultStatus::NOT_SOLVED;
  }
};

struct SYS_EXPORT_STRUCT ORToolsVRPResult : public ORToolsResult {
  using SPtr = std::shared_ptr<ORToolsVRPResult>;

  struct VRPRoute {
    /// Identifier of the vehicle on this route
    int vehicleId{-1};

    /// Route of this vehicle
    std::vector<int64> route;

    /// Load of this vehicle when load constraints are present
    std::vector<int64> load;

    /// Total distance of this route
    int64 totDistance{-1};

    /// Total load carried by this vehicle when load constraints are present
    int64 totLoad{-1};
  };

  /// Status of the solver upon termination
  operations_research::RoutingModel::Status
  status{operations_research::RoutingModel::Status::ROUTING_NOT_SOLVED};

  /// Total distance of all routes
  int64 totDistance{-1};

  /// Total load carried when load constraints are present
  int64 totLoad{-1};

  /// List of routes for each vehicle
  std::vector<VRPRoute> routeList;

  /// Clear the results
  void clear()
  {
    totDistance = -1;
    totLoad = -1;
    routeList.clear();
    status = operations_research::RoutingModel::Status::ROUTING_NOT_SOLVED;
  }
};

}  // namespace ortoolsengine

namespace ortoolsutils {

/// Returns a Boolean/Integer
SYS_EXPORT_FCN optilab::ORToolsCPVariable::SPtr getCPVar(
    const optilab::ModelObjectVariable::SPtr& varPtr, operations_research::Solver* solver);

SYS_EXPORT_FCN optilab::ORToolsCPConstraint::SPtr getCPCon(
    const optilab::ModelObjectConstraint::SPtr& conPtr, operations_research::Solver* solver,
    CPModel* model);

SYS_EXPORT_FCN operations_research::Solver::IntVarStrategy getVarSelectionStrategyFromString(
    const std::string& varStrategy);

SYS_EXPORT_FCN operations_research::Solver::IntValueStrategy getValSelectionStrategyFromString(
    const std::string& valStrategy);

/// Returns a Boolean/Integer/continuous variable
SYS_EXPORT_FCN optilab::ORToolsMIPVariable::SPtr getMIPVar(
    const optilab::ModelObjectVariable::SPtr& varPtr, operations_research::MPSolver* solver);

/// Builds a JSON object representing the set of CP solutions given as parameter
SYS_EXPORT_FCN JSON::SPtr buildCPSolutionJson(ortoolsengine::ORToolsCPResult::SPtr res,
                                              int numSolutions);

/// Builds a JSON object representing the MIP solution given as parameter
SYS_EXPORT_FCN JSON::SPtr buildMIPSolutionJson(ortoolsengine::ORToolsMIPResult::SPtr res);

/// Builds a protobuf object representing the MIP solution given as parameter
SYS_EXPORT_FCN LinearModelSolutionProto buildMIPSolutionProto(
    ortoolsengine::ORToolsMIPResult::SPtr res);

/// Builds a JSON object representing the VRP solution given as argument
SYS_EXPORT_FCN JSON::SPtr buildVRPSolutionJson(ortoolsengine::ORToolsVRPResult::SPtr res);

}  // namespace ortoolsutils

}  // namespace optilab
