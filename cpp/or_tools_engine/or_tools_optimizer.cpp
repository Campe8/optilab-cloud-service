#include "or_tools_engine/or_tools_optimizer.hpp"

#include <spdlog/spdlog.h>

#include <cassert>
#include <stdexcept>  // for std::invalid_argument

namespace optilab
{
ORToolsOptimizer::ORToolsOptimizer(const std::string& engineName,
                                   const ORToolsProcessorFactory& procFactory,
                                   MetricsRegister::SPtr metricsRegister)
    : BaseClass(engineName),
      pOptimizerModel(nullptr),
      pORToolsProcessor(nullptr),
      pProcessorFactory(procFactory),
      pMetricsRegister(metricsRegister)
{
  if (!pMetricsRegister)
  {
    throw std::invalid_argument(
        "ORToolsOptimizer - empty pointer to the metrics register");
  }

  // Initialize the engine,
  // e.g., the internal pipeline
  BaseClass::init();
}

void ORToolsOptimizer::loadModel(const OptimizerModel& protoModel)
{
  // Load the model into the optimizer
  assert(pORToolsProcessor);
  pORToolsProcessor->loadModelAndCreateSolver(protoModel);
}  // loadModel

void ORToolsOptimizer::loadModel(const std::string& jsonModel, bool keepJSON)
{
  // Load the model into the optimizer
  assert(pORToolsProcessor);

  if (keepJSON)
  {
    pORToolsProcessor->loadJSONModelAndCreateSolver(jsonModel);
  }
  else
  {
    // Load the model from the given JSON
    loadModelImpl(jsonModel);
    assert(pOptimizerModel);
    pORToolsProcessor->loadModelAndCreateSolver(pOptimizerModel);
  }
}  // loadModel

void ORToolsOptimizer::loadModelImpl(const std::string& jsonModel)
{
  if (jsonModel.empty())
  {
    throw std::invalid_argument(
        "ORToolsOptimizer - loadModelImpl: empty JSON model");
  }

  pOptimizerModel = std::make_shared<Model>();
  pOptimizerModel->loadModelFromJson(jsonModel);
}  // loadModelImpl

void ORToolsOptimizer::runOptimizer()
{
  if (!isActive())
  {
    const std::string errMsg =
        "ORToolsOptimizer - runOptimizer: optimizer is not active";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  Work::SPtr work = std::make_shared<Work>(
      ortoolsengine::ORToolsWork::WorkType::kRunOptimizer, pMetricsRegister);
  pushTask(work);
}  // runOptimizer

bool ORToolsOptimizer::interruptOptimizer()
{
  assert(pORToolsProcessor);
  return pORToolsProcessor->interruptSolver();
}  // interruptOptimizer

ProcessorPipeline::SPtr ORToolsOptimizer::buildPipeline(int)
{
  ProcessorPipeline::SPtr pipeline = std::make_shared<ProcessorPipeline>();
  pORToolsProcessor = pProcessorFactory.buildProcessor();
  pipeline->pushBackProcessor(pORToolsProcessor);
  return pipeline;
}  // buildPipeline

}  // namespace optilab
