//
// Copyright OptiLab 2019. All rights reserved.
//
// Processor for OR-Tools CP engines.
//

#pragma once

#include <cstdint>  // for uint64_t
#include <memory>   // for std::shared_ptr
#include <string>
#include <map>
#include <utility>  // for std::pair
#include <vector>

#include <boost/logic/tribool.hpp>
#include <boost/thread.hpp>

#include "model/model.hpp"
#include "or_tools_engine/or_tools_cp_model.hpp"
#include "or_tools_engine/or_tools_cp_solver.hpp"
#include "or_tools_engine/or_tools_processor.hpp"
#include "or_tools_engine/or_tools_utilities.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS OrToolsCPProcessor : public ORToolsProcessor {
public:
  using SPtr = std::shared_ptr<OrToolsCPProcessor>;

public:
  OrToolsCPProcessor(ortoolsengine::ORToolsResult::SPtr result);

  /// Loads the model and creates a solver to run on it.
  /// @note throw std::invalid_argument on empty model
  void loadModelAndCreateSolver(const Model::SPtr& model) override;

  /// Interrupts the internal solving process (if running).
  /// The solver remains in a non-executable state until another model is loaded
  bool interruptSolver() override;

  /// Returns a copy of the current result
  ortoolsengine::ORToolsCPResult getCurrentResult();

protected:
  using Work = ortoolsengine::ORToolsWork::SPtr;

  /// Lock type on the internal mutex
  using LockGuard = boost::lock_guard<boost::mutex>;

protected:
  void processWork(Work work) override;

private:
  /// The ORTools CP model to solve
  ORToolsCPModel::SPtr pCPModel;

  /// ORTools solver
  ORToolsCPSolver::SPtr pCPSolver;

  /// Pointer to the downcast CP result instance
  ortoolsengine::ORToolsCPResult::SPtr pCPResult;

  /// Mutex synch.ing on the state of the result
  boost::mutex pResultMutex;

  /// Boolean flag indicating whether or not the model is a satisfiable model
  boost::logic::tribool pSatModel{boost::logic::indeterminate};

  /// Triggers the execution of the solver
  /// @note this doesn't check whether an engine is currently running or not.
  /// It is responsibility of the caller to handle that logic
  void runSolver(Work& work);

  /// Runs constraint propagation only and returns whether it failed or not
  bool runConstraintPropagationOnly();

  /// Runs the solver to collect the give number of solution
  void runSolver(int numSol);

  /// Utility function: set the model run by the engine as unsatisfiable model
  void storeUnsatModel();

  /// Utility function: set the model run by the engine as satisfiable model
  void storeSatModel();

  /// Utility function: store current solution
  void storeSolution();
};

}  // namespace optilab
