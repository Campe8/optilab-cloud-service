#include "or_tools_engine/or_tools_cp_variable.hpp"

#include <algorithm>  // for std::max
#include <cassert>
#include <stdexcept>  // for std::invalid_argument

#include <spdlog/spdlog.h>

#include "or_tools_engine/or_tools_utilities.hpp"
#include "utilities/variables.hpp"

namespace optilab {

ORToolsCPVariable::ORToolsCPVariable(const std::string& name, operations_research::Solver* solver)
: CPVariable(name), pSolver(solver)
{
  if (!pSolver)
  {
    throw std::invalid_argument("ORToolsCPVariable - pointer to solver is NULL");
  }
}

ORToolsCPVariable::ORToolsCPVariable(const std::string& name, const CPDomain& domain,
                                     operations_research::Solver* solver)
: CPVariable(name, domain), pSolver(solver)
{
  if (!pSolver)
  {
    throw std::invalid_argument("ORToolsCPVariable - pointer to solver is NULL");
  }

  // Build the variable and store its index into the list of variable
  pVariableList.push_back(buildVariableAndAddItToSolver(getDomain()));
}

ORToolsCPVariable::ORToolsCPVariable(const std::string& name, const CPDomain& domain, int len,
                                     operations_research::Solver* solver)
: CPVariable(name, domain, len), pSolver(solver)
{
  if (!pSolver)
  {
    throw std::invalid_argument("ORToolsCPVariable - pointer to solver is NULL");
  }

  for (int vIdx = 0; vIdx < len; ++vIdx)
  {
    pVariableList.push_back(buildVariableAndAddItToSolver(getDomain()));
  }
}

ORToolsCPVariable::ORToolsCPVariable(const std::string& name, const CPDomain& domain,
                                     const std::vector<int>& dims,
                                     operations_research::Solver* solver)
: CPVariable(name, domain, dims), pSolver(solver)
{
  if (!pSolver)
  {
    throw std::invalid_argument("ORToolsCPVariable - pointer to solver is NULL");
  }

  if (getNumDim() < 2)
  {
    throw std::runtime_error("ORToolsCPVariable - "
        "invalid number of dimensions for a matrix variable");
  }

  int numvars = 1;
  for (auto d : getVarDims())
  {
    numvars *= d;
  }

  for (int vIdx = 0; vIdx < numvars; ++vIdx)
  {
    pVariableList.push_back(buildVariableAndAddItToSolver(getDomain()));
  }
}

operations_research::IntVar* ORToolsCPVariable::buildVariableAndAddItToSolver(
    const CPDomain& domain)
{
  operations_research::IntExpr* result;
  if (domain.isSingleton())
  {
    result = pSolver->MakeIntConst(domain.intValues.back(), getVarName());
  }
  else if (domain.isBool)
  {
    result = pSolver->MakeBoolVar(getVarName());
  }
  else if (domain.isInterval)
  {
    result = pSolver->MakeIntVar(std::max<int64>(domain.intValues.front(), kint32min),
                                 std::min<int64>(domain.intValues.back(), kint32max),
                                 getVarName());
  }
  else if (domain.isListOfValues)
  {
    result = pSolver->MakeIntVar(domain.intValues, getVarName());
  }
  else if (domain.isListOfRanges)
  {
    if ((domain.intValues.size() % 2) != 0)
    {
      throw std::runtime_error("ORToolsCPVariable - wrong number of pairs for "
          "list of ranges domain");
    }

    std::vector<int64_t> listOfValues;
    for (int idx = 0; idx + 1 < static_cast<int>(domain.intValues.size()); idx = idx + 2)
    {
      if (domain.intValues[idx] > domain.intValues[idx + 1])
      {
        throw std::runtime_error("ORToolsCPVariable - error while creating the list of ranges "
            "domain, lower bound " + std::to_string(domain.intValues[idx]) + " greater than "
                "upper bound " + std::to_string(domain.intValues[idx + 1]));
      }

      for (int64_t val = domain.intValues[idx]; val <= domain.intValues[idx + 1]; ++val)
      {
        listOfValues.push_back(val);
      }
    }
    result = pSolver->MakeIntVar(listOfValues, getVarName());
  }
  else
  {
    const std::string errMsg = "ORToolsCPVariable - invalid variable domain type declaration "
        "(the type may not be supported)";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  return result->Var();
}  // buildVariableAndAddItToSolver

CPVariable::SPtr ORToolsCPVariable::getSubVariable(const std::vector<int>& subscr)
{
  if (isScalar())
  {
    throw std::runtime_error("ORToolsCPVariable - "
        "cannot get a sub-variable from a scalar variable");
  }

  // Get the flattened index
  const auto idx = getFlattenedIndex(subscr);

  // The flattened index corresponds to the index in the internal list of variables
  if (idx >= pVariableList.size())
  {
    throw std::runtime_error("ORToolsCPVariable - invalid indexing in array/matrix");
  }

  // Create a new CPVariable with "varIdx" as unique index
  const auto flatIdxName = createNameForIndexedVariable(subscr);
  ORToolsCPVariable::SPtr subVar = std::shared_ptr<ORToolsCPVariable>(
      new ORToolsCPVariable(flatIdxName, pSolver));

  // Set the pointer to the indexed variable
  auto varPtr = pVariableList[idx];
  subVar->pVariableList.push_back(varPtr);

  // Update the domain of the internal variable
  CPDomain domainCopy = getDomain();

  domainCopy.intValues.clear();
  domainCopy.intValues.push_back(varPtr->Min());
  domainCopy.intValues.push_back(varPtr->Max());

  // Override the domain with the updated copy
  subVar->overrideDomain(domainCopy);

  // Set output of the sub-variable the same as this variable
  subVar->setOutput(isOutput());

  // Return the sub-varibale
  return subVar;
}  // getSubVariable

}  // namespace optilab
