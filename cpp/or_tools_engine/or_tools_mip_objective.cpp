#include "or_tools_engine/or_tools_mip_objective.hpp"

#include <cassert>
#include <stdexcept>  // for std::invalid_argument

#include "or_tools_engine/or_tools_mip_variable.hpp"

namespace optilab
{
ORToolsMIPObjective::ORToolsMIPObjective(operations_research::MPObjective* obj)
    : MIPObjective(), pObjective(obj)
{
}

ORToolsMIPObjective::ORToolsMIPObjective(
    ModelObjectMIPObjective::SPtr mipObjectiveObject,
    const std::unordered_map<std::string, MIPVariable::SPtr>& varMap,
    operations_research::MPSolver* solver)
    : MIPObjective(mipObjectiveObject, varMap)
{
  // Instantiate the constraint
  if (!solver)
  {
    throw std::invalid_argument(
        "ORToolsMIPObjective - pointer to solver instance is NULL");
  }

  pObjective = solver->MutableObjective();

  // Add the coefficients
  for (const auto& varCoeffPair : getVarCoeffList())
  {
    ORToolsMIPVariable::SPtr var =
        std::dynamic_pointer_cast<ORToolsMIPVariable>(varCoeffPair.first);
    if (!var)
    {
      throw std::runtime_error(
          "ORToolsMIPObjective - invalid cast to ORToolsMIPVariable for "
          "variables from the var-coeff list");
    }
    pObjective->SetCoefficient(var->getVariable(), varCoeffPair.second);
  }

  // Set objective direction
  const auto objDir = getObjectiveDirection();
  switch (objDir)
  {
    case MIPObjective::ObjectiveDirection::MAXIMIZE:
      pObjective->SetMaximization();
      break;
    default:
      assert(objDir == MIPObjective::ObjectiveDirection::MINIMIZE);
      pObjective->SetMinimization();
      break;
  }
}

}  // namespace optilab
