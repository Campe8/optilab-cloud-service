//
// Copyright OptiLab 2020. All rights reserved.
//
// Test processor for Job-Shop strategies.
//

#pragma once

#include <memory>   // for std::shared_ptr

#include "scheduling_toolbox/job_shop_processor.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace toolbox {

class SYS_EXPORT_CLASS ClientJobShopProcessor : public JobShopProcessor {
public:
  using SPtr = std::shared_ptr<ClientJobShopProcessor>;

public:
  ClientJobShopProcessor();
  ~ClientJobShopProcessor() override = default;

  void loadCustomModel();

  void runProcessor();

  void printResult();
};

}  // namespace toolbox
}  // namespace optilab
