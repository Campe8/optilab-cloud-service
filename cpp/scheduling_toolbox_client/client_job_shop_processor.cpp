#include "scheduling_toolbox_client/client_job_shop_processor.hpp"

#include <chrono>
#include <cstdint>    // for int64_t
#include <fstream>
#include <iostream>
#include <limits>     // for std::numeric_limits
#include <stdexcept>  // for std::invalid_argument
#include <string>
#include <utility>    // For std::pair
#include <vector>

#include <rapidjson/document.h>
#include <spdlog/spdlog.h>

#include "data_structure/metrics/metrics_register.hpp"
#include "optilab_protobuf/job_shop_model.pb.h"
#include "optilab_protobuf/scheduling_model.pb.h"
#include "scheduling_toolbox/scheduling_instance.hpp"
#include "scheduling_toolbox/scheduling_utilities.hpp"
#include "utilities/timer.hpp"
#include "utilities/file_sys.hpp"


namespace optilab {
namespace toolbox {

ClientJobShopProcessor::ClientJobShopProcessor()
: JobShopProcessor(std::make_shared<schedulingengine::JobShopResult>())
{
}

void ClientJobShopProcessor::loadCustomModel()
{
  SchedulingModelProto model;
  model.set_model_id("Job-Shop");
  auto jsmodel = model.mutable_job_shop_model()->mutable_job_shop_model();

  // Set the job-shop as an optimization problem
  jsmodel->set_is_optimization(true);

  // Set default time cost per time unit
  jsmodel->set_makespan_cost_per_time_unit(1);

  // Load the data files
  const auto glbPath = utilsfilesys::getPathToProgramLocation();
  const auto resPath = utilsfilesys::getFullPathToLocation(
          {glbPath, "..", "cpp", "scheduling_toolbox_client", "data", "res.json"});
  const auto taskPath = utilsfilesys::getFullPathToLocation(
          {glbPath, "..", "cpp", "scheduling_toolbox_client", "data", "task.json"});

  std::ifstream resfile(resPath);
  std::string resString((std::istreambuf_iterator<char>(resfile)),
                        (std::istreambuf_iterator<char>()));

  std::ifstream taskfile(taskPath);
  std::string taskString((std::istreambuf_iterator<char>(taskfile)),
                         (std::istreambuf_iterator<char>()));

  // Parse the inputs into Json
  rapidjson::Document resDoc;
  resDoc.Parse(resString.c_str());

  rapidjson::Document taskDoc;
  taskDoc.Parse(taskString.c_str());

  // Parse the resources
  const auto& resJson = resDoc["res"];
  for (rapidjson::SizeType idx{0}; idx < resJson.Size(); ++idx)
  {
    const auto& resObj = resJson[idx].GetObject();
    auto machine = jsmodel->add_machines();

    const auto& machineAvailability = resObj.FindMember("periods")->value.GetArray();
    for (rapidjson::SizeType availIdx{0}; availIdx < machineAvailability.Size(); ++availIdx)
    {
      const auto& availObj = machineAvailability[availIdx].GetObject();
      const auto start = availObj.FindMember("s")->value.GetInt() * 10;
      const auto end = availObj.FindMember("e")->value.GetInt() * 10;
      machine->mutable_availability_matrix()->add_availability_time(start);
      machine->mutable_availability_matrix()->add_availability_time(end);
    }
  }

  // Parse the tasks
  const auto& taskJson = taskDoc["tasks"];
  for (rapidjson::SizeType idx{0}; idx < taskJson.Size(); ++idx)
  {
    const auto& taskObj = taskJson[idx].GetObject();
    auto job = jsmodel->add_jobs();

    // Set default values
    job->set_earliest_start(0);
    job->set_latest_end(std::numeric_limits<int64_t>::max());

    const auto& jobId = taskObj.FindMember("id")->value.GetInt();
    const auto& jobDuration = static_cast<int>(taskObj.FindMember("dur")->value.GetDouble() * 10);
    const auto& jobDependencyList = taskObj.FindMember("dep")->value.GetArray();
    const auto& jobResourcesList = taskObj.FindMember("res")->value.GetArray();
    const auto resNeeded = jobResourcesList[0].GetInt() == 0 ?
            0 : (jobResourcesList[0].GetInt() - 1);

    // Note: 1 job <-> 1 task
    auto task = job->add_tasks();
    task->add_duration(jobDuration);
    task->add_machine(resNeeded);
    for (rapidjson::Value::ConstValueIterator itr = jobDependencyList.Begin();
            itr != jobDependencyList.End(); ++itr)
    {
      const auto depId = itr->GetInt();
      if (depId > -1)
      {
        auto precedence = jsmodel->add_precedences();
        precedence->set_second_job_index(jobId);
        precedence->set_first_job_index(depId);
      }
    }
  }

  // Load the model and create a new Job-Shop solver
  JobShopProcessor::loadInstanceAndCreateSolver(SchedulingInstance::UPtr(
          new SchedulingInstance(model)));
}

void ClientJobShopProcessor::runProcessor()
{
  using namespace std::chrono;

  std::cout << "Start solving..." << std::endl;
  auto tic = high_resolution_clock::now();

  MetricsRegister::SPtr mreg = std::make_shared<MetricsRegister>();

  schedulingengine::SchedulingWork::SPtr work = std::make_shared<schedulingengine::SchedulingWork>(
          schedulingengine::SchedulingWork::WorkType::kRunOptimizer, mreg);
  processWork(work);
  SchedulingProcessor::waitForTaskToComplete();

  auto toc = high_resolution_clock::now();
  auto solvingSec = static_cast<uint64_t>(std::chrono::duration_cast<std::chrono::seconds>(
          toc - tic).count());
  std::cout << "Optimized Job-Shop in " << solvingSec << " sec." << std::endl;
  std::cout << "Done solving" << std::endl;
}

void ClientJobShopProcessor::printResult()
{
  auto result = std::dynamic_pointer_cast<schedulingengine::JobShopResult>(getResult());
  auto& jsResult = result->jsSolution;

  std::cout << "Optimal value: " << jsResult.makespan_cost() / 10.0 <<  std::endl;

  int ctr{0};
  for (const auto& job : jsResult.jobs())
  {
    std::cout << "Job: " << ctr++ << ":" << std::endl;
    for (const auto& task : job.tasks())
    {
      std::cout << "\tStart: " << task.start_time() / 10.0 << std::endl;
    }
  }
  //const auto& solution = result->

}

}  // namespace toolbox
}  // namespace optilab
