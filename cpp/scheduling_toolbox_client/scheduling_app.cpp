//
// Copyright OptiLab 2020. All rights reserved.
//
// Entry point for the OptiLab Scheduling toolbox application.
//

#include <getopt.h>
#include <signal.h>

#include <cstdint>  // for uint32_t
#include <iostream>
#include <limits>   // for std::numeric_limits
#include <memory>   // for std::shared_ptr
#include <string>
#include <vector>

#include "scheduling_toolbox_client/client_job_shop_processor.hpp"

namespace {

void runJobShopFunction()
{
  using namespace optilab;
  using namespace toolbox;

  // Prepare a model
  std::cout << "Job-Shop processor - create processor..." << std::endl;
  auto proc = std::make_shared<ClientJobShopProcessor>();
  std::cout << "...done" << std::endl;

  // Create the model
  std::cout << "Job-Shop processor - load model..." << std::endl;
  proc->loadCustomModel();
  std::cout << "...done" << std::endl;

  // Run the Job-Shop processor
  std::cout << "Job-Shop processor - run job-shop..." << std::endl;
  proc->runProcessor();
  std::cout << "...done" << std::endl;

  std::cout << "Job-Shop processor - print results..." << std::endl;
  proc->printResult();
  std::cout << "...done" << std::endl;
}

}  // namespace

int main(int argc, char* argv[])
{
  std::cout << "=== Running Job-Shop processor ===" << std::endl;
  try
  {
    runJobShopFunction();
  }
  catch(...)
  {
    std::cerr << "Error while running Job-Shop processor" << std::endl;
    return 1;
  }
  std::cout << "=== Done running Job-Shop processor ===" << std::endl;

  return 0;
}
