//
// Copyright OptiLab 2020. All rights reserved.
//
// Processor for routing pipelines.
//

#pragma once

#include <memory>  // for std::shared_ptr
#include <string>

#include "engine/optimizer_processor.hpp"
#include "routing_toolbox/routing_instance.hpp"
#include "routing_toolbox/routing_utilities.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace toolbox {

class SYS_EXPORT_CLASS RoutingProcessor
    : public OptimizerProcessor<routingengine::RoutingWork::SPtr, routingengine::RoutingWork::SPtr>
{
 public:
  using SPtr = std::shared_ptr<RoutingProcessor>;

 public:
  RoutingProcessor(const std::string& procName, routingengine::RoutingResult::SPtr result)
   : BaseProcessor(procName), pResult(result)
  {
    if (!pResult)
    {
      throw std::runtime_error("RoutingProcessor - empty pointer to result");
    }
  }

  /// Loads the routing instance and creates a solver to run on it.
  /// @note throw std::invalid_argument on empty instance
  virtual void loadInstanceAndCreateSolver(RoutingInstance::UPtr instance){};

  /// Interrupts the internal solving process (if running).
  /// The solver remains in a non-executable state until another model is
  /// loaded. Returns true on success, false otherwise
  virtual bool interruptSolver() = 0;

 protected:
  inline routingengine::RoutingResult::SPtr getResult() const
  {
    return pResult;
  }

 private:
  using Work = routingengine::RoutingWork::SPtr;
  using BaseProcessor = OptimizerProcessor<Work, Work>;

 private:
  /// Routes collected as a result of running the router
  /// on the loaded instance
  routingengine::RoutingResult::SPtr pResult;
};

}  // namespace toolbox
}  // namespace optilab
