#include "routing_toolbox/actor_vrp_solver.hpp"

#include <stdexcept>   // for std::invalid_argument

#include <spdlog/spdlog.h>

namespace optilab {
namespace toolbox {

ActorVRPSolver::ActorVRPSolver(const VRPModel::SPtr& vrp)
: pVRPModel(vrp),
  pStatus(routingengine::RoutingResult::ResultStatus::NOT_SOLVED)
{
  if (!pVRPModel)
  {
    throw std::invalid_argument("ActorVRPSolver - empty pointer to a VRP model");
  }
}

routingengine::RoutingResult::ResultStatus ActorVRPSolver::getResultStatus() const
{
  return pStatus;
}  // getResultStatus

bool ActorVRPSolver::interruptSearchProcess()
{
  const bool isInterrupted = false; // pSolver->InterruptSolve();
  if (!isInterrupted)
  {
    // If the solver doesn't allow interruption, try to set the timeout to zero
    spdlog::warn("ActorVRPSolver - interruptSearchProcess: solver cannot be interrupted");
    // pSolver->set_time_limit(1);
  }

  return isInterrupted;
}  // interruptSearchProcess

void ActorVRPSolver::solve()
{
  // pResultStatus = pSolver->Solve();
}  // solve

void ActorVRPSolver::storeSolution(routingengine::RoutingVRPResult& sol)
{

}  // storeSolution

}  // namespace toolbox
}  // namespace optilab
