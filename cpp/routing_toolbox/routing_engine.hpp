//
// Copyright OptiLab 2020. All rights reserved.
//
// Base class for a routing optimizer engine.
// The goal or a routing problem is to find efficient paths
// for transporting items through a complex network.
// Most routing problems involve finding efficient routes
// for vehicles (e.g., cars, trains, airplanes, and so on)
// so they are often referred to as vehicle routing problems (VRPs).
// Moreover, VRPs can have additional constraints such
// maximum load for vehicles, delivery time limits, etc.
//

#pragma once

#include "engine/engine.hpp"

#include <memory>  // for std::shared_ptr
#include <string>

#include "routing_toolbox/routing_instance.hpp"
#include "routing_toolbox/routing_utilities.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace toolbox {

class SYS_EXPORT_CLASS RoutingEngine {
 public:
  using SPtr = std::shared_ptr<RoutingEngine>;

 public:
  virtual ~RoutingEngine() = default;

  /// Registers an instance of a routing optimization problem.
  /// @throw std::invalid argument on empty instances
  virtual void registerInstance(RoutingInstance::UPtr instance) = 0;

  /// Notifies the optimizer on a given RoutingEvent
  virtual void notifyEngine(const routingengine::RoutingEvent& event) = 0;

  /// Wait for the current jobs in the queue to finish.
  /// @note if "timeoutSec" is -1, waits for all jobs in the queue to complete
  virtual void engineWait(int timeoutMsec) = 0;

  /// Shuts down the engine
  virtual void turnDown() = 0;
};

}  // namespace toolbox
}  // namespace optilab
