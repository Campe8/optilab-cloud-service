#include "routing_toolbox/routing_utilities.hpp"

#include <stdexcept>  // for std::runtime_error

#include "engine/engine_constants.hpp"
#include "optilab_protobuf/optimizer_defs.pb.h"

namespace optilab {
namespace toolbox {

namespace routingutils {

RoutingSolutionProto buildVRPSolutionProto(routingengine::RoutingVRPResult::SPtr res)
{
  if (!res)
  {
    throw std::runtime_error("buildVRPSolutionProto - empty result");
  }

  RoutingSolutionProto sol;
  switch (res->status)
  {
    case routingengine::RoutingResult::ResultStatus::SUCCESS:
      sol.set_status(OptimizerSolutionStatusProto::OPT_SOLVER_SUCCESS);
      break;
    case routingengine::RoutingResult::ResultStatus::FAIL:
      sol.set_status(OptimizerSolutionStatusProto::OPT_SOLVER_FAIL);
      break;
    case routingengine::RoutingResult::ResultStatus::NOT_SOLVED:
      sol.set_status(OptimizerSolutionStatusProto::OPT_SOLVER_NOT_SOLVED);
      break;
    case routingengine::RoutingResult::ResultStatus::FAIL_TIMEOUT:
      sol.set_status(OptimizerSolutionStatusProto::OPT_SOLVER_FAIL_TIMEOUT);
      break;
    case routingengine::RoutingResult::ResultStatus::INVALID:
      sol.set_status(OptimizerSolutionStatusProto::OPT_SOLVER_MODEL_INVALID);
      break;
    default:
      sol.set_status(OptimizerSolutionStatusProto::OPT_SOLVER_UNKNOWN_STATUS);
      break;
  }
  sol.set_tot_distance(res->totDistance);
  sol.set_tot_load(res->totLoad);
  for (const auto& route : res->routeList)
  {
    auto routeProto = sol.add_single_route();
    routeProto->set_vehicle_id(static_cast<unsigned int>(route.vehicleId));
    routeProto->set_tot_load(route.totLoad);
    routeProto->set_tot_route_distance(route.totDistance);

    for (auto stop : route.route)
    {
      routeProto->add_route(stop);
    }

    for (auto stopLoad : route.load)
    {
      routeProto->add_load(stopLoad);
    }
  }

  // Return the protobuf solution
  return sol;
}  // buildVRPSolutionProto

}  // namespave routingutils
}  // namespace toolbox
}  // namespace optilab
