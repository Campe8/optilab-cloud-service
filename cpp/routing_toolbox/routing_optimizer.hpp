//
// Copyright OptiLab 2020. All rights reserved.
//
// A routing optimizer is an asynchronous engine that
// holds a pipeline containing routing processors.
//

#pragma once

#include <memory>  // for std::shared_ptr
#include <string>

#include "data_structure/metrics/metrics_register.hpp"
#include "engine/async_engine.hpp"
#include "engine/processor_pipeline.hpp"
#include "routing_toolbox/routing_instance.hpp"
#include "routing_toolbox/routing_processor.hpp"
#include "routing_toolbox/routing_processor_factory.hpp"
#include "routing_toolbox/routing_utilities.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace toolbox {

class SYS_EXPORT_CLASS RoutingOptimizer : public AsyncEngine<routingengine::RoutingWork::SPtr>
{
 public:
  using SPtr = std::shared_ptr<RoutingOptimizer>;

 public:
  /// Constructor. The factory allows the caller to specify the type of processor
  RoutingOptimizer(const std::string& optimizerName,
                   const RoutingProcessorFactory& procFactory,
                   MetricsRegister::SPtr metricsRegister);

  /// Initializes this optimizer with the given routing instance.
  /// i.e., loads the instance into the internal state and creates
  /// the optimizer processor that will solve it.
  /// @note this is a blocking call
  void loadInstance(RoutingInstance::UPtr instance);

  /// Runs this optimizer on the loaded model
  void runOptimizer();

  /// Interrupts the solving processes on this optimizer.
  /// Returns true if the solver was successfully interrupted.
  /// Returns false otherwise
  bool interruptOptimizer();

 protected:
  ProcessorPipeline::SPtr buildPipeline(int pipeline = 0) override;

 private:
  using Work = routingengine::RoutingWork;
  using BaseClass = AsyncEngine<Work::SPtr>;

 private:
  /// Instance to be solved by the optimizer
  RoutingInstance::UPtr pRoutingInstance;

  /// Pointer to the instance of the processor in the pipeline
  RoutingProcessor::SPtr pRoutingProcessor;

  /// Instance of the factory builder for problem-specific processors
  RoutingProcessorFactory pProcessorFactory;

  /// Pointer to the metrics register given to each work task processed
  /// by each processor
  MetricsRegister::SPtr pMetricsRegister;
};

}  // namespace toolbox
}  // namespace optilab
