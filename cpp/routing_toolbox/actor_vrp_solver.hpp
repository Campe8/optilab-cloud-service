//
// Copyright OptiLab 2020. All rights reserved.
//
// Solver for VRP routing problem based on
// an actor trained with a reinforcement learning framework.
//

#pragma once

#include <memory>  // for std::shared_ptr

#include "routing_toolbox/routing_solver.hpp"
#include "routing_toolbox/routing_utilities.hpp"
#include "routing_toolbox/vrp_model.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace toolbox {

class SYS_EXPORT_CLASS ActorVRPSolver : public RoutingSolver {
 public:
  using SPtr = std::shared_ptr<ActorVRPSolver>;

 public:
  /// Constructor.
  /// @note throws std::invalid_argument on empty argument pointer
  ActorVRPSolver(const VRPModel::SPtr& vrp);

  virtual ~ActorVRPSolver() = default;

  /// Runs the solver until a solution is found or a given timeout is reached
  void solve() override;

  /// Returns the current status of the search result
  routingengine::RoutingResult::ResultStatus getResultStatus() const override;

  /// Stops the (ongoing) search process and returns true if the solver
  /// was actually interrupted, returns false otherwise.
  /// @note depending on the package in use, some solvers don't provide
  /// interruption functionalities
  bool interruptSearchProcess() override;

  /// Stores the solution of the solving process.
  /// @note throws std::runtime_error if the solver has not being invoked,
  /// see also "solve(...)" method
  void storeSolution(routingengine::RoutingVRPResult& sol);

 private:
  /// Pointer to the model to solve
  VRPModel::SPtr pVRPModel;

  /// Flag indicating whether the solver should be running or not
  bool pSolverActive{true};

  /// Flag indicating whether the solver has been initialized or not
  bool pSolverInitialized{false};

  /// Status of the search process
  routingengine::RoutingResult::ResultStatus pStatus;
};

}  // namespace toolbox
}  // namespace optilab
