//
// Copyright OptiLab 2020. All rights reserved.
//
// Utilities for routing toolbox.
//

#pragma once

#include <cstdint>  // for int64_t
#include <memory>   // for std::shared_ptr

#include "data_structure/metrics/metrics_register.hpp"
#include "optilab_protobuf/routing_model.pb.h"
#include "ortools/constraint_solver/routing.h"
#include "system/system_export_defs.hpp"

#include "data_structure/json/json.hpp"

namespace optilab {
namespace toolbox {

namespace routingengine {
enum RoutingProcessorType : int {
  /// Processor based on Reinforcement Learning Actors
  /// for VRP problems
  RPT_ACTOR_VRP = 0,

  /// Processor based on Reinforcement Learning Actors
  /// for TSP problems
  RPT_ACTOR_TSP = 0,

  /// Processor based on Constraint Programming solving
  /// techniques and implemented on top of the
  /// Google OR-Tools framework.
  /// This processor type is specialized for solving
  /// TSP problems.
  /// For more information, see
  /// https://developers.google.com/optimization/routing.
  RPT_CP_TSP,

  /// Processor based on Constraint Programming solving
  /// techniques and implemented on top of the
  /// Google OR-Tools framework.
  /// This processor type is specialized for solving
  /// VRP problems.
  /// For more information, see
  /// https://developers.google.com/optimization/routing
  RPT_CP_VRP,

  /// Undefined routing processor type.
  /// @note this is the default if the processor type
  /// is not specified
  RPT_UNDEF
};

struct SYS_EXPORT_STRUCT RoutingWork {
  using SPtr = std::shared_ptr<RoutingWork>;

  enum WorkType {
    /// Runs the optimizer on the loaded model
    kRunOptimizer,
    kWorkTypeUndef
  };

  RoutingWork(WorkType wtype, MetricsRegister::SPtr metReg)
      : workType(wtype),
        metricsRegister(metReg) {
  }

  WorkType workType;
  MetricsRegister::SPtr metricsRegister;
};

class SYS_EXPORT_CLASS RoutingEvent {
 public:
  enum EventType {
    /// Starts the solving process
    kRunEngine,
    /// Kills any ongoing computation
    kInterruptEngine,
    /// Collects solutions
    kSolutions
  };

 public:
  explicit RoutingEvent(EventType aType)
      : pEventType(aType) {
  }

  inline EventType getType() const noexcept {
    return pEventType;
  }
  inline void setNumSolutions(int numSolutions) noexcept {
    pNumSolutions = numSolutions;
  }
  inline int getNumSolutions() const noexcept {
    return pNumSolutions;
  }

 private:
  /// Type of this event
  EventType pEventType;

  /// Number of solutions for "kSolutions" events
  int pNumSolutions { 0 };
};

struct SYS_EXPORT_STRUCT RoutingResult {
  /// Result status
  enum ResultStatus {
    /// Success on solution
    SUCCESS = 0,

    /// Failure in finding a solution
    FAIL,

    /// Failure in finding a solution due to timeout
    FAIL_TIMEOUT,

    /// Model proven to be invalid
    INVALID,

    /// Model not yet solved
    NOT_SOLVED
  };

  using SPtr = std::shared_ptr<RoutingResult>;

  virtual ~RoutingResult() = default;

  /// Name of the instance (problem that has been solved)
  std::string instanceName;
};

struct SYS_EXPORT_STRUCT RoutingVRPResult : public RoutingResult {
  using SPtr = std::shared_ptr<RoutingVRPResult>;

  struct VRPRoute {
    /// Identifier of the vehicle on this route
    int vehicleId { -1 };

    /// Route of this vehicle
    std::vector<int64_t> route;

    /// Load of this vehicle when load constraints are present
    std::vector<int64_t> load;

    /// Total distance of this route
    int64_t totDistance { -1 };

    /// Total load carried by this vehicle when load constraints are present
    int64_t totLoad { -1 };
  };

  /// Status of the solver upon termination
  RoutingResult::ResultStatus status{RoutingResult::ResultStatus::NOT_SOLVED};

  /// Total distance of all routes
  int64_t totDistance { -1 };

  /// Total load carried when load constraints are present
  int64_t totLoad { -1 };

  /// List of routes for each vehicle
  std::vector<VRPRoute> routeList;

  /// Clear the results
  void clear() {
    totDistance = -1;
    totLoad = -1;
    routeList.clear();
    status = RoutingResult::ResultStatus::NOT_SOLVED;
  }
};
}  // namespace routingengine

namespace routingutils {

/// Builds a protobuf object representing the VRP solution given as parameter
SYS_EXPORT_FCN RoutingSolutionProto buildVRPSolutionProto(
        routingengine::RoutingVRPResult::SPtr res);

}  // namespace routingutils
}  // namespace toolbox

}  // namespace optilab
