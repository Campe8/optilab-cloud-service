//
// Copyright OptiLab 2020. All rights reserved.
//
// Solver class for routing frameworks.
//

#pragma once

#include <memory>  // for std::shared_ptr

#include "routing_toolbox/routing_utilities.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace toolbox {

class SYS_EXPORT_CLASS RoutingSolver {
 public:
  using SPtr = std::shared_ptr<RoutingSolver>;

 public:
  RoutingSolver() = default;

  virtual ~RoutingSolver() = default;

  /// Initializes the solver for the search
  virtual void initSearch() {}

  /// Executes cleanup code after search is completed
  virtual void cleanupSearch() {}

  /// Runs the solver until a solution is found or a given timeout is reached
  virtual void solve() = 0;

  /// Returns the current status of the search result
  virtual routingengine::RoutingResult::ResultStatus getResultStatus() const = 0;

  /// Stops the (ongoing) search process and returns true if the solver
  /// was actually interrupted, returns false otherwise.
  /// @note depending on the package in use, some solvers don't provide
  /// interruption functionalities
  virtual bool interruptSearchProcess() { return false; }

};

}
}  // namespace optilab
