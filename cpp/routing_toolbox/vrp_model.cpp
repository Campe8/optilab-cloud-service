#include "routing_toolbox/vrp_model.hpp"

#include <cassert>
#include <stdexcept>  // for std::invalid_argument

#include <spdlog/spdlog.h>

#include "engine/engine_constants.hpp"
#include "model/model_constants.hpp"

namespace optilab {
namespace toolbox {

VRPModel::VRPModel(RoutingInstance::UPtr instance)
: pNumCities(-1),
  pMultDistanceMatrixData(1),
  pNumVehicles(0),
  pDepot(-1)

{
  if (!instance)
  {
    throw std::invalid_argument("VRPModel - empty routing instance");
  }

  initModel(instance.get());
}

void VRPModel::initModel(RoutingInstance* instance)
{
  assert(instance);
  const RoutingModelProto& rmodel = instance->getRoutingModel();
  pModelName = rmodel.model_id();

  if (!rmodel.has_vrp_model() && !rmodel.has_tsp_model())
  {
    throw std::runtime_error("VRPModel - initModel: instance is not a TSP/VRP model");
  }

  // TSP is a simple case of VRP.
  // Both TSP and VRP share the following information:
  // - depot
  // - distance_matrix
  if (rmodel.has_tsp_model())
  {

    const auto& tsp = rmodel.tsp_model();
    pDepot = static_cast<int64_t>(tsp.depot());
    pNumVehicles = 1;
    pNumCities = rmodel.tsp_model().distance_matrix().rows();

    // Set distance matrix
    setDistanceMatrix(tsp.distance_matrix(), rmodel);
  }
  else
  {
    assert(rmodel.has_vrp_model());
    const auto& vrp = rmodel.vrp_model();
    pDepot = static_cast<int64_t>(vrp.depot());
    pNumCities = vrp.distance_matrix().rows();

    for (int dIdx = 0; dIdx < vrp.demand_size(); ++dIdx)
    {
      pDemandList.push_back(static_cast<int64_t>(vrp.demand(dIdx)));
    }

    for (int cIdx = 0; cIdx < vrp.capacity_size(); ++cIdx)
    {
      pVehicleCapacityList.push_back(static_cast<int64_t>(vrp.capacity(cIdx)));
    }

    pNumVehicles = vrp.num_vehicle();

    // Check data consistency
    if (static_cast<int>(pVehicleCapacityList.size()) != pNumVehicles)
    {
      throw std::runtime_error("VRPModel - initModel: "
          "invalid capacity list length of " + std::to_string(pVehicleCapacityList.size()) +
          " expected to match the number of vehicles " + std::to_string(pNumVehicles));
    }

    // Set distance matrix
    setDistanceMatrix(vrp.distance_matrix(), rmodel);

    if (vrp.has_time_windows())
    {
      setTimeWindows(vrp.time_windows(), rmodel);
    }
  }
}  // initModel

void VRPModel::setDistanceMatrix(const ::optilab::toolbox::DistanceMatrixProto& dMatrix,
                                 const RoutingModelProto& rmodel)
{
  int mrows = dMatrix.rows();
  int mcols = dMatrix.cols();
  if (mrows != mcols)
  {
    throw std::runtime_error("VRPModel - initModel: "
        "invalid distance matrix rows " + std::to_string(mrows) +
        " different from columns " + std::to_string(mcols));
  }

  if (static_cast<int>(pDemandList.size()) != mrows && rmodel.has_vrp_model())
  {
    throw std::runtime_error("VRPModel - initModel: "
        "invalid demand list length " + std::to_string(pDemandList.size()) +
        " different from distance matrix size " + std::to_string(mrows));
  }

  pMultDistanceMatrixData = dMatrix.multiplier_data();
  if (pMultDistanceMatrixData < 1) pMultDistanceMatrixData = 1;

  std::vector<int64_t> distRow(mcols);
  pDistanceMatrix.resize(mrows, distRow);
  for (int rIdx = 0; rIdx < mrows; ++rIdx)
  {
    for (int cIdx = 0; cIdx < mcols; ++cIdx)
    {
      // @note row-major order
      pDistanceMatrix[rIdx][cIdx] =
          static_cast<int64_t>(
              dMatrix.data(rIdx * mcols + cIdx) * pMultDistanceMatrixData);
    }
  }
}  // setDistanceMatrix

void VRPModel::setTimeWindows(const ::optilab::toolbox::TimeWindowsProto& tWindows, const RoutingModelProto& rmodel)
{
  int nTimeWindows = tWindows.initial_times().size();
  if (nTimeWindows != pNumCities)
  {
    throw std::runtime_error("VRPModel - initModel: "
        "invalid number of time windows: " + std::to_string(nTimeWindows) +
        " for number of locations: " + std::to_string(pNumCities));
  }

  std::vector<int64_t> size_time_window(2);
  pTimeWindows.resize(nTimeWindows, size_time_window);
  for (int i = 0; i < nTimeWindows; ++i)
  {
    pTimeWindows[i][0] = static_cast<int64_t>(tWindows.initial_times(i));
    pTimeWindows[i][1] = static_cast<int64_t>(tWindows.end_times(i));
  }

  pWaitingTime = tWindows.waiting_time();
  pMaxTimePerVehicle = tWindows.max_time_per_vehicle();
}  // setTimeWindows

}  // namespace toolbox
}  // namespace optilab
