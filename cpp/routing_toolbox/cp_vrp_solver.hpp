//
// Copyright OptiLab 2020. All rights reserved.
//
// Solver for VRP routing problem based on
// an constraint programming and using the Google OR-Tools
// routing package.
//

#pragma once

#include <memory>   // for std::shared_ptr
#include <vector>

#include "ortools/constraint_solver/routing.h"
#include "ortools/constraint_solver/routing_index_manager.h"
#include "ortools/constraint_solver/routing_parameters.h"
#include "routing_toolbox/routing_solver.hpp"
#include "routing_toolbox/vrp_model.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace toolbox {

class SYS_EXPORT_CLASS CPVRPSolver : public RoutingSolver {
 public:
  using SPtr = std::shared_ptr<CPVRPSolver>;

 public:
  /// Constructor.
  /// @note throws std::invalid_argument on empty argument pointer
  explicit CPVRPSolver(const VRPModel::SPtr& vrp);

  ~CPVRPSolver();

  /// Runs the solver until a solution is found or a given timeout is reached
  void solve() override;

  /// Returns the current status of the search result
  routingengine::RoutingResult::ResultStatus getResultStatus() const override;

  /// Stops the (ongoing) search process and returns true if the solver
  /// was actually interrupted, returns false otherwise.
  /// @note depending on the package in use, some solvers don't provide
  /// interruption functionalities
  bool interruptSearchProcess() override;

  /// Stores the solution of the solving process.
  /// @note throws std::runtime_error if the solver has not being invoked,
  /// see also "solve(...)" method
  void storeSolution(routingengine::RoutingVRPResult& sol);

 private:
  using RoutingIndexManagerPtr = std::shared_ptr<operations_research::RoutingIndexManager>;
  using RoutingModelPtr = std::shared_ptr<operations_research::RoutingModel>;
  using RoutingSearchParams = operations_research::RoutingSearchParameters;

 private:
  /// Pointer to the model to solve
  VRPModel::SPtr pVRPModel;

  /// Routing index manager
  RoutingIndexManagerPtr pRIManager;

  /// Pointer to the routing model
  RoutingModelPtr pRoutingModel;

  /// Search parameters
  RoutingSearchParams pSearchParams;

  /// Status of the search solving process
  operations_research::RoutingModel::Status pSearchStatus{
    operations_research::RoutingModel::Status::ROUTING_NOT_SOLVED};

  /// Pointer to the final solution
  const operations_research::Assignment* pSolution{nullptr};

  /// Utility function: initializes this solver w.r.t. the internal VRP model
  void initSolver();
};

}  // namespace toolbox
}  // namespace optilab
