#include "routing_toolbox/cp_vrp_processor.hpp"

#include <exception>
#include <stdexcept>   // for std::runtime_error

#include <spdlog/spdlog.h>

#include "data_structure/metrics/metrics_register.hpp"
#include "engine/engine_constants.hpp"
#include "solver/solver_model.hpp"
#include "utilities/timer.hpp"

namespace optilab {
namespace toolbox {

CPVRPProcessor::CPVRPProcessor(routingengine::RoutingResult::SPtr result)
: RoutingProcessor("CPVRPProcessor", result),
  pVRPModel(nullptr),
  pVRPSolver(nullptr)
{
  pVRPResult = std::dynamic_pointer_cast<routingengine::RoutingVRPResult>(getResult());
  if (!pVRPResult)
  {
    throw std::runtime_error("CPVRPProcessor - invalid downcast to ORToolsVRPResult");
  }
}

void CPVRPProcessor::loadInstanceAndCreateSolver(RoutingInstance::UPtr instance)
{
  if (!instance)
  {
    throw std::runtime_error("CPVRPProcessor - loadInstanceAndCreateSolver: "
        "empty instance");
  }

  // Create a new OR-Tools VRP model from the given input JSON model
  try
  {
    pVRPModel = std::make_shared<VRPModel>(std::move(instance));
  }
  catch(const std::exception& ex)
  {
    const std::string errMsg = "CPVRPProcessor - loadInstanceAndCreateSolver: "
        "error parsing and initializing the model " + std::string(ex.what());
    spdlog::error(errMsg);
    throw;
  }
  catch(...)
  {
    const std::string errMsg = "CPVRPProcessor - loadInstanceAndCreateSolver: "
        "error parsing and initializing the model";
    spdlog::error(errMsg);
    throw;
  }

  // Set the name of the model
  pVRPResult->instanceName = pVRPModel->getModelName();

  // Build a MIP solver
  try
  {
    pVRPSolver = std::make_shared<CPVRPSolver>(pVRPModel);
  }
  catch(const std::exception& ex)
  {
    const std::string errMsg = "ActorVRPProcessor - loadInstanceAndCreateSolver: "
        "cannot build the VRP solver " + std::string(ex.what());
    spdlog::error(errMsg);
    throw;
  }
  catch(...)
  {
    const std::string errMsg = "ActorVRPProcessor - loadInstanceAndCreateSolver: "
        "cannot build the VRP solver";
    spdlog::error(errMsg);
    throw;
  }
}  // setupModel

bool CPVRPProcessor::interruptSolver()
{
  if (!pVRPSolver)
  {
    return false;
  }

  pVRPSolver->interruptSearchProcess();
  return true;
}  // interruptSolver

void CPVRPProcessor::processWork(Work work)
{
  if (work->workType == routingengine::RoutingWork::kRunOptimizer)
  {
    runSolver(work);
  }
  else
  {
    spdlog::warn("CPVRPProcessor - processWork: unrecognized work type");
  }
}  // processWork

void CPVRPProcessor::runSolver(Work& work)
{
  if (!pVRPModel)
  {
    const std::string errMsg = "CPVRPProcessor - runSolver: no model loaded";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  if (!pVRPSolver)
  {
    const std::string errMsg = "CPVRPProcessor - runSolver: no solver available";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  timer::Timer timer;

  // Initialize the solver
  pVRPSolver->initSearch();

  // Run the solver
  pVRPSolver->solve();

  // Cleanup the search
  pVRPSolver->cleanupSearch();
  const auto wallclockMsec = timer.getWallClockTimeMsec();
  work->metricsRegister->setMetric(jsonmetrics::OPTIMIZER_LATENCY_MSEC, wallclockMsec);

  // Store the solution found
  storeSolution();
}  // runEngine

void CPVRPProcessor::storeSolution()
{
  LockGuard lock(pResultMutex);

  // Retrieve the solution
  pVRPSolver->storeSolution(*pVRPResult);
}  // storeSolution

}  // namespace toolbox
}  // namespace optilab
