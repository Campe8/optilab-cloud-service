#include "routing_toolbox/cp_vrp_solver.hpp"

#include <stdexcept>   // for std::invalid_argument

#include <spdlog/spdlog.h>

namespace optilab {
namespace toolbox {

CPVRPSolver::CPVRPSolver(const VRPModel::SPtr& vrp)
: pVRPModel(vrp)
{
  if (!pVRPModel)
  {
    throw std::invalid_argument("CPVRPSolver - empty pointer to a VRP model");
  }

  initSolver();
}

CPVRPSolver::~CPVRPSolver()
{
  pVRPModel.reset();
  pRIManager.reset();
  pRoutingModel.reset();
}

void CPVRPSolver::initSolver() {
  // Instantiate a routing manager
  const auto depot =
      static_cast<operations_research::RoutingIndexManager::NodeIndex>(pVRPModel->getDepot());
  pRIManager =
      std::shared_ptr<operations_research::RoutingIndexManager>(
          new operations_research::RoutingIndexManager(pVRPModel->getNumCities(),
                                                       pVRPModel->getNumVehicles(),
                                                       depot));

  // Instantiate a new routing model from the routing manager
  pRoutingModel = std::make_shared<operations_research::RoutingModel>(*pRIManager);

  // Register a transit callback to specify the cost of each path
  const int transitCallbackIndex = pRoutingModel->RegisterTransitCallback(
      [this](int64 fromIndex, int64 toIndex) -> int64 {
       // Convert from routing variable Index to distance matrix NodeIndex.
        int fromNode = pRIManager->IndexToNode(fromIndex).value();
        int toNode = pRIManager->IndexToNode(toIndex).value();
        return pVRPModel->getDistanceMatrix().at(fromNode).at(toNode);
  });

  // Define the cost of each arc
  pRoutingModel->SetArcCostEvaluatorOfAllVehicles(transitCallbackIndex);

  // ********** Add capacity constraints if they exist ****************** //
  if (!(pVRPModel->getDemands()).empty())
  {
    const int demandCallbackIndex = pRoutingModel->RegisterUnaryTransitCallback(
        [this](int64 fromIndex) -> int64 {
         // Convert from routing variable Index to demand NodeIndex.
        int fromNode = pRIManager->IndexToNode(fromIndex).value();
        return pVRPModel->getDemands().at(fromNode);
    });

    pRoutingModel->AddDimensionWithVehicleCapacity(
        demandCallbackIndex,                // transit callback index
        int64{0},                           // null capacity slack
        pVRPModel->getVehicleCapacities(),  // vehicle maximum capacities
        true,                               // start cumul to zero
        "Capacity");
  }

  // ********** Add time constraints if they exist ****************** //
  if (!(pVRPModel->getTimeWindows()).empty())
  {
     pRoutingModel->AddDimension(transitCallbackIndex,    // transit callback index
                       pVRPModel->getWaitingTime(),       // allow waiting time
                       pVRPModel->getMaxTimePerVehicle(), // maximum time per vehicle
                       false,  // Don't force start cumulative to zero
                       "Time");
      const operations_research::RoutingDimension& time_dimension = pRoutingModel->GetDimensionOrDie("Time");

      // Add time window constraints for each location except depot.
      for (int i = 1; i < pVRPModel->getTimeWindows().size(); ++i) {
        int64 index = pRIManager->NodeToIndex(operations_research::RoutingIndexManager::NodeIndex(i));
        time_dimension.CumulVar(index)->SetRange(pVRPModel->getTimeWindows()[i][0],
                                                pVRPModel->getTimeWindows()[i][1]);
      }

      // Add time window constraints for each vehicle start node.
      for (int i = 0; i < pVRPModel->getNumVehicles(); ++i) {
        int64 index = pRoutingModel->Start(i);
        time_dimension.CumulVar(index)->SetRange(pVRPModel->getTimeWindows()[0][0],
                                                pVRPModel->getTimeWindows()[0][1]);
      }

      // Instantiate route start and end times to produce feasible times.
      for (int i = 0; i < pVRPModel->getNumVehicles(); ++i) {
        pRoutingModel->AddVariableMinimizedByFinalizer(
            time_dimension.CumulVar(pRoutingModel->Start(i)));
        pRoutingModel->AddVariableMinimizedByFinalizer(
            time_dimension.CumulVar(pRoutingModel->End(i)));
      }
  }

  // Set search strategy
  pSearchParams = operations_research::DefaultRoutingSearchParameters();
  pSearchParams.set_first_solution_strategy(
      operations_research::FirstSolutionStrategy::PATH_CHEAPEST_ARC);
}  // initSolver

routingengine::RoutingResult::ResultStatus CPVRPSolver::getResultStatus() const
{
  const operations_research::RoutingModel::Status status = pRoutingModel->status();
  switch (status) {
    case operations_research::RoutingModel::Status::ROUTING_FAIL:
      return routingengine::RoutingResult::ResultStatus::FAIL;
    case operations_research::RoutingModel::Status::ROUTING_FAIL_TIMEOUT:
      return routingengine::RoutingResult::ResultStatus::FAIL_TIMEOUT;
    case operations_research::RoutingModel::Status::ROUTING_INVALID:
      return routingengine::RoutingResult::ResultStatus::INVALID;
    case operations_research::RoutingModel::Status::ROUTING_NOT_SOLVED:
      return routingengine::RoutingResult::ResultStatus::NOT_SOLVED;
    case operations_research::RoutingModel::Status::ROUTING_SUCCESS:
      return routingengine::RoutingResult::ResultStatus::SUCCESS;
    default:
      throw std::runtime_error("CPVRPSolver - getResultStatus: invalid result status");
  }
}  // getResultStatus

bool CPVRPSolver::interruptSearchProcess()
{
  pRoutingModel->CloseModel();
  return true;
}  // interruptSearchProcess

void CPVRPSolver::solve()
{
  pSolution = pRoutingModel->SolveWithParameters(pSearchParams);
}  // solve

void CPVRPSolver::storeSolution(routingengine::RoutingVRPResult& sol)
{
  // Set status of the solution
  sol.status = getResultStatus();

  if (!pSolution)
  {
    spdlog::warn("CPVRPSolver - storeSolution: empty solution");
    // throw std::runtime_error("CPVRPSolver - storeSolution: the solver did not run");

    return;
  }

  double totalDistance{0.0};
  int64 totalLoad{0};
  bool hasLoad = !(pVRPModel->getDemands()).empty();
  double demultiplierData = static_cast<double>(pVRPModel->getMultDistanceMatrix());
  for (int vehicleId = 0; vehicleId < pVRPModel->getNumVehicles(); ++vehicleId)
  {
    routingengine::RoutingVRPResult::VRPRoute vrpRoute;
    vrpRoute.vehicleId = vehicleId;

    int64 index = pRoutingModel->Start(vehicleId);
    double routeDistance{0.0};
    int64 routeLoad{0};
    while (pRoutingModel->IsEnd(index) == false)
    {
      int64 nodeIndex = pRIManager->IndexToNode(index).value();

      vrpRoute.route.push_back(nodeIndex);

      if (hasLoad)
      {
        routeLoad += pVRPModel->getDemands().at(nodeIndex);
        vrpRoute.load.push_back(routeLoad);
      }

      int64 previousIndex = index;
      index = pSolution->Value(pRoutingModel->NextVar(index));
      routeDistance +=
          (pRoutingModel->GetArcCostForVehicle(
              previousIndex, index, int64{vehicleId}) / demultiplierData);
    }

    // Add last index of the route
    vrpRoute.route.push_back(pRIManager->IndexToNode(index).value());
    vrpRoute.totDistance = static_cast<int64>(routeDistance);
    vrpRoute.totLoad = routeLoad;

    // Push back the route
    sol.routeList.push_back(vrpRoute);

    // Accumulate the distances and loads
    totalDistance += routeDistance;
    totalLoad += routeLoad;
  }

  // Set total distance and load
  sol.totDistance = static_cast<int64>(totalDistance);
  sol.totLoad = totalLoad;
}  // getSolution

}  // namespace toolbox
}  // namespace optilab
