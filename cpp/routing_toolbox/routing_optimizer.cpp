#include "routing_toolbox/routing_optimizer.hpp"

#include <spdlog/spdlog.h>

#include <cassert>
#include <stdexcept>  // for std::invalid_argument
#include <utility>    // for std::move

namespace optilab {
namespace toolbox {

RoutingOptimizer::RoutingOptimizer(const std::string& optimizerName,
                                   const RoutingProcessorFactory& procFactory,
                                   MetricsRegister::SPtr metricsRegister)
    : BaseClass(optimizerName),
      pRoutingInstance(nullptr),
      pRoutingProcessor(nullptr),
      pProcessorFactory(procFactory),
      pMetricsRegister(metricsRegister)
{
  if (!pMetricsRegister)
  {
    throw std::invalid_argument(
        "RoutingOptimizer - empty pointer to the metrics register");
  }

  // Initialize the engine,
  // e.g., the internal pipeline
  BaseClass::init();
}

void RoutingOptimizer::loadInstance(RoutingInstance::UPtr instance)
{
  // Load the model into the optimizer
  assert(instance);
  pRoutingProcessor->loadInstanceAndCreateSolver(std::move(instance));
}  // loadModel

void RoutingOptimizer::runOptimizer()
{
  if (!isActive())
  {
    const std::string errMsg =
        "RoutingOptimizer - runOptimizer: optimizer is not active";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  Work::SPtr work = std::make_shared<Work>(
      routingengine::RoutingWork::WorkType::kRunOptimizer, pMetricsRegister);
  pushTask(work);
}  // runOptimizer

bool RoutingOptimizer::interruptOptimizer()
{
  assert(pRoutingProcessor);
  return pRoutingProcessor->interruptSolver();
}  // interruptOptimizer

ProcessorPipeline::SPtr RoutingOptimizer::buildPipeline(int)
{
  ProcessorPipeline::SPtr pipeline = std::make_shared<ProcessorPipeline>();
  pRoutingProcessor = pProcessorFactory.buildProcessor();
  pipeline->pushBackProcessor(pRoutingProcessor);
  return pipeline;
}  // buildPipeline

}  // namespace toolbox
}  // namespace optilab
