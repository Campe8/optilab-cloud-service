#include "routing_toolbox/routing_processor_factory.hpp"

#include <stdexcept>  // for std::invalid_argument
#include <string>

#include "routing_toolbox/actor_vrp_processor.hpp"
#include "routing_toolbox/cp_vrp_processor.hpp"

namespace optilab {
namespace toolbox {

RoutingProcessorFactory::RoutingProcessorFactory(routingengine::RoutingResult::SPtr result)
: pResult(result),
  pProcessorType(routingengine::RoutingProcessorType::RPT_UNDEF)
{
  if (!pResult)
  {
    throw std::runtime_error("RoutingProcessorFactory - empty result");
  }
}

RoutingProcessor::SPtr RoutingProcessorFactory::buildProcessor()
{
  RoutingProcessor::SPtr proc;
  switch(pProcessorType)
  {
    case routingengine::RoutingProcessorType::RPT_ACTOR_VRP:
    {
      proc = std::make_shared<ActorVRPProcessor>(pResult);
      break;
    }
    case routingengine::RoutingProcessorType::RPT_CP_TSP:
    case routingengine::RoutingProcessorType::RPT_CP_VRP:
    {
      proc = std::make_shared<CPVRPProcessor>(pResult);
      break;
    }
    default:
      throw std::runtime_error("RoutingProcessorFactory - buildProcessor: "
          "unrecognized processor type " + std::to_string(static_cast<int>(pProcessorType)));
  }
  return proc;
}  // buildProcessor

}  // namespace toolbox
}  // namespace optilab
