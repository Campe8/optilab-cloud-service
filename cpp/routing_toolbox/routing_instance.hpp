//
// Copyright OptiLab 2020. All rights reserved.
//
// Base class for a routing instance.
//

#pragma once

#include <memory>  // for std::unique_ptr

#include "optilab_protobuf/routing_model.pb.h"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace toolbox {

class SYS_EXPORT_CLASS RoutingInstance {
 public:
  using UPtr = std::unique_ptr<RoutingInstance>;
  using SPtr = std::shared_ptr<RoutingInstance>;

 public:
  RoutingInstance(const RoutingModelProto& routingModel)
 : pRoutingModel(routingModel)
 {
 }

  inline const RoutingModelProto& getRoutingModel() const noexcept { return pRoutingModel; }

 private:
  /// Protobuf containing the routing model
  RoutingModelProto pRoutingModel;
};

}  // namespace toolbox
}  // namespace optilab
