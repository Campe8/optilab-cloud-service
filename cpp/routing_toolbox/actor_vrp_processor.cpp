#include "routing_toolbox/actor_vrp_processor.hpp"

#include <exception>
#include <stdexcept>  // for std::runtime_error
#include <utility>    // for std::move

#include <spdlog/spdlog.h>

#include "data_structure/metrics/metrics_register.hpp"
#include "engine/engine_constants.hpp"
#include "solver/solver_model.hpp"
#include "utilities/timer.hpp"

namespace optilab {
namespace toolbox {

ActorVRPProcessor::ActorVRPProcessor(routingengine::RoutingResult::SPtr result)
: RoutingProcessor("ActorVRPProcessor", result),
  pVRPModel(nullptr),
  pVRPSolver(nullptr)
{
  pVRPResult = std::dynamic_pointer_cast<routingengine::RoutingVRPResult>(getResult());
  if (!pVRPResult)
  {
    throw std::runtime_error("ActorVRPProcessor - invalid downcast to RoutingVRPResult");
  }
}

void ActorVRPProcessor::loadInstanceAndCreateSolver(RoutingInstance::UPtr instance)
{
  if (!instance)
  {
    throw std::runtime_error("ActorVRPProcessor - loadInstanceAndCreateSolver: "
        "empty instance");
  }

  // Create a new OR-Tools VRP model from the given input JSON model
  try
  {
    pVRPModel = std::make_shared<VRPModel>(std::move(instance));
  }
  catch(const std::exception& ex)
  {
    const std::string errMsg = "ActorVRPProcessor - loadInstanceAndCreateSolver: "
        "error parsing and initializing the model " + std::string(ex.what());
    spdlog::error(errMsg);
    throw;
  }
  catch(...)
  {
    const std::string errMsg = "ActorVRPProcessor - loadInstanceAndCreateSolver: "
        "error parsing and initializing the model";
    spdlog::error(errMsg);
    throw;
  }

  // Set the name of the model
  pVRPResult->instanceName = pVRPModel->getModelName();

  // Build a MIP solver
  try
  {
    pVRPSolver = std::make_shared<ActorVRPSolver>(pVRPModel);
  }
  catch(const std::exception& ex)
  {
    const std::string errMsg = "ActorVRPProcessor - loadInstanceAndCreateSolver: "
        "cannot build the VRP solver " + std::string(ex.what());
    spdlog::error(errMsg);
    throw;
  }
  catch(...)
  {
    const std::string errMsg = "ActorVRPProcessor - loadInstanceAndCreateSolver: "
        "cannot build the VRP solver";
    spdlog::error(errMsg);
    throw;
  }
}  // setupModel

bool ActorVRPProcessor::interruptSolver()
{
  if (!pVRPSolver)
  {
    return false;
  }

  pVRPSolver->interruptSearchProcess();
  return true;
}  // interruptSolver

void ActorVRPProcessor::processWork(Work work)
{
  if (work->workType == routingengine::RoutingWork::kRunOptimizer)
  {
    runSolver(work);
  }
  else
  {
    spdlog::warn("ActorVRPProcessor - processWork: unrecognized work type");
  }
}  // processWork

void ActorVRPProcessor::runSolver(Work& work)
{
  if (!pVRPModel)
  {
    const std::string errMsg = "ActorVRPProcessor - runSolver: no model loaded";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  if (!pVRPSolver)
  {
    const std::string errMsg = "ActorVRPProcessor - runSolver: no solver available";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  timer::Timer timer;

  // Initialize the solver
  pVRPSolver->initSearch();

  // Run the solver
  pVRPSolver->solve();

  // Cleanup the search
  pVRPSolver->cleanupSearch();

  const auto wallclockMsec = timer.getWallClockTimeMsec();
  work->metricsRegister->setMetric(jsonmetrics::OPTIMIZER_LATENCY_MSEC, wallclockMsec);

  // Store the solution found
  storeSolution();
}  // runEngine

void ActorVRPProcessor::storeSolution()
{
  LockGuard lock(pResultMutex);

  // Retrieve the solution
  pVRPSolver->storeSolution(*pVRPResult);
}  // storeSolution

}  // namespace toolbox
}  // namespace optilab
