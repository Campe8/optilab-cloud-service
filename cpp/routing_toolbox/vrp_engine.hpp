//
// Copyright OptiLab 2020. All rights reserved.
//
// Engine class for Vehicle Routing Problem (VRP) frameworks.
//

#pragma once

#include <atomic>
#include <cstdint>  // uint64_t
#include <memory>   // for std::shared_ptr
#include <string>

#include "data_structure/metrics/metrics_register.hpp"
#include "engine/engine_callback_handler.hpp"
#include "routing_toolbox/routing_engine.hpp"
#include "routing_toolbox/routing_instance.hpp"
#include "routing_toolbox/routing_optimizer.hpp"
#include "routing_toolbox/routing_utilities.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace toolbox {

class SYS_EXPORT_CLASS VRPEngine : public RoutingEngine
{
 public:
  using SPtr = std::shared_ptr<VRPEngine>;

 public:
  /// Constructor: builds a new VRP engine with given processor.
  /// @note the processor must be a VRP processor
  VRPEngine(const std::string& engineId, const EngineCallbackHandler::SPtr& handler,
            routingengine::RoutingProcessorType routingProcessorType);

  ~VRPEngine();

  /// Registers the protobuf model given as input argument.
  /// @note this method does not run the model
  void registerInstance(RoutingInstance::UPtr instance) override;

  /// Notifies the engine on a given EngineEvent
  void notifyEngine(const routingengine::RoutingEvent& event) override;

  /// Wait for the current jobs in the queue to finish.
  /// @note if "timeoutMsec" is -1, waits for all jobs in the queue to complete
  void engineWait(int timeoutMsec) override;

  /// Turns-down this engine
  void turnDown() override;

 private:
  /// Engine identifier
  std::string pEngineId;

  /// Handler for callbacks to send back results to caller methods
  EngineCallbackHandler::SPtr pCallbackHandler;

  /// Pointer to the result instance collecting result from the optimizer
  routingengine::RoutingResult::SPtr pVRPResult;

  /// Flag indicating whether this engine already started running
  std::atomic<bool> pActiveEngine{false};

  /// Latency in msec. to create this engine
  std::atomic<uint64_t> pEngineCreationTimeMsec{0};

  /// Latency in msec. caused by loading a model into the optimizer
  std::atomic<uint64_t> pLoadModelLatencyMsec{0};

  /// Latency in msec. caused by running the optimizer
  std::atomic<uint64_t> pOptimizerLatencyMsec{0};

  /// Register for metrics
  MetricsRegister::SPtr pMetricsRegister;

  /// Asynchronous optimizer
  RoutingOptimizer::SPtr pOptimizer;

  // Builds the instance of a ORTools optimizer with a specified VRP processor
  void buildOptimizer(routingengine::RoutingProcessorType procType);

  /// Runs the registered model
  void processRunModelEvent();

  /// Sends back using the handler all the solutions collected so far
  void processCollectSolutionsEvent(int numSolutions);

  /// Interrupts the current computation, if any, and returns
  void processInterruptEngineEvent();

  /// Collects metrics in the internal metrics register
  void collectMetrics();
};

}  // namespace toolbox
}  // namespace optilab
