//
// Copyright OptiLab 2020. All rights reserved.
//
// Class encapsulating a VRP model.
//

#pragma once

#include <cstdint>  // for int64_t
#include <memory>   // for std::shared_ptr
#include <string>
#include <vector>

#include "optilab_protobuf/routing_model.pb.h"
#include "routing_toolbox/routing_instance.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace toolbox {

class SYS_EXPORT_CLASS VRPModel {
 public:
  using SPtr = std::shared_ptr<VRPModel>;

  /// Distance matrix representation
  using DistanceMatrix = std::vector<std::vector<int64_t>>;
  using VehicleCapacities = std::vector<int64_t>;
  using TimeWindows = std::vector<std::vector<int64_t>>;

 public:
  /// Constructor creates a new VRP model run by the VRP engine.
  /// The model is build on the given routing instance.
  /// @note throws std::invalid_argument on empty input arguments
  explicit VRPModel(RoutingInstance::UPtr instance);

  /// Returns the model's name
  inline const std::string& getModelName() const noexcept { return pModelName; }

  /// Returns the total number of cities
  inline int getNumCities() const noexcept { return pNumCities; }

  /// Returns the number of vehicles
  inline int getNumVehicles() const noexcept { return pNumVehicles; }

  /// Returns the waiting time
  inline int64_t getWaitingTime() const noexcept { return pWaitingTime; }

  /// Returns the number of vehicles
  inline int64_t getMaxTimePerVehicle() const noexcept { return pMaxTimePerVehicle; }

  /// Returns the distance matrix
  inline const DistanceMatrix& getDistanceMatrix() const noexcept { return pDistanceMatrix; }

  /// Returns the time windows
  inline const TimeWindows& getTimeWindows() const noexcept { return pTimeWindows; }

  /// Returns the demands list
  inline const std::vector<int64_t>& getDemands() const noexcept { return pDemandList; }

  /// Returns the vehicle capacities list
  inline const std::vector<int64_t>& getVehicleCapacities() const noexcept
  {
    return pVehicleCapacityList;
  }

  /// Returns the depot index
  inline const int64_t& getDepot() const noexcept
  {
    return pDepot;
  }

  /// Multiplier used for distance matrix
  inline int getMultDistanceMatrix() const noexcept { return pMultDistanceMatrixData; }

 private:
  /// Model's name
  std::string pModelName;

  /// Total number of cities in the model
  int pNumCities;

  /// Multiplier for the distances in the distance matrix
  int pMultDistanceMatrixData;

  /// Distance matrix for the VRP problem
  DistanceMatrix pDistanceMatrix;

  /// Time windows for the VRP problem
  TimeWindows pTimeWindows;
  int64_t pWaitingTime;
  int64_t pMaxTimePerVehicle;

  /// Total number of vehicles.
  /// @note this number must be greater than zero.
  /// @note if the number of vehicles is one,
  /// the problem reduces to TSP (with capacity constraints)
  int pNumVehicles;

  /// List of demands.
  /// @note the list can be empty.
  /// @note if not empty, the size should match the number of cities
  std::vector<int64_t> pDemandList;

  /// List of vehicle capacities.
  /// @note the list can be empty.
  /// @note if not empty, the size should match the number of vehicles
  std::vector<int64_t> pVehicleCapacityList;

  /// Depot, i.e., starting point
  int64_t pDepot;

  /// Utility function: initializes this model from the give routing instance
  void initModel(RoutingInstance* instance);

  /// Utility function: sets the distance matrix
  void setDistanceMatrix(const ::optilab::toolbox::DistanceMatrixProto& dMatrix,
                         const RoutingModelProto& rmodel);

  /// Utility function: sets time windows
  void setTimeWindows(const ::optilab::toolbox::TimeWindowsProto& dMatrix,
                      const RoutingModelProto& rmodel);
};

}  // namespave toolbox
}  // namespace optilab
