//
// Copyright OptiLab 2019. All rights reserved.
//
// Factory class for ORTools processors.
//

#pragma once

#include "engine/engine_constants.hpp"
#include "routing_toolbox/routing_processor.hpp"
#include "routing_toolbox/routing_utilities.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace toolbox {

class SYS_EXPORT_CLASS RoutingProcessorFactory {
public:
  explicit RoutingProcessorFactory(routingengine::RoutingResult::SPtr result);

  /// Sets the type of routing engine processor to build
  inline void setProcessorType(routingengine::RoutingProcessorType ptype) noexcept
  {
    pProcessorType = ptype;
  }

  /// Returns the processor build w.r.t. the set type.
  /// See also "setProcessorType(...)".
  /// @note throw std::runtime_error on invalid processor type
  RoutingProcessor::SPtr buildProcessor();


private:
  /// Type of processor to build
  routingengine::RoutingProcessorType pProcessorType;

  /// Result used to build a routing processor
  routingengine::RoutingResult::SPtr pResult;
};

}  // namespace toolbox
}  // namespace optilab
