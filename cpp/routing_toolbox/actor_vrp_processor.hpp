//
// Copyright OptiLab 2020. All rights reserved.
//
// Actor processor for VRP engines.
// The actor processor is based on Reinforcement Learning
// algorithms and acts over a policy already learned.
// For more information, see
// https://arxiv.org/pdf/1803.08475v3.pdf
//

#pragma once

#include <memory>   // for std::shared_ptr
#include <string>
#include <map>
#include <utility>  // for std::pair
#include <vector>

#include <boost/thread.hpp>

#include "routing_toolbox/actor_vrp_solver.hpp"
#include "routing_toolbox/routing_processor.hpp"
#include "routing_toolbox/routing_utilities.hpp"
#include "routing_toolbox/vrp_model.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace toolbox {

class SYS_EXPORT_CLASS ActorVRPProcessor : public RoutingProcessor {
public:
  using SPtr = std::shared_ptr<ActorVRPProcessor>;

public:
  ActorVRPProcessor(routingengine::RoutingResult::SPtr result);

  /// Loads the vrp model and creates a solver to run on it.
  /// @note throw std::invalid_argument on empty instance
  void loadInstanceAndCreateSolver(RoutingInstance::UPtr instance) override;

  /// Interrupts the internal solving process (if running).
  /// The solver remains in a non-executable state until another model is loaded
  bool interruptSolver() override;

  /// Stores the solution of the solving process.
  /// @note throws std::runtime_error if the solver has not being invoked,
  /// see also "solve(...)" method
  void storeSolution(routingengine::RoutingVRPResult* result);

protected:
  using Work = routingengine::RoutingWork::SPtr;

  /// Lock type on the internal mutex
  using LockGuard = boost::lock_guard<boost::mutex>;

protected:
  void processWork(Work work) override;

private:
  /// VRP model to solve
  VRPModel::SPtr pVRPModel;

  /// Actor solver used to solve VRP problems
  ActorVRPSolver::SPtr pVRPSolver;

  /// Pointer to the downcast VRP result instance
  routingengine::RoutingVRPResult::SPtr pVRPResult;

  /// Mutex synch.ing the state of the result
  boost::mutex pResultMutex;

  /// Utility function: runs the solver
  void runSolver(Work& work);

  /// Utility function: store solution
  void storeSolution();
};

}  // namespace toolbox
}  // namespace optilab
