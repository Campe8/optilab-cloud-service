#include "routing_toolbox/vrp_engine.hpp"

#include <spdlog/spdlog.h>

#include <stdexcept>  // for std::runtime_error
#include <utility>    // for std::move

#include "data_structure/json/json.hpp"
#include "engine/engine_constants.hpp"
#include "engine/engine_utils.hpp"
#include "routing_toolbox/routing_processor_factory.hpp"
#include "routing_toolbox/routing_utilities.hpp"
#include "utilities/timer.hpp"

namespace {
/// Waiting value for an idle processor
constexpr int kWaitOnIdleProcessor{-1};
}  // namespace

namespace optilab {
namespace toolbox {

VRPEngine::VRPEngine(const std::string& engineId, const EngineCallbackHandler::SPtr& handler,
                     routingengine::RoutingProcessorType routingProcessorType)
: pEngineId(engineId),
  pCallbackHandler(handler)

{
  if (engineId.empty())
  {
    const std::string errMsg = "VRPEngine - empty engine identifier";
    throw std::invalid_argument(errMsg);
  }

  if (!handler)
  {
    const std::string errMsg = "VRPEngine - engine callback handler";
    throw std::invalid_argument(errMsg);
  }

  timer::Timer timer;

  pVRPResult = std::make_shared<routingengine::RoutingVRPResult>();

  pMetricsRegister = std::make_shared<MetricsRegister>();

  // Builds the optimizer running ORTools VRP models
  buildOptimizer(routingProcessorType);
  assert(pOptimizer);

  // Start  the asynchronous thread in the optimizer
  pOptimizer->startUp();

  pActiveEngine = true;

  pEngineCreationTimeMsec = timer.getWallClockTimeMsec();
}

VRPEngine::~VRPEngine()
{
  try
  {
    turnDown();
  }
  catch (...)
  {
    // Do not throw in destructor
    spdlog::warn("VRPEngine - thrown in destructor");
  }
}

void VRPEngine::turnDown()
{
  if (pActiveEngine)
  {
    pActiveEngine = false;
    pOptimizer->tearDown();
    pOptimizer.reset();
  }
}  // turnDown

void VRPEngine::buildOptimizer(routingengine::RoutingProcessorType procType)
{
  // Create and set the factory for ORTools CP processors
  RoutingProcessorFactory procFactory(pVRPResult);
  procFactory.setProcessorType(procType);
  pOptimizer = std::make_shared<RoutingOptimizer>(pEngineId, procFactory, pMetricsRegister);
}  // buildOptimizer

void VRPEngine::registerInstance(RoutingInstance::UPtr instance)
{
  if (!instance)
  {
    throw std::runtime_error("VRPEngine - registerInstance: "
        "registering an empty instance for engine: " + pEngineId);
  }

  // Start timer for loading the model
  timer::Timer timer;

  // Load the model into the optimizer
  try
  {
    pOptimizer->loadInstance(std::move(instance));
  }
  catch (std::exception& ex)
  {
    const std::string errMsg =
        "VRPEngine - registerModel: "
        "error while loading the model on engine " +
        pEngineId + " " + std::string(ex.what());
    spdlog::error(errMsg);
    throw;
  }
  catch (...)
  {
    const std::string errMsg =
        "VRPEngine - registerModel: "
        "undefined error while loading the model on engine " +
        pEngineId;
    spdlog::error(errMsg);
    throw;
  }

  pLoadModelLatencyMsec = timer.getWallClockTimeMsec();

  // Reset solution
  std::dynamic_pointer_cast<routingengine::RoutingVRPResult>(pVRPResult)->clear();
}  // registerModel

void VRPEngine::engineWait(int timeoutMsec) {
  if (pActiveEngine)
  {
    pOptimizer->waitOnTaskCompletion(timeoutMsec);
  }
}  // engineWait

void VRPEngine::notifyEngine(const routingengine::RoutingEvent& event)
{
  switch (event.getType())
  {
    case routingengine::RoutingEvent::EventType::kRunEngine:
    {
      processRunModelEvent();
      break;
    }
    case routingengine::RoutingEvent::EventType::kSolutions:
    {
      processCollectSolutionsEvent(event.getNumSolutions());
      break;
    }
    default:
    {
      assert(event.getType() ==
          routingengine::RoutingEvent::EventType::kInterruptEngine);
      processInterruptEngineEvent();
      break;
    }
  }
}  // notifyEngine

void VRPEngine::processRunModelEvent() {
  // Return if the engine is not active
  if (!pActiveEngine)
  {
    spdlog::warn(
        "VRPEngine - processRunModelEvent: "
        "inactive engine, returning " +
        pEngineId);
    return;
  }

  timer::Timer timer;

  // Wait on previous tasks to complete.
  // For example, it waits on model registration
  engineWait(kWaitOnIdleProcessor);

  // Trigger the engine to run and return
  pOptimizer->runOptimizer();

  // Latency for running the optimizer
  pOptimizerLatencyMsec = timer.getWallClockTimeMsec();
}  // processRunModelEvent

void VRPEngine::processInterruptEngineEvent()
{
  if (!pActiveEngine)
  {
    spdlog::warn(
        "VRPEngine - processInterruptEngineEvent: "
        "inactive engine, returning " +
        pEngineId);
    return;
  }

  pOptimizer->interruptOptimizer();
}  // processKillEngineEvent

void VRPEngine::processCollectSolutionsEvent(int numSolutions)
{
  // @note it is assumed that the processor
  // is not currently adding result into the solution
  routingengine::RoutingVRPResult::SPtr res =
      std::dynamic_pointer_cast<routingengine::RoutingVRPResult>(pVRPResult);

  // Callback method to send results back to the client
  pCallbackHandler->resultCallback(pEngineId, routingutils::buildVRPSolutionProto(res));

  // Callback to send the metrics back to the client
  collectMetrics();
  auto jsonMetrics = engineutils::createMetricsJson(pMetricsRegister);

  // Callback method to send metrics back to the client
  pCallbackHandler->metricsCallback(pEngineId, jsonMetrics->toString());

  // Completed run
  pCallbackHandler->processCompletionCallback(pEngineId);
}  // processRunModelEvent

void VRPEngine::collectMetrics()
{
  assert(pMetricsRegister);
  pMetricsRegister->setMetric(jsonmetrics::ENGINE_CREATION_TIME_MSEC,
                              pEngineCreationTimeMsec);
  pMetricsRegister->setMetric(jsonmetrics::LOAD_MODEL_LATENCY_MSEC,
                              pLoadModelLatencyMsec);
}  // collectMetrics

}  // namespace toolbox
}  // namespace optilab
