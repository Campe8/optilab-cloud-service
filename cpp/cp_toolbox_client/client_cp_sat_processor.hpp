//
// Copyright OptiLab 2020. All rights reserved.
//
// Processor for constraint problems solved with CP-Sat solvers.
//

#pragma once

#include <memory>   // for std::shared_ptr
#include <string>

#include "cp_toolbox/cp_sat_processor.hpp"
#include "cp_toolbox/cp_utilities.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {
namespace toolbox {

class SYS_EXPORT_CLASS ClientCPSatProcessor : public CPSatProcessor{
public:
  using SPtr = std::shared_ptr<ClientCPSatProcessor>;

public:
  ClientCPSatProcessor();
  ~ClientCPSatProcessor() override;

  void loadCustomModel(const std::string& modelName,
                       int numVars, int varLB, int varUB);

  void runSolver();

  void printSolution();
};

}  // namespace toolbox
}  // namespace optilab
