//
// Copyright OptiLab 2020. All rights reserved.
//
// Entry point for the OptiLab CP-Sat function
// toolbox application.
//

#include <getopt.h>
#include <signal.h>

#include <cstdint>  // for uint32_t
#include <iostream>
#include <limits>   // for std::numeric_limits
#include <memory>   // for std::shared_ptr
#include <string>
#include <vector>

#include "cp_toolbox_client/client_cp_sat_processor.hpp"

#include <chrono>
#include <stdexcept>  // for std::invalid_argument
#include <utility>    // For std::pair

namespace {

void runCPSat(const std::string& modelName)
{
  using namespace optilab;
  using namespace toolbox;

  // Prepare a model
  std::cout << "CP-Sat processor - create processor..." << std::endl;

  auto proc = std::make_shared<ClientCPSatProcessor>();
  std::cout << "...done" << std::endl;

  // Create the model
  std::cout << "CP-Sat - load model..." << std::endl;
  proc->loadCustomModel(modelName, 10, 0, 9);

  std::cout << "...done" << std::endl;

  // Run the model
  std::cout << "CP-Sat processor - run solver..." << std::endl;
  proc->runSolver();
  std::cout << "...done" << std::endl;

  std::cout << "CP-Sat - print solution..." << std::endl;
  proc->printSolution();
  std::cout << "...done" << std::endl;

  // Delete processor instance
  proc.reset();
}  // runCPSat

}  // namespace

int main(int argc, char* argv[]) {

  std::cout << "=== Running CP-Sat processor ===" << std::endl;
  try
  {
    runCPSat("my_model");
  }
  catch(...)
  {
    std::cerr << "Error while running CP-Sat processor" << std::endl;
    return 1;
  }
  std::cout << "=== Done running CP-Sat processor ===" << std::endl;

  return 0;
}

