#include "cp_toolbox_client/client_cp_sat_processor.hpp"

#include <cassert>
#include <chrono>
#include <exception>
#include <stdexcept>  // for std::runtime_error
#include <utility>    // for std::move

#include <spdlog/spdlog.h>

#include "data_structure/metrics/metrics_register.hpp"
#include "engine/engine_constants.hpp"
#include "utilities/timer.hpp"

#include "optilab_protobuf/constraint_model.pb.h"

namespace optilab {
namespace toolbox {

ClientCPSatProcessor::ClientCPSatProcessor()
: CPSatProcessor("ClientCPSatProcessor", std::make_shared<cpengine::CPSatResult>())
{
}

ClientCPSatProcessor::~ClientCPSatProcessor()
{
}

void ClientCPSatProcessor::loadCustomModel(const std::string& modelName,
                                           int numVars, int varLB, int varUB)
{
  // First create the actual model using OR-Tools CP Model proto message
  ::operations_research::sat::CpModelProto cpModelProto;
  cpModelProto.set_name(modelName);

  std::string varName{"v_"};
  for (int vidx = 0; vidx < numVars; ++vidx)
  {
    auto var = cpModelProto.add_variables();
    var->set_name(varName + std::to_string(vidx));
    var->add_domain(varLB);
    var->add_domain(varUB);
  }

  // Add a simple all different constraint
  auto allDifferent = cpModelProto.add_constraints();
  allDifferent->set_name("all_different");

  ::operations_research::sat::AllDifferentConstraintProto allDiffSpecs;
  for (int vidx = 0; vidx < numVars; ++vidx)
  {
    allDiffSpecs.add_vars(vidx);
  }
  auto allDiffProto = allDifferent->mutable_all_diff();
  allDiffProto->CopyFrom(allDiffSpecs);

  // Set the objective: maximize (-1) the first variable (0)
  auto obj = cpModelProto.mutable_objective();
  obj->add_vars(0);
  obj->add_coeffs(-1);

  // Then create the Parallel AI CP model based on the CpModelProto
  toolbox::ConstraintModelProto model;
  model.set_model_id(modelName);
  auto cpModel = model.mutable_cp_sat_model();
  auto cpSatModel = cpModel->mutable_cp_model();
  cpSatModel->CopyFrom(cpModelProto);

  CPInstance::UPtr instance = CPInstance::UPtr(new CPInstance(model));
  CPSatProcessor::loadInstanceAndCreateSolver(std::move(instance));
}

void ClientCPSatProcessor::runSolver()
{
  using namespace std::chrono;

  std::cout << "Start solving..." << std::endl;
  auto tic = high_resolution_clock::now();

  MetricsRegister::SPtr mreg = std::make_shared<MetricsRegister>();

  cpengine::CPWork::SPtr work = std::make_shared<cpengine::CPWork>(
          cpengine::CPWork::WorkType::kRunOptimizer, mreg);
  processWork(work);
  CPSatProcessor::waitForTaskToComplete();

  auto toc = high_resolution_clock::now();
  auto solvingSec = static_cast<uint64_t>(std::chrono::duration_cast<std::chrono::seconds>(
          toc - tic).count());

  std::cout << "Run solver in " << solvingSec << " sec." << std::endl;
  std::cout << "Done solving" << std::endl;
}

void ClientCPSatProcessor::printSolution()
{
  auto result = getResult();
  result->solutionProto.PrintDebugString();
}

}  // namespace toolbox
}  // namespace optilab
