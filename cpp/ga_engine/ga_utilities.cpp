#include "ga_engine/ga_utilities.hpp"

#include "engine/engine_constants.hpp"
#include "ga_engine/ga_constants.hpp"

namespace optilab {

namespace gautilsdynlib {

std::string getCustomObjectiveFunctionArgument(const std::string& varId,
                                               const std::string& varType)
{
  static const std::string arg = "const std::vector<";
  return arg + varType + ">& " + varId;
}  // getCustomObjectiveFunctionArgument

std::string customObjectiveClassToString(const std::string& varId, const std::string& varType,
                                         const std::string& objFcnDef)
{
  std::string objClassStr;

  // Class prefix (includes and al.)
  objClassStr =
      std::string(gacustomenvironment::GA_CUSTOM_ENVIRONMENT_CUSTOM_OBJECTIVE_CLASS_PREFIX);

  // Objective function prototype
  objClassStr +=
      std::string(
          gacustomenvironment::GA_CUSTOM_ENVIRONMENT_CUSTOM_OBJECTIVE_FCN_SIGNATURE_START);

  // Objective function argument
  objClassStr += getCustomObjectiveFunctionArgument(varId, varType);
  objClassStr += std::string(
      gacustomenvironment::GA_CUSTOM_ENVIRONMENT_CUSTOM_OBJECTIVE_FCN_SIGNATURE_END);

  // Objective function definition
  objClassStr += objFcnDef;

  // Class suffix
  objClassStr +=
      std::string(gacustomenvironment::GA_CUSTOM_ENVIRONMENT_CUSTOM_OBJECTIVE_CLASS_SUFFIX);

  return objClassStr;
}  // customObjectiveClassToString

}  // gautilsdynlib

}  // namespace optilab
