//
// Copyright OptiLab 2019-2020. All rights reserved.
//
// Standard environment for Genetic Algorithms optimizers.
// A "standard" environment is an environment which allows the user to choose
// among a pre-determined list of options for the environment parameters.
//

#pragma once

#include "ga_engine/ga_environment.hpp"

#include <memory>   // for std::shared_ptr
#include <string>

#include "ga_engine/ga_std_environment_impl.hpp"
#include "optilab_protobuf/evolutionary_model.pb.h"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS GAStdEnvironment : public GAEnvironment {
 public:
  using SPtr = std::shared_ptr<GAStdEnvironment>;

 public:
  GAStdEnvironment();

  virtual ~GAStdEnvironment();

  static bool isa(const GAEnvironment* env);
  static GAStdEnvironment* cast(GAEnvironment* env);
  static const GAStdEnvironment* cast(const GAEnvironment* env);

  StdEnvironmentBaseImpl::SPtr getEnvironmentImpl() const { return pEnvironmentImplPtr; }

 protected:
  void loadEnvironmentImpl(const toolbox::GeneticModelProto& model) override;

 private:
  /// Name of the dynamic library linked as objective function
  std::string pDynLibName;

  /// Identifier of the solution, i.e., the name of the list of chromosomes
  std::string pSolutionId;


  /// Pointer to the actual implementation of the environment
  StdEnvironmentBaseImpl::SPtr pEnvironmentImplPtr;

  /// Compiles and builds a dynamic library containing the
  /// objective function to be linked to the environment implementation.
  /// Returns the name of the library to be dynamically loaded.
  /// @note the method will build the dynamic library from the file "fcn_path"
  std::string compileObjectiveIntoDynLib(const std::string& fcn_path);
};

}  // namespace optilab
