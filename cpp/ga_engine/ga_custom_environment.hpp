//
// Copyright OptiLab 2019. All rights reserved.
//
// Custom environment for Genetic Algorithms optimizers.
// A "custom" environment is a user-define environment.
//

#pragma once

#include "ga_engine/ga_environment.hpp"

#include <memory>   // for std::shared_ptr
#include <string>
#include <vector>

#include "ga_engine/ga_custom_environment_impl.hpp"
#include "system/system_export_defs.hpp"

namespace optilab {

class SYS_EXPORT_CLASS GACustomEnvironment : public GAEnvironment {
 public:
  using SPtr = std::shared_ptr<GACustomEnvironment>;

 public:
  GACustomEnvironment();

  virtual ~GACustomEnvironment();

  static bool isa(const GAEnvironment* env);
  static GACustomEnvironment* cast(GAEnvironment* env);
  static const GACustomEnvironment* cast(const GAEnvironment* env);

  CustomEnvironmentBaseImpl::SPtr getEnvironmentImpl() const { return pEnvironmentImplPtr; }

 protected:
  void loadEnvironmentImpl(const toolbox::GeneticModelProto& model) override;

 private:
  /// Name of the dynamic library linked as objective function
  std::string pDynLibName;

  /// Identifier of the solution, i.e., the name of the list of chromosomes
  std::string pSolutionId;

  /// Pointer to the actual implementation of the environment
  CustomEnvironmentBaseImpl::SPtr pEnvironmentImplPtr;

  /// Compiles and builds a dynamic library containing the
  /// objective function to be linked to the environment implementation.
  /// Returns the name of the library to be dynamically loaded
  std::string compileObjectiveIntoDynLib(const std::vector<std::string>& fcnList);
};

}  // namespace optilab
