// Base class
#include "ga_engine/openga_optimizer.hpp"

#include <limits>  // for std::numeric_limits

#include <spdlog/spdlog.h>

namespace optilab {

OpenGAOptimizer::OpenGAOptimizer(GAEnvironment::SPtr env)
: pEnvironment(env),
  pGAOptimizer(nullptr),
  pGenerationNumber(0),
  pTimeoutSec(-1)
{
  if (!pEnvironment)
  {
    throw std::invalid_argument("OpenGAOptimizer - empty environment");
  }

  pBestCost.resize(1, std::numeric_limits<double>::max());
  pAverageCost.resize(1, std::numeric_limits<double>::max());
}

void OpenGAOptimizer::initSearch()
{
  // Load the environment into the optimizer
  loadEnvironment();
}  // initSearch

void OpenGAOptimizer::cleanupSearch()
{
  // Set cost and chromosomes
  pEnvironment->setObjectiveValue(pBestCost.at(0));
  pEnvironment->setBestChromosomes(pBestChromosomes);
}  // cleanupSearch

void OpenGAOptimizer::runOptimizer()
{
  // Run the optimizer
  EA::StopReason stopReason = EA::StopReason::Undefined;
  try
  {
    stopReason = pGAOptimizer->solve();
  }
  catch(...)
  {
    // Set optimizer status to error
    setOptimizerStatus(gaengine::GAStatus::ERROR);
    throw;
  }

  // Set the stop reason
  switch (stopReason)
  {
    case EA::StopReason::MaxGenerations:
      setOptimizerStatus(gaengine::GAStatus::MAX_GENERATIONS);
      break;
    case EA::StopReason::StallAverage:
    case EA::StopReason::StallBest:
      setOptimizerStatus(gaengine::GAStatus::STALL_BEST);
      break;
    case EA::StopReason::UserRequest:
      setOptimizerStatus(gaengine::GAStatus::TIMEOUT);
      break;
    default:
      setOptimizerStatus(gaengine::GAStatus::UNKNOWN);
      break;
  }
}  // cleanupSearch

void OpenGAOptimizer::interruptOptimizationProcess()
{
  // Set the "user request stop" flag to true to stop the optimization process
  pGAOptimizer->user_request_stop = true;
}  // interruptOptimizationProcess

void OpenGAOptimizer::loadEnvironment()
{
  // Set default values for objective
  pEnvironment->setObjectiveValue(pBestCost.at(0));

  // Get the type of environment
  const auto envType = pEnvironment->getGAEnvironmentType();
  if (envType == GAEnvironment::GAEnvironmentType::GA_ENV_STD)
  {
    GAStdEnvironment* env = GAStdEnvironment::cast(pEnvironment.get());
    if (!env)
    {
      throw std::runtime_error("OpenGAOptimizer - trying to cast a non-standard environment "
          "to a standard environment");
    }

    if (env->getChromosome().type == Chromosome::ChromosomeType::CT_DOUBLE)
    {
      loadStandardEnvironment<double>();
    }
    else
    {
      loadStandardEnvironment<int>();
    }
  }
  else
  {
    assert(envType == GAEnvironment::GAEnvironmentType::GA_ENV_CUSTOM);
    GACustomEnvironment* env = GACustomEnvironment::cast(pEnvironment.get());
    if (!env)
    {
      throw std::runtime_error("OpenGAOptimizer - trying to cast a non-custom environment "
          "to a custom environment");
    }

    if (env->getChromosome().type == Chromosome::ChromosomeType::CT_DOUBLE)
    {
      loadCustomEnvironment<double>();
    }
    else
    {
      loadCustomEnvironment<int>();
    }
  }
}  // loadEnvironment

}  // namespace optilab
