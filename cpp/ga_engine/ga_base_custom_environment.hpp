#ifndef GA_BASE_CUSTOM_ENVIRONMENT__H
#define GA_BASE_CUSTOM_ENVIRONMENT__H

#include <algorithm>
#include <cassert>
#include <cmath>
#include <cstddef>
#include <functional>
#include <iostream>
#include <limits>
#include <map>
#include <memory>
#include <set>
#include <thread>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <vector>

class GABaseCustomEnvironment {
public:
  GABaseCustomEnvironment() {}
  virtual ~GABaseCustomEnvironment() {}

  virtual void initGenes(std::vector<int>&) {}
  virtual void initGenes(std::vector<double>&) {}

  virtual std::vector<int> mutate(const std::vector<int>& x) { return x; }
  virtual std::vector<double> mutate(const std::vector<double>& x) { return x; }

  virtual std::vector<int> crossover(const std::vector<int>& x, const std::vector<int>& y)
      { return x; }
  virtual std::vector<double> crossover(const std::vector<double>& x, const std::vector<double>& y)
      { return x; }

  virtual double objectiveFunction(const std::vector<int>&)
  { return std::numeric_limits<int>::max(); }
  virtual double objectiveFunction(const std::vector<double>&)
  { return std::numeric_limits<double>::max(); }

  typedef GABaseCustomEnvironment* create_t();
  typedef void destroy_t(GABaseCustomEnvironment *);
};

#endif  // GA_BASE_CUSTOM_ENVIRONMENT__H
