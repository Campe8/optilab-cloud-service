//
// Copyright OptiLab 2019. All rights reserved.
//
// Constants for Model(s).
//

#pragma once

namespace optilab {

namespace gacustomenvironment {
extern const char GA_CUSTOM_ENVIRONMENT_CUSTOM_OBJECTIVE_CLASS_PREFIX[];
extern const char GA_CUSTOM_ENVIRONMENT_CUSTOM_OBJECTIVE_CLASS_SUFFIX[];
extern const char GA_CUSTOM_ENVIRONMENT_CUSTOM_OBJECTIVE_FCN_SIGNATURE_START[];
extern const char GA_CUSTOM_ENVIRONMENT_CUSTOM_OBJECTIVE_FCN_SIGNATURE_END[];
}  // gacustomenvironment

namespace gaenvironment {
extern const char GA_ENVIRONMENT_CROSSOVER_FRACTION[];
extern const char GA_ENVIRONMENT_CROSSOVER_TYPE[];
extern const char GA_ENVIRONMENT_CROSSOVER_TYPE_CYCLE[];
extern const char GA_ENVIRONMENT_CROSSOVER_TYPE_DIRECT_INSERTION[];
extern const char GA_ENVIRONMENT_CROSSOVER_TYPE_EDGE_RECOMBINATION[];
extern const char GA_ENVIRONMENT_CROSSOVER_TYPE_ORDER_1[];
extern const char GA_ENVIRONMENT_CROSSOVER_TYPE_ORDER_MULTIPLE[];
extern const char GA_ENVIRONMENT_CROSSOVER_TYPE_PMX[];
extern const char GA_ENVIRONMENT_FILE_LIST[];
extern const char GA_ENVIRONMENT_INITIALIZATION[];
extern const char GA_ENVIRONMENT_INITIALIZATION_PERMUTATION[];
extern const char GA_ENVIRONMENT_INITIALIZATION_RANDOM[];
extern const char GA_ENVIRONMENT_LIST[];
extern const char GA_ENVIRONMENT_MAX_GENERATIONS[];
extern const char GA_ENVIRONMENT_MULTI_THREADING[];
extern const char GA_ENVIRONMENT_MUTATION_TYPE[];
extern const char GA_ENVIRONMENT_MUTATION_TYPE_INSERTION[];
extern const char GA_ENVIRONMENT_MUTATION_TYPE_INVERSION[];
extern const char GA_ENVIRONMENT_MUTATION_TYPE_RANDOM_SLIDE[];
extern const char GA_ENVIRONMENT_MUTATION_TYPE_SCRAMBLE[];
extern const char GA_ENVIRONMENT_MUTATION_TYPE_SINGLE_SWAP[];
extern const char GA_ENVIRONMENT_MUTATION_RATE[];
extern const char GA_ENVIRONMENT_NAME[];
extern const char GA_ENVIRONMENT_OBJECT_ID[];
extern const char GA_ENVIRONMENT_OBJECT_LIST[];
extern const char GA_ENVIRONMENT_OBJECTIVE[];
extern const char GA_ENVIRONMENT_PARAMETERS[];
extern const char GA_ENVIRONMENT_POPULATION_SIZE[];
extern const char GA_ENVIRONMENT_ROOT_ID[];
extern const char GA_ENVIRONMENT_SEED[];
extern const char GA_ENVIRONMENT_STALL_BEST[];
extern const char GA_ENVIRONMENT_STALL_GEN_MAX[];
extern const char GA_ENVIRONMENT_TIMEOUT_SEC[];
extern const char GA_ENVIRONMENT_TYPE[];
extern const char GA_ENVIRONMENT_TYPE_CUSTOM[];
extern const char GA_ENVIRONMENT_TYPE_STANDARD[];
extern const char GA_ENVIRONMENT_VAR_TYPE_DOUBLE[];
extern const char GA_ENVIRONMENT_VAR_TYPE_INT[];
}  // namespace gaenvironment

}  // namespace optilab
