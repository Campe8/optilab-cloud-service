//
// Copyright OptiLab 2019-2020. All rights reserved.
//
// Utilities for GA optimizers.
//

#pragma once

#include <algorithm>      // for std::sort
#include <cassert>
#include <functional>     // for std::function
#include <cstddef>        // for std::size_t
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <utility>        // for std::pair
#include <vector>

#include "data_structure/json/json.hpp"
#include "data_structure/metrics/metrics_register.hpp"
#include "utilities/random_generators.hpp"

namespace optilab {

namespace gaengine {

enum class GAStatus : int {
  MAX_GENERATIONS,
  STALL_BEST,
  TIMEOUT,
  ERROR,
  UNKNOWN
};

}  // namespace gaengine

/// Namespace for dynamic library building
namespace gautilsdynlib {

std::string getCustomObjectiveFunctionArgument(const std::string& varId,
                                               const std::string& varType);

/// Returns the verbatim stringified version of the custom objective class.
/// The custom objective class provides a method called "objectiveFunction(...)" that
/// accepts a vector of "varType" named "varId" and returns the double value of
/// the objective
std::string customObjectiveClassToString(const std::string& varId, const std::string& varType,
                                         const std::string& objFcnDef);
}  // gautilsdynlib

}  // namespace optilab
