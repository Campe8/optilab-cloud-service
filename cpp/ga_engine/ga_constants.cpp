#include "ga_engine/ga_constants.hpp"

namespace optilab {

namespace gacustomenvironment {
const char GA_CUSTOM_ENVIRONMENT_CUSTOM_OBJECTIVE_CLASS_PREFIX[] = R"(
#include "ga_environment_objective_fcn.hpp"

#include <algorithm>
#include <array>
#include <cassert>
#include <cmath>
#include <cstddef>
#include <cstdio>
#include <functional>
#include <iostream>
#include <map>
#include <memory>
#include <set>
#include <thread>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <vector>

class CustomObjectiveFcn : public GAObjectiveFcn {
public:
)";

const char GA_CUSTOM_ENVIRONMENT_CUSTOM_OBJECTIVE_CLASS_SUFFIX[] = R"(
}
};

// the class factories
extern "C" GAObjectiveFcn* create() {
    return new CustomObjectiveFcn();
}

extern "C" void destroy(GAObjectiveFcn* p) {
    delete p;
}
)";

const char GA_CUSTOM_ENVIRONMENT_CUSTOM_OBJECTIVE_FCN_SIGNATURE_START[] = R"(
double objectiveFunction()";

const char GA_CUSTOM_ENVIRONMENT_CUSTOM_OBJECTIVE_FCN_SIGNATURE_END[] = R"() override {

)";

}  // gacustomenvironment

namespace gaenvironment {
const char GA_ENVIRONMENT_CROSSOVER_FRACTION[] = "crossoverFraction";
const char GA_ENVIRONMENT_CROSSOVER_TYPE[] = "crossover";
const char GA_ENVIRONMENT_CROSSOVER_TYPE_CYCLE[] = "cycle";
const char GA_ENVIRONMENT_CROSSOVER_TYPE_DIRECT_INSERTION[] = "directInsertion";
const char GA_ENVIRONMENT_CROSSOVER_TYPE_EDGE_RECOMBINATION[] = "edgeRecombination";
const char GA_ENVIRONMENT_CROSSOVER_TYPE_ORDER_1[] = "order1";
const char GA_ENVIRONMENT_CROSSOVER_TYPE_ORDER_MULTIPLE[] = "orderMultiple";
const char GA_ENVIRONMENT_CROSSOVER_TYPE_PMX[] = "pmx";
const char GA_ENVIRONMENT_FILE_LIST[] = "environmentFileList";
const char GA_ENVIRONMENT_INITIALIZATION[] = "initialization";
const char GA_ENVIRONMENT_INITIALIZATION_PERMUTATION[] = "permutation";
const char GA_ENVIRONMENT_INITIALIZATION_RANDOM[] = "initialization";
const char GA_ENVIRONMENT_LIST[] = "gaEnvironmentList";
const char GA_ENVIRONMENT_MAX_GENERATIONS[] = "maxGenerations";
const char GA_ENVIRONMENT_MULTI_THREADING[] = "multiThreading";
const char GA_ENVIRONMENT_MUTATION_TYPE[] = "mutation";
const char GA_ENVIRONMENT_MUTATION_TYPE_INSERTION[] = "insertion";
const char GA_ENVIRONMENT_MUTATION_TYPE_INVERSION[] = "inversion";
const char GA_ENVIRONMENT_MUTATION_TYPE_RANDOM_SLIDE[] = "randomSlide";
const char GA_ENVIRONMENT_MUTATION_TYPE_SCRAMBLE[] = "scramble";
const char GA_ENVIRONMENT_MUTATION_TYPE_SINGLE_SWAP[] = "singleSwap";
const char GA_ENVIRONMENT_MUTATION_RATE[] = "mutationRate";
const char GA_ENVIRONMENT_NAME[] = "model";
const char GA_ENVIRONMENT_OBJECT_ID[] = "id";
const char GA_ENVIRONMENT_OBJECT_LIST[] = "gaEnvironmentObjects";
const char GA_ENVIRONMENT_OBJECTIVE[] = "objective";
const char GA_ENVIRONMENT_PARAMETERS[] = "gaParameters";
const char GA_ENVIRONMENT_POPULATION_SIZE[] = "populationSize";
const char GA_ENVIRONMENT_ROOT_ID[] = "environmentId";
const char GA_ENVIRONMENT_SEED[] = "seed";
const char GA_ENVIRONMENT_STALL_BEST[] = "stallBest";
const char GA_ENVIRONMENT_STALL_GEN_MAX[] = "stallGenMax";
const char GA_ENVIRONMENT_TIMEOUT_SEC[] = "timeout";
const char GA_ENVIRONMENT_TYPE[] = "environmentType";
const char GA_ENVIRONMENT_TYPE_CUSTOM[] = "custom";
const char GA_ENVIRONMENT_TYPE_STANDARD[] = "standard";
const char GA_ENVIRONMENT_VAR_TYPE_DOUBLE[] = "double";
const char GA_ENVIRONMENT_VAR_TYPE_INT[] = "int";
}  // namespace gaenvironment

}  // namespace optilab
