#include "ga_engine/ga_environment.hpp"

namespace optilab {

GAEnvironment::GAEnvironment(GAEnvironmentType envType)
: pGAEnvironmentType(envType)
{
}

void GAEnvironment::loadEnvironment(const toolbox::GeneticModelProto& model)
{
  pEnvironmentName = model.environment_name();
  pMultithreading = model.multithread();
  pNumParentsInTournament = model.parents_in_tournament();
  pPopulationSize = model.population_size();
  pNumGenerations = model.num_generations();
  pMutationRate = model.mutation_rate();
  pCrossoverFraction = model.crossover_fraction();
  pTimeoutSec = model.timeout_sec();
  pStallBest = model.stall_best();
  pStallMax = model.stall_max_gen();
  pPseudoRandomSeed = model.random_seed();
  pPathObjectiveFcn = model.objective_function_path();

  loadChromosome(model.chromosome());

  // Load environment-specific information
  loadEnvironmentImpl(model);
}  // loadEnvironment

void GAEnvironment::loadChromosome(const toolbox::ChromosomeProto& chr)
{
  pChromosome.lb = chr.lower_bound();
  pChromosome.ub = chr.upper_bound();
  pChromosome.id = chr.id();

  pChromosome.dims = 1;
  for (int dimNum = 0; dimNum < chr.dimensions_size(); ++dimNum)
  {
    pChromosome.dims = pChromosome.dims * chr.dimensions(dimNum);
  }

  pChromosome.type =
          (chr.type() == ::optilab::toolbox::ChromosomeProto_ChromosomeType::
                  ChromosomeProto_ChromosomeType_CHROMOSOME_INT) ?
                          Chromosome::ChromosomeType::CT_INT :
                          Chromosome::ChromosomeType::CT_DOUBLE;
}  // loadChromosome

GAEnvironment::GAEnvironmentType GAEnvironment::getEnvironmentTypeFromModel(
        const toolbox::GeneticModelProto& model)
{
  if (model.has_standard_environment())
  {
    return GAEnvironmentType::GA_ENV_STD;
  }

  return GAEnvironmentType::GA_ENV_CUSTOM;
}  // getEnvironmentTypeFromModel


}  // namespace optilab
