//
// Copyright OptiLab 2019. All rights reserved.
//
// Class for GA optimizers based on the OpenGA framework.
// @note this class derives from the more general "Solver" class.
//

#pragma once

#include <cassert>
#include <cstdint>     // for uint64_t
#include <functional>  // for std::bind
#include <memory>      // for std::shared_ptr
#include <stdexcept>   // for std::runtime_error

#include <openGA/openga.hpp>

#include "ga_engine/ga_environment.hpp"
#include "ga_engine/ga_custom_environment.hpp"
#include "ga_engine/ga_custom_environment_impl.hpp"
#include "ga_engine/ga_std_environment.hpp"
#include "ga_engine/ga_std_environment_impl.hpp"
#include "ga_engine/ga_utilities.hpp"
#include "solver/solver.hpp"

namespace optilab {

class SYS_EXPORT_CLASS OpenGAOptimizer : public Solver {
 public:
  using SPtr = std::shared_ptr<OpenGAOptimizer>;

 public:
  /// Builds a new (openGA) optimizer running on the specified environment.
  /// @note throws invalid_argument on empty environment
  OpenGAOptimizer(GAEnvironment::SPtr env);

  virtual ~OpenGAOptimizer() = default;

  /// Returns the status of the solver
  virtual gaengine::GAStatus getStatus() const { return pOptimizerStatus; }

  /// Initializes the solver for the search
  virtual void initSearch();

  /// Executes cleanup code after search is completed
  /// and updates the objective values and best chromosomes in the environment
  virtual void cleanupSearch();

  /// Start the optimization process
  virtual void runOptimizer();

  /// Stops the (ongoing) search process and return
  virtual void interruptOptimizationProcess();

  /// Returns the generation number leading to the best cost found so far
  inline int getGenerationNumber() const { return pGenerationNumber; }

  /// Returns best cost found so far
  inline double getBestCost() const { return pBestCost[0]; }

  /// Returns average cost found so far
  inline double getAverageCost() const { return pAverageCost[0]; }

 protected:
  /// Sets the status of the solver
  inline void setOptimizerStatus(gaengine::GAStatus status)
  {
    pOptimizerStatus = status;
  }

 private:
  template<typename Real>
  using GASolution = typename StdEnvironmentImpl<Real>::Solution;

  template<typename Real>
  using GAType = EA::Genetic<GASolution<Real>, MiddleCost>;

  template<typename Real>
  using GACustomSolution = typename CustomEnvironmentImpl<Real>::Solution;

  template<typename Real>
  using GACustomType = EA::Genetic<GACustomSolution<Real>, CustomMiddleCost>;

 private:
  /// Status of the solving process
  gaengine::GAStatus pOptimizerStatus{gaengine::GAStatus::UNKNOWN};

  /// Environment to run on
  GAEnvironment::SPtr pEnvironment;

  /// Pointer to the actual instance of the (openGA) optimizer
  EA::GeneticBase::SPtr pGAOptimizer;

  /// Generation number
  int pGenerationNumber;

  /// Best cost found so far
  std::vector<double> pBestCost;

  /// Average cost found so far
  std::vector<double> pAverageCost;

  /// Vector containing the best chromosomes found so far
  std::vector<double> pBestChromosomes;

  /// Search timeout is second.
  /// @note a negative timeout means no timeout
  int pTimeoutSec;

  /// Loads the environment into the optimizer
  void loadEnvironment();

  /// Loads a standard environment
  template<typename Real>
  void loadStandardEnvironment()
  {
    GAStdEnvironment* env = GAStdEnvironment::cast(pEnvironment.get());
    if (!env)
    {
      throw std::runtime_error("OpenGAOptimizer - trying to cast a non-standard environment "
          "to a standard environment");
    }

    // Load best chromosomes vector
    pBestChromosomes.resize(env->getChromosome().dims, std::numeric_limits<double>::max());

    // Set timeout (if any)
    pTimeoutSec = env->timeoutSec();

    // Create the optimizer
    auto gaOpt = std::make_shared<GAType<Real>>();

    // Set the environment
    (*gaOpt).problem_mode = EA::GA_MODE::SOGA;
    (*gaOpt).multi_threading = env->useThreadParallelism();
    if ((*gaOpt).multi_threading)
    {
      // Switch between threads quickly
      (*gaOpt).idle_delay_us = 1;
    }
    (*gaOpt).verbose = false;
    (*gaOpt).num_parents_in_tournament = std::max(2, env->numParentsInTournament());
    (*gaOpt).population = static_cast<unsigned int>(env->populationSize());
    (*gaOpt).generation_max = static_cast<int>(env->numGenerations());
    (*gaOpt).crossover_fraction = env->crossoverFraction();
    (*gaOpt).mutation_rate = env->mutationRate();
    (*gaOpt).best_stall_max = static_cast<int>(env->stallMax());
    (*gaOpt).tol_stall_best = env->stallBest();

    // Bind GA functions
    auto envImpl = std::dynamic_pointer_cast<StdEnvironmentImpl<Real>>((env->getEnvironmentImpl()));
    if(!envImpl)
    {
      throw std::runtime_error("OpenGAOptimizer - invalid cast to StdEnvironmentImpl "
          "from StdEnvironment");
    }

    auto genesFcn = std::bind(&StdEnvironmentImpl<Real>::initGenes, envImpl.get(),
                              std::placeholders::_1, std::placeholders::_2);
    (*gaOpt).init_genes = genesFcn;

    auto evalFcn = std::bind(&StdEnvironmentImpl<Real>::evalSolution, envImpl.get(),
                             std::placeholders::_1, std::placeholders::_2);
    (*gaOpt).eval_solution = evalFcn;

    auto mutateFcn = std::bind(&StdEnvironmentImpl<Real>::mutate, envImpl.get(),
                               std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
    (*gaOpt).mutate = mutateFcn;

    auto crossoverFcn = std::bind(&StdEnvironmentImpl<Real>::crossover, envImpl.get(),
                                  std::placeholders::_1, std::placeholders::_2,
                                  std::placeholders::_3);
    (*gaOpt).crossover = crossoverFcn;

    // Set the lambda for the total fitness as the same objective value of the middle cost
    (*gaOpt).calculate_SO_total_fitness = [](const typename GAType<Real>::thisChromosomeType& X) {
      double finalCost = 0.0;
      finalCost += X.middle_costs.objective;
      return finalCost;
    };

    // Set the report as no-op except storing the best solution
    (*gaOpt).SO_report_generation = [this](int genNumber,
        const EA::GenerationType<GASolution<Real>, MiddleCost>& lastGen,
        const GASolution<Real>& bestGenes) {
      assert(pBestChromosomes.size() == bestGenes.solution.size());
      pGenerationNumber = genNumber;
      pBestCost[0] = lastGen.best_total_cost;
      pAverageCost[0] = lastGen.average_cost;

      for (int cidx = 0; cidx < static_cast<int>(bestGenes.solution.size()); ++cidx)
      {
        pBestChromosomes[cidx] = static_cast<double>(bestGenes.solution[cidx]);
      }

      // Check for timeout and interrupt the search process if timeout went off
      if ((this->pTimeoutSec >= 0) && (static_cast<int>(lastGen.exe_time) > this->pTimeoutSec))
      {
        this->interruptOptimizationProcess();
      }
    };

    // Store the pointer to the optimizer
    pGAOptimizer = gaOpt;
  }

  /// Loads a standard environment
  template<typename Real>
  void loadCustomEnvironment()
  {
    GACustomEnvironment* env = GACustomEnvironment::cast(pEnvironment.get());
    if (!env)
    {
      throw std::runtime_error("OpenGAOptimizer - trying to cast a non-custom environment "
          "to a custom environment");
    }

    // Load best chromosomes vector
    pBestChromosomes.resize(env->getChromosome().dims, std::numeric_limits<double>::max());

    // Set timeout (if any)
    pTimeoutSec = env->timeoutSec();

    // Create the optimizer
    auto gaOpt = std::make_shared<GACustomType<Real>>();

    // Set the environment
    (*gaOpt).problem_mode = EA::GA_MODE::SOGA;
    (*gaOpt).multi_threading = env->useThreadParallelism();
    if ((*gaOpt).multi_threading)
    {
      // Switch between threads quickly
      (*gaOpt).idle_delay_us = 1;
    }
    (*gaOpt).verbose = false;
    (*gaOpt).num_parents_in_tournament = std::max(2, env->numParentsInTournament());
    (*gaOpt).population = static_cast<unsigned int>(env->populationSize());
    (*gaOpt).generation_max = static_cast<int>(env->numGenerations());
    (*gaOpt).crossover_fraction = env->crossoverFraction();
    (*gaOpt).mutation_rate = env->mutationRate();
    (*gaOpt).best_stall_max = static_cast<int>(env->stallMax());
    (*gaOpt).tol_stall_best = env->stallBest();

    // Bind GA functions
    auto envImpl =
        std::dynamic_pointer_cast<CustomEnvironmentImpl<Real>>((env->getEnvironmentImpl()));
    if(!envImpl)
    {
      throw std::runtime_error("OpenGAOptimizer - invalid cast to CustomEnvironmentImpl "
          "from CustomEnvironment");
    }

    auto genesFcn = std::bind(&CustomEnvironmentImpl<Real>::initGenes, envImpl.get(),
                              std::placeholders::_1, std::placeholders::_2);
    (*gaOpt).init_genes = genesFcn;

    auto evalFcn = std::bind(&CustomEnvironmentImpl<Real>::evalSolution, envImpl.get(),
                             std::placeholders::_1, std::placeholders::_2);
    (*gaOpt).eval_solution = evalFcn;

    auto mutateFcn = std::bind(&CustomEnvironmentImpl<Real>::mutate, envImpl.get(),
                               std::placeholders::_1, std::placeholders::_2,
                               std::placeholders::_3);
    (*gaOpt).mutate = mutateFcn;

    auto crossoverFcn = std::bind(&CustomEnvironmentImpl<Real>::crossover, envImpl.get(),
                                  std::placeholders::_1, std::placeholders::_2,
                                  std::placeholders::_3);
    (*gaOpt).crossover = crossoverFcn;

    // Set the lambda for the total fitness as the same objective value of the middle cost
    (*gaOpt).calculate_SO_total_fitness =
        [](const typename GACustomType<Real>::thisChromosomeType& X) {
      double finalCost = 0.0;
      finalCost += X.middle_costs.objective;
      return finalCost;
    };

    // Set the report as no-op except storing the best solution
    (*gaOpt).SO_report_generation = [this](int genNumber,
        const EA::GenerationType<GACustomSolution<Real>, CustomMiddleCost>& lastGen,
        const GACustomSolution<Real>& bestGenes) {
      assert(pBestChromosomes.size() == bestGenes.solution.size());
      pGenerationNumber = genNumber;
      pBestCost[0] = lastGen.best_total_cost;
      pAverageCost[0] = lastGen.average_cost;

      for (int cidx = 0; cidx < static_cast<int>(bestGenes.solution.size()); ++cidx)
      {
        pBestChromosomes[cidx] = static_cast<double>(bestGenes.solution[cidx]);
      }

      // Check for timeout and interrupt the search process if timeout went off
      if ((this->pTimeoutSec >= 0) && (static_cast<int>(lastGen.exe_time) > this->pTimeoutSec))
      {
        this->interruptOptimizationProcess();
      }
    };

    // Store the pointer to the optimizer
    pGAOptimizer = gaOpt;
  }

};

}  // namespace optilab
