// Base class
#include "ga_engine/ga_std_environment.hpp"

#include <algorithm>  // for std::min
#include <cstdio>     // for remove
#include <limits>
#include <stdexcept>  // for std::runtime_error

#include <spdlog/spdlog.h>

#include "ga_engine/ga_constants.hpp"
#include "ga_engine/ga_utilities.hpp"
#include "model/model_constants.hpp"
#include "utilities/file_sys.hpp"
#include "utilities/random_generators.hpp"

namespace {

#ifdef __APPLE__
const char kCompilerName[] = "g++";
#else
const char kCompilerName[] = "g++";
#endif


constexpr int kDynLibNameLen = 8;
const char kDynLibExt[] = ".so";
const char kIncludePath[] = "-Iinclude/optilab";
const char kOptiLabIncludePath[] = "include/optilab";
const char kTempFileExt[] = "cpp";

optilab::CrossoverType getCrossoverType(
        const ::optilab::toolbox::StandardGeneticEnvironmentProto& env)
{
  switch (env.crossover())
  {
    case ::optilab::toolbox::StandardGeneticEnvironmentProto_CrossoverType::
    StandardGeneticEnvironmentProto_CrossoverType_ORDER_1:
      return optilab::CrossoverType::CT_ORDER_1;
    case ::optilab::toolbox::StandardGeneticEnvironmentProto_CrossoverType::
    StandardGeneticEnvironmentProto_CrossoverType_EDGE_RECOMBINATION:
      return optilab::CrossoverType::CT_EDGE_RECOMBINATION;
    case ::optilab::toolbox::StandardGeneticEnvironmentProto_CrossoverType::
    StandardGeneticEnvironmentProto_CrossoverType_PMX:
      return optilab::CrossoverType::CT_PMX;
    case ::optilab::toolbox::StandardGeneticEnvironmentProto_CrossoverType::
    StandardGeneticEnvironmentProto_CrossoverType_ORDER_MULTIPLE:
      return optilab::CrossoverType::CT_ORDER_MULTIPLE;
    case ::optilab::toolbox::StandardGeneticEnvironmentProto_CrossoverType::
    StandardGeneticEnvironmentProto_CrossoverType_CYCLE:
      return optilab::CrossoverType::CT_CYCLE;
    case ::optilab::toolbox::StandardGeneticEnvironmentProto_CrossoverType::
    StandardGeneticEnvironmentProto_CrossoverType_DIRECT_INSERTION:
      return optilab::CrossoverType::CT_DIRECT_INSERTION;
    default:
    {
      const std::string errMsg = "Invalid crossover type";
      spdlog::error(errMsg);
      throw std::runtime_error(errMsg);
    }
  }
}  // getCrossoverType

optilab::MutationType getMutationType(
        const ::optilab::toolbox::StandardGeneticEnvironmentProto& env)
{
  switch(env.mutation())
  {
    case ::optilab::toolbox::StandardGeneticEnvironmentProto_MutationType::
    StandardGeneticEnvironmentProto_MutationType_INSERTION:
      return optilab::MutationType::MT_INSERTION;
    case ::optilab::toolbox::StandardGeneticEnvironmentProto_MutationType::
    StandardGeneticEnvironmentProto_MutationType_INVERSION:
      return optilab::MutationType::MT_INVERSION;
    case ::optilab::toolbox::StandardGeneticEnvironmentProto_MutationType::
    StandardGeneticEnvironmentProto_MutationType_RANDOM_SLIDE:
      return optilab::MutationType::MT_RANDOM_SLIDE;
    case ::optilab::toolbox::StandardGeneticEnvironmentProto_MutationType::
    StandardGeneticEnvironmentProto_MutationType_SINGLE_SWAP:
      return optilab::MutationType::MT_SINGLE_SWAP;
    case ::optilab::toolbox::StandardGeneticEnvironmentProto_MutationType::
    StandardGeneticEnvironmentProto_MutationType_SCRAMBLE:
      return optilab::MutationType::MT_SCRAMBLE;
    default:
    {
      const std::string errMsg = "Invalid mutation type";
      spdlog::error(errMsg);
      throw std::runtime_error(errMsg);
    }
  }
}  // getMutationType

optilab::InitializationType getInitializationType(
        const ::optilab::toolbox::StandardGeneticEnvironmentProto& env)
{
  switch(env.initialization())
  {
    case ::optilab::toolbox::StandardGeneticEnvironmentProto_Initialization::
    StandardGeneticEnvironmentProto_Initialization_PERMUTATION:
      return optilab::InitializationType::IT_PERMUTATION;
    case ::optilab::toolbox::StandardGeneticEnvironmentProto_Initialization::
    StandardGeneticEnvironmentProto_Initialization_RANDOM:
      return optilab::InitializationType::IT_RANDOM;
    default:
    {
      const std::string errMsg = "Invalid initialization type";
      spdlog::error(errMsg);
      throw std::runtime_error(errMsg);
    }
  }
}  // getInitializationType

}  // namespace

namespace optilab {

GAStdEnvironment::GAStdEnvironment()
: GAEnvironment(GAEnvironmentType::GA_ENV_STD),
  pEnvironmentImplPtr(nullptr)
{
}

bool GAStdEnvironment::isa(const GAEnvironment* env)
{
  return env &&
      env->getGAEnvironmentType() == GAEnvironment::GAEnvironmentType::GA_ENV_STD;
}

GAStdEnvironment* GAStdEnvironment::cast(GAEnvironment* env)
{
  if (!isa(env)) return nullptr;
  return static_cast<GAStdEnvironment*>(env);
}

const GAStdEnvironment* GAStdEnvironment::cast(const GAEnvironment* env)
{
  if (!isa(env)) return nullptr;
  return static_cast<const GAStdEnvironment*>(env);
}

GAStdEnvironment::~GAStdEnvironment()
{
  // Remove the library
  remove(pDynLibName.c_str());
}

void GAStdEnvironment::loadEnvironmentImpl(const toolbox::GeneticModelProto& model)
{
  if (!model.has_standard_environment())
  {
    const std::string errMsg = "GAStdEnvironment - loadEnvironmentImpl: "
            "the given environment is non standard";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  const auto& env = model.standard_environment();
  const CrossoverType crossoverType = getCrossoverType(env);
  const MutationType mutationType = getMutationType(env);
  const InitializationType initType = getInitializationType(env);

  // Chromosome descriptor
  const auto& chromo = getChromosome();

  // Load the objective function as a dynamic library
  pDynLibName = compileObjectiveIntoDynLib(getObjectiveFcnPath());

  // Load the environment
  if (getChromosomeBaseType() == Chromosome::ChromosomeType::CT_DOUBLE)
  {
    pEnvironmentImplPtr = std::make_shared<StdEnvironmentImpl<double>>(
            chromo.dims,
            chromo.lb,
            chromo.ub,
            initType,
            crossoverType,
            mutationType,
            pseudoRandomSeed(),
            pDynLibName);
  }
  else
  {
    pEnvironmentImplPtr = std::make_shared<StdEnvironmentImpl<int>>(
            chromo.dims,
            static_cast<int>(chromo.lb),
            static_cast<int>(chromo.ub),
            initType,
            crossoverType,
            mutationType,
            pseudoRandomSeed(),
            pDynLibName);
  }
}  // loadEnvironmentImpl

std::string GAStdEnvironment::compileObjectiveIntoDynLib(const std::string& fcn_path)
{
  // Compile the file and create a dynamic library with random name
  const std::string libName = utilsrandom::buildRandomAlphaNumString(kDynLibNameLen) +
          std::string(kDynLibExt);

  // Create the dynamic library
  std::string buildOutput;
  std::pair<int, std::string> tempFileInfo;

  // Build the library
  const std::string path1 = utilsfilesys::getFullPathToLocation(
          { utilsfilesys::getPathToProgramLocation(), "..", std::string(kOptiLabIncludePath) });
  const std::string path2 = utilsfilesys::getFullPathToLocation(
          { utilsfilesys::getPathToProgramLocation(), std::string(kOptiLabIncludePath) });
  std::string incPath = "-I" + path1;
  incPath += " -I" + path2;
  buildOutput = utilsfilesys::compileAndCreateDynamicLib(fcn_path, libName,
                                                         std::string(kCompilerName),
                                                         incPath /*std::string(kIncludePath)*/);

  // Check the output of the built
  if (!buildOutput.empty())
  {
    const std::string errMsg = "GAStdEnvironment - error building the dynamic library: " +
        buildOutput;
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  // Return the library name
  return libName;
}  // compileObjectiveIntoDynLib

}  // namespace optilab
