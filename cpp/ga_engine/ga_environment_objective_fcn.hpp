#ifndef GA_ENVIRONMENT_OBJECTIVE_FCN_H
#define GA_ENVIRONMENT_OBJECTIVE_FCN_H

#include <algorithm>
#include <cassert>
#include <cmath>
#include <cstddef>
#include <functional>
#include <iostream>
#include <limits>
#include <map>
#include <memory>
#include <set>
#include <thread>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <vector>

class GAObjectiveFcn {
public:
  GAObjectiveFcn() {}
  virtual ~GAObjectiveFcn() {}

  virtual double objectiveFunction(const std::vector<int>&)
  { return std::numeric_limits<int>::max(); }
  virtual double objectiveFunction(const std::vector<double>&)
  { return std::numeric_limits<double>::max(); }

  typedef GAObjectiveFcn* create_t();
  typedef void destroy_t(GAObjectiveFcn *);
};

#endif  // GA_ENVIRONMENT_OBJECTIVE_FCN_H
