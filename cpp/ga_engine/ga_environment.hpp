//
// Copyright OptiLab 2019-2020. All rights reserved.
//
// Environment for Genetic Algorithms optimizers.
//

#pragma once

#include "engine/engine.hpp"

#include <cstdint>  // for uint32_t
#include <limits>   // for std::numeric_limits
#include <memory>   // for std::shared_ptr
#include <string>
#include <vector>

#include "optilab_protobuf/evolutionary_model.pb.h"
#include "system/system_export_defs.hpp"

namespace optilab {

struct SYS_EXPORT_STRUCT Chromosome {
  enum ChromosomeType {
    CT_INT = 0,
    CT_DOUBLE
  };

  std::string id;
  uint32_t dims{1};
  double lb{0.0};
  double ub{0.0};
  ChromosomeType type{ChromosomeType::CT_DOUBLE};
};

class SYS_EXPORT_CLASS GAEnvironment {
 public:
  /// Types of GA environment
  enum GAEnvironmentType : char {
    /// User specified environment
    GA_ENV_CUSTOM = 0,

    /// Standard GA environment
    GA_ENV_STD
  };

  /// List of chromosomes.
  /// @note usually a list of chromosomes is represented as a list of bits.
  /// In this environment, the chromosome can have double values with
  /// specified lower and upper bounds.
  /// The underneath genetic solver will map these representation to
  /// the correspondent bit representeation
  using ChromosomeList = std::vector<double>;

  using SPtr = std::shared_ptr<GAEnvironment>;

 public:
  virtual ~GAEnvironment() = default;

  /// Loads this environment from the given protobuf message
  void loadEnvironment(const toolbox::GeneticModelProto& model);

  /// Returns the chromosome
  inline const Chromosome& getChromosome() const noexcept { return pChromosome; }

  /// Sets objective list values
  inline void setObjectiveValue(double obj) noexcept { pObjectiveValue = obj; }

  /// Returns the objective value
  inline double getObjectiveValue() const noexcept { return pObjectiveValue; }

  /// Utility function: returns the type of environment given the protobuf environment
  static GAEnvironmentType getEnvironmentTypeFromModel(const toolbox::GeneticModelProto& model);

  /// Returns the type of this GA environment
  inline GAEnvironmentType getGAEnvironmentType() const noexcept { return pGAEnvironmentType; }

  /// Sets best chromosomes
  inline void setBestChromosomes(const ChromosomeList& chr) noexcept
  {
    pBestChromosomes = chr;
  }

  /// Returns the list of the best chromosomes
  inline const ChromosomeList& getBestChromosomes() const noexcept { return pBestChromosomes; }

  /// Returns the name of this environment
  inline const std::string& getEnvironmentName() const noexcept { return pEnvironmentName; }

  /// Returns the path to the objective function definition file
  inline const std::string& getObjectiveFcnPath() const noexcept { return pPathObjectiveFcn; }

  /// Returns true if "multiThreading" option is enabled, returns false otherwise
  inline bool useThreadParallelism() const noexcept { return pMultithreading; }

  /// Returns the number of parents to consider for tournaments
  /// when performing crossover.
  /// Default number is two parents
  inline int numParentsInTournament() const noexcept { return pNumParentsInTournament; }

  /// Returns the population size
  inline uint32_t populationSize() const noexcept { return pPopulationSize; }

  /// Returns the (maximum) number of generations
  inline uint32_t numGenerations() const noexcept { return pNumGenerations; }

  /// Returns the crossover fraction.
  /// @note crossover fraction is always between 0 and 1
  inline double crossoverFraction() const noexcept { return pCrossoverFraction; }

  /// Returns the mutation rate.
  /// @note mutation rate percentage is between 0 and 1
  inline double mutationRate() const noexcept { return pMutationRate; }

  /// Returns timeout in sec.
  /// @note a negative value represents no timeout
  inline int timeoutSec() const noexcept { return pTimeoutSec; }

  /// Returns the stalling value, i.e., the delta on the best solution found so far
  /// to consider the current solution equivalent to the best solution
  inline double stallBest() const noexcept { return pStallBest; }

  /// Returns the number of generations that need to provide stallBest solutions
  /// before the evolution is stopped
  inline uint32_t stallMax() const noexcept { return pStallMax; }

  /// Returns the pseudo-random seed
  inline int pseudoRandomSeed() const noexcept { return pPseudoRandomSeed; }

  /// Returns the identifier of the chromosome list
  inline const std::string& getChromosomeId() const noexcept
  {
    return pChromosome.id;
  }

  /// Returns the base type of the solution
  inline Chromosome::ChromosomeType getChromosomeBaseType() const noexcept
  {
    return pChromosome.type;
  }

 protected:
  /// List of objective values
  double pObjectiveValue{std::numeric_limits<double>::max()};

  /// Best chromosomes
  std::vector<double> pBestChromosomes;

  GAEnvironment(GAEnvironmentType envType);

  /// Derived classes implement this method w.r.t. their environment
  virtual void loadEnvironmentImpl(const toolbox::GeneticModelProto& model) = 0;

 private:
  GAEnvironmentType pGAEnvironmentType;

  /// Name of this environment
  std::string pEnvironmentName;

  /// Path to the file containing the objective function definition
  std::string pPathObjectiveFcn;

  /// Chromosome description
  Chromosome pChromosome;

  /// Flag for multi-threading option
  bool pMultithreading{false};

  /// Number of parents to be considered while running crossover
  int pNumParentsInTournament{2};

  /// Population size.
  /// @note population size contains at least one individual
  uint32_t pPopulationSize{1};

  /// Number of generations
  uint32_t pNumGenerations{1};

  /// Crossover fraction in [0, 1].
  /// @note by default the crossover fraction is 0.7
  double pCrossoverFraction{0.7};

  /// Mutation rate in perc., represented as a value between 0 and 1.
  /// @note by default, mutation rate is 20%
  double pMutationRate{0.2};

  /// Timeout for the optimizer in seconds.
  /// @note by default there is no timeout
  int pTimeoutSec{-1};

  /// Stalling, delta value on optimal solution.
  /// @note by default, the stalling value is 6 decimals
  double pStallBest{1e-6};

  /// Number of generations stalled on "pStallBest" before stopping the engine.
  /// @note by default the maximum number of stalled generation is set to 10
  uint32_t pStallMax{10};

  /// Seed for pseudo-random algorihtm.
  /// @note if negative, uses the current system's timestamp
  int pPseudoRandomSeed{-1};

  /// Loads the chromosome description
  void loadChromosome(const toolbox::ChromosomeProto& chr);
};

}  // namespace optilab
