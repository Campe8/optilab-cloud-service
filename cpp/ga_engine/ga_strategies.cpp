#include "ga_engine/ga_strategies.hpp"

#include <spdlog/spdlog.h>

#include "engine/engine_constants.hpp"
#include "ga_engine/ga_constants.hpp"

namespace optilab {

namespace gastrategies {

template <>
void individualRandomInit<double>(std::vector<double>& vals, double lb, double ub,
                                  utilsrandom::RandomGenerator* rndGen)
{
  assert(rndGen);
  for (auto& v : vals)
  {
    v = rndGen->randReal(lb, ub);
  }
}  // individualRandomInit

template <>
void individualRandomInit<int>(std::vector<int>& vals, int lb, int ub,
                               utilsrandom::RandomGenerator* rndGen)
{
  assert(rndGen);
  for (auto& v : vals)
  {
    v = rndGen->randInt(lb, ub);
  }
}  // individualRandomInit

}  // namespace gastrategies

}  // namespace optilab
