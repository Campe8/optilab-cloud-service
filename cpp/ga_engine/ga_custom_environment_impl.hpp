//
// Copyright OptiLab 2019-2020. All rights reserved.
//
// Implementation for a custom GA environment.
//

#pragma once

#include <cstdint>    // for uint32_t
#include <exception>  // for std::exception
#include <memory>     // for std::shared_ptr
#include <stdexcept>  // for std::runtime_error
#include <vector>

#include <openGA/openga.hpp>
#include <spdlog/spdlog.h>

#include "ga_engine/ga_base_custom_environment.hpp"
#include "system/system_export_defs.hpp"
#include "utilities/file_sys.hpp"
#include "utilities/random_generators.hpp"

namespace optilab
{

struct CustomMiddleCost
{
  // This is where the results of simulation is stored
  // but not yet finalized
  double objective;
};

/// This base class is declared as base pointer class
class SYS_EXPORT_CLASS CustomEnvironmentBaseImpl {
 public:
  using SPtr = std::shared_ptr<CustomEnvironmentBaseImpl>;

 public:
  virtual ~CustomEnvironmentBaseImpl() = default;
};

template<typename Real>
class SYS_EXPORT_CLASS CustomEnvironmentImpl : public CustomEnvironmentBaseImpl {
 public:
  struct Solution
  {
    std::vector<Real> solution;
  };

  using SPtr = std::shared_ptr<CustomEnvironmentImpl<Real>>;

 public:
  /// Parameters:
  /// numVars: number of variables in the solution with domain [lowerBound, upperBound)
  /// lowerBound: lower bound on each variable (included)
  /// upperBound: upper bound on each variable (excluded)
  /// seed: seed used for the pseudo-random number generator.
  /// If seed is less than zero, use the current timestamp as seed
  CustomEnvironmentImpl(uint32_t numVars,
                        Real lowerBound,
                        Real upperBound,
                        int seed,
                        const std::string& dynLibName)
      : pNumVars(numVars),
        pVarLb(lowerBound),
        pVarUb(upperBound),
        pSeed(seed),
        pEnvironmentDynLibName(dynLibName),
        pCustomEnvClassPtr(nullptr),
        pRndGen(nullptr)
  {
    pRndGen =
        std::unique_ptr<utilsrandom::RandomGenerator>(new utilsrandom::RandomGenerator(pSeed));

    if (pEnvironmentDynLibName.empty())
    {
      throw std::invalid_argument("CustomEnvironmentImpl - "
          "empty optimization dynamic library name");
    }

    // Load the library
    try
    {
      auto objClass = new utilsfilesys::DLClass<GABaseCustomEnvironment>(pEnvironmentDynLibName);
      pCustomEnvClassPtr = std::shared_ptr<GABaseCustomEnvironment>(objClass->make_obj());
      delete objClass;
    }
    catch (std::exception& e)
    {
      const std::string& errMsg = "CustomEnvironmentImpl - cannot load library " +
          pEnvironmentDynLibName + ": " + std::string(e.what());
      spdlog::error(errMsg);
      throw;
    }
    catch (...)
    {
      const std::string& errMsg = "CustomEnvironmentImpl - cannot load library " +
          pEnvironmentDynLibName;
      spdlog::error(errMsg);
      throw;
    }

    if (!pCustomEnvClassPtr)
    {
      const std::string& errMsg = "CustomEnvironmentImpl - "
          "failed to instantiate the objective class";
      spdlog::error(errMsg);
      throw std::runtime_error(errMsg);
    }
  }

  virtual ~CustomEnvironmentImpl() = default;

  /// Public interface for mutating an individual
  Solution mutate(const Solution& X_base, const std::function<double(void)>& rnd01,
                  double shrink_scale)
  {
    return mutateImpl(X_base, rnd01, shrink_scale);
  }  // mutate

  /// Public interface for performing a crossover between two individuals and
  /// generating an offspring individual
  Solution crossover(const Solution& X1, const Solution& X2,
                     const std::function<double(void)>& rnd01)
  {
    return crossoverImpl(X1, X2, rnd01);
  }

  /// Public interface for initializing the genes
  void initGenes(Solution& sol, const std::function<double(void)>& rnd01)
  {
    // Set the size of the vector collecting the chromosomes
    sol.solution.resize(getNumVars(), 0);
    initGenesImpl(sol, rnd01);
  }

  /// Public interface for evaluating a solution
  bool evalSolution(const Solution& sol, CustomMiddleCost& cost)
  {
    return evalSolutionImpl(sol, cost);
  }

 protected:
  /// Returns the number of variables/chromosomes in the solution
  inline uint32_t getNumVars() const { return pNumVars; }

  /// Returns the variable lower bound
  inline Real getVarLb() const { return pVarLb; }

  /// Returns the variable upper bound
  inline Real getVarUb() const { return pVarUb; }

  virtual Solution mutateImpl(const Solution& X_base, const std::function<double(void)>&, double)
  {
    if (!pCustomEnvClassPtr)
    {
      throw std::runtime_error("CustomEnvironmentImpl - dynamic library for custom environment "
          "not present");
    }

    Solution mutatedSolution;
    mutatedSolution.solution = pCustomEnvClassPtr->mutate(X_base.solution);

    return mutatedSolution;
  }

  virtual Solution crossoverImpl(const Solution& X1, const Solution& X2,
                                 const std::function<double(void)>&)
  {
    if (!pCustomEnvClassPtr)
    {
      throw std::runtime_error("CustomEnvironmentImpl - dynamic library for custom environment "
          "not present");
    }

    Solution child;
    child.solution = pCustomEnvClassPtr->crossover(X1.solution, X2.solution);

    return child;
  }

  virtual void initGenesImpl(Solution& sol, const std::function<double(void)>&)
  {
    if (!pCustomEnvClassPtr)
    {
      throw std::runtime_error("CustomEnvironmentImpl - dynamic library for custom environment "
          "not present");
    }

    pCustomEnvClassPtr->initGenes(sol.solution);
  }

  virtual bool evalSolutionImpl(const Solution& sol, CustomMiddleCost& cost)
  {
    if (!pCustomEnvClassPtr)
    {
      throw std::runtime_error("CustomEnvironmentImpl - dynamic library for custom environment "
          "not present");
    }
    cost.objective = pCustomEnvClassPtr->objectiveFunction(sol.solution);
    return true;
  }

 private:
  /// Number of variables in the solution
  uint32_t pNumVars;

  /// Lower bound on each variables
  Real pVarLb;

  /// Upper bound on each variable
  Real pVarUb;

  /// Seed to be used for pseudo-random generator.
  /// If less than zero, use timestamp
  int pSeed;

  /// Name of the dynamic library to load.
  /// The dynamic library will have the user-specified environment functions
  std::string pEnvironmentDynLibName;

  /// Pointer to the custom environment class
  std::shared_ptr<GABaseCustomEnvironment> pCustomEnvClassPtr;

  /// Random generator
  std::unique_ptr<utilsrandom::RandomGenerator> pRndGen;
};

}  // namespace optilab
