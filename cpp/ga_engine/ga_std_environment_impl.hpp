//
// Copyright OptiLab 2019-2020. All rights reserved.
//
// Implementation for a standard GA environment.
//

#pragma once

#include <cstdint>    // for uint32_t
#include <exception>  // for std::exception
#include <memory>     // for std::shared_ptr
#include <stdexcept>  // for std::runtime_error
#include <vector>

#include <openGA/openga.hpp>
#include <spdlog/spdlog.h>

#include "ga_engine/ga_environment_objective_fcn.hpp"
#include "ga_engine/ga_strategies.hpp"
#include "system/system_export_defs.hpp"
#include "utilities/file_sys.hpp"
#include "utilities/random_generators.hpp"

namespace optilab
{

enum CrossoverType
{
  CT_ORDER_1,
  CT_CYCLE,
  CT_EDGE_RECOMBINATION,
  CT_PMX,
  CT_ORDER_MULTIPLE,
  CT_DIRECT_INSERTION
};

enum MutationType
{
  MT_INVERSION,
  MT_RANDOM_SLIDE,
  MT_INSERTION,
  MT_SINGLE_SWAP,
  MT_SCRAMBLE
};

enum InitializationType
{
  IT_RANDOM,
  IT_PERMUTATION
};

struct MiddleCost
{
  // This is where the results of simulation is stored
  // but not yet finalized
  double objective;
};

/// This base class is declared as base pointer class
class SYS_EXPORT_CLASS StdEnvironmentBaseImpl {
 public:
  using SPtr = std::shared_ptr<StdEnvironmentBaseImpl>;

 public:
  virtual ~StdEnvironmentBaseImpl() = default;
};

template<typename Real>
class SYS_EXPORT_CLASS StdEnvironmentImpl : public StdEnvironmentBaseImpl {
 public:
  struct Solution
  {
    std::vector<Real> solution;
  };

  using SPtr = std::shared_ptr<StdEnvironmentImpl<Real>>;

 public:
  /// Parameters:
  /// numVars: number of variables in the solution with domain [lowerBound, upperBound)
  /// lowerBound: lower bound on each variable (included)
  /// upperBound: upper bound on each variable (excluded)
  /// initType: initialization type
  /// crossType: crossover type
  /// mutType: mutation type
  /// seed: seed used for the pseudo-random number generator.
  /// If seed is less than zero, use the current timestamp as seed
  StdEnvironmentImpl(uint32_t numVars,
                     Real lowerBound,
                     Real upperBound,
                     InitializationType initType,
                     CrossoverType crossType,
                     MutationType mutType,
                     int seed,
                     const std::string& optDynLibName)
      : pNumVars(numVars),
        pVarLb(lowerBound),
        pVarUb(upperBound),
        pInitType(initType),
        pCrossoverType(crossType),
        pMutationStrategy(mutType),
        pSeed(seed),
        pOptimizationDynLibName(optDynLibName),
        pObjFcnPtr(nullptr),
        pRndGen(nullptr)
  {
    pRndGen =
        std::unique_ptr<utilsrandom::RandomGenerator>(new utilsrandom::RandomGenerator(pSeed));

    if (pOptimizationDynLibName.empty())
    {
      throw std::invalid_argument("StdEnvironmentImpl - "
          "empty optimization dynamic library name");
    }

    // Load the library
    try
    {
      auto objClass = new utilsfilesys::DLClass<GAObjectiveFcn>(pOptimizationDynLibName);
      pObjFcnPtr = std::shared_ptr<GAObjectiveFcn>(objClass->make_obj());
      delete objClass;
    }
    catch (std::exception& e)
    {
      const std::string& errMsg = "StdEnvironmentImpl - cannot load library " +
          pOptimizationDynLibName + ": " + std::string(e.what());
      spdlog::error(errMsg);
      throw;
    }
    catch (...)
    {
      const std::string& errMsg = "StdEnvironmentImpl - cannot load library " +
          pOptimizationDynLibName;
      spdlog::error(errMsg);
      throw;
    }

    if (!pObjFcnPtr)
    {
      const std::string& errMsg = "StdEnvironmentImpl - "
          "failed to instantiate the objective class";
      spdlog::error(errMsg);
      throw std::runtime_error(errMsg);
    }
  }

  virtual ~StdEnvironmentImpl() = default;

  /// Public interface for mutating an individual
  Solution mutate(const Solution& X_base, const std::function<double(void)>& rnd01,
                  double shrink_scale)
  {
    return mutateImpl(X_base, rnd01, shrink_scale);
  }  // mutate

  /// Public interface for performing a crossover between two individuals and
  /// generating an offspring individual
  Solution crossover(const Solution& X1, const Solution& X2,
                     const std::function<double(void)>& rnd01)
  {
    return crossoverImpl(X1, X2, rnd01);
  }

  /// Public interface for initializing the genes
  void initGenes(Solution& sol, const std::function<double(void)>& rnd01)
  {
    // Set the size of the vector collecting the chromosomes
    sol.solution.resize(getNumVars(), 0);
    initGenesImpl(sol, rnd01);
  }

  /// Public interface for evaluating a solution
  bool evalSolution(const Solution& sol, MiddleCost& cost)
  {
    return evalSolutionImpl(sol, cost);
  }

 protected:
  /// Returns the number of variables/chromosomes in the solution
  inline std::size_t getNumVars() const { return pNumVars; }

  /// Returns the variable lower bound
  inline Real getVarLb() const { return pVarLb; }

  /// Returns the variable upper bound
  inline Real getVarUb() const { return pVarUb; }

  /// Returns the initialization type
  inline InitializationType getInitializationType() const { return pInitType; }

  /// Returns the crossover type
  inline CrossoverType getCrossoverType() const { return pCrossoverType; }

  /// Returns the mutation type
  inline MutationType getMutationType() const { return pMutationStrategy; }

  virtual Solution mutateImpl(const Solution& X_base, const std::function<double(void)>&,
                              double shrink_scale)
  {
    Solution mutatedSolution;
    switch(getMutationType())
    {
      case MutationType::MT_INVERSION:
      {
        mutatedSolution.solution =
            gastrategies::inversionMutation<Real>(X_base.solution, pRndGen.get(), shrink_scale);
        break;
      }
      case MutationType::MT_SINGLE_SWAP:
      {
        mutatedSolution.solution =
            gastrategies::singleSwapMutation<Real>(X_base.solution, pRndGen.get(), shrink_scale);
        break;
      }
      case MutationType::MT_SCRAMBLE:
      {
        mutatedSolution.solution =
            gastrategies::scrambleMutation<Real>(X_base.solution, pRndGen.get(), shrink_scale);
        break;
      }
      default:
        throw std::runtime_error("mutateImpl - invalid mutation type");
    }
    return mutatedSolution;
  }

  virtual Solution crossoverImpl(const Solution& X1, const Solution& X2,
                                 const std::function<double(void)>&)
  {
    Solution child;
    switch(getCrossoverType())
    {
      case CrossoverType::CT_ORDER_1:
      {
        child.solution =
            gastrategies::order1Crossover<Real>(X1.solution, X2.solution, pRndGen.get());
        break;
      }
      case CrossoverType::CT_EDGE_RECOMBINATION:
      {
        child.solution =
            gastrategies::edgeRecombinationCrossover<Real>(X1.solution, X2.solution, pRndGen.get());
        break;
      }
      case CrossoverType::CT_PMX:
      {
        child.solution =
            gastrategies::pmxCrossover<Real>(X1.solution, X2.solution, pRndGen.get());
        break;
      }
      case CrossoverType::CT_ORDER_MULTIPLE:
      {
        child.solution =
            gastrategies::orderMultipleCrossover<Real>(X1.solution, X2.solution, pRndGen.get());
        break;
      }
      default:
        throw std::runtime_error("crossoverImpl - invalid recombination type");
    }
    return child;
  }

  virtual void initGenesImpl(Solution& sol, const std::function<double(void)>&)
  {
    switch (getInitializationType())
    {
      case InitializationType::IT_RANDOM:
      {
        gastrategies::individualRandomInit<Real>(sol.solution, getVarLb(), getVarUb(), pRndGen.get());
        break;
      }
      case InitializationType::IT_PERMUTATION:
      {
        gastrategies::individualPermutationInit<Real>(
            sol.solution, getVarLb(), getVarUb(), pRndGen.get());
        break;
      }
      default:
        throw std::runtime_error("initGenesImpl - invalid initialization type");
    }
  }

  virtual bool evalSolutionImpl(const Solution& sol, MiddleCost& cost)
  {
    if (!pObjFcnPtr)
    {
      throw std::runtime_error("StdEnvironmentImpl - dynamic library for objective function "
          "not present");
    }
    cost.objective = pObjFcnPtr->objectiveFunction(sol.solution);
    return true;
  }

 private:
  /// Number of variables in the solution
  uint32_t pNumVars;

  /// Lower bound on each variables
  Real pVarLb;

  /// Upper bound on each variable
  Real pVarUb;

  /// Initialization type
  InitializationType pInitType;

  /// Crossover strategy
  CrossoverType pCrossoverType;

  /// Mutation strategy
  MutationType pMutationStrategy;

  /// Seed to be used for pseudo-random generator.
  /// If less than zero, use timestamp
  int pSeed;

  /// Name of the dynamic library to load.
  /// The dynamic library will have the user-specified optimization function
  std::string pOptimizationDynLibName;

  /// Pointer to the objective function class
  std::shared_ptr<GAObjectiveFcn> pObjFcnPtr;

  /// Random generator
  std::unique_ptr<utilsrandom::RandomGenerator> pRndGen;
};

}  // namespace optilab
