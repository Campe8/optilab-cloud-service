//
// Copyright OptiLab 2019. All rights reserved.
//
// Strategies for GA optimizers and algorithms.
//

#pragma once

#include <algorithm>      // for std::sort
#include <cassert>
#include <functional>     // for std::function
#include <cstddef>        // for std::size_t
#include <unordered_map>
#include <unordered_set>
#include <utility>        // for std::pair
#include <vector>

#include "utilities/random_generators.hpp"

namespace optilab {

namespace gastrategies {

/// Initialize a vector "vals" with random numbers in [lb, ub]
template <class Real>
void individualRandomInit(std::vector<Real>& vals, Real lb, Real ub,
                          utilsrandom::RandomGenerator* rndGen)
{
  assert(rndGen);

  const Real span = ub - lb;
  for (auto& v : vals)
  {
    v = lb + static_cast<Real>(rndGen->randReal(0, 1)) * span;
  }
}  // individualRandomInit

/// Initialize a vector "vals" with a permutation of numbers in [lb, ub).
/// If "vals" size is greater than ub - lb + 1, it repeats the permutation
template <class Real>
void individualPermutationInit(std::vector<Real>& vals, Real lb, Real ub,
                               utilsrandom::RandomGenerator* rndGen)
{
  assert(rndGen);

  // Initialize with numbers from lb to ub
  const std::size_t vsize = vals.size();
  Real ptr = lb;
  for (int idx = 0; idx < vsize; ++idx)
  {
    vals[idx] = ptr++;
    if (ptr >= ub) ptr = lb;
  }

  // Apply Fisher–Yates algorithm
  for (int idx = 0; idx < vsize - 2; ++idx)
  {
    int index = rndGen->randInt(idx, vsize);
    std::swap(vals[idx], vals[index]);
  }
}  // randomIndividualInit

/// Perform inversion mutation:
/// [0, 1, [2, 3, 4, 5, 6], 7, 8, 9] -> [0, 1, [6, 5, 4, 3, 2], 7, 8, 9]
template <class Real>
std::vector<Real> inversionMutation(const std::vector<Real>& base,
                                    utilsrandom::RandomGenerator* rndGen,
                                    double)
{
  assert(rndGen);

  // Get the size of the swath (it can be the full array)
  const int vsize = static_cast<int>(base.size());
  const int dimSwath = rndGen->randInt(2, vsize);

  // Get start/end point of the swath
  int start = rndGen->randInt(0, vsize - dimSwath);
  int end = start + dimSwath - 1;

  // Put the values back in reverse order
  std::vector<Real> newVector = base;
  for (int idx = start; idx < start + (dimSwath / 2); ++idx)
  {
    std::swap(newVector[idx], newVector[start + dimSwath - idx - 1]);
  }

  return newVector;
}  // inversionMutation

/// Perform random slide mutation:
/// [0, 1, [2, 3, 4, 5, 6], 7, 8, 9] -> [0, [2, 3, 4, 5, 6], 1, 7, 8, 9]
/// [0, 1, [2, 3, 4, 5, 6], 7, 8, 9] -> [0, 1, 7, 8, [2, 3, 4, 5, 6], 9]
template <class Real>
std::vector<Real> randomSlideMutation(const std::vector<Real>& base,
                                      utilsrandom::RandomGenerator* rndGen,
                                      double)
{
  /// TODO
  throw std::runtime_error("randomSlideMutation - not supported");
}  // randomSlideMutation

/// Perform single swap mutation:
/// [0, 1, [2], 3, 4, 5, [6], 7, 8, 9] -> [0, 1, [6], 3, 4, 5, [2], 7, 8, 9]
template <class Real>
std::vector<Real> singleSwapMutation(const std::vector<Real>& base,
                                      utilsrandom::RandomGenerator* rndGen,
                                      double)
{
  assert(rndGen);
  const int vsize = static_cast<int>(base.size());

  int idx1 = rndGen->randInt(0, vsize);
  int idx2 = rndGen->randInt(0, vsize);

  std::vector<Real> newVector = base;
  std::swap<Real>(newVector[idx1], newVector[idx2]);

  return base;
}  // singleSwapMutation

/// Perform scramble mutation:
/// [0, 1, [2, 3, 4, 5, 6], 7, 8, 9] -> [0, 1, [3, 6, 2, 4, 5], 7, 8, 9]
template <class Real>
std::vector<Real> scrambleMutation(const std::vector<Real>& base,
                                    utilsrandom::RandomGenerator* rndGen,
                                    double)
{
  assert(rndGen);

  // Get the size of the swath (it can be the full array)
  const int vsize = static_cast<int>(base.size());
  const int dimSwath = rndGen->randInt(2, vsize);

  // Get start/end point of the swath
  int start = rndGen->randInt(0, vsize - dimSwath);
  int end = start + dimSwath - 1;

  // Put the values back in reverse order
  std::vector<Real> newVector = base;
  std::shuffle(newVector.begin() + start, newVector.begin() + end, rndGen->generator());

  return newVector;
}  // inversionMutation

/// Perform order1 crossover: a swath of consecutive alleles from parent 1 drops down,
/// and remaining values are placed in the child in the order which they appear in parent 2.
/// base1: [8, 4, 7, [3, 6, 2, 5, 1], 9, 0]
/// base2: [0, 1, 2,  3, 4, 5, 6, 7,  8, 9]
/// child: [0, 4, 7,  3, 6, 2, 5, 1,  8, 9].
/// @note this works only for permutation parents.
/// @note this is also known as one point crossover
template <class Real>
std::vector<Real> order1Crossover(const std::vector<Real>& base1, const std::vector<Real>& base2,
                                  utilsrandom::RandomGenerator* rndGen)
{
  assert(rndGen);
  assert(base1.size() == base2.size());

  // Get the size of the swath (it can be the full array)
  const int vsize = static_cast<int>(base1.size());
  if (vsize == 1)
  {
    return (rndGen->randReal() >= 0.5) ? base1 : base2;
  }

  int start = rndGen->randInt(0, vsize);
  int end;
  do
  {
    end = rndGen->randInt(0, vsize);
  }
  while (end == start);
  if (end < start) std::swap<int>(start, end);

  // Initialize child with the second parent.
  // This would reduce the effect of having the same value repeated in the same parent
  std::vector<Real> child = (rndGen->randReal() >= 0.5) ? base1 : base2;
  std::unordered_set<Real> p1Set;
  for (int idx = start; idx <= end; ++idx)
  {
    child[idx] = base1[idx];
    p1Set.insert(base1[idx]);
  }

  // Copy the remaining elements from parent 2 starting from the set of consecutive integers
  int ptr = end + 1;

  // Wrap around if needed
  if (ptr == vsize) ptr = 0;
  for (int idx = end + 1; idx < vsize; ++idx)
  {
    if (p1Set.find(base2[idx]) == p1Set.end())
    {
      child[ptr] = base2[idx];
      p1Set.insert(base2[idx]);
      ptr++;

      // If pointer is too high, wrap it around
      if (ptr == vsize) ptr = 0;
    }
  }

  for (int idx = 0; idx <= end; ++idx)
  {
    if (p1Set.find(base2[idx]) == p1Set.end())
    {
      child[ptr] = base2[idx];
      p1Set.insert(base2[idx]);
      ptr++;

      // If pointer is too high, wrap it around
      if (ptr == vsize) ptr = 0;
    }
  }

  return child;
}  // order1Crossover

/// Perform edge recombination crossover: it introduces the fewest paths possible.
/// The idea is to use as many existing edges, or node-connections, as possible
/// to generate children.
/// @note this works only for permutation parents
template <class Real>
std::vector<Real> edgeRecombinationCrossover(const std::vector<Real>& base1,
                                             const std::vector<Real>& base2,
                                             utilsrandom::RandomGenerator* rndGen)
{
  assert(rndGen);
  assert(base1.size() == base2.size());
  std::unordered_map<Real, std::unordered_set<Real>> neighborMap;

  const int vsize = static_cast<int>(base1.size());
  if (vsize == 1)
  {
    return (rndGen->randReal() >= 0.5) ? base1 : base2;
  }

  for (int idx = 0; idx < vsize; ++idx)
  {
    // Insert left and right neighbor
    int leftIdx = (idx == 0) ? (vsize - 1) : (idx - 1);
    int rightIdx = (idx == (vsize - 1)) ? 0 : (idx + 1);

    neighborMap[base1[idx]].insert(base1[leftIdx]);
    neighborMap[base1[idx]].insert(base1[rightIdx]);
    neighborMap[base2[idx]].insert(base2[leftIdx]);
    neighborMap[base2[idx]].insert(base2[rightIdx]);
  }

  // Generate child.
  // This would reduce the effect of having the same value repeated in the same parent.
  // Note also that the first node is already set from a random parent
  std::vector<Real> child = (rndGen->randReal() >= 0.5) ? base1 : base2;

  // Fill all other chromosomes of the child
  int ptr = 1;
  Real chromosome = child[0];
  while (ptr < vsize)
  {
    // Remove chromosome from neighborMap and determine which neighbor has fewest entries
    neighborMap.erase(chromosome);
    std::vector<Real> bestNeighbor;
    std::size_t neighborSize = static_cast<std::size_t>(vsize);
    for (auto& it : neighborMap)
    {
      it.second.erase(chromosome);

      if (it.second.size() < neighborSize)
      {
        // Update best neighbor
        neighborSize = it.second.size();
        bestNeighbor.clear();
        bestNeighbor.push_back(it.first);
      }
      else if (it.second.size() <= neighborSize)
      {
        // Add a potential candidate
        bestNeighbor.push_back(it.first);
      }
    }

    // Random pick from the list of potential candidate neighbors
    if (bestNeighbor.size() == 1)
    {
      child[ptr] = bestNeighbor[0];
      chromosome = bestNeighbor[0];
    }
    else
    {
      // Pick one best neighbor at random
      Real best = bestNeighbor[rndGen->randInt(0, static_cast<int>(bestNeighbor.size()))];
      child[ptr] = best;
      chromosome = best;
    }

    // Increment pointer
    ptr++;
  }

  return child;
}  // edgeRecombinationCrossover

/// Perform PMX recombination crossover: it introduces the fewest paths possible.
/// Parent 1 donates a swath of genetic material and the corresponding swath from the other
/// parent is sprinkled about in the child. Once that is done, the remaining alleles are copied
/// direct from parent 2:
/// base1: [8, 4, 7, [3, 6, 2, 5, 1], 9, 0]
/// base2: [0, 1, 2,  3, 4, 5, 6, 7,  8, 9]
/// child: [0, 7, 4,  3, 6, 2, 5, 1,  8, 9].
template <class Real>
std::vector<Real> pmxCrossover(const std::vector<Real>& base1, const std::vector<Real>& base2,
                               utilsrandom::RandomGenerator* rndGen)
{
  assert(rndGen);
  assert(base1.size() == base2.size());

  const int vsize = static_cast<int>(base1.size());
  if (vsize == 1)
  {
    return (rndGen->randReal() >= 0.5) ? base1 : base2;
  }

  // 1 - Setup the Swath
  int randswathsize = std::max<int>(2, rndGen->randInt(0, vsize));

  // "start" refers to left index of Swath and "end" refers to right index of swath
  int start = rndGen->randInt(0, vsize - randswathsize);
  int end = start + randswathsize - 1;

  // Generate child.
  // This would reduce the effect of having the same value repeated in the same parent.
  // Note also that the first node is already set from a random parent
  std::vector<Real> child = (rndGen->randReal() >= 0.5) ? base1 : base2;

  // 2 - Copy swath from parent 1 into the child
  std::unordered_set<Real> valsSwath;
  std::unordered_set<int> childPosTaken;
  for (int idx = start; idx <= end; ++idx)
  {
    child[idx] = base1[idx];
    valsSwath.insert(base1[idx]);
    childPosTaken.insert(idx);
  }

  // 3 - Figure out which values from Parent 2 swath were not put in the child from Parent 1
  std::vector<std::pair<int, Real>> p2Set;
  for (int idx = start; idx <= end; ++idx)
  {
    if (valsSwath.find(base2[idx]) == valsSwath.end())
    {
      p2Set.push_back({idx, base2[idx]});
    }
  }

  // p2Set now contains the indices and values of the P2 swath not in child
  for (const auto& p : p2Set)
  {
    // 1 - Find position (in parent 2) of value in Pair's position in child
    int position = 0;
    for (int idx = 0; idx < vsize; ++idx)
    {
      // Note p is position in swath child of a value that is not in swath of parent 2.
      // Find where (i.e., the index) in parent 2 of this value
      if (base2[idx] == child[p.first])
      {
        position = idx;
        break;
      }
    }

    // 2. If position is taken in child already, find position of item that is in its spot
    // in parent 2. Otherwise, put it there
    while (childPosTaken.find(position) != childPosTaken.end())
    {
      // A - Find position of value in this spot in the child in parent 2
      for (int idx = 0; idx < vsize; ++idx)
      {
        if (base2[idx] == child[position])
        {
          position = idx;
          break;
        }
      }
    }

    // If this is reached, we drop the Pair.value in Position
    child[position] = p.second;
    childPosTaken.insert(position);
    valsSwath.insert(p.second);
  }

  // Drop the remaining values in
  for (int idx = 0; idx < vsize; ++idx)
  {
    if (valsSwath.find(base2[idx]) == valsSwath.end())
    {
      child[idx] = base2[idx];
      valsSwath.insert(base2[idx]);
    }
  }

  return child;
}  // pmxCrossover

/// Perform multiple crossover: same as order1 crossover but with multiple swath
template <class Real>
std::vector<Real> orderMultipleCrossover(const std::vector<Real>& base1,
                                         const std::vector<Real>& base2,
                                         utilsrandom::RandomGenerator* rndGen)
{

  // Keep the number of swaths to 20% the total size of the parents onto the child
  assert(rndGen);
  assert(base1.size() == base2.size());

  const int vsize = static_cast<int>(base1.size());
  if (vsize == 1)
  {
    return (rndGen->randReal() >= 0.5) ? base1 : base2;
  }

  int numSwaths = static_cast<int>(vsize * 0.2);
  std::vector<int> swathStart(numSwaths);
  std::vector<int> swathEnd(numSwaths);


  std::unordered_set<int> swathEndPos;
  for (int idx = 0; idx < numSwaths; ++idx)
  {
    swathEnd[idx] = rndGen->randInt(1, vsize - 1);
    while (swathEndPos.find(swathEnd[idx]) != swathEndPos.end())
    {
      // Pick a new end pos
      swathEnd[idx] = rndGen->randInt(0, vsize - 1);
    }

    // Store this position as already picked
    swathEndPos.insert(swathEnd[idx]);
  }

  // Sort the end positions
  std::sort(swathEnd.begin(), swathEnd.end(), std::greater<int>());

  // Start from the far right end point and determine the start point
  for (int idx = 0; idx < numSwaths; ++idx)
  {
    int randomLen;
    if (idx == numSwaths - 1)
    {
      randomLen = rndGen->randInt(1, std::max<int>(1, swathEnd[idx] + 1));
    }
    else
    {
      randomLen = rndGen->randInt(1, std::max<int>(1, (swathEnd[idx] - swathEnd[idx+1])));
    }
    swathStart[idx] = std::max<int>(0, swathEnd[idx] - randomLen);
  }

  // Copy swaths from parent 1 and remaining from parent 2
  std::vector<Real> child = base2;
  std::unordered_set<Real> takenVal;
  std::unordered_set<Real> takenIdx;
  for (int idx = 0; idx < numSwaths; ++idx)
  {
    int s = swathStart[idx];
    int e = swathEnd[idx];
    for (; s <= e; ++s)
    {
      child[s] = base1[s];
      takenVal.insert(base1[s]);
      takenIdx.insert(s);
    }
  }

  int ptr = 0;
  for (int idx = 0; idx < vsize; ++idx)
  {
    // Find the first next pointer to fill in the child
    while((takenIdx.find(idx) != takenIdx.end()) &&  ptr < vsize) ptr++;

    int newValIdx = idx;
    Real valToSet = base2[newValIdx];
    while ((takenVal.find(valToSet) != takenVal.end()) && newValIdx < vsize)
    {
      newValIdx++;
      valToSet = base2[newValIdx];
    }

    // Insert the value as taken
    takenVal.insert(valToSet);
    takenIdx.insert(ptr);
    child[ptr] = valToSet;
    ptr++;
  }

  return child;
}  // orderMultipleCrossover

}  // namespace gastrategies

}  // namespace optilab
