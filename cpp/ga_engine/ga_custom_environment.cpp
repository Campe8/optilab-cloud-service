// Base class
#include "ga_engine/ga_custom_environment.hpp"

#include <algorithm>  // for std::min
#include <cstdio>     // for remove
#include <limits>
#include <stdexcept>  // for std::runtime_error

#include <spdlog/spdlog.h>

#include "ga_engine/ga_constants.hpp"
#include "ga_engine/ga_utilities.hpp"
#include "model/model_constants.hpp"
#include "utilities/file_sys.hpp"
#include "utilities/random_generators.hpp"

namespace {

#ifdef __APPLE__
const char kCompilerName[] = "g++";
#else
const char kCompilerName[] = "g++";
#endif


constexpr int kDynLibNameLen = 8;
const char kDynLibExt[] = ".so";
const char kIncludePath[] = "-Iinclude/optilab";
const char kOptiLabIncludePath[] = "include/optilab";
const char kTempFileExt[] = "cpp";

}  // namespace

namespace optilab {

GACustomEnvironment::GACustomEnvironment()
: GAEnvironment(GAEnvironmentType::GA_ENV_CUSTOM),
  pEnvironmentImplPtr(nullptr)
{
}

GACustomEnvironment::~GACustomEnvironment()
{
  // Remove the library
  remove(pDynLibName.c_str());
}

bool GACustomEnvironment::isa(const GAEnvironment* env)
{
  return env &&
      env->getGAEnvironmentType() == GAEnvironment::GAEnvironmentType::GA_ENV_CUSTOM;
}

GACustomEnvironment* GACustomEnvironment::cast(GAEnvironment* env)
{
  if (!isa(env)) return nullptr;
  return static_cast<GACustomEnvironment*>(env);
}

const GACustomEnvironment* GACustomEnvironment::cast(const GAEnvironment* env)
{
  if (!isa(env)) return nullptr;
  return static_cast<const GACustomEnvironment*>(env);
}

void GACustomEnvironment::loadEnvironmentImpl(const toolbox::GeneticModelProto& model)
{
  if (!model.has_custom_environment())
  {
    const std::string errMsg = "GACustomEnvironment - loadEnvironmentImpl: "
            "the given environment is non custom";
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  const auto& customEnvPath = model.custom_environment();

  std::vector<std::string> envPath;
  for (int idx{0}; idx < customEnvPath.environment_path_size(); ++idx)
  {
    envPath.push_back(customEnvPath.environment_path(idx));
  }

  // Chromosome descriptor
  const auto& chromo = getChromosome();
  pDynLibName = compileObjectiveIntoDynLib(envPath);

  // Load the environment
  if (getChromosomeBaseType() == Chromosome::ChromosomeType::CT_DOUBLE)
  {
    pEnvironmentImplPtr = std::make_shared<CustomEnvironmentImpl<double>>(
            chromo.dims,
            chromo.lb,
            chromo.ub,
            pseudoRandomSeed(),
            pDynLibName);
  }
  else
  {
    pEnvironmentImplPtr = std::make_shared<CustomEnvironmentImpl<int>>(
            chromo.dims,
            static_cast<int>(chromo.lb),
            static_cast<int>(chromo.ub),
            pseudoRandomSeed(),
            pDynLibName);
  }
}  // loadEnvironmentImpl

std::string GACustomEnvironment::compileObjectiveIntoDynLib(const std::vector<std::string>& fcnList)
{
  // Check on the list of files to compile
  if (fcnList.empty())
  {
    throw std::runtime_error("GACustomEnvironment - compileObjectiveIntoDynLib: "
        "empty file list");
  }
  // Compile the file and create a dynamic library with random name
  const std::string libName = utilsrandom::buildRandomAlphaNumString(kDynLibNameLen) +
      std::string(kDynLibExt);

  // Build the dynamic library
  const std::string path1 = utilsfilesys::getFullPathToLocation(
          { utilsfilesys::getPathToProgramLocation(), "..", std::string(kOptiLabIncludePath) });
  const std::string path2 = utilsfilesys::getFullPathToLocation(
          { utilsfilesys::getPathToProgramLocation(), std::string(kOptiLabIncludePath) });
  std::string incPath = "-I" + path1;
  incPath += " -I" + path2;
  std::string buildOutput = utilsfilesys::compileAndCreateDynamicLib(
          fcnList,
          libName,
          std::string(kCompilerName),
          incPath /*std::string(kIncludePath)*/);

  // Check the output of the built
  if (!buildOutput.empty())
  {
    const std::string errMsg = "GAStdEnvironment - error building the dynamic library: " +
        buildOutput;
    spdlog::error(errMsg);
    throw std::runtime_error(errMsg);
  }

  // Return the library name
  return libName;
}  // compileObjectiveIntoDynLib

}  // namespace optilab
