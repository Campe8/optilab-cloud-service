# OptiLab-cloud-service

OptiLab cloud service repository.

### How do I get set up? ###

* Read this tutorial to know how to use git and Bitbucket: https://www.atlassian.com/git/tutorials/setting-up-a-repository

* Clone repository every time you want to start working on new features (and delete it after the push): 
git clone https://<your_bitbucket_username>@bitbucket.org/Campe8/optilab.git

### How do I run it? ###

The following instructions are valid for mac architectures. Libraries are compiled and tested with iMac
running macOS Mojave v 10.14.2:

1. Copy all the script files into the main folder (i.e., outside the folder script)
1. Run all the "build" scripts first
1. Run all the "compile" scripts
1. Open five windows: (1) for the broker, (2) for the client, (3) for the service, (4) for the back-end broker, and (5) for the back-end workers
1. Run the broker, e.g., ./optilab_broker
1. Run the service, e.g., ./optilab_service
1. Run the back-end broker, e.g., ./optilab_back_end_broker
1. Run the back-end service, e.g., ./optilab_back_end_service
1. ~~Run the simulated client with the chosen input (CP, MIP, COMB), e.g., ./optilab_client -f COMB~~
1. Run the cloud front-end, e.g., ./optilab_front_end

Client (simulated) input:

1. CP: a simple CP model
1. MIP: a simple MIP model
1. COMB: a combinator block runnnig in parallel a CP and a MIP model

If the terminal replies with:

"dyld: Library not loaded: <some_path>/optilab-cloud-service/zeromq/lib/libzmq.5.dylib"

load the dynamic libraries by running the following commands:

1. export DYLD_LIBRARY_PATH=$DYLD_LIBRARY_PATH:<some_path>/optilab-cloud-service/lib/mac/or-tools
1. export DYLD_LIBRARY_PATH=$DYLD_LIBRARY_PATH:<some_path>/optilab-cloud-service/lib/mac/protobuf
1. export DYLD_LIBRARY_PATH=$DYLD_LIBRARY_PATH:<some_path>/optilab-cloud-service/lib/mac/zeromq

Using the REST API:

1. Same as above but run the front-end with '--rest' | '-r' option, i.e., ./optilab_front_end -r
1. Create an optimizer: curl -X GET --header 'Accept: application/json' 'http://127.0.0.1:8080/optimizer'
1. Run a model: curl -X DELETE --header 'Accept: application/json' 'http://127.0.0.1:8080/optimizer/5PN2qmWqBl'
1. Delete an optimizer: curl -X POST --header 'Content-Type: application/json' --header 'Accept */*' -d "@data.json" 'http://127.0.0.1:8080/model/5PN2qmWqBl'

where "data.json" contains the JSON model to be solved and "5PN2qmWqBl" is the random identifier returned by the server when creating the optimizer.

TODO: use CMAKE

TODO: create a single file/script to run all the above

### How do I contribute? ###

Have a look at the Wiki page where there is a list of todo(s) before the new release of OptiLab.
For more information about OptiLab, check it out at www.optilabtech.com.

To work on a feature without messing up with the remote master branch do the following from the master branch:

1. git checkout -b my_feature
1. work on my feature...
1. git commit (first add/stage the files you want to push)
1. git checkout master
1. git pull
1. git checkout my_feature
1. git rebase master
1. git checkout master
1. git merge my_feature
1. git push origin master

A good tool to have that would help you with git (if you are working on mac or windows) is "fork".
See here https://git-fork.com/ for more information.

### Contribution guidelines ###

* Writing tests
Every new module, component or file added must be UNIT TESTED and 
the code must be covered.
Moreover, it is advisable to check the complexity complexity of the 
code which MUST be under 10.
Check http://oclint.org/ for a cyclomatic complexity tool.

* Code review
Before submitting any code send a code review request to the owners of
this repository.

### Other guidelines ###

* Use C++11 NOT C style programming.

* Follow the current syntax if possible, i.e., CamelCase for classes, arguments name for functions with 'a' as a prefix, private members with 'p' as a prefix, etc.

* Exported classes, i.e., classes with API that should be linked and used by other components must have the EXPORT macro and the interface header should be places in the export folder. Moreover, the file name should be present in the correspondend headers.mk file.

### Who do I talk to? ###

* Repo owners
