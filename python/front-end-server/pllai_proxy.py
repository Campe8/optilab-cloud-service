import sys
import os
import logging
from pllai_proxy_impl import PllAIProxyImpl


class PllAIProxy:
    """Proxy class for the Parallel AI server"""
    broker_port = None
    proxy_id = ""
    frontendRunnerImpl = None
    web_socket = None

    def __init__(self, proxy_id, broker_port, web_socket):
        self.broker_port = broker_port
        self.proxy_id = proxy_id
        self.web_socket = web_socket
        self.proxy_impl = PllAIProxyImpl(self.proxy_id, self.broker_port, self.web_socket)
        logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.WARNING)

    def get_id(self):
        return self.proxy_id

    def init_proxy(self):
        # This proxy implementation will start running,
        # i.e., it will start listening on incoming connections
        self.proxy_impl.run()

    def turn_down(self):
        self.proxy_impl.shut_down()

    def on_proto_message(self, msg):
        """Forward protobuf message to the proxy"""
        res = self.proxy_impl.route_proto_message_to_broker(msg)
        if not res:
            err_msg = "PllAIProxy - on_proto_message: error while sending the message to the server"
            logging.error(err_msg)
            self.web_socket.write_message(err_msg)
