from proto import optilab_pb2


def build_wait_request(engine_id, package_type, wait_msec, framework_type):
    wait_msg = optilab_pb2.OptimizerWaitReq()
    wait_msg.waitTimeout = wait_msec
    req = optilab_pb2.OptilabRequestMessage()
    req.details.Pack(wait_msg)
    req.type = optilab_pb2.OptilabRequestMessage.EngineEvent
    req.pkgType = package_type
    req.requestType = optilab_pb2.POST
    req.frameworkType = framework_type
    req.messageInfo = engine_id
    return req


def build_create_engine_request(engine_id, class_type, package_type, framework_type, opt_req):
    if opt_req is None:
        opt_req = optilab_pb2.CreateOptimizerReq()
        opt_req.engineType = class_type
    req = optilab_pb2.OptilabRequestMessage()
    req.details.Pack(opt_req)
    req.type = optilab_pb2.OptilabRequestMessage.EngineEvent
    req.requestType = optilab_pb2.POST
    req.pkgType = package_type
    req.frameworkType = framework_type
    req.messageInfo = engine_id
    return req


def build_load_model_request(model_id, package_type, framework_type, proto_model):
    req = optilab_pb2.OptilabRequestMessage()
    req.details.Pack(proto_model)
    req.type = optilab_pb2.OptilabRequestMessage.EngineEvent
    req.requestType = optilab_pb2.POST
    req.pkgType = package_type
    req.frameworkType = framework_type
    req.messageInfo = model_id
    return req


def build_run_model_request(model_id, package_type, framework_type):
    opt_req = optilab_pb2.RunModelReq()
    req = optilab_pb2.OptilabRequestMessage()
    req.details.Pack(opt_req)
    req.type = optilab_pb2.OptilabRequestMessage.EngineEvent
    req.requestType = optilab_pb2.PUT
    req.pkgType = package_type
    req.frameworkType = framework_type
    req.messageInfo = model_id
    return req


def build_solutions_request(model_id, package_type, num_sol, framework_type):
    opt_req = optilab_pb2.ModelSolutionsReq()
    opt_req.numSolutions = num_sol
    req = optilab_pb2.OptilabRequestMessage()
    req.details.Pack(opt_req)
    req.type = optilab_pb2.OptilabRequestMessage.EngineEvent
    req.requestType = optilab_pb2.GET
    req.pkgType = package_type
    req.frameworkType = framework_type
    req.messageInfo = model_id
    return req


def build_delete_engine_request(model_id, package_type, framework_type):
    opt_req = optilab_pb2.CreateOptimizerReq()
    req = optilab_pb2.OptilabRequestMessage()
    req.details.Pack(opt_req)
    req.type = optilab_pb2.OptilabRequestMessage.EngineEvent
    req.requestType = optilab_pb2.DELETE
    req.pkgType = package_type
    req.frameworkType = framework_type
    req.messageInfo = model_id
    return req
