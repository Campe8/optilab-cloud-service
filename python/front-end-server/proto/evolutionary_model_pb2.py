# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: evolutionary_model.proto

from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


from google.protobuf import any_pb2 as google_dot_protobuf_dot_any__pb2
import optimizer_defs_pb2 as optimizer__defs__pb2


DESCRIPTOR = _descriptor.FileDescriptor(
  name='evolutionary_model.proto',
  package='optilab.toolbox',
  syntax='proto3',
  serialized_options=None,
  create_key=_descriptor._internal_create_key,
  serialized_pb=b'\n\x18\x65volutionary_model.proto\x12\x0foptilab.toolbox\x1a\x19google/protobuf/any.proto\x1a\x14optimizer_defs.proto\"\xdf\x01\n\x1e\x43reateEvolutionaryOptimizerReq\x12h\n\x18\x65volutionary_engine_type\x18\x01 \x01(\x0e\x32\x46.optilab.toolbox.CreateEvolutionaryOptimizerReq.EvolutionaryEngineType\"S\n\x16\x45volutionaryEngineType\x12\x15\n\x11GENETIC_ALGORITHM\x10\x00\x12\x12\n\x0ePARTICLE_SWARM\x10\x01\x12\x0e\n\nANT_COLONY\x10\x02\"\xd7\x01\n\x0f\x43hromosomeProto\x12\n\n\x02id\x18\x01 \x01(\t\x12\x12\n\ndimensions\x18\x02 \x03(\r\x12\x13\n\x0blower_bound\x18\x03 \x01(\x01\x12\x13\n\x0bupper_bound\x18\x04 \x01(\x01\x12=\n\x04type\x18\x05 \x01(\x0e\x32/.optilab.toolbox.ChromosomeProto.ChromosomeType\";\n\x0e\x43hromosomeType\x12\x12\n\x0e\x43HROMOSOME_INT\x10\x00\x12\x15\n\x11\x43HROMOSOME_DOUBLE\x10\x01\"\xa0\x04\n\x1fStandardGeneticEnvironmentProto\x12Q\n\tcrossover\x18\n \x01(\x0e\x32>.optilab.toolbox.StandardGeneticEnvironmentProto.CrossoverType\x12O\n\x08mutation\x18\x0b \x01(\x0e\x32=.optilab.toolbox.StandardGeneticEnvironmentProto.MutationType\x12W\n\x0einitialization\x18\x0c \x01(\x0e\x32?.optilab.toolbox.StandardGeneticEnvironmentProto.Initialization\"r\n\rCrossoverType\x12\x0b\n\x07ORDER_1\x10\x00\x12\x16\n\x12\x45\x44GE_RECOMBINATION\x10\x01\x12\x07\n\x03PMX\x10\x02\x12\x12\n\x0eORDER_MULTIPLE\x10\x03\x12\t\n\x05\x43YCLE\x10\x04\x12\x14\n\x10\x44IRECT_INSERTION\x10\x05\"]\n\x0cMutationType\x12\x10\n\x0cRANDOM_SLIDE\x10\x00\x12\r\n\tINVERSION\x10\x01\x12\r\n\tINSERTION\x10\x02\x12\x0f\n\x0bSINGLE_SWAP\x10\x03\x12\x0c\n\x08SCRAMBLE\x10\x04\"-\n\x0eInitialization\x12\n\n\x06RANDOM\x10\x00\x12\x0f\n\x0bPERMUTATION\x10\x01\"9\n\x1d\x43ustomGeneticEnvironmentProto\x12\x18\n\x10\x65nvironment_path\x18\x01 \x03(\t\"\xa0\x04\n\x11GeneticModelProto\x12\x18\n\x10\x65nvironment_name\x18\x01 \x01(\t\x12\x13\n\x0bmultithread\x18\x02 \x01(\x08\x12\x1d\n\x15parents_in_tournament\x18\r \x01(\x05\x12\x17\n\x0fpopulation_size\x18\x03 \x01(\r\x12\x17\n\x0fnum_generations\x18\x04 \x01(\r\x12\x15\n\rmutation_rate\x18\x05 \x01(\x01\x12\x1a\n\x12\x63rossover_fraction\x18\x06 \x01(\x01\x12\x13\n\x0btimeout_sec\x18\x07 \x01(\r\x12\x12\n\nstall_best\x18\x08 \x01(\x01\x12\x15\n\rstall_max_gen\x18\t \x01(\r\x12\x34\n\nchromosome\x18\n \x01(\x0b\x32 .optilab.toolbox.ChromosomeProto\x12\x13\n\x0brandom_seed\x18\x0b \x01(\x05\x12L\n\x12\x63ustom_environment\x18\x14 \x01(\x0b\x32..optilab.toolbox.CustomGeneticEnvironmentProtoH\x00\x12P\n\x14standard_environment\x18\x15 \x01(\x0b\x32\x30.optilab.toolbox.StandardGeneticEnvironmentProtoH\x00\x12\x1f\n\x17objective_function_path\x18\x0c \x01(\tB\x0c\n\nenviroment\"p\n\x16\x45volutionaryModelProto\x12\x10\n\x08model_id\x18\x01 \x01(\t\x12;\n\rgenetic_model\x18\n \x01(\x0b\x32\".optilab.toolbox.GeneticModelProtoH\x00\x42\x07\n\x05model\"V\n\x1dGeneticAlgorithmSolutionProto\x12\x1c\n\x10\x63hromosome_value\x18\x01 \x03(\x01\x42\x02\x10\x01\x12\x17\n\x0fobjective_value\x18\x02 \x01(\x01\"\xd0\x01\n\x19\x45volutionarySolutionProto\x12=\n\x06status\x18\x01 \x01(\x0e\x32-.optilab.toolbox.OptimizerSolutionStatusProto\x12\x12\n\nstatus_str\x18\x02 \x01(\t\x12T\n\x1agenetic_algorithm_solution\x18\x03 \x01(\x0b\x32..optilab.toolbox.GeneticAlgorithmSolutionProtoH\x00\x42\n\n\x08solutionb\x06proto3'
  ,
  dependencies=[google_dot_protobuf_dot_any__pb2.DESCRIPTOR,optimizer__defs__pb2.DESCRIPTOR,])



_CREATEEVOLUTIONARYOPTIMIZERREQ_EVOLUTIONARYENGINETYPE = _descriptor.EnumDescriptor(
  name='EvolutionaryEngineType',
  full_name='optilab.toolbox.CreateEvolutionaryOptimizerReq.EvolutionaryEngineType',
  filename=None,
  file=DESCRIPTOR,
  create_key=_descriptor._internal_create_key,
  values=[
    _descriptor.EnumValueDescriptor(
      name='GENETIC_ALGORITHM', index=0, number=0,
      serialized_options=None,
      type=None,
      create_key=_descriptor._internal_create_key),
    _descriptor.EnumValueDescriptor(
      name='PARTICLE_SWARM', index=1, number=1,
      serialized_options=None,
      type=None,
      create_key=_descriptor._internal_create_key),
    _descriptor.EnumValueDescriptor(
      name='ANT_COLONY', index=2, number=2,
      serialized_options=None,
      type=None,
      create_key=_descriptor._internal_create_key),
  ],
  containing_type=None,
  serialized_options=None,
  serialized_start=235,
  serialized_end=318,
)
_sym_db.RegisterEnumDescriptor(_CREATEEVOLUTIONARYOPTIMIZERREQ_EVOLUTIONARYENGINETYPE)

_CHROMOSOMEPROTO_CHROMOSOMETYPE = _descriptor.EnumDescriptor(
  name='ChromosomeType',
  full_name='optilab.toolbox.ChromosomeProto.ChromosomeType',
  filename=None,
  file=DESCRIPTOR,
  create_key=_descriptor._internal_create_key,
  values=[
    _descriptor.EnumValueDescriptor(
      name='CHROMOSOME_INT', index=0, number=0,
      serialized_options=None,
      type=None,
      create_key=_descriptor._internal_create_key),
    _descriptor.EnumValueDescriptor(
      name='CHROMOSOME_DOUBLE', index=1, number=1,
      serialized_options=None,
      type=None,
      create_key=_descriptor._internal_create_key),
  ],
  containing_type=None,
  serialized_options=None,
  serialized_start=477,
  serialized_end=536,
)
_sym_db.RegisterEnumDescriptor(_CHROMOSOMEPROTO_CHROMOSOMETYPE)

_STANDARDGENETICENVIRONMENTPROTO_CROSSOVERTYPE = _descriptor.EnumDescriptor(
  name='CrossoverType',
  full_name='optilab.toolbox.StandardGeneticEnvironmentProto.CrossoverType',
  filename=None,
  file=DESCRIPTOR,
  create_key=_descriptor._internal_create_key,
  values=[
    _descriptor.EnumValueDescriptor(
      name='ORDER_1', index=0, number=0,
      serialized_options=None,
      type=None,
      create_key=_descriptor._internal_create_key),
    _descriptor.EnumValueDescriptor(
      name='EDGE_RECOMBINATION', index=1, number=1,
      serialized_options=None,
      type=None,
      create_key=_descriptor._internal_create_key),
    _descriptor.EnumValueDescriptor(
      name='PMX', index=2, number=2,
      serialized_options=None,
      type=None,
      create_key=_descriptor._internal_create_key),
    _descriptor.EnumValueDescriptor(
      name='ORDER_MULTIPLE', index=3, number=3,
      serialized_options=None,
      type=None,
      create_key=_descriptor._internal_create_key),
    _descriptor.EnumValueDescriptor(
      name='CYCLE', index=4, number=4,
      serialized_options=None,
      type=None,
      create_key=_descriptor._internal_create_key),
    _descriptor.EnumValueDescriptor(
      name='DIRECT_INSERTION', index=5, number=5,
      serialized_options=None,
      type=None,
      create_key=_descriptor._internal_create_key),
  ],
  containing_type=None,
  serialized_options=None,
  serialized_start=827,
  serialized_end=941,
)
_sym_db.RegisterEnumDescriptor(_STANDARDGENETICENVIRONMENTPROTO_CROSSOVERTYPE)

_STANDARDGENETICENVIRONMENTPROTO_MUTATIONTYPE = _descriptor.EnumDescriptor(
  name='MutationType',
  full_name='optilab.toolbox.StandardGeneticEnvironmentProto.MutationType',
  filename=None,
  file=DESCRIPTOR,
  create_key=_descriptor._internal_create_key,
  values=[
    _descriptor.EnumValueDescriptor(
      name='RANDOM_SLIDE', index=0, number=0,
      serialized_options=None,
      type=None,
      create_key=_descriptor._internal_create_key),
    _descriptor.EnumValueDescriptor(
      name='INVERSION', index=1, number=1,
      serialized_options=None,
      type=None,
      create_key=_descriptor._internal_create_key),
    _descriptor.EnumValueDescriptor(
      name='INSERTION', index=2, number=2,
      serialized_options=None,
      type=None,
      create_key=_descriptor._internal_create_key),
    _descriptor.EnumValueDescriptor(
      name='SINGLE_SWAP', index=3, number=3,
      serialized_options=None,
      type=None,
      create_key=_descriptor._internal_create_key),
    _descriptor.EnumValueDescriptor(
      name='SCRAMBLE', index=4, number=4,
      serialized_options=None,
      type=None,
      create_key=_descriptor._internal_create_key),
  ],
  containing_type=None,
  serialized_options=None,
  serialized_start=943,
  serialized_end=1036,
)
_sym_db.RegisterEnumDescriptor(_STANDARDGENETICENVIRONMENTPROTO_MUTATIONTYPE)

_STANDARDGENETICENVIRONMENTPROTO_INITIALIZATION = _descriptor.EnumDescriptor(
  name='Initialization',
  full_name='optilab.toolbox.StandardGeneticEnvironmentProto.Initialization',
  filename=None,
  file=DESCRIPTOR,
  create_key=_descriptor._internal_create_key,
  values=[
    _descriptor.EnumValueDescriptor(
      name='RANDOM', index=0, number=0,
      serialized_options=None,
      type=None,
      create_key=_descriptor._internal_create_key),
    _descriptor.EnumValueDescriptor(
      name='PERMUTATION', index=1, number=1,
      serialized_options=None,
      type=None,
      create_key=_descriptor._internal_create_key),
  ],
  containing_type=None,
  serialized_options=None,
  serialized_start=1038,
  serialized_end=1083,
)
_sym_db.RegisterEnumDescriptor(_STANDARDGENETICENVIRONMENTPROTO_INITIALIZATION)


_CREATEEVOLUTIONARYOPTIMIZERREQ = _descriptor.Descriptor(
  name='CreateEvolutionaryOptimizerReq',
  full_name='optilab.toolbox.CreateEvolutionaryOptimizerReq',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='evolutionary_engine_type', full_name='optilab.toolbox.CreateEvolutionaryOptimizerReq.evolutionary_engine_type', index=0,
      number=1, type=14, cpp_type=8, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
    _CREATEEVOLUTIONARYOPTIMIZERREQ_EVOLUTIONARYENGINETYPE,
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=95,
  serialized_end=318,
)


_CHROMOSOMEPROTO = _descriptor.Descriptor(
  name='ChromosomeProto',
  full_name='optilab.toolbox.ChromosomeProto',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='id', full_name='optilab.toolbox.ChromosomeProto.id', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='dimensions', full_name='optilab.toolbox.ChromosomeProto.dimensions', index=1,
      number=2, type=13, cpp_type=3, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='lower_bound', full_name='optilab.toolbox.ChromosomeProto.lower_bound', index=2,
      number=3, type=1, cpp_type=5, label=1,
      has_default_value=False, default_value=float(0),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='upper_bound', full_name='optilab.toolbox.ChromosomeProto.upper_bound', index=3,
      number=4, type=1, cpp_type=5, label=1,
      has_default_value=False, default_value=float(0),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='type', full_name='optilab.toolbox.ChromosomeProto.type', index=4,
      number=5, type=14, cpp_type=8, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
    _CHROMOSOMEPROTO_CHROMOSOMETYPE,
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=321,
  serialized_end=536,
)


_STANDARDGENETICENVIRONMENTPROTO = _descriptor.Descriptor(
  name='StandardGeneticEnvironmentProto',
  full_name='optilab.toolbox.StandardGeneticEnvironmentProto',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='crossover', full_name='optilab.toolbox.StandardGeneticEnvironmentProto.crossover', index=0,
      number=10, type=14, cpp_type=8, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='mutation', full_name='optilab.toolbox.StandardGeneticEnvironmentProto.mutation', index=1,
      number=11, type=14, cpp_type=8, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='initialization', full_name='optilab.toolbox.StandardGeneticEnvironmentProto.initialization', index=2,
      number=12, type=14, cpp_type=8, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
    _STANDARDGENETICENVIRONMENTPROTO_CROSSOVERTYPE,
    _STANDARDGENETICENVIRONMENTPROTO_MUTATIONTYPE,
    _STANDARDGENETICENVIRONMENTPROTO_INITIALIZATION,
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=539,
  serialized_end=1083,
)


_CUSTOMGENETICENVIRONMENTPROTO = _descriptor.Descriptor(
  name='CustomGeneticEnvironmentProto',
  full_name='optilab.toolbox.CustomGeneticEnvironmentProto',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='environment_path', full_name='optilab.toolbox.CustomGeneticEnvironmentProto.environment_path', index=0,
      number=1, type=9, cpp_type=9, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=1085,
  serialized_end=1142,
)


_GENETICMODELPROTO = _descriptor.Descriptor(
  name='GeneticModelProto',
  full_name='optilab.toolbox.GeneticModelProto',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='environment_name', full_name='optilab.toolbox.GeneticModelProto.environment_name', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='multithread', full_name='optilab.toolbox.GeneticModelProto.multithread', index=1,
      number=2, type=8, cpp_type=7, label=1,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='parents_in_tournament', full_name='optilab.toolbox.GeneticModelProto.parents_in_tournament', index=2,
      number=13, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='population_size', full_name='optilab.toolbox.GeneticModelProto.population_size', index=3,
      number=3, type=13, cpp_type=3, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='num_generations', full_name='optilab.toolbox.GeneticModelProto.num_generations', index=4,
      number=4, type=13, cpp_type=3, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='mutation_rate', full_name='optilab.toolbox.GeneticModelProto.mutation_rate', index=5,
      number=5, type=1, cpp_type=5, label=1,
      has_default_value=False, default_value=float(0),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='crossover_fraction', full_name='optilab.toolbox.GeneticModelProto.crossover_fraction', index=6,
      number=6, type=1, cpp_type=5, label=1,
      has_default_value=False, default_value=float(0),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='timeout_sec', full_name='optilab.toolbox.GeneticModelProto.timeout_sec', index=7,
      number=7, type=13, cpp_type=3, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='stall_best', full_name='optilab.toolbox.GeneticModelProto.stall_best', index=8,
      number=8, type=1, cpp_type=5, label=1,
      has_default_value=False, default_value=float(0),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='stall_max_gen', full_name='optilab.toolbox.GeneticModelProto.stall_max_gen', index=9,
      number=9, type=13, cpp_type=3, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='chromosome', full_name='optilab.toolbox.GeneticModelProto.chromosome', index=10,
      number=10, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='random_seed', full_name='optilab.toolbox.GeneticModelProto.random_seed', index=11,
      number=11, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='custom_environment', full_name='optilab.toolbox.GeneticModelProto.custom_environment', index=12,
      number=20, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='standard_environment', full_name='optilab.toolbox.GeneticModelProto.standard_environment', index=13,
      number=21, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='objective_function_path', full_name='optilab.toolbox.GeneticModelProto.objective_function_path', index=14,
      number=12, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
    _descriptor.OneofDescriptor(
      name='enviroment', full_name='optilab.toolbox.GeneticModelProto.enviroment',
      index=0, containing_type=None,
      create_key=_descriptor._internal_create_key,
    fields=[]),
  ],
  serialized_start=1145,
  serialized_end=1689,
)


_EVOLUTIONARYMODELPROTO = _descriptor.Descriptor(
  name='EvolutionaryModelProto',
  full_name='optilab.toolbox.EvolutionaryModelProto',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='model_id', full_name='optilab.toolbox.EvolutionaryModelProto.model_id', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='genetic_model', full_name='optilab.toolbox.EvolutionaryModelProto.genetic_model', index=1,
      number=10, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
    _descriptor.OneofDescriptor(
      name='model', full_name='optilab.toolbox.EvolutionaryModelProto.model',
      index=0, containing_type=None,
      create_key=_descriptor._internal_create_key,
    fields=[]),
  ],
  serialized_start=1691,
  serialized_end=1803,
)


_GENETICALGORITHMSOLUTIONPROTO = _descriptor.Descriptor(
  name='GeneticAlgorithmSolutionProto',
  full_name='optilab.toolbox.GeneticAlgorithmSolutionProto',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='chromosome_value', full_name='optilab.toolbox.GeneticAlgorithmSolutionProto.chromosome_value', index=0,
      number=1, type=1, cpp_type=5, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=b'\020\001', file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='objective_value', full_name='optilab.toolbox.GeneticAlgorithmSolutionProto.objective_value', index=1,
      number=2, type=1, cpp_type=5, label=1,
      has_default_value=False, default_value=float(0),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=1805,
  serialized_end=1891,
)


_EVOLUTIONARYSOLUTIONPROTO = _descriptor.Descriptor(
  name='EvolutionarySolutionProto',
  full_name='optilab.toolbox.EvolutionarySolutionProto',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='status', full_name='optilab.toolbox.EvolutionarySolutionProto.status', index=0,
      number=1, type=14, cpp_type=8, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='status_str', full_name='optilab.toolbox.EvolutionarySolutionProto.status_str', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='genetic_algorithm_solution', full_name='optilab.toolbox.EvolutionarySolutionProto.genetic_algorithm_solution', index=2,
      number=3, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
    _descriptor.OneofDescriptor(
      name='solution', full_name='optilab.toolbox.EvolutionarySolutionProto.solution',
      index=0, containing_type=None,
      create_key=_descriptor._internal_create_key,
    fields=[]),
  ],
  serialized_start=1894,
  serialized_end=2102,
)

_CREATEEVOLUTIONARYOPTIMIZERREQ.fields_by_name['evolutionary_engine_type'].enum_type = _CREATEEVOLUTIONARYOPTIMIZERREQ_EVOLUTIONARYENGINETYPE
_CREATEEVOLUTIONARYOPTIMIZERREQ_EVOLUTIONARYENGINETYPE.containing_type = _CREATEEVOLUTIONARYOPTIMIZERREQ
_CHROMOSOMEPROTO.fields_by_name['type'].enum_type = _CHROMOSOMEPROTO_CHROMOSOMETYPE
_CHROMOSOMEPROTO_CHROMOSOMETYPE.containing_type = _CHROMOSOMEPROTO
_STANDARDGENETICENVIRONMENTPROTO.fields_by_name['crossover'].enum_type = _STANDARDGENETICENVIRONMENTPROTO_CROSSOVERTYPE
_STANDARDGENETICENVIRONMENTPROTO.fields_by_name['mutation'].enum_type = _STANDARDGENETICENVIRONMENTPROTO_MUTATIONTYPE
_STANDARDGENETICENVIRONMENTPROTO.fields_by_name['initialization'].enum_type = _STANDARDGENETICENVIRONMENTPROTO_INITIALIZATION
_STANDARDGENETICENVIRONMENTPROTO_CROSSOVERTYPE.containing_type = _STANDARDGENETICENVIRONMENTPROTO
_STANDARDGENETICENVIRONMENTPROTO_MUTATIONTYPE.containing_type = _STANDARDGENETICENVIRONMENTPROTO
_STANDARDGENETICENVIRONMENTPROTO_INITIALIZATION.containing_type = _STANDARDGENETICENVIRONMENTPROTO
_GENETICMODELPROTO.fields_by_name['chromosome'].message_type = _CHROMOSOMEPROTO
_GENETICMODELPROTO.fields_by_name['custom_environment'].message_type = _CUSTOMGENETICENVIRONMENTPROTO
_GENETICMODELPROTO.fields_by_name['standard_environment'].message_type = _STANDARDGENETICENVIRONMENTPROTO
_GENETICMODELPROTO.oneofs_by_name['enviroment'].fields.append(
  _GENETICMODELPROTO.fields_by_name['custom_environment'])
_GENETICMODELPROTO.fields_by_name['custom_environment'].containing_oneof = _GENETICMODELPROTO.oneofs_by_name['enviroment']
_GENETICMODELPROTO.oneofs_by_name['enviroment'].fields.append(
  _GENETICMODELPROTO.fields_by_name['standard_environment'])
_GENETICMODELPROTO.fields_by_name['standard_environment'].containing_oneof = _GENETICMODELPROTO.oneofs_by_name['enviroment']
_EVOLUTIONARYMODELPROTO.fields_by_name['genetic_model'].message_type = _GENETICMODELPROTO
_EVOLUTIONARYMODELPROTO.oneofs_by_name['model'].fields.append(
  _EVOLUTIONARYMODELPROTO.fields_by_name['genetic_model'])
_EVOLUTIONARYMODELPROTO.fields_by_name['genetic_model'].containing_oneof = _EVOLUTIONARYMODELPROTO.oneofs_by_name['model']
_EVOLUTIONARYSOLUTIONPROTO.fields_by_name['status'].enum_type = optimizer__defs__pb2._OPTIMIZERSOLUTIONSTATUSPROTO
_EVOLUTIONARYSOLUTIONPROTO.fields_by_name['genetic_algorithm_solution'].message_type = _GENETICALGORITHMSOLUTIONPROTO
_EVOLUTIONARYSOLUTIONPROTO.oneofs_by_name['solution'].fields.append(
  _EVOLUTIONARYSOLUTIONPROTO.fields_by_name['genetic_algorithm_solution'])
_EVOLUTIONARYSOLUTIONPROTO.fields_by_name['genetic_algorithm_solution'].containing_oneof = _EVOLUTIONARYSOLUTIONPROTO.oneofs_by_name['solution']
DESCRIPTOR.message_types_by_name['CreateEvolutionaryOptimizerReq'] = _CREATEEVOLUTIONARYOPTIMIZERREQ
DESCRIPTOR.message_types_by_name['ChromosomeProto'] = _CHROMOSOMEPROTO
DESCRIPTOR.message_types_by_name['StandardGeneticEnvironmentProto'] = _STANDARDGENETICENVIRONMENTPROTO
DESCRIPTOR.message_types_by_name['CustomGeneticEnvironmentProto'] = _CUSTOMGENETICENVIRONMENTPROTO
DESCRIPTOR.message_types_by_name['GeneticModelProto'] = _GENETICMODELPROTO
DESCRIPTOR.message_types_by_name['EvolutionaryModelProto'] = _EVOLUTIONARYMODELPROTO
DESCRIPTOR.message_types_by_name['GeneticAlgorithmSolutionProto'] = _GENETICALGORITHMSOLUTIONPROTO
DESCRIPTOR.message_types_by_name['EvolutionarySolutionProto'] = _EVOLUTIONARYSOLUTIONPROTO
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

CreateEvolutionaryOptimizerReq = _reflection.GeneratedProtocolMessageType('CreateEvolutionaryOptimizerReq', (_message.Message,), {
  'DESCRIPTOR' : _CREATEEVOLUTIONARYOPTIMIZERREQ,
  '__module__' : 'evolutionary_model_pb2'
  # @@protoc_insertion_point(class_scope:optilab.toolbox.CreateEvolutionaryOptimizerReq)
  })
_sym_db.RegisterMessage(CreateEvolutionaryOptimizerReq)

ChromosomeProto = _reflection.GeneratedProtocolMessageType('ChromosomeProto', (_message.Message,), {
  'DESCRIPTOR' : _CHROMOSOMEPROTO,
  '__module__' : 'evolutionary_model_pb2'
  # @@protoc_insertion_point(class_scope:optilab.toolbox.ChromosomeProto)
  })
_sym_db.RegisterMessage(ChromosomeProto)

StandardGeneticEnvironmentProto = _reflection.GeneratedProtocolMessageType('StandardGeneticEnvironmentProto', (_message.Message,), {
  'DESCRIPTOR' : _STANDARDGENETICENVIRONMENTPROTO,
  '__module__' : 'evolutionary_model_pb2'
  # @@protoc_insertion_point(class_scope:optilab.toolbox.StandardGeneticEnvironmentProto)
  })
_sym_db.RegisterMessage(StandardGeneticEnvironmentProto)

CustomGeneticEnvironmentProto = _reflection.GeneratedProtocolMessageType('CustomGeneticEnvironmentProto', (_message.Message,), {
  'DESCRIPTOR' : _CUSTOMGENETICENVIRONMENTPROTO,
  '__module__' : 'evolutionary_model_pb2'
  # @@protoc_insertion_point(class_scope:optilab.toolbox.CustomGeneticEnvironmentProto)
  })
_sym_db.RegisterMessage(CustomGeneticEnvironmentProto)

GeneticModelProto = _reflection.GeneratedProtocolMessageType('GeneticModelProto', (_message.Message,), {
  'DESCRIPTOR' : _GENETICMODELPROTO,
  '__module__' : 'evolutionary_model_pb2'
  # @@protoc_insertion_point(class_scope:optilab.toolbox.GeneticModelProto)
  })
_sym_db.RegisterMessage(GeneticModelProto)

EvolutionaryModelProto = _reflection.GeneratedProtocolMessageType('EvolutionaryModelProto', (_message.Message,), {
  'DESCRIPTOR' : _EVOLUTIONARYMODELPROTO,
  '__module__' : 'evolutionary_model_pb2'
  # @@protoc_insertion_point(class_scope:optilab.toolbox.EvolutionaryModelProto)
  })
_sym_db.RegisterMessage(EvolutionaryModelProto)

GeneticAlgorithmSolutionProto = _reflection.GeneratedProtocolMessageType('GeneticAlgorithmSolutionProto', (_message.Message,), {
  'DESCRIPTOR' : _GENETICALGORITHMSOLUTIONPROTO,
  '__module__' : 'evolutionary_model_pb2'
  # @@protoc_insertion_point(class_scope:optilab.toolbox.GeneticAlgorithmSolutionProto)
  })
_sym_db.RegisterMessage(GeneticAlgorithmSolutionProto)

EvolutionarySolutionProto = _reflection.GeneratedProtocolMessageType('EvolutionarySolutionProto', (_message.Message,), {
  'DESCRIPTOR' : _EVOLUTIONARYSOLUTIONPROTO,
  '__module__' : 'evolutionary_model_pb2'
  # @@protoc_insertion_point(class_scope:optilab.toolbox.EvolutionarySolutionProto)
  })
_sym_db.RegisterMessage(EvolutionarySolutionProto)


_GENETICALGORITHMSOLUTIONPROTO.fields_by_name['chromosome_value']._options = None
# @@protoc_insertion_point(module_scope)
