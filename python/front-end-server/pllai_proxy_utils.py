import logging
from pllai_message_builder import *


def send_message_to_service(request, socket, condition_variable, reply_set, ok_message):
    str_msg = request.SerializeToString()

    # Wait to receive OK from back-end from previous send call.
    # Notice that "reply_set" is initialized containing one (1) OK message
    with condition_variable:
        while ok_message not in reply_set:
            condition_variable.wait()

        reply_set.remove(ok_message)
        if not str_msg:
            print("EMPTY MESSAGE ------!")
        socket.send(str_msg)
        return 0


def send_wait_request(model_id, package_type, framework_type, socket, condition, rep_set, ok_msg):
    try:
        req_msg = build_wait_request(model_id, package_type, -1, framework_type)
        err = send_message_to_service(req_msg, socket, condition, rep_set, ok_msg)
        if err != 0:
            err_msg = "Optimizer wait request failed"
            logging.error(err_msg)
            return False
    except Exception as e:
        logging.exception(e)
        err_msg = "Cannot send optimizer wait request to backend, return"
        logging.error(err_msg)
        return False
    return True


def send_build_engine_request(model_id, class_type, package_type, framework_type, socket, condition, rep_set, ok_msg, opt_req=None):
    try:
        req_msg = build_create_engine_request(model_id, class_type, package_type, framework_type, opt_req)
        err = send_message_to_service(req_msg, socket, condition, rep_set, ok_msg)
        if err != 0:
            err_msg = "Engine creation request failed"
            logging.error(err_msg)
            return False
    except Exception as e:
        logging.exception(e)
        err_msg = "Cannot send engine creation request to backend, return"
        logging.error(err_msg)
        return False
    return True


def send_load_model_request(model_id, package_type, framework_type, proto_model, socket, condition, rep_set, ok_msg):
    try:
        req_msg = build_load_model_request(model_id, package_type, framework_type, proto_model)
        err = send_message_to_service(req_msg, socket, condition, rep_set, ok_msg)
        if err != 0:
            err_msg = "Optimizer load model request failed"
            logging.error(err_msg)
            return False
    except Exception as e:
        logging.exception(e)
        err_msg = "Cannot send load model request to backend, return"
        logging.error(err_msg)
        return False
    return True


def send_run_engine_request(model_id, package_type, framework_type, socket, condition, rep_set, ok_msg):
    try:
        req_msg = build_run_model_request(model_id, package_type, framework_type)
        err = send_message_to_service(req_msg, socket, condition, rep_set, ok_msg)
        if err != 0:
            err_msg = "Optimizer run request failed"
            logging.error(err_msg)
            return False
    except Exception as e:
        logging.exception(e)
        err_msg = "Cannot send run request to backend, return"
        logging.error(err_msg)
        return False
    return True


def send_solutions_request(model_id, package_type, framework_type, socket, condition, rep_set, ok_msg):
    try:
        req_msg = build_solutions_request(model_id, package_type, 1, framework_type)
        err = send_message_to_service(req_msg, socket, condition, rep_set, ok_msg)
        if err != 0:
            err_msg = "Optimizer collect solutions request failed"
            logging.error(err_msg)
            return False
    except Exception as e:
        logging.exception(e)
        err_msg = "Cannot send collect solutions request to backend, return"
        logging.error(err_msg)
        return False
    return True


def send_delete_engine_request(model_id, package_type, framework_type, socket, condition, rep_set, term_msg):
    try:
        req_msg = build_delete_engine_request(model_id, package_type, framework_type)
        err = send_message_to_service(req_msg, socket, condition, rep_set, term_msg)
        if err != 0:
            logging.error("Optimizer delete optimizer request failed")
    except Exception as e:
        logging.exception(e)
        err_msg = "Cannot send delete optimizer request to backend, return"
        logging.error(err_msg)
        return False
    return True


def build_engine(model_id, class_type, package_type, framework_type, socket, condition, rep_set, ok_msg, opt_req):
    logging.info(model_id + " - Create engine")
    res = send_build_engine_request(model_id, class_type, package_type, framework_type, socket,
                                    condition, rep_set, ok_msg, opt_req)
    if not res:
        return res
    res = send_wait_request(model_id, package_type, framework_type, socket, condition, rep_set, ok_msg)
    if not res:
        return res
    logging.info("done")
    return res


def load_model(model_id, package_type, framework_type, proto_model, socket, condition, rep_set, ok_msg):
    logging.info(model_id + " - Load model...")
    res = send_load_model_request(model_id, package_type, framework_type, proto_model, socket, condition, rep_set,
                                  ok_msg)
    if not res:
        return res
    res = send_wait_request(model_id, package_type, framework_type, socket, condition, rep_set, ok_msg)
    if not res:
        return res
    logging.info("done")
    return res


def run_engine(model_id, class_type, package_type, framework_type, socket, condition, rep_set, ok_msg):
    logging.info(model_id + " - Run engine...")
    res = send_run_engine_request(model_id, package_type, framework_type, socket, condition, rep_set, ok_msg)
    if not res:
        return res
    res = send_wait_request(model_id, package_type, framework_type, socket, condition, rep_set, ok_msg)
    if not res:
        return res
    logging.info("done")
    return res


def get_solutions(model_id, package_type, framework_type, socket, condition, rep_set, ok_msg):
    logging.info(model_id + " - Collect solutions...")
    res = send_solutions_request(model_id, package_type, framework_type, socket, condition, rep_set, ok_msg)
    if not res:
        return res
    res = send_wait_request(model_id, package_type, framework_type, socket, condition, rep_set, ok_msg)
    if not res:
        return res
    logging.info("done")
    return res


def delete_engine(model_id, package_type, framework_type, socket, condition, rep_set, term_msg):
    logging.info(model_id + " - Delete engine...")
    res = send_delete_engine_request(model_id, package_type, framework_type, socket, condition, rep_set, term_msg)
    if res:
        logging.info("done")
    return res


def build_request_and_forward_to_broker(model_id, class_type, package_type, proto_model, framework_type,
                                        socket, condition, rep_set, ok_msg, term_msg, opt_req=None):

    # Create back-end engine
    res = build_engine(model_id, class_type, package_type, framework_type, socket, condition, rep_set, ok_msg, opt_req)
    if not res:
        return res

    # Load model on the engine
    res = load_model(model_id, package_type, framework_type, proto_model, socket, condition, rep_set, ok_msg)
    if not res:
        delete_engine(model_id, package_type, framework_type, socket, condition, rep_set, ok_msg)
        return res

    # Run the engine
    res = run_engine(model_id, class_type, package_type, framework_type, socket, condition, rep_set, ok_msg)
    if not res:
        delete_engine(model_id, package_type, framework_type, socket, condition, rep_set, ok_msg)
        return res

    # Get solutions
    res_sol = get_solutions(model_id, package_type, framework_type, socket, condition, rep_set, ok_msg)
    if not res_sol:
        delete_engine(model_id, package_type, framework_type, socket, condition, rep_set, ok_msg)
        return res_sol

    # Delete engine
    final_msg = ok_msg
    if res_sol:
        final_msg = term_msg
    res = delete_engine(model_id, package_type, framework_type, socket, condition, rep_set, final_msg)
    return res
