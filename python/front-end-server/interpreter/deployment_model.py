import os
import logging
import time
import socket
from flask import Flask, request
from flask.views import View
from parallelsdk.client import *


class StreamAction(View):
    def __init__(self, node, parallel_ai_server_ip):
        self.node = node
        self.optimizer_ip = parallel_ai_server_ip
        self.optimizer = None
        self.init_connection()

    def dispatch_request(self):
        # The following depends on the type of data received by the node.
        # Notice that the request can contain data from multiple-sources:
        # - request.json
        # - request.files
        # - request.args
        # - request.values
        json_request = request.json
        file_request = request.files

        if json_request:
            streamed_input = json_request
            if isinstance(streamed_input, list):
                arg_stream_type = 'list'
            else:
                arg_stream_type = 'json'
        else:
            # File request
            f = file_request["data"]
            streamed_input = f.read()
            arg_stream_type = 'bytes'

        # Create a new optimizer connected to the back-end service for running this pipeline
        #start_time = time.time()
        #optimizer = ParallelClient(self.optimizer_ip)
        #optimizer.connect()
        #time.sleep(0.2)
        #startup_time = time.time() - start_time

        # Calling run on the node, this will trigger recursive execution of all children
        start_time = time.time()
        try:
            self.node.run(streamed_input, self.optimizer, arg_type=arg_stream_type)
        except Exception as e:
            return "Parallel deployment service: error while streaming data\n\t" + str(e) + '\n'
        except:
            return "Parallel deployment service: error while streaming data\n"

        exe_time = time.time() - start_time
        timer_info = "(execution (sec.): " + str(int(exe_time)) + ")"
        # timer_info = "(startup: " + str(int(startup_time)) + ", execution: " + str(int(exe_time)) + ")"
        return "Parallel deployment service: success " + timer_info + '\n'

    def init_connection(self):
        self.optimizer = ParallelClient(self.optimizer_ip)
        self.optimizer.connect()
        time.sleep(0.2)


class EndpointAction(View):
    def __init__(self, node):
        self.node = node

    def dispatch_request(self):
        return self.node.get_query()


class DeploymentModel:
    def __init__(self, model, parallel_ai_service_ip):
        self.model = model
        self.parallel_ai_service_ip = parallel_ai_service_ip
        self.input_node_list = []
        self.output_node_list = []
        self.app_runner = Flask(str(id(self)))
        self.app_runner.logger.setLevel(logging.ERROR)

        for node in self.model.values():
            if node.is_streamable():
                self.app_runner.add_url_rule(node.get_stream_endpoint(),
                                             os.path.basename(node.get_stream_endpoint()),
                                             view_func=StreamAction.as_view(
                                                 'stream', node=node,
                                                 parallel_ai_server_ip=self.parallel_ai_service_ip),
                                             methods=["POST"])
                # Add this node to the list of input nodes
                self.input_node_list.append(node)
            elif node.is_queryable():
                self.app_runner.add_url_rule(node.get_query_endpoint(),
                                             os.path.basename(node.get_query_endpoint()),
                                             view_func=EndpointAction.as_view(
                                                 'endpoint', node=node),
                                             methods=["GET"])

                # Add this node to the list of output nodes
                self.output_node_list.append(node)

    def requires_deployment(self):
        # Deploy the model only if the pipeline has at least one input/streamable node
        # and at least one output/queryable node
        return self.input_node_list and self.output_node_list

    def __call__(self):
        # TODO start multiple threads, one for each streamable node and run the following in parallel
        try:
            self.app_runner.run(host=self.input_node_list[0].get_host(), port=self.input_node_list[0].get_port())
        except socket.error:
            logging.error("Deployment run: socket error")
            pass
        except:
            logging.error("Deployment run: undefined error")
            pass
