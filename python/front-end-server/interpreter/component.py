class Component:
    def __init__(self, root):
        self.root = root

    def is_leaf(self):
        raise NotImplemented()

    def run(self, input_args=None):
        raise NotImplemented()


class Composite(Component):
    def __init__(self, root, has_input=False):
        super().__init__(root)
        self.children = set()
        self.has_input = has_input

    def is_leaf(self):
        return self.get_num_children() == 0

    def get_num_children(self):
        return len(self.children)

    def run(self, input_args, optimizer, arg_type=''):
        for child in self.children:
            child.root.run(input_args, optimizer, arg_type)

    def add(self, component):
        self.children.add(component)

    def remove(self, component):
        self.children.discard(component)

    def __str__(self):
        child_strings = [str(child) for child in self.children]
        return "%s block w/ children: %s" % (self._mtype, str(child_strings))


class Leaf(Component):
    def __init__(self, root):
        super().__init__(root)
        self.has_input = True

    def run(self, input_args):
        pass

    def is_leaf(self):
        return True

    def __str__(self):
        return self._mtype
