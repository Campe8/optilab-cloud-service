import logging
import json
from flask import Flask
from flask import request
from flask_socketio import SocketIO
from .deployment_register import *
from nodes.data_stream import *
from nodes.endpoint import *
from nodes.pyfunction import *
from nodes.node_config import *

# Define the interpreter application
InterpreterApp = Flask(__name__)
InterpreterApp.config['SECRET_KEY'] = 'secret!'
logger = logging.getLogger(__name__)
sio = SocketIO(InterpreterApp, cors_allowed_origins='*', logger=logger)

deployment_register = DeploymentRegister()


def get_node_class_from_data(data):
    if 'toolbox_code' not in data:
        return None
    return get_class_from_opcode(data['toolbox_code'])


@sio.on('connect')
def handle_connection():
    InterpreterApp.logger.info("New client connected: %s" % str(request.sid))
    deployment_register.add_client_deployment(request.sid)


@sio.on('disconnect')
def handle_disconnect():
    InterpreterApp.logger.info("Client disconnected: %s" % str(request.sid))
    deployment_register.remove_client_deployment(request.sid)


@sio.on_error()
def error_handler(e):
    InterpreterApp.logger.error(e, exc_info=True)


@sio.on('add node')
def handle_add_node(data):
    InterpreterApp.logger.info("add node")
    try:
        node_data = json.loads(data, encoding='utf-8')
        new_node = get_node_class_from_data(node_data)()
        print(new_node)
        print(node_data)
        new_node.load_node(node_data)
        deployment_register[request.sid].add_node_to_model(new_node)
    except Exception as e:
        return False, str(e)
    except:
        return False, "Undefined error while deploying a node"
    return True, ""


@sio.on('add edge')
def handle_add_edge(data):
    # Data is a JSON message with the following syntax
    # {'id': 140511087034128, 'start': 140511086975872, 'end': 140511086976640}
    InterpreterApp.logger.info("add edge")
    try:
        edge_data = json.loads(data, encoding='utf-8')
        start_socket = edge_data["start"]
        end_socket = edge_data["end"]
        node_input = deployment_register[request.sid].get_node_from_socket_id(start_socket)
        node_output = deployment_register[request.sid].get_node_from_socket_id(end_socket)
        node_input.add(node_output)
    except Exception as e:
        return False, str(e)
    except:
        return False, "Undefined error while deploying an edge"
    return True, ""


@sio.on('deploy')
def handle_deploy():
    deployment = deployment_register[request.sid]
    mid = deployment.deploy_model()
    InterpreterApp.logger.info("deploy: MODEL ID %s, %s" % (str(mid), str(deployment.model)))
