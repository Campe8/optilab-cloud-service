from multiprocessing import Process
from .deployment_model import DeploymentModel
from parallelsdk.client import *


class ClientDeployment:
    def __init__(self, parallel_ai_service_ip):
        self.model = {}
        self.parallel_ai_service_ip = parallel_ai_service_ip
        self.socket_to_node_map = {}
        self.deployed_models = {}

    def __del__(self):
        # TODO fix when a user disconnects we delete their deployed models
        for deployed_model in self.deployed_models.values():
            deployed_model.terminate()

    def add_node_to_model(self, node):
        # Add the node and do a reverse mapping from sockets to nodes
        node_id = node.get_id()
        self.model[node_id] = node
        for socket_id in node.get_input_sockets():
            self.socket_to_node_map[socket_id] = node_id
        for socket_id in node.get_output_sockets():
            self.socket_to_node_map[socket_id] = node_id

    def get_node_from_socket_id(self, socket_id):
        node_id = self.socket_to_node_map[socket_id]
        return self.model[node_id]

    def run_model(self):
        for node in self.model.values():
            if node.is_root():
                node.run(None)

    def deploy_model(self):
        deployment_model = DeploymentModel(self.model, self.parallel_ai_service_ip)
        if deployment_model.requires_deployment():
            p = Process(target=deployment_model)
            self.deployed_models[p.pid] = p
            p.start()
            return p.pid
        return -1


class DeploymentRegister:
    def __init__(self, service_ip='127.0.0.1'):
        self.service_ip = service_ip
        self.deployments = {}

    def __getitem__(self, deployment_id):
        return self.deployments[deployment_id]

    def add_client_deployment(self, deployment_id):
        self.deployments[deployment_id] = ClientDeployment(self.service_ip)

    def remove_client_deployment(self, deployment_id):
        del self.deployments[deployment_id]
