import logging

from interpreter.node_config import *
from proto import optimizer_model_pb2
from proto import data_model_pb2
from google.protobuf.json_format import Parse

DEBUG = False


class DeploymentEngine:
    def __init__(self):
        pass

    def set_typed_value(self, typed_val, val, proto_type):
        if proto_type == "double":
            typed_val.double_arg = val
            return True
        elif proto_type == "float":
            typed_val.float_arg = val
            return True
        elif proto_type == "int":
            typed_val.int_arg = val
            return True
        elif proto_type == "uint":
            typed_val.uint_arg = val
            return True
        elif proto_type == "bool":
            typed_val.bool_arg = val
            return True
        elif proto_type == "string":
            typed_val.string_arg = val
            return True
        elif proto_type == "byte":
            typed_val.bytes_arg = val
            return True
        elif proto_type == "bytes":
            typed_val.bytes_arg = val
            return True
        else:
            return False

    def get_node_class_from_data(self, data):
        if 'op_code' not in data:
            return None
        return get_class_from_opcode(data['op_code'])

    def deploy(self, graph):
        ##############################################################
        # BELOW IS JUST AN EXAMPLE. THIS WHOLE CLASS WILL BE REMOVED #
        ##############################################################
        graph_nodes = graph['nodes']
        for node_data in graph_nodes:
            new_node = self.get_node_class_from_data(node_data)()
            new_node.load_node(node_data)

            if node_data['node_name'] == 'Data Stream':
                pass
            elif node_data['node_name'] == 'Endpoint':
                pass
            elif node_data['node_name'] == 'Function':
                semantics = node_data['semantics']
                assert semantics[0] == 'proto'
                # The proto message encapsulates a DataModelProto.
                # Refer to data_model.proto for the fcn_proto fields.
                # Notice that the type of proto message "DataModelProto" to parse into it is given.
                # It is possible to store these type in a map ["node_name", proto_type]
                fcn_proto = message = Parse(semantics[1], data_model_pb2.DataModelProto())
                print("model_id: ", fcn_proto.model_id)

                # Python model is a PythonModelProto
                python_model = fcn_proto.python_model
                print("Entry function name: ", python_model.entry_fcn_name)
                print("Entry module name: ", python_model.entry_module_name)
                # Similar for other members

                # To setup some INPUT arguments (when the daemon receives them),
                # do what is done in parallelsdk.data_toolbox.python_function_tool:
                my_input_arg_from_stream = 123

                # Note that this implies that the type of the input is available
                my_input_arg_from_stream_data_type = "int"
                fcn_arg = python_model.input_args.add()
                if isinstance(my_input_arg_from_stream, list):
                    # List of values
                    for x in my_input_arg_from_stream:
                        typed_val = fcn_arg.argument.add()
                        if not self.set_typed_value(typed_val, x, my_input_arg_from_stream_data_type):
                            err_msg = "PythonFunctionTool - invalid list input data type"
                            logging.error(err_msg)
                            raise Exception(err_msg)
                else:
                    # Scalar value
                    typed_val = fcn_arg.argument.add()
                    if not self.set_typed_value(typed_val, my_input_arg_from_stream,
                                                my_input_arg_from_stream_data_type):
                        err_msg = "PythonFunctionTool - invalid scalar input data type"
                        logging.error(err_msg)
                        raise Exception(err_msg)

                # Now the function is almost ready to be sent to the back-end engine.
                # To send it, prepare it such that the broker and back-end can receive it.
                # Basically prepare a message that can be given to the self.data_and_ports_to_broker(msg) method.
                # In other words, we need to create an OptimizerModel (see optimizer_model.proto)
                # First you create an optimizer model
                python_proto_fcn_model = optimizer_model_pb2.OptimizerModel()
                python_proto_fcn_model.data_model.CopyFrom(fcn_proto)

                # Now python_proto_fcn_model is ready to be sent to the back-end broker
                # self.data_and_ports_to_broker(python_proto_fcn_model)
                return True
