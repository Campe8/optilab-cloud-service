import sys
import os
import asyncio
import tornado.escape
import tornado.ioloop
import tornado.options
import tornado.web
import tornado.websocket
import logging
from tornado.options import options
from server_app import ServerApp
from interpreter.interpreter_app import InterpreterApp, sio, deployment_register
from flask import request
from tornado.options import define

DEBUG = True

sys.path.insert(0, os.path.join(os.path.dirname(__file__), "..", ".."))

define("port", default=8080, help="run the server on the given port", type=int)


def run_server():
    tornado.options.parse_command_line()

    # Create the event loop spawning up the tornado server
    asyncio.set_event_loop(asyncio.new_event_loop())

    # Create the server application listening on the specified port
    server_app = ServerApp()
    server_app.listen(options.port)

    # Start the server loop
    if DEBUG:
        print("Parallel AI server - start server...")
    try:
        tornado.ioloop.IOLoop.current().start()
    except Exception as e:
        logging.exception(e)
        print("Parallel AI server - exception captured while running the server")
    except:
        print("Parallel AI server - undefined exception captured while running the server")
    if DEBUG:
        print("Parallel AI server - server shutdown")


def run_interpreter():
    sio.run(InterpreterApp)


def print_help(name):
    print("Usage:")
    print("\t" + name + " [server|interpreter]")


def main():
    if len(sys.argv[1:]) > 0:
        for arg in sys.argv[1:]:
            print("Running: ", arg)
            if arg == 'server':
                run_server()
            elif arg == 'interpreter':
                run_interpreter()
            else:
                print_help(sys.argv[0])
    else:
        print_help(sys.argv[0])


if __name__ == '__main__':
    main()
