import random
import string
import tornado.web
import tornado.options
from tornado.options import options
from tornado.options import define

from pllai_proxy import PllAIProxy

DEBUG = True

define("brokerPort", default=5672, help="broker input port", type=int)


def get_random_identifier(str_len=10):
    """Generate a random string of letters and digits """
    letters_and_digits = string.ascii_letters + string.digits
    return ''.join(random.choice(letters_and_digits) for i in range(str_len))


class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.write("MainHandler")

    def check_origin(self, origin):
        return True


class OptimizerProtoSocketHandler(tornado.websocket.WebSocketHandler):
    socket_set = set()
    proxy_map = {}

    def check_origin(self, origin):
        return True

    def is_open(self):
        if hasattr(self, '_open_socket'):
            return self._open_socket
        else:
            return False

    def open(self):
        print("OptimizerFrontend - server: opened connection")

        # On open add self to the set of opened sockets
        OptimizerProtoSocketHandler.socket_set.add(self)

        # Create a new Parallel AI proxy and add it to the map of proxies
        pllai_proxy_id = get_random_identifier()
        pllai_proxy = PllAIProxy(pllai_proxy_id, options.brokerPort, self)
        if DEBUG:
            print("Open connection: create new Parallel AI proxy - ", pllai_proxy_id)
        OptimizerProtoSocketHandler.proxy_map[self] = pllai_proxy

        # Start the proxy
        OptimizerProtoSocketHandler.proxy_map[self].init_proxy()

        # Flag indicating that the socket is open
        self._open_socket = True

    def on_close(self):
        print("OptimizerFrontend - server: closed connection")

        # Remove the socket and the proxy from the set and map respectively
        self._open_socket = False
        OptimizerProtoSocketHandler.socket_set.remove(self)
        # TODO investigate why calling the following "turn_down" destroyes the socket
        # and ZeroMQ recv throws even if LINGER is set to zero.
        # Idea: maybe it needs to wait for some seconds for complete shut-down
        # OptimizerProtoSocketHandler.proxy_map[self].turn_down()
        if DEBUG:
            print("Close connection: delete new Parallel AI proxy - ",
                  OptimizerProtoSocketHandler.proxy_map[self].get_id())
        del OptimizerProtoSocketHandler.proxy_map[self]

    def on_message(self, message):
        if message == "__CLIENT_LOG_OFF__":
            self.close()
        else:
            # Re-direct the message received from the client to the proxy
            OptimizerProtoSocketHandler.proxy_map[self].on_proto_message(message)


class ServerApp(tornado.web.Application):
    def __init__(self):
        # Define the handlers, i.e., the ports to listen to
        self.handlers = [
            (r"/", MainHandler),
            (r"/proto_service", OptimizerProtoSocketHandler)]
        super(ServerApp, self).__init__(self.handlers)
