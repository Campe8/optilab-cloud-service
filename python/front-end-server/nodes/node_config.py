OP_TOOL_FILE = 1
OP_TOOL_DATA_TABLE = 2
OP_TOOL_SQL = 3
OP_TOOL_DATA_STREAM = 4
OP_TOOL_MONITOR = 5
OP_TOOL_MINIZINC = 6
OP_TOOL_MPS = 7
OP_TOOL_LOCATION = 8
OP_TOOL_VEHICLE = 9
OP_TOOL_ROUTING = 10
OP_TOOL_TSP = 11
OP_TOOL_TEAM = 12
OP_TOOL_EMP_SCHED = 13
OP_TOOL_JSON = 14
OP_TOOL_MAP = 15
OP_TOOL_FUNCTION = 16
OP_TOOL_PLLAI = 17
OP_TOOL_ENDPOINT = 18
OP_TOOL_CSV = 19

OP_TOOLS = {
}


class ConfigException(Exception):
    pass


class OpCodeNotRegistered(ConfigException):
    pass


class InvalidNodeRegistration(ConfigException):
    pass


def register_node_impl(op_code, class_reference):
    if op_code in OP_TOOLS:
        raise InvalidNodeRegistration("Duplicate node registration of '%s'. There is already %s" % (
            op_code, OP_TOOLS[op_code]
        ))
    OP_TOOLS[op_code] = class_reference


def register_node(op_code):
    def decorator(original_class):
        register_node_impl(op_code, original_class)
        return original_class
    return decorator


def get_class_from_opcode(op_code):
    if op_code not in OP_TOOLS:
        raise OpCodeNotRegistered("OpCode '%d' is not registered" % op_code)
    return OP_TOOLS[op_code]
