import logging
import json
import copy
import threading
from nodes.node import *
from nodes.node_config import *

from parallelsdk.data_toolbox.data_toolbox import *
from proto import optimizer_model_pb2
from proto import data_model_pb2
from google.protobuf.json_format import Parse

DEBUG = True


@register_node(OP_TOOL_FUNCTION)
class PyFunction(Node):
    op_code = OP_TOOL_FUNCTION

    def __init__(self):
        # TODO: check Composite/Leaf pattern for functions with no-inputs, no-outputs or both
        super().__init__(Composite(self))

        # Proto message to send to the broker
        self.fcn_proto = None

        # Proto message encapsulating Python function node
        self.python_model = None

        # Reference to the input argument types
        self.python_input_arg_descriptor = None

        # Python function sdk tool used to run the optimizer
        self.python_fcn_tool = None

    def load_node_semantics(self, semantics):
        if semantics[0] == 'proto':
            self.load_node_from_proto(semantics[1])
        else:
            assert semantics[0] == 'json'
            self.load_node_from_json(semantics[1])

    def load_node_from_json(self, semantics):
        # The semantics for this node is a dictionary similar to the following
        # {
        #    "model_id": "myFcn",
        #    "entry_fcn_name": "myFcn",
        #    "entry_module_name": "tmpnr06fi3c",
        #    "input_args": [
        #        [
        #            "int",
        #            false
        #        ]
        #    ],
        #    "output_args": [
        #        [
        #            "int",
        #            false
        #        ]
        #    ],
        #    "python_file_path": "/tmp/"
        # }
        json_semantics = json.loads(semantics, encoding='utf-8')
        if DEBUG:
            print("model_id: ", json_semantics['model_id'])
            print("Entry function name: ", json_semantics['entry_fcn_name'])
            print("Entry module name: ", json_semantics['entry_module_name'])
            print("File path: ", json_semantics['python_file_path'])

        self.python_fcn_tool = BuildPythonFunctionTool(json_semantics['model_id'])
        python_fcn = self.python_fcn_tool.get_instance()
        python_fcn.add_entry_fcn_name(json_semantics['entry_fcn_name'])
        python_fcn.add_entry_module_name(json_semantics['entry_module_name'])
        python_fcn.add_environment_path(json_semantics['python_file_path'])

        # Add output arguments
        for arg in json_semantics['output_args']:
            python_fcn.add_output_argument(arg[0], arg[1])

        # Store reference to the input argument types
        self.python_input_arg_descriptor = json_semantics['input_args']

    def load_node_from_proto(self, semantics):
        # The semantics for this node is a dictionary similar to the following
        # [ "proto,
        # { "model_id": "myFcn",
        #   "python_model": {
        #     "entry_fcn_name": "myFcn",
        #     "entry_module_name": "tmpteqtvcjh",
        #     "output_args": [ {"argument": [ {"int_arg": "0"} ]} ],
        #     "python_file_path": ["tmp"]
        #    }
        # }]"
        # The proto message encapsulates a DataModelProto.
        # Refer to data_model.proto for the fcn_proto fields.
        # Notice that the type of proto message "DataModelProto" to parse into is given.
        # Therefore, fcn_proto is of type DataModelProto
        self.fcn_proto = Parse(semantics, data_model_pb2.DataModelProto())
        self.python_model = self.fcn_proto.python_model

        if DEBUG:
            print("model_id: ", self.fcn_proto.model_id)
            print("Entry function name: ", self.python_model.entry_fcn_name)
            print("Entry module name: ", self.python_model.entry_module_name)
            print("File path: ", self.python_model.python_file_path)

        self.python_fcn_tool = BuildPythonFunctionTool(self.fcn_proto.model_id)
        python_fcn = self.python_fcn_tool.get_instance()
        python_fcn.add_entry_fcn_name(self.python_model.entry_fcn_name)
        python_fcn.add_entry_module_name(self.python_model.entry_module_name)
        python_fcn.add_environment_path(self.python_model.python_file_path[0])

        # self.python_model.output_args
        # TODO fix the below w.r.t. the actual output arguments
        python_fcn.add_output_argument("int", False)

    def set_callback_arguments(self, args):
        # Clear all previous runs arguments
        fcn_tool = copy.deepcopy(self.python_fcn_tool)
        fcn_tool.get_instance().clear_input_arguments()

        # The given arguments should match the descriptor in self.python_input_arg_descriptor
        num_inputs = len(self.python_input_arg_descriptor)
        if num_inputs > 1:
            if not isinstance(args, list):
                return False
            else:
                if len(args) != num_inputs:
                    return None
                else:
                    idx = 0
                    for arg in args:
                        fcn_tool.get_instance().add_input_argument(
                            arg, self.python_input_arg_descriptor[idx][0])
                        idx += 1
        else:
            fcn_tool.get_instance().add_input_argument(args, self.python_input_arg_descriptor[0][0])

        return fcn_tool

    def execute(self, input_args, optimizer, arg_type=''):
        if DEBUG:
            print("PyFunction input received: ", str(input_args), " on ", self)

        # Set the input args in the protobuf and call the optimizer
        assert self.python_fcn_tool
        fcn_tool = self.set_callback_arguments(input_args)
        if not fcn_tool:
            return None

        # Call the back-end engine
        if DEBUG:
            print("Call back-end service on ", fcn_tool, " with thread ", threading.get_ident())

        optimizer.run_optimizer_synch(fcn_tool)
        out_result = fcn_tool.get_instance().get_output()

        if DEBUG:
            print("PyFunction return: ", str(out_result))

        return out_result
