import requests
import json
import queue
import collections
import threading
from nodes.node import *
from nodes.node_config import *

from proto import optimizer_model_pb2
from proto import data_model_pb2

DEBUG = True


@register_node(OP_TOOL_ENDPOINT)
class Endpoint(QueryableNode):
    op_code = OP_TOOL_ENDPOINT

    def __init__(self):
        super().__init__(Composite(self), query_endpoint='/output')
        self.input_data_type = ''
        self.ip = ''
        self.port = ''
        self.clear_buffer = False
        self.get_query_mutex = threading.Lock()
        self.queue = queue.Queue(100)
        self.get_queue = collections.deque(maxlen=100)

    def get_target_uri(self):
        uri = "http://" + self.ip + ":" + str(self.port) + self.get_query_endpoint()
        return uri

    def load_node_semantics(self, semantics):
        # The semantics for this node is a dictionary similar to the following
        # [ "json,
        # "{
        #   "ip": "127.0.0.1",
        #   "port": 64000,
        #   "channel": "output"
        #   "clear_buffer": true
        #   }]"
        assert semantics[0] == 'json'

        data_semantics = json.loads(semantics[1], encoding='utf-8')
        self.ip = data_semantics['ip']
        self.port = data_semantics['port']
        self.clear_buffer = data_semantics['clear_buffer']
        endpoint_name = "/" + data_semantics['channel']
        self.set_query_endpoint(endpoint_name)

    def execute(self, input_args, optimizer, arg_type=''):
        """
        Execute this node on the given input arguments and with
        the given optimizer.
        """

        # Clean previous data if clear buffer is True
        if self.clear_buffer:
            self.get_query_mutex.acquire()
            self.get_queue.clear()
            self.get_query_mutex.release()
            while not self.queue.empty():
                self.queue.get()

        target_uri = self.get_target_uri()
        if DEBUG:
            print("endpoint: sending data to ", target_uri)

        # input_args is the data streamed by the client
        try:
            self.queue.put_nowait(input_args)
            self.get_query_mutex.acquire()
            self.get_queue.append(input_args)
            self.get_query_mutex.release()
        except:
            print("Queue full: cannot add another element")
            requests.post(target_uri, {"data": ''})
            return None

        if DEBUG:
            print("endpoint: data type - ", type(input_args), " data ", input_args)

        data = self.query()
        if DEBUG:
            print("endpoint: sending data ", data)

        requests.post(target_uri, {"data": data})
        return data

    def get_query(self):
        self.get_query_mutex.acquire()
        data = ''
        if self.get_queue:
            data = self.get_queue.popleft()
        self.get_query_mutex.release()
        return {"data": data}

    def query(self):
        try:
            data = self.queue.get_nowait()
        except:
            print("No data in the queue")
            data = ''
        return data
