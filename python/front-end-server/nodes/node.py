from interpreter.component import *


DEBUG = True


class Node:
    def __init__(self, component):
        self.node_id = -1
        self.node_name = ''
        self.inputs = []
        self.outputs = []
        self.toolbox_code = -1
        self.component = component

    def is_streamable(self):
        return False

    def is_queryable(self):
        return False

    def is_root(self):
        return not self.component.has_input

    def get_id(self):
        return self.node_id

    def get_input_sockets(self):
        return self.inputs

    def get_output_sockets(self):
        return self.outputs

    def load_node(self, node_data):
        # Example of a JSON node representation
        # {
        #    "id": 140330653667440,
        #    "node_name": "Function",
        #    "is_root": false,
        #    "is_leaf": false,
        #    "inputs": [
        #        {
        #            "id": 140330653667632,
        #            "index": 0
        #        }
        #    ],
        #    "outputs": [
        #        {
        #            "id": 140330650768624,
        #            "index": 0
        #        }
        #    ],
        #    "toolbox_code": 14,
        #    "semantics": [
        #        "proto",
        #        "{...}"
        #    ]
        # }
        self.node_id = node_data['id']
        self.node_name = node_data['node_name']
        for input_socket in node_data['inputs']:
            self.inputs.append(input_socket['id'])
        for output_socket in node_data['outputs']:
            self.outputs.append(output_socket['id'])
        self.toolbox_code = node_data['toolbox_code']
        self.load_node_semantics(node_data['semantics'])

    def load_node_semantics(self, semantics):
        raise NotImplemented()

    def run(self, input_args, optimizer, arg_type=''):
        if DEBUG:
            print("Running: ", self.node_name)
        out_vals = self.execute(input_args, optimizer, arg_type)

        if DEBUG:
            print("output: ", out_vals)

            if self.component.is_leaf():
                print("Running num. children: 0")
            else:
                print("Running num. children: ", self.component.get_num_children())

        # TODO provide the type/suggestion for out_vals to the children
        self.component.run(out_vals, optimizer)

    def execute(self, input_args, optimizer, arg_type=''):
        """
        Execute this node on the given input arguments and with
        the given optimizer.
        """
        raise NotImplemented()

    def add(self, node):
        if isinstance(self.component, Composite):
            self.component.add(node.component)
        else:
            raise TypeError("Cannot add child to Leaf component")

    def remove(self, node):
        if isinstance(self.component, Composite):
            self.component.remove(node.component)
        else:
            raise TypeError("Cannot remove child from Leaf component")


class StreamableNode(Node):
    def __init__(self, component, host, port, stream_endpoint="/input"):
        super().__init__(component)

        self.host = host
        self.port = port
        self.stream_endpoint = stream_endpoint

    def is_streamable(self):
        return True

    def set_stream_endpoint(self, endpoint):
        self.stream_endpoint = endpoint

    def get_stream_endpoint(self):
        return self.stream_endpoint

    def set_host(self, host):
        self.host = host

    def get_host(self):
        return self.host

    def set_port(self, port):
        self.port = port

    def get_port(self):
        return self.port


class QueryableNode(Node):
    def __init__(self, component, query_endpoint="/output"):
        super().__init__(component)

        self.query_endpoint = query_endpoint

    def is_queryable(self):
        return True

    def set_query_endpoint(self, endpoint):
        self.query_endpoint = endpoint

    def get_query_endpoint(self):
        return self.query_endpoint

    def get_query(self):
        """Query method used for GET requests"""
        raise NotImplemented()

    def query(self):
        """Query method used for POST requests to specified endpoints"""
        raise NotImplemented()
