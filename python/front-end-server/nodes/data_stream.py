import logging
import json
from nodes.node import *
from nodes.node_config import *

from proto import optimizer_model_pb2
from proto import data_model_pb2

DEBUG = True


@register_node(OP_TOOL_DATA_STREAM)
class DataStream(StreamableNode):
    op_code = OP_TOOL_DATA_STREAM

    def __init__(self):
        super().__init__(Composite(self), host='127.0.0.1', port='64000')
        self.input_data_type = ''
        self.is_data_list = False

    def parse_streamed_input(self, input_args, arg_type):
        if DEBUG:
            print("Parse streamed input: ", input_args, type(input_args))
            print("Expected: ", self.input_data_type, " is list: ", self.is_data_list)

        if arg_type == 'json':
            streamed_data = json.loads(input_args, encoding='utf-8')
        elif arg_type == 'bytes':
            streamed_data = input_args.decode("utf-8")
        else:
            # Everything else
            streamed_data = input_args

        return streamed_data

    def load_node_semantics(self, semantics):
        # The semantics for this node is a dictionary similar to the following
        # [ "json,
        # "{
        #   "data_type": "int",
        #   "data_list": true,
        #   "channel": "input"
        #   "port": "64000",
        #   "buffer": "1000"
        #   }]"
        assert semantics[0] == 'json'

        stream_data = json.loads(semantics[1], encoding='utf-8')
        self.set_host('127.0.0.1')
        self.set_port(stream_data['port'])
        endpoint_name = "/" + stream_data['channel']

        self.set_stream_endpoint(endpoint_name)
        self.input_data_type = stream_data['data_type']
        self.is_data_list = stream_data['data_list']

    def execute(self, input_args, optimizer, arg_type=''):
        """
        Execute this node on the given input arguments and with
        the given optimizer.
        """
        # input_args is the text streamed by the client
        return self.parse_streamed_input(input_args, arg_type)
