import threading
import time
import zmq
import logging
import random
import string
import asyncio
import json

from proto import optilab_pb2
from proto import optimizer_model_pb2
from proto import linear_model_pb2
from proto import evolutionary_model_pb2
from proto import routing_model_pb2
from proto import scheduling_model_pb2
from proto import data_model_pb2
from proto import constraint_model_pb2

from pllai_proxy_utils import build_request_and_forward_to_broker

kCondition = threading.Condition()
kOKMessageId = 1000
kTermMessageId = 1010
kThreadRun = True
k_process_completed = "__PROCESS_COMPLETED__"
k_health_message = "__HEALTH_CHECK_STATUS_OK__"
k_error_message = "__ERROR_MESSAGE__"
k_info_message = "__INFO_MESSAGE__"

DEBUG = False


def get_random_identifier(str_len=10):
    """Generate a random string of letters and digits """
    letters_and_digits = string.ascii_letters + string.digits
    return ''.join(random.choice(letters_and_digits) for i in range(str_len))


class HealthMonitorThread(threading.Thread):
    def __init__(self, group=None, target=None, name=None, args=(), kwargs=None, daemon_run=False):
        threading.Thread.__init__(self, group=group, target=target, name=name, daemon=daemon_run)
        self.args = args
        self.kwargs = kwargs

    def run(self):
        asyncio.set_event_loop(asyncio.new_event_loop())
        client_websocket = self.args[0]
        global kThreadRun
        while kThreadRun:
            time.sleep(2)
            try:
                if client_websocket.is_open():
                    client_websocket.write_message(k_health_message)
                else:
                    logging.info("HealthMonitorThread - socket closed, return")
                    return
            except Exception as e:
                logging.exception(e)
                logging.error("HealthMonitorThread - run: bad write_message")
                kThreadRun = False
                return
            except:
                logging.error("HealthMonitorThread - run: bad write_message")
                kThreadRun = False
                return
        return


class PllAIProxyImpl:
    """Implementation for the Parallel AI proxy server"""
    proxy_id = ""
    broker_port = None
    context = None
    socket = None
    web_socket = None

    def __init__(self, proxy_id, port, web_socket):
        self.proxy_id = proxy_id
        self.broker_port = port
        self.web_socket = web_socket
        self.replay_set = set()

        # Engine use for model deployments
        # self.deployment_engine = DeploymentEngine()

        # Initial okay message
        self.replay_set.add(kOKMessageId)

    def __del__(self):
        self.shut_down()

    def get_broker_address(self):
        return "tcp://localhost:" + str(self.broker_port)

    def run(self):
        # Prepare context and sockets.
        # @note "socket" is the socket used for communication to/from this client and the broker.
        # "socket" is not facing the user and it is a ZeroMQ socket.
        # On the other hand, the user communicates to this front-end runner
        # using the websocekt "self.web_socket"
        self.context = zmq.Context()
        self.socket = self.context.socket(zmq.DEALER)
        self.socket.setsockopt_string(zmq.IDENTITY, get_random_identifier())

        # Connect this proxy to the broker
        logging.info("PllAIProxyImpl - run: connecting to %s", self.get_broker_address())
        self.socket.connect(self.get_broker_address())

        # Start the thread handling incoming messages from the broker
        th = threading.Thread(name="Server_Broker_Thread", target=self.handle_incoming_broker_messages,
                              args=(self.socket, self.web_socket, self.replay_set), daemon=True)
        th.start()

        # Start the health thread
        health_th = HealthMonitorThread(name="Health_Thread", args=(self.web_socket, ), daemon_run=True)
        health_th.start()

    def shut_down(self):
        global kThreadRun
        kThreadRun = False
        self.socket.close(linger=0)
        self.context.destroy(linger=0)
        self.socket = None

    def handle_incoming_broker_messages(self, socket, client_websocket, replay_set):
        """
        Handles incoming messages from the back-end engine.
        These messages are forwarded back to the client process
        connected on the other end of the socket.
        """
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)

        global kThreadRun
        while kThreadRun:
            # Blocking call, waiting for incoming messages from the broker
            try:
                if socket:
                    reply = socket.recv()
                else:
                    logging.info("PllAIProxyImpl - run: no socket connected to the broker, return")
                    return
            except Exception as e:
                logging.exception(e)
                logging.error("PllAIProxyImpl - run: bad recv from internal (broker) socket")
                kThreadRun = False
                loop.stop()
                return
            except:
                kThreadRun = False
                loop.stop()
                return

            # Get the protobuf reply message from its serialized version
            rep_msg = None
            try:
                # All incoming messages from the broker must be reply messages
                rep_msg = optilab_pb2.OptiLabReplyMessage()
                rep_msg.ParseFromString(reply)
            except Exception as e:
                logging.exception(e)
                logging.error("PllAIProxyImpl - run: received invalid message")
                client_websocket.write_message(k_error_message + "Invalid message received")
                continue
            except:
                continue

            if rep_msg.replyStatus != optilab_pb2.ReplyStatus.OK:
                logging.error("PllAIProxyImpl - run: received a non OK status")
                client_websocket.write_message(k_error_message + rep_msg.messageInfo)
                kThreadRun = False
                loop.stop()
                return

            with kCondition:
                # The server received an okay response from the broker.
                # The communication client <-> backend can continue.
                # Add the okay message in the reply set and notify any waiting thread
                replay_set.add(kOKMessageId)
                kCondition.notify()

            msg_type = rep_msg.type
            if msg_type == optilab_pb2.OptiLabReplyMessage.InformationalMessage:
                # Do not perform any action, continue to next read
                if rep_msg.messageInfo.strip():
                    # Communicate informational message to the client, if any
                    logging.info(rep_msg.messageInfo)
                    client_websocket.write_message(k_info_message + rep_msg.messageInfo)
                continue
            elif msg_type == optilab_pb2.OptiLabReplyMessage.ServiceLogoff:
                logging.info("PllAIProxyImpl - run: received a logoff message, shutting down")
                kThreadRun = False
                loop.stop()
                return
            elif msg_type == optilab_pb2.OptiLabReplyMessage.EngineProcessCompleted:
                logging.info("PllAIProxyImpl - run: engine process completed")
                client_websocket.write_message(k_process_completed)
                with kCondition:
                    # The server received an okay response from the broker
                    replay_set.add(kTermMessageId)
                    kCondition.notify()
                pass
            else:
                # Forward the message directly to the client as received from the broker
                client_websocket.write_message(rep_msg.SerializeToString(), binary=True)

        kThreadRun = False
        loop.stop()

    def route_proto_message_to_broker(self, msg):
        opt_model = optimizer_model_pb2.OptimizerModel()
        try:
            opt_model.ParseFromString(msg)
        except Exception as e:
            logging.exception(e)
            logging.error("PllAIProxyImpl - route_proto_message_to_broker: invalid protobuf message")
            return False

        if opt_model.HasField("linear_model"):
            return self.mp_to_broker(opt_model)
        elif opt_model.HasField("routing_model"):
            return self.routing_to_broker(opt_model)
        elif opt_model.HasField("scheduling_model"):
            return self.scheduling_to_broker(opt_model)
        elif opt_model.HasField("evolutionary_model"):
            return self.evolutionary_to_broker(opt_model)
        elif opt_model.HasField("data_model"):
            return self.data_and_ports_to_broker(opt_model)
        elif opt_model.HasField("cp_model"):
            return self.cp_to_broker(opt_model)
        elif opt_model.HasField("deployment_model"):
            deployment_okay = self.deploy_model(opt_model)

            # Communicate the client that the deployment process is complete
            self.web_socket.write_message(k_process_completed)
            return deployment_okay
        else:
            logging.error("PllAIProxyImpl - route_proto_message_to_broker: unrecognized protobuf model")
            return False

    def deploy_model(self, proto_model):
        # deployed_model is a DeploymentModelProto message (see optimizer_model.proto)
        deployed_model = proto_model.deployment_model

        # Get the graph encoded as bytes from the deployed model
        graph_model = deployed_model.graph_model.decode("utf-8")

        # Get the JSON representation of the deployed model
        graph = json.loads(graph_model)
        if DEBUG or True:
            print(graph)

        # return self.deployment_engine.deploy(graph)
        return True

    def mp_to_broker(self, proto_model):
        mp_model = proto_model.linear_model
        pkg_type = mp_model.package_type
        model_type = mp_model.model_format_type

        opt_pkg_type = optilab_pb2.OR_TOOLS
        if pkg_type == linear_model_pb2.LinearModelSpecProto.SCIP:
            opt_pkg_type = optilab_pb2.SCIP

        # Only MIP models for linear models
        opt_class_type = optilab_pb2.CreateOptimizerReq.MIP
        opt_framework_type = optilab_pb2.OR

        if (pkg_type == linear_model_pb2.LinearModelSpecProto.SCIP) or \
           (model_type == linear_model_pb2.LinearModelSpecProto.MPS) or  \
           (model_type == linear_model_pb2.LinearModelSpecProto.CSV):
           opt_framework_type = optilab_pb2.OR_MP

        return build_request_and_forward_to_broker(
            mp_model.model_id,
            opt_class_type,
            opt_pkg_type,
            proto_model,
            opt_framework_type,
            self.socket,
            kCondition,
            self.replay_set,
            kOKMessageId,
            kTermMessageId,
            None)

    def evolutionary_to_broker(self, proto_model):
        opt_req = evolutionary_model_pb2.CreateEvolutionaryOptimizerReq()
        opt_req.evolutionary_engine_type = evolutionary_model_pb2.CreateEvolutionaryOptimizerReq.GENETIC_ALGORITHM
        return build_request_and_forward_to_broker(
            proto_model.evolutionary_model.model_id,
            optilab_pb2.CreateOptimizerReq.EVOLUTIONARY,
            optilab_pb2.OR_TOOLS,
            proto_model,
            optilab_pb2.EVOLUTIONARY,
            self.socket,
            kCondition,
            self.replay_set,
            kOKMessageId,
            kTermMessageId,
            opt_req)

    def scheduling_to_broker(self, proto_model):
        opt_req = scheduling_model_pb2.CreateSchedulingOptimizerReq()
        opt_req.scheduling_engine_type = scheduling_model_pb2.CreateSchedulingOptimizerReq.EMPLOYEE_SCHEDULING
        return build_request_and_forward_to_broker(
            proto_model.scheduling_model.model_id,
            optilab_pb2.CreateOptimizerReq.SCHEDULING,
            optilab_pb2.OR_TOOLS,
            proto_model,
            optilab_pb2.SCHEDULING,
            self.socket,
            kCondition,
            self.replay_set,
            kOKMessageId,
            kTermMessageId,
            opt_req)

    def routing_to_broker(self, proto_model):
        opt_req = routing_model_pb2.CreateRoutingOptimizerReq()
        opt_req.routing_engine_type = routing_model_pb2.CreateRoutingOptimizerReq.CP_VRP
        return build_request_and_forward_to_broker(
            proto_model.routing_model.model_id,
            optilab_pb2.CreateOptimizerReq.VRP,
            optilab_pb2.OR_TOOLS,
            proto_model,
            optilab_pb2.ROUTING,
            self.socket,
            kCondition,
            self.replay_set,
            kOKMessageId,
            kTermMessageId,
            opt_req)

    def data_and_ports_to_broker(self, proto_model):
        opt_req = data_model_pb2.CreateDataOptimizerReq()
        opt_req.data_engine_type = data_model_pb2.CreateDataOptimizerReq.PYTHON_FCN
        return build_request_and_forward_to_broker(
            proto_model.data_model.model_id,
            optilab_pb2.CreateOptimizerReq.DATA_AND_PORTS,
            optilab_pb2.OR_TOOLS,
            proto_model,
            optilab_pb2.DATA_AND_PORTS,
            self.socket,
            kCondition,
            self.replay_set,
            kOKMessageId,
            kTermMessageId,
            opt_req)

    def cp_to_broker(self, proto_model):
        opt_req = constraint_model_pb2.CreateCPOptimizerReq()
        opt_req.cp_engine_type = constraint_model_pb2.CreateCPOptimizerReq.ORTOOLS_CP_SAT
        return build_request_and_forward_to_broker(
            proto_model.cp_model.model_id,
            optilab_pb2.CreateOptimizerReq.CONSTRAINT_PROGRAMMING,
            optilab_pb2.OR_TOOLS,
            proto_model,
            optilab_pb2.CONSTRAINT_PROGRAMMING,
            self.socket,
            kCondition,
            self.replay_set,
            kOKMessageId,
            kTermMessageId,
            opt_req)

