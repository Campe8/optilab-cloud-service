from routing.tasks import tsp
from routing.tasks import vrp

from routing.tasks.tsp import TSPDataset
from routing.tasks.vrp import VehicleRoutingDataset


import torch
from torch.utils.data import DataLoader


import json
import sys
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

class Optimizer:


    def set_task(self, task):
        try:
            self.task = task
            self.actor = None
            self.critic = None
            self.data_loader = None
            self.reward_fn = None
            self.render_fn = None
            return True
        except:
            print(sys.exc_info()[0])
            return False


    def __str__(self):
        print("Call Working - task type %s" %(self.task))


    def load_models(self, args):

        args_json = json.loads(args)
        print("Loading model with parameters:\n%s" %(args_json))

        try:
            actor = torch.load(args_json['actor_path']).to(device)

            self.actor = actor
            return True
        except:
            print(sys.exc_info())
            return False


    def set_problem(self, args):

        args_json = json.loads(args)
        print("Setting problem description for %s:\n%s" %(self.task, args_json))

        try:
            reward_fn, render_fn = None, None
            if self.task == 'tsp':

                problems_json = json.load(open(args_json['problem_desc'], "r"))
                num_problems = len(problems_json["problems"])
                num_cities = len(problems_json["problems"][0]["x"])

                dataset = TSPDataset(size=num_cities, num_samples=num_problems, seed=None, problems_json=problems_json)
                reward_fn = tsp.reward
                render_fn = tsp.render


            elif self.task == 'vrp':

                problems_json = json.load(open(args_json['problem_desc'], "r"))
                max_load = problems_json["max_load"]
                max_demand = problems_json["max_demand"]
                num_samples = len(problems_json["problems"])
                num_locations = len(problems_json["problems"][0]["x"])

                dataset = VehicleRoutingDataset(num_samples=num_samples, input_size=num_locations,
                                                max_load=max_load, max_demand=max_demand, seed=None,
                                                problems_json=problems_json)
                reward_fn = vrp.reward
                render_fn = vrp.render

            data_loader = DataLoader(dataset, 2, False, num_workers=0)

            self.reward_fn = reward_fn
            self.render_fn = render_fn
            self.data_loader = data_loader

            return True
        except:
            print(sys.exc_info())
            return False



    def evaluate(self):

        # print("Evaluating solutions...")
        # try:
            rewards = []
            solutions = []
            for batch_idx, batch in enumerate(self.data_loader):

                static, dynamic, x0 = batch

                static = static.to(device)
                dynamic = dynamic.to(device)
                x0 = x0.to(device) if len(x0) > 0 else None

                with torch.no_grad():
                    tour_indices, _ = self.actor.forward(static, dynamic, x0)

                r = self.reward_fn(static, tour_indices).tolist()
                rewards.extend(r)

                tour_indices = tour_indices.detach().numpy()
                for i in range(tour_indices.shape[0]):
                    sol = {"tour": tour_indices[i].tolist(), "reward": r[i]}
                    solutions.append(sol)

            json_result = json.dumps(solutions)

            return json_result

        # except:
        #     print(sys.exc_info())
        #     return ""



    '''
    json options
    seed: int - random number seed
    num_nodes: int - number of nodes
    actor_lr: float - actor learning rate
    critic_lr: float - critic learning rate
    max_grad_norm: float - maximum norm
    batch_size: int - batch size
    hidden_size: int - hidden size
    dropout: float - dropout prob
    num_layers: int - number of layers
    train-size: int - number of training instances
    valid-size: int - number of validation instances
    '''

    def create_trainer(self, args):

        try:
            trainer = None
            if self.task == 'tsp':
                trainer = TSPTrainer()
            elif self.task == 'vrp':
                trainer = VRPTrainer()

            trainer.train(args)

            return True
        except:
            print(sys.exc_info())
            return False
