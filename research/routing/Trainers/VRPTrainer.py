from routing.Trainers.Trainer import *

from routing.tasks import vrp
from routing.tasks.vrp import VehicleRoutingDataset

from routing.Models.model import CombinatorialSolver
from routing.Models.CriticModels import StateCritic


class VRPTrainer(Trainer):

    # Determines the maximum amount of load for a vehicle based on num nodes
    MAX_DEMAND = 9
    MAX_LOAD = 20
    STATIC_SIZE = 2  # (x, y)
    DYNAMIC_SIZE = 2  # (load, demand)

    def init_models(self, **kwargs):
        print(kwargs)
        self.MAX_DEMAND = kwargs['max_demand']
        self.MAX_LOAD = kwargs['max_load']

        self.train_data = VehicleRoutingDataset(kwargs['train_size'], kwargs['num_nodes'],
                                           self.MAX_LOAD, kwargs['max_demand'], kwargs['seed'])

        self.valid_data = VehicleRoutingDataset(kwargs['valid_size'], kwargs['num_nodes'],
                                           self.MAX_LOAD, kwargs['max_demand'],
                                           kwargs['seed'] + 1)

        self.actor = CombinatorialSolver(self.STATIC_SIZE, self.DYNAMIC_SIZE,
                                    kwargs['hidden_size'], self.train_data.update_dynamic,
                                    self.train_data.update_mask, kwargs['num_layers'], kwargs['dropout']).to(device)

        self.critic = StateCritic(self.STATIC_SIZE, self.DYNAMIC_SIZE, kwargs['hidden_size']).to(device)



    def train(self, args):

        # Goals from paper:
        # VRP10, Capacity 20:  4.84  (Greedy)
        # VRP20, Capacity 30:  6.59  (Greedy)
        # VRP50, Capacity 40:  11.39 (Greedy)
        # VRP100, Capacity 50: 17.23  (Greedy)

        kwargs = vars(args)
        kwargs['train_data'] = self.train_data
        kwargs['valid_data'] = self.valid_data
        kwargs['reward_fn'] = vrp.reward
        kwargs['render_fn'] = vrp.render


        if not args.test:
            super().train(**kwargs)

        test_data = VehicleRoutingDataset(kwargs['valid_size'], kwargs['num_nodes'],
                            self.MAX_LOAD, self.MAX_DEMAND, kwargs['seed'] + 2)

        test_dir = 'test'
        test_loader = DataLoader(test_data, kwargs['batch_size'], False, num_workers=0)
        out = self.eval(test_loader, vrp.reward, vrp.render, test_dir, num_plot=5)

        print('Average tour length: ', out)

