
from routing.Trainers.Trainer import *
from routing.tasks import tsp
from routing.tasks.tsp import TSPDataset

from routing.Models.model import CombinatorialSolver
from routing.Models.CriticModels import StateCritic


class TSPTrainer(Trainer):
    STATIC_SIZE = 2  # (x, y)
    DYNAMIC_SIZE = 1  # dummy for compatibility

    def init_models(self, **kwargs):
        update_fn = None
        self.actor = CombinatorialSolver(self.STATIC_SIZE, self.DYNAMIC_SIZE, kwargs['hidden_size'], update_fn, tsp.update_mask,
                                    kwargs['num_layers'], kwargs['dropout']).to(device)
        self.critic = StateCritic(self.STATIC_SIZE, self.DYNAMIC_SIZE, kwargs['hidden_size']).to(device)

    def train(self, args):

        # Goals from paper:
        # TSP20, 3.97
        # TSP50, 6.08
        # TSP100, 8.44

        train_data = TSPDataset(args.num_nodes, args.train_size, args.seed)
        valid_data = TSPDataset(args.num_nodes, args.valid_size, args.seed + 1)


        kwargs = vars(args)
        kwargs['train_data'] = train_data
        kwargs['valid_data'] = valid_data
        kwargs['reward_fn'] = tsp.reward
        kwargs['render_fn'] = tsp.render

        if args.test:
            super().train(**kwargs)

        test_data = TSPDataset(args.num_nodes, args.train_size, args.seed + 2)

        test_dir = 'test'
        test_loader = DataLoader(test_data, args.batch_size, False, num_workers=0)
        out = self.eval(test_loader, tsp.reward, tsp.render, test_dir, num_plot=5)

        print('Average tour length: ', out)
