#pragma once
#include "Python.h"
#include <iostream>

class RLRouting {
    public:
        RLRouting();
        ~RLRouting();

        bool createOptimizer(char* problem_type);
        bool loadModel(char* json);
        bool setProblem(char* json);
        bool forceTermination();
        std::string getSolution(char* json_path);

        PyObject *optimizer_instance;
        PyObject *optimizer_module;


};