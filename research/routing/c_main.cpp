#include <stdio.h>
#include <iostream>
#include "Python.h"
#include "c_wrapper.hpp"


//For compiling need to point to python-config for version of python you want to use.
//Had trouble using python 2.7 versions 11 and above. No problem with python 2.7.10.
//Need to figure out why that is, something in the python build changed.

//g++ $(python-config --cflags) $(python-config --ldflags) c_wrapper.hpp c_wrapper.cpp c_main.cpp

//Example compiling in my machine.
//g++ $(~/miniconda3/envs/python2env/bin/python2-config --cflags) $(~/miniconda3/envs/python2env/bin/python2-config --ldflags) c_wrapper.hpp c_wrapper.cpp c_main.cpp


int main(int argc, char *argv[])
{

	Py_Initialize();

	RLRouting routing = RLRouting();
	bool create_success = routing.createOptimizer((char*)"tsp");

	if (create_success) {
        char* json_actor = "{ \"actor_path\" : \"./tsp/20/09_31_21.494246/actor.pt\"}";
        bool actor_load_success = routing.loadModel(json_actor);

        char* json_problem = "{ \"problem_desc\" : \"./ProblemDescriptions/tsp_example.json\"}";
        bool problem_load_success = routing.setProblem(json_problem);

        if (actor_load_success && problem_load_success) {
            char* output_json = "./output.json";
            std::string s = routing.getSolution(output_json);
        }
	}


	Py_Finalize();
	return 0;
}