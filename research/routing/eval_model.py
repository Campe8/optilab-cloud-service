from routing.tasks import tsp
from routing.tasks import vrp

from routing.Models.model import CombinatorialSolver

from routing.tasks.tsp import TSPDataset
from routing.tasks.vrp import VehicleRoutingDataset

import argparse
import numpy as np
import os

import torch
from torch.utils.data import DataLoader
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
import json


def load_models(args):

    reward_fn, render_fn = None, None
    if args.task == 'tsp':
        STATIC_SIZE = 2  # (x, y)
        DYNAMIC_SIZE = 1  # dummy for compatibility
        update_fn = None


        problems_json = json.load( open(args.problem_desc, "r") )
        num_problems = len(problems_json["problems"])
        num_cities = len(problems_json["problems"][0]["x"])


        dataset = TSPDataset(size=num_cities, num_samples=num_problems, seed=None, problems_json=problems_json)
        reward_fn = tsp.reward
        render_fn = tsp.render

        actor = CombinatorialSolver(STATIC_SIZE, DYNAMIC_SIZE, args.hidden_size, update_fn,
                    tsp.update_mask, args.num_layers, args.dropout).to(device)


    elif args.task == 'vrp':
        STATIC_SIZE = 2  # (x, y)
        DYNAMIC_SIZE = 2  # (load, demand)

        problems_json = json.load( open(args.problem_desc, "r") )

        max_load = problems_json["max_load"] #LOAD_DICT[args.num_nodes]
        max_demand = problems_json["max_demand"]
        num_samples = len(problems_json["problems"])
        num_locations = len(problems_json["problems"][0]["x"])

        dataset = VehicleRoutingDataset(num_samples=num_samples, input_size=num_locations,
                                        max_load=max_load, max_demand=max_demand, seed=None, problems_json=problems_json)
        reward_fn = vrp.reward
        render_fn = vrp.render

        actor = CombinatorialSolver(STATIC_SIZE, DYNAMIC_SIZE, args.hidden_size, dataset.update_dynamic,
                    dataset.update_mask, args.num_layers, args.dropout).to(device)

    actor.load_state_dict(torch.load(args.actor_path, device))

    data_loader = DataLoader(dataset, 2, False, num_workers=0)

    return actor, data_loader, reward_fn, render_fn


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Combinatorial Optimization')
    parser.add_argument('--task', default='tsp', help="Task type [tsp or vrp]")
    parser.add_argument('--actor', dest='actor_path', type=str, required=True, help="path to saved actor model")
    parser.add_argument('--save_dir', dest='save_dir', default="Evaluation",
                        type=str, help="directory where to save visualization of solutions")

    parser.add_argument('--hidden', dest='hidden_size', default=128, type=int)
    parser.add_argument('--dropout', default=0.1, type=float)
    parser.add_argument('--layers', dest='num_layers', default=1, type=int)
    parser.add_argument('--problem_desc', required=True, type=str, help="input json with problem description")
    parser.add_argument('--output_json', required=True, type=str, help="path where output json with solutions will be stored")

    num_plot = 5

    args = parser.parse_args()
    actor, data_loader, reward_fn, render_fn = load_models(args)

    if os.path.isdir(args.save_dir) == False:
        os.mkdir(args.save_dir)

    rewards = []
    solutions = []
    for batch_idx, batch in enumerate(data_loader):

        static, dynamic, x0 = batch

        static = static.to(device)
        dynamic = dynamic.to(device)
        x0 = x0.to(device) if len(x0) > 0 else None

        with torch.no_grad():
            tour_indices, _ = actor.forward(static, dynamic, x0)


        r = reward_fn(static, tour_indices).tolist()
        rewards.extend( r )

        if render_fn is not None and batch_idx < num_plot:
            name = 'batch%d_%2.4f.png' % (batch_idx, np.mean(r))
            path = os.path.join(args.save_dir, name)
            render_fn(static, tour_indices, path)

        tour_indices = tour_indices.detach().numpy()
        for i in range(tour_indices.shape[0]):
            sol = { "tour" : tour_indices[i].tolist(), "reward" : r[i] }
            solutions.append(sol)



    json.dump( solutions, open(args.output_json, "w") )
    print("Tour Rewards:")
    print(str(rewards))
