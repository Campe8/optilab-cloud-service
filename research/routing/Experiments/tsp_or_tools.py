"""Simple travelling salesman problem between cities."""

from __future__ import print_function
from ortools.constraint_solver import routing_enums_pb2
from ortools.constraint_solver import pywrapcp

from routing.Optimizer import Optimizer
import torch
import json
import sys
import time
import matplotlib.pyplot as plt

from sklearn.metrics import pairwise_distances

import numpy as np

def create_data_model(num_cities):
    """Stores the data for the problem."""

    cities = np.random.uniform(0.0, 10.0, (num_cities, 2))

    distances = pairwise_distances(cities)

    data = {}
    data['cities'] = cities
    data['distance_matrix'] = distances
    # data['distance_matrix'] = [
    #     [0, 2451, 713, 1018, 1631, 1374, 2408, 213, 2571, 875, 1420, 2145, 1972],
    #     [2451, 0, 1745, 1524, 831, 1240, 959, 2596, 403, 1589, 1374, 357, 579],
    #     [713, 1745, 0, 355, 920, 803, 1737, 851, 1858, 262, 940, 1453, 1260],
    #     [1018, 1524, 355, 0, 700, 862, 1395, 1123, 1584, 466, 1056, 1280, 987],
    #     [1631, 831, 920, 700, 0, 663, 1021, 1769, 949, 796, 879, 586, 371],
    #     [1374, 1240, 803, 862, 663, 0, 1681, 1551, 1765, 547, 225, 887, 999],
    #     [2408, 959, 1737, 1395, 1021, 1681, 0, 2493, 678, 1724, 1891, 1114, 701],
    #     [213, 2596, 851, 1123, 1769, 1551, 2493, 0, 2699, 1038, 1605, 2300, 2099],
    #     [2571, 403, 1858, 1584, 949, 1765, 678, 2699, 0, 1744, 1645, 653, 600],
    #     [875, 1589, 262, 466, 796, 547, 1724, 1038, 1744, 0, 679, 1272, 1162],
    #     [1420, 1374, 940, 1056, 879, 225, 1891, 1605, 1645, 679, 0, 1017, 1200],
    #     [2145, 357, 1453, 1280, 586, 887, 1114, 2300, 653, 1272, 1017, 0, 504],
    #     [1972, 579, 1260, 987, 371, 999, 701, 2099, 600, 1162, 1200, 504, 0],
    # ]  # yapf: disable
    data['num_vehicles'] = 1
    data['depot'] = 0
    return data


def print_solution(manager, routing, assignment, data):
    """Prints assignment on console."""
    print('Objective: {} miles'.format(assignment.ObjectiveValue()))
    index = routing.Start(0)
    plan_output = 'Route for vehicle 0:\n'
    route_distance = 0
    indices = []
    while not routing.IsEnd(index):
        plan_output += ' {} ->'.format(manager.IndexToNode(index))
        previous_index = index
        index = assignment.Value(routing.NextVar(index))
        indices.append(index)
        route_distance += data['distance_matrix'][ manager.IndexToNode(previous_index), manager.IndexToNode(index) ]
        # route_distance += routing.GetArcCostForVehicle(previous_index, index, 0)
    plan_output += ' {}\n'.format(manager.IndexToNode(index))
    plan_output += 'Route distance: %.2f miles\n' %(route_distance)
    print(plan_output)
    return route_distance


if __name__ == '__main__':
    """Entry point of the program."""
    # Instantiate the data problem.

    actor_path = None if len(sys.argv) < 2 else sys.argv[1]

    num_trials = 5


    rl_runtime, or_runtime, rl_only_runtime = [], [], []
    rl_distance, or_distance, rl_only_distance = [], [], []

    for k in range(num_trials):

        data = create_data_model(num_cities=500)

        def distance_callback(from_index, to_index):
            """Returns the distance between the two nodes."""
            # Convert from routing variable Index to distance matrix NodeIndex.
            from_node = manager.IndexToNode(from_index)
            to_node = manager.IndexToNode(to_index)
            dist =  data['distance_matrix'][from_node,to_node]
            return dist


        num_tests = 1 if actor_path is None else 2
        for i in range(num_tests):
            s = time.time()
            if i == 1:
                optimizer = Optimizer()
                optimizer.set_task(task="tsp")
                actor_json = "{ \"actor_path\" : \"%s\" }" %(actor_path)
                optimizer.load_models( actor_json )

                problem_description = { "problems" : [ { "x" : data['cities'][:,0].tolist(), "y" : data['cities'][:,1].tolist() } ] }

                json.dump(problem_description, open("problem.json","w"))

                optimizer.set_problem( "{ \"problem_desc\" : \"problem.json\" }" )
                s = time.time()
                result = json.loads( optimizer.evaluate() )
                rl_only_runtime.append( time.time() - s )
                rl_only_distance.append( result[0]['reward'] )
                data[ 'initial_routes' ] = [ result[0]["tour"] ]
                print(result[0]['reward'])
                s = time.time()

            # Create the routing index manager.
            manager = pywrapcp.RoutingIndexManager(len(data['distance_matrix']), data['num_vehicles'], data['depot'])

            # Create Routing Model.
            routing = pywrapcp.RoutingModel(manager)


            transit_callback_index = routing.RegisterTransitCallback(distance_callback)

            # Define cost of each arc.
            routing.SetArcCostEvaluatorOfAllVehicles(transit_callback_index)

            # Setting first solution heuristic.
            search_parameters = pywrapcp.DefaultRoutingSearchParameters()
            search_parameters.time_limit.seconds = 1
            if i == 1:
                initial_solution = routing.ReadAssignmentFromRoutes(data['initial_routes'], True)
                assignment = routing.SolveFromAssignmentWithParameters(initial_solution, search_parameters)
            else:
                search_parameters.first_solution_strategy = (routing_enums_pb2.FirstSolutionStrategy.PATH_CHEAPEST_ARC)
                # Solve the problem.
                assignment = routing.SolveWithParameters(search_parameters)

            # Print solution on console.
            # if assignment:
            distance = print_solution(manager, routing, assignment, data)


            if i == 0:
                or_runtime.append( time.time() - s )
                or_distance.append( distance )
            else:
                rl_runtime.append( time.time() - s)
                rl_distance.append( distance )



    print("OR: %.3f - %.3f\nRL: %.3f - %.3f" %(np.mean(or_runtime), np.std(or_runtime), np.mean(rl_runtime), np.std(rl_runtime)))
    print("OR: %.3f - %.3f\nRL: %.3f - %.3f" %(np.mean(or_distance), np.std(or_distance), np.mean(rl_distance), np.std(rl_distance)))

    labels = [ "OR", "RL"]
    positions = np.arange(len(labels))


    plt.bar(labels, [ np.mean(or_runtime), np.mean(rl_runtime) ],
           yerr=[ np.std(or_runtime), np.std(rl_runtime)], color=["black", "red"], alpha=0.5)
    plt.ylabel( "Run time" )
    plt.savefig("runtime_results.png")

    plt.clf()
    plt.bar(labels, [np.mean(or_distance), np.mean(rl_distance)],
            yerr=[np.std(or_distance), np.std(rl_distance)], color=["black", "red"], alpha=0.5)
    plt.ylabel("Tour Length")
    plt.savefig("distance_results.png")