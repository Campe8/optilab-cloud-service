#include "c_wrapper.hpp"
#include "Python.h"

RLRouting::RLRouting()
{
    PyObject *module_name = PyString_FromString("Optimizer");
    PyObject *module = PyImport_Import(module_name);
    PyObject *dict = PyModule_GetDict(module);
    this->optimizer_module = PyDict_GetItemString(dict, "Optimizer");
}

RLRouting::~RLRouting() {}

bool RLRouting::createOptimizer(char* problem_type)
{

    this->optimizer_instance = PyObject_CallObject(this->optimizer_module, NULL);
    PyObject *result = PyObject_CallMethod(this->optimizer_instance, (char*) "set_task", (char*)"(s)", problem_type);
    PyObject_CallMethod(this->optimizer_instance, (char*) "__str__", NULL);
    bool success = PyObject_IsTrue(result);

    std::cout << "Create optimizer - " << success << std::endl;

    return success;
}


bool RLRouting::loadModel(char* json)
{

    PyObject *result = PyObject_CallMethod(this->optimizer_instance, (char*) "load_models", (char*)"(s)", json);
    bool success = PyObject_IsTrue(result);
    std::cout << "Loading models - " << success << std::endl;

    return success;

}

bool RLRouting::setProblem(char* json)
{

    PyObject *result = PyObject_CallMethod(this->optimizer_instance, (char*) "set_problem", (char*)"(s)", json);
    bool success = PyObject_IsTrue(result);
    std::cout << "Setting problem - " << success << std::endl;

    return success;

}

bool RLRouting::forceTermination()
{

    return true;

}

std::string RLRouting::getSolution(char* json_path)
{
    PyObject *result = PyObject_CallMethod(this->optimizer_instance, (char*) "evaluate", NULL);
    const char* solution = PyString_AsString(result);
    std::cout << "Solution: " << solution << std::endl;

    return solution;
}
