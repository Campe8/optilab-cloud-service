from routing.Trainers.TSPTrainer import TSPTrainer
from routing.Trainers.VRPTrainer import VRPTrainer


import argparse

def create_trainer(args):

    trainer = None
    load_model = args.actor_path is not None and args.critic_path is not None
    if args.task == 'tsp':
        trainer = TSPTrainer()
        if load_model is False:
            trainer.init_models(hidden_size=args.hidden_size, num_layers=args.hidden_size, dropout=args.dropout)

    elif args.task == 'vrp':
        trainer = VRPTrainer()
        if load_model is False:
            trainer.init_models(num_nodes=args.num_nodes, train_size=args.train_size, seed=args.seed, valid_size=args.valid_size, dropout=args.dropout,
                                hidden_size=args.hidden_size, num_layers=args.num_layers, max_load=args.max_load, max_demand=args.max_demand)

    if load_model is True:
        trainer.load_models(actor_path=args.actor_path, critic_path=args.critic_path)


    return trainer


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Combinatorial Optimization')
    parser.add_argument('--seed', default=12345, type=int)
    parser.add_argument('--actor_path', default=None)
    parser.add_argument('--critic_path', default=None)
    parser.add_argument('--test', action='store_true', default=False)
    parser.add_argument('--task', default='tsp')
    parser.add_argument('--nodes', dest='num_nodes', default=20, type=int)
    parser.add_argument('--actor_lr', default=5e-4, type=float)
    parser.add_argument('--critic_lr', default=5e-4, type=float)
    parser.add_argument('--max_grad_norm', default=2., type=float)
    parser.add_argument('--batch_size', default=256, type=int)
    parser.add_argument('--hidden', dest='hidden_size', default=128, type=int)
    parser.add_argument('--dropout', default=0.1, type=float)
    parser.add_argument('--layers', dest='num_layers', default=1, type=int)
    parser.add_argument('--train_size',default=1000, type=int)
    parser.add_argument('--valid_size', default=100, type=int)
    parser.add_argument('--max_load', default=20, type=int)
    parser.add_argument('--max_demand', default=10, type=int)


    args = parser.parse_args()

    trainer = create_trainer(args)
    assert trainer is not None, "Training problem not recognized: %s" %(args.task)

    print(args)
    trainer.train(args)
