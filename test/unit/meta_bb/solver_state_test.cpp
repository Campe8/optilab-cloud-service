#include <cstdint>  // for uint64_t
#include <limits>   // for std::numeric_limits
#include <memory>   // for std::shared_ptr

#include <gtest/gtest.h>

#include "meta_bb_mocks.hpp"
#include "meta_bb/solver_state.hpp"

TEST(SolverState, Default_Constructor_And_Get_Methods_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  SolverState::SPtr state;
  ASSERT_NO_THROW(state = std::make_shared<MockSolverState>());
  EXPECT_FALSE(state->isRacingStage());
  EXPECT_EQ(0, state->getNotificationId());
  EXPECT_EQ(-1, state->getLMId());
  EXPECT_EQ(-1, state->getGlobalSubtreeId());
  EXPECT_DOUBLE_EQ(0.0, state->getSolverLocalBestDualBoundValue());
  EXPECT_DOUBLE_EQ(std::numeric_limits<double>::max(), state->getGlobalBestPrimalBoundValue());
  EXPECT_EQ(0, state->getNumNodesSolved());
  EXPECT_EQ(-1, state->getNumNodesLeft());
  EXPECT_EQ(0, state->getTimeMsec());
  EXPECT_DOUBLE_EQ(0.0, state->getAverageDualBoundGainRecv());
}  // Default_Constructor_And_Get_Methods_No_Throw

TEST(SolverState, Constructor_And_Get_Methods_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool racingStage{true};
  const unsigned int notificationId{1};
  const int lmId{2};
  const int globalSubtreeId{3};
  const long long numSolvedNodes{4};
  const int numNodesLeft{5};
  const double bestDualBoundValue{-1.0};
  const double globalBestPrimalBoundValue{100.2};
  const uint64_t timeMsec{1234};
  const double avgDualBoundGainRecv{3.14};

  SolverState::SPtr state;
  ASSERT_NO_THROW(state = std::make_shared<MockSolverState>(
          racingStage,
          notificationId,
          lmId,
          globalSubtreeId,
          numSolvedNodes,
          numNodesLeft,
          bestDualBoundValue,
          globalBestPrimalBoundValue,
          timeMsec,
          avgDualBoundGainRecv));
  EXPECT_TRUE(state->isRacingStage());
  EXPECT_EQ(notificationId, state->getNotificationId());
  EXPECT_EQ(lmId, state->getLMId());
  EXPECT_EQ(globalSubtreeId, state->getGlobalSubtreeId());
  EXPECT_DOUBLE_EQ(bestDualBoundValue, state->getSolverLocalBestDualBoundValue());
  EXPECT_DOUBLE_EQ(globalBestPrimalBoundValue, state->getGlobalBestPrimalBoundValue());
  EXPECT_EQ(numSolvedNodes, state->getNumNodesSolved());
  EXPECT_EQ(numNodesLeft, state->getNumNodesLeft());
  EXPECT_EQ(timeMsec, state->getTimeMsec());
  EXPECT_DOUBLE_EQ(avgDualBoundGainRecv, state->getAverageDualBoundGainRecv());
}  // Constructor_And_Get_Methods_No_Throw
