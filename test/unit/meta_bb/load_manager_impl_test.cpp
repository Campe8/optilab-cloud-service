#include <memory>     // for std::unique_ptr
#include <utility>    // for std::move
#include <vector>

#include <gtest/gtest.h>

#include "meta_bb_mocks.hpp"
#include "meta_bb/framework.hpp"
#include "meta_bb/initiator.hpp"
#include "meta_bb/load_manager_impl.hpp"
#include "meta_bb/meta_bb_struct.hpp"
#include "meta_bb/meta_bb_utilities.hpp"
#include "meta_bb/node_pool.hpp"
#include "meta_bb/paramset.hpp"
#include "optimizer_network/packet.hpp"
#include "utilities/timer.hpp"

class LoadManagerImplFixture : public testing::Test {
 protected:
  void SetUp() override
  {
    using namespace optilab;
    using namespace metabb;

    // Instantiate a new load manager
    pLM = std::make_shared<optilab::metabb::MockLoadManagerFixture>();
    ASSERT_NO_THROW(pMockLoadManagerImpl = std::make_shared<MockLoadManagerImpl>(pLM->getLM()));
    ASSERT_NO_THROW(pLoadManagerImpl = std::make_shared<LoadManagerImpl>(pLM->getLM()));
  }

  inline void setRacingRampup() noexcept
  {
    getMockLMImpl()->setRacingRampup();
    getLMImpl()->setRacingRampup();
  }

  inline optilab::metabb::MockLoadManagerFixture* getLM() const noexcept { return pLM.get(); }
  inline optilab::metabb::NodePool::SPtr getNodePool() const { return getLM()->getNodePool(); }

  inline optilab::metabb::MockLoadManagerImpl::SPtr getMockLMImpl() const noexcept
  {
    return pMockLoadManagerImpl;
  }

  inline optilab::metabb::LoadManagerImpl::SPtr getLMImpl() const noexcept
  {
    return pLoadManagerImpl;
  }

  optilab::optnet::MockNetworkCommunicator::SPtr getComm() const noexcept
  {
    return getLM()->getComm();
  }

  template<typename T>
  optilab::optnet::Packet::UPtr createPacket(
          optilab::metabb::net::MetaBBPacketTag::PacketTagType tag,
          const T& data)
  {
    using namespace optilab;
    using namespace metabb;

    optnet::Packet::UPtr packet(new optnet::Packet());
    packet->packetTag = net::MetaBBPacketTag::UPtr(new net::MetaBBPacketTag(tag));
    packet->networkPacket =
            typename optnet::NetworkPacket<T>::UPtr(new optnet::NetworkPacket<T>(data));
    return std::move(packet);
  }

  template<typename T>
  optilab::optnet::Packet::UPtr createListPacket(
          optilab::metabb::net::MetaBBPacketTag::PacketTagType tag,
          const std::vector<T>& data)
  {
    using namespace optilab;
    using namespace metabb;

    optnet::Packet::UPtr packet(new optnet::Packet());
    packet->packetTag = net::MetaBBPacketTag::UPtr(new net::MetaBBPacketTag(tag));
    packet->networkPacket =
            typename optnet::NetworkPacket<T>::UPtr(new optnet::NetworkPacket<T>(data));
    return std::move(packet);
  }

  void executeMessage(optilab::optnet::Packet::UPtr packet, int source, bool useMock=true)
  {
    using namespace optilab;
    using namespace metabb;

    // Check if the received packet has a tag that is supported
    net::MetaBBPacketTag::PacketTagType tag;
    ASSERT_NO_THROW(tag = utils::castToMetaBBPacketTagAndGetTagOrThrow(packet->packetTag));

    if (useMock)
    {
      ASSERT_TRUE(getMockLMImpl()->isHandlerSupported(tag));

      // Callback function
      ASSERT_NO_THROW((*pMockLoadManagerImpl)(source, std::move(packet)));
    }
    else
    {
      ASSERT_TRUE(getLMImpl()->isHandlerSupported(tag));

      // Callback function
      ASSERT_NO_THROW((*pLoadManagerImpl)(source, std::move(packet)));
    }
  }

 private:
  // Mock load manager implementation
  optilab::metabb::MockLoadManagerImpl::SPtr pMockLoadManagerImpl;

  // Standard load manager implementation
  optilab::metabb::LoadManagerImpl::SPtr pLoadManagerImpl;

  // Mock Load manager
  optilab::metabb::MockLoadManagerFixture::SPtr pLM;
};

TEST_F(LoadManagerImplFixture, Mock_Tag_Node_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{true};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PTT_NODE;

  EXPECT_EQ(getMockLMImpl()->tagNode(), 0);
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);
  EXPECT_EQ(getMockLMImpl()->tagNode(), 1);
}  // Mock_Tag_Node_No_Throw

TEST_F(LoadManagerImplFixture, Tag_Node_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{false};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PTT_NODE;

  // The communicator hasn't sent anything yet
  EXPECT_FALSE(getComm()->lastSend);

  // Before receiving the packet, the node pool is empty
  EXPECT_EQ(0, getNodePool()->getNumNodes());
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);
  EXPECT_EQ(1, getLM()->getTermState().numNodesReceived) << "One node has been received";
  EXPECT_EQ(1, getNodePool()->getNumNodes()) << "The received node should be inserted "
          "in the node pool";

  // Check that the "NODE_RECEIVED" tag has been sent to the sender
  EXPECT_TRUE(getComm()->lastSend);
  EXPECT_EQ(getComm()->nodeId, source);
  EXPECT_EQ(getComm()->lastTag, net::MetaBBPacketTag::PacketTagType::PPT_NODE_RECEIVED);

  // Get the newly received node from the node pool
  Node::SPtr recvNode;
  EXPECT_NO_THROW(recvNode = getNodePool()->extractNode());
  ASSERT_TRUE(recvNode);
  EXPECT_TRUE(recvNode->getParent());
  EXPECT_TRUE(std::dynamic_pointer_cast<NodeGenealogicalLocalRel>(
          recvNode->getParent())->hasValue());
}  // Tag_Node_No_Throw

TEST_F(LoadManagerImplFixture, Mock_Tag_Solution_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{true};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_SOLUTION;

  EXPECT_EQ(getMockLMImpl()->tagSolution(), 0);
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);
  EXPECT_EQ(getMockLMImpl()->tagSolution(), 1);
}  // Mock_Tag_Solution_No_Throw

TEST_F(LoadManagerImplFixture, Tag_Solution_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{false};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_SOLUTION;

  getLM()->getParamSet()->getDescriptor().set_noupperboundtransferinracing(false);
  EXPECT_FALSE(getComm()->lastSend);
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);
  EXPECT_TRUE(getComm()->lastSend);
}  // Tag_Solution_No_Throw

TEST_F(LoadManagerImplFixture, Mock_Tag_Solver_State_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{true};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_SOLVER_STATE;

  EXPECT_EQ(getMockLMImpl()->tagSolverState(), 0);
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);
  EXPECT_EQ(getMockLMImpl()->tagSolverState(), 1);
}  // Mock_Tag_Solver_State_No_Throw

TEST_F(LoadManagerImplFixture, Tag_Solver_State_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{false};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_SOLVER_STATE;

  getLM()->getParamSet()->getDescriptor().set_noupperboundtransferinracing(false);
  EXPECT_FALSE(getComm()->lastSend);
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);

  // Messages sent:
  // (0 - PPT_OUT_COLLECTING_MODE during "updateSolverStatus(...)")
  // 1 - PPT_LM_BEST_BOUND_VALUE for updating the dual bound
  // 2 - PPT_NOTIFICATION_ID for the notification id
  // (3 - PPT_BREAKING if breaking is not finished and phase is normal)
  EXPECT_TRUE(getComm()->lastSend);
  EXPECT_EQ(getComm()->numSend, 2);
}  // Tag_Solver_State_No_Throw

TEST_F(LoadManagerImplFixture, Tag_Solver_State_Set_Init_Nodes_Gen_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{false};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_SOLVER_STATE;

  getLM()->getParamSet()->getDescriptor().set_noupperboundtransferinracing(false);
  getLM()->getParamSet()->getDescriptor().set_initialnodesgeneration(true);
  getLM()->getParamSet()->getDescriptor().set_numinitialnodes(0);
  EXPECT_FALSE(getComm()->lastSend);
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);

  // Messages sent:
  // (0 - PPT_OUT_COLLECTING_MODE during "updateSolverStatus(...)")
  // 1 - PPT_LM_BEST_BOUND_VALUE for updating the dual bound
  // 2 - PPT_NOTIFICATION_ID for the notification id
  // (3 - PPT_BREAKING if breaking is not finished and phase is normal)
  // 4 - PPT_COLLECT_ALL_NODES if initial nodes generation is set: it is.
  //     Sends one message per solver
  EXPECT_TRUE(getComm()->lastSend);
  EXPECT_EQ(getComm()->numSend, 2 + getLM()->getSolverPool()->getNumSolvers());
}  // Tag_Solver_State_Set_Init_Nodes_Gen_No_Throw

TEST_F(LoadManagerImplFixture, Mock_Tag_Completion_Of_Calculation_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{true};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_COMPLETION_OF_CALCULATION;

  EXPECT_EQ(getMockLMImpl()->tagCompletionOfCalculation(), 0);
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);
  EXPECT_EQ(getMockLMImpl()->tagCompletionOfCalculation(), 1);
}  // Mock_Tag_Completion_Of_Calculation_No_Throw

TEST_F(LoadManagerImplFixture, Tag_Completion_Of_Calculation_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{false};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_COMPLETION_OF_CALCULATION;

  // The number of nodes solved in the solver pool should be zero
  EXPECT_EQ(getLM()->getSolverPool()->getNumNodesSolvedInSolvers(), 0);

  // The number of inactive solvers should be zero
  const auto inactiveSolvers = getLM()->getSolverPool()->getNumInactiveSolvers();
  EXPECT_EQ(inactiveSolvers, 0);

  // This triggers normal termination by default
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);

  // Termination state sent by the solver should:
  // 1 - increase the number of solved nodes in the solver pool
  EXPECT_EQ(getLM()->getSolverPool()->getNumNodesSolvedInSolvers(),
            kCalculationStateNodesSolved);

  // 2 - set the source solver as inactive solver
  EXPECT_EQ(getLM()->getSolverPool()->getNumInactiveSolvers(), inactiveSolvers + 1);

  // The LM should have received a terminated message
  EXPECT_TRUE(getComm()->lastReceive);
  EXPECT_EQ(getComm()->numReceive, 1);
  EXPECT_EQ(getComm()->lastTag, net::MetaBBPacketTag::PacketTagType::PPT_TERMINATED);
}  // Tag_Completion_Of_Calculation_No_Throw

TEST_F(LoadManagerImplFixture, Tag_Completion_Of_Calculation_Interrupt_In_Racing_No_Throw)
{
  using namespace optilab;
  using namespace metabb;
  return;
  const bool useMock{false};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_COMPLETION_OF_CALCULATION;

  // Interruption in racing stage should behave as no-op
  getLM()->setTerminationStateForSolvers(solver::TerminationState::TS_INTERRUPTED_IN_RACING_STAGE);
  EXPECT_NO_THROW(executeMessage(createPacket<int>(tag, packetValue), source, useMock));

  EXPECT_EQ(getLM()->getSolverPool()->getNumInactiveSolvers(), 0) <<
          "No solver should get deactivated";

  // The LM should have received a terminated message
  EXPECT_TRUE(getComm()->lastReceive);
  EXPECT_EQ(getComm()->numReceive, 1);
  EXPECT_EQ(getComm()->lastTag, net::MetaBBPacketTag::PacketTagType::PPT_TERMINATED);
}  // Tag_Completion_Of_Calculation_Interrupt_In_Racing_No_Throw

TEST_F(LoadManagerImplFixture, Tag_Completion_Of_Calculation_Terminated_By_Another_Node_No_Throw)
{
  using namespace optilab;
  using namespace metabb;
  return;
  const bool useMock{false};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_COMPLETION_OF_CALCULATION;

  EXPECT_EQ(getLM()->getSolverPool()->getNumInactiveSolvers(), 0);

  // Termination by another node should behave as no-op and update the nodes solved
  getLM()->setTerminationStateForSolvers(solver::TerminationState::TS_TERMINATED_BY_ANOTHER_NODE);
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);

  EXPECT_EQ(getLM()->getSolverPool()->getNumInactiveSolvers(), kCalculationStateNodesSolved);

  // The LM should have received a terminated message
  EXPECT_TRUE(getComm()->lastReceive);
  EXPECT_EQ(getComm()->numReceive, 1);
  EXPECT_EQ(getComm()->lastTag, net::MetaBBPacketTag::PacketTagType::PPT_TERMINATED);
}  // Tag_Completion_Of_Calculation_Terminated_By_Another_Node_No_Throw

TEST_F(LoadManagerImplFixture,
       Tag_Completion_Of_Calculation_Terminated_By_Interrupt_Request_No_Throw)
{
  using namespace optilab;
  using namespace metabb;
  return;
  const bool useMock{false};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_COMPLETION_OF_CALCULATION;

  EXPECT_EQ(getLM()->getSolverPool()->getNumInactiveSolvers(), 0);

  // Termination by interrupt request should:
  // 1 - deactivate the solver
  // 2 - add the current solving node back into the node pool
  const auto numNodes = getLM()->getNodePool()->getNumNodes();
  const auto numInactiveSolvers = getLM()->getSolverPool()->getNumInactiveSolvers();

  getLM()->setTerminationStateForSolvers(
          solver::TerminationState::TS_TERMINATED_BY_INTERRUPT_REQUEST);
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);

  EXPECT_EQ(getLM()->getNodePool()->getNumNodes(), numNodes + kCalculationStateNodesSolved);
  EXPECT_EQ(getLM()->getSolverPool()->getNumInactiveSolvers(), numInactiveSolvers + 1);

  // The LM should have received a terminated message
  EXPECT_TRUE(getComm()->lastReceive);
  EXPECT_EQ(getComm()->numReceive, 1);
  EXPECT_EQ(getComm()->lastTag, net::MetaBBPacketTag::PacketTagType::PPT_TERMINATED);
}  // Tag_Completion_Of_Calculation_Terminated_By_Interrupt_Request_No_Throw

TEST_F(LoadManagerImplFixture, Tag_Completion_Of_Calculation_Terminated_In_Racing_Stage_No_Throw)
{
  using namespace optilab;
  using namespace metabb;
  return;
  const bool useMock{false};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_COMPLETION_OF_CALCULATION;

  // Termination in racing stage should:
  // 1 - set racing termination flag to true
  // 2 - set the number of nodes solved at racing termination
  const auto numNodes = getLM()->getLM()->getNumNodesSolvedAtRacingTermination();
  EXPECT_FALSE(getLM()->getLM()->isRacingTermination());

  getLM()->setTerminationStateForSolvers(
          solver::TerminationState::TS_TERMINATED_IN_RACING_STAGE);
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);

  EXPECT_EQ(getLM()->getLM()->getNumNodesSolvedAtRacingTermination(),
            numNodes + kCalculationStateNodesSolved);
  EXPECT_TRUE(getLM()->getLM()->isRacingTermination());

  // The LM should have received a terminated message
  EXPECT_TRUE(getComm()->lastReceive);
  EXPECT_EQ(getComm()->numReceive, 1);
  EXPECT_EQ(getComm()->lastTag, net::MetaBBPacketTag::PacketTagType::PPT_TERMINATED);
}  // Tag_Completion_Of_Calculation_Terminated_In_Racing_Stage_No_Throw

TEST_F(LoadManagerImplFixture, Tag_Completion_Of_Calculation_Interrupted_In_Merging_No_Throw)
{
  using namespace optilab;
  using namespace metabb;
  return;
  const bool useMock{false};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_COMPLETION_OF_CALCULATION;

  // Termination by interrupt in merging should:
  // 1 - regenerate merge nodes candidates
  // 2 - deactivate the solver
  // 3 - insert the current solving node back into the node pool
  const auto numNodes = getLM()->getNodePool()->getNumNodes();
  const auto numInactiveSolvers = getLM()->getSolverPool()->getNumInactiveSolvers();

  getLM()->setTerminationStateForSolvers(
          solver::TerminationState::TS_INTERRUPTED_IN_MERGING);
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);

  EXPECT_EQ(getLM()->getNodePool()->getNumNodes(), numNodes + kCalculationStateNodesSolved);
  EXPECT_EQ(getLM()->getSolverPool()->getNumInactiveSolvers(), numInactiveSolvers + 1);

  // The LM should have received a terminated message
  EXPECT_TRUE(getComm()->lastReceive);
  EXPECT_EQ(getComm()->numReceive, 1);
  EXPECT_EQ(getComm()->lastTag, net::MetaBBPacketTag::PacketTagType::PPT_TERMINATED);
}  // Tag_Completion_Of_Calculation_Terminated_By_Interrupt_Request_No_Throw

TEST_F(LoadManagerImplFixture, Tag_Completion_Of_Calculation_Terminated_By_Time_Limit_No_Throw)
{
  using namespace optilab;
  using namespace metabb;
  return;
  const bool useMock{false};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_COMPLETION_OF_CALCULATION;

  // Termination by interrupt in merging should:
  // 1 - insert the current solving node back into the node pool
  const auto numNodes = getLM()->getNodePool()->getNumNodes();
  getLM()->setTerminationStateForSolvers(
          solver::TerminationState::TS_TERMINATED_BY_TIME_LIMIT);
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);

  EXPECT_EQ(getLM()->getNodePool()->getNumNodes(), numNodes + kCalculationStateNodesSolved);

  // The LM should have received a terminated message
  EXPECT_TRUE(getComm()->lastReceive);
  EXPECT_EQ(getComm()->numReceive, 1);
  EXPECT_EQ(getComm()->lastTag, net::MetaBBPacketTag::PacketTagType::PPT_TERMINATED);
}  // Tag_Completion_Of_Calculation_Terminated_By_Time_Limit_No_Throw

TEST_F(LoadManagerImplFixture, Mock_Tag_Another_Node_Request_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{true};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_ANOTHER_NODE_REQUEST;

  EXPECT_EQ(getMockLMImpl()->tagAnotherNodeRequest(), 0);
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);
  EXPECT_EQ(getMockLMImpl()->tagAnotherNodeRequest(), 1);
}  // Mock_Tag_Another_Node_Request_No_Throw

TEST_F(LoadManagerImplFixture, Tag_Another_Node_Request_No_Nodes_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{false};
  const int source{1};
  const double packetValue{10.0};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_ANOTHER_NODE_REQUEST;

  const auto nodesFailed = getLM()->getTermState().numNodesFailedToSendBack;
  executeMessage(createPacket<double>(tag, packetValue), source, useMock);

  // Another node request when the node pool is empty should send
  // NO_NODES tag message to the source
  EXPECT_TRUE(getComm()->lastSend);
  EXPECT_EQ(getComm()->numSend, 1);
  EXPECT_EQ(getComm()->lastTag, net::MetaBBPacketTag::PacketTagType::PPT_NO_NODES);
  EXPECT_EQ(getLM()->getTermState().numNodesFailedToSendBack, nodesFailed + 1);
}  // Tag_Another_Node_Request_No_Nodes_No_Throw

TEST_F(LoadManagerImplFixture, Tag_Another_Node_Request_Send_Nodes_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{false};
  const int source{1};
  const double packetValue{10.0};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_ANOTHER_NODE_REQUEST;

  // Add a node to the node pool so it is not empty and can send nodes
  // to the solver requesting the node.
  // Set the dual bound value lower than the objective function value
  auto node = getLM()->getFramework()->buildNode();
  node->setDualBoundValue(kObjectiveFunctionValue - 1.0);
  getNodePool()->insert(node);

  auto nodePoolSize = getNodePool()->getNumNodes();
  executeMessage(createPacket<double>(tag, packetValue), source, useMock);

  // The node is valid but its lower bound is not enough to activate a solver.
  // Put the node back into the node pool
  EXPECT_TRUE(getComm()->lastSend);
  EXPECT_EQ(getComm()->numSend, 1);
  EXPECT_EQ(getComm()->lastTag, net::MetaBBPacketTag::PacketTagType::PPT_NO_NODES);
  EXPECT_EQ(getNodePool()->getNumNodes(), nodePoolSize);
}  // Tag_Another_Node_Request_Send_Nodes_No_Throw

TEST_F(LoadManagerImplFixture, Mock_Tag_Terminated_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{true};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_TERMINATE_REQUEST;

  EXPECT_EQ(getMockLMImpl()->tagTerminated(), 0);
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);
  EXPECT_EQ(getMockLMImpl()->tagTerminated(), 1);
}  // Mock_Tag_Terminated_No_Throw

TEST_F(LoadManagerImplFixture, Tag_Terminated_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{false};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_TERMINATE_REQUEST;

  const auto numTerm = getLM()->getLM()->getNumTerminated();
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);
  EXPECT_EQ(getLM()->getLM()->getNumTerminated(), numTerm + 1);
}  // Tag_Terminated_No_Throw

TEST_F(LoadManagerImplFixture, Mock_Tag_Hard_Time_Limit_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{true};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_HARD_TIME_LIMIT;

  EXPECT_EQ(getMockLMImpl()->tagHardTimeLimit(), 0);
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);
  EXPECT_EQ(getMockLMImpl()->tagHardTimeLimit(), 1);
}  // Mock_Tag_Hard_Time_Limit_No_Throw

TEST_F(LoadManagerImplFixture, Tag_Hard_Time_Limit_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{false};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_HARD_TIME_LIMIT;

  EXPECT_FALSE(getLM()->getLM()->isHardTimeLimitReached());
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);
  EXPECT_TRUE(getLM()->getLM()->isHardTimeLimitReached());
}  // Tag_Hard_Time_Limit_No_Throw

TEST_F(LoadManagerImplFixture, Mock_Tag_Statistics_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{true};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_INITIAL_STAT;

  EXPECT_EQ(getMockLMImpl()->tagStatistics(), 0);
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);
  EXPECT_EQ(getMockLMImpl()->tagStatistics(), 1);
}  // Mock_Tag_Statistics_No_Throw

TEST_F(LoadManagerImplFixture, Tag_Statistics_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{false};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_INITIAL_STAT;

  EXPECT_NO_THROW(executeMessage(createPacket<int>(tag, packetValue), source, useMock));
}  // Tag_Statistics_No_Throw

TEST_F(LoadManagerImplFixture, Mock_Tag_Allow_To_Be_In_Collecting_Mode_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{true};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_ALLOW_TO_BE_IN_COLLECTING_MODE;

  EXPECT_EQ(getMockLMImpl()->tagAllowToBeInCollectingMode(), 0);
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);
  EXPECT_EQ(getMockLMImpl()->tagAllowToBeInCollectingMode(), 1);
}  // Mock_Tag_Allow_To_Be_In_Collecting_Mode_No_Throw

TEST_F(LoadManagerImplFixture, Tag_Allow_To_Be_In_Collecting_Mode_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{false};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_ALLOW_TO_BE_IN_COLLECTING_MODE;

  EXPECT_NO_THROW(executeMessage(createPacket<int>(tag, packetValue), source, useMock));
}  // Tag_Allow_To_Be_In_Collecting_Mode_No_Throw

TEST_F(LoadManagerImplFixture, Mock_Tag_Lb_Bound_Tightened_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{true};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_LB_BOUND_TIGHTENED_INDEX;

  EXPECT_EQ(getMockLMImpl()->tagLbBoundTightened(), 0);
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);
  EXPECT_EQ(getMockLMImpl()->tagLbBoundTightened(), 1);
}  // Mock_Tag_Lb_Bound_Tightened_No_Throw

TEST_F(LoadManagerImplFixture, Mock_Tag_Ub_Bound_Tightened_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{true};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_UB_BOUND_TIGHTENED_INDEX;

  EXPECT_EQ(getMockLMImpl()->tagUbBoundTightened(), 0);
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);
  EXPECT_EQ(getMockLMImpl()->tagUbBoundTightened(), 1);
}  // Mock_Tag_Ub_Bound_Tightened_No_Throw

TEST_F(LoadManagerImplFixture, Tag_Lb_Bound_Tightened_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{false};
  const int source{1};
  const std::vector<double> packetValue{0.0, 0.0};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_LB_BOUND_TIGHTENED_INDEX;

  EXPECT_NO_THROW(executeMessage(createListPacket<double>(tag, packetValue), source, useMock));
}  // Mock_Tag_Lb_Bound_Tightened_No_Throw

TEST_F(LoadManagerImplFixture, Tag_Ub_Bound_Tightened_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{false};
  const int source{1};
  const std::vector<double> packetValue{0.0, 0.0};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_UB_BOUND_TIGHTENED_INDEX;

  EXPECT_NO_THROW(executeMessage(createListPacket<double>(tag, packetValue), source, useMock));
}  // Tag_Ub_Bound_Tightened_No_Throw

TEST_F(LoadManagerImplFixture, Mock_Tag_Solver_State_Racing_Rampup_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{true};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_SOLVER_STATE;

  // Set racing ramp-up to trigger racing ramp-up solver state
  setRacingRampup();

  EXPECT_EQ(getMockLMImpl()->racingRampUpTagSolverState(), 0);
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);
  EXPECT_EQ(getMockLMImpl()->racingRampUpTagSolverState(), 1);
}  // Mock_Tag_Solver_State_Racing_Rampup_No_Throw

TEST_F(LoadManagerImplFixture, Tag_Solver_State_Racing_Rampup_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{false};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_SOLVER_STATE;

  // Set racing ramp-up to trigger racing ramp-up solver state
  setRacingRampup();

  // Set Racing solver pool
  getLM()->setRacingSolverPool();

  // Set racing stage for the solver state
  getLM()->getFramework()->setIsRacing();

  executeMessage(createPacket<int>(tag, packetValue), source, useMock);

  // Racing solver state should send a best bound value and a notification id
  // back to the source solver
  EXPECT_TRUE(getComm()->lastSend);
  EXPECT_EQ(getComm()->numSend, 2);
  EXPECT_EQ(getComm()->lastTag, net::MetaBBPacketTag::PacketTagType::PPT_NOTIFICATION_ID);
}  // Tag_Solver_State_Racing_Rampup_No_Throw

TEST_F(LoadManagerImplFixture, Mock_Tag_CompletionOfCalculation_Racing_Rampup_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{true};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_COMPLETION_OF_CALCULATION;

  // Set racing ramp-up to trigger racing ramp-up solver state
  setRacingRampup();

  EXPECT_EQ(getMockLMImpl()->racingRampUpTagCompletionOfCalculation(), 0);
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);
  EXPECT_EQ(getMockLMImpl()->racingRampUpTagCompletionOfCalculation(), 1);
}  // Mock_Tag_CompletionOfCalculation_Racing_Rampup_No_Throw

TEST_F(LoadManagerImplFixture, Tag_CompletionOfCalculation_Time_Limit_Racing_Rampup_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{false};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_COMPLETION_OF_CALCULATION;

  // Set racing ramp-up to trigger racing ramp-up solver state
  setRacingRampup();

  // Set Racing solver pool
  getLM()->setRacingSolverPool();

  // Set racing stage for the solver state
  getLM()->getFramework()->setIsRacing();


  // Set termination state
  const auto numNodes = getLM()->getNodePool()->getNumNodes();
  getLM()->setTerminationStateForSolvers(
          solver::TerminationState::TS_TERMINATED_BY_TIME_LIMIT);

  const auto numInactiveSolvers = getLM()->getRacingSolverPool()->getNumInactiveSolvers();
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);

  // The LM should have received a terminated message
  EXPECT_TRUE(getComm()->lastReceive);
  EXPECT_EQ(getComm()->numReceive, 1);
  EXPECT_EQ(getComm()->lastTag, net::MetaBBPacketTag::PacketTagType::PPT_TERMINATED);

  // Solver should be deactive in solver pool
  EXPECT_EQ(getLM()->getRacingSolverPool()->getNumInactiveSolvers(), numInactiveSolvers + 1);
}  // Tag_CompletionOfCalculation_Time_Limit_Racing_Rampup_No_Throw

TEST_F(LoadManagerImplFixture, Tag_CompletionOfCalculation_Racing_Rampup_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{false};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_COMPLETION_OF_CALCULATION;

  // Set racing ramp-up to trigger racing ramp-up solver state
  setRacingRampup();

  // Set Racing solver pool
  getLM()->setRacingSolverPool();

  // Set racing stage for the solver state
  getLM()->getFramework()->setIsRacing();


  // Set termination state
  const auto numNodes = getLM()->getNodePool()->getNumNodes();
  getLM()->setTerminationStateForSolvers(
          solver::TerminationState::TS_TERMINATED_IN_RACING_STAGE);

  const auto numInactiveSolvers = getLM()->getRacingSolverPool()->getNumInactiveSolvers();
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);

  // The LM should have received a terminated message
  EXPECT_TRUE(getComm()->lastReceive);
  EXPECT_EQ(getComm()->numReceive, 1);
  EXPECT_EQ(getComm()->lastTag, net::MetaBBPacketTag::PacketTagType::PPT_TERMINATED);

  // Solver should be deactive in solver pool
  EXPECT_EQ(getLM()->getRacingSolverPool()->getNumInactiveSolvers(), numInactiveSolvers + 1);
}  // Tag_CompletionOfCalculation_Racing_Rampup_No_Throw
