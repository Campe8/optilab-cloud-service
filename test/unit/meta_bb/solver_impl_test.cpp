#include <limits>   // for std::numeric_limits
#include <memory>   // for std::unique_ptr
#include <utility>  // for std::move
#include <vector>

#include <gtest/gtest.h>

#include "meta_bb_mocks.hpp"
#include "meta_bb/framework.hpp"
#include "meta_bb/initiator.hpp"
#include "meta_bb/load_manager_impl.hpp"
#include "meta_bb/meta_bb_struct.hpp"
#include "meta_bb/meta_bb_utilities.hpp"
#include "meta_bb/node_pool.hpp"
#include "meta_bb/paramset.hpp"
#include "optimizer_network/packet.hpp"
#include "utilities/timer.hpp"

class SolverImplFixture : public testing::Test {
 protected:
  void SetUp() override
  {
    using namespace optilab;
    using namespace metabb;

    // Instantiate a new load manager
    pLM = std::make_shared<optilab::metabb::MockLoadManagerFixture>();
    pSolver = std::make_shared<MockSolver>(
            pLM->getFramework(),
            pLM->getParamSet(),
            pLM->getInitiator()->getInstance(),
            pLM->getTimer());
  }

  inline optilab::metabb::MockLoadManagerFixture* getLM() const noexcept { return pLM.get(); }
  inline optilab::metabb::NodePool::SPtr getNodePool() const { return getLM()->getNodePool(); }

  optilab::metabb::SolverImpl::SPtr getSolverImpl() const
  {
    return pSolver->getSolverImpl();
  }

  optilab::metabb::MockSolver::SPtr getSolver() const
  {
    return pSolver;
  }

  optilab::metabb::MockSolverImpl::SPtr getMockSolverImpl() const
  {
    return pSolver->getMockSolverImpl();
  }

  optilab::optnet::MockNetworkCommunicator::SPtr getComm() const noexcept
  {
    return getLM()->getComm();
  }

  template<typename T>
  optilab::optnet::Packet::UPtr createPacket(
          optilab::metabb::net::MetaBBPacketTag::PacketTagType tag,
          const T& data)
  {
    using namespace optilab;
    using namespace metabb;

    optnet::Packet::UPtr packet(new optnet::Packet());
    packet->packetTag = net::MetaBBPacketTag::UPtr(new net::MetaBBPacketTag(tag));
    packet->networkPacket =
            typename optnet::NetworkPacket<T>::UPtr(new optnet::NetworkPacket<T>(data));
    return std::move(packet);
  }

  template<typename T>
  optilab::optnet::Packet::UPtr createListPacket(
          optilab::metabb::net::MetaBBPacketTag::PacketTagType tag,
          const std::vector<T>& data)
  {
    using namespace optilab;
    using namespace metabb;

    optnet::Packet::UPtr packet(new optnet::Packet());
    packet->packetTag = net::MetaBBPacketTag::UPtr(new net::MetaBBPacketTag(tag));
    packet->networkPacket =
            typename optnet::NetworkPacket<T>::UPtr(new optnet::NetworkPacket<T>(data));
    return std::move(packet);
  }

  void executeMessage(optilab::optnet::Packet::UPtr packet, int source, bool useMock=true)
  {
    using namespace optilab;
    using namespace metabb;

    // Check if the received packet has a tag that is supported
    net::MetaBBPacketTag::PacketTagType tag;
    ASSERT_NO_THROW(tag = utils::castToMetaBBPacketTagAndGetTagOrThrow(packet->packetTag));

    if (useMock)
    {
      ASSERT_TRUE(getMockSolverImpl()->isHandlerSupported(tag));

      // Callback function
      ASSERT_NO_THROW((*getMockSolverImpl())(source, std::move(packet)));
    }
    else
    {
      ASSERT_TRUE(getSolverImpl()->isHandlerSupported(tag));

      // Callback function
      ASSERT_NO_THROW((*getSolverImpl())(source, std::move(packet)));
    }
  }

 private:
  // Mock Load manager
  optilab::metabb::MockLoadManagerFixture::SPtr pLM;

  // Mock solver
  optilab::metabb::MockSolver::SPtr pSolver;
};

TEST_F(SolverImplFixture, Mock_Tag_Breaking_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{true};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_BREAKING;

  EXPECT_EQ(getMockSolverImpl()->tagBreaking(), 0);
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);
  EXPECT_EQ(getMockSolverImpl()->tagBreaking(), 1);
}  // Mock_Tag_Breaking_No_Throw

TEST_F(SolverImplFixture, Mock_Tag_CollectAllNodes_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{true};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_COLLECT_ALL_NODES;

  EXPECT_EQ(getMockSolverImpl()->tagCollectAllNodes(), 0);
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);
  EXPECT_EQ(getMockSolverImpl()->tagCollectAllNodes(), 1);
}  // Mock_Tag_CollectAllNodes_No_Throw

TEST_F(SolverImplFixture, Mock_Tag_CutOffValue_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{true};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_CUTOFF_VALUE;

  EXPECT_EQ(getMockSolverImpl()->tagCutOffValue(), 0);
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);
  EXPECT_EQ(getMockSolverImpl()->tagCutOffValue(), 1);
}  // Mock_Tag_CutOffValue_No_Throw\

TEST_F(SolverImplFixture, Mock_Tag_GivenGapIsReached_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{true};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_GIVEN_GAP_IS_REACHED;

  EXPECT_EQ(getMockSolverImpl()->tagGivenGapIsReached(), 0);
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);
  EXPECT_EQ(getMockSolverImpl()->tagGivenGapIsReached(), 1);
}  // Mock_Tag_GivenGapIsReached_No_Throw

TEST_F(SolverImplFixture, Mock_Tag_GlobalBestDualBoundValueAtWarmStart_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{true};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_GLOBAL_BEST_DUAL_BOUND_VALUE_AT_WARM_START;

  EXPECT_EQ(getMockSolverImpl()->tagGlobalBestDualBoundValueAtWarmStart(), 0);
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);
  EXPECT_EQ(getMockSolverImpl()->tagGlobalBestDualBoundValueAtWarmStart(), 1);
}  // Mock_Tag_GlobalBestDualBoundValueAtWarmStart_No_Throw

TEST_F(SolverImplFixture, Mock_Tag_InCollectingMode_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{true};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_IN_COLLECTING_MODE;

  EXPECT_EQ(getMockSolverImpl()->tagInCollectingMode(), 0);
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);
  EXPECT_EQ(getMockSolverImpl()->tagInCollectingMode(), 1);
}  // Mock_Tag_InCollectingMode_No_Throw

TEST_F(SolverImplFixture, Mock_Tag_IncumbentValue_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{true};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_INCUMBENT_VALUE;

  EXPECT_EQ(getMockSolverImpl()->tagIncumbentValue(), 0);
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);
  EXPECT_EQ(getMockSolverImpl()->tagIncumbentValue(), 1);
}  // Mock_Tag_IncumbentValue_No_Throw

TEST_F(SolverImplFixture, Mock_Tag_InterruptRequest_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{true};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_INTERRUPT_REQUEST;

  EXPECT_EQ(getMockSolverImpl()->tagInterruptRequest(), 0);
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);
  EXPECT_EQ(getMockSolverImpl()->tagInterruptRequest(), 1);
}  // Mock_Tag_InterruptRequest_No_Throw

TEST_F(SolverImplFixture, Mock_Tag_LightWeightRootNodeProcess_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{true};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_LIGHT_WEIGHT_ROOT_NODE_PROCESS;

  EXPECT_EQ(getMockSolverImpl()->tagLightWeightRootNodeProcess(), 0);
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);
  EXPECT_EQ(getMockSolverImpl()->tagLightWeightRootNodeProcess(), 1);
}  // Mock_Tag_LightWeightRootNodeProcess_No_Throw

TEST_F(SolverImplFixture, Mock_Tag_LMBestBoundValue_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{true};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_LM_BEST_BOUND_VALUE;

  EXPECT_EQ(getMockSolverImpl()->tagLMBestBoundValue(), 0);
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);
  EXPECT_EQ(getMockSolverImpl()->tagLMBestBoundValue(), 1);
}  // Mock_Tag_LMBestBoundValue_No_Throw

TEST_F(SolverImplFixture, Mock_Tag_NoNodes_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{true};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_NO_NODES;

  EXPECT_EQ(getMockSolverImpl()->tagNoNodes(), 0);
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);
  EXPECT_EQ(getMockSolverImpl()->tagNoNodes(), 1);
}  // Mock_Tag_NoNodes_No_Throw

TEST_F(SolverImplFixture, Mock_Tag_NoTestDualBoundGain_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{true};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_NO_TEST_DUAL_BOUND_GAIN;

  EXPECT_EQ(getMockSolverImpl()->tagNoTestDualBoundGain(), 0);
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);
  EXPECT_EQ(getMockSolverImpl()->tagNoTestDualBoundGain(), 1);
}  // Mock_Tag_NoNodes_No_Throw

TEST_F(SolverImplFixture, Mock_Tag_NoWaitModeSend_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{true};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_NO_WAIT_MODE_SEND;

  EXPECT_EQ(getMockSolverImpl()->tagNoWaitModeSend(), 0);
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);
  EXPECT_EQ(getMockSolverImpl()->tagNoWaitModeSend(), 1);
}  // Mock_Tag_NoNodes_No_Throw

TEST_F(SolverImplFixture, Mock_Tag_NodeReceived_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{true};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_NODE_RECEIVED;

  EXPECT_EQ(getMockSolverImpl()->tagNodeReceived(), 0);
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);
  EXPECT_EQ(getMockSolverImpl()->tagNodeReceived(), 1);
}  // Mock_Tag_NodeReceived_No_Throw

TEST_F(SolverImplFixture, Mock_Tag_NotificationId_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{true};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_NOTIFICATION_ID;

  EXPECT_EQ(getMockSolverImpl()->tagNotificationId(), 0);
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);
  EXPECT_EQ(getMockSolverImpl()->tagNotificationId(), 1);
}  // Mock_Tag_NotificationId_No_Throw

TEST_F(SolverImplFixture, Mock_Tag_OutCollectingMode_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{true};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_OUT_COLLECTING_MODE;

  EXPECT_EQ(getMockSolverImpl()->tagOutCollectingMode(), 0);
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);
  EXPECT_EQ(getMockSolverImpl()->tagOutCollectingMode(), 1);
}  // Mock_Tag_OutCollectingMode_No_Throw

TEST_F(SolverImplFixture, Mock_Tag_WinnerRacingRampUpParamSet_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{true};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_RACING_RAMP_UP_PARAMSET;

  EXPECT_EQ(getMockSolverImpl()->tagWinnerRacingRampUpParamSet(), 0);
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);
  EXPECT_EQ(getMockSolverImpl()->tagWinnerRacingRampUpParamSet(), 1);
}  // Mock_Tag_WinnerRacingRampUpParamSet_No_Throw

TEST_F(SolverImplFixture, Mock_Tag_RampUp_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{true};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_RAMP_UP;

  EXPECT_EQ(getMockSolverImpl()->tagRampUp(), 0);
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);
  EXPECT_EQ(getMockSolverImpl()->tagRampUp(), 1);
}  // Mock_Tag_RampUp_No_Throw

TEST_F(SolverImplFixture, Mock_Tag_Restart_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{true};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_RESTART;

  EXPECT_EQ(getMockSolverImpl()->tagRestart(), 0);
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);
  EXPECT_EQ(getMockSolverImpl()->tagRestart(), 1);
}  // Mock_Tag_Restart_No_Throw

TEST_F(SolverImplFixture, Mock_Tag_RetryRampUp_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{true};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_RETRY_RAMP_UP;

  EXPECT_EQ(getMockSolverImpl()->tagRetryRampUp(), 0);
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);
  EXPECT_EQ(getMockSolverImpl()->tagRetryRampUp(), 1);
}  // Mock_Tag_RetryRampUp_No_Throw

TEST_F(SolverImplFixture, Mock_Tag_Solution_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{true};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_SOLUTION;

  EXPECT_EQ(getMockSolverImpl()->tagSolution(), 0);
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);
  EXPECT_EQ(getMockSolverImpl()->tagSolution(), 1);
}  // Mock_Tag_Solution_No_Throw

TEST_F(SolverImplFixture, Mock_Tag_TerminateRequest_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{true};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_TERMINATE_REQUEST;

  EXPECT_EQ(getMockSolverImpl()->tagTerminateRequest(), 0);
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);
  EXPECT_EQ(getMockSolverImpl()->tagTerminateRequest(), 1);
}  // Mock_Tag_TerminateRequest_No_Throw

TEST_F(SolverImplFixture, Mock_Tag_TerminateSolvingToRestart_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{true};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_TERMINATE_SOLVING_TO_RESTART;

  EXPECT_EQ(getMockSolverImpl()->tagTerminateSolvingToRestart(), 0);
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);
  EXPECT_EQ(getMockSolverImpl()->tagTerminateSolvingToRestart(), 1);
}  // Mock_Tag_TerminateSolvingToRestart_No_Throw

TEST_F(SolverImplFixture, Mock_Tag_TestDualBoundGain_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{true};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_TEST_DUAL_BOUND_GAIN;

  EXPECT_EQ(getMockSolverImpl()->tagTestDualBoundGain(), 0);
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);
  EXPECT_EQ(getMockSolverImpl()->tagTestDualBoundGain(), 1);
}  // Mock_Tag_TestDualBoundGain_No_Throw

TEST_F(SolverImplFixture, Mock_Tag_Winner_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{true};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_WINNER;

  EXPECT_EQ(getMockSolverImpl()->tagWinner(), 0);
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);
  EXPECT_EQ(getMockSolverImpl()->tagWinner(), 1);
}  // Mock_Tag_Winner_No_Throw

TEST_F(SolverImplFixture, Mock_Tag_Node_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{true};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PTT_NODE;

  EXPECT_EQ(getMockSolverImpl()->tagNode(), 0);
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);
  EXPECT_EQ(getMockSolverImpl()->tagNode(), 1);
}  // Mock_Tag_Node_No_Throw

TEST_F(SolverImplFixture, Mock_Tag_LbBoundTightened_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{true};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_LB_BOUND_TIGHTENED_INDEX;

  EXPECT_EQ(getMockSolverImpl()->tagLbBoundTightened(), 0);
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);
  EXPECT_EQ(getMockSolverImpl()->tagLbBoundTightened(), 1);
}  // Mock_Tag_LbBoundTightened_No_Throw

TEST_F(SolverImplFixture, Mock_Tag_UbBoundTightened_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{true};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_UB_BOUND_TIGHTENED_INDEX;

  EXPECT_EQ(getMockSolverImpl()->tagUbBoundTightened(), 0);
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);
  EXPECT_EQ(getMockSolverImpl()->tagUbBoundTightened(), 1);
}  // Mock_Tag_UbBoundTightened_No_Throw

TEST_F(SolverImplFixture, Tag_Breaking_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{false};
  const int source{1};
  const std::vector<double> packetValue{1.0, 2.0};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_BREAKING;
  getSolver()->setSolverImpl();

  EXPECT_DOUBLE_EQ(getSolver()->getTargetBound(), std::numeric_limits<double>::lowest());
  EXPECT_FALSE(getSolver()->isBreaking());
  EXPECT_FALSE(getSolver()->isManyNodesCollectionRequested());
  executeMessage(createListPacket<double>(tag, packetValue), source, useMock);
  EXPECT_DOUBLE_EQ(getSolver()->getTargetBound(), packetValue[0]);
  EXPECT_TRUE(getSolver()->isBreaking());
  EXPECT_TRUE(getSolver()->isManyNodesCollectionRequested());
}  // Tag_Breaking_No_Throw

TEST_F(SolverImplFixture, Tag_CollectAllNodes_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{false};
  const int source{1};
  const int packetValue{-1};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_COLLECT_ALL_NODES;
  getSolver()->setSolverImpl();

  EXPECT_FALSE(getSolver()->isCollectingAllNodes());
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);
  EXPECT_TRUE(getSolver()->isCollectingAllNodes());
}  // CollectAllNodes_No_Throw

TEST_F(SolverImplFixture, Tag_CutOffValue_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{false};
  const int source{1};
  const double packetValue{10.0};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_CUTOFF_VALUE;
  getSolver()->setSolverImpl();

  EXPECT_DOUBLE_EQ(getSolver()->getCutOffValue(), std::numeric_limits<double>::max());
  executeMessage(createPacket<double>(tag, packetValue), source, useMock);
  EXPECT_DOUBLE_EQ(getSolver()->getCutOffValue(), packetValue);
}  // Tag_CutOffValue_No_Throw

TEST_F(SolverImplFixture, Tag_GivenGapIsReached_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{false};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_GIVEN_GAP_IS_REACHED;
  getSolver()->setSolverImpl();

  EXPECT_FALSE(getSolver()->isGivenGapReached());
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);
  EXPECT_TRUE(getSolver()->isGivenGapReached());
}  // Tag_GivenGapIsReached_No_Throw

TEST_F(SolverImplFixture, Tag_GlobalBestDualBoundValueAtWarmStart_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{false};
  const int source{1};
  const double packetValue{10.0};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_GLOBAL_BEST_DUAL_BOUND_VALUE_AT_WARM_START;
  getSolver()->setSolverImpl();

  EXPECT_DOUBLE_EQ(getSolver()->getGlobalBestDualBoundValueAtWarmStart(),
                   std::numeric_limits<double>::lowest());
  executeMessage(createPacket<double>(tag, packetValue), source, useMock);
  EXPECT_DOUBLE_EQ(getSolver()->getGlobalBestDualBoundValueAtWarmStart(), packetValue);
}  // Tag_GlobalBestDualBoundValueAtWarmStart_No_Throw

TEST_F(SolverImplFixture, Tag_InCollectingMode_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{false};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_IN_COLLECTING_MODE;
  getSolver()->setSolverImpl();

  EXPECT_EQ(getSolver()->getNumNodesSendInCollectingMode(), 0);
  EXPECT_FALSE(getSolver()->isInCollectingMode());
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);
  EXPECT_EQ(getSolver()->getNumNodesSendInCollectingMode(), packetValue);
  EXPECT_TRUE(getSolver()->isInCollectingMode());
}  // Tag_InCollectingMode_No_Throw

TEST_F(SolverImplFixture, Tag_IncumbentValue_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{false};
  const int source{1};
  const double packetValue{10.0};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_INCUMBENT_VALUE;
  getSolver()->setSolverImpl();

  EXPECT_FALSE(getSolver()->isGlobalIncumbentUpdated());
  EXPECT_DOUBLE_EQ(getSolver()->getGlobalBestIncumbentValue(), std::numeric_limits<double>::max());
  executeMessage(createPacket<double>(tag, packetValue), source, useMock);
  EXPECT_TRUE(getSolver()->isGlobalIncumbentUpdated());
  EXPECT_DOUBLE_EQ(getSolver()->getGlobalBestIncumbentValue(), packetValue);
}  // Tag_IncumbentValue_No_Throw

TEST_F(SolverImplFixture, Tag_InterruptRequest_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{false};
  const int source{1};
  const int packetValue{1};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_INTERRUPT_REQUEST;
  getSolver()->setSolverImpl();

  EXPECT_FALSE(getSolver()->isCollectingInterrupt());
  EXPECT_FALSE(getSolver()->isInCollectingMode());
  EXPECT_FALSE(getSolver()->isInterrupting());
  EXPECT_EQ(getSolver()->getTerminationMode(), solver::TerminationMode::TM_NO_TERMINATION);

  executeMessage(createPacket<int>(tag, packetValue), source, useMock);

  // Sending a value of 1 means collecting interrupt
  EXPECT_TRUE(getSolver()->isCollectingInterrupt());
  EXPECT_EQ(getSolver()->getTerminationMode(), solver::TerminationMode::TM_INTERRUPTED_TERMINATION);
  EXPECT_TRUE(getSolver()->isInterrupting());
  EXPECT_TRUE(getSolver()->isInCollectingMode());
}  // Tag_InterruptRequest_No_Throw

TEST_F(SolverImplFixture, Tag_LightWeightRootNodeProcess_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{false};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_LIGHT_WEIGHT_ROOT_NODE_PROCESS;
  getSolver()->setSolverImpl();

  EXPECT_FALSE(getSolver()->isLightWeightRootNodeProcess());
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);
  EXPECT_TRUE(getSolver()->isLightWeightRootNodeProcess());
}  // Tag_LightWeightRootNodeProcess_No_Throw

TEST_F(SolverImplFixture, Tag_LMBestBoundValue_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{false};
  const int source{1};
  const double packetValue{10.0};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_LM_BEST_BOUND_VALUE;
  getSolver()->setSolverImpl();

  EXPECT_DOUBLE_EQ(getSolver()->getLMBestDualBoundValue(), std::numeric_limits<double>::lowest());
  executeMessage(createPacket<double>(tag, packetValue), source, useMock);
  EXPECT_DOUBLE_EQ(getSolver()->getLMBestDualBoundValue(), packetValue);
}  // Tag_LMBestBoundValue_No_Throw

TEST_F(SolverImplFixture, Tag_NoNodes_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{false};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_NO_NODES;
  getSolver()->setSolverImpl();

  EXPECT_NO_THROW(executeMessage(createPacket<int>(tag, packetValue), source, useMock));
}  // Tag_NoNodes_No_Throw

TEST_F(SolverImplFixture, Tag_NoTestDualBoundGain_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{false};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_NO_TEST_DUAL_BOUND_GAIN;
  getSolver()->setSolverImpl();

  EXPECT_NO_THROW(executeMessage(createPacket<int>(tag, packetValue), source, useMock));
}  // Tag_NoTestDualBoundGain_No_Throw

TEST_F(SolverImplFixture, Tag_NoWaitModeSend_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{false};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_NO_WAIT_MODE_SEND;
  getSolver()->setSolverImpl();

  EXPECT_FALSE(getSolver()->isNoWaitModeSend());
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);
  EXPECT_TRUE(getSolver()->isNoWaitModeSend());
}  // Tag_NoWaitModeSend_No_Throw

TEST_F(SolverImplFixture, Tag_NodeReceived_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{false};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_NODE_RECEIVED;
  getSolver()->setSolverImpl();

  EXPECT_NO_THROW(executeMessage(createPacket<int>(tag, packetValue), source, useMock));
}  // Tag_NodeReceived_No_Throw

TEST_F(SolverImplFixture, Tag_NotificationId_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{false};
  const int source{1};
  const int packetValue = getSolver()->getNotificationIdGenerator();
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_NOTIFICATION_ID;
  getSolver()->setSolverImpl();

  EXPECT_NO_THROW(executeMessage(createPacket<int>(tag, packetValue), source, useMock));
}  // Tag_NotificationId_No_Throw

TEST_F(SolverImplFixture, Tag_OutCollectingMode_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{false};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_OUT_COLLECTING_MODE;
  getSolver()->setSolverImpl();

  getSolver()->setNumNodesSendInCollectingMode(packetValue);
  EXPECT_EQ(getSolver()->getNumNodesSendInCollectingMode(), packetValue);
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);
  EXPECT_EQ(getSolver()->getNumNodesSendInCollectingMode(), 0);
}  // Tag_OutCollectingMode_No_Throw

TEST_F(SolverImplFixture, Tag_WinnerRacingRampUpParamSet_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{false};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_RACING_RAMP_UP_PARAMSET;
  getSolver()->setSolverImpl();

  EXPECT_FALSE(getSolver()->isRacingInterruptRequested());
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);
  EXPECT_TRUE(getSolver()->isRacingInterruptRequested());
}  // Tag_WinnerRacingRampUpParamSet_No_Throw

TEST_F(SolverImplFixture, Tag_RampUp_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{false};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_RAMP_UP;
  getSolver()->setSolverImpl();

  EXPECT_FALSE(getSolver()->isRampUp());
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);
  EXPECT_TRUE(getSolver()->isRampUp());
}  // Tag_RampUp_No_Throw

TEST_F(SolverImplFixture, Tag_Restart_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{false};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_RESTART;
  getSolver()->setSolverImpl();

  getSolver()->setTerminationMode(solver::TerminationMode::TM_INTERRUPTED_TERMINATION);
  EXPECT_EQ(getSolver()->getTerminationMode(), solver::TerminationMode::TM_INTERRUPTED_TERMINATION);

  executeMessage(createPacket<int>(tag, packetValue), source, useMock);

  EXPECT_EQ(getSolver()->getTerminationMode(), solver::TerminationMode::TM_NO_TERMINATION);
}  // Tag_Restart_No_Throw

TEST_F(SolverImplFixture, Tag_RetryRampUp_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{false};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_RETRY_RAMP_UP;
  getSolver()->setSolverImpl();

  getSolver()->setNumNodesSendInCollectingMode(packetValue);
  EXPECT_EQ(getSolver()->getNumNodesSendInCollectingMode(), packetValue);
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);
  EXPECT_EQ(getSolver()->getNumNodesSendInCollectingMode(), 0);
}  // Tag_RetryRampUp_No_Throw

TEST_F(SolverImplFixture, Tag_Solution_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{false};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_SOLUTION;
  getSolver()->setSolverImpl();

  EXPECT_NO_THROW(executeMessage(createPacket<int>(tag, packetValue), source, useMock));
}  // Tag_Solution_No_Throw

TEST_F(SolverImplFixture, Tag_TerminateRequest_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{false};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_TERMINATE_REQUEST;
  getSolver()->setSolverImpl();

  EXPECT_EQ(getSolver()->getTerminationMode(), solver::TerminationMode::TM_NO_TERMINATION);
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);
  EXPECT_EQ(getSolver()->getTerminationMode(), solver::TerminationMode::TM_NORMAL_TERMINATION);
}  // Tag_TerminateRequest_No_Throw

TEST_F(SolverImplFixture, Tag_TerminateSolvingToRestart_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{false};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_TERMINATE_SOLVING_TO_RESTART;
  getSolver()->setSolverImpl();

  getSolver()->setRacingRampUpParamSet();
  getLM()->getParamSet()->getDescriptor().set_rampupphaseprocess(1);

  EXPECT_FALSE(getSolver()->isRacingInterruptRequested());
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);
  EXPECT_TRUE(getSolver()->isRacingInterruptRequested());
}  // Tag_TerminateSolvingToRestart_No_Throw

TEST_F(SolverImplFixture, Tag_TestDualBoundGain_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{false};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_TEST_DUAL_BOUND_GAIN;
  getSolver()->setSolverImpl();

  EXPECT_FALSE(getSolver()->isDualBoundGainTestNeeded());
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);
  EXPECT_TRUE(getSolver()->isDualBoundGainTestNeeded());
}  // Tag_TestDualBoundGain_No_Throw

TEST_F(SolverImplFixture, Tag_Winner_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{false};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_WINNER;
  getSolver()->setSolverImpl();

  EXPECT_FALSE(getSolver()->isRacingWinner());
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);
  EXPECT_TRUE(getSolver()->isRacingWinner());
}  // Tag_Winner_No_Throw

TEST_F(SolverImplFixture, Tag_Node_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{false};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PTT_NODE;
  getSolver()->setSolverImpl();

  const auto numNodes = getSolver()->getNumNodesReceived();
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);
  EXPECT_EQ(getSolver()->getNumNodesReceived(), numNodes + 1);
}  // Tag_Node_No_Throw

TEST_F(SolverImplFixture, Tag_LbBoundTightened_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{false};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_LB_BOUND_TIGHTENED_INDEX;
  getSolver()->setSolverImpl();

  EXPECT_FALSE(getSolver()->getLBUpdate());
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);
  EXPECT_TRUE(getSolver()->getLBUpdate());
}  // Tag_LbBoundTightened_No_Throw

TEST_F(SolverImplFixture, Tag_UbBoundTightened_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool useMock{false};
  const int source{1};
  const int packetValue{10};
  auto tag = net::MetaBBPacketTag::PacketTagType::PPT_UB_BOUND_TIGHTENED_INDEX;
  getSolver()->setSolverImpl();

  EXPECT_FALSE(getSolver()->getUBUpdate());
  executeMessage(createPacket<int>(tag, packetValue), source, useMock);
  EXPECT_TRUE(getSolver()->getUBUpdate());
}  // Tag_UbBoundTightened_No_Throw
