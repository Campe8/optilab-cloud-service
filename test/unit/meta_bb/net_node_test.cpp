#include <memory>     // for std::shared_ptr

#include <gtest/gtest.h>

#include "meta_bb_mocks.hpp"
#include "meta_bb_net_mocks.hpp"
#include "meta_bb/diff_subproblem.hpp"
#include "meta_bb/net_node.hpp"
#include "meta_bb/node.hpp"
#include "meta_bb/tree_id.hpp"

namespace {
constexpr int kLMId = 10;
constexpr int kGlbSubtreeId = 11;
constexpr int kSolverId = 12;
constexpr long long kSeqIdNum = 1000;
constexpr int kNodeDepth = 20;
constexpr double kNodeDualBound = -1.0;
constexpr double kNodeOrigDualBound = 1.0;
constexpr double kNodeEstimatedValue = -10.0;
}  // namespace

class NetNodeFixture : public testing::Test {
 protected:
  void SetUp() override
  {
    using namespace optilab;
    using namespace metabb;
    stid = std::make_shared<SubtreeId>(kLMId, kGlbSubtreeId, kSolverId);
    nGenId = std::make_shared<NodeId>(kSeqIdNum, *(stid.get()));
    nId = std::make_shared<NodeId>(kSeqIdNum+1, *(stid.get()));
    diffSubproblem = std::make_shared<MockDiffSubproblem>();
    communicator = std::make_shared<optnet::MockNetworkCommunicator>();
    framework = std::make_shared<MockFramework>(communicator);
  }

  optilab::metabb::MockNetNode::SPtr buildNode(
          optilab::metabb::DiffSubproblem::SPtr diffSubproblem,
          std::shared_ptr<optilab::metabb::NodeId> nodeId=nullptr,
          double dualBound=kNodeDualBound)
  {
    using namespace optilab;
    using namespace metabb;

    optilab::metabb::MockNetNode::SPtr node;
    if (!nodeId)
    {
      node = std::make_shared<MockNetNode>(*(nId.get()), *(nGenId.get()), kNodeDepth,
                                           dualBound, kNodeOrigDualBound, kNodeEstimatedValue,
                                           diffSubproblem);
    }
    else
    {
      node = std::make_shared<MockNetNode>(*(nodeId.get()), *(nGenId.get()), kNodeDepth,
                                           dualBound, kNodeOrigDualBound, kNodeEstimatedValue,
                                           diffSubproblem);
    }

    return node;
  }

  optilab::optnet::MockNetworkCommunicator::SPtr getComm() const { return communicator; }
  optilab::metabb::MockFramework::SPtr getFramework() const { return framework; }
  optilab::metabb::SubtreeId& getSubtreeId() { return *stid; }
  optilab::metabb::NodeId& getGenNodeId() { return *nGenId; }
  optilab::metabb::NodeId& getNodeId() { return *nId; }

 private:
  std::shared_ptr<optilab::metabb::SubtreeId> stid;
  std::shared_ptr<optilab::metabb::NodeId> nGenId;
  std::shared_ptr<optilab::metabb::NodeId> nId;
  optilab::metabb::DiffSubproblem::SPtr diffSubproblem;
  optilab::optnet::MockNetworkCommunicator::SPtr communicator;
  optilab::metabb::MockFramework::SPtr framework;
};

TEST(NetNode, Default_Constructor_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  NetNode::SPtr node;
  ASSERT_NO_THROW(node = std::make_shared<NetNode>());
  ASSERT_TRUE(!!node);
}  // Default_Constructor_No_Throw

TEST_F(NetNodeFixture, Constructor_No_DiffSubproblem_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  DiffSubproblem::SPtr diffSubproblem;
  auto node = buildNode(diffSubproblem);
  ASSERT_TRUE(!!node);

  // Check get/set methods
  EXPECT_TRUE(node->isSameNodeId(*node));
  EXPECT_TRUE(node->isSameParentNodeId(*node));
  EXPECT_TRUE(node->isSameParentNodeSubtreeId(getNodeId()));
  EXPECT_TRUE(node->isSameSubtreeId(*node));
  EXPECT_TRUE(node->isSameLMId(*node));
  EXPECT_TRUE(node->isSameLMId(kLMId));
  EXPECT_TRUE(node->isSameGlobalSubtreeIdInLM(*node));
  EXPECT_TRUE(node->isSameGlobalSubtreeIdInLMAs(kGlbSubtreeId));
  EXPECT_EQ(kLMId, node->getLMId());
  EXPECT_EQ(kGlbSubtreeId, node->getGlobalSubtreeIdInLM());
  EXPECT_EQ(kSolverId, node->getSolverId());
  EXPECT_TRUE(getNodeId() == node->getNodeId());
  EXPECT_EQ(kNodeDepth, node->getDepth());
  EXPECT_DOUBLE_EQ(kNodeDualBound, node->getDualBoundValue());
  EXPECT_DOUBLE_EQ(kNodeOrigDualBound, node->getInitialDualBoundValue());
  EXPECT_DOUBLE_EQ(kNodeEstimatedValue, node->getEstimatedValue());
  EXPECT_FALSE(node->getDiffSubproblem());
  EXPECT_FALSE(node->getParent());
  EXPECT_FALSE(node->hasChildren());
  EXPECT_EQ(Node::NodeMergingStatus::NMS_NO_MERGING_NODE, node->getMergingStatus());
  EXPECT_FALSE(node->areNodesCollected());
}  // Constructor_No_DiffSubproblem_No_Throw

TEST_F(NetNodeFixture, Clone_No_DiffSubproblem_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  DiffSubproblem::SPtr diffSubproblem;
  auto node = buildNode(diffSubproblem);
  ASSERT_TRUE(!!node);

  auto cloneNode = node->clone(getFramework());

  // Check get/set methods
  EXPECT_TRUE(cloneNode->isSameNodeId(*node));
  EXPECT_TRUE(cloneNode->isSameParentNodeId(*node));
  EXPECT_TRUE(cloneNode->isSameParentNodeSubtreeId(getNodeId()));
  EXPECT_TRUE(cloneNode->isSameSubtreeId(*node));
  EXPECT_TRUE(cloneNode->isSameLMId(*node));
  EXPECT_TRUE(cloneNode->isSameLMId(kLMId));
  EXPECT_TRUE(cloneNode->isSameGlobalSubtreeIdInLM(*node));
  EXPECT_TRUE(cloneNode->isSameGlobalSubtreeIdInLMAs(kGlbSubtreeId));
  EXPECT_EQ(kLMId, cloneNode->getLMId());
  EXPECT_EQ(kGlbSubtreeId, cloneNode->getGlobalSubtreeIdInLM());
  EXPECT_EQ(kSolverId, cloneNode->getSolverId());
  EXPECT_TRUE(getNodeId() == cloneNode->getNodeId());
  EXPECT_EQ(kNodeDepth, cloneNode->getDepth());
  EXPECT_DOUBLE_EQ(kNodeDualBound, cloneNode->getDualBoundValue());
  EXPECT_DOUBLE_EQ(kNodeOrigDualBound, cloneNode->getInitialDualBoundValue());
  EXPECT_DOUBLE_EQ(node->getInitialDualBoundValue(), cloneNode->getEstimatedValue());
  EXPECT_FALSE(cloneNode->getDiffSubproblem());
  EXPECT_FALSE(cloneNode->getParent());
  EXPECT_FALSE(cloneNode->hasChildren());
  EXPECT_EQ(Node::NodeMergingStatus::NMS_NO_MERGING_NODE, cloneNode->getMergingStatus());
  EXPECT_FALSE(cloneNode->areNodesCollected());
}  // Clone_No_DiffSubproblem_No_Throw

TEST_F(NetNodeFixture, Clone_And_Check_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  DiffSubproblem::SPtr diffSubproblem;
  auto node = buildNode(diffSubproblem);
  ASSERT_TRUE(!!node);

  // Set node values
  NodeId nodeId2(kSeqIdNum+10, getSubtreeId());
  EXPECT_NO_THROW(node->setGlobalSubtreeId(kLMId+1, kGlbSubtreeId+1));
  EXPECT_NO_THROW(node->setSolverId(kSolverId+1));
  EXPECT_NO_THROW(node->setNodeId(nodeId2));
  EXPECT_NO_THROW(node->setGeneratorNodeId(nodeId2));
  EXPECT_NO_THROW(node->setDepth(kNodeDepth + 1));
  EXPECT_NO_THROW(node->setDualBoundValue(kNodeDualBound + 1));
  EXPECT_NO_THROW(node->setInitialDualBoundValue(kNodeOrigDualBound + 1));
  EXPECT_NO_THROW(node->setEstimatedValue(kNodeEstimatedValue + 1));

  // Clone the node
  auto cloneNode = node->clone(getFramework());

  // Check the values of the cloned node to be matched with the original node
  EXPECT_TRUE(cloneNode->isSameLMId(kLMId)) << "LM Id is not cloned";
  EXPECT_TRUE(cloneNode->isSameGlobalSubtreeIdInLMAs(kGlbSubtreeId)) <<
          "Global subtree id in LM is not cloned";
  EXPECT_EQ(kSolverId, cloneNode->getSolverId()) << "Solver id is not cloned";
  EXPECT_TRUE(cloneNode->getNodeId() == nodeId2);
  EXPECT_TRUE(cloneNode->getGeneratorNodeId() == nodeId2);
  EXPECT_EQ(kNodeDepth + 1, cloneNode->getDepth());
  EXPECT_DOUBLE_EQ(kNodeDualBound + 1, cloneNode->getDualBoundValue());
  EXPECT_DOUBLE_EQ(kNodeOrigDualBound + 1, cloneNode->getInitialDualBoundValue());
  EXPECT_DOUBLE_EQ(cloneNode->getInitialDualBoundValue(), cloneNode->getEstimatedValue()) <<
          "Estimated value is not cloned but it is set "
          "to the original node initial dual bound value ";
}  // Clone_And_Check_No_Throw

TEST_F(NetNodeFixture, Broadcast_Check_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const int nodeId{1};
  DiffSubproblem::SPtr diffSubproblem;
  auto node = buildNode(diffSubproblem);
  node->broadcast(getFramework(), nodeId);
  EXPECT_TRUE(getComm()->lastBroadcast);
  EXPECT_EQ(getComm()->nodeId, nodeId);
}  // Broadcast_Check_No_Throw

TEST_F(NetNodeFixture, Send_Check_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const int nodeId{1};
  DiffSubproblem::SPtr diffSubproblem;
  auto node = buildNode(diffSubproblem);
  node->send(getFramework(), 1);
  EXPECT_TRUE(getComm()->lastSend);
  EXPECT_EQ(getComm()->nodeId, nodeId);
}  // Send_Check_No_Throw

TEST_F(NetNodeFixture, Upload_Check_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const int nodeId{1};
  DiffSubproblem::SPtr diffSubproblem;
  auto node = buildNode(diffSubproblem);

  // Set node values different from original constructor values
  EXPECT_NO_THROW(node->setGlobalSubtreeId(kLMId + 1, kGlbSubtreeId + 1));
  EXPECT_NO_THROW(node->setSolverId(kSolverId + 1));
  EXPECT_NO_THROW(node->setDepth(kNodeDepth + 1));
  EXPECT_NO_THROW(node->setDualBoundValue(kNodeDualBound + 10.0));
  EXPECT_NO_THROW(node->setInitialDualBoundValue(kNodeOrigDualBound + 100.0));
  EXPECT_NO_THROW(node->setEstimatedValue(kNodeEstimatedValue + 1000.0));
  EXPECT_NO_THROW(node->setMergingStatus(Node::NodeMergingStatus::NMS_MERGED));

  // Build a new node
  auto bnode = std::make_shared<MockNetNode>();

  // Create the optimizer node packet to send to the destination
  optnet::Packet::UPtr nodePacket = node->buildNetworkPacket();
  nodePacket->packetTag = optnet::PacketTag::UPtr(new net::MetaBBPacketTag(
      net::MetaBBPacketTag::PacketTagType::PTT_NODE));

  ASSERT_EQ(bnode->upload(getFramework(), std::move(nodePacket)), 0);

  // Check that the uploaded node is the same as the original node
  EXPECT_EQ(bnode->getLMId(), kLMId + 1);
  EXPECT_EQ(bnode->getGlobalSubtreeIdInLM(), kGlbSubtreeId + 1);
  EXPECT_EQ(bnode->getSolverId(), kSolverId + 1);
  EXPECT_EQ(bnode->getDepth(), kNodeDepth + 1);
  EXPECT_DOUBLE_EQ(bnode->getDualBoundValue(), kNodeDualBound + 10.0);
  EXPECT_DOUBLE_EQ(bnode->getInitialDualBoundValue(), kNodeOrigDualBound + 100.0);
  EXPECT_DOUBLE_EQ(bnode->getEstimatedValue(), kNodeEstimatedValue + 1000.0);
  EXPECT_EQ(bnode->getMergingStatus(), Node::NodeMergingStatus::NMS_MERGED);
}  // Upload_Check_No_Throw
