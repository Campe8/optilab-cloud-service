#include <memory>     // for std::shared_ptr
#include <stdexcept>  // for std::invalid_argument
#include <string>
#include <vector>

#include <gtest/gtest.h>

#include "meta_bb/linear_solver.hpp"

class MPSolverFixture : public testing::Test {
 protected:
  void SetUp() override
  {
    pSolverName = "solver_name";
    ASSERT_NO_THROW(pSolver =
          std::make_shared<optilab::metabb::MPSolver>(
              pSolverName, optilab::metabb::MPSolver::OptimizationSolverPackageType::OSPT_SCIP));
  }

  inline const std::string& getSolverName() const noexcept { return pSolverName; }
  inline optilab::metabb::MPSolver::SPtr getSolver() const { return pSolver; }

  void TearDown() override
  {
    ASSERT_NO_THROW(pSolver.reset());
  }

 private:
  std::string pSolverName;
  optilab::metabb::MPSolver::SPtr pSolver;
};

TEST(MPSolver, Constructor_Undef_SolverPackage_Throw)
{
  using namespace optilab;
  using namespace metabb;

  EXPECT_THROW(MPSolver("name", MPSolver::OptimizationSolverPackageType::OSPT_UNDEF),
               std::runtime_error);
}  // Constructor_Undef_SolverPackage_Throw

TEST(MPSolver, Constructor_SCIP_SolverPackage_NoThrow)
{
  using namespace optilab;
  using namespace metabb;

  const std::string solverName = "name";
  MPSolver::SPtr solver;
  ASSERT_NO_THROW(solver =
      std::make_shared<MPSolver>(solverName, MPSolver::OptimizationSolverPackageType::OSPT_SCIP));

  // Check the solver's name
  EXPECT_EQ(solverName, solver->getName());

  // Check that the destructor doesn't throw
  EXPECT_NO_THROW(solver.reset());
}  // Constructor_SCIP_SolverPackage_NoThrow

TEST(MPSolver, Constructor_SCIP_Solve_Empty_Model_NoThrow)
{
  using namespace optilab;
  using namespace metabb;

  const std::string solverName = "name";
  MPSolver::SPtr solver;
  ASSERT_NO_THROW(solver =
      std::make_shared<MPSolver>(solverName, MPSolver::OptimizationSolverPackageType::OSPT_SCIP));

  // Check empty model
  EXPECT_EQ(0, solver->getNumVariables());
  EXPECT_EQ(0, solver->getNumConstraints());

  MPSolver::ResultStatus status;
  ASSERT_NO_THROW(status = solver->solve());

  // Solving an empty model should be optimal
  EXPECT_EQ(MPSolver::ResultStatus::OPTIMAL, status);

  // Check that the destructor doesn't throw
  EXPECT_NO_THROW(solver.reset());
}  // Constructor_Solve_Empty_Model_NoThrow

TEST_F(MPSolverFixture, Build_IntVar_NoThrow)
{
  using namespace optilab;
  using namespace metabb;

  auto solver = getSolver();
  std::shared_ptr<MPVariable> intVar;

  const double lb = -10.0;
  const double ub = +10.0;
  const std::string varName = "var_name";
  ASSERT_NO_THROW(intVar = solver->buildIntVar(lb, ub, varName));

  // Check the variable's values
  EXPECT_TRUE(intVar->isInteger());
  EXPECT_EQ(lb, intVar->lowerBound());
  EXPECT_EQ(ub, intVar->upperBound());
  EXPECT_EQ(varName, intVar->getName());

  // Check that the variable is stored into the solver
  EXPECT_EQ(1, solver->getNumVariables());

  // Query the variable
  std::shared_ptr<MPVariable> qvar;
  ASSERT_NO_THROW(qvar = solver->getVariableByName(varName));
  EXPECT_EQ(intVar.get(), qvar.get());
}  // Build_NumVar_NoThrow

TEST_F(MPSolverFixture, Build_BoolVar_NoThrow)
{
  using namespace optilab;
  using namespace metabb;

  auto solver = getSolver();
  std::shared_ptr<MPVariable> boolVar;

  const double lb = 0.0;
  const double ub = 1.0;
  const std::string varName = "var_name";
  ASSERT_NO_THROW(boolVar = solver->buildBoolVar(varName));

  // Check the variable's values
  EXPECT_TRUE(boolVar->isInteger());
  EXPECT_EQ(lb, boolVar->lowerBound());
  EXPECT_EQ(ub, boolVar->upperBound());
  EXPECT_EQ(varName, boolVar->getName());

  // Check that the variable is stored into the solver
  EXPECT_EQ(1, solver->getNumVariables());

  // Query the variable
  std::shared_ptr<MPVariable> qvar;
  ASSERT_NO_THROW(qvar = solver->getVariableByName(varName));
  EXPECT_EQ(boolVar.get(), qvar.get());
}  // Build_BoolVar_NoThrow

TEST_F(MPSolverFixture, Build_NumVar_NoThrow)
{
  using namespace optilab;
  using namespace metabb;

  auto solver = getSolver();
  std::shared_ptr<MPVariable> numVar;

  const double lb = -10.123;
  const double ub = +10.123;
  const std::string varName = "var_name";
  ASSERT_NO_THROW(numVar = solver->buildNumVar(lb, ub, varName));

  // Check the variable's values
  EXPECT_FALSE(numVar->isInteger());
  EXPECT_EQ(lb, numVar->lowerBound());
  EXPECT_EQ(ub, numVar->upperBound());
  EXPECT_EQ(varName, numVar->getName());

  // Check that the variable is stored into the solver
  EXPECT_EQ(1, solver->getNumVariables());

  // Query the variable
  std::shared_ptr<MPVariable> qvar;
  ASSERT_NO_THROW(qvar = solver->getVariableByName(varName));
  EXPECT_EQ(numVar.get(), qvar.get());
}  // Build_NumVar_NoThrow

TEST_F(MPSolverFixture, Build_NumVar_Array_NoThrow)
{
  using namespace optilab;
  using namespace metabb;

  auto solver = getSolver();
  std::vector<std::shared_ptr<MPVariable>> varArray;

  const int numVar = 2;
  const double lb = -10.123;
  const double ub = +10.123;
  const std::string varName = "var_name";
  ASSERT_NO_THROW(varArray = solver->buildNumVarArray(numVar, lb, ub, varName));

  // Check the array size
  EXPECT_EQ(numVar, static_cast<int>(varArray.size()));

  // Check that the variables are stored into the solver
  EXPECT_EQ(numVar, solver->getNumVariables());

  // Check variable's values
  int idx = 0;
  for (auto var : varArray)
  {
    const std::string vname = varName + "_" + std::to_string(idx++);
    EXPECT_FALSE(var->isInteger());
    EXPECT_EQ(lb, var->lowerBound());
    EXPECT_EQ(ub, var->upperBound());
    EXPECT_EQ(vname, var->getName());

    std::shared_ptr<MPVariable> qvar;
    ASSERT_NO_THROW(qvar = solver->getVariableByName(vname));
    EXPECT_EQ(var.get(), qvar.get());
  }
}  // Build_NumVar_Array_NoThrow

TEST_F(MPSolverFixture, Build_IntVar_Array_NoThrow)
{
  using namespace optilab;
  using namespace metabb;

  auto solver = getSolver();
  std::vector<std::shared_ptr<MPVariable>> varArray;

  const int numVar = 2;
  const double lb = -10.0;
  const double ub = +10.0;
  const std::string varName = "var_name";
  ASSERT_NO_THROW(varArray = solver->buildIntVarArray(numVar, lb, ub, varName));

  // Check the array size
  EXPECT_EQ(numVar, static_cast<int>(varArray.size()));

  // Check that the variables are stored into the solver
  EXPECT_EQ(numVar, solver->getNumVariables());

  // Check variable's values
  int idx = 0;
  for (auto var : varArray)
  {
    const std::string vname = varName + "_" + std::to_string(idx++);
    EXPECT_TRUE(var->isInteger());
    EXPECT_EQ(lb, var->lowerBound());
    EXPECT_EQ(ub, var->upperBound());
    EXPECT_EQ(vname, var->getName());

    std::shared_ptr<MPVariable> qvar;
    ASSERT_NO_THROW(qvar = solver->getVariableByName(vname));
    EXPECT_EQ(var.get(), qvar.get());
  }
}  // Build_IntVar_Array_NoThrow

TEST_F(MPSolverFixture, Build_BoolVar_Array_NoThrow)
{
  using namespace optilab;
  using namespace metabb;

  auto solver = getSolver();
  std::vector<std::shared_ptr<MPVariable>> varArray;

  const int numVar = 2;
  const double lb = 0.0;
  const double ub = 1.0;
  const std::string varName = "var_name";
  ASSERT_NO_THROW(varArray = solver->buildBoolVarArray(numVar, varName));

  // Check the array size
  EXPECT_EQ(numVar, static_cast<int>(varArray.size()));

  // Check that the variables are stored into the solver
  EXPECT_EQ(numVar, solver->getNumVariables());

  // Check variable's values
  int idx = 0;
  for (auto var : varArray)
  {
    const std::string vname = varName + "_" + std::to_string(idx++);
    EXPECT_TRUE(var->isInteger());
    EXPECT_EQ(lb, var->lowerBound());
    EXPECT_EQ(ub, var->upperBound());
    EXPECT_EQ(vname, var->getName());

    std::shared_ptr<MPVariable> qvar;
    ASSERT_NO_THROW(qvar = solver->getVariableByName(vname));
    EXPECT_EQ(var.get(), qvar.get());
  }
}  // Build_BoolVar_Array_NoThrow

TEST_F(MPSolverFixture, Build_Unbounded_RowConstraint_NoThrow)
{
  using namespace optilab;
  using namespace metabb;

  auto solver = getSolver();
  std::shared_ptr<MPConstraint> con;

  const std::string conName = "con_name";
  ASSERT_NO_THROW(con = solver->buildRowConstraint(conName));

  // Check that the constraint is stored into the solver
  EXPECT_EQ(1, solver->getNumConstraints());

  // Check constraint's values
  const double lb = -MPSolver::infinity();
  const double ub = +MPSolver::infinity();
  EXPECT_EQ(lb, con->lowerBound());
  EXPECT_EQ(ub, con->upperBound());
  EXPECT_EQ(conName, con->getName());
  EXPECT_EQ(con.get(), solver->getConstraintByName(conName).get());
}  // Build_Unbounded_RowConstraint_NoThrow

TEST_F(MPSolverFixture, Build_Bouded_RowConstraint_NoThrow)
{
  using namespace optilab;
  using namespace metabb;

  auto solver = getSolver();
  std::shared_ptr<MPConstraint> con;

  const double lb = -10.123;
  const double ub = +10.123;
  const std::string conName = "con_name";
  ASSERT_NO_THROW(con = solver->buildRowConstraint(lb, ub, conName));

  // Check that the constraint is stored into the solver
  EXPECT_EQ(1, solver->getNumConstraints());

  // Check constraint's values
  EXPECT_EQ(lb, con->lowerBound());
  EXPECT_EQ(ub, con->upperBound());
  EXPECT_EQ(conName, con->getName());
  EXPECT_EQ(con.get(), solver->getConstraintByName(conName).get());
}  // Build_Bouded_RowConstraint_NoThrow

TEST_F(MPSolverFixture, Build_And_Reset_Model_NoThrow)
{
  using namespace optilab;
  using namespace metabb;

  auto solver = getSolver();
  std::shared_ptr<MPConstraint> con;

  const double lb = -10.123;
  const double ub = +10.123;
  const std::string conName = "con_name";
  ASSERT_NO_THROW(con = solver->buildRowConstraint(lb, ub, conName));

  const std::string varName = "var_name";
  std::shared_ptr<MPVariable> var;
  ASSERT_NO_THROW(var = solver->buildNumVar(lb, ub, varName));

  // Check the model
  EXPECT_EQ(1, solver->getNumVariables());
  EXPECT_EQ(1, solver->getNumConstraints());

  EXPECT_NO_THROW(solver->reset());

  // Check that the model is still available
  EXPECT_EQ(1, solver->getNumVariables());
  EXPECT_EQ(1, solver->getNumConstraints());
  EXPECT_EQ(var.get(), solver->getVariableByName(varName).get());
  EXPECT_EQ(con.get(), solver->getConstraintByName(conName).get());
}  // Build_And_Reset_Model_NoThrow

TEST_F(MPSolverFixture, Solve_Model_NoThrow)
{
  using namespace optilab;
  using namespace metabb;

  /**
   * Test run for a simple MIP problem:
   * x + 7y <= 17.5
   * x      <= 3.5
   * x      >= 0
   * y      >= 0
   * x, y integers
   */
  auto solver = getSolver();

  std::shared_ptr<MPVariable> var_x;
  std::shared_ptr<MPVariable> var_y;
  const std::string var_x_name = "x";
  const std::string var_y_name = "y";
  ASSERT_NO_THROW(var_x = solver->buildIntVar(0.0, MPSolver::infinity(), var_x_name));
  ASSERT_NO_THROW(var_y = solver->buildIntVar(0.0, MPSolver::infinity(), var_y_name));

  // x + 7 * y <= 17.5.
  std::shared_ptr<MPConstraint> con;
  ASSERT_NO_THROW(con = solver->buildRowConstraint(-MPSolver::infinity(), 17.5, "c0"));
  con->setCoefficient(var_x, 1);
  con->setCoefficient(var_y, 7);

  EXPECT_EQ(2, solver->getNumVariables());
  EXPECT_EQ(1, solver->getNumConstraints());

  // Maximize x + 10 * y.
  MPObjective* objective;
  ASSERT_NO_THROW(objective = solver->getObjectivePtr());
  objective->setCoefficient(var_x, 1);
  objective->setCoefficient(var_y, 10);
  objective->setMaximization();

  MPSolver::ResultStatus resultStatus;
  ASSERT_NO_THROW(resultStatus = solver->solve());

  // Expect optimal result
  EXPECT_EQ(MPSolver::ResultStatus::OPTIMAL, resultStatus);

  // Check result consistency
  EXPECT_EQ(23, objective->getValue());
  EXPECT_EQ(3, var_x->getSolutionValue());
  EXPECT_EQ(2, var_y->getSolutionValue());

  // Check that timers are set
  EXPECT_LE(0, solver->getBuildModelTimeMsec());
  EXPECT_LE(0, solver->getSolveModelTimeMsec());
}  // Solve_Model_NoThrow
