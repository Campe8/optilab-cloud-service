#include <cstdint>  // for uint64_t
#include <memory>   // for std::shared_ptr
#include <utility>  // for std::move

#include <gtest/gtest.h>

#include "meta_bb_mocks.hpp"
#include "meta_bb_net_mocks.hpp"
#include "meta_bb/meta_bb_constants.hpp"
#include "meta_bb/meta_bb_struct.hpp"
#include "meta_bb/net_solver_state.hpp"

namespace {
constexpr bool kRacingStage = true;
constexpr unsigned int kNotificationId = 1;
constexpr int kLMId = 2;
constexpr int kGlobalSubtreeId = 3;
constexpr long long kNumSolvedNodes = 4;
constexpr int kNumNodesLeft = 5;
constexpr double kBestDualBoundValue = 100.123;
constexpr double kGlobalBestPrimalBoundValue = 200.123;
uint64_t kTimeMsec= 6;
constexpr double kAvgDualBoundGainRecv = 300.123;
}  // namespace

TEST(NetSolverState, Send_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const int destination{0};
  NetSolverState solverState(kRacingStage,
                             kNotificationId,
                             kLMId,
                             kGlobalSubtreeId,
                             kNumSolvedNodes,
                             kNumNodesLeft,
                             kBestDualBoundValue,
                             kGlobalBestPrimalBoundValue,
                             kTimeMsec,
                             kAvgDualBoundGainRecv);

  auto communicator = std::make_shared<optnet::MockNetworkCommunicator>();
  auto framework = std::make_shared<MockFramework>(communicator);
  solverState.send(framework, destination);

  EXPECT_TRUE(communicator->lastSend);
  EXPECT_EQ(communicator->nodeId, destination);
  EXPECT_EQ(communicator->lastTag,
            metabb::net::MetaBBPacketTag::PacketTagType::PPT_SOLVER_STATE);
}  // Send_No_Throw

TEST(NetSolverState, Upload_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  // Create a solver state
  MockNetSolverState solverState(kRacingStage,
                                 kNotificationId,
                                 kLMId,
                                 kGlobalSubtreeId,
                                 kNumSolvedNodes,
                                 kNumNodesLeft,
                                kBestDualBoundValue,
                                kGlobalBestPrimalBoundValue,
                                kTimeMsec,
                                kAvgDualBoundGainRecv);

  // Create an empty solver state
  NetSolverState uploadToState;

  // Verify that its members are different from the solverState to upload
  EXPECT_NE(solverState.isRacingStage(), uploadToState.isRacingStage());
  EXPECT_NE(solverState.getNotificationId(), uploadToState.getNotificationId());
  EXPECT_NE(solverState.getLMId(), uploadToState.getLMId());
  EXPECT_NE(solverState.getGlobalSubtreeId(), uploadToState.getGlobalSubtreeId());
  EXPECT_NE(solverState.getSolverLocalBestDualBoundValue(),
            uploadToState.getSolverLocalBestDualBoundValue());
  EXPECT_NE(solverState.getGlobalBestPrimalBoundValue(),
            uploadToState.getGlobalBestPrimalBoundValue());
  EXPECT_NE(solverState.getNumNodesSolved(), uploadToState.getNumNodesSolved());
  EXPECT_NE(solverState.getNumNodesLeft(), uploadToState.getNumNodesLeft());
  EXPECT_NE(solverState.getTimeMsec(), uploadToState.getTimeMsec());
  EXPECT_NE(solverState.getAverageDualBoundGainRecv(), uploadToState.getAverageDualBoundGainRecv());

  auto packet = solverState.buildNetworkPacket();
  packet->packetTag = optnet::PacketTag::UPtr(new net::MetaBBPacketTag(
      net::MetaBBPacketTag::PacketTagType::PPT_SOLVER_STATE));
  ASSERT_EQ(uploadToState.upload(std::move(packet)), 0);

  // Verify that the states have the same value
  EXPECT_EQ(solverState.isRacingStage(), uploadToState.isRacingStage());
  EXPECT_EQ(solverState.getNotificationId(), uploadToState.getNotificationId());
  EXPECT_EQ(solverState.getLMId(), uploadToState.getLMId());
  EXPECT_EQ(solverState.getGlobalSubtreeId(), uploadToState.getGlobalSubtreeId());
  EXPECT_DOUBLE_EQ(solverState.getSolverLocalBestDualBoundValue(),
                   uploadToState.getSolverLocalBestDualBoundValue());
  EXPECT_DOUBLE_EQ(solverState.getGlobalBestPrimalBoundValue(),
                   uploadToState.getGlobalBestPrimalBoundValue());
  EXPECT_EQ(solverState.getNumNodesSolved(), uploadToState.getNumNodesSolved());
  EXPECT_EQ(solverState.getNumNodesLeft(), uploadToState.getNumNodesLeft());
  EXPECT_EQ(solverState.getTimeMsec(), uploadToState.getTimeMsec());
  EXPECT_DOUBLE_EQ(solverState.getAverageDualBoundGainRecv(),
                   uploadToState.getAverageDualBoundGainRecv());
}  // Upload_Check_No_Throw
