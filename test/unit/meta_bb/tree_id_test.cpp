#include <memory>     // for std::unique_ptr

#include <gtest/gtest.h>

#include "meta_bb/tree_id.hpp"

TEST(TreeId, Subtree_Id_Constructor_Default_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  std::unique_ptr<SubtreeId> stid;
  ASSERT_NO_THROW(stid = std::unique_ptr<SubtreeId>(new SubtreeId()));
  ASSERT_TRUE(!!stid);

  EXPECT_EQ(stid->getLMId(), treeconsts::DEFAULT_UNSPECIFIED_ID);
  EXPECT_EQ(stid->getGlobalSubstreeIdInLM(), treeconsts::DEFAULT_UNSPECIFIED_ID);
  EXPECT_EQ(stid->getSolverId(), treeconsts::DEFAULT_UNSPECIFIED_ID);
}  // Subtree_Id_Constructor_Default_No_Throw

TEST(TreeId, Subtree_Id_Constructor_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const int lmid = 0;
  const int glbst = 1;
  const int solverid = 2;
  std::unique_ptr<SubtreeId> stid;
  ASSERT_NO_THROW(stid = std::unique_ptr<SubtreeId>(new SubtreeId(lmid, glbst, solverid)));
  ASSERT_TRUE(!!stid);

  EXPECT_EQ(stid->getLMId(), lmid);
  EXPECT_EQ(stid->getGlobalSubstreeIdInLM(), glbst);
  EXPECT_EQ(stid->getSolverId(), solverid);
}  // Subtree_Id_Constructor_No_Throw

TEST(TreeId, Subtree_Id_Eq)
{
  using namespace optilab;
  using namespace metabb;

  const int lmid = 0;
  const int glbst = 1;
  const int solverid = 2;
  std::unique_ptr<SubtreeId> stid1;
  std::unique_ptr<SubtreeId> stid2;
  ASSERT_NO_THROW(stid1 = std::unique_ptr<SubtreeId>(new SubtreeId(lmid, glbst, solverid)));
  ASSERT_NO_THROW(stid2 = std::unique_ptr<SubtreeId>(new SubtreeId(lmid, glbst, solverid)));
  ASSERT_TRUE(!!stid1);
  ASSERT_TRUE(!!stid2);

  EXPECT_TRUE(*stid1 == *stid2);
  EXPECT_FALSE(*stid1 != *stid2);
  EXPECT_FALSE(*stid1 < *stid2);
}  // Subtree_Id_Eq

TEST(TreeId, Subtree_Id_Not_Eq)
{
  using namespace optilab;
  using namespace metabb;

  const int lmid = 0;
  const int glbst = 1;
  const int solverid = 2;
  std::unique_ptr<SubtreeId> stid1;
  std::unique_ptr<SubtreeId> stid2;
  ASSERT_NO_THROW(stid1 = std::unique_ptr<SubtreeId>(new SubtreeId(lmid, glbst, solverid)));
  ASSERT_NO_THROW(stid2 = std::unique_ptr<SubtreeId>(new SubtreeId(lmid+1, glbst, solverid)));
  ASSERT_TRUE(!!stid1);
  ASSERT_TRUE(!!stid2);

  EXPECT_FALSE(*stid1 == *stid2);
  EXPECT_TRUE(*stid1 != *stid2);
  EXPECT_TRUE(*stid1 < *stid2);
}  // Subtree_Id_Not_Eq


TEST(TreeId, Node_Id_Constructor_Default_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  std::unique_ptr<NodeId> nid;
  ASSERT_NO_THROW(nid = std::unique_ptr<NodeId>(new NodeId()));
  ASSERT_TRUE(!!nid);

  SubtreeId stid;
  EXPECT_EQ(nid->getSeqNum(), treeconsts::DEFAULT_UNSPECIFIED_ID);
  EXPECT_TRUE(nid->getSubtreeId() == stid);
}  // Node_Id_Constructor_Default_No_Throw

TEST(TreeId, Node_Id_Constructor_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const int lmid = 0;
  const int glbst = 1;
  const int solverid = 2;
  SubtreeId stid(lmid, glbst, solverid);

  const long long seqIdNum = 1000;
  std::unique_ptr<NodeId> nid;
  ASSERT_NO_THROW(nid = std::unique_ptr<NodeId>(new NodeId(seqIdNum, stid)));
  ASSERT_TRUE(!!nid);

  EXPECT_EQ(nid->getSeqNum(), seqIdNum);
  EXPECT_TRUE(nid->getSubtreeId() == stid);
}  // Subtree_Id_Constructor_No_Throw

TEST(TreeId, Node_Id_Eq)
{
  using namespace optilab;
  using namespace metabb;

  const int lmid = 0;
  const int glbst = 1;
  const int solverid = 2;
  SubtreeId stid(lmid, glbst, solverid);

  const long long seqIdNum = 1000;
  NodeId nid1(seqIdNum, stid);
  NodeId nid2(seqIdNum, stid);

  EXPECT_TRUE(nid1 == nid2);
  EXPECT_FALSE(nid1 != nid2);
  EXPECT_FALSE(nid1 < nid2);
}  // Subtree_Id_Eq

TEST(TreeId, Node_Id_Not_Eq)
{
  using namespace optilab;
  using namespace metabb;

  const int lmid = 0;
  const int glbst = 1;
  const int solverid = 2;
  SubtreeId stid(lmid, glbst, solverid);

  // Same subtree but different sequence ids
  const long long seqIdNum1 = 1000;
  const long long seqIdNum2 = 1001;
  NodeId nid1(seqIdNum1, stid);
  NodeId nid2(seqIdNum2, stid);

  EXPECT_FALSE(nid1 == nid2);
  EXPECT_TRUE(nid1 != nid2);
  EXPECT_TRUE(nid1 < nid2);
}  // Node_Id_Not_Eq

