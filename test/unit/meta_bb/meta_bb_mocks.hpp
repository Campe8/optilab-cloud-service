#pragma once

#include <cstdint>  // for uint64_t
#include <limits>   // for std::numeric_limits
#include <memory>   // for std::shared_ptr

// #include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "meta_bb/calculation_state.hpp"
#include "meta_bb/diff_subproblem.hpp"
#include "meta_bb/framework.hpp"
#include "meta_bb/initial_stat.hpp"
#include "meta_bb/initiator.hpp"
#include "meta_bb/instance.hpp"
#include "meta_bb/load_manager.hpp"
#include "meta_bb/load_manager_impl.hpp"
#include "meta_bb/meta_bb_struct.hpp"
#include "meta_bb/meta_bb_utilities.hpp"
#include "meta_bb/node.hpp"
#include "meta_bb/paramset.hpp"
#include "meta_bb/racing_rampup_paramset.hpp"
#include "meta_bb/solution.hpp"
#include "meta_bb/solver.hpp"
#include "meta_bb/solver_impl.hpp"
#include "meta_bb/solver_pool.hpp"
#include "meta_bb/solver_state.hpp"
#include "meta_bb/solver_termination_state.hpp"
#include "optimizer_network/network_communicator.hpp"
#include "utilities/timer.hpp"

namespace {
constexpr int kInstanceNumVariables{100};
constexpr int kNoError{0};
constexpr double kNodePoolBGap{0.10};
constexpr double kGlobalBestDualBound{-1000.0};
constexpr double kObjectiveFunctionValue{1000.0};
constexpr long long kNumNodesSolved{5};
constexpr int kOriginRank{1};
constexpr int kCalculationStateNodesSolved{1};
}  // namespace

namespace optilab {
namespace  optnet {
class MockNetworkCommunicator : public NetworkCommunicator {
 public:
  using SPtr = std::shared_ptr<MockNetworkCommunicator>;

 public:
  MockNetworkCommunicator()
  : NetworkCommunicator(),
    nodeId(-1),
    numSend(0),
    numReceive(0),
    numBroadcast(0),
    lastSend(false),
    lastReceive(false),
    lastBroadcast(false),
    lastTag(metabb::net::MetaBBPacketTag::PacketTagType::PPT_UNDEF)
  {
    nodeId = -1;
  }

  ~MockNetworkCommunicator() override {};

  // Check members for method calls
  int nodeId;
  int numSend;
  int numReceive;
  int numBroadcast;
  bool lastSend;
  bool lastReceive;
  bool lastBroadcast;
  metabb::net::MetaBBPacketTag::PacketTagType lastTag;

  // Methods
  void turnDown() override {};
  int init(int) override { return kNoError; };
  int getRank() const noexcept override { return 0; }
  int getSize() const noexcept override { return 5; }
  void abort() override {};
  int broadcast(Packet::UPtr& packet, const int root) noexcept override
  {
    nodeId = root;
    numBroadcast++;
    lastBroadcast = true;
    return kNoError;
  }

  int send(Packet::UPtr packet, const int dest) noexcept override
  {
    nodeId = dest;
    numSend++;
    lastSend = true;
    lastTag = metabb::utils::castToMetaBBPacketTagAndGetTagOrThrow(packet->packetTag);
    return kNoError;
  }

  int receive(Packet::UPtr& packet, const int source) noexcept override
  {
    nodeId = source;
    numReceive++;
    lastReceive = true;
    lastTag = metabb::utils::castToMetaBBPacketTagAndGetTagOrThrow(packet->packetTag);
    return kNoError;
  }

  int ireceive(Packet::UPtr& packet, const int source, bool* err=nullptr) noexcept override
  {
    return kNoError;
  }
  int probe(Packet::UPtr& packet) noexcept override { return kNoError; }
  int iprobe(Packet::UPtr& packet, bool* err=nullptr) noexcept override { return kNoError; }
  int waitSpecTagFromSpecSource(Packet::UPtr& packet, const int source,
                                bool replaceTag=true) noexcept override { return kNoError; }
};
}  // optnet
}  // optilab

namespace optilab {
namespace  metabb {

class MockInitialStat : public InitialStat {
public:
  using SPtr = std::shared_ptr<MockInitialStat>;

public:
  MockInitialStat() = default;
  virtual ~MockInitialStat() = default;

  int getMaxDepth() override { return 10; };
  InitialStat::SPtr clone(const Framework::SPtr&) override
  {
    return std::make_shared<MockInitialStat>();
  }
  int send(const Framework::SPtr&, int) noexcept override { return 0; }
  int upload(optnet::Packet::UPtr) noexcept override { return 0; }
};


class MockSolution : public Solution {
 public:
  using SPtr = std::shared_ptr<MockSolution>;
 public:
  MockSolution(double objVal=kObjectiveFunctionValue) : pObjVal(objVal) {}
  double getObjectiveFuntionValue() override { return pObjVal; }
  Solution::SPtr clone() override { return std::make_shared<MockSolution>(); }
  int broadcast(const Framework::SPtr& framework, int root) noexcept override { return 0; }
  int send(const Framework::SPtr& framework, int destination) noexcept override { return 0; }
  int upload(optnet::Packet::UPtr packet) noexcept override { return 0; }
  double getCutOffValue() override { return 1.0; }
 private:
  double pObjVal{kObjectiveFunctionValue};
};

class MockSolverState : public SolverState {
 public:
  using SPtr = std::shared_ptr<MockSolverState>;

 public:
  MockSolverState() : SolverState() {}
  MockSolverState(bool racingStage,
                  unsigned int notificationId,
                  int lmId,
                  int globalSubtreeId,
                  long long numSolvedNodes,
                  int numNodesLeft,
                  double bestDualBoundValue,
                  double globalBestPrimalBoundValue,
                  uint64_t timeMsec,
                  double avgDualBoundGainRecv)
  : SolverState(racingStage, notificationId, lmId, globalSubtreeId, numSolvedNodes,
                numNodesLeft, bestDualBoundValue, globalBestPrimalBoundValue,
                timeMsec, avgDualBoundGainRecv)
  {
  }

  void setRacingStage() { pRacingStage = true; }
  int send(const Framework::SPtr& fr, int destination) noexcept override { return 0; }
  int upload(optnet::Packet::UPtr packet) noexcept override { return 0; }
};

class MockCalculationState : public CalculationState {
 public:
  MockCalculationState(solver::TerminationState terminationState,
                       int numNodesSolver = kCalculationStateNodesSolved)
  : CalculationState()
 {
    pNodesSolved = numNodesSolver;
    pTerminationState = terminationState;
 }

  MockCalculationState(uint64_t compTimeMsec,
                       uint64_t rootTimeMsec,
                       int numNodesSolved,
                       int numNodesSent,
                       int numImprovedIncumbent,
                       solver::TerminationState terminationState,
                       int numNodesSolverWithoutPreprocess,
                       std::size_t numSimplexIterRoot,
                       double avgSimplexIter,
                       std::size_t numTransferredLocalCuts,
                       std::size_t minTransferredLocalCuts,
                       std::size_t maxTransferredLocalCuts,
                       std::size_t numRestarts,
                       double minInfeasibilitySum,
                       double maxInfeasibilitySum,
                       std::size_t minInfeasibilityNum,
                       std::size_t maxInfeasibilityNum,
                       double dualBound)
 : CalculationState(compTimeMsec,
                    rootTimeMsec,
                    numNodesSolved,
                    numNodesSent,
                    numImprovedIncumbent,
                    terminationState,
                    numNodesSolverWithoutPreprocess,
                    numSimplexIterRoot,
                    avgSimplexIter,
                    numTransferredLocalCuts,
                    minTransferredLocalCuts,
                    maxTransferredLocalCuts,
                    numRestarts,
                    minInfeasibilitySum,
                    maxInfeasibilitySum,
                    minInfeasibilityNum,
                    maxInfeasibilityNum,
                    dualBound)
 {
 }

  int send(const Framework::SPtr& fr, int destination) noexcept override { return 0; }
  int upload(optnet::Packet::UPtr packet) noexcept override { return 0; }
};

class MockRacingRampUpParamSet : public RacingRampUpParamSet {
 public:
  using SPtr = std::shared_ptr<MockRacingRampUpParamSet>;

 public:
  MockRacingRampUpParamSet() : RacingRampUpParamSet() {}
  MockRacingRampUpParamSet(RacingRampUpParamSet::RacingTerminationCriteria termCriteria,
                           int numNodesLeft,
                           uint64_t timeoutMsec=std::numeric_limits<uint64_t>::max())
  : RacingRampUpParamSet(termCriteria, numNodesLeft, timeoutMsec)
  {
  }

  int send(Framework::SPtr framework, int destination) override { return 0; }
  int upload(optnet::Packet::UPtr packet) override { return 0; }
  int getStrategy() override { return 0; }
};

class MockInstance : public Instance {
public:
  std::string getName() const noexcept override { return "MockInstance"; }
  int getVarIndexRange() const noexcept override { return kInstanceNumVariables; }
  int broadcast(const Framework::SPtr& comm, int root) noexcept override { return 0; }
};

class MockDiffSubproblem : public DiffSubproblem {
 public:
  using SPtr = std::shared_ptr<MockDiffSubproblem>;

 public:
  MockDiffSubproblem(int numBoundChanges = 0, int numFixedVariables = 0)
 : pNumBoundChanges(numBoundChanges),
   pNumFixedVariables(numFixedVariables)
 {
 }

  DiffSubproblem::SPtr clone(const Framework::SPtr&) override { return nullptr; }
  int getNBoundChanges() const noexcept override { return pNumBoundChanges; }
  int getFixedVariables(const Instance::SPtr,
                        std::vector<mergenodes::FixedVariable::SPtr>& fvList) override
  {
    double val{1.0};
    for (int idx = 0; idx < pNumFixedVariables; ++idx)
    {
      auto fvar = std::make_shared<mergenodes::FixedVariable>();
      fvar->value = val;
      val += 1.0;
      fvar->numSameValue = 0;
      fvar->index = idx;
      fvList.push_back(fvar);
    }
    return static_cast<int>(fvList.size());
  }
  DiffSubproblem::SPtr createDiffSubproblem(
          Framework::SPtr, std::shared_ptr<Initiator>,
          const std::vector<mergenodes::FixedVariable*>&) override
  {
    return nullptr;
  }
  int send(const Framework::SPtr&, int) noexcept override { return 0; }
  int receive(const Framework::SPtr&, int) noexcept override { return 0; }
  int broadcast(const Framework::SPtr& framework, int root) noexcept override { return 0; }

 private:
  int pNumBoundChanges;
  int pNumFixedVariables;
};

class MockInitiator : public Initiator {
 public:
  MockInitiator(Framework::SPtr framework, timer::Timer::SPtr timer=nullptr)
 : Initiator(framework, timer)
 {
 }

  int init(ParamSet::SPtr) override { return 0; }
  int reInit(int) override { return 0; }
  Instance::SPtr getInstance() override
  {
    return std::make_shared<MockInstance>();
  }

  DiffSubproblem::SPtr buildRootNodeDiffSubproblem() override
  {
    return std::make_shared<MockDiffSubproblem>();
  }

  void sendSolverInitializationMessage() override {}
  void generateRacingRampUpParameterSets(std::vector<RacingRampUpParamSet::SPtr>&) override {}
  double convertToExternalValue(double internalValue) override { return internalValue; }

  Solution::SPtr getGlobalBestIncumbentSolution() override
  {
    return std::make_shared<MockSolution>(kObjectiveFunctionValue + 1.0);
  }

  bool tryToSetIncumbentSolution(Solution::SPtr, bool) { return true; }
  double getAbsGap(double) override { return 0.0; }
  double getGap(double) override { return 0.0; }
  double getAbsGapValue() override { return 0.0; }
  double getGapValue() override { return 0.0; }
  double getEpsilon() const noexcept override { return 0.0; }
  void setFinalSolverStatus(solver::FinalSolverState status) noexcept override {}
  void setNumSolvedNodes(long long numNodes) noexcept override {}
  void setDualBound(double bound) noexcept override {}
  void accumulateInitialStat(InitialStat::SPtr initialStat) override {}
  bool isFeasibleSolution() override { return true; }
  void setInitialStatOnDiffSubproblem(int minDepth, int maxDepth,
                                      DiffSubproblem::SPtr diffSubproblem) override {}
  void writeSolution(const std::string& message) override {}
};

class MockNode : public Node {
 public:
  using SPtr = std::shared_ptr<MockNode>;

 public:
  MockNode() : Node() {}

  MockNode(const NodeId& nodeId,
           const NodeId& generatorNodeId,
           int depth,
           double dualBoundValue,
           double originalDualBoundValue,
           double estimatedValue,
           DiffSubproblem::SPtr diffSubproblem)
 : Node(nodeId, generatorNodeId, depth, dualBoundValue, originalDualBoundValue,
        estimatedValue, diffSubproblem)
 {
 }

  Node::SPtr clone(const Framework::SPtr&) override
  {
    return nullptr;
  }

  int broadcast(const Framework::SPtr&, int) noexcept override
  {
    return 0;
  }

  int send(const Framework::SPtr&, int) noexcept override
  {
    return 0;
  }

  int upload(const Framework::SPtr& framework, optnet::Packet::UPtr) noexcept override { return 0; }
};

class MockSolverTerminationState : public SolverTerminationState {
 public:
  using SPtr = std::shared_ptr<MockSolverTerminationState>;

 public:
  MockSolverTerminationState() : SolverTerminationState() {}
  MockSolverTerminationState(
      solver::InterruptionPoint interrupted,
      int rank,
      int totalNumSolved,
      int minNumSolved,
      int maxNumSolved,
      int totalNumSent,
      int totalNumImprovedIncumbent,
      int numNodesReceived,
      int numNodesSolved,
      int numNodesSolvedAtRoot,
      int numNodesSolvedAtPreCheck,
      int numTransferredLocalCutsFromSolver,
      int minNumTransferredLocalCutsFromSolver,
      int maxNUmTransferredLocalCutsFromSolver,
      int numTotalRestarts,
      int minNumRestarts,
      int maxNumRestarts,
      int numVarTightened,
      int numIntVarTightenedInt,
      uint64_t runningTimeMsec,
      uint64_t idleTimeToFirstNodeMsec,
      uint64_t idleTimeBetweenNodesMsec,
      uint64_t iddleTimeAfterLastNodeMsec,
      uint64_t idleTimeToWaitNotificationIdMsec,
      uint64_t idleTimeToWaitAckCompletionMsec,
      uint64_t idleTimeToWaitTokenMsec,
      uint64_t totalRootNodeTimeMsec,
      uint64_t minRootNodeTimeMsec,
      uint64_t maxRootNodeTimeMsec)
  : SolverTerminationState(interrupted,
                           rank,
                           totalNumSolved,
                           minNumSolved,
                           maxNumSolved,
                           totalNumSent,
                           totalNumImprovedIncumbent,
                           numNodesReceived,
                           numNodesSolved,
                           numNodesSolvedAtRoot,
                           numNodesSolvedAtPreCheck,
                           numTransferredLocalCutsFromSolver,
                           minNumTransferredLocalCutsFromSolver,
                           maxNUmTransferredLocalCutsFromSolver,
                           numTotalRestarts,
                           minNumRestarts,
                           maxNumRestarts,
                           numVarTightened,
                           numIntVarTightenedInt,
                           runningTimeMsec,
                           idleTimeToFirstNodeMsec,
                           idleTimeBetweenNodesMsec,
                           iddleTimeAfterLastNodeMsec,
                           idleTimeToWaitNotificationIdMsec,
                           idleTimeToWaitAckCompletionMsec,
                           idleTimeToWaitTokenMsec,
                           totalRootNodeTimeMsec,
                           minRootNodeTimeMsec,
                           maxRootNodeTimeMsec)
  {
  }

  inline void setInterruptedMode(solver::InterruptionPoint ipoint)
  {
    pInterrupted = ipoint;
  }

  int send(const Framework::SPtr& framework, int destination) noexcept override { return 0; }

  int upload(optnet::Packet::UPtr packet) noexcept override { return 0; }
};

class MockSolverPool : public SolverPool {
 public:
  MockSolverPool(double MP, double BGap, double MBGap, int origRank,
                 Framework::SPtr framework, ParamSet::SPtr paramSet,
                 timer::Timer::SPtr timer = nullptr)
 : SolverPool(MP, BGap, MBGap, origRank, framework, paramSet, timer)
 {
    pSelectionHeap = std::make_shared<DescendingSelectionHeap>(pNumSolvers);
 }

  double getGlobalBestDualBoundValue() override { return kGlobalBestDualBound; }
  void switchIntoCollectingMode(NodePool::SPtr) override {}
  void updateSolverStatus(int, uint64_t, int, double, NodePool::SPtr) noexcept override {}
};

class MockFramework : public Framework {
public:
  using SPtr = std::shared_ptr<MockFramework>;

public:
  explicit MockFramework(optnet::NetworkBaseCommunicator::SPtr comm)
  : Framework(comm),
    pIsRacing(false),
    pTerminationState(solver::TerminationState::TS_TERMINATED_NORMALLY),
    pInterruptionPoint(solver::InterruptionPoint::IP_INTERRUPTED_CHECKPOINT)
  {
  }

  inline void setTerminationStateForSolvers(solver::TerminationState state)
  {
    pTerminationState = state;
  }

  inline void setInterruptionPointForSolvers(solver::InterruptionPoint interruption)
  {
    pInterruptionPoint = interruption;
  }

  inline void setIsRacing() { pIsRacing = true; }

  std::shared_ptr<ParamSet> buildParamSet() override
  {
    return std::make_shared<ParamSet>();
  }

  std::shared_ptr<RacingRampUpParamSet> buildRacingRampUpParamSet() override
  {
    return std::make_shared<MockRacingRampUpParamSet>();
  }

  std::shared_ptr<CalculationState> buildCalculationState() override
  {
    return std::make_shared<MockCalculationState>(pTerminationState);
  }

  std::shared_ptr<CalculationState> buildCalculationState(
        uint64_t compTimeMsec,
        uint64_t rootTimeMsec,
        int numNodesSolved,
        int numNodesSent,
        int numImprovedIncumbent,
        solver::TerminationState terminationState,
        int numNodesSolverWithoutPreprocess,
        int numSimplexIterRoot,
        double avgSimplexIter,
        int numTransferredLocalCuts,
        int minTransferredLocalCuts,
        int maxTransferredLocalCuts,
        int numRestarts,
        double minInfeasibilitySum,
        double maxInfeasibilitySum,
        int minInfeasibilityNum,
        int maxInfeasibilityNum,
        double dualBound) override { return nullptr; }

  std::shared_ptr<InitialStat> buildInitialStat() override
  {
    return std::make_shared<MockInitialStat>();
  }

  std::shared_ptr<Node> buildNode() override { return std::make_shared<MockNode>(); }
  std::shared_ptr<Node> buildNode(
        NodeId nodeId,
        NodeId generatorNodeId,
        int depth,
        double dualBoundValue,
        double originalDualBoundValue,
        double estimatedValue,
        std::shared_ptr<DiffSubproblem> diffSubproblem) override
  {
    return std::make_shared<MockNode>();
  }

  std::shared_ptr<SolverState> buildSolverState() override
  {
    auto solverState = std::make_shared<MockSolverState>();
    if (pIsRacing)
    {
      solverState->setRacingStage();
    }
    return solverState;
  }

  std::shared_ptr<SolverState> buildSolverState(
        int racingStage,
        unsigned int notificationId,
        int LMId,
        int globalSubtreeId,
        long long nodesSolved,
        int nodesLeft,
        double bestDualBoundValue,
        double globalBestPrimalBoundValue,
        uint64_t timeMsec,
        double averageDualBoundGain) override
  {
    return std::make_shared<MockSolverState>(racingStage, notificationId, LMId, globalSubtreeId,
                                             nodesSolved, nodesLeft, bestDualBoundValue,
                                             globalBestPrimalBoundValue, timeMsec,
                                             averageDualBoundGain);
  }

  std::shared_ptr<SolverTerminationState> buildSolverTerminationState() override
  {
    auto state = std::make_shared<MockSolverTerminationState>();
    state->setInterruptedMode(pInterruptionPoint);
    return state;
  }
  std::shared_ptr<SolverTerminationState> buildSolverTerminationState(
        solver::InterruptionPoint terminationMode,
        int rank,
        int totalNumSolved,
        int minNumSolved,
        int maxNumSolved,
        int totalNumSent,
        int totalNumImprovedIncumbent,
        int numNodesReceived,
        int numNodesSolved,
        int numNodesSolvedAtRoot,
        int numNodesSolvedAtPreCheck,
        int numTransferredLocalCutsFromSolver,
        int minNumTransferredLocalCutsFromSolver,
        int maxNUmTransferredLocalCutsFromSolver,
        int numTotalRestarts,
        int minNumRestarts,
        int maxNumRestarts,
        int numVarTightened,
        int numIntVarTightenedInt,
        uint64_t runningTimeMsec,
        uint64_t idleTimeToFirstNodeMsec,
        uint64_t idleTimeBetweenNodesMsec,
        uint64_t iddleTimeAfterLastNodeMsec,
        uint64_t idleTimeToWaitNotificationIdMsec,
        uint64_t idleTimeToWaitAckCompletionMsec,
        uint64_t idleTimeToWaitTokenMsec,
        uint64_t totalRootNodeTimeMsec,
        uint64_t minRootNodeTimeMsec,
        uint64_t maxRootNodeTimeMsec) override
  {
    return std::make_shared<MockSolverTerminationState>(terminationMode,
                                                        rank,
                                                        totalNumSolved,
                                                        minNumSolved,
                                                        maxNumSolved,
                                                        totalNumSent,
                                                        totalNumImprovedIncumbent,
                                                        numNodesReceived,
                                                        numNodesSolved,
                                                        numNodesSolvedAtRoot,
                                                        numNodesSolvedAtPreCheck,
                                                        numTransferredLocalCutsFromSolver,
                                                        minNumTransferredLocalCutsFromSolver,
                                                        maxNUmTransferredLocalCutsFromSolver,
                                                        numTotalRestarts,
                                                        minNumRestarts,
                                                        maxNumRestarts,
                                                        numVarTightened,
                                                        numIntVarTightenedInt,
                                                        runningTimeMsec,
                                                        idleTimeToFirstNodeMsec,
                                                        idleTimeBetweenNodesMsec,
                                                        iddleTimeAfterLastNodeMsec,
                                                        idleTimeToWaitNotificationIdMsec,
                                                        idleTimeToWaitAckCompletionMsec,
                                                        idleTimeToWaitTokenMsec,
                                                        totalRootNodeTimeMsec,
                                                        minRootNodeTimeMsec,
                                                        maxRootNodeTimeMsec);
  }

  std::shared_ptr<DiffSubproblem> buildDiffSubproblem() override
  {
    return std::make_shared<MockDiffSubproblem>();
  }

  std::shared_ptr<Solution> buildSolution() override { return std::make_shared<MockSolution>(); }

private:
  bool pIsRacing;
  solver::TerminationState pTerminationState;
  solver::InterruptionPoint pInterruptionPoint;
};

class MockSolverImpl : public SolverImpl {
 public:
  using SPtr = std::shared_ptr<MockSolverImpl>;

 public:
  MockSolverImpl(Solver* solver) : SolverImpl(solver) {}

  inline int tagNode() const noexcept { return pProcessTagNode; }
  inline int tagNodeReceived() const noexcept { return pProcessTagNodeReceived; }
  inline int tagRampUp() const noexcept { return pProcessTagRampUp; }
  inline int tagRetryRampUp() const noexcept { return pProcessTagRetryRampUp; }
  inline int tagSolution() const noexcept { return pProcessTagSolution; }
  inline int tagIncumbentValue() const noexcept { return pProcessTagIncumbentValue; }
  inline int tagGlobalBestDualBoundValueAtWarmStart() const noexcept
  {
    return pProcessTagGlobalBestDualBoundValueAtWarmStart;
  }
  inline int tagNoNodes() const noexcept { return pProcessTagNoNodes; }
  inline int tagInCollectingMode() const noexcept { return pProcessTagInCollectingMode; }
  inline int tagCollectAllNodes() const noexcept { return pProcessTagCollectAllNodes; }
  inline int tagOutCollectingMode() const noexcept { return pProcessTagOutCollectingMode; }
  inline int tagLMBestBoundValue() const noexcept { return pProcessTagLMBestBoundValue; }
  inline int tagNotificationId() const noexcept { return pProcessTagNotificationId; }
  inline int tagTerminateRequest() const noexcept { return pProcessTagTerminateRequest; }
  inline int tagInterruptRequest() const noexcept { return pProcessTagInterruptRequest; }
  inline int tagWinnerRacingRampUpParamSet() const noexcept
  {
    return pProcessTagWinnerRacingRampUpParamSet;
  }
  inline int tagWinner() const noexcept { return pProcessTagWinner; }
  inline int tagLightWeightRootNodeProcess() const noexcept
  {
    return pProcessTagLightWeightRootNodeProcess;
  }
  inline int tagBreaking() const noexcept { return pProcessTagBreaking; }
  inline int tagTerminateSolvingToRestart() const noexcept
  {
    return pProcessTagTerminateSolvingToRestart;
  }
  inline int tagGivenGapIsReached() const noexcept { return pProcessTagGivenGapIsReached; }
  inline int tagTestDualBoundGain() const noexcept { return pProcessTagTestDualBoundGain; }
  inline int tagNoTestDualBoundGain() const noexcept { return pProcessTagNoTestDualBoundGain; }
  inline int tagNoWaitModeSend() const noexcept { return pProcessTagNoWaitModeSend; }
  inline int tagRestart() const noexcept { return pProcessTagRestart; }
  inline int tagLbBoundTightened() const noexcept { return pProcessTagLbBoundTightened; }
  inline int tagUbBoundTightened() const noexcept { return pProcessTagUbBoundTightened; }
  inline int tagCutOffValue() const noexcept { return pProcessTagCutOffValue; }

 protected:
  int processTagNode(int, optnet::Packet::UPtr) override
  {
    pProcessTagNode++;
    return 0;
  }

  int processTagNodeReceived(int, optnet::Packet::UPtr) override
  {
    pProcessTagNodeReceived++;
    return 0;
  }

  int processTagRampUp(int, optnet::Packet::UPtr) override
  {
    pProcessTagRampUp++;
    return 0;
  }

  int processTagRetryRampUp(int, optnet::Packet::UPtr) override
  {
    pProcessTagRetryRampUp++;
    return 0;
  }

  int processTagSolution(int, optnet::Packet::UPtr) override
  {
    pProcessTagSolution++;
    return 0;
  }

  int processTagIncumbentValue(int, optnet::Packet::UPtr) override
  {
    pProcessTagIncumbentValue++;
    return 0;
  }

  int processTagGlobalBestDualBoundValueAtWarmStart(int, optnet::Packet::UPtr) override
  {
    pProcessTagGlobalBestDualBoundValueAtWarmStart++;
    return 0;
  }

  int processTagNoNodes(int, optnet::Packet::UPtr) override
  {
    pProcessTagNoNodes++;
    return 0;
  }

  int processTagInCollectingMode(int, optnet::Packet::UPtr) override
  {
    pProcessTagInCollectingMode++;
    return 0;
  }

  int processTagCollectAllNodes(int, optnet::Packet::UPtr) override
  {
    pProcessTagCollectAllNodes++;
    return 0;
  }

  int processTagOutCollectingMode(int, optnet::Packet::UPtr) override
  {
    pProcessTagOutCollectingMode++;
    return 0;
  }

  int processTagLMBestBoundValue(int, optnet::Packet::UPtr) override
  {
    pProcessTagLMBestBoundValue++;
    return 0;
  }

  int processTagNotificationId(int, optnet::Packet::UPtr) override
  {
    pProcessTagNotificationId++;
    return 0;
  }

  int processTagTerminateRequest(int, optnet::Packet::UPtr) override
  {
    pProcessTagTerminateRequest++;
    return 0;
  }

  int processTagInterruptRequest(int, optnet::Packet::UPtr) override
  {
    pProcessTagInterruptRequest++;
    return 0;
  }

  int processTagWinnerRacingRampUpParamSet(int, optnet::Packet::UPtr) override
  {
    pProcessTagWinnerRacingRampUpParamSet++;
    return 0;
  }

  int processTagWinner(int, optnet::Packet::UPtr) override
  {
    pProcessTagWinner++;
    return 0;
  }

  int processTagLightWeightRootNodeProcess(int, optnet::Packet::UPtr) override
  {
    pProcessTagLightWeightRootNodeProcess++;
    return 0;
  }

  int processTagBreaking(int, optnet::Packet::UPtr) override
  {
    pProcessTagBreaking++;
    return 0;
  }

  int processTagTerminateSolvingToRestart(int, optnet::Packet::UPtr) override
  {
    pProcessTagTerminateSolvingToRestart++;
    return 0;
  }

  int processTagGivenGapIsReached(int, optnet::Packet::UPtr) override
  {
    pProcessTagGivenGapIsReached++;
    return 0;
  }

  int processTagTestDualBoundGain(int, optnet::Packet::UPtr) override
  {
    pProcessTagTestDualBoundGain++;
    return 0;
  }

  int processTagNoTestDualBoundGain(int, optnet::Packet::UPtr) override
  {
    pProcessTagNoTestDualBoundGain++;
    return 0;
  }

  int processTagNoWaitModeSend(int, optnet::Packet::UPtr) override
  {
    pProcessTagNoWaitModeSend++;
    return 0;
  }

  int processTagRestart(int, optnet::Packet::UPtr) override
  {
    pProcessTagRestart++;
    return 0;
  }

  int processTagLbBoundTightened(int, optnet::Packet::UPtr) override
  {
    pProcessTagLbBoundTightened++;
    return 0;
  }

  int processTagUbBoundTightened(int, optnet::Packet::UPtr) override
  {
    pProcessTagUbBoundTightened++;
    return 0;
  }

  int processTagCutOffValue(int, optnet::Packet::UPtr) override
  {
    pProcessTagCutOffValue++;
    return 0;
  }

 private:
  int pProcessTagNode{0};
  int pProcessTagNodeReceived{0};
  int pProcessTagRampUp{0};
  int pProcessTagRetryRampUp{0};
  int pProcessTagSolution{0};
  int pProcessTagIncumbentValue{0};
  int pProcessTagGlobalBestDualBoundValueAtWarmStart{0};
  int pProcessTagNoNodes{0};
  int pProcessTagInCollectingMode{0};
  int pProcessTagCollectAllNodes{0};
  int pProcessTagOutCollectingMode{0};
  int pProcessTagLMBestBoundValue{0};
  int pProcessTagNotificationId{0};
  int pProcessTagTerminateRequest{0};
  int pProcessTagInterruptRequest{0};
  int pProcessTagWinnerRacingRampUpParamSet{0};
  int pProcessTagWinner{0};
  int pProcessTagLightWeightRootNodeProcess{0};
  int pProcessTagBreaking{0};
  int pProcessTagTerminateSolvingToRestart{0};
  int pProcessTagGivenGapIsReached{0};
  int pProcessTagTestDualBoundGain{0};
  int pProcessTagNoTestDualBoundGain{0};
  int pProcessTagNoWaitModeSend{0};
  int pProcessTagRestart{0};
  int pProcessTagLbBoundTightened{0};
  int pProcessTagUbBoundTightened{0};
  int pProcessTagCutOffValue{0};
};

class MockSolver : public Solver {
 public:
  using SPtr = std::shared_ptr<MockSolver>;

 public:
  MockSolver(Framework::SPtr framework, ParamSet::SPtr paramSet, Instance::SPtr instance,
             timer::Timer::SPtr timer = nullptr)
  : Solver()
 {
    pFramework = framework;
    pParamSet = paramSet;
    pInstance = instance;
    pTimer = timer;

    // Set the network communicator
    pCommunicator = std::dynamic_pointer_cast<optnet::NetworkCommunicator>(pFramework->getComm());
    if (!pCommunicator)
    {
      throw std::runtime_error("MockSolver - invalid downcast from base network communicator "
              "to network communicator");
    }

    // Create the solver's implementation instance holding the callback functions
    // that are triggered upon receiving network messages
    pMockSolverImpl = std::make_shared<MockSolverImpl>(this);
    pOrigSolverImpl = std::make_shared<SolverImpl>(this);

    // By default, use the mock solver impl
    pSolverImpl = pMockSolverImpl;
 }

  inline void setSolverImpl()
  {
    pSolverImpl = getSolverImpl();
  }

  inline void setMockSolverImpl()
  {
    pSolverImpl = getMockSolverImpl();
  }

  inline SolverImpl::SPtr getSolverImpl() const
  {
    return pOrigSolverImpl;
  }

  inline MockSolverImpl::SPtr getMockSolverImpl() const
  {
    return pMockSolverImpl;
  }

  bool wasTerminatedNormally() override { return true; }
  void tryNewSolution(Solution::SPtr sol) override {}
  void setLightWeightRootNodeProcess() override { pIsLightWeightRootNodeProcess = true; }
  void setOriginalRootNodeProcess() override {}
  long long getSimplexIter() override { return 1; }
  inline bool isLightWeightRootNodeProcess() const noexcept
  {
    return pIsLightWeightRootNodeProcess;
  }

  inline bool isNoWaitModeSend() const noexcept
  {
    return pNoWaitModeSend;
  }

  inline int getNotificationIdGenerator() const noexcept
  {
    return pNotificationIdGenerator;
  }

  inline void setNumNodesSendInCollectingMode(int val) noexcept { pNumSendInCollectingMode = val; }
  inline int getNumNodesReceived() const noexcept { return pNumNodesReceived; }
  inline bool getLBUpdate() const noexcept { return pLBUpdate; }
  inline bool getUBUpdate() const noexcept { return pUBUpdate; }
  inline void setRacingRampUpParamSet()
  {
    pRacingRampUpParamSet = std::make_shared<MockRacingRampUpParamSet>();
  }


 protected:
  SolverImpl::SPtr pOrigSolverImpl;
  MockSolverImpl::SPtr pMockSolverImpl;

  void setRacingParams(RacingRampUpParamSet::SPtr racingParms, bool winnerParam) override {}
  void setWinnerRacingParams(RacingRampUpParamSet::SPtr racingParams) override {}
  void createSubproblem() override {}
  void freeSubproblem() override {}
  void solve() override {}
  long long getNumNodesSolved() override { return kNumNodesSolved; }
  int getNumNodesLeft() override { return 0; }
  double getDualBoundValue() override { return 0.0; }
  void reinitializeInstance() override {}
  void setOriginalNodeSelectionStrategy() override {}
  int lowerBoundTightened(int source, optnet::Packet::UPtr packet) override
  {
    pLBUpdate = true;
    return 0;
  }

  int upperBoundTightened(int source, optnet::Packet::UPtr packet) override
  {
    pUBUpdate = true;
    return 0;
  }

 private:
  bool pLBUpdate{false};
  bool pUBUpdate{false};
  bool pIsLightWeightRootNodeProcess{false};
};

class MockLoadManager : public LoadManager {
 public:
  using UPtr = std::unique_ptr<MockLoadManager>;
  using SPtr = std::shared_ptr<MockLoadManager>;

 public:
  MockLoadManager() : LoadManager() {}
  ~MockLoadManager() override { pTearDown = true; }

  inline void setTimer(timer::Timer::SPtr timer) { LoadManager::setTimer(timer); }
  inline void setFramework(Framework::SPtr framework) { LoadManager::setFramework(framework); }
  inline void setCommunicator(optnet::NetworkCommunicator::SPtr comm)
  {
    LoadManager::setCommunicator(comm);
  }

  inline void setInitiator(Initiator::SPtr initiator) { LoadManager::setInitiator(initiator); }
  inline void setParamSet(ParamSet::SPtr paramSet) { LoadManager::setParamSet(paramSet); }
  inline void setNodePool(NodePool::SPtr nodePool) { LoadManager::setNodePool(nodePool); }
  inline void setSolverPool(SolverPool::SPtr solverPool) { LoadManager::setSolverPool(solverPool); }
  inline const LoadManagerTerminationState& getTermState() { return pLMTerminationState; }

  inline bool isRacingTermination() const noexcept { return LoadManager::isRacingTermination(); }
  inline int getNumNodesSolvedAtRacingTermination() const noexcept
  {
    return LoadManager::getNumNodesSolvedAtRacingTermination();
  }
  inline std::size_t getNumTerminated() const noexcept { return LoadManager::getNumTerminated(); }
  inline bool isHardTimeLimitReached() const noexcept
  {
    return LoadManager::isHardTimeLimitReached();
  }

  void setRacingSolverPool(RacingSolverPool::SPtr racingPool)
  {
    LoadManager::setRacingSolverPool(racingPool);
  }

 protected:
  /// Initializes the load manager
  void initLoadManager() override {}

  /// Tear down the load manager
  void tearDown() override {}
};

class MockLoadManagerFixture {
 public:
  using UPtr = std::unique_ptr<MockLoadManagerFixture>;
  using SPtr = std::shared_ptr<MockLoadManagerFixture>;

 public:
  MockLoadManagerFixture()
 {
    // Instantiate a new load manager
    pLM = optilab::metabb::MockLoadManager::UPtr(new optilab::metabb::MockLoadManager());

    // Build and set the framework
    pComm = std::make_shared<optnet::MockNetworkCommunicator>();
    pFramework = std::make_shared<MockFramework>(pComm);
    pLM->setFramework(pFramework);
    pLM->setCommunicator(pComm);

    // Instantiate and start the timer and set it as Timer in the load manager
    pTimer = std::make_shared<timer::Timer>();
    pLM->setTimer(pTimer);

    // Instantiate a new initiator and set it as Initiator int the load manager
    pInitiator = std::make_shared<MockInitiator>(pFramework, pTimer);
    pLM->setInitiator(pInitiator);

    // Instantiate a new parameter set and set it as ParamSet in the load manager
    pParamSet = std::make_shared<ParamSet>();
    pLM->setParamSet(pParamSet);

    // Instantiate a new node pool and set it as NodePool in the load manager
    pNodePool = std::make_shared<NodePoolForMinimization>(kNodePoolBGap);
    pLM->setNodePool(pNodePool);

    // Instantiate a new solver pool and set it as SolverPool in the load manager
    pSolverPool = std::make_shared<SolverPoolForMinimization>(
            1.0,
            0.1,
            1.0,
            kOriginRank,
            pFramework,
            pParamSet,
            pTimer);
    pLM->setSolverPool(pSolverPool);
    for (int idx = kOriginRank; idx <= pSolverPool->getNumSolvers(); ++idx)
    {
      pSolverPool->activateSolver(idx, pFramework->buildNode());
    }

    pRacingPool = std::make_shared<RacingSolverPool>(
            kOriginRank,
            pFramework,
            pParamSet,
            pTimer);
 }

  void setRacingSolverPool()
  {
    // Activate the pool
    pRacingPool->activate(pFramework->buildNode());
    pLM->setRacingSolverPool(pRacingPool);
  }

  inline void setTerminationStateForSolvers(solver::TerminationState state)
  {
    pFramework->setTerminationStateForSolvers(state);
  }

  inline optilab::metabb::MockFramework::SPtr getFramework() const { return pFramework; }
  inline optilab::optnet::MockNetworkCommunicator::SPtr getComm() const { return pComm; }
  inline optilab::metabb::MockLoadManager* getLM() const { return pLM.get(); }
  inline optilab::metabb::Initiator::SPtr getInitiator() const { return pInitiator; }
  inline optilab::metabb::ParamSet::SPtr getParamSet() const { return pParamSet; }
  inline optilab::metabb::NodePool::SPtr getNodePool() const { return pNodePool; }
  inline const LoadManagerTerminationState& getTermState() { return pLM->getTermState(); }
  inline optilab::metabb::SolverPool::SPtr getSolverPool() const { return pSolverPool; }
  inline optilab::metabb::RacingSolverPool::SPtr getRacingSolverPool() const { return pRacingPool; }
  inline optilab::timer::Timer::SPtr getTimer() const { return pTimer; }

 private:
   // Mock Load manager
   optilab::metabb::MockLoadManager::UPtr pLM;

   // Objects needed to instantiate and setup the mock load manager
   optilab::optnet::MockNetworkCommunicator::SPtr pComm;
   optilab::metabb::MockFramework::SPtr pFramework;
   optilab::metabb::Initiator::SPtr pInitiator;
   optilab::metabb::NodePool::SPtr pNodePool;
   optilab::metabb::ParamSet::SPtr pParamSet;
   optilab::metabb::SolverPool::SPtr pSolverPool;
   optilab::metabb::RacingSolverPool::SPtr pRacingPool;
   optilab::timer::Timer::SPtr pTimer;
};

class MockLoadManagerImpl : public LoadManagerImpl {
 public:
  using SPtr = std::shared_ptr<MockLoadManagerImpl>;

 public:
  MockLoadManagerImpl(LoadManager* lm) : LoadManagerImpl(lm) {}

  inline void setRacingRampup() noexcept { pIsRacingRampUp = true; }

  inline int tagNode() const noexcept { return pProcessTagNode; }
  inline int tagSolution() const noexcept { return pProcessTagSolution; }
  inline int tagSolverState() const noexcept { return pProcessTagSolverState; }
  inline int tagCompletionOfCalculation() const noexcept { return pCompletionOfCalculation; }
  inline int tagAnotherNodeRequest() const { return pAnotherNodeRequest; }
  inline int tagTerminated() const { return pTerminated; }
  inline int tagHardTimeLimit() const { return pHardTimeLimit; }
  inline int tagStatistics() const { return pStatistics; }
  inline int tagAllowToBeInCollectingMode() const { return pAllowToBeInCollectingMode; }
  inline int tagLbBoundTightened() const { return pLbBoundTightened; }
  inline int tagUbBoundTightened() const { return pUbBoundTightened; }
  inline int racingRampUpTagSolverState() const { return pRacingProcessTagSolverState; }
  inline int racingRampUpTagCompletionOfCalculation() const
  {
    return pRacingCompletionOfCalculation;
  }

 protected:
  int processTagNode(int, optnet::Packet::UPtr) override
  {
    pProcessTagNode++;
    return 0;
  }

  int processTagSolution(int, optnet::Packet::UPtr) override
  {
    pProcessTagSolution++;
    return 0;
  }

  int processTagSolverState(int, optnet::Packet::UPtr) override
  {
    pProcessTagSolverState++;
    return 0;
  }

  int processTagCompletionOfCalculation(int, optnet::Packet::UPtr) override
  {
    pCompletionOfCalculation++;
    return 0;
  }

  int processTagAnotherNodeRequest(int, optnet::Packet::UPtr) override
  {
    pAnotherNodeRequest++;
    return 0;
  }

  int processTagTerminated(int, optnet::Packet::UPtr) override
  {
    pTerminated++;
    return 0;
  }

  int processTagHardTimeLimit(int, optnet::Packet::UPtr) override
  {
    pHardTimeLimit++;
    return 0;
  }

  int processTagStatistics(int, optnet::Packet::UPtr) override
  {
    pStatistics++;
    return 0;
  }

  int processTagAllowToBeInCollectingMode(int, optnet::Packet::UPtr) override
  {
    pAllowToBeInCollectingMode++;
    return 0;
  }

  int processTagLbBoundTightened(int, optnet::Packet::UPtr) override
  {
    pLbBoundTightened++;
    return 0;
  }

  int processTagUbBoundTightened(int, optnet::Packet::UPtr) override
  {
    pUbBoundTightened++;
    return 0;
  }

  int processRacingRampUpTagSolverState(int, optnet::Packet::UPtr) override
  {
    pRacingProcessTagSolverState++;
    return 0;
  }

  int processRacingRampUpTagCompletionOfCalculation(int, optnet::Packet::UPtr) override
  {
    pRacingCompletionOfCalculation++;
    return 0;
  }

 private:
  int pProcessTagNode{0};
  int pProcessTagSolution{0};
  int pProcessTagSolverState{0};
  int pCompletionOfCalculation{0};
  int pAnotherNodeRequest{0};
  int pTerminated{0};
  int pHardTimeLimit{0};
  int pStatistics{0};
  int pAllowToBeInCollectingMode{0};
  int pLbBoundTightened{0};
  int pUbBoundTightened{0};
  int pRacingProcessTagSolverState{0};
  int pRacingCompletionOfCalculation{0};
};

}  // namespace metabb
}  // namespace optilab

