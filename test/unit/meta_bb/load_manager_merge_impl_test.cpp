#include <memory>     // for std::unique_ptr

#include <gtest/gtest.h>

#include "meta_bb_mocks.hpp"
#include "meta_bb/framework.hpp"
#include "meta_bb/initiator.hpp"
#include "meta_bb/load_manager_merge_impl.hpp"
#include "meta_bb/node_pool.hpp"
#include "meta_bb/paramset.hpp"
#include "utilities/timer.hpp"

class LoadManagerMergeImplFixture : public testing::Test {
 protected:
  void SetUp() override
  {
    using namespace optilab;
    using namespace metabb;

    // Instantiate a new load manager
    pLM = optilab::metabb::MockLoadManagerFixture::UPtr(
            new optilab::metabb::MockLoadManagerFixture());

    ASSERT_NO_THROW(pLMMerge = std::make_shared<LoadManagerMergeImpl>(pLM->getLM()));
  }

  optilab::metabb::MockLoadManagerFixture* getLM() const { return pLM.get(); }

  inline optilab::metabb::LoadManagerMergeImpl::SPtr getMergeNodeImpl() const { return pLMMerge; }
  inline optilab::metabb::MockLoadManagerFixture* getFixture() const { return pLM.get(); }
  inline optilab::metabb::ParamSet::SPtr getParamSet() const { return pLM->getParamSet(); }

  inline optilab::metabb::Node::SPtr getNode(int numBoundChangesBestNode, int numFixedVars)
  {
    using namespace optilab;
    using namespace metabb;

    MockDiffSubproblem::SPtr diffSubproblem =
            std::make_shared<MockDiffSubproblem>(numBoundChangesBestNode, numFixedVars);
    MockNode::SPtr node = std::make_shared<MockNode>(
            NodeId(), // node id
            NodeId(), // generator node id
            1,        // node depth
            1.0,      // dual bound value
            1.0,      // original dual bound value
            1.0,      // estimated value
            diffSubproblem);

    return node;
  }

 private:
  // Mock merge node impl
  optilab::metabb::LoadManagerMergeImpl::SPtr pLMMerge;

  // Mock Load manager
  optilab::metabb::MockLoadManagerFixture::UPtr pLM;
};

TEST_F(LoadManagerMergeImplFixture, Init_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  auto lmMerge = getMergeNodeImpl();
  EXPECT_NO_THROW(lmMerge->initMergeNodesStructs());
  EXPECT_EQ(lmMerge->getVarIndexTable().size(),
            getFixture()->getInitiator()->getInstance()->getVarIndexRange());
  for (auto ptr : lmMerge->getVarIndexTable())
  {
    EXPECT_FALSE(ptr);
  }
}  // Init_No_Throw

TEST_F(LoadManagerMergeImplFixture, GenerateMergeNodesCandidates_No_Nodes_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  auto lmMerge = getMergeNodeImpl();
  ASSERT_NO_THROW(lmMerge->initMergeNodesStructs());
  ASSERT_NO_THROW(lmMerge->generateMergeNodesCandidates());
  EXPECT_EQ(0, lmMerge->getVarIndexTable().size()) << "Var index table should be empty after "
          " merge node generation";
}  // GenerateMergeNodesCandidates_No_Nodes_No_Throw

TEST_F(LoadManagerMergeImplFixture, addNodeToMergeNodeStructs_No_FixedVars_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  auto lmMerge = getMergeNodeImpl();
  ASSERT_NO_THROW(lmMerge->initMergeNodesStructs());

  int numBoundChanges{1};
  int numFixedVariables{0};
  auto node = getNode(numBoundChanges, numFixedVariables);

  // The first time a node is added, it sets the number of bound changes and returns
  // since it is assumed there is only a node and nothing needs to be merged
  ASSERT_NO_THROW(lmMerge->addNodeToMergeNodeStructs(node));

  // Same if the node is being re-inserted and the number of bound changes is
  // the same as the previous time
  ASSERT_NO_THROW(lmMerge->addNodeToMergeNodeStructs(node));

  // Add a new node with higher number of bound changes.
  // This should trigger merge
  auto mergeNode = getNode(numBoundChanges + 1, numFixedVariables);
  ASSERT_NO_THROW(lmMerge->addNodeToMergeNodeStructs(mergeNode));

  // With no fixed variables, the method should return setting the status of the node
  // as "cannot be merged"
  EXPECT_FALSE(mergeNode->getMergeNodeInfo());
  EXPECT_EQ(mergeNode->getMergingStatus(), Node::NodeMergingStatus::NMS_CANNOT_BE_MERGED);
}  // addNodeToMergeNodeStructs_No_FixedVars_No_Throw

TEST_F(LoadManagerMergeImplFixture, addNodeToMergeNodeStructs_FixedVars_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  auto lmMerge = getMergeNodeImpl();
  ASSERT_NO_THROW(lmMerge->initMergeNodesStructs());

  int numBoundChanges{1};
  int numFixedVariables{2};
  auto node = getNode(numBoundChanges, numFixedVariables);

  // The first time a node is added, it sets the number of bound changes and returns
  // since it is assumed there is only a node and nothing needs to be merged
  ASSERT_NO_THROW(lmMerge->addNodeToMergeNodeStructs(node));

  // Same if the node is being re-inserted and the number of bound changes is
  // the same as the previous time
  ASSERT_NO_THROW(lmMerge->addNodeToMergeNodeStructs(node));

  // The merge info head and tail should not be set
  EXPECT_FALSE(lmMerge->getMergeInfoHead());
  EXPECT_FALSE(lmMerge->getMergeInfoTail());

  // Add a new node with higher number of bound changes.
  // This should trigger merge
  auto mergeNode = getNode(numBoundChanges + 1, numFixedVariables);
  ASSERT_NO_THROW(lmMerge->addNodeToMergeNodeStructs(mergeNode));

  // Status of the node should be checking
  EXPECT_EQ(mergeNode->getMergingStatus(), Node::NodeMergingStatus::NMS_CHECKING);
  return;
  // With fixed variables, the method should set the merge info head and tail.
  // This should set the variable index table of the load manager merge info
  const auto varTable = lmMerge->getVarIndexTable();
  ASSERT_LE(numFixedVariables, varTable.size());

  double val{1.0};
  for (int idx = 0; idx < numFixedVariables; ++idx)
  {
    EXPECT_TRUE(varTable.at(idx)) << "Fixed valued at index " << idx << " should be valid";
    EXPECT_DOUBLE_EQ(varTable.at(idx)->value, val);
    val += 1.0;
  }

  // The merge info head and tail should be set
  EXPECT_TRUE(lmMerge->getMergeInfoHead());
  EXPECT_TRUE(lmMerge->getMergeInfoTail());

  // The merge info head should be set to the node's merge info (initialize the head)
  auto nodeMergeInfo = mergeNode->getMergeNodeInfo();
  EXPECT_EQ(lmMerge->getMergeInfoHead(), nodeMergeInfo.get());

  // Each fixed variable should have the same node's merge info
  for (auto fvar : (nodeMergeInfo->fixedVariableList))
  {
    EXPECT_EQ(fvar->mnode, nodeMergeInfo.get());
  }
}  // addNodeToMergeNodeStructs_FixedVars_No_Throw

TEST_F(LoadManagerMergeImplFixture, generateMergeNodesCandidates_No_Merge_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  auto lmMerge = getMergeNodeImpl();
  ASSERT_NO_THROW(lmMerge->initMergeNodesStructs());

  int numBoundChanges{1};
  int numFixedVariables{2};
  auto nodeA = getNode(numBoundChanges, numFixedVariables);

  // The first time a node is added, it sets the number of bound changes and returns
  // since it is assumed there is only a node and nothing needs to be merged
  ASSERT_NO_THROW(lmMerge->addNodeToMergeNodeStructs(nodeA));

  // Add a two new nodes with higher number of bound changes.
  // This should trigger merge
  auto nodeB = getNode(numBoundChanges + 1, numFixedVariables);
  auto nodeC = getNode(numBoundChanges + 1, numFixedVariables);
  ASSERT_NO_THROW(lmMerge->addNodeToMergeNodeStructs(nodeB));
  ASSERT_NO_THROW(lmMerge->addNodeToMergeNodeStructs(nodeC));

  ASSERT_EQ(nodeB->getMergingStatus(), Node::NodeMergingStatus::NMS_CHECKING);
  ASSERT_EQ(nodeC->getMergingStatus(), Node::NodeMergingStatus::NMS_CHECKING);

  // The merge info head and tail should be set
  ASSERT_TRUE(lmMerge->getMergeInfoHead());
  ASSERT_TRUE(lmMerge->getMergeInfoTail());
  EXPECT_TRUE(lmMerge->getMergeInfoHead()->isReadyToBeMerged());

  // Generate the candidates for merging
  EXPECT_NO_THROW(lmMerge->generateMergeNodesCandidates());

  // The  nodes cannot be merged since there are less than the minimum
  // number of nodes required for merging (i.e., two nodes)
  ASSERT_FALSE(lmMerge->getMergeInfoHead());

  // Both nodes cannot be merged
  ASSERT_FALSE(nodeB->getMergeNodeInfo());
  ASSERT_FALSE(nodeC->getMergeNodeInfo());
  ASSERT_EQ(nodeB->getMergingStatus(), Node::NodeMergingStatus::NMS_CANNOT_BE_MERGED);
  ASSERT_EQ(nodeC->getMergingStatus(), Node::NodeMergingStatus::NMS_CANNOT_BE_MERGED);
}  // generateMergeNodesCandidates_No_Merge_No_Throw

TEST_F(LoadManagerMergeImplFixture, generateMergeNodesCandidates_Merge_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  auto lmMerge = getMergeNodeImpl();
  ASSERT_NO_THROW(lmMerge->initMergeNodesStructs());

  int numBoundChanges{1};
  int numFixedVariables{2};
  auto nodeA = getNode(numBoundChanges, numFixedVariables);

  // The first time a node is added, it sets the number of bound changes and returns
  // since it is assumed there is only a node and nothing needs to be merged
  ASSERT_NO_THROW(lmMerge->addNodeToMergeNodeStructs(nodeA));

  // Add a three  new nodes with higher number of bound changes.
  // This should trigger merge and merge the nodes
  auto nodeB = getNode(numBoundChanges + 1, numFixedVariables);
  auto nodeC = getNode(numBoundChanges + 1, numFixedVariables);
  auto nodeD = getNode(numBoundChanges + 1, numFixedVariables);
  ASSERT_NO_THROW(lmMerge->addNodeToMergeNodeStructs(nodeB));
  ASSERT_NO_THROW(lmMerge->addNodeToMergeNodeStructs(nodeC));
  ASSERT_NO_THROW(lmMerge->addNodeToMergeNodeStructs(nodeD));

  EXPECT_EQ(nodeB->getMergingStatus(), Node::NodeMergingStatus::NMS_CHECKING);
  EXPECT_EQ(nodeC->getMergingStatus(), Node::NodeMergingStatus::NMS_CHECKING);
  EXPECT_EQ(nodeD->getMergingStatus(), Node::NodeMergingStatus::NMS_CHECKING);
  EXPECT_EQ(lmMerge->getVarIndexTable().size(), kInstanceNumVariables);

  // Generate the candidates for merging
  EXPECT_NO_THROW(lmMerge->generateMergeNodesCandidates());

  // After generating candidates, the var index table should be empty
  EXPECT_TRUE(lmMerge->getVarIndexTable().empty());

  // The first node should be the representative of the merged nodes
  EXPECT_EQ(nodeB->getMergeNodeInfo()->status,
              mergenodes::MergeNodeInfo::Status::STATUS_MERGED_REPRESENTATIVE);

  // All other nodes are in merge checking (until they are merged) to
  // the representative node
  EXPECT_EQ(nodeC->getMergeNodeInfo()->status,
              mergenodes::MergeNodeInfo::Status::STATUS_MERGE_CHECKING_TO_OTHER_NODE);
  EXPECT_EQ(nodeD->getMergeNodeInfo()->status,
            mergenodes::MergeNodeInfo::Status::STATUS_MERGE_CHECKING_TO_OTHER_NODE);
}  // generateMergeNodesCandidates_Merge_No_Throw

TEST_F(LoadManagerMergeImplFixture, mergeNodes_Merge_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  auto lmMerge = getMergeNodeImpl();
  ASSERT_NO_THROW(lmMerge->initMergeNodesStructs());

  int numBoundChanges{1};
  int numFixedVariables{2};
  auto nodeA = getNode(numBoundChanges, numFixedVariables);
  auto nodeB = getNode(numBoundChanges + 1, numFixedVariables);
  auto nodeC = getNode(numBoundChanges + 1, numFixedVariables);
  auto nodeD = getNode(numBoundChanges + 1, numFixedVariables);

  // Add nodes in the node pool
  getLM()->getNodePool()->insert(nodeA);
  getLM()->getNodePool()->insert(nodeB);
  getLM()->getNodePool()->insert(nodeC);
  getLM()->getNodePool()->insert(nodeD);
  EXPECT_EQ(getLM()->getNodePool()->getNumNodes(), 4);

  ASSERT_NO_THROW(lmMerge->addNodeToMergeNodeStructs(nodeA));
  ASSERT_NO_THROW(lmMerge->addNodeToMergeNodeStructs(nodeB));
  ASSERT_NO_THROW(lmMerge->addNodeToMergeNodeStructs(nodeC));
  ASSERT_NO_THROW(lmMerge->addNodeToMergeNodeStructs(nodeD));

  // Generate the candidates for merging
  EXPECT_NO_THROW(lmMerge->generateMergeNodesCandidates());

  // Merge nodes
  EXPECT_NO_THROW(lmMerge->mergeNodes(nodeB));

  // The node should be merged
  EXPECT_EQ(nodeB->getMergingStatus(), Node::NodeMergingStatus::NMS_MERGED);

  // Since the node is merged, it should be as a "new" node,
  // i.e., it should not have any merge node info
  EXPECT_FALSE(nodeB->getMergeNodeInfo());

  // Other nodes, since they don't have a lower dual bound, should be deleted.
  // In fact, these nodes have been merged and there is no need to keep them around anymore
  EXPECT_EQ(nodeC->getMergingStatus(),
            Node::NodeMergingStatus::NMS_MERGING_REPRESENTATIVE_WAS_DELETED);
  EXPECT_EQ(nodeD->getMergingStatus(),
            Node::NodeMergingStatus::NMS_MERGING_REPRESENTATIVE_WAS_DELETED);
  EXPECT_EQ(nodeC->getMergeNodeInfo()->status, mergenodes::MergeNodeInfo::Status::STATUS_DELETED);
  EXPECT_EQ(nodeD->getMergeNodeInfo()->status, mergenodes::MergeNodeInfo::Status::STATUS_DELETED);
  EXPECT_FALSE(nodeC->getMergeNodeInfo()->mergedTo);
  EXPECT_FALSE(nodeD->getMergeNodeInfo()->mergedTo);

  // Nodes C and D should be merged into node B.
  // @note the first node (nodeA) didn't merge since, being the first node,
  // it set the value "pNumVarBoundChangesOfBestNode' and returned
  EXPECT_EQ(getLM()->getNodePool()->getNumNodes(), 2);
}  // mergeNodes_Merge_No_Throw

TEST_F(LoadManagerMergeImplFixture, regenerateMergeNodesCandidates_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  auto lmMerge = getMergeNodeImpl();
  ASSERT_NO_THROW(lmMerge->initMergeNodesStructs());

  int numBoundChanges{1};
  int numFixedVariables{2};
  auto nodeA = getNode(numBoundChanges, numFixedVariables);
  auto nodeB = getNode(numBoundChanges + 1, numFixedVariables);
  auto nodeC = getNode(numBoundChanges + 1, numFixedVariables);
  auto nodeD = getNode(numBoundChanges + 1, numFixedVariables);

  // Add nodes in the node pool
  getLM()->getNodePool()->insert(nodeA);
  getLM()->getNodePool()->insert(nodeB);
  getLM()->getNodePool()->insert(nodeC);
  getLM()->getNodePool()->insert(nodeD);
  EXPECT_EQ(getLM()->getNodePool()->getNumNodes(), 4);

  ASSERT_NO_THROW(lmMerge->addNodeToMergeNodeStructs(nodeA));
  ASSERT_NO_THROW(lmMerge->addNodeToMergeNodeStructs(nodeB));
  ASSERT_NO_THROW(lmMerge->addNodeToMergeNodeStructs(nodeC));
  ASSERT_NO_THROW(lmMerge->addNodeToMergeNodeStructs(nodeD));

  // Generate the candidates for merging
  EXPECT_NO_THROW(lmMerge->generateMergeNodesCandidates());

  // Regenerate the candidates for merging
  EXPECT_NO_THROW(lmMerge->regenerateMergeNodesCandidates(nodeB));

  // After generating candidates, the var index table should be empty
  EXPECT_TRUE(lmMerge->getVarIndexTable().empty());

  // Merge node info has been set to nullptr by the regenerate method
  EXPECT_FALSE(nodeB->getMergeNodeInfo());

  // Force clean-up
  nodeA.reset();
  nodeB.reset();
  nodeC.reset();
  nodeD.reset();
  getLM()->getNodePool()->freePool();
}  // regenerateMergeNodesCandidates_No_Throw
