#include <limits>     // for std::numeric_limits
#include <memory>     // for std::shared_ptr
#include <stdexcept>  // for std::invalid_argument

#include <gtest/gtest.h>

#include "meta_bb_mocks.hpp"
#include "meta_bb/meta_bb_constants.hpp"
#include "meta_bb/solver_pool.hpp"
#include "optimizer_network/network_communicator.hpp"

namespace {
constexpr double kMP{1.5};
constexpr double kBGap{0.1};
constexpr double kMBGap{1.0};
constexpr int kOrigRank{0};
}  // namespace

TEST(SolverPool, Constructor_Empty_Framework_Throws)
{
  using namespace optilab;
  using namespace metabb;

  MockSolverPool::SPtr pool;
  ParamSet::SPtr paramSet = std::make_shared<ParamSet>();
  EXPECT_THROW(pool = std::make_shared<MockSolverPool>(kMP, kBGap, kMBGap, kOrigRank,
                                                       nullptr, paramSet), std::invalid_argument);
}  // Constructor_Empty_Framework_Throws

TEST(SolverPool, Constructor_Empty_ParamSet_Throws)
{
  using namespace optilab;
  using namespace metabb;
  using namespace optnet;

  MockSolverPool::SPtr pool;
  Framework::SPtr framework = std::make_shared<MockFramework>(
          std::make_shared<optnet::MockNetworkCommunicator>());
  EXPECT_THROW(pool = std::make_shared<MockSolverPool>(kMP, kBGap, kMBGap, kOrigRank,
                                                       framework, nullptr), std::invalid_argument);
}  // Constructor_Empty_ParamSet_Throws

TEST(SolverPool, Constructor_Valid_Arguments_No_Throw)
{
  using namespace optilab;
  using namespace metabb;
  using namespace optnet;

  auto comm = std::make_shared<optnet::MockNetworkCommunicator>();
  auto framework = std::make_shared<MockFramework>(comm);
  auto paramSet = std::make_shared<ParamSet>();

  MockSolverPool::SPtr pool;
  EXPECT_NO_THROW(pool = std::make_shared<MockSolverPool>(kMP, kBGap, kMBGap, kOrigRank, framework,
                                                          paramSet));
  EXPECT_FALSE(pool->isActive());
  EXPECT_EQ(comm->getSize(), pool->getNumSolvers());
  EXPECT_EQ(0, pool->getNumNodesSolvedInSolvers());
  EXPECT_EQ(0, pool->getNumNodesInAllSolvers());
  EXPECT_EQ(0, pool->getTotalNodesSolved());
  EXPECT_EQ(0, pool->getNumNodesInSolvers());
  EXPECT_EQ(0, static_cast<int>(pool->getNumActiveSolvers()));
  EXPECT_EQ(comm->getSize(), static_cast<int>(pool->getNumInactiveSolvers()));
  EXPECT_FALSE(pool->isInCollectingMode());
  EXPECT_LE(kOrigRank, pool->getInactiveSolverRank());
}  // Constructor_Valid_Arguments_No_Throw

TEST(SolverPool, Set_Get_Methods)
{
  using namespace optilab;
  using namespace metabb;
  using namespace optnet;

  auto comm = std::make_shared<optnet::MockNetworkCommunicator>();
  auto framework = std::make_shared<MockFramework>(comm);
  auto paramSet = std::make_shared<ParamSet>();

  MockSolverPool::SPtr pool;
  EXPECT_NO_THROW(pool = std::make_shared<MockSolverPool>(kMP, kBGap, kMBGap, kOrigRank, framework,
                                                          paramSet));

  const uint64_t addNodes{10};
  const int invalidRank{pool->getNumSolvers() + 1};
  pool->activate();
  EXPECT_TRUE(pool->isActive());
  EXPECT_THROW(pool->getCurrentNodeFromSolver(invalidRank), std::out_of_range);
  for (int rank = 0; rank < pool->getNumSolvers(); ++rank)
  {
    EXPECT_NO_THROW(pool->getCurrentNodeFromSolver(rank));
  }

  auto numNodes = pool->getTotalNodesSolved();
  pool->addTotalNodesSolved(addNodes);
  EXPECT_EQ(numNodes + addNodes, pool->getTotalNodesSolved());

  EXPECT_THROW(pool->isSolverActive(invalidRank), std::out_of_range);
  for (int rank = 0; rank < pool->getNumSolvers(); ++rank)
  {
    bool val{true};
    EXPECT_NO_THROW(val = pool->isSolverActive(rank));
    EXPECT_FALSE(val);
  }

  EXPECT_THROW(pool->isSolverInCollectingMode(invalidRank), std::out_of_range);
  for (int rank = 0; rank < pool->getNumSolvers(); ++rank)
  {
    bool val{true};
    EXPECT_NO_THROW(val = pool->isSolverInCollectingMode(rank));
    EXPECT_FALSE(val);
  }

  EXPECT_THROW(pool->currentSolvingNodeHasDescendant(invalidRank), std::out_of_range);
  for (int rank = 0; rank < pool->getNumSolvers(); ++rank)
  {
    // No node as "current node"
    EXPECT_THROW(pool->currentSolvingNodeHasDescendant(rank), std::runtime_error);
  }

  EXPECT_THROW(pool->extractCurrentNodeAndInactivate(0, nullptr), std::invalid_argument);

  SolverTerminationState::SPtr termState;
  EXPECT_THROW(pool->setTermState(invalidRank, termState), std::out_of_range);
  for (int rank = 0; rank < pool->getNumSolvers(); ++rank)
  {
    EXPECT_NO_THROW(pool->setTermState(rank, termState));
  }

  EXPECT_NO_THROW(pool->updateDualBoundsForSavingNodes());

  // The limit is bounded by the value pNumMaxCollectingModeSolvers
  const auto limitCollecting = pool->getNumLimitCollectingModeSolvers();
  EXPECT_NO_THROW(pool->incNumLimitCollectingModeSolvers());
  EXPECT_EQ(limitCollecting + 1, pool->getNumLimitCollectingModeSolvers());

  EXPECT_THROW(pool->setCollectingIsAllowed(invalidRank), std::out_of_range);
  for (int rank = 0; rank < pool->getNumSolvers(); ++rank)
  {
    EXPECT_NO_THROW(pool->setCollectingIsAllowed(rank));
  }

  Node::SPtr mockNode = std::make_shared<MockNode>();
  EXPECT_THROW(pool->activateSolver(invalidRank, mockNode), std::out_of_range);
  EXPECT_THROW(pool->activateSolver(0, nullptr), std::invalid_argument);
  for (int rank = 0; rank < pool->getNumSolvers(); ++rank)
  {
    EXPECT_NO_THROW(pool->activateSolver(rank, mockNode));
  }

  EXPECT_THROW(pool->solverKill(invalidRank), std::out_of_range);
  for (int rank = 0; rank < pool->getNumSolvers(); ++rank)
  {
    EXPECT_NO_THROW(pool->solverKill(rank));
  }

  EXPECT_THROW(pool->enforcedSwitchOutCollectingMode(invalidRank), std::out_of_range);
  for (int rank = 0; rank < pool->getNumSolvers(); ++rank)
  {
    EXPECT_NO_THROW(pool->enforcedSwitchOutCollectingMode(rank));
  }

  EXPECT_THROW(pool->sendSwitchOutCollectingModeIfNecessary(invalidRank), std::out_of_range);
  for (int rank = 0; rank < pool->getNumSolvers(); ++rank)
  {
    EXPECT_NO_THROW(pool->sendSwitchOutCollectingModeIfNecessary(rank));
  }

  EXPECT_THROW(pool->isDualBounGainTesting(invalidRank), std::out_of_range);
  for (int rank = 0; rank < pool->getNumSolvers(); ++rank)
  {
    bool val{true};
    EXPECT_NO_THROW(val = pool->isDualBounGainTesting(rank));
    EXPECT_FALSE(val);
  }

  const uint64_t time = 10;
  EXPECT_NO_THROW(pool->setSwichOutTime(time));
  EXPECT_EQ(time, pool->getSwichOutTime());
}  // Set_Get_Methods

TEST(RacingSolverPool, Constructor_Empty_Framework_Throws)
{
  using namespace optilab;
  using namespace metabb;

  RacingSolverPool::SPtr pool;
  ParamSet::SPtr paramSet = std::make_shared<ParamSet>();
  EXPECT_THROW(pool = std::make_shared<RacingSolverPool>(kOrigRank, nullptr, paramSet),
               std::invalid_argument);
}  // Constructor_Empty_Framework_Throws

TEST(RacingSolverPool, Constructor_Empty_ParamSet_Throws)
{
  using namespace optilab;
  using namespace metabb;
  using namespace optnet;

  RacingSolverPool::SPtr pool;
  Framework::SPtr framework = std::make_shared<MockFramework>(
          std::make_shared<optnet::MockNetworkCommunicator>());
  EXPECT_THROW(pool = std::make_shared<RacingSolverPool>(kOrigRank, framework, nullptr),
               std::invalid_argument);
}  // Constructor_Empty_ParamSet_Throws

TEST(RacingSolverPool, Constructor_No_Throw)
{
  using namespace optilab;
  using namespace metabb;
  using namespace optnet;

  RacingSolverPool::SPtr pool;
  ParamSet::SPtr paramSet = std::make_shared<ParamSet>();
  Framework::SPtr framework = std::make_shared<MockFramework>(
          std::make_shared<optnet::MockNetworkCommunicator>());
  EXPECT_NO_THROW(pool = std::make_shared<RacingSolverPool>(kOrigRank, framework, paramSet));
}  // Constructor_No_Throw

TEST(RacingSolverPool, Get_Set_Methods)
{
  using namespace optilab;
  using namespace metabb;
  using namespace optnet;

  RacingSolverPool::SPtr pool;
  ParamSet::SPtr paramSet = std::make_shared<ParamSet>();
  Framework::SPtr framework = std::make_shared<MockFramework>(
          std::make_shared<optnet::MockNetworkCommunicator>());
  EXPECT_NO_THROW(pool = std::make_shared<RacingSolverPool>(kOrigRank, framework, paramSet));

  const int invalidRank{pool->getNumSolvers() + 1};
  EXPECT_THROW(pool->extractRacingRootNode(), std::runtime_error);

  EXPECT_THROW(pool->getDualBoundValue(invalidRank), std::out_of_range);
  for (int rank = 0; rank < pool->getNumSolvers(); ++rank)
  {
    double val{0.0};
    EXPECT_NO_THROW(val = pool->getDualBoundValue(rank));
    EXPECT_DOUBLE_EQ(0.0, val);
  }

  EXPECT_DOUBLE_EQ(std::numeric_limits<double>::lowest(),
                   pool->getGlobalBestDualBoundValue());

  EXPECT_THROW(pool->getNumOfNodesSolved(invalidRank), std::out_of_range);
  for (int rank = 0; rank < pool->getNumSolvers(); ++rank)
  {
    long long val{0};
    EXPECT_NO_THROW(val = pool->getNumOfNodesSolved(rank));
    EXPECT_EQ(0, val);
  }

  EXPECT_THROW(pool->getNumNodesLeft(invalidRank), std::out_of_range);
  for (int rank = 0; rank < pool->getNumSolvers(); ++rank)
  {
    int val{0};
    EXPECT_NO_THROW(val = pool->getNumNodesLeft(rank));
    EXPECT_EQ(0, val);
  }

  EXPECT_EQ(-1, pool->getWinner());
  EXPECT_EQ(0, pool->getNumNodesSolvedInBestSolver());
  EXPECT_EQ(0, pool->getNumNodesLeftInBestSolver());
  EXPECT_DOUBLE_EQ(std::numeric_limits<double>::lowest(),
                   pool->getBestDualBoundDeactivatedSolvers());

  Node::SPtr mockNode = std::make_shared<MockNode>();
  EXPECT_THROW(pool->activate(nullptr), std::invalid_argument);
  EXPECT_EQ(0, pool->getNumActiveSolvers());
  EXPECT_NO_THROW(pool->activate(mockNode));
  EXPECT_EQ(pool->getNumSolvers(), pool->getNumActiveSolvers()) <<
          "All solvers should be activated";
  EXPECT_EQ(1, pool->getNumNodesLeftInBestSolver());

  EXPECT_THROW(pool->isActive(invalidRank), std::out_of_range);
  for (int rank = 0; rank < pool->getNumSolvers(); ++rank)
  {
    bool val{false};
    EXPECT_NO_THROW(val = pool->isActive(rank));
    EXPECT_TRUE(val) << "All solvers should be active";
  }

  EXPECT_THROW(pool->deactivateSolver(invalidRank), std::out_of_range);
  for (int rank = 0; rank < pool->getNumSolvers(); ++rank)
  {
    bool val{true};
    EXPECT_NO_THROW(pool->deactivateSolver(rank));
    EXPECT_NO_THROW(val = pool->isActive(rank));
    EXPECT_FALSE(val);
  }
  EXPECT_EQ(0, pool->getNumActiveSolvers()) << "All solvers should be deactivated";

  EXPECT_THROW(pool->isEvaluationStage(invalidRank), std::out_of_range);
  for (int rank = 0; rank < pool->getNumSolvers(); ++rank)
  {
    bool val{true};
    EXPECT_NO_THROW(val = pool->isEvaluationStage(rank));
    EXPECT_FALSE(val);
  }
}  // Get_Set_Methods

TEST(SolverPoolForMinimization, Constructor_Empty_Framework_Throws)
{
  using namespace optilab;
  using namespace metabb;

  ParamSet::SPtr paramSet = std::make_shared<ParamSet>();
  EXPECT_THROW(std::make_shared<SolverPoolForMinimization>(
          kMP, kBGap, kMBGap, kOrigRank, nullptr, paramSet), std::invalid_argument);
}  // Constructor_Empty_Framework_Throws

TEST(SolverPoolForMinimization, Constructor_Empty_ParamSet_Throws)
{
  using namespace optilab;
  using namespace metabb;
  using namespace optnet;

  Framework::SPtr framework = std::make_shared<MockFramework>(
          std::make_shared<optnet::MockNetworkCommunicator>());
  EXPECT_THROW(std::make_shared<SolverPoolForMinimization>(
          kMP, kBGap, kMBGap, kOrigRank, framework, nullptr), std::invalid_argument);
}  // Constructor_Empty_ParamSet_Throws

TEST(SolverPoolForMinimization, Constructor_No_Throw)
{
  using namespace optilab;
  using namespace metabb;
  using namespace optnet;

  SolverPoolForMinimization::SPtr pool;
  ParamSet::SPtr paramSet = std::make_shared<ParamSet>();
  Framework::SPtr framework = std::make_shared<MockFramework>(
          std::make_shared<optnet::MockNetworkCommunicator>());
  EXPECT_NO_THROW(pool = std::make_shared<SolverPoolForMinimization>(
          kMP, kBGap, kMBGap, kOrigRank, framework, paramSet));
}  // Constructor_No_Throw

TEST(SolverPoolForMinimization, Get_Best_Global_Dual_Bound_Value)
{
  using namespace optilab;
  using namespace metabb;
  using namespace optnet;

  SolverPoolForMinimization::SPtr pool;
  ParamSet::SPtr paramSet = std::make_shared<ParamSet>();
  Framework::SPtr framework = std::make_shared<MockFramework>(
          std::make_shared<optnet::MockNetworkCommunicator>());
  EXPECT_NO_THROW(pool = std::make_shared<SolverPoolForMinimization>(
          kMP, kBGap, kMBGap, kOrigRank, framework, paramSet));

  EXPECT_EQ(framework->getComm()->getSize(), pool->getNumSolvers());

  // Check best dual bound value.
  // No solver has been added to the pool yet, which means that
  // the best dual bound value is positive infinity
  double bestDualBound{0.0};
  EXPECT_NO_THROW(bestDualBound = pool->getGlobalBestDualBoundValue());
  EXPECT_DOUBLE_EQ(std::numeric_limits<double>::max(), bestDualBound);
}  // Get_Best_Dual_Bound_Value
