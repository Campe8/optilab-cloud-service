#include <memory>   // for std::make_shared
#include <utility>  // for std::move

#include <gtest/gtest.h>

#include "optilab_protobuf/meta_bb_paramset.pb.h"

#include "meta_bb_mocks.hpp"
#include "meta_bb/net_paramset.hpp"

TEST(NetParamSet, Broadcast_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  auto communicator = std::make_shared<optnet::MockNetworkCommunicator>();
  auto framework = std::make_shared<MockFramework>(communicator);

  EXPECT_FALSE(communicator->lastBroadcast);
  EXPECT_EQ(communicator->nodeId, -1);

  /*
   * TODO fix linking to protobuf to enable this unit test.
  const int root{0};
  NetParamSet paramSet;
  ASSERT_EQ(paramSet.broadcast(framework, root), 0);

  EXPECT_TRUE(communicator->lastBroadcast);
  EXPECT_EQ(communicator->nodeId, root);
  */
}  // Broadcast_No_Throw
