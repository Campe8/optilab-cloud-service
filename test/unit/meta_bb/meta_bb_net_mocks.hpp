#pragma once

#include <cstdint>  // for uint64_t
#include <limits>   // for std::numeric_limits
#include <memory>   // for std::shared_ptr

// #include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "meta_bb_mocks.hpp"
#include "meta_bb/net_calculation_state.hpp"
#include "meta_bb/net_framework.hpp"
#include "meta_bb/net_node.hpp"
#include "meta_bb/net_solver_state.hpp"
#include "meta_bb/net_solver_termination_state.hpp"

namespace optilab {
namespace  metabb {

class MockNetFramework : public NetFramework {
 public:
  using SPtr = std::shared_ptr<MockNetFramework>;

 public:
  MockNetFramework(bool isRoot) : NetFramework(isRoot) {}

  std::shared_ptr<RacingRampUpParamSet> buildRacingRampUpParamSet() override
  {
    return std::make_shared<MockRacingRampUpParamSet>();
  }

  std::shared_ptr<InitialStat> buildInitialStat() override
  {
    return std::make_shared<MockInitialStat>();
  }

  std::shared_ptr<DiffSubproblem> buildDiffSubproblem() override
  {
    return std::make_shared<MockDiffSubproblem>();
  }

  std::shared_ptr<Solution> buildSolution() override { return std::make_shared<MockSolution>(); }
};

class MockNetSolverTerminationState : public NetSolverTerminationState {
 public:
  MockNetSolverTerminationState()
  : NetSolverTerminationState()
 {
 }

  MockNetSolverTerminationState(solver::InterruptionPoint interrupted,
                                int rank,
                                int totalNumSolved,
                                int minNumSolved,
                                int maxNumSolved,
                                int totalNumSent,
                                int totalNumImprovedIncumbent,
                                int numNodesReceived,
                                int numNodesSolved,
                                int numNodesSolvedAtRoot,
                                int numNodesSolvedAtPreCheck,
                                int numTransferredLocalCutsFromSolver,
                                int minNumTransferredLocalCutsFromSolver,
                                int maxNUmTransferredLocalCutsFromSolver,
                                int numTotalRestarts,
                                int minNumRestarts,
                                int maxNumRestarts,
                                int numVarTightened,
                                int numIntVarTightenedInt,
                                uint64_t runningTimeMsec,
                                uint64_t idleTimeToFirstNodeMsec,
                                uint64_t idleTimeBetweenNodesMsec,
                                uint64_t idleTimeAfterLastNodeMsec,
                                uint64_t idleTimeToWaitNotificationIdMsec,
                                uint64_t idleTimeToWaitAckCompletionMsec,
                                uint64_t idleTimeToWaitTokenMsec,
                                uint64_t totalRootNodeTimeMsec,
                                uint64_t minRootNodeTimeMsec,
                                uint64_t maxRootNodeTimeMsec)
 : NetSolverTerminationState(interrupted,
                             rank,
                             totalNumSolved,
                             minNumSolved,
                             maxNumSolved,
                             totalNumSent,
                             totalNumImprovedIncumbent,
                             numNodesReceived,
                             numNodesSolved,
                             numNodesSolvedAtRoot,
                             numNodesSolvedAtPreCheck,
                             numTransferredLocalCutsFromSolver,
                             minNumTransferredLocalCutsFromSolver,
                             maxNUmTransferredLocalCutsFromSolver,
                             numTotalRestarts,
                             minNumRestarts,
                             maxNumRestarts,
                             numVarTightened,
                             numIntVarTightenedInt,
                             runningTimeMsec,
                             idleTimeToFirstNodeMsec,
                             idleTimeBetweenNodesMsec,
                             idleTimeAfterLastNodeMsec,
                             idleTimeToWaitNotificationIdMsec,
                             idleTimeToWaitAckCompletionMsec,
                             idleTimeToWaitTokenMsec,
                             totalRootNodeTimeMsec,
                             minRootNodeTimeMsec,
                             maxRootNodeTimeMsec)
 {
 }

  optnet::Packet::UPtr buildNetworkPacket()
  {
    return NetSolverTerminationState::buildNetworkPacket();
  }

  inline int getRank() const noexcept { return pRank; }
  inline int getTotalNumNodesSolved() const noexcept { return pTotalNumNodesSolved; }
  inline int getMinNumNodesSolved() const noexcept { return pMinNumNodesSolved; }
  inline int getMaxNumNodesSolved() const noexcept { return pMaxNumNodesSolved; }
  inline int getTotalNumNodesSent() const noexcept { return pTotalNumNodesSent; }
  inline int getTotalNumImprovedIncumbent() const noexcept { return pTotalNumImprovedIncumbent; }
  inline int getNumNodesReceived() const noexcept { return pNumNodesReceived; }
  inline int getNumNodesSolved() const noexcept { return pNumNodesSolved; }
  inline int getNumNodesSolvedAtRoot() const noexcept { return pNumNodesSolvedAtRoot; }
  inline int getNumNodesSolvedAtPreCheck() const noexcept { return pNumNodesSolvedAtPreCheck; }
  inline int getNumTransferredLocalCutsFromSolver() const noexcept
  {
    return pNumTransferredLocalCutsFromSolver;
  }
  inline int getMinTransferredLocalCutsFromSolver() const noexcept
  {
    return pMinTransferredLocalCutsFromSolver;
  }
  inline int getMaxTransferredLocalCutsFromSolver() const noexcept
  {
    return pMaxTransferredLocalCutsFromSolver;
  }
  inline int getNumTotalRestarts() const noexcept { return pNumTotalRestarts; }
  inline int getMinNumRestarts() const noexcept { return pMinNumRestarts; }
  inline int getMaxNumRestarts() const noexcept { return pMaxNumRestarts; }
  inline int getNumVarBoundsTightened() const noexcept { return pNumVarBoundsTightened; }
  inline int getNumIntVarBoundsTightened() const noexcept { return pNumIntVarBoundsTightened; }
  inline uint64_t geTRunnigTimeMsec() const noexcept { return pRunningTimeMsec; }
  inline uint64_t getIdleTimeToFirstNodeMsec() const noexcept { return pIdleTimeToFirstNodeMsec; }
  inline uint64_t getIdleTimeBetweenNodesMsec() const noexcept { return pIdleTimeBetweenNodesMsec; }
  inline uint64_t getIdleTimeAfterLastNodesMsec() const noexcept
  {
    return pIdleTimeAfterLastNodesMsec;
  }
  inline uint64_t getIdleTimeToWaitForNotificationIdMsec() const noexcept
  {
    return pIdleTimeToWaitForNotificationIdMsec;
  }
  inline uint64_t getIdleTimeToWaitForAckCompletionMsec() const noexcept
  {
    return pIdleTimeToWaitForAckCompletionMsec;
  }
  inline uint64_t getIdleTimeToWaitForTokenMsec() const noexcept
  {
    return pIdleTimeToWaitForTokenMsec;
  }
  inline uint64_t getTotalRootNodeTimeMsec() const noexcept { return pTotalRootNodeTimeMsec; }
  inline uint64_t getMinRootNodeTimeMsec() const noexcept { return pMinRootNodeTimeMsec; }
  inline uint64_t getMaxRootNodeTimeMsec() const noexcept { return pMaxRootNodeTimeMsec; }
};


class MockNetSolverState : public NetSolverState {
 public:
  MockNetSolverState(bool racingStage,
                     unsigned int notificationId,
                     int lmId,
                     int globalSubtreeId,
                     long long numSolvedNodes,
                     int numNodesLeft,
                     double bestDualBoundValue,
                     double globalBestPrimalBoundValue,
                     uint64_t timeMsec,
                     double avgDualBoundGainRecv)
 : NetSolverState(racingStage,
                  notificationId,
                  lmId,
                  globalSubtreeId,
                  numSolvedNodes,
                  numNodesLeft,
                  bestDualBoundValue,
                  globalBestPrimalBoundValue,
                  timeMsec,
                  avgDualBoundGainRecv)
 {
 }

  optnet::Packet::UPtr buildNetworkPacket()
  {
    return NetSolverState::buildNetworkPacket();
  }
};

class MockNetCalculationState : public NetCalculationState {
 public:
  using SPtr = std::shared_ptr<MockNetCalculationState>;

 public:
  MockNetCalculationState(solver::TerminationState terminationState,
                          int numNodesSolver = kCalculationStateNodesSolved)
  : NetCalculationState()
 {
    pNodesSolved = numNodesSolver;
    pTerminationState = terminationState;
 }

  MockNetCalculationState(uint64_t compTimeMsec,
                          uint64_t rootTimeMsec,
                          int numNodesSolved,
                          int numNodesSent,
                          int numImprovedIncumbent,
                          solver::TerminationState terminationState,
                          int numNodesSolverWithoutPreprocess,
                          std::size_t numSimplexIterRoot,
                          double avgSimplexIter,
                          std::size_t numTransferredLocalCuts,
                          std::size_t minTransferredLocalCuts,
                          std::size_t maxTransferredLocalCuts,
                          std::size_t numRestarts,
                          double minInfeasibilitySum,
                          double maxInfeasibilitySum,
                          std::size_t minInfeasibilityNum,
                          std::size_t maxInfeasibilityNum,
                          double dualBound)
 : NetCalculationState(compTimeMsec,
                       rootTimeMsec,
                       numNodesSolved,
                       numNodesSent,
                       numImprovedIncumbent,
                       terminationState,
                       numNodesSolverWithoutPreprocess,
                       numSimplexIterRoot,
                       avgSimplexIter,
                       numTransferredLocalCuts,
                       minTransferredLocalCuts,
                       maxTransferredLocalCuts,
                       numRestarts,
                       minInfeasibilitySum,
                       maxInfeasibilitySum,
                       minInfeasibilityNum,
                       maxInfeasibilityNum,
                       dualBound)
 {
 }

  optnet::Packet::UPtr buildNetworkPacket()
  {
    return NetCalculationState::buildNetworkPacket();
  }
};

class MockNetNode : public NetNode {
 public:
  using SPtr = std::shared_ptr<MockNetNode>;

 public:
  MockNetNode() : NetNode() {}

  MockNetNode(const NodeId& nodeId,
           const NodeId& generatorNodeId,
           int depth,
           double dualBoundValue,
           double originalDualBoundValue,
           double estimatedValue,
           DiffSubproblem::SPtr diffSubproblem)
 : NetNode(nodeId, generatorNodeId, depth, dualBoundValue, originalDualBoundValue,
           estimatedValue, diffSubproblem)
 {
 }

  optnet::Packet::UPtr buildNetworkPacket() { return NetNode::buildNetworkPacket(); }
};

}  // namespace metabb
}  // namespace optilab

