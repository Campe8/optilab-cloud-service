#include <limits>     // for std::numeric_limits
#include <memory>     // for std::shared_ptr
#include <stdexcept>  // for std::invalid_argument
#include <vector>

#include <gtest/gtest.h>

#include "meta_bb_mocks.hpp"
#include "meta_bb/calculation_state.hpp"

TEST(CalculationState, Constructor_And_Get_Methods_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  uint64_t compTimeMsec{10};
  uint64_t rootTimeMsec{20};
  int numNodesSolved{5};
  int numNodesSent{4};
  int numImprovedIncumbent{1};
  solver::TerminationState terminationState = solver::TerminationState::TS_TERMINATED_NORMALLY;
  int numNodesSolverWithoutPreprocess{0};
  std::size_t numSimplexIterRoot{2};
  double avgSimplexIter{1.3};
  std::size_t numTransferredLocalCuts{3};
  std::size_t minTransferredLocalCuts{2};
  std::size_t maxTransferredLocalCuts{2};
  std::size_t numRestarts{2};
  double minInfeasibilitySum{2.1};
  double maxInfeasibilitySum{2.2};
  std::size_t minInfeasibilityNum{1};
  std::size_t maxInfeasibilityNum{3};
  double dualBound{-23.4};

  CalculationState::SPtr state;
  ASSERT_NO_THROW(state =
          std::make_shared<MockCalculationState>(
                  compTimeMsec,
                  rootTimeMsec,
                  numNodesSolved,
                  numNodesSent,
                  numImprovedIncumbent,
                  terminationState,
                  numNodesSolverWithoutPreprocess,
                  numSimplexIterRoot,
                  avgSimplexIter,
                  numTransferredLocalCuts,
                  minTransferredLocalCuts,
                  maxTransferredLocalCuts,
                  numRestarts,
                  minInfeasibilitySum,
                  maxInfeasibilitySum,
                  minInfeasibilityNum,
                  maxInfeasibilityNum,
                  dualBound
                  ));
  EXPECT_EQ(compTimeMsec, state->getCompTimeMsec());
  EXPECT_EQ(rootTimeMsec, state->getRootTimeMsec());
  EXPECT_EQ(numRestarts, state->getNumRestarts());
  EXPECT_EQ(numNodesSolved, state->getNumNodesSolved());
  EXPECT_EQ(numNodesSent, state->getNumNodesSent());
  EXPECT_EQ(numImprovedIncumbent, state->getNumImprovedIncumbent());
  EXPECT_EQ(terminationState, state->getTerminationState());
  EXPECT_EQ(numNodesSolverWithoutPreprocess, state->getNumNodesSolvedWithNoPreprocess());
  EXPECT_EQ(dualBound, state->getDualBoundValue());
}  // Constructor_And_Get_Methods_No_Throw
