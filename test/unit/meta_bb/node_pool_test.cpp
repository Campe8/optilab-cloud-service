#include <limits>     // for std::numeric_limits
#include <memory>     // for std::shared_ptr
#include <stdexcept>  // for std::invalid_argument
#include <vector>

#include <gtest/gtest.h>

#include "meta_bb_mocks.hpp"
#include "meta_bb/meta_bb_constants.hpp"
#include "meta_bb/node_pool.hpp"
#include "optimizer_network/network_communicator.hpp"

TEST(NodePoolForMinimization, Constructor_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const double bgap{0.01};
  NodePool::SPtr nodePool;
  ASSERT_NO_THROW(nodePool = std::make_shared<NodePoolForMinimization>(bgap));
  EXPECT_EQ(0, nodePool->getMaxUsageOfPool());
}  // Constructor_No_Throw

TEST(NodePoolForMinimization, Default_Values)
{
  using namespace optilab;
  using namespace metabb;

  const double bgap{0.01};
  NodePool::SPtr nodePool;
  ASSERT_NO_THROW(nodePool = std::make_shared<NodePoolForMinimization>(bgap));
  EXPECT_EQ(0, nodePool->getMaxUsageOfPool());
  EXPECT_TRUE(nodePool->isEmpty());

  Node::SPtr node;
  ASSERT_NO_THROW(node = nodePool->extractNode());
  EXPECT_FALSE(node);

  ASSERT_NO_THROW(node = nodePool->extractNodeRandomly());
  EXPECT_FALSE(node);

  EXPECT_DOUBLE_EQ(std::numeric_limits<double>::max(), nodePool->getBestDualBoundValue());
  EXPECT_EQ(0, nodePool->getNumOfGoodNodes(1000.0));
  EXPECT_EQ(0, nodePool->getNumNodes());
}  // Default_Values

TEST(NodePoolForMinimization, Insert_And_Extract_Nodes)
{
  using namespace optilab;
  using namespace metabb;

  const double bgap{0.01};
  NodePool::SPtr nodePool;
  ASSERT_NO_THROW(nodePool = std::make_shared<NodePoolForMinimization>(bgap));

  const int numNodes{10};
  double dualBound{0.0};
  std::vector<Node::SPtr> nodeList;

  double bestDualBound{dualBound};
  for (int idx = 0; idx < numNodes; ++idx)
  {
    nodeList.push_back(std::make_shared<MockNode>());
    nodeList.back()->setDualBoundValue(dualBound);
    if (dualBound < bestDualBound)
    {
      bestDualBound = dualBound;
    }
    dualBound -= 1.0;
  }

  for (auto node : nodeList)
  {
    EXPECT_NO_THROW(nodePool->insert(node));
  }
  EXPECT_EQ(nodeList.size(), nodePool->getNumNodes());
  EXPECT_EQ(nodeList.size(), nodePool->getMaxUsageOfPool());

  Node::SPtr enode;
  for (int idx = 0; idx < numNodes; ++idx)
  {
    // Check best dual bound before extracting the node
    EXPECT_DOUBLE_EQ(bestDualBound, nodePool->getBestDualBoundValue());

    // Check the number of good nodes.
    // Force the gap between the best node and the best dual bound to be zero such that
    // only the current best node is a "good" node
    EXPECT_EQ(1, nodePool->getNumOfGoodNodes(bestDualBound));

    // Extract the node with the best dual bound
    ASSERT_NO_THROW(enode = nodePool->extractNode());

    // Check that the extracted node has the best dual bound
    EXPECT_DOUBLE_EQ(bestDualBound, enode->getDualBoundValue());

    // Update the best dual bound
    bestDualBound += 1.0;
  }
  EXPECT_TRUE(nodePool->isEmpty());
  EXPECT_EQ(0, nodePool->getNumNodes());
  EXPECT_EQ(nodeList.size(), nodePool->getMaxUsageOfPool());
}  // Insert_And_Extract_Nodes

TEST(NodePoolForMinimization, Insert_And_Extract_Nodes_Randomly)
{
  using namespace optilab;
  using namespace metabb;

  const double bgap{0.01};
  NodePool::SPtr nodePool;
  ASSERT_NO_THROW(nodePool = std::make_shared<NodePoolForMinimization>(bgap));

  const int numNodes{10};
  double dualBound{0.0};
  std::vector<Node::SPtr> nodeList;

  double bestDualBound{dualBound};
  for (int idx = 0; idx < numNodes; ++idx)
  {
    nodeList.push_back(std::make_shared<MockNode>());
    nodeList.back()->setDualBoundValue(dualBound);
    if (dualBound < bestDualBound)
    {
      bestDualBound = dualBound;
    }
    dualBound -= 1.0;
  }

  for (auto node : nodeList)
  {
    EXPECT_NO_THROW(nodePool->insert(node));
  }
  EXPECT_EQ(nodeList.size(), nodePool->getNumNodes());
  EXPECT_EQ(nodeList.size(), nodePool->getMaxUsageOfPool());

  Node::SPtr enode;
  for (int idx = 0; idx < numNodes; ++idx)
  {
    ASSERT_NO_THROW(enode = nodePool->extractNodeRandomly());
  }
  EXPECT_TRUE(nodePool->isEmpty());
  EXPECT_EQ(0, nodePool->getNumNodes());
  EXPECT_EQ(nodeList.size(), nodePool->getMaxUsageOfPool());
}  // Insert_And_Extract_Nodes_Randomly

TEST(NodePoolForMinimization, Remove_Bounded_Nodes)
{
  using namespace optilab;
  using namespace metabb;

  const double bgap{0.01};
  NodePool::SPtr nodePool;
  ASSERT_NO_THROW(nodePool = std::make_shared<NodePoolForMinimization>(bgap));

  const int numNodes{10};
  double dualBound{0.0};
  std::vector<Node::SPtr> nodeList;

  double bestDualBound{dualBound};
  for (int idx = 0; idx < numNodes; ++idx)
  {
    nodeList.push_back(std::make_shared<MockNode>());
    nodeList.back()->setDualBoundValue(dualBound);
    if (dualBound < bestDualBound)
    {
      bestDualBound = dualBound;
    }
    dualBound -= 1.0;
  }

  for (auto node : nodeList)
  {
    EXPECT_NO_THROW(nodePool->insert(node));
  }
  EXPECT_EQ(nodeList.size(), nodePool->getNumNodes());
  EXPECT_EQ(nodeList.size(), nodePool->getMaxUsageOfPool());

  // Remove half nodes, i.e., nodes with lower bound higher than incumbent
  double incumbent{bestDualBound/2.0};
  int numDeletedNodes{0};
  EXPECT_NO_THROW(numDeletedNodes = nodePool->removeBoundedNodes(incumbent));
  EXPECT_EQ(numNodes/2, numDeletedNodes);
  EXPECT_EQ(numNodes/2, nodePool->getNumNodes());

  // Remove all nodes
  incumbent = bestDualBound - 1.0;
  EXPECT_NO_THROW(numDeletedNodes = nodePool->removeBoundedNodes(incumbent));
  EXPECT_TRUE(nodePool->isEmpty());
}  // Remove_Bounded_Nodes

TEST(NodePoolForCleanUp, Default_Values)
{
  using namespace optilab;
  using namespace metabb;

  const double bgap{0.01};
  NodePool::SPtr nodePool;
  ASSERT_NO_THROW(nodePool = std::make_shared<NodePoolForCleanUp>(bgap));
  EXPECT_EQ(0, nodePool->getMaxUsageOfPool());
  EXPECT_TRUE(nodePool->isEmpty());

  Node::SPtr node;
  ASSERT_NO_THROW(node = nodePool->extractNode());
  EXPECT_FALSE(node);

  ASSERT_NO_THROW(node = nodePool->extractNodeRandomly());
  EXPECT_FALSE(node);

  EXPECT_DOUBLE_EQ(std::numeric_limits<double>::max(), nodePool->getBestDualBoundValue());
  EXPECT_EQ(0, nodePool->getNumOfGoodNodes(1000.0));
  EXPECT_EQ(0, nodePool->getNumNodes());
}  // Default_Values

TEST(NodePoolForCleanUp, Insert_And_Extract_Nodes)
{
  using namespace optilab;
  using namespace metabb;

  const double bgap{0.01};
  NodePool::SPtr nodePool;
  ASSERT_NO_THROW(nodePool = std::make_shared<NodePoolForCleanUp>(bgap));

  const int numNodes{10};
  double dualBound{0.0};
  std::vector<Node::SPtr> nodeList;

  double bestDualBound{dualBound};
  for (int idx = 0; idx < numNodes; ++idx)
  {
    nodeList.push_back(std::make_shared<MockNode>());
    nodeList.back()->setDualBoundValue(dualBound);
    if (dualBound > bestDualBound)
    {
      bestDualBound = dualBound;
    }
    dualBound += 1.0;
  }

  for (auto node : nodeList)
  {
    EXPECT_NO_THROW(nodePool->insert(node));
  }
  EXPECT_EQ(nodeList.size(), nodePool->getNumNodes());
  EXPECT_EQ(nodeList.size(), nodePool->getMaxUsageOfPool());

  Node::SPtr enode;
  for (int idx = 0; idx < numNodes; ++idx)
  {
    // Check best dual bound before extracting the node.
    // This is always the minimum
    EXPECT_DOUBLE_EQ(0.0, nodePool->getBestDualBoundValue());

    // Check the number of good nodes.
    // Force the gap between the best node and the best dual bound to be zero such that
    // only the current best node is a "good" node
    EXPECT_EQ(1, nodePool->getNumOfGoodNodes(0.0));

    // Extract the node with the best dual bound
    ASSERT_NO_THROW(enode = nodePool->extractNode());

    // Check that the extracted node has the best dual bound
    EXPECT_DOUBLE_EQ(bestDualBound, enode->getDualBoundValue());

    // Update the best dual bound
    bestDualBound -= 1.0;
  }
  EXPECT_TRUE(nodePool->isEmpty());
  EXPECT_EQ(0, nodePool->getNumNodes());
  EXPECT_EQ(nodeList.size(), nodePool->getMaxUsageOfPool());
}  // Insert_And_Extract_Nodes

TEST(NodePoolForCleanUp, Insert_And_Extract_Nodes_Randomly)
{
  using namespace optilab;
  using namespace metabb;

  const double bgap{0.01};
  NodePool::SPtr nodePool;
  ASSERT_NO_THROW(nodePool = std::make_shared<NodePoolForCleanUp>(bgap));

  const int numNodes{10};
  double dualBound{0.0};
  std::vector<Node::SPtr> nodeList;

  double bestDualBound{dualBound};
  for (int idx = 0; idx < numNodes; ++idx)
  {
    nodeList.push_back(std::make_shared<MockNode>());
    nodeList.back()->setDualBoundValue(dualBound);
    if (dualBound > bestDualBound)
    {
      bestDualBound = dualBound;
    }
    dualBound += 1.0;
  }

  for (auto node : nodeList)
  {
    EXPECT_NO_THROW(nodePool->insert(node));
  }
  EXPECT_EQ(nodeList.size(), nodePool->getNumNodes());
  EXPECT_EQ(nodeList.size(), nodePool->getMaxUsageOfPool());

  Node::SPtr enode;
  for (int idx = 0; idx < numNodes; ++idx)
  {
    ASSERT_NO_THROW(enode = nodePool->extractNodeRandomly());
  }
  EXPECT_TRUE(nodePool->isEmpty());
  EXPECT_EQ(0, nodePool->getNumNodes());
  EXPECT_EQ(nodeList.size(), nodePool->getMaxUsageOfPool());
}  // Insert_And_Extract_Nodes_Randomly

TEST(NodePoolForCleanUp, Remove_Bounded_Nodes)
{
  using namespace optilab;
  using namespace metabb;

  const double bgap{0.01};
  NodePool::SPtr nodePool;
  ASSERT_NO_THROW(nodePool = std::make_shared<NodePoolForCleanUp>(bgap));

  const int numNodes{10};
  double dualBound{0.0};
  std::vector<Node::SPtr> nodeList;

  double bestDualBound{dualBound};
  for (int idx = 0; idx < numNodes; ++idx)
  {
    nodeList.push_back(std::make_shared<MockNode>());
    nodeList.back()->setDualBoundValue(dualBound);
    if (dualBound > bestDualBound)
    {
      bestDualBound = dualBound;
    }
    dualBound += 1.0;
  }

  for (auto node : nodeList)
  {
    EXPECT_NO_THROW(nodePool->insert(node));
  }
  EXPECT_EQ(nodeList.size(), nodePool->getNumNodes());
  EXPECT_EQ(nodeList.size(), nodePool->getMaxUsageOfPool());

  // Remove half nodes, i.e., nodes with lower bound higher than incumbent
  double incumbent{bestDualBound/2.0};
  int numDeletedNodes{0};
  EXPECT_NO_THROW(numDeletedNodes = nodePool->removeBoundedNodes(incumbent));
  EXPECT_EQ(numNodes/2, numDeletedNodes);
  EXPECT_EQ(numNodes/2, nodePool->getNumNodes());

  // Remove all nodes
  incumbent = -1.0;
  EXPECT_NO_THROW(numDeletedNodes = nodePool->removeBoundedNodes(incumbent));
  EXPECT_TRUE(nodePool->isEmpty());
}  // Remove_Bounded_Nodes


