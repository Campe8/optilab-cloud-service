#include <cstdint>  // for uint64_t
#include <limits>   // for std::numeric_limits
#include <memory>   // for std::shared_ptr

#include <gtest/gtest.h>

#include "meta_bb_mocks.hpp"
#include "meta_bb/racing_rampup_paramset.hpp"

TEST(RacingRampUpParamSet, Default_Constructor_And_Default_Values_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  MockRacingRampUpParamSet::SPtr params;
  ASSERT_NO_THROW(params = std::make_shared<MockRacingRampUpParamSet>());
  EXPECT_EQ(RacingRampUpParamSet::RacingTerminationCriteria::RTC_UNDEF,
            params->getRacingTerminationCriteria());
  EXPECT_EQ(-1, params->getStopRacingNumNodesLeft());
  EXPECT_EQ(std::numeric_limits<uint64_t>::max(), params->getStopRacingTimeoutMsec());
}  // Default_Constructor_And_Default_Values_No_Throw

TEST(RacingRampUpParamSet, Constructor_And_Default_Values_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const int numNodesLeft{10};
  const uint64_t timeout{1000};
  MockRacingRampUpParamSet::SPtr params;
  ASSERT_NO_THROW(params = std::make_shared<MockRacingRampUpParamSet>(
          RacingRampUpParamSet::RacingTerminationCriteria::RTC_WITH_NODES_LEFT,
          numNodesLeft, timeout));
  EXPECT_EQ(RacingRampUpParamSet::RacingTerminationCriteria::RTC_WITH_NODES_LEFT,
            params->getRacingTerminationCriteria());
  EXPECT_EQ(numNodesLeft, params->getStopRacingNumNodesLeft());
  EXPECT_EQ(timeout, params->getStopRacingTimeoutMsec());
}  // Constructor_And_Default_Values_No_Throw
