#include <limits>   // for std::numeric_limits
#include <memory>   // for std::shared_ptr
#include <numeric>  // for std::iota
#include <vector>

#include <gtest/gtest.h>

#include "meta_bb_mocks.hpp"
#include "meta_bb/meta_bb_constants.hpp"
#include "meta_bb/solver_pool.hpp"
#include "meta_bb/solver_termination_state.hpp"

namespace {
constexpr int kSolverPoolElementRank{1};
constexpr int kLMId = 10;
constexpr int kGlbSubtreeId = 11;
constexpr int kSolverId = 12;
constexpr long long kSeqIdNum = 1000;
constexpr int kNodeDepth = 20;
constexpr double kNodeDualBound = -1.0;
constexpr double kNodeOrigDualBound = 1.0;
constexpr double kNodeEstimatedValue = -10.0;
}  // namespace

TEST(SolverPoolElement, Solver_Pool_Element_Default_Constructor_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  SolverPoolElement::SPtr pel;
  ASSERT_NO_THROW(pel = std::make_shared<SolverPoolElement>(kSolverPoolElementRank));
  EXPECT_EQ(kSolverPoolElementRank, pel->getRank());

  // Check default values
  EXPECT_FALSE(pel->getCurrentNode());
  EXPECT_FALSE(pel->extractCurrentNode());
  EXPECT_EQ(0, pel->getSelectionHeapElement());
  EXPECT_EQ(0, pel->getCollectingModeSolverHeapElement());
  EXPECT_EQ(0, pel->getNumOfNodesSolved());
  EXPECT_EQ(0, pel->getNumOfNodesLeft());
  EXPECT_EQ(0, pel->getNumOfDiffNodesSolved());
  EXPECT_EQ(0, pel->getNumOfDiffNodesLeft());
  EXPECT_EQ(0, pel->getCollectingModeSolverHeapElement());
  EXPECT_DOUBLE_EQ(0.0, pel->getBestDualBoundValue());
  EXPECT_TRUE(pel->isOutCollectingMode());
  EXPECT_FALSE(pel->isInCollectingMode());
  EXPECT_FALSE(pel->isCandidateOfCollecting());
  EXPECT_FALSE(pel->getTermState());
  EXPECT_FALSE(pel->isRacingStage());
  EXPECT_FALSE(pel->isEvaluationStage());
  EXPECT_FALSE(pel->isGenerator());
  EXPECT_FALSE(pel->isCollectingProhibited());
  EXPECT_FALSE(pel->isDualBoundGainTesting());
}  // Solver_Pool_Element_Default_Constructor_No_Throw

TEST(SolverPoolElement, Solver_Pool_Element_Activate)
{
  using namespace optilab;
  using namespace metabb;

  SolverPoolElement::SPtr pel;
  ASSERT_NO_THROW(pel = std::make_shared<SolverPoolElement>(kSolverPoolElementRank));

  // Activate the pool element without a node
  EXPECT_NO_THROW(pel->activate(nullptr));
  EXPECT_EQ(solver::SolverStatus::SS_ACTIVE, pel->getStatus()) << "Pool element should be active "
          "on a nullptr node";
  EXPECT_DOUBLE_EQ(std::numeric_limits<double>::lowest(), pel->getBestDualBoundValue());
  EXPECT_EQ(0, pel->getNumOfNodesSolved());
  EXPECT_EQ(1, pel->getNumOfNodesLeft()) << "There should be 1 node left to be solved, "
          "the current active one";

  // Activate the pool element on a node
  std::shared_ptr<optilab::metabb::SubtreeId> stid;
  std::shared_ptr<optilab::metabb::NodeId> nGenId;
  std::shared_ptr<optilab::metabb::NodeId> nId;
  optilab::metabb::DiffSubproblem::SPtr diffSubproblem;
  stid = std::make_shared<SubtreeId>(kLMId, kGlbSubtreeId, kSolverId);
  nGenId = std::make_shared<NodeId>(kSeqIdNum, *(stid.get()));
  nId = std::make_shared<NodeId>(kSeqIdNum+1, *(stid.get()));
  diffSubproblem = std::make_shared<MockDiffSubproblem>();

  optilab::metabb::MockNode::SPtr node;
  node = std::make_shared<MockNode>(*(nId.get()), *(nGenId.get()), kNodeDepth,
                                    kNodeDualBound, kNodeOrigDualBound, kNodeEstimatedValue,
                                    diffSubproblem);
  EXPECT_NO_THROW(pel->activate(node));
  EXPECT_EQ(solver::SolverStatus::SS_ACTIVE, pel->getStatus()) << "Pool element should be active "
          "on a nullptr node";
  EXPECT_DOUBLE_EQ(kNodeDualBound, pel->getBestDualBoundValue());
  EXPECT_EQ(0, pel->getNumOfNodesSolved());
  EXPECT_EQ(1, pel->getNumOfNodesLeft()) << "There should be 1 node left to be solved, "
          "the current active one";

  // Deactivate the pool element
  EXPECT_NO_THROW(pel->deactivate());
  EXPECT_EQ(solver::SolverStatus::SS_INACTIVE, pel->getStatus());
  EXPECT_EQ(0, pel->getNumOfNodesSolved());
  EXPECT_EQ(0, pel->getNumOfNodesLeft());
}  // Solver_Pool_Element_Activate

TEST(SolverPoolElement, Solver_Pool_Element_Racing_Activate)
{
  using namespace optilab;
  using namespace metabb;

  SolverPoolElement::SPtr pel;
  ASSERT_NO_THROW(pel = std::make_shared<SolverPoolElement>(kSolverPoolElementRank));

  // Activate the pool element without a node
  EXPECT_NO_THROW(pel->racingActivate());
  EXPECT_EQ(solver::SolverStatus::SS_RACING, pel->getStatus()) << "Pool element should be active "
          "on a nullptr node";
  EXPECT_TRUE(pel->isRacingStage());
  EXPECT_DOUBLE_EQ(std::numeric_limits<double>::lowest(), pel->getBestDualBoundValue());
  EXPECT_EQ(-1, pel->getNumOfNodesSolved());
  EXPECT_EQ(1, pel->getNumOfNodesLeft()) << "There should be 1 node left to be solved, "
          "the current active one";

  // Switch into evaluation stage
  EXPECT_NO_THROW(pel->switchIntoEvaluation());
  EXPECT_FALSE(pel->isRacingStage());
  EXPECT_TRUE(pel->isEvaluationStage());

  // Switch back to racing
  EXPECT_NO_THROW(pel->switchOutEvaluation());
  EXPECT_TRUE(pel->isRacingStage());
  EXPECT_FALSE(pel->isEvaluationStage());

  // Deactivate the pool element
  EXPECT_NO_THROW(pel->deactivate());
  EXPECT_EQ(solver::SolverStatus::SS_INACTIVE, pel->getStatus());
  EXPECT_EQ(0, pel->getNumOfNodesSolved());
  EXPECT_EQ(0, pel->getNumOfNodesLeft());
}  // Solver_Pool_Element_Racing_Activate

TEST(SolverPoolElement, Solver_Pool_Element_Kill_Activate)
{
  using namespace optilab;
  using namespace metabb;

  SolverPoolElement::SPtr pel;
  ASSERT_NO_THROW(pel = std::make_shared<SolverPoolElement>(kSolverPoolElementRank));

  // Activate the pool element without a node
  EXPECT_NO_THROW(pel->racingActivate());
  EXPECT_TRUE(pel->isRacingStage());

  Node::SPtr ptr;
  EXPECT_NO_THROW(ptr = pel->kill());
  EXPECT_EQ(solver::SolverStatus::SS_DEAD, pel->getStatus());
  EXPECT_FALSE(pel->isRacingStage());
  EXPECT_FALSE(pel->isEvaluationStage());
  EXPECT_FALSE(pel->isInCollectingMode());
  EXPECT_FALSE(pel->isDualBoundGainTesting());
  EXPECT_EQ(0, pel->getNumOfNodesLeft());
  EXPECT_EQ(0, pel->getNumOfNodesLeft());
  EXPECT_FALSE(ptr);
}  // Solver_Pool_Element_Kill_Activate

TEST(SolverPoolElement, Solver_Pool_Element_Get_Set)
{
  using namespace optilab;
  using namespace metabb;

  SolverPoolElement::SPtr pel;
  ASSERT_NO_THROW(pel = std::make_shared<SolverPoolElement>(kSolverPoolElementRank));

  const int val{10};
  EXPECT_NO_THROW(pel->setSelectionHeapElement(val));
  EXPECT_EQ(val, pel->getSelectionHeapElement());
  EXPECT_NO_THROW(pel->setCollectingModeSolverHeapElement(val));
  EXPECT_EQ(val, pel->getCollectingModeSolverHeapElement());
  EXPECT_NO_THROW(pel->setNumOfNodesSolved(val));
  EXPECT_EQ(val, pel->getNumOfNodesSolved());
  EXPECT_NO_THROW(pel->setNumOfDiffNodesSolved(val));
  EXPECT_EQ(val, pel->getNumOfDiffNodesSolved());
  EXPECT_NO_THROW(pel->setNumOfNodesLeft(val));
  EXPECT_EQ(val, pel->getNumOfNodesLeft());
  EXPECT_THROW(pel->setDualBoundValue(1.0), std::runtime_error);
  EXPECT_NO_THROW(pel->setBestDualBoundValue(val * 1.0));
  EXPECT_DOUBLE_EQ(val * 1.0, pel->getBestDualBoundValue());
  EXPECT_NO_THROW(pel->setCandidateOfCollecting(true));
  EXPECT_TRUE(pel->isCandidateOfCollecting());
  EXPECT_NO_THROW(pel->setNoGenerator());
  EXPECT_FALSE(pel->isGenerator());
  EXPECT_NO_THROW(pel->prohibitCollectingMode());
  EXPECT_TRUE(pel->isCollectingProhibited());
  EXPECT_NO_THROW(pel->setCollectingIsAllowed());
  EXPECT_FALSE(pel->isCollectingProhibited());
  EXPECT_NO_THROW(pel->setDualBoundGainTesting(true));
  EXPECT_TRUE(pel->isDualBoundGainTesting());
}  // Solver_Pool_Element_Get_Set

TEST(AscendingSelectionHeap, Sorting_Ascending_Heap)
{
  // Testing the Min Heap
  using namespace optilab;
  using namespace metabb;

  const int maxHeapSize{10};
  std::shared_ptr<AscendingSelectionHeap> heap;
  ASSERT_NO_THROW(heap = std::make_shared<AscendingSelectionHeap>(maxHeapSize));

  std::vector<double> bounds(maxHeapSize);
  std::iota(bounds.begin(), bounds.end(), 1);

  std::vector<SolverPoolElement::SPtr> elemList;
  for (int ctr = 0; ctr < maxHeapSize; ++ctr)
  {
    elemList.push_back(std::make_shared<SolverPoolElement>(kSolverPoolElementRank));
    elemList.back()->setBestDualBoundValue(bounds[ctr]);
  }

  for (int idx = 0; idx < maxHeapSize; ++idx)
  {
    EXPECT_EQ(SelectionHeap::ResultOfInsert::ROI_SUCCEEDED, heap->insert(elemList[idx]));
  }

  for (int idx = 0; idx < maxHeapSize; ++idx)
  {
    EXPECT_DOUBLE_EQ(bounds[idx], heap->top()->getBestDualBoundValue());
    heap->remove();
  }
  EXPECT_EQ(0, heap->getHeapSize());
}  // Sorting_Ascending_Heap

TEST(DescendingSelectionHeap, Sorting_Descending_Heap)
{
  // Testing the Max Heap
  using namespace optilab;
  using namespace metabb;

  const int maxHeapSize{10};
  std::shared_ptr<DescendingSelectionHeap> heap;
  ASSERT_NO_THROW(heap = std::make_shared<DescendingSelectionHeap>(maxHeapSize));

  std::vector<double> bounds(maxHeapSize);
  std::iota(bounds.begin(), bounds.end(), 1);

  std::vector<SolverPoolElement::SPtr> elemList;
  for (int ctr = 0; ctr < maxHeapSize; ++ctr)
  {
    elemList.push_back(std::make_shared<SolverPoolElement>(kSolverPoolElementRank));
    elemList.back()->setBestDualBoundValue(bounds[ctr]);
  }

  for (int idx = 0; idx < maxHeapSize; ++idx)
  {
    EXPECT_EQ(SelectionHeap::ResultOfInsert::ROI_SUCCEEDED, heap->insert(elemList[idx]));
  }

  for (int idx = 0; idx < maxHeapSize; ++idx)
  {
    EXPECT_DOUBLE_EQ(bounds[maxHeapSize - 1 - idx], heap->top()->getBestDualBoundValue());
    heap->remove();
  }
  EXPECT_EQ(0, heap->getHeapSize());
}  // Sorting_Ascending_Heap

TEST(CollectingModeSolverHeap, Constructor_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const int maxHeapSize{10};
  std::shared_ptr<CollectingModeSolverHeap> heap;
  ASSERT_NO_THROW(heap = std::make_shared<CollectingModeSolverHeap>(maxHeapSize));
  EXPECT_EQ(maxHeapSize, heap->getMaxHeapSize());
}  // Constructor_No_Throw

TEST(CollectingModeSolverHeap, Resize_Heap)
{
  using namespace optilab;
  using namespace metabb;

  const int maxHeapSize{10};
  CollectingModeSolverHeap heap(maxHeapSize);
  EXPECT_EQ(0, heap.getHeapSize());
  EXPECT_EQ(maxHeapSize, heap.getMaxHeapSize());

  EXPECT_NO_THROW(heap.resize(maxHeapSize + 10));
  EXPECT_EQ(0, heap.getHeapSize());
  EXPECT_EQ(maxHeapSize + 10, heap.getMaxHeapSize());
}  // Resize_Heap

TEST(CollectingModeSolverHeap, Insert_Solver_Pool_Elements)
{
  using namespace optilab;
  using namespace metabb;

  int maxHeapSize{1};
  CollectingModeSolverHeap heap(maxHeapSize);
  EXPECT_EQ(0, heap.getHeapSize());
  EXPECT_EQ(maxHeapSize, heap.getMaxHeapSize());

  int numElem = 5;

  // Insert two elements, the second element should
  // return an error due to heap size
  std::vector<double> bounds(numElem);
  std::iota(bounds.begin(), bounds.end(), 1.5);

  std::vector<SolverPoolElement::SPtr> elemList;
  for (int ctr = 0; ctr < numElem; ++ctr)
  {
    elemList.push_back(std::make_shared<SolverPoolElement>(kSolverPoolElementRank));
    elemList.back()->setBestDualBoundValue(bounds[ctr]);
  }

  // Adding the first element should work as expected
  EXPECT_EQ(SelectionHeap::ResultOfInsert::ROI_SUCCEEDED, heap.insert(elemList[0]));

  // The second element should not be added due to heap size
  EXPECT_EQ(SelectionHeap::ResultOfInsert::ROI_FAILED_BY_FULL, heap.insert(elemList[1]));
  EXPECT_EQ(1, heap.getHeapSize());

  // Resize the heap to contain all elements
  ASSERT_NO_THROW(heap.resize(numElem));

  // Add the remaining elements
  for (int idx = 1; idx < numElem; ++idx)
  {
    EXPECT_EQ(SelectionHeap::ResultOfInsert::ROI_SUCCEEDED, heap.insert(elemList[idx]));
  }
  EXPECT_EQ(numElem, heap.getHeapSize()) << "The heap should contain " << numElem << " elements.";

  SolverPoolElement::SPtr solver;
  for (int idx = 0; idx < numElem; ++idx)
  {
    ASSERT_TRUE(!!heap.top());
    EXPECT_EQ(bounds.back(), (heap.top())->getBestDualBoundValue());

    // Remove the top
    ASSERT_NO_THROW(solver = heap.remove());
    ASSERT_TRUE(!!solver);
    EXPECT_EQ(bounds.back(), solver->getBestDualBoundValue());
    EXPECT_EQ(numElem - idx - 1, heap.getHeapSize());

    // Remove the bound from the vector to prepare for next comparison
    bounds.pop_back();
  }
}  // Insert_Solver_Pool_Elements

TEST(CollectingModeSolverHeap, Update_Dual_Bound_Value)
{
  using namespace optilab;
  using namespace metabb;

  int numElem = 5;
  CollectingModeSolverHeap heap(numElem);

  std::vector<double> bounds(numElem);
  std::iota(bounds.begin(), bounds.end(), 1.5);

  std::vector<SolverPoolElement::SPtr> elemList;
  for (int ctr = 0; ctr < numElem; ++ctr)
  {
    elemList.push_back(std::make_shared<SolverPoolElement>(kSolverPoolElementRank));
    elemList.back()->setBestDualBoundValue(bounds[ctr]);
  }

  // Add the elements to the heap
  for (int idx = 0; idx < numElem; ++idx)
  {
    EXPECT_EQ(SelectionHeap::ResultOfInsert::ROI_SUCCEEDED, heap.insert(elemList[idx]));
  }

  // Check the current top
  ASSERT_TRUE(!!heap.top());
  EXPECT_EQ(bounds.back(), (heap.top())->getBestDualBoundValue());

  // Update the first solver element (bottom of the heap).
  // This should bring it to the front of the heap
  const double newBound{1000.0};
  EXPECT_NO_THROW(heap.updateDualBoundValue(elemList.at(0), newBound));

  // Check the new top of the heap
  EXPECT_EQ(elemList.at(0).get(), heap.top().get());
  EXPECT_EQ(newBound, (heap.top())->getBestDualBoundValue());
}  // Update_Dual_Bound_Value

TEST(CollectingModeSolverHeap, Delete_Element)
{
  using namespace optilab;
  using namespace metabb;

  int numElem = 5;
  CollectingModeSolverHeap heap(numElem);

  std::vector<double> bounds(numElem);
  std::iota(bounds.begin(), bounds.end(), 1.5);

  std::vector<SolverPoolElement::SPtr> elemList;
  for (int ctr = 0; ctr < numElem; ++ctr)
  {
    elemList.push_back(std::make_shared<SolverPoolElement>(kSolverPoolElementRank));
    elemList.back()->setBestDualBoundValue(bounds[ctr]);
  }

  // Add the elements to the heap
  for (int idx = 0; idx < numElem; ++idx)
  {
    EXPECT_EQ(SelectionHeap::ResultOfInsert::ROI_SUCCEEDED, heap.insert(elemList[idx]));
  }
  EXPECT_EQ(numElem, heap.getHeapSize()) << "The heap should contain " << numElem << " elements.";

  // Check the current top
  ASSERT_TRUE(!!heap.top());
  EXPECT_EQ(bounds.back(), (heap.top())->getBestDualBoundValue());

  // Delete top of the heap
  EXPECT_NO_THROW(heap.deleteElement(elemList.back()));
  EXPECT_EQ(numElem - 1, heap.getHeapSize());
  EXPECT_EQ(numElem, heap.getMaxHeapSize()) << "Max heap size should be always the same";

  // Check the current top again
  bounds.pop_back();
  ASSERT_TRUE(!!heap.top());
  EXPECT_EQ(bounds.back(), (heap.top())->getBestDualBoundValue());
}  // Delete_Element

TEST(AscendingCollectingModeSolverHeap, Constructor_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const int maxHeapSize{10};
  std::shared_ptr<AscendingCollectingModeSolverHeap> heap;
  ASSERT_NO_THROW(heap = std::make_shared<AscendingCollectingModeSolverHeap>(maxHeapSize));
  EXPECT_EQ(maxHeapSize, heap->getMaxHeapSize());
}  // Constructor_No_Throw

TEST(AscendingCollectingModeSolverHeap, Resize_Heap)
{
  using namespace optilab;
  using namespace metabb;

  const int maxHeapSize{10};
  AscendingCollectingModeSolverHeap heap(maxHeapSize);
  EXPECT_EQ(0, heap.getHeapSize());
  EXPECT_EQ(maxHeapSize, heap.getMaxHeapSize());

  EXPECT_NO_THROW(heap.resize(maxHeapSize + 10));
  EXPECT_EQ(0, heap.getHeapSize());
  EXPECT_EQ(maxHeapSize + 10, heap.getMaxHeapSize());
}  // Resize_Heap

TEST(AscendingCollectingModeSolverHeap, Insert_Solver_Pool_Elements)
{
  using namespace optilab;
  using namespace metabb;

  int maxHeapSize{1};
  AscendingCollectingModeSolverHeap heap(maxHeapSize);
  EXPECT_EQ(0, heap.getHeapSize());
  EXPECT_EQ(maxHeapSize, heap.getMaxHeapSize());

  int numElem = 5;

  // Insert two elements, the second element should
  // return an error due to heap size
  std::vector<double> bounds(numElem);
  std::iota(bounds.begin(), bounds.end(), 1.5);

  std::vector<SolverPoolElement::SPtr> elemList;
  for (int ctr = 0; ctr < numElem; ++ctr)
  {
    elemList.push_back(std::make_shared<SolverPoolElement>(kSolverPoolElementRank));
    elemList.back()->setBestDualBoundValue(bounds[ctr]);
  }

  // Adding the first element should work as expected
  EXPECT_EQ(SelectionHeap::ResultOfInsert::ROI_SUCCEEDED, heap.insert(elemList[0]));

  // The second element should not be added due to heap size
  EXPECT_EQ(SelectionHeap::ResultOfInsert::ROI_FAILED_BY_FULL, heap.insert(elemList[1]));
  EXPECT_EQ(1, heap.getHeapSize());

  // Resize the heap to contain all elements
  ASSERT_NO_THROW(heap.resize(numElem));

  // Add the remaining elements
  for (int idx = 1; idx < numElem; ++idx)
  {
    EXPECT_EQ(SelectionHeap::ResultOfInsert::ROI_SUCCEEDED, heap.insert(elemList[idx]));
  }
  EXPECT_EQ(numElem, heap.getHeapSize()) << "The heap should contain " << numElem << " elements.";

  SolverPoolElement::SPtr solver;
  for (int idx = 0; idx < numElem; ++idx)
  {
    ASSERT_TRUE(!!heap.top());
    EXPECT_EQ(bounds[idx], (heap.top())->getBestDualBoundValue());

    // Remove the top
    ASSERT_NO_THROW(solver = heap.remove());
    ASSERT_TRUE(!!solver);
    EXPECT_EQ(bounds[idx], solver->getBestDualBoundValue());
    EXPECT_EQ(numElem - idx - 1, heap.getHeapSize());
  }
}  // Insert_Solver_Pool_Elements

TEST(AscendingCollectingModeSolverHeap, Update_Dual_Bound_Value)
{
  using namespace optilab;
  using namespace metabb;

  int numElem = 5;
  AscendingCollectingModeSolverHeap heap(numElem);

  std::vector<double> bounds(numElem);
  std::iota(bounds.begin(), bounds.end(), 1.5);

  std::vector<SolverPoolElement::SPtr> elemList;
  for (int ctr = 0; ctr < numElem; ++ctr)
  {
    elemList.push_back(std::make_shared<SolverPoolElement>(kSolverPoolElementRank));
    elemList.back()->setBestDualBoundValue(bounds[ctr]);
  }

  // Add the elements to the heap
  for (int idx = 0; idx < numElem; ++idx)
  {
    EXPECT_EQ(SelectionHeap::ResultOfInsert::ROI_SUCCEEDED, heap.insert(elemList[idx]));
  }

  // Check the current top
  ASSERT_TRUE(!!heap.top());
  EXPECT_EQ(bounds.front(), (heap.top())->getBestDualBoundValue());

  // Update the first solver element (bottom of the heap).
  // This should bring it to the front of the heap
  const double newBound{-1000.0};
  EXPECT_NO_THROW(heap.updateDualBoundValue(elemList.back(), newBound));

  // Check the new top of the heap
  EXPECT_EQ(elemList.back().get(), heap.top().get());
  EXPECT_EQ(newBound, (heap.top())->getBestDualBoundValue());
}  // Update_Dual_Bound_Value

TEST(AscendingCollectingModeSolverHeap, Delete_Element)
{
  using namespace optilab;
  using namespace metabb;

  int numElem = 5;
  AscendingCollectingModeSolverHeap heap(numElem);

  std::vector<double> bounds(numElem);
  std::iota(bounds.begin(), bounds.end(), 1.5);

  std::vector<SolverPoolElement::SPtr> elemList;
  for (int ctr = 0; ctr < numElem; ++ctr)
  {
    elemList.push_back(std::make_shared<SolverPoolElement>(kSolverPoolElementRank));
    elemList.back()->setBestDualBoundValue(bounds[ctr]);
  }

  // Add the elements to the heap
  for (int idx = 0; idx < numElem; ++idx)
  {
    EXPECT_EQ(SelectionHeap::ResultOfInsert::ROI_SUCCEEDED, heap.insert(elemList[idx]));
  }
  EXPECT_EQ(numElem, heap.getHeapSize()) << "The heap should contain " << numElem << " elements.";

  // Check the current top
  ASSERT_TRUE(!!heap.top());
  EXPECT_EQ(bounds.front(), (heap.top())->getBestDualBoundValue());

  // Delete top of the heap
  EXPECT_NO_THROW(heap.deleteElement(elemList.at(0)));
  EXPECT_EQ(numElem - 1, heap.getHeapSize());
  EXPECT_EQ(numElem, heap.getMaxHeapSize()) << "Max heap size should be always the same";

  // Check the current top again
  ASSERT_TRUE(!!heap.top());
  EXPECT_EQ(bounds.at(1), (heap.top())->getBestDualBoundValue());
}  // Delete_Element
