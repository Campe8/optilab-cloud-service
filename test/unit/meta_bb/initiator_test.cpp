#include <limits>     // for std::numeric_limits
#include <memory>     // for std::make_shared
#include <stdexcept>  // for std::invalid_argument

#include <gtest/gtest.h>

#include "meta_bb_mocks.hpp"
#include "meta_bb/initiator.hpp"

TEST(Initiator, Constructor_No_Framework_Throws)
{
  using namespace optilab;
  using namespace metabb;

  Initiator::SPtr initiator;
  EXPECT_THROW(initiator = std::make_shared<MockInitiator>(nullptr), std::invalid_argument);
}  // Constructor_No_Framework_Throws

TEST(Initiator, Constructor_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  Initiator::SPtr initiator;
  Framework::SPtr framework = std::make_shared<MockFramework>(
          std::make_shared<optnet::MockNetworkCommunicator>());
  EXPECT_NO_THROW(initiator = std::make_shared<MockInitiator>(framework));
}  // Constructor_No_Throw

TEST(Initiator, Get_Default_Values)
{
  using namespace optilab;
  using namespace metabb;

  Initiator::SPtr initiator;
  Framework::SPtr framework = std::make_shared<MockFramework>(
          std::make_shared<optnet::MockNetworkCommunicator>());
  ASSERT_NO_THROW(initiator = std::make_shared<MockInitiator>(framework));
  EXPECT_FALSE(initiator->isWarmStarted());
  EXPECT_FALSE(initiator->isSolveAtInit());
  EXPECT_FALSE(initiator->isSolveAtReInit());
  EXPECT_EQ(framework.get(), initiator->getFramework().get());
  EXPECT_EQ(0, initiator->getNumTightenedVarLB());
  EXPECT_EQ(0, initiator->getNumTightenedVarUB());
  EXPECT_DOUBLE_EQ(std::numeric_limits<double>::max(), initiator->getTightenedVarLB(0));
  EXPECT_DOUBLE_EQ(std::numeric_limits<double>::lowest(), initiator->getTightenedVarUB(0));
  EXPECT_FALSE(initiator->areTightenedVarBounds());
  EXPECT_FALSE(initiator->isObjIntegral());
  EXPECT_FALSE(initiator->canGenerateSpecialCutOffValue());
}  // Get_Methods

