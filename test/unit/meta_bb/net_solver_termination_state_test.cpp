#include <cstdint>  // for uint64_t
#include <memory>   // for std::shared_ptr
#include <utility>  // for std::move

#include <gtest/gtest.h>

#include "meta_bb_mocks.hpp"
#include "meta_bb_net_mocks.hpp"
#include "meta_bb/meta_bb_constants.hpp"
#include "meta_bb/meta_bb_struct.hpp"
#include "meta_bb/net_solver_termination_state.hpp"

TEST(NetSolverTerminationState, Send_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const int destination{0};
  NetSolverTerminationState netSolverTerminationState(
          solver::InterruptionPoint::IP_INTERRUPTED_RACING_RAMPUP,
          1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18,
          19, 20, 21, 22, 23, 24, 25, 26, 27, 28);

  auto communicator = std::make_shared<optnet::MockNetworkCommunicator>();
  auto framework = std::make_shared<MockFramework>(communicator);
  netSolverTerminationState.send(framework, destination);

  EXPECT_TRUE(communicator->lastSend);
  EXPECT_EQ(communicator->nodeId, destination);
  EXPECT_EQ(communicator->lastTag,
            metabb::net::MetaBBPacketTag::PacketTagType::PPT_TERMINATED);
}  // Send_No_Throw

TEST(NetSolverTerminationState, Upload_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  // Create a solver termination state
  MockNetSolverTerminationState netSolverTerminationState(
            solver::InterruptionPoint::IP_INTERRUPTED_RACING_RAMPUP,
            1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18,
            19, 20, 21, 22, 23, 24, 25, 26, 27, 28);

  // Create an empty solver termination state
  MockNetSolverTerminationState uploadToState;

  // Verify that its members are different from the solverState to upload
  EXPECT_NE(netSolverTerminationState.getInterruptedMode(), uploadToState.getInterruptedMode());
  EXPECT_NE(netSolverTerminationState.getTotalNumNodesSolved(),
            uploadToState.getTotalNumNodesSolved());
  EXPECT_NE(netSolverTerminationState.getMinNumNodesSolved(), uploadToState.getMinNumNodesSolved());
  EXPECT_NE(netSolverTerminationState.getMaxNumNodesSolved(), uploadToState.getMaxNumNodesSolved());
  EXPECT_NE(netSolverTerminationState.getTotalNumNodesSent(), uploadToState.getTotalNumNodesSent());
  EXPECT_NE(netSolverTerminationState.getTotalNumImprovedIncumbent(),
            uploadToState.getTotalNumImprovedIncumbent());
  EXPECT_NE(netSolverTerminationState.getNumNodesReceived(), uploadToState.getNumNodesReceived());
  EXPECT_NE(netSolverTerminationState.getNumNodesSolved(), uploadToState.getNumNodesSolved());
  EXPECT_NE(netSolverTerminationState.getNumNodesSolvedAtRoot(),
            uploadToState.getNumNodesSolvedAtRoot());
  EXPECT_NE(netSolverTerminationState.getNumNodesSolvedAtPreCheck(),
            uploadToState.getNumNodesSolvedAtPreCheck());
  EXPECT_NE(netSolverTerminationState.getNumTransferredLocalCutsFromSolver(),
            uploadToState.getNumTransferredLocalCutsFromSolver());
  EXPECT_NE(netSolverTerminationState.getMinTransferredLocalCutsFromSolver(),
            uploadToState.getMinTransferredLocalCutsFromSolver());
  EXPECT_NE(netSolverTerminationState.getMaxTransferredLocalCutsFromSolver(),
            uploadToState.getMaxTransferredLocalCutsFromSolver());
  EXPECT_NE(netSolverTerminationState.getNumTotalRestarts(), uploadToState.getNumTotalRestarts());
  EXPECT_NE(netSolverTerminationState.getMinNumRestarts(), uploadToState.getMinNumRestarts());
  EXPECT_NE(netSolverTerminationState.getMaxNumRestarts(), uploadToState.getMaxNumRestarts());
  EXPECT_NE(netSolverTerminationState.getNumVarBoundsTightened(),
            uploadToState.getNumVarBoundsTightened());
  EXPECT_NE(netSolverTerminationState.getNumIntVarBoundsTightened(),
            uploadToState.getNumIntVarBoundsTightened());
  EXPECT_NE(netSolverTerminationState.geTRunnigTimeMsec(), uploadToState.geTRunnigTimeMsec());
  EXPECT_NE(netSolverTerminationState.getIdleTimeToFirstNodeMsec(),
            uploadToState.getIdleTimeToFirstNodeMsec());
  EXPECT_NE(netSolverTerminationState.getIdleTimeBetweenNodesMsec(),
            uploadToState.getIdleTimeBetweenNodesMsec());
  EXPECT_NE(netSolverTerminationState.getIdleTimeAfterLastNodesMsec(),
            uploadToState.getIdleTimeAfterLastNodesMsec());
  EXPECT_NE(netSolverTerminationState.getIdleTimeToWaitForNotificationIdMsec(),
            uploadToState.getIdleTimeToWaitForNotificationIdMsec());
  EXPECT_NE(netSolverTerminationState.getIdleTimeToWaitForAckCompletionMsec(),
            uploadToState.getIdleTimeToWaitForAckCompletionMsec());
  EXPECT_NE(netSolverTerminationState.getIdleTimeToWaitForTokenMsec(),
            uploadToState.getIdleTimeToWaitForTokenMsec());
  EXPECT_NE(netSolverTerminationState.getTotalRootNodeTimeMsec(),
            uploadToState.getTotalRootNodeTimeMsec());
  EXPECT_NE(netSolverTerminationState.getMinRootNodeTimeMsec(),
            uploadToState.getMinRootNodeTimeMsec());
  EXPECT_NE(netSolverTerminationState.getMaxRootNodeTimeMsec(),
            uploadToState.getMaxRootNodeTimeMsec());

  auto packet = netSolverTerminationState.buildNetworkPacket();
  packet->packetTag = optnet::PacketTag::UPtr(new net::MetaBBPacketTag(
      net::MetaBBPacketTag::PacketTagType::PPT_TERMINATED));
  ASSERT_EQ(uploadToState.upload(std::move(packet)), 0);

  // Verify that the states have the same value
  EXPECT_EQ(netSolverTerminationState.getInterruptedMode(), uploadToState.getInterruptedMode());
  EXPECT_EQ(netSolverTerminationState.getTotalNumNodesSolved(),
            uploadToState.getTotalNumNodesSolved());
  EXPECT_EQ(netSolverTerminationState.getMinNumNodesSolved(), uploadToState.getMinNumNodesSolved());
  EXPECT_EQ(netSolverTerminationState.getMaxNumNodesSolved(), uploadToState.getMaxNumNodesSolved());
  EXPECT_EQ(netSolverTerminationState.getTotalNumNodesSent(), uploadToState.getTotalNumNodesSent());
  EXPECT_EQ(netSolverTerminationState.getTotalNumImprovedIncumbent(),
            uploadToState.getTotalNumImprovedIncumbent());
  EXPECT_EQ(netSolverTerminationState.getNumNodesReceived(), uploadToState.getNumNodesReceived());
  EXPECT_EQ(netSolverTerminationState.getNumNodesSolved(), uploadToState.getNumNodesSolved());
  EXPECT_EQ(netSolverTerminationState.getNumNodesSolvedAtRoot(),
            uploadToState.getNumNodesSolvedAtRoot());
  EXPECT_EQ(netSolverTerminationState.getNumNodesSolvedAtPreCheck(),
            uploadToState.getNumNodesSolvedAtPreCheck());
  EXPECT_EQ(netSolverTerminationState.getNumTransferredLocalCutsFromSolver(),
            uploadToState.getNumTransferredLocalCutsFromSolver());
  EXPECT_EQ(netSolverTerminationState.getMinTransferredLocalCutsFromSolver(),
            uploadToState.getMinTransferredLocalCutsFromSolver());
  EXPECT_EQ(netSolverTerminationState.getMaxTransferredLocalCutsFromSolver(),
            uploadToState.getMaxTransferredLocalCutsFromSolver());
  EXPECT_EQ(netSolverTerminationState.getNumTotalRestarts(), uploadToState.getNumTotalRestarts());
  EXPECT_EQ(netSolverTerminationState.getMinNumRestarts(), uploadToState.getMinNumRestarts());
  EXPECT_EQ(netSolverTerminationState.getMaxNumRestarts(), uploadToState.getMaxNumRestarts());
  EXPECT_EQ(netSolverTerminationState.getNumVarBoundsTightened(),
            uploadToState.getNumVarBoundsTightened());
  EXPECT_EQ(netSolverTerminationState.getNumIntVarBoundsTightened(),
            uploadToState.getNumIntVarBoundsTightened());
  EXPECT_EQ(netSolverTerminationState.geTRunnigTimeMsec(), uploadToState.geTRunnigTimeMsec());
  EXPECT_EQ(netSolverTerminationState.getIdleTimeToFirstNodeMsec(),
            uploadToState.getIdleTimeToFirstNodeMsec());
  EXPECT_EQ(netSolverTerminationState.getIdleTimeBetweenNodesMsec(),
            uploadToState.getIdleTimeBetweenNodesMsec());
  EXPECT_EQ(netSolverTerminationState.getIdleTimeAfterLastNodesMsec(),
            uploadToState.getIdleTimeAfterLastNodesMsec());
  EXPECT_EQ(netSolverTerminationState.getIdleTimeToWaitForNotificationIdMsec(),
            uploadToState.getIdleTimeToWaitForNotificationIdMsec());
  EXPECT_EQ(netSolverTerminationState.getIdleTimeToWaitForAckCompletionMsec(),
            uploadToState.getIdleTimeToWaitForAckCompletionMsec());
  EXPECT_EQ(netSolverTerminationState.getIdleTimeToWaitForTokenMsec(),
            uploadToState.getIdleTimeToWaitForTokenMsec());
  EXPECT_EQ(netSolverTerminationState.getTotalRootNodeTimeMsec(),
            uploadToState.getTotalRootNodeTimeMsec());
  EXPECT_EQ(netSolverTerminationState.getMinRootNodeTimeMsec(),
            uploadToState.getMinRootNodeTimeMsec());
  EXPECT_EQ(netSolverTerminationState.getMaxRootNodeTimeMsec(),
            uploadToState.getMaxRootNodeTimeMsec());
}  // Upload_Check_No_Throw
