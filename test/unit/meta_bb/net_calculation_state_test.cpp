#include <cstddef>  // for std::size_t
#include <cstdint>  // for uint64_t
#include <memory>   // for std::shared_ptr
#include <utility>  // for std::move

#include <gtest/gtest.h>

#include "meta_bb_mocks.hpp"
#include "meta_bb_net_mocks.hpp"
#include "meta_bb/calculation_state.hpp"
#include "meta_bb/meta_bb_constants.hpp"
#include "meta_bb/meta_bb_struct.hpp"
#include "meta_bb/net_calculation_state.hpp"

namespace {
constexpr uint64_t kCompTimeMsec = 10;
constexpr uint64_t kRootTimeMsec = 100;
constexpr int kNumNodesSolvedCState = 1000;
constexpr int kNumNodesSent = 2000;
constexpr int kNumImprovedIncumbent = 3000;
constexpr optilab::metabb::solver::TerminationState kTermState =
        optilab::metabb::solver::TerminationState::TS_TERMINATED_BY_TIME_LIMIT;
constexpr int kNumNodesSolverWithoutPreprocess = 4000;
constexpr std::size_t kNumSimplexIterRoot = 1;
double kAvgSimplexIter = 10.5;
std::size_t kNumTransferredLocalCuts = 2;
std::size_t kMinTransferredLocalCuts = 3;
std::size_t kMaxTransferredLocalCuts = 4;
std::size_t kNumRestarts = 5;
double kMinInfeasibilitySum = 1.2;
double kMaxInfeasibilitySum = 2.3;
std::size_t kMinInfeasibilityNum = 6;
std::size_t kMaxInfeasibilityNum = 7;
double kDualBound = 100.123;
}  // namespace

class NetCalculationStateFixture : public testing::Test {
 protected:
  void SetUp() override
  {
    using namespace optilab;
    using namespace metabb;
    communicator = std::make_shared<optnet::MockNetworkCommunicator>();
    framework = std::make_shared<MockFramework>(communicator);
  }

  optilab::metabb::MockNetCalculationState::SPtr buildCalcStateDefault(
          optilab::metabb::solver::TerminationState termState)
  {
    using namespace optilab;
    using namespace metabb;
    return std::make_shared<MockNetCalculationState>(termState);
  }

  optilab::metabb::MockNetCalculationState::SPtr buildCalcState()
  {
    using namespace optilab;
    using namespace metabb;
    return std::make_shared<MockNetCalculationState>(
            kCompTimeMsec,
            kRootTimeMsec,
            kNumNodesSolvedCState,
            kNumNodesSent,
            kNumImprovedIncumbent,
            kTermState,
            kNumNodesSolverWithoutPreprocess,
            kNumSimplexIterRoot,
            kAvgSimplexIter,
            kNumTransferredLocalCuts,
            kMinTransferredLocalCuts,
            kMaxTransferredLocalCuts,
            kNumRestarts,
            kMinInfeasibilitySum,
            kMaxInfeasibilitySum,
            kMinInfeasibilityNum,
            kMaxInfeasibilityNum,
            kDualBound);
  }

  optilab::optnet::MockNetworkCommunicator::SPtr getComm() const { return communicator; }
  optilab::metabb::MockFramework::SPtr getFramework() const { return framework; }

 private:
  optilab::optnet::MockNetworkCommunicator::SPtr communicator;
  optilab::metabb::MockFramework::SPtr framework;
};

TEST(NetCalculationState, Default_Constructor_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  NetCalculationState::SPtr calcState;
  ASSERT_NO_THROW(calcState = std::make_shared<NetCalculationState>());
  ASSERT_TRUE(!!calcState);
}  // Default_Constructor_No_Throw

TEST_F(NetCalculationStateFixture, Send_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const int destination{0};
  auto calcState = buildCalcState();
  calcState->send(getFramework(), destination);
  EXPECT_TRUE(getComm()->lastSend);
  EXPECT_EQ(getComm()->nodeId, destination);
  EXPECT_EQ(getComm()->lastTag,
            metabb::net::MetaBBPacketTag::PacketTagType::PPT_COMPLETION_OF_CALCULATION);
}  // Send_Check_No_Throw

TEST_F(NetCalculationStateFixture, Upload_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  // Create a calculation state
  auto calcState = buildCalcState();

  // Create an empty calculation state
  auto uploadToState = std::make_shared<MockNetCalculationState>(
          solver::TerminationState::TS_TERMINATED_NORMALLY);

  // Verify that its members are different from the calcState to upload
  EXPECT_NE(calcState->getCompTimeMsec(), uploadToState->getCompTimeMsec());
  EXPECT_NE(calcState->getRootTimeMsec(), uploadToState->getRootTimeMsec());
  EXPECT_NE(calcState->getNumRestarts(), uploadToState->getNumRestarts());
  EXPECT_NE(calcState->getNumNodesSolved(), uploadToState->getNumNodesSolved());
  EXPECT_NE(calcState->getNumNodesSent(), uploadToState->getNumNodesSent());
  EXPECT_NE(calcState->getNumImprovedIncumbent(), uploadToState->getNumImprovedIncumbent());
  EXPECT_NE(calcState->getTerminationState(), uploadToState->getTerminationState());
  EXPECT_NE(calcState->getNumNodesSolvedWithNoPreprocess(),
            uploadToState->getNumNodesSolvedWithNoPreprocess());
  EXPECT_NE(calcState->getDualBoundValue(), uploadToState->getDualBoundValue());

  // Upload the calculation state
  auto packet = calcState->buildNetworkPacket();
  packet->packetTag = optnet::PacketTag::UPtr(new net::MetaBBPacketTag(
          net::MetaBBPacketTag::PacketTagType::PPT_COMPLETION_OF_CALCULATION));
  ASSERT_EQ(uploadToState->upload(std::move(packet)), 0);

  // Verify that the states have the same value
  EXPECT_EQ(calcState->getCompTimeMsec(), uploadToState->getCompTimeMsec());
  EXPECT_EQ(calcState->getRootTimeMsec(), uploadToState->getRootTimeMsec());
  EXPECT_EQ(calcState->getNumRestarts(), uploadToState->getNumRestarts());
  EXPECT_EQ(calcState->getNumNodesSolved(), uploadToState->getNumNodesSolved());
  EXPECT_EQ(calcState->getNumNodesSent(), uploadToState->getNumNodesSent());
  EXPECT_EQ(calcState->getNumImprovedIncumbent(), uploadToState->getNumImprovedIncumbent());
  EXPECT_EQ(calcState->getTerminationState(), uploadToState->getTerminationState());
  EXPECT_EQ(calcState->getNumNodesSolvedWithNoPreprocess(),
            uploadToState->getNumNodesSolvedWithNoPreprocess());
  EXPECT_DOUBLE_EQ(calcState->getDualBoundValue(), uploadToState->getDualBoundValue());
}  // Upload_Check_No_Throw
