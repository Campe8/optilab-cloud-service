#include <memory>     // for std::shared_ptr

#include <gtest/gtest.h>

#include "meta_bb_mocks.hpp"
#include "meta_bb/diff_subproblem.hpp"
#include "meta_bb/node.hpp"

namespace {
constexpr int kLMId = 10;
constexpr int kGlbSubtreeId = 11;
constexpr int kSolverId = 12;
constexpr long long kSeqIdNum = 1000;
constexpr int kNodeDepth = 20;
constexpr double kNodeDualBound = -1.0;
constexpr double kNodeOrigDualBound = 1.0;
constexpr double kNodeEstimatedValue = -10.0;
}  // namespace

class MetaBBNodeFixture : public testing::Test {
 protected:
  void SetUp() override
  {
    using namespace optilab;
    using namespace metabb;
    stid = std::make_shared<SubtreeId>(kLMId, kGlbSubtreeId, kSolverId);
    nGenId = std::make_shared<NodeId>(kSeqIdNum, *(stid.get()));
    nId = std::make_shared<NodeId>(kSeqIdNum+1, *(stid.get()));
    diffSubproblem = std::make_shared<MockDiffSubproblem>();
  }

  optilab::metabb::MockNode::SPtr buildNode(optilab::metabb::DiffSubproblem::SPtr diffSubproblem,
                                            std::shared_ptr<optilab::metabb::NodeId> nodeId=nullptr,
                                            double dualBound=kNodeDualBound)
  {
    using namespace optilab;
    using namespace metabb;

    optilab::metabb::MockNode::SPtr node;
    if (!nodeId)
    {
      node = std::make_shared<MockNode>(*(nId.get()), *(nGenId.get()), kNodeDepth,
                                        dualBound, kNodeOrigDualBound, kNodeEstimatedValue,
                                        diffSubproblem);
    }
    else
    {
      node = std::make_shared<MockNode>(*(nodeId.get()), *(nGenId.get()), kNodeDepth,
                                        dualBound, kNodeOrigDualBound, kNodeEstimatedValue,
                                        diffSubproblem);
    }

    return node;
  }

  optilab::metabb::SubtreeId& getSubtreeId() { return *stid; }
  optilab::metabb::NodeId& getGenNodeId() { return *nGenId; }
  optilab::metabb::NodeId& getNodeId() { return *nId; }

 private:
  std::shared_ptr<optilab::metabb::SubtreeId> stid;
  std::shared_ptr<optilab::metabb::NodeId> nGenId;
  std::shared_ptr<optilab::metabb::NodeId> nId;
  optilab::metabb::DiffSubproblem::SPtr diffSubproblem;
};

TEST(MetaBBNode, Default_Constructor_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  MockNode::SPtr node;
  ASSERT_NO_THROW(node = std::make_shared<MockNode>());
  ASSERT_TRUE(!!node);
}  // Default_Constructor_No_Throw

TEST_F(MetaBBNodeFixture,  Constructor_No_DiffSubproblem_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  DiffSubproblem::SPtr diffSubproblem;
  MockNode::SPtr node = buildNode(diffSubproblem);
  ASSERT_TRUE(!!node);

  // Check get/set methods
  EXPECT_TRUE(node->isSameNodeId(*node));
  EXPECT_TRUE(node->isSameParentNodeId(*node));
  EXPECT_TRUE(node->isSameParentNodeSubtreeId(getNodeId()));
  EXPECT_TRUE(node->isSameSubtreeId(*node));
  EXPECT_TRUE(node->isSameLMId(*node));
  EXPECT_TRUE(node->isSameLMId(kLMId));
  EXPECT_TRUE(node->isSameGlobalSubtreeIdInLM(*node));
  EXPECT_TRUE(node->isSameGlobalSubtreeIdInLMAs(kGlbSubtreeId));
  EXPECT_EQ(kLMId, node->getLMId());
  EXPECT_EQ(kGlbSubtreeId, node->getGlobalSubtreeIdInLM());
  EXPECT_EQ(kSolverId, node->getSolverId());
  EXPECT_TRUE(getNodeId() == node->getNodeId());
  EXPECT_EQ(kNodeDepth, node->getDepth());
  EXPECT_DOUBLE_EQ(kNodeDualBound, node->getDualBoundValue());
  EXPECT_DOUBLE_EQ(kNodeOrigDualBound, node->getInitialDualBoundValue());
  EXPECT_DOUBLE_EQ(kNodeEstimatedValue, node->getEstimatedValue());
  EXPECT_FALSE(node->getDiffSubproblem());
  EXPECT_FALSE(node->getParent());
  EXPECT_FALSE(node->hasChildren());
  EXPECT_EQ(Node::NodeMergingStatus::NMS_NO_MERGING_NODE, node->getMergingStatus());
  EXPECT_FALSE(node->areNodesCollected());
}  // Constructor_No_DiffSubproblem_No_Throw

TEST_F(MetaBBNodeFixture, Node_Setters)
{
  using namespace optilab;
  using namespace metabb;

  DiffSubproblem::SPtr diffSubproblem;
  MockNode::SPtr node = buildNode(diffSubproblem);
  ASSERT_TRUE(!!node);

  EXPECT_NO_THROW(node->setGlobalSubtreeId(kLMId+1, kGlbSubtreeId+1));
  EXPECT_FALSE(node->isSameLMId(kLMId));
  EXPECT_FALSE(node->isSameGlobalSubtreeIdInLMAs(kGlbSubtreeId));

  EXPECT_NO_THROW(node->setSolverId(kSolverId+1));
  EXPECT_EQ(kSolverId+1, node->getSolverId());

  NodeId nodeId2(kSeqIdNum+10, getSubtreeId());
  EXPECT_NO_THROW(node->setNodeId(nodeId2));
  EXPECT_TRUE(node->getNodeId() == nodeId2);
  EXPECT_NO_THROW(node->setGeneratorNodeId(nodeId2));
  EXPECT_TRUE(node->getGeneratorNodeId() == nodeId2);

  EXPECT_NO_THROW(node->setDepth(kNodeDepth + 1));
  EXPECT_EQ(kNodeDepth + 1, node->getDepth());

  EXPECT_NO_THROW(node->setDualBoundValue(kNodeDualBound + 1));
  EXPECT_DOUBLE_EQ(kNodeDualBound + 1, node->getDualBoundValue());
  EXPECT_NO_THROW(node->setInitialDualBoundValue(kNodeOrigDualBound + 1));
  EXPECT_DOUBLE_EQ(kNodeOrigDualBound + 1, node->getInitialDualBoundValue());
  EXPECT_NO_THROW(node->setEstimatedValue(kNodeEstimatedValue + 1));
  EXPECT_DOUBLE_EQ(kNodeEstimatedValue + 1, node->getEstimatedValue());

  diffSubproblem = std::make_shared<MockDiffSubproblem>();
  EXPECT_NO_THROW(node->setDiffSubproblem(diffSubproblem));
  EXPECT_EQ(diffSubproblem.get(), node->getDiffSubproblem().get());
}  // Node_Setters

TEST_F(MetaBBNodeFixture, Node_Set_Invalid_Parent_Throw)
{
  using namespace optilab;
  using namespace metabb;

  DiffSubproblem::SPtr diffSubproblem;
  MockNode::SPtr node = buildNode(diffSubproblem);
  ASSERT_TRUE(!!node);

  // An empty parent should not throw
  EXPECT_NO_THROW(node->setParent(nullptr));

  // A parent that is not a local parent should throw
  NodeGenealogicalRel::SPtr parent = std::make_shared<NodeGenealogicalRemoteRel>();
  EXPECT_THROW(node->setParent(parent), std::runtime_error);

  // A parent that is a local parent but without a value should throw
  parent = std::make_shared<NodeGenealogicalLocalRel>();
  EXPECT_THROW(node->setParent(parent), std::runtime_error);
}  // Node_Set_Invalid_Parent_Throw

TEST_F(MetaBBNodeFixture, Node_Set_Valid_Parent_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  DiffSubproblem::SPtr diffSubproblem;
  MockNode::SPtr parentNode = buildNode(diffSubproblem);
  MockNode::SPtr node = buildNode(diffSubproblem);
  ASSERT_TRUE(!!parentNode);
  ASSERT_TRUE(!!node);

  // A parent that is a local parent and with a value should not throw
  NodeId parentNodeId(kSeqIdNum, getSubtreeId());
  NodeGenealogicalRel::SPtr parent = std::make_shared<NodeGenealogicalLocalRel>(
          parentNodeId, parentNode);

  EXPECT_NO_THROW(node->setParent(parent));
}  // Node_Set_Valid_Parent_No_Throw

TEST_F(MetaBBNodeFixture, Node_Genealogical_Relationships_Child)
{
  using namespace optilab;
  using namespace metabb;

  const double childDualBound = kNodeDualBound - 100.0;
  std::shared_ptr<NodeId> childId = std::make_shared<NodeId>(kSeqIdNum+2, getSubtreeId());
  DiffSubproblem::SPtr diffSubproblem;
  MockNode::SPtr node = buildNode(diffSubproblem);
  MockNode::SPtr childNode = buildNode(diffSubproblem, childId, childDualBound);
  ASSERT_TRUE(!!node);
  ASSERT_TRUE(!!childNode);

  NodeGenealogicalRel::SPtr child = std::make_shared<NodeGenealogicalLocalRel>(
          childNode->getNodeId(), childNode);


  EXPECT_FALSE(node->hasChildren());

  // Check that the dual bound is the original dual bound
  EXPECT_DOUBLE_EQ(kNodeDualBound,
                   node->getMinimumDualBoundInDesendants(node->getDualBoundValue()));

  // Add child
  EXPECT_NO_THROW(node->addChild(child));
  EXPECT_TRUE(node->hasChildren());

  // Check that the dual bound is the child dual bound (which is lower
  // than the original dual bound
  EXPECT_DOUBLE_EQ(childDualBound,
                   node->getMinimumDualBoundInDesendants(node->getDualBoundValue()));

  // Remove the child
  EXPECT_NO_THROW(node->removeChild(childNode->getNodeId()));
  EXPECT_FALSE(node->hasChildren());

  // Check that the dual bound is the original dual bound
  EXPECT_DOUBLE_EQ(kNodeDualBound,
                   node->getMinimumDualBoundInDesendants(node->getDualBoundValue()));
}  // Node_Genealogical_Relationships
