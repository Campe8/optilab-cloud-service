#include <future>
#include <stdexcept>  // for std::runtime_error
#include <thread>

#include <gtest/gtest.h>

#include "config/system_network_config.hpp"
#include "meta_bb_mocks.hpp"
#include "meta_bb_net_mocks.hpp"

namespace {

int runFramework(optilab::metabb::MockNetFramework::SPtr framework)
{
  framework->init();
  if (framework->getNetworkSize() != 2)
  {
    return framework->getNetworkSize();
  }
  return 0;
}  // runRootFramework

}  // namespace

TEST(NetFramework, Default_Root_Constructor_Destructor_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool isRoot{true};
  MockNetFramework::SPtr framework;
  ASSERT_NO_THROW(framework = std::make_shared<MockNetFramework>(isRoot));
  ASSERT_TRUE(!!framework);
  ASSERT_NO_THROW(framework.reset());
}  // Default_Root_Constructor_Destructor_No_Throw

TEST(NetFramework, Default_No_Root_Constructor_Destructor_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool isRoot{false};
  MockNetFramework::SPtr framework;
  ASSERT_NO_THROW(framework = std::make_shared<MockNetFramework>(isRoot));
  ASSERT_TRUE(!!framework);
  ASSERT_NO_THROW(framework.reset());
}  // Default_No_Root_Constructor_Destructor_No_Throw

TEST(NetFramework, TearDown_No_Init_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool isRoot{false};
  MockNetFramework::SPtr framework;
  ASSERT_NO_THROW(framework = std::make_shared<MockNetFramework>(isRoot));
  ASSERT_NO_THROW(framework->tearDown());
}  // TearDown_No_Init_No_Throw

TEST(NetFramework, Abort_No_Init_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool isRoot{false};
  MockNetFramework::SPtr framework;
  ASSERT_NO_THROW(framework = std::make_shared<MockNetFramework>(isRoot));
  ASSERT_THROW(framework->abort(), std::runtime_error);
}  // TearDown_No_Init_No_Throw

TEST(NetFramework, Root_Init_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  // Set a reduced waiting time
  auto& config = SystemNetworkConfig::getInstance().getMetaBBNetworkConfig();
  config.connectSynchNetworkWaitTimeMsec = 500;

  const bool isRoot{true};
  MockNetFramework::SPtr framework;
  ASSERT_NO_THROW(framework = std::make_shared<MockNetFramework>(isRoot));
  ASSERT_TRUE(!!framework);
  ASSERT_NO_THROW(framework->init());

  EXPECT_TRUE(framework->isRootFramework());

  // Check the rank and network size
  EXPECT_EQ(framework->getRank(), 0) << "Root node should have rank of zero";
  EXPECT_EQ(framework->getNetworkSize(), 1) << "There should be only one node in the network";

  EXPECT_NO_THROW(framework->abort());
  ASSERT_NO_THROW(framework.reset());
}  // Root_Init_No_Throw

TEST(NetFramework, No_Root_Init_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  // Set a reduced waiting time
  auto& config = SystemNetworkConfig::getInstance().getMetaBBNetworkConfig();
  config.connectTimeoutMsec = 500;

  const bool isRoot{false};
  MockNetFramework::SPtr framework;
  ASSERT_NO_THROW(framework = std::make_shared<MockNetFramework>(isRoot));
  ASSERT_TRUE(!!framework);
  ASSERT_NO_THROW(framework->init());

  // Check the rank and network size
  EXPECT_NE(framework->getRank(), 0) << "Non-root node should have rank different from zero";
  EXPECT_EQ(framework->getNetworkSize(), 0) << "Connection doesn't happen "
          "and network size should be zero";

  EXPECT_NO_THROW(framework->abort());
  ASSERT_NO_THROW(framework.reset());
}  // No_Root_Init_No_Throw

TEST(NetFramework, Network_Framework_Setup_No_Throw)
{
  using namespace optilab;
  using namespace metabb;

  const bool isRoot{true};
  MockNetFramework::SPtr frameworkRoot;
  MockNetFramework::SPtr frameworkSolver;
  ASSERT_NO_THROW(frameworkRoot = std::make_shared<MockNetFramework>(isRoot));
  ASSERT_NO_THROW(frameworkSolver = std::make_shared<MockNetFramework>(!isRoot));

  std::future<int> runRoot = std::async(std::launch::async, runFramework, frameworkRoot);
  std::future<int> runSolver = std::async(std::launch::async, runFramework, frameworkSolver);

  EXPECT_EQ(runRoot.get(), 0);
  EXPECT_EQ(runSolver.get(), 0);

  EXPECT_NO_THROW(frameworkRoot->abort());
  EXPECT_NO_THROW(frameworkRoot->tearDown());
  EXPECT_NO_THROW(frameworkSolver->tearDown());
  ASSERT_NO_THROW(frameworkRoot.reset());
  ASSERT_NO_THROW(frameworkSolver.reset());
}  // Network_Framework_Setup_No_Throw
