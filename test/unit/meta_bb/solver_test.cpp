#include <cstdint>    // for uint64_t
#include <limits>     // for std::numeric_limits
#include <memory>     // for std::shared_ptr
#include <stdexcept>  // for std::invalid_argument
#include <vector>

#include <gtest/gtest.h>

#include "meta_bb_mocks.hpp"
#include "meta_bb/meta_bb_constants.hpp"
#include "meta_bb/solver.hpp"
#include "optimizer_network/network_communicator.hpp"

TEST(Solver, Constructor_Empty_Framwork_Throws)
{
  using namespace optilab;
  using namespace metabb;

  auto paramSet = std::make_shared<ParamSet>();
  auto instance = std::make_shared<MockInstance>();

  Solver::SPtr solver;
  ASSERT_THROW(solver = std::make_shared<MockSolver>(nullptr, paramSet, instance),
               std::invalid_argument);
}  // Constructor_Empty_Framwork_Throws

TEST(Solver, Constructor_Empty_ParamSet_Throws)
{
  using namespace optilab;
  using namespace metabb;

  auto comm = std::make_shared<optnet::MockNetworkCommunicator>();
  auto framework = std::make_shared<MockFramework>(comm);
  auto instance = std::make_shared<MockInstance>();

  Solver::SPtr solver;
  ASSERT_THROW(solver = std::make_shared<MockSolver>(framework, nullptr, instance),
               std::invalid_argument);
}  // Constructor_Empty_ParamSet_Throws

TEST(Solver, Constructor_Empty_Instance_Throws)
{
  using namespace optilab;
  using namespace metabb;

  auto comm = std::make_shared<optnet::MockNetworkCommunicator>();
  auto framework = std::make_shared<MockFramework>(comm);
  auto paramSet = std::make_shared<ParamSet>();

  Solver::SPtr solver;
  ASSERT_THROW(solver = std::make_shared<MockSolver>(framework, paramSet, nullptr),
               std::invalid_argument);
}  // Constructor_Empty_Instance_Throws

TEST(Solver, Constructor_Destructor_No_Throws)
{
  using namespace optilab;
  using namespace metabb;

  auto comm = std::make_shared<optnet::MockNetworkCommunicator>();
  auto framework = std::make_shared<MockFramework>(comm);
  auto paramSet = std::make_shared<ParamSet>();
  auto instance = std::make_shared<MockInstance>();

  Solver::SPtr solver;
  ASSERT_NO_THROW(solver = std::make_shared<MockSolver>(framework, paramSet, instance));
  ASSERT_NO_THROW(solver.reset());
}  // Constructor_Invalid_Network_Communicator_Throws

TEST(Solver, Constructor_And_Default_Get_Methods)
{
  using namespace optilab;
  using namespace metabb;

  auto comm = std::make_shared<optnet::MockNetworkCommunicator>();
  auto framework = std::make_shared<MockFramework>(comm);
  auto paramSet = std::make_shared<ParamSet>();
  auto instance = std::make_shared<MockInstance>();

  Solver::SPtr solver;
  ASSERT_NO_THROW(solver = std::make_shared<MockSolver>(framework, paramSet, instance));

  EXPECT_EQ(framework.get(), solver->getFramework().get());
  EXPECT_FALSE(solver->isWarmStarted());
  EXPECT_DOUBLE_EQ(std::numeric_limits<double>::lowest(),
                   solver->getGlobalBestDualBoundValueAtWarmStart());
  EXPECT_DOUBLE_EQ(std::numeric_limits<double>::lowest(), solver->getLMBestDualBoundValue());
  EXPECT_EQ(paramSet->getDescriptor().numstopsolvingmode(), solver->getNumStopSolvingMode());
  EXPECT_EQ(static_cast<uint64_t>(paramSet->getDescriptor().timestopsolvingmodemsec()),
            solver->getTimeStopSolvingModeMsec());
  EXPECT_EQ(0, solver->getRootNodeTimeMsec());
  EXPECT_DOUBLE_EQ(paramSet->getDescriptor().bgapstopsolvingmode(),
                   solver->getBoundGapForStopSolving());
  EXPECT_DOUBLE_EQ(paramSet->getDescriptor().bgapcollectingmode(),
                   solver->getBoundGapForCollectingMode());
  EXPECT_FALSE(solver->isGlobalIncumbentUpdated());
  EXPECT_FALSE(solver->isRampUp());
  EXPECT_FALSE(solver->isRacingInterruptRequested());
  EXPECT_TRUE(solver->isRacingRampUp()) << "Default should use racing ramp-up";
  EXPECT_FALSE(solver->isRacingWinner());
  EXPECT_FALSE(solver->isCollectingInterrupt());
  EXPECT_FALSE(solver->isInterrupting());
  EXPECT_FALSE(solver->isTerminationRequested());
  EXPECT_FALSE(solver->newNodeExists());
  EXPECT_FALSE(solver->isInCollectingMode());
  EXPECT_FALSE(solver->isAggressiveCollecting());
  EXPECT_FALSE(solver->isManyNodesCollectionRequested());
  EXPECT_FALSE(solver->getNotificaionProcessed());
  EXPECT_DOUBLE_EQ(std::numeric_limits<double>::max(), solver->getGlobalBestIncumbentValue());
  EXPECT_FALSE(solver->getCurrentNode());
  EXPECT_EQ(instance.get(), solver->getInstance().get());
  EXPECT_EQ(paramSet.get(), solver->getParamSet().get());
  EXPECT_EQ(comm->getRank(), solver->getRank());
  EXPECT_EQ(0, solver->getNumNodesSendInCollectingMode());
  EXPECT_FALSE(solver->isRacingStage());
  EXPECT_FALSE(solver->getGlobalBestIncumbentSolution());
  EXPECT_FALSE(solver->isWaitingForSpecificMessage());
  EXPECT_FALSE(solver->isBreaking());
  EXPECT_DOUBLE_EQ(std::numeric_limits<double>::lowest(),
                   solver->getTargetBound());
  EXPECT_TRUE(solver->isTransferLimitReached());
  EXPECT_FALSE(solver->isOnceBreak());
  EXPECT_FALSE(solver->isAggressivePresolvingSpecified());
  EXPECT_EQ(paramSet->getDescriptor().aggressivepresolvedepth(),
            solver->getAggresivePresolvingDepth());
  EXPECT_EQ(paramSet->getDescriptor().aggressivepresolvestopdepth(),
            solver->getAggresivePresolvingStopDepth());
  EXPECT_EQ(-1, solver->getSubMIPDepth());
  EXPECT_FALSE(solver->isCollectingAllNodes());
  EXPECT_EQ(0, solver->getBigDualGapSubtreeHandlingStrategy());
  EXPECT_FALSE(solver->isGivenGapReached());
  EXPECT_FALSE(solver->isIterativeBreakDownApplied());
  EXPECT_EQ(Node::NodeMergingStatus::NMS_NO_MERGING_NODE,
            solver->getCurrentSolvingNodeMergingStatus());
  EXPECT_DOUBLE_EQ(std::numeric_limits<double>::max(),
                   solver->getCurrentSolvingNodeInitialDualBound());
  EXPECT_TRUE(!!(solver->getTimer()));
  EXPECT_DOUBLE_EQ(0.0, solver->getAverageDualBoundGain());
  EXPECT_TRUE(solver->isEnoughGainObtained());
  EXPECT_FALSE(solver->isDualBoundGainTestNeeded());
  EXPECT_FALSE(solver->canGenerateSpecialCutOffValue());
  EXPECT_DOUBLE_EQ(std::numeric_limits<double>::max(), solver->getCutOffValue());
  EXPECT_EQ(solver::TerminationMode::TM_NO_TERMINATION, solver->getTerminationMode());
  EXPECT_FALSE(solver->isAnotherNodeIsRequested());
}  // Constructor_And_Default_Get_Methods
