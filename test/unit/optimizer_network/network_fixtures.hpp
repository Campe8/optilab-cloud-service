#include <memory>     // for std::shared_ptr
#include <stdexcept>  // for std::invalid_argument
#include <string>
#include <utility>    // for std::move
#include <vector>

#include <gtest/gtest.h>
#include <spdlog/spdlog.h>
#include <zeromq/zhelpers.hpp>

#include "optimizer_network/network_communicator.hpp"
#include "optimizer_network/network_constants.hpp"
#include "optimizer_network/network_utilities.hpp"
#include "optimizer_network/socket_connector.hpp"

namespace {
const char kTCPNetworkPrefix[] = "tcp://";
}  // namespace

class NetworkCommFixture : public testing::Test {
 protected:
  void SetUp() override
  {
  }

  /// Connects the sockets to the given (localhost) ports.
  /// @note "connectionTimeoutMsec" is the timeout in msec. for connection.
  /// A value less than or equal to zero means no connection timeout.
  /// @note "numConns" is the number of 4-uples of connections to create
  void connectSockets(const int ptpSendPort, const int ptpRecvPort, const int bcastSendPort,
                      const int bcastRecvPort, const int connectionTimeoutMsec=0,
                      const int numConns=1)
  {
    using namespace optilab;
    using namespace optnet;

    for (int conn = 0; conn < numConns; ++conn)
    {
      Conns connStruct;
      connStruct.pContext.reset(
          new zmq::context_t(optilab::optnet::networkconst::CONTEXT_DEFAULT_NUM_THREADS));
      connStruct.pSendSocket = utils::buildPointToPointSocket(*connStruct.pContext);
      connStruct.pRecvSocket = utils::buildPointToPointSocket(*connStruct.pContext);
      connStruct.pBroadcastSendSocket = utils::buildSenderBroadcastSocket(*connStruct.pContext);
      connStruct.pBroadcastRecvSocket = utils::buildReceivingBroadcastSocket(*connStruct.pContext);

      // Set connection timeout if specified
      if (connectionTimeoutMsec > 0)
      {
        int timeout1 = connectionTimeoutMsec;
        int timeout2 = connectionTimeoutMsec;
        int timeout3 = connectionTimeoutMsec;
        int timeout4 = connectionTimeoutMsec;
        connStruct.pSendSocket->setsockopt(ZMQ_CONNECT_TIMEOUT, &timeout1, sizeof(timeout1));
        connStruct.pRecvSocket->setsockopt(ZMQ_CONNECT_TIMEOUT, &timeout2, sizeof(timeout2));
        connStruct.pBroadcastSendSocket->setsockopt(
            ZMQ_CONNECT_TIMEOUT, &timeout3, sizeof(timeout3));
        connStruct.pBroadcastRecvSocket->setsockopt(
            ZMQ_CONNECT_TIMEOUT, &timeout4, sizeof(timeout4));
      }

      spdlog::info(std::string("[UNIT_TEST] - connectSockets num. ") + std::to_string(conn));
      try {
        // Bind the hub to the input address
        spdlog::info(std::string("[UNIT_TEST] - "
            "PToP send connect to input address ") + getSocketAddr(ptpSendPort).c_str());
        connStruct.pSendSocket->connect(getSocketAddr(ptpSendPort).c_str());

        // Bind the hub to the output address
        spdlog::info(std::string("[UNIT_TEST] - "
            "PToP recv connect to input address ") + getSocketAddr(ptpRecvPort).c_str());
        connStruct.pRecvSocket->connect(getSocketAddr(ptpRecvPort).c_str());

        // Bind the hub to the input broadcast address
        spdlog::info(std::string("[UNIT_TEST] - "
            "Bcast send connect to input address ") + getSocketAddr(bcastSendPort).c_str());
        connStruct.pBroadcastSendSocket->connect(getSocketAddr(bcastSendPort).c_str());

        // Bind the hub to the output broadcast address
        spdlog::info(std::string("[UNIT_TEST] - "
            "Bcast recv connect to input address ") + getSocketAddr(bcastRecvPort).c_str());
        connStruct.pBroadcastRecvSocket->connect(getSocketAddr(bcastRecvPort).c_str());

        // Subscribe to all messages
        connStruct.pBroadcastRecvSocket->setsockopt(ZMQ_SUBSCRIBE, "", 0);
      }
      catch(...)
      {
        spdlog::warn("[UNIT_TEST] - connectSockets: cannot connect sockets");
      }

      // Create a new socket connector to start listening for incoming messages
      connStruct.pPToPSendConn.reset(new SocketConnector(connStruct.pSendSocket));
      connStruct.pPToPRecvConn.reset(new SocketConnector(connStruct.pRecvSocket));
      connStruct.pBcastSendConn.reset(
          new BroadcastSocketConnector(connStruct.pBroadcastSendSocket));
      connStruct.pBcastRecvConn.reset(
          new BroadcastSocketConnector(connStruct.pBroadcastRecvSocket));

      pConnectionsList.push_back(std::move(connStruct));
    }
  }

  inline std::unique_ptr<zmq::context_t> getContext(int idx=0)
  {
    return std::move(pConnectionsList.at(idx).pContext);
  }

  inline optilab::optnet::BaseSocketConnector::UPtr getPToPSendConn(int idx=0)
  {
    return std::move(pConnectionsList.at(idx).pPToPSendConn);
  }

  inline optilab::optnet::BaseSocketConnector::UPtr getPToPRecvConn(int idx=0)
  {
    return std::move(pConnectionsList.at(idx).pPToPRecvConn);
  }

  inline optilab::optnet::BaseSocketConnector::UPtr getBcastSendConn(int idx=0)
  {
    return std::move(pConnectionsList.at(idx).pBcastSendConn);
  }

  inline optilab::optnet::BaseSocketConnector::UPtr getBcastRecvConn(int idx=0)
  {
    return std::move(pConnectionsList.at(idx).pBcastRecvConn);
  }

  void TearDown() override
  {
  }

 private:
  struct Conns {
    optilab::optnet::SocketConnector::SocketSPtr pSendSocket;
    optilab::optnet::SocketConnector::SocketSPtr pRecvSocket;
    optilab::optnet::SocketConnector::SocketSPtr pBroadcastSendSocket;
    optilab::optnet::SocketConnector::SocketSPtr pBroadcastRecvSocket;

    optilab::optnet::SocketConnector::UPtr pPToPSendConn;
    optilab::optnet::SocketConnector::UPtr pPToPRecvConn;
    optilab::optnet::BroadcastSocketConnector::UPtr pBcastSendConn;
    optilab::optnet::BroadcastSocketConnector::UPtr pBcastRecvConn;

    std::unique_ptr<zmq::context_t> pContext;
  };

  /// List of connections
  std::vector<Conns> pConnectionsList;

  std::string getSocketAddr(const int socketPort)
  {
    std::string addr(kTCPNetworkPrefix);
    addr += "localhost:";
    addr += std::to_string(socketPort);
    return addr;
  }
};
