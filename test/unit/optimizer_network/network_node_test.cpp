#include <chrono>
#include <memory>     // for std::shared_ptr
#include <stdexcept>  // for std::invalid_argument
#include <string>
#include <thread>
#include <utility>    // for std::move

#include <gtest/gtest.h>

#include "optimizer_network/network_constants.hpp"
#include "optimizer_network/network_hub.hpp"
#include "optimizer_network/network_node.hpp"
#include "optimizer_network/network_packet.hpp"
#include "optimizer_network/packet.hpp"

namespace {
constexpr int kPToPSendPort = 11000;
constexpr int kPToPRecvPort = 11001;
constexpr int kBcastSendPort = 11002;
constexpr int kBcastRecvPort = 11003;
}  // namespace

TEST(NetworkNode, Constructor_Valid_PortNumber_NoThrow) {
  using namespace optilab;
  using namespace optnet;

  NetworkNode::SPtr node;
  ASSERT_NO_THROW(node = std::make_shared<NetworkNode>(kPToPSendPort, kPToPRecvPort, kBcastSendPort,
                                                       kBcastRecvPort));
  EXPECT_FALSE(node->getNetworkCommunicator());
}  // Constructor_Valid_PortNumber_NoThrow

TEST(NetworkNode, GetRank_NoInit_Throws) {
  using namespace optilab;
  using namespace optnet;

  NetworkNode::SPtr node;
  ASSERT_NO_THROW(node = std::make_shared<NetworkNode>(kPToPSendPort, kPToPRecvPort, kBcastSendPort,
                                                       kBcastRecvPort));
  EXPECT_THROW(node->getRank(), std::runtime_error);
}  // GetRank_NoInit_Throws

TEST(NetworkNode, GetNetworkSize_NoInit_Throws) {
  using namespace optilab;
  using namespace optnet;

  NetworkNode::SPtr node;
  ASSERT_NO_THROW(node = std::make_shared<NetworkNode>(kPToPSendPort, kPToPRecvPort, kBcastSendPort,
                                                       kBcastRecvPort));
  EXPECT_THROW(node->getNetworkSize(), std::runtime_error);
}  // GetNetworkSize_NoInit_Throws

TEST(NetworkNode, Broadcast_NoInit_Throws) {
  using namespace optilab;
  using namespace optnet;

  NetworkNode::SPtr node;
  ASSERT_NO_THROW(node = std::make_shared<NetworkNode>(kPToPSendPort, kPToPRecvPort, kBcastSendPort,
                                                       kBcastRecvPort));

  Packet::UPtr packet;
  EXPECT_THROW(node->broadcast(packet, 0), std::runtime_error);
}  // Broadcast_NoInit_Throws

TEST(NetworkNode, Send_NoInit_Throws) {
  using namespace optilab;
  using namespace optnet;

  NetworkNode::SPtr node;
  ASSERT_NO_THROW(node = std::make_shared<NetworkNode>(kPToPSendPort, kPToPRecvPort, kBcastSendPort,
                                                       kBcastRecvPort));

  Packet::UPtr packet;
  EXPECT_THROW(node->send(std::move(packet), 0), std::runtime_error);
}  // Send_NoInit_Throws

TEST(NetworkNode, Receive_NoInit_Throws) {
  using namespace optilab;
  using namespace optnet;

  NetworkNode::SPtr node;
  ASSERT_NO_THROW(node = std::make_shared<NetworkNode>(kPToPSendPort, kPToPRecvPort, kBcastSendPort,
                                                       kBcastRecvPort));

  EXPECT_THROW(node->receive(0), std::runtime_error);
}  // Receive_NoInit_Throws

TEST(NetworkNode, Constructor_Invalid_PortNumber_Throws) {
  using namespace optilab;
  using namespace optnet;

  EXPECT_THROW(NetworkNode(networkconst::MIN_NETWORK_PORT_NUMBER - 1, kPToPRecvPort, kBcastSendPort,
                           kBcastRecvPort), std::invalid_argument);
  EXPECT_THROW(NetworkNode(networkconst::MAX_NETWORK_PORT_NUMBER + 1, kPToPRecvPort, kBcastSendPort,
                           kBcastRecvPort), std::invalid_argument);

  EXPECT_THROW(NetworkNode(kPToPSendPort, networkconst::MIN_NETWORK_PORT_NUMBER - 1, kBcastSendPort,
                           kBcastRecvPort), std::invalid_argument);
  EXPECT_THROW(NetworkNode(kPToPSendPort, networkconst::MAX_NETWORK_PORT_NUMBER + 1, kBcastSendPort,
                           kBcastRecvPort), std::invalid_argument);

  EXPECT_THROW(NetworkNode(kPToPSendPort, kPToPRecvPort, networkconst::MIN_NETWORK_PORT_NUMBER - 1,
                           kBcastRecvPort), std::invalid_argument);
  EXPECT_THROW(NetworkNode(kPToPSendPort, kPToPRecvPort,  networkconst::MAX_NETWORK_PORT_NUMBER + 1,
                           kBcastRecvPort), std::invalid_argument);

  EXPECT_THROW(NetworkNode(kPToPSendPort, kPToPRecvPort, kBcastSendPort,
                           networkconst::MIN_NETWORK_PORT_NUMBER - 1), std::invalid_argument);
  EXPECT_THROW(NetworkNode(kPToPSendPort, kPToPRecvPort, kBcastSendPort,
                           networkconst::MAX_NETWORK_PORT_NUMBER + 1), std::invalid_argument);
}  // Constructor_Invalid_PortNumber_Throws

TEST(NetworkNode, PTP_Message_No_Throw)
{

  using namespace optilab;
  using namespace optnet;

  // Create and start the hub
  NetworkHub::SPtr hub;
  ASSERT_NO_THROW(hub =
      std::make_shared<NetworkHub>(kPToPSendPort, kPToPRecvPort, kBcastSendPort, kBcastRecvPort));

  // Run the hub
  ASSERT_NO_THROW(hub->run());

  // Hub should be running
  ASSERT_TRUE(hub->isRunning());

  // Create two network nodes
  NetworkNode::SPtr rootNode;
  NetworkNode::SPtr netNode;

  const std::string addr{""};
  ASSERT_NO_THROW(rootNode = std::make_shared<NetworkNode>(
          kPToPSendPort, kPToPRecvPort, kBcastSendPort, kBcastRecvPort,
          addr, addr, addr, addr, true /* this node is the root node*/));
  ASSERT_NO_THROW(netNode = std::make_shared<NetworkNode>(
          kPToPSendPort, kPToPRecvPort, kBcastSendPort, kBcastRecvPort));

  // Initialize the nodes, first on the non-root,
  // then on the root to synchronize the network
  int err = optilab::optnet::networkconst::ERR_NO_ERROR;
  ASSERT_NO_THROW(err = rootNode->init());
  ASSERT_EQ(err, optilab::optnet::networkconst::ERR_NO_ERROR);

  ASSERT_NO_THROW(err = netNode->init());
  ASSERT_EQ(err, optilab::optnet::networkconst::ERR_NO_ERROR);

  // Wait for initialization to complete
  std::this_thread::sleep_for(std::chrono::seconds(1));
  ASSERT_TRUE(!!rootNode->getNetworkCommunicator());
  ASSERT_TRUE(!!netNode->getNetworkCommunicator());

  // Register the nodes to the hub
  ASSERT_NO_THROW(err = netNode->registerNodeToHub());
  ASSERT_EQ(err, optilab::optnet::networkconst::ERR_NO_ERROR);

  ASSERT_NO_THROW(err = rootNode->registerNodeToHub());
  ASSERT_EQ(err, optilab::optnet::networkconst::ERR_NO_ERROR);

  std::this_thread::sleep_for(std::chrono::seconds(1));

  // Network size should be two for both nodes
  EXPECT_EQ(2, rootNode->getNetworkSize());
  EXPECT_EQ(2, netNode->getNetworkSize());

  // Send a packet from root node to network node
  const int packetData = 10;
  Packet::UPtr sendPacket(new Packet());
  sendPacket->networkPacket = NetworkPacket<int>::UPtr(new NetworkPacket<int>(packetData));
  ASSERT_EQ(optilab::optnet::networkconst::ERR_NO_ERROR,
            rootNode->send(std::move(sendPacket), netNode->getRank()));

  // Receive the packet from sent by the root node
  Packet::UPtr recvPacket = netNode->receive(rootNode->getRank());
  ASSERT_TRUE(!!recvPacket);

  NetworkPacket<int>::UPtr recvNetworkPacket;
  ASSERT_NO_THROW(recvNetworkPacket =
      castBaseToNetworkPacket<int>(std::move(recvPacket->networkPacket)));
  ASSERT_TRUE(!!recvNetworkPacket);

  // Check the data into the received packet
  EXPECT_EQ(1, recvNetworkPacket->size());
  EXPECT_EQ(packetData, recvNetworkPacket->data.at(0));

  // Tear down the nodes
  rootNode.reset();
  netNode.reset();

  // Turn-down should stop the running hub without throwing
  EXPECT_NO_THROW(hub->turnDown());
}  // SendMessage_PTP_No_Throw

TEST(NetworkNode, Broadcast_Message_No_Throw)
{

  using namespace optilab;
  using namespace optnet;

  // Create and start the hub
  NetworkHub::SPtr hub;
  ASSERT_NO_THROW(hub =
      std::make_shared<NetworkHub>(kPToPSendPort, kPToPRecvPort, kBcastSendPort, kBcastRecvPort));

  // Run the hub
  ASSERT_NO_THROW(hub->run());

  // Hub should be running
  ASSERT_TRUE(hub->isRunning());

  // Create two network nodes
  NetworkNode::SPtr rootNode;
  NetworkNode::SPtr netNodeA;
  NetworkNode::SPtr netNodeB;

  const std::string addr{""};
  ASSERT_NO_THROW(rootNode = std::make_shared<NetworkNode>(
          kPToPSendPort, kPToPRecvPort, kBcastSendPort, kBcastRecvPort,
          addr, addr, addr, addr, true /* this node is the root node*/));
  ASSERT_NO_THROW(netNodeA = std::make_shared<NetworkNode>(
          kPToPSendPort, kPToPRecvPort, kBcastSendPort, kBcastRecvPort));
  ASSERT_NO_THROW(netNodeB = std::make_shared<NetworkNode>(
          kPToPSendPort, kPToPRecvPort, kBcastSendPort, kBcastRecvPort));

  // Initialize the nodes, first on the non-root,
  // then on the root to synchronize the network
  int err = optilab::optnet::networkconst::ERR_NO_ERROR;
  ASSERT_NO_THROW(err = rootNode->init());
  ASSERT_EQ(err, optilab::optnet::networkconst::ERR_NO_ERROR);

  ASSERT_NO_THROW(err = netNodeA->init());
  ASSERT_EQ(err, optilab::optnet::networkconst::ERR_NO_ERROR);

  ASSERT_NO_THROW(err = netNodeB->init());
  ASSERT_EQ(err, optilab::optnet::networkconst::ERR_NO_ERROR);

  // Wait for initialization to complete
  std::this_thread::sleep_for(std::chrono::seconds(1));
  ASSERT_TRUE(!!rootNode->getNetworkCommunicator());
  ASSERT_TRUE(!!netNodeA->getNetworkCommunicator());
  ASSERT_TRUE(!!netNodeB->getNetworkCommunicator());

  // Register the nodes to the hub
  ASSERT_NO_THROW(err = netNodeA->registerNodeToHub());
  ASSERT_EQ(err, optilab::optnet::networkconst::ERR_NO_ERROR);

  ASSERT_NO_THROW(err = netNodeB->registerNodeToHub());
  ASSERT_EQ(err, optilab::optnet::networkconst::ERR_NO_ERROR);

  ASSERT_NO_THROW(err = rootNode->registerNodeToHub());
  ASSERT_EQ(err, optilab::optnet::networkconst::ERR_NO_ERROR);

  std::this_thread::sleep_for(std::chrono::seconds(1));

  // Network size should be two for both nodes
  EXPECT_EQ(3, rootNode->getNetworkSize());
  EXPECT_EQ(3, netNodeA->getNetworkSize());
  EXPECT_EQ(3, netNodeB->getNetworkSize());

  // Send a packet from root node to network node
  const int packetData = 10;
  Packet::UPtr sendPacket(new Packet());
  sendPacket->networkPacket = NetworkPacket<int>::UPtr(new NetworkPacket<int>(packetData));
  ASSERT_EQ(optilab::optnet::networkconst::ERR_NO_ERROR,
            rootNode->broadcast(sendPacket, rootNode->getRank()));

  // Receive the broadcasted packet from the root node
  Packet::UPtr recvPacketA;
  Packet::UPtr recvPacketB;
  netNodeA->broadcast(recvPacketA, rootNode->getRank());
  netNodeB->broadcast(recvPacketB, rootNode->getRank());
  ASSERT_TRUE(!!recvPacketA);
  ASSERT_TRUE(!!recvPacketB);

  NetworkPacket<int>::UPtr recvNetworkPacketA;
  NetworkPacket<int>::UPtr recvNetworkPacketB;
  ASSERT_NO_THROW(recvNetworkPacketA =
      castBaseToNetworkPacket<int>(std::move(recvPacketA->networkPacket)));
  ASSERT_NO_THROW(recvNetworkPacketB =
      castBaseToNetworkPacket<int>(std::move(recvPacketB->networkPacket)));
  ASSERT_TRUE(!!recvNetworkPacketA);
  ASSERT_TRUE(!!recvNetworkPacketB);

  // Check the data into the received packet
  EXPECT_EQ(1, recvNetworkPacketA->size());
  EXPECT_EQ(1, recvNetworkPacketB->size());
  EXPECT_EQ(packetData, recvNetworkPacketA->data.at(0));
  EXPECT_EQ(packetData, recvNetworkPacketB->data.at(0));

  // Tear down the nodes
  rootNode.reset();
  netNodeA.reset();
  netNodeB.reset();

  // Turn-down should stop the running hub without throwing
  EXPECT_NO_THROW(hub->turnDown());
}  // SendMessage_PTP_No_Throw
