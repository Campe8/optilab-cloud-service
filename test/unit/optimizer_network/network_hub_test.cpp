#include <memory>     // for std::shared_ptr
#include <stdexcept>  // for std::invalid_argument
#include <string>

#include <gtest/gtest.h>

#include "optimizer_network/network_hub.hpp"
#include "optimizer_network/network_constants.hpp"

namespace {
constexpr int kPToPSendPort = 11000;
constexpr int kPToPRecvPort = 11001;
constexpr int kBcastSendPort = 11002;
constexpr int kBcastRecvPort = 11003;
}  // namespace

TEST(NetworkHub, Constructor_Valid_PortNumber_NoThrow) {
  using namespace optilab;
  using namespace optnet;

  NetworkHub::SPtr hub;
  ASSERT_NO_THROW(hub = std::make_shared<NetworkHub>(kPToPSendPort, kPToPRecvPort, kBcastSendPort,
                                                     kBcastRecvPort));
  EXPECT_FALSE(hub->isRunning());
}  // Constructor_Valid_PortNumber_NoThrow

TEST(NetworkHub, Hub_Double_Turndown_NoThrow) {
  using namespace optilab;
  using namespace optnet;

  NetworkHub::SPtr hub;
  ASSERT_NO_THROW(hub = std::make_shared<NetworkHub>(kPToPSendPort, kPToPRecvPort, kBcastSendPort,
                                                     kBcastRecvPort));
  EXPECT_NO_THROW(hub->turnDown());
  EXPECT_NO_THROW(hub->turnDown());
}  // Hub_Double_Turndown_NoThrow

TEST(NetworkHub, Hub_SynchNetwork_NoInit_NoThrow) {
  using namespace optilab;
  using namespace optnet;

  NetworkHub::SPtr hub;
  ASSERT_NO_THROW(hub = std::make_shared<NetworkHub>(kPToPSendPort, kPToPRecvPort, kBcastSendPort,
                                                     kBcastRecvPort));
  EXPECT_NO_THROW(hub->synchNetwork());
}  // Hub_SynchNetwork_NoInit_NoThrow

TEST(NetworkHub, Hub_Run_And_Turndown_NoThrow) {
  using namespace optilab;
  using namespace optnet;

  NetworkHub::SPtr hub;
  ASSERT_NO_THROW(hub = std::make_shared<NetworkHub>(kPToPSendPort, kPToPRecvPort, kBcastSendPort,
                                                     kBcastRecvPort));

  // Run the hub
  EXPECT_NO_THROW(hub->run());

  // Hub should be running
  EXPECT_TRUE(hub->isRunning());

  // Turn-down should stop the running hub without throwing
  EXPECT_NO_THROW(hub->turnDown());

  // Hub should not be running anymore
  EXPECT_FALSE(hub->isRunning());

  // Forcing the call to the destructor to verify that it doesn't throw nor hang
  EXPECT_NO_THROW(hub.reset());
}  // Hub_Run_And_Turndown_NoThrow
