#include <chrono>
#include <memory>     // for std::shared_ptr
#include <stdexcept>  // for std::invalid_argument
#include <string>
#include <thread>
#include <utility>    // for std::move

#include <gtest/gtest.h>
#include <zeromq/zhelpers.hpp>

#include "network_fixtures.hpp"
#include "optimizer_network/network_communicator.hpp"
#include "optimizer_network/network_constants.hpp"
#include "optimizer_network/network_hub.hpp"
#include "optimizer_network/network_packet.hpp"
#include "optimizer_network/packet.hpp"

namespace {
constexpr int kPToPSendPort = 10000;
constexpr int kPToPRecvPort = 10001;
constexpr int kBcastSendPort = 10002;
constexpr int kBcastRecvPort = 10003;
constexpr int kConnectionTimeoutMsec = 100;
}  // namespace

TEST_F(NetworkCommFixture, Constructor_Valid_Connector_NoThrows)
{
  connectSockets(kPToPSendPort, kPToPRecvPort, kBcastSendPort, kBcastRecvPort,
                 kConnectionTimeoutMsec);
  EXPECT_NO_THROW(optilab::optnet::NetworkCommunicator(
      getPToPSendConn(), getPToPRecvConn(), getBcastSendConn(), getBcastRecvConn(), getContext()));
}  // Constructor_Vali_Connector_NoThrows

TEST_F(NetworkCommFixture, Default_Rank_And_NetworkSize)
{
  connectSockets(kPToPSendPort, kPToPRecvPort, kBcastSendPort, kBcastRecvPort,
                 kConnectionTimeoutMsec);
  optilab::optnet::NetworkCommunicator::SPtr comm;
  ASSERT_NO_THROW(comm = std::make_shared<optilab::optnet::NetworkCommunicator>(
      getPToPSendConn(), getPToPRecvConn(), getBcastSendConn(), getBcastRecvConn(), getContext()));

  EXPECT_EQ(optilab::optnet::networkconst::INVALID_NETWORK_NODE_ID, comm->getRank());
  EXPECT_EQ(0, comm->getSize());
}  // Default_Rank_And_NetworkSize

TEST_F(NetworkCommFixture, Constructor_Empty_PToPSend_Throws)
{
  connectSockets(kPToPSendPort, kPToPRecvPort, kBcastSendPort, kBcastRecvPort,
                 kConnectionTimeoutMsec);

  getPToPSendConn()->closeSocket();
  EXPECT_THROW(
      optilab::optnet::NetworkCommunicator(
          nullptr, getPToPRecvConn(), getBcastSendConn(), getBcastRecvConn(), getContext()),
          std::invalid_argument);
}  // Constructor_Empty_PToPSend_Throws

TEST_F(NetworkCommFixture, Constructor_Empty_PToPRecv_Throws)
{
  connectSockets(kPToPSendPort, kPToPRecvPort, kBcastSendPort, kBcastRecvPort,
                 kConnectionTimeoutMsec);

  getPToPRecvConn()->closeSocket();
  EXPECT_THROW(
      optilab::optnet::NetworkCommunicator(
          getPToPSendConn(), nullptr, getBcastSendConn(), getBcastRecvConn(), getContext()),
          std::invalid_argument);
}  // Constructor_Empty_PToPRecv_Throws

TEST_F(NetworkCommFixture, Constructor_Empty_BcastSend_Throws)
{
  connectSockets(kPToPSendPort, kPToPRecvPort, kBcastSendPort, kBcastRecvPort,
                 kConnectionTimeoutMsec);

  getBcastSendConn()->closeSocket();
  EXPECT_THROW(
      optilab::optnet::NetworkCommunicator(
          getPToPSendConn(), getPToPRecvConn(), nullptr, getBcastRecvConn(), getContext()),
          std::invalid_argument);
}  // Constructor_Empty_BcastSend_Throws

TEST_F(NetworkCommFixture, Constructor_Empty_BcastRecv_Throws)
{
  connectSockets(kPToPSendPort, kPToPRecvPort, kBcastSendPort, kBcastRecvPort,
                 kConnectionTimeoutMsec);

  getBcastRecvConn()->closeSocket();
  EXPECT_THROW(
      optilab::optnet::NetworkCommunicator(
          getPToPSendConn(), getPToPRecvConn(), getBcastSendConn(), nullptr, getContext()),
          std::invalid_argument);
}  // Constructor_Empty_BcastRecv_Throws

TEST_F(NetworkCommFixture, Constructor_Empty_Ctx_Throws)
{
  connectSockets(kPToPSendPort, kPToPRecvPort, kBcastSendPort, kBcastRecvPort,
                 kConnectionTimeoutMsec);

  EXPECT_THROW(
      optilab::optnet::NetworkCommunicator(
          getPToPSendConn(), getPToPRecvConn(), getBcastSendConn(), getBcastRecvConn(), nullptr),
          std::invalid_argument);

  getContext()->close();
}  // Constructor_Empty_Ctx_Throws

TEST_F(NetworkCommFixture, Invalid_Init_Return_Error_No_Throws)
{
  connectSockets(kPToPSendPort, kPToPRecvPort, kBcastSendPort, kBcastRecvPort,
                 kConnectionTimeoutMsec);

  optilab::optnet::NetworkCommunicator::SPtr comm;
  ASSERT_NO_THROW(comm = std::make_shared<optilab::optnet::NetworkCommunicator>(
      getPToPSendConn(), getPToPRecvConn(), getBcastSendConn(), getBcastRecvConn(), getContext()));

  int initWaitTimeMsec = 500;
  int err = optilab::optnet::networkconst::ERR_NO_ERROR;
  ASSERT_NO_THROW(err = comm->init(initWaitTimeMsec));
  EXPECT_EQ(err, optilab::optnet::networkconst::ERR_GENERIC_ERROR);

  // Force cleanup
  EXPECT_NO_THROW(comm.reset());
}  // Invalid_Init_Return_Error_No_Throws

TEST_F(NetworkCommFixture, Network_Setup_Synch_Teardown_NoThrow)
{
  using namespace optilab;
  using namespace optnet;

  // Create and start the hub listening on sockets
  NetworkHub::SPtr hub;
  ASSERT_NO_THROW(hub =
      std::make_shared<NetworkHub>(kPToPSendPort, kPToPRecvPort, kBcastSendPort, kBcastRecvPort));

  // Run the hub
  ASSERT_NO_THROW(hub->run());

  // Hub should be running
  ASSERT_TRUE(hub->isRunning());

  // Connect the sockets to the hub
  connectSockets(kPToPSendPort, kPToPRecvPort, kBcastSendPort, kBcastRecvPort, 0, 2);

  // Create two network communicators
  optilab::optnet::NetworkCommunicator::SPtr commA;
  optilab::optnet::NetworkCommunicator::SPtr commB;

  // Use the first communicator as root communicator
  const bool isRoot = true;
  ASSERT_NO_THROW(commA = std::make_shared<optilab::optnet::NetworkCommunicator>(
      getPToPSendConn(0), getPToPRecvConn(0), getBcastSendConn(0), getBcastRecvConn(0),
      getContext(0), isRoot, isRoot));
  ASSERT_NO_THROW(commB = std::make_shared<optilab::optnet::NetworkCommunicator>(
      getPToPSendConn(1), getPToPRecvConn(1), getBcastSendConn(1), getBcastRecvConn(1),
      getContext(1)));

  // Initialize the communicator, first on the non-root, then on the root to synchronize
  // the network
  int err = optilab::optnet::networkconst::ERR_NO_ERROR;
  ASSERT_NO_THROW(err = commB->init());
  ASSERT_EQ(err, optilab::optnet::networkconst::ERR_NO_ERROR);

  ASSERT_NO_THROW(err = commA->init());
  ASSERT_EQ(err, optilab::optnet::networkconst::ERR_NO_ERROR);

  // Wait for initialization to complete
  std::this_thread::sleep_for(std::chrono::seconds(1));

  // Check network identifiers and size
  EXPECT_EQ(networkconst::ROOT_NETWORK_NODE_ID, commA->getRank());
  EXPECT_GT(commB->getRank(), networkconst::ROOT_NETWORK_NODE_ID);
  EXPECT_EQ(2, commA->getSize());
  EXPECT_EQ(2, commB->getSize());

  // Send abort message
  ASSERT_NO_THROW(commA->abort());

  // Turn down communicators
  ASSERT_NO_THROW(commA->turnDown());
  ASSERT_NO_THROW(commB->turnDown());

  // Turn-down should stop the running hub without throwing
  EXPECT_NO_THROW(hub->turnDown());

  commA.reset();
  commB.reset();
  hub.reset();
}  // Network_Setup_Synch_Teardown_NoThrow

TEST_F(NetworkCommFixture, Send_Receive_Network_PointToPoint)
{
  using namespace optilab;
  using namespace optnet;

  // Create and start the hub listening on sockets
  NetworkHub::SPtr hub;
  ASSERT_NO_THROW(hub =
      std::make_shared<NetworkHub>(kPToPSendPort, kPToPRecvPort, kBcastSendPort, kBcastRecvPort));

  // Run the hub
  ASSERT_NO_THROW(hub->run());

  // Hub should be running
  ASSERT_TRUE(hub->isRunning());

  // Connect the sockets to the hub
  connectSockets(kPToPSendPort, kPToPRecvPort, kBcastSendPort, kBcastRecvPort, 0, 2);

  // Create two network communicators
  optilab::optnet::NetworkCommunicator::SPtr commA;
  optilab::optnet::NetworkCommunicator::SPtr commB;

  // Use the first communicator as root communicator
  const bool isRoot = true;
  ASSERT_NO_THROW(commA = std::make_shared<optilab::optnet::NetworkCommunicator>(
      getPToPSendConn(0), getPToPRecvConn(0), getBcastSendConn(0), getBcastRecvConn(0),
      getContext(0), isRoot, isRoot));
  ASSERT_NO_THROW(commB = std::make_shared<optilab::optnet::NetworkCommunicator>(
      getPToPSendConn(1), getPToPRecvConn(1), getBcastSendConn(1), getBcastRecvConn(1),
      getContext(1)));

  // Initialize the communicator, first on the non-root,
  // then on the root to synchronize the network
  int err = optilab::optnet::networkconst::ERR_NO_ERROR;
  ASSERT_NO_THROW(err = commB->init());
  ASSERT_EQ(err, optilab::optnet::networkconst::ERR_NO_ERROR);

  ASSERT_NO_THROW(err = commA->init());
  ASSERT_EQ(err, optilab::optnet::networkconst::ERR_NO_ERROR);

  // Wait for initialization to complete
  std::this_thread::sleep_for(std::chrono::seconds(1));

  // Check network identifiers and sizes
  const int commARank = commA->getRank();
  const int commBRank = commB->getRank();

  // Send a network packet to commB network node
  const int packetData = 10;
  Packet::UPtr sendPacket(new Packet());
  sendPacket->networkPacket = NetworkPacket<int>::UPtr(new NetworkPacket<int>(packetData));
  ASSERT_EQ(optilab::optnet::networkconst::ERR_NO_ERROR,
            commA->send(std::move(sendPacket), commBRank));

  // Receive the packet from commA
  Packet::UPtr recvPacket(new Packet());
  ASSERT_EQ(optilab::optnet::networkconst::ERR_NO_ERROR,
            commB->receive(recvPacket, commARank));
  ASSERT_TRUE(!!recvPacket);

  NetworkPacket<int>::UPtr recvNetworkPacket;
  ASSERT_NO_THROW(recvNetworkPacket =
      castBaseToNetworkPacket<int>(std::move(recvPacket->networkPacket)));
  ASSERT_TRUE(!!recvNetworkPacket);

  // Check the data into the received packet
  EXPECT_EQ(1, recvNetworkPacket->size());
  EXPECT_EQ(packetData, recvNetworkPacket->data.at(0));

  // Check that the received packet is coming from the the node with commARank
  EXPECT_EQ(commARank, commB->convertNetworkAddrToRank(recvPacket->networkAddr));

  // Send abort message
  ASSERT_NO_THROW(commA->abort());

  // Turn down communicators
  ASSERT_NO_THROW(commA->turnDown());
  ASSERT_NO_THROW(commB->turnDown());

  // Turn-down should stop the running hub without throwing
  EXPECT_NO_THROW(hub->turnDown());

  commA.reset();
  commB.reset();
  hub.reset();
}  // Send_Receive_Network_PointToPoint

TEST_F(NetworkCommFixture, IReceive_Network_PointToPoint)
{
  using namespace optilab;
  using namespace optnet;

  // Create and start the hub listening on sockets
  NetworkHub::SPtr hub;
  ASSERT_NO_THROW(hub =
      std::make_shared<NetworkHub>(kPToPSendPort, kPToPRecvPort, kBcastSendPort, kBcastRecvPort));

  // Run the hub
  ASSERT_NO_THROW(hub->run());

  // Hub should be running
  ASSERT_TRUE(hub->isRunning());

  // Connect the sockets to the hub
  connectSockets(kPToPSendPort, kPToPRecvPort, kBcastSendPort, kBcastRecvPort, 0, 1);

  // Create two network communicators
  optilab::optnet::NetworkCommunicator::SPtr commA;

  // Use the first communicator as root communicator
  const bool isRoot = true;
  ASSERT_NO_THROW(commA = std::make_shared<optilab::optnet::NetworkCommunicator>(
      getPToPSendConn(0), getPToPRecvConn(0), getBcastSendConn(0), getBcastRecvConn(0),
      getContext(0), isRoot, isRoot));

  // Initialize the communicator, first on the non-root,
  // then on the root to synchronize the network
  int err = optilab::optnet::networkconst::ERR_NO_ERROR;

  ASSERT_NO_THROW(err = commA->init());
  ASSERT_EQ(err, optilab::optnet::networkconst::ERR_NO_ERROR);

  // Wait for initialization to complete
  std::this_thread::sleep_for(std::chrono::seconds(1));

  // IReceive
  bool errIRecv = true;
  int irecvPacket = 1;
  Packet::UPtr recvPacket(new Packet());
  ASSERT_NO_THROW(irecvPacket = commA->ireceive(recvPacket, 10, &errIRecv));

  // Check results: no errors
  EXPECT_FALSE(errIRecv);

  // Check results: packet not received
  EXPECT_EQ(0, irecvPacket);

  // Check results: empty packet
  EXPECT_TRUE((recvPacket->networkAddr).empty());
  EXPECT_FALSE(recvPacket->networkPacket);

  // Send abort message
  ASSERT_NO_THROW(commA->abort());

  // Turn down communicators
  ASSERT_NO_THROW(commA->turnDown());

  // Turn-down should stop the running hub without throwing
  EXPECT_NO_THROW(hub->turnDown());

  commA.reset();
  hub.reset();
}  // IReceive_Network_PointToPoint

TEST_F(NetworkCommFixture, Send_Receive_Network_PointToPoint_Tag)
{
  using namespace optilab;
  using namespace optnet;

  // Create and start the hub listening on sockets
  NetworkHub::SPtr hub;
  ASSERT_NO_THROW(hub =
      std::make_shared<NetworkHub>(kPToPSendPort, kPToPRecvPort, kBcastSendPort, kBcastRecvPort));

  // Run the hub
  ASSERT_NO_THROW(hub->run());

  // Hub should be running
  ASSERT_TRUE(hub->isRunning());

  // Connect the sockets to the hub
  connectSockets(kPToPSendPort, kPToPRecvPort, kBcastSendPort, kBcastRecvPort, 0, 2);

  // Create two network communicators
  optilab::optnet::NetworkCommunicator::SPtr commA;
  optilab::optnet::NetworkCommunicator::SPtr commB;

  // Use the first communicator as root communicator
  const bool isRoot = true;
  ASSERT_NO_THROW(commA = std::make_shared<optilab::optnet::NetworkCommunicator>(
      getPToPSendConn(0), getPToPRecvConn(0), getBcastSendConn(0), getBcastRecvConn(0),
      getContext(0), isRoot, isRoot));
  ASSERT_NO_THROW(commB = std::make_shared<optilab::optnet::NetworkCommunicator>(
      getPToPSendConn(1), getPToPRecvConn(1), getBcastSendConn(1), getBcastRecvConn(1),
      getContext(1)));

  // Initialize the communicator, first on the non-root,
  // then on the root to synchronize the network
  int err = optilab::optnet::networkconst::ERR_NO_ERROR;
  ASSERT_NO_THROW(err = commB->init());
  ASSERT_EQ(err, optilab::optnet::networkconst::ERR_NO_ERROR);

  ASSERT_NO_THROW(err = commA->init());
  ASSERT_EQ(err, optilab::optnet::networkconst::ERR_NO_ERROR);

  // Wait for initialization to complete
  std::this_thread::sleep_for(std::chrono::seconds(1));

  // Check network identifiers and sizes
  const int commARank = commA->getRank();
  const int commBRank = commB->getRank();

  // Send a network packet to commB network node
  const int packetTag1 = 1;
  const int packetTag2 = 2;
  const int packetData1 = 10;
  const int packetData2 = 20;
  Packet::UPtr sendPacket1(new Packet());
  Packet::UPtr sendPacket2(new Packet());
  sendPacket1->networkPacket = NetworkPacket<int>::UPtr(new NetworkPacket<int>(packetData1));
  sendPacket2->networkPacket = NetworkPacket<int>::UPtr(new NetworkPacket<int>(packetData2));

  // Set a packet tag which identifies this packet type
  sendPacket1->packetTag = PacketTag::UPtr(new PacketTag(packetTag1));
  sendPacket2->packetTag = PacketTag::UPtr(new PacketTag(packetTag2));

  // Send both packets to commBRank
  ASSERT_EQ(optilab::optnet::networkconst::ERR_NO_ERROR,
            commA->send(std::move(sendPacket1), commBRank));
  ASSERT_EQ(optilab::optnet::networkconst::ERR_NO_ERROR,
            commA->send(std::move(sendPacket2), commBRank));

  // Receive the packet from commA
  Packet::UPtr recvPacket(new Packet());

  // Set the receiving tag
  recvPacket->packetTag = PacketTag::UPtr(new PacketTag(packetTag2));

  // Receive from the hub only the packets with specified tag,
  // i.e., packetTag2.
  // @note packetData1 is discarded
  ASSERT_EQ(optilab::optnet::networkconst::ERR_NO_ERROR,
            commB->receive(recvPacket, commARank));
  ASSERT_TRUE(!!recvPacket);

  NetworkPacket<int>::UPtr recvNetworkPacket;
  ASSERT_NO_THROW(recvNetworkPacket =
      castBaseToNetworkPacket<int>(std::move(recvPacket->networkPacket)));
  ASSERT_TRUE(!!recvNetworkPacket);

  // Check the data into the received packet
  EXPECT_EQ(1, recvNetworkPacket->size());
  EXPECT_EQ(packetData2, recvNetworkPacket->data.at(0));

  // Send abort message
  ASSERT_NO_THROW(commA->abort());

  // Turn down communicators
  ASSERT_NO_THROW(commA->turnDown());
  ASSERT_NO_THROW(commB->turnDown());

  // Turn-down should stop the running hub without throwing
  EXPECT_NO_THROW(hub->turnDown());

  commA.reset();
  commB.reset();
  hub.reset();
}  // Send_Receive_Network_PointToPoint_Tag

TEST_F(NetworkCommFixture, Receive_Network_Broadcast)
{
  using namespace optilab;
  using namespace optnet;

  // Create and start the hub listening on sockets
  NetworkHub::SPtr hub;
  ASSERT_NO_THROW(hub =
      std::make_shared<NetworkHub>(kPToPSendPort, kPToPRecvPort, kBcastSendPort, kBcastRecvPort));

  // Run the hub
  ASSERT_NO_THROW(hub->run());

  // Hub should be running
  ASSERT_TRUE(hub->isRunning());

  // Connect the sockets to the hub
  connectSockets(kPToPSendPort, kPToPRecvPort, kBcastSendPort, kBcastRecvPort, 0, 2);

  // Create two network communicators
  optilab::optnet::NetworkCommunicator::SPtr commA;
  optilab::optnet::NetworkCommunicator::SPtr commB;

  // Use the first communicator as root communicator
  const bool isRoot = true;
  ASSERT_NO_THROW(commA = std::make_shared<optilab::optnet::NetworkCommunicator>(
      getPToPSendConn(0), getPToPRecvConn(0), getBcastSendConn(0), getBcastRecvConn(0),
      getContext(0), isRoot, isRoot));
  ASSERT_NO_THROW(commB = std::make_shared<optilab::optnet::NetworkCommunicator>(
      getPToPSendConn(1), getPToPRecvConn(1), getBcastSendConn(1), getBcastRecvConn(1),
      getContext(1)));

  // Initialize the communicator, first on the non-root,
  // then on the root to synchronize the network
  int err = optilab::optnet::networkconst::ERR_NO_ERROR;
  ASSERT_NO_THROW(err = commB->init());
  ASSERT_EQ(err, optilab::optnet::networkconst::ERR_NO_ERROR);

  ASSERT_NO_THROW(err = commA->init());
  ASSERT_EQ(err, optilab::optnet::networkconst::ERR_NO_ERROR);

  // Wait for initialization to complete
  std::this_thread::sleep_for(std::chrono::seconds(1));

  // Check network identifiers and sizes
  const int commARank = commA->getRank();
  const int commBRank = commB->getRank();

  // Broadcast a network packet from the root of the network to all other nodes
  const int packetData = 10;
  Packet::UPtr sendPacket(new Packet());
  sendPacket->networkPacket = NetworkPacket<int>::UPtr(new NetworkPacket<int>(packetData));
  ASSERT_EQ(optilab::optnet::networkconst::ERR_NO_ERROR,
            commA->broadcast(sendPacket, commARank));

  // Receive the broadcasted packet from the sender commARank
  Packet::UPtr recvPacket;
  ASSERT_EQ(optilab::optnet::networkconst::ERR_NO_ERROR,
            commB->broadcast(recvPacket, commARank));
  ASSERT_TRUE(!!recvPacket);

  NetworkPacket<int>::UPtr recvNetworkPacket;
  ASSERT_NO_THROW(recvNetworkPacket =
      castBaseToNetworkPacket<int>(std::move(recvPacket->networkPacket)));
  ASSERT_TRUE(!!recvNetworkPacket);

  // Check the data into the received packet
  EXPECT_EQ(1, recvNetworkPacket->size());
  EXPECT_EQ(packetData, recvNetworkPacket->data.at(0));

  // Send abort message
  ASSERT_NO_THROW(commA->abort());

  // Turn down communicators
  ASSERT_NO_THROW(commA->turnDown());
  ASSERT_NO_THROW(commB->turnDown());

  // Turn-down should stop the running hub without throwing
  EXPECT_NO_THROW(hub->turnDown());

  commA.reset();
  commB.reset();
  hub.reset();
}  // Receive_Network_Broadcast

TEST_F(NetworkCommFixture, Send_Probe_Network_PointToPoint)
{
  using namespace optilab;
  using namespace optnet;

  // Create and start the hub listening on sockets
  NetworkHub::SPtr hub;
  ASSERT_NO_THROW(hub =
      std::make_shared<NetworkHub>(kPToPSendPort, kPToPRecvPort, kBcastSendPort, kBcastRecvPort));

  // Run the hub
  ASSERT_NO_THROW(hub->run());

  // Hub should be running
  ASSERT_TRUE(hub->isRunning());

  // Connect the sockets to the hub
  const int numConnections = 2;
  connectSockets(kPToPSendPort, kPToPRecvPort, kBcastSendPort, kBcastRecvPort, 0, numConnections);

  // Create two network communicators that will use the connections previously created
  optilab::optnet::NetworkCommunicator::SPtr commA;
  optilab::optnet::NetworkCommunicator::SPtr commB;

  // Use the first communicator as root communicator
  const bool isRoot = true;
  ASSERT_NO_THROW(commA = std::make_shared<optilab::optnet::NetworkCommunicator>(
      getPToPSendConn(0), getPToPRecvConn(0), getBcastSendConn(0), getBcastRecvConn(0),
      getContext(0), isRoot, isRoot));
  ASSERT_NO_THROW(commB = std::make_shared<optilab::optnet::NetworkCommunicator>(
      getPToPSendConn(1), getPToPRecvConn(1), getBcastSendConn(1), getBcastRecvConn(1),
      getContext(1)));

  // Initialize the communicator, first on the non-root,
  // then on the root to synchronize the network
  int err = optilab::optnet::networkconst::ERR_NO_ERROR;
  ASSERT_NO_THROW(err = commB->init());
  ASSERT_EQ(err, optilab::optnet::networkconst::ERR_NO_ERROR);

  ASSERT_NO_THROW(err = commA->init());
  ASSERT_EQ(err, optilab::optnet::networkconst::ERR_NO_ERROR);

  // Wait for initialization to complete
  std::this_thread::sleep_for(std::chrono::seconds(1));

  // Check network identifiers and sizes
  const int commARank = commA->getRank();
  const int commBRank = commB->getRank();

  // Send a network packet to commB network node
  const int packetTag = 1;
  const int packetData = 10;
  Packet::UPtr sendPacket(new Packet());
  sendPacket->networkPacket = NetworkPacket<int>::UPtr(new NetworkPacket<int>(packetData));

  // Set a packet tag which identifies this packet type
  sendPacket->packetTag = PacketTag::UPtr(new PacketTag(packetTag));

  // Send both packets to commBRank
  ASSERT_EQ(optilab::optnet::networkconst::ERR_NO_ERROR,
            commA->send(std::move(sendPacket), commBRank));

  // Receive the packet from commA
  Packet::UPtr recvPacket(new Packet());

  // Probe the hub, i.e., wait to receive any packet (not tag) from any node
  // @note packetData1 is discarded
  ASSERT_EQ(optilab::optnet::networkconst::ERR_NO_ERROR,
            commB->probe(recvPacket));
  ASSERT_TRUE(!!recvPacket);

  NetworkPacket<int>::UPtr recvNetworkPacket;
  ASSERT_NO_THROW(recvNetworkPacket =
      castBaseToNetworkPacket<int>(std::move(recvPacket->networkPacket)));
  ASSERT_TRUE(!!recvNetworkPacket);

  // Check the tag of the received packet
  EXPECT_EQ(packetTag, recvPacket->packetTag->getTag());

  // Check the data into the received packet
  EXPECT_EQ(1, recvNetworkPacket->size());
  EXPECT_EQ(packetData, recvNetworkPacket->data.at(0));

  // Send abort message
  ASSERT_NO_THROW(commA->abort());

  // Turn down communicators
  ASSERT_NO_THROW(commA->turnDown());
  ASSERT_NO_THROW(commB->turnDown());

  // Turn-down should stop the running hub without throwing
  EXPECT_NO_THROW(hub->turnDown());

  commA.reset();
  commB.reset();
  hub.reset();
}  // Send_Probe_Network_PointToPoint

TEST_F(NetworkCommFixture, IProbe_Network_PointToPoint)
{
  using namespace optilab;
  using namespace optnet;

  // Create and start the hub listening on sockets
  NetworkHub::SPtr hub;
  ASSERT_NO_THROW(hub =
      std::make_shared<NetworkHub>(kPToPSendPort, kPToPRecvPort, kBcastSendPort, kBcastRecvPort));

  // Run the hub
  ASSERT_NO_THROW(hub->run());

  // Hub should be running
  ASSERT_TRUE(hub->isRunning());

  // Connect the sockets to the hub
  const int numConnections = 1;
  connectSockets(kPToPSendPort, kPToPRecvPort, kBcastSendPort, kBcastRecvPort, 0, numConnections);

  // Create a network communicator that will use the connections previously created
  optilab::optnet::NetworkCommunicator::SPtr commA;

  // Use the first communicator as root communicator
  const bool isRoot = true;
  ASSERT_NO_THROW(commA = std::make_shared<optilab::optnet::NetworkCommunicator>(
      getPToPSendConn(0), getPToPRecvConn(0), getBcastSendConn(0), getBcastRecvConn(0),
      getContext(0), isRoot, isRoot));

  // Initialize the communicator, first on the non-root,
  // then on the root to synchronize the network
  int err = optilab::optnet::networkconst::ERR_NO_ERROR;
  ASSERT_NO_THROW(err = commA->init());
  ASSERT_EQ(err, optilab::optnet::networkconst::ERR_NO_ERROR);

  // Wait for initialization to complete
  std::this_thread::sleep_for(std::chrono::seconds(1));

  // Receive the packet from commA
  bool errProbe = true;
  int iprobeRes = 1;
  Packet::UPtr recvPacket(new Packet());

  // iprobe the hub, i.e., check-and-return for any packet (not tag) from any node
  ASSERT_NO_THROW(iprobeRes = commA->iprobe(recvPacket, &errProbe));

  // Check results: no error happened
  EXPECT_FALSE(errProbe);

  // Check results: no message received
  EXPECT_EQ(0, iprobeRes);

  // Check results: packet has not been set
  EXPECT_TRUE((recvPacket->networkAddr).empty());
  EXPECT_FALSE(recvPacket->networkPacket);

  // Send abort message
  ASSERT_NO_THROW(commA->abort());

  // Turn down communicators
  ASSERT_NO_THROW(commA->turnDown());

  // Turn-down should stop the running hub without throwing
  EXPECT_NO_THROW(hub->turnDown());

  commA.reset();
  hub.reset();
}  // IProbe_Network_PointToPoint

TEST_F(NetworkCommFixture, Send_WaitSpecTagFromSpecSource_PointToPoint_Replacement_Tag)
{
  using namespace optilab;
  using namespace optnet;

  // Create and start the hub listening on sockets
  NetworkHub::SPtr hub;
  ASSERT_NO_THROW(hub =
      std::make_shared<NetworkHub>(kPToPSendPort, kPToPRecvPort, kBcastSendPort, kBcastRecvPort));

  // Run the hub
  ASSERT_NO_THROW(hub->run());

  // Hub should be running
  ASSERT_TRUE(hub->isRunning());

  // Connect the sockets to the hub
  const int numConnections = 2;
  connectSockets(kPToPSendPort, kPToPRecvPort, kBcastSendPort, kBcastRecvPort, 0, numConnections);

  // Create two network communicators
  optilab::optnet::NetworkCommunicator::SPtr commA;
  optilab::optnet::NetworkCommunicator::SPtr commB;

  // Use the first communicator as root communicator
  const bool isRoot = true;
  ASSERT_NO_THROW(commA = std::make_shared<optilab::optnet::NetworkCommunicator>(
      getPToPSendConn(0), getPToPRecvConn(0), getBcastSendConn(0), getBcastRecvConn(0),
      getContext(0), isRoot, isRoot));
  ASSERT_NO_THROW(commB = std::make_shared<optilab::optnet::NetworkCommunicator>(
      getPToPSendConn(1), getPToPRecvConn(1), getBcastSendConn(1), getBcastRecvConn(1),
      getContext(1)));

  // Initialize the communicator, first on the non-root,
  // then on the root to synchronize the network
  int err = optilab::optnet::networkconst::ERR_NO_ERROR;
  ASSERT_NO_THROW(err = commB->init());
  ASSERT_EQ(err, optilab::optnet::networkconst::ERR_NO_ERROR);

  ASSERT_NO_THROW(err = commA->init());
  ASSERT_EQ(err, optilab::optnet::networkconst::ERR_NO_ERROR);

  // Wait for initialization to complete
  std::this_thread::sleep_for(std::chrono::seconds(1));

  // Check network identifiers and sizes
  const int commARank = commA->getRank();
  const int commBRank = commB->getRank();

  // Send a network packet to commB network node
  const int packetTag1 = 1;
  const int packetTag2 = 2;
  const int packetData1 = 10;
  const int packetData2 = 20;
  const int packetData3 = 30;
  Packet::UPtr sendPacket1(new Packet());
  Packet::UPtr sendPacket2(new Packet());
  Packet::UPtr sendPacket3(new Packet());
  sendPacket1->networkPacket = NetworkPacket<int>::UPtr(new NetworkPacket<int>(packetData1));
  sendPacket2->networkPacket = NetworkPacket<int>::UPtr(new NetworkPacket<int>(packetData2));
  sendPacket3->networkPacket = NetworkPacket<int>::UPtr(new NetworkPacket<int>(packetData3));

  // Set a packet tag which identifies this packet type
  sendPacket1->packetTag = PacketTag::UPtr(new PacketTag(packetTag1));
  sendPacket2->packetTag = PacketTag::UPtr(new PacketTag(packetTag2));

  // Send all packets to commBRank
  commA->send(std::move(sendPacket1), commBRank);
  commA->send(std::move(sendPacket2), commBRank);
  commA->send(std::move(sendPacket3), commBRank);

  // Receive a packet from commA
  Packet::UPtr recvPacket(new Packet());

  // Wait to receive from the hub/commARank only the packets with specified tag,
  // i.e., packetTag2.
  // @note packetData1 is discarded
  int packetCtr = 0;
  do
  {
    packetCtr++;

    // (Re)Set the expected tag back for next loop
    recvPacket->packetTag = PacketTag::UPtr(new PacketTag(packetTag2));

    // Safety measure
    if (packetCtr > 2) break;
  }
  while (commB->waitSpecTagFromSpecSource(recvPacket, commARank) != 0);

  // commB should have received 2 packets before breaking the while-loop:
  // packet with packetTag1 and the expected packet with packetTag2
  EXPECT_EQ(2, packetCtr);

  // Check the tag of the received packet
  EXPECT_EQ(packetTag2, recvPacket->packetTag->getTag());

  // Check the content of the packet
  NetworkPacket<int>::UPtr recvNetworkPacket;
  ASSERT_NO_THROW(recvNetworkPacket =
      castBaseToNetworkPacket<int>(std::move(recvPacket->networkPacket)));
  ASSERT_TRUE(!!recvNetworkPacket);

  // Check the data into the received packet
  EXPECT_EQ(1, recvNetworkPacket->size());
  EXPECT_EQ(packetData2, recvNetworkPacket->data.at(0));

  // Check for another packet with same tag.
  // This time, the third packet, i.e., sendPacket3 doesn't have a tag.
  // This should return ERR_GENERIC_ERROR
  const auto genErr = commB->waitSpecTagFromSpecSource(recvPacket, commARank);
  EXPECT_EQ(networkconst::ERR_GENERIC_ERROR, genErr);

  // Send abort message
  ASSERT_NO_THROW(commA->abort());

  // Turn down communicators
  ASSERT_NO_THROW(commA->turnDown());
  ASSERT_NO_THROW(commB->turnDown());

  // Turn-down should stop the running hub without throwing
  EXPECT_NO_THROW(hub->turnDown());

  commA.reset();
  commB.reset();
  hub.reset();
}  // Send_WaitSpecTagFromSpecSource_PointToPoint_Replacement_Tag

TEST_F(NetworkCommFixture, Send_WaitSpecTagFromSpecSource_PointToPoint_NoReplacement_Tag)
{
  using namespace optilab;
  using namespace optnet;

  // Create and start the hub listening on sockets
  NetworkHub::SPtr hub;
  ASSERT_NO_THROW(hub =
      std::make_shared<NetworkHub>(kPToPSendPort, kPToPRecvPort, kBcastSendPort, kBcastRecvPort));

  // Run the hub
  ASSERT_NO_THROW(hub->run());

  // Hub should be running
  ASSERT_TRUE(hub->isRunning());

  // Connect the sockets to the hub
  const int numConnections = 2;
  connectSockets(kPToPSendPort, kPToPRecvPort, kBcastSendPort, kBcastRecvPort, 0, numConnections);

  // Create two network communicators
  optilab::optnet::NetworkCommunicator::SPtr commA;
  optilab::optnet::NetworkCommunicator::SPtr commB;

  // Use the first communicator as root communicator
  const bool isRoot = true;
  ASSERT_NO_THROW(commA = std::make_shared<optilab::optnet::NetworkCommunicator>(
      getPToPSendConn(0), getPToPRecvConn(0), getBcastSendConn(0), getBcastRecvConn(0),
      getContext(0), isRoot, isRoot));
  ASSERT_NO_THROW(commB = std::make_shared<optilab::optnet::NetworkCommunicator>(
      getPToPSendConn(1), getPToPRecvConn(1), getBcastSendConn(1), getBcastRecvConn(1),
      getContext(1)));

  // Initialize the communicator, first on the non-root,
  // then on the root to synchronize the network
  int err = optilab::optnet::networkconst::ERR_NO_ERROR;
  ASSERT_NO_THROW(err = commB->init());
  ASSERT_EQ(err, optilab::optnet::networkconst::ERR_NO_ERROR);

  ASSERT_NO_THROW(err = commA->init());
  ASSERT_EQ(err, optilab::optnet::networkconst::ERR_NO_ERROR);

  // Wait for initialization to complete
  std::this_thread::sleep_for(std::chrono::seconds(1));

  // Check network identifiers and sizes
  const int commARank = commA->getRank();
  const int commBRank = commB->getRank();

  // Send a network packet to commB network node
  const int packetTag1 = 1;
  const int packetTag2 = 2;
  const int packetData1 = 10;
  const int packetData2 = 20;
  const int packetData3 = 30;
  Packet::UPtr sendPacket1(new Packet());
  Packet::UPtr sendPacket2(new Packet());
  Packet::UPtr sendPacket3(new Packet());
  sendPacket1->networkPacket = NetworkPacket<int>::UPtr(new NetworkPacket<int>(packetData1));
  sendPacket2->networkPacket = NetworkPacket<int>::UPtr(new NetworkPacket<int>(packetData2));
  sendPacket3->networkPacket = NetworkPacket<int>::UPtr(new NetworkPacket<int>(packetData3));

  // Set a packet tag which identifies this packet type
  sendPacket1->packetTag = PacketTag::UPtr(new PacketTag(packetTag1));
  sendPacket2->packetTag = PacketTag::UPtr(new PacketTag(packetTag2));

  // Send all packets to commBRank
  ASSERT_EQ(optilab::optnet::networkconst::ERR_NO_ERROR,
            commA->send(std::move(sendPacket1), commBRank));
  ASSERT_EQ(optilab::optnet::networkconst::ERR_NO_ERROR,
            commA->send(std::move(sendPacket2), commBRank));
  ASSERT_EQ(optilab::optnet::networkconst::ERR_NO_ERROR,
            commA->send(std::move(sendPacket3), commBRank));

  // Receive a packet from commA
  Packet::UPtr recvPacket(new Packet());

  // Set the receiving tag
  recvPacket->packetTag = PacketTag::UPtr(new PacketTag(packetTag2));

  // Wait to receive from the hub/commARank only the packets with specified tag,
  // i.e., packetTag2.
  // @note packetData1 is discarded
  const bool replaceTag = false;
  int packetCtr = 0;
  do
  {
    // Note that the tag has not been replaced with the received one
    packetCtr++;

    // Safety measure
    if (packetCtr > 2) break;
  }
  while (commB->waitSpecTagFromSpecSource(recvPacket, commARank, replaceTag) != 0);

  // commB should have received 2 packets before breaking the while-loop:
  // packet with packetTag1 and the expected packet with packetTag2
  EXPECT_EQ(2, packetCtr);

  // Check the tag of the received packet
  EXPECT_EQ(packetTag2, recvPacket->packetTag->getTag());

  // Check the content of the packet
  NetworkPacket<int>::UPtr recvNetworkPacket;
  ASSERT_NO_THROW(recvNetworkPacket =
      castBaseToNetworkPacket<int>(std::move(recvPacket->networkPacket)));
  ASSERT_TRUE(!!recvNetworkPacket);

  // Check the data into the received packet
  EXPECT_EQ(1, recvNetworkPacket->size());
  EXPECT_EQ(packetData2, recvNetworkPacket->data.at(0));

  // Check for another packet with same tag.
  // This time, the third packet, i.e., sendPacket3 doesn't have a tag.
  // This should return ERR_GENERIC_ERROR
  const auto genErr = commB->waitSpecTagFromSpecSource(recvPacket, commARank);
  EXPECT_EQ(networkconst::ERR_GENERIC_ERROR, genErr);

  // Send abort message
  ASSERT_NO_THROW(commA->abort());

  // Turn down communicators
  ASSERT_NO_THROW(commA->turnDown());
  ASSERT_NO_THROW(commB->turnDown());

  // Turn-down should stop the running hub without throwing
  EXPECT_NO_THROW(hub->turnDown());

  commA.reset();
  commB.reset();
  hub.reset();
}  // Send_WaitSpecTagFromSpecSource_PointToPoint_NoReplacement_Tag
