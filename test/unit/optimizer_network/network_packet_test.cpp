#include <memory>     // for std::shared_ptr
#include <stdexcept>  // for std::invalid_argument
#include <string>
#include <utility>    // for std::move

#include <gtest/gtest.h>

#include "optimizer_network/network_packet.hpp"

TEST(NetworkPacket, Packet_Eq_Operator) {
  using namespace optilab;
  using namespace optnet;

  const std::vector<int> data = {1, 2, 3};
  const std::vector<int> dataOther = {1, 2};

  NetworkPacket<int>::UPtr packet1;
  NetworkPacket<int>::UPtr packet2;
  NetworkPacket<int>::UPtr packet3;
  ASSERT_NO_THROW(packet1 = NetworkPacket<int>::UPtr(new NetworkPacket<int>(data)));
  ASSERT_NO_THROW(packet2 = NetworkPacket<int>::UPtr(new NetworkPacket<int>(data)));
  ASSERT_NO_THROW(packet3 = NetworkPacket<int>::UPtr(new NetworkPacket<int>(dataOther)));

  int idx = 0;
  EXPECT_EQ(packet1->size(), 3);
  EXPECT_EQ(packet3->size(), 2);
  EXPECT_EQ(packet1->size(), packet2->size());
  for (auto d : data)
  {
    EXPECT_EQ(d, packet1->data.at(idx));
    EXPECT_EQ(d, packet2->data.at(idx));
    idx++;
  }

  // Test equality operator
  EXPECT_TRUE(*packet1 == *packet2);
  EXPECT_FALSE(*packet1 == *packet3);
  EXPECT_FALSE(*packet2 == *packet3);
}  // Packet_Eq_Operator

TEST(NetworkPacket, Packet_Ne_Operator) {
  using namespace optilab;
  using namespace optnet;

  const std::vector<int> data = {1, 2, 3};
  const std::vector<int> dataOther = {1, 2};

  NetworkPacket<int>::UPtr packet1;
  NetworkPacket<int>::UPtr packet2;
  NetworkPacket<int>::UPtr packet3;
  ASSERT_NO_THROW(packet1 = NetworkPacket<int>::UPtr(new NetworkPacket<int>(data)));
  ASSERT_NO_THROW(packet2 = NetworkPacket<int>::UPtr(new NetworkPacket<int>(data)));
  ASSERT_NO_THROW(packet3 = NetworkPacket<int>::UPtr(new NetworkPacket<int>(dataOther)));

  int idx = 0;
  EXPECT_EQ(packet1->size(), 3);
  EXPECT_EQ(packet3->size(), 2);
  EXPECT_EQ(packet1->size(), packet2->size());
  for (auto d : data)
  {
    EXPECT_EQ(d, packet1->data.at(idx));
    EXPECT_EQ(d, packet2->data.at(idx));
    idx++;
  }

  // Test disequality operator
  EXPECT_TRUE(*packet1 != *packet3);
  EXPECT_TRUE(*packet2 != *packet3);
  EXPECT_FALSE(*packet1 != *packet2);
}  // Packet_Ne_Operator

TEST(NetworkPacket, Packet_Subscript_Operator) {
  using namespace optilab;
  using namespace optnet;

  const std::vector<int> data = {1, 2, 3};

  NetworkPacket<int>::UPtr packet;
  ASSERT_NO_THROW(packet = NetworkPacket<int>::UPtr(new NetworkPacket<int>(data)));

  // Test subscript operator
  int idx = 0;
  EXPECT_EQ(packet->size(), 3);
  for (auto d : data)
  {
    EXPECT_EQ(d, (*packet)[idx++]);
  }
}  // Packet_Subscript_Operator

TEST(NetworkPacket, Double_DeSerialize_NoThrow) {
  using namespace optilab;
  using namespace optnet;

  const std::vector<double> data = {1.1, 2.2, 3.3};

  NetworkPacket<double>::UPtr packet;
  ASSERT_NO_THROW(packet = NetworkPacket<double>::UPtr(new NetworkPacket<double>(data)));

  // Check packet size and data
  int idx = 0;
  EXPECT_EQ(packet->size(), 3);
  for (auto d : data)
  {
    EXPECT_EQ(d, packet->data.at(idx++));
  }

  // Serialize packet
  std::pair<BaseNetworkPacket::BaseNetworkPacketType, std::string> spacket;
  ASSERT_NO_THROW(spacket = packet->serialize());

  // Deserialize packet
  BaseNetworkPacket::UPtr deserBasePacket;
  ASSERT_EQ(spacket.first, BaseNetworkPacket::BaseNetworkPacketType::BNPT_NETWORK_PACKET);
  ASSERT_NO_THROW(deserBasePacket = deserializeNetworkPacket(spacket.second));

  NetworkPacket<double>::UPtr deserNetworkPacket;
  ASSERT_NO_THROW(deserNetworkPacket = castBaseToNetworkPacket<double>(std::move(deserBasePacket)));
  ASSERT_TRUE(!!deserNetworkPacket);

  // Check deserialized packet size and data
  idx = 0;
  EXPECT_EQ(deserNetworkPacket->size(), 3);
  for (auto d : data)
  {
    EXPECT_EQ(d, deserNetworkPacket->data.at(idx++));
  }
}  // Double_DeSerialize_NoThrow

TEST(NetworkPacket, Float_DeSerialize_NoThrow) {
  using namespace optilab;
  using namespace optnet;

  const std::vector<float> data = {1.1, 2.2, 3.3};

  NetworkPacket<float>::UPtr packet;
  ASSERT_NO_THROW(packet = NetworkPacket<float>::UPtr(new NetworkPacket<float>(data)));

  // Check packet size and data
  int idx = 0;
  EXPECT_EQ(packet->size(), 3);
  for (auto d : data)
  {
    EXPECT_EQ(d, packet->data.at(idx++));
  }

  // Serialize packet
  std::pair<BaseNetworkPacket::BaseNetworkPacketType, std::string> spacket;
  ASSERT_NO_THROW(spacket = packet->serialize());

  // Deserialize packet
  BaseNetworkPacket::UPtr deserBasePacket;
  ASSERT_EQ(spacket.first, BaseNetworkPacket::BaseNetworkPacketType::BNPT_NETWORK_PACKET);
  ASSERT_NO_THROW(deserBasePacket = deserializeNetworkPacket(spacket.second));

  NetworkPacket<float>::UPtr deserNetworkPacket;
  ASSERT_NO_THROW(deserNetworkPacket = castBaseToNetworkPacket<float>(std::move(deserBasePacket)));
  ASSERT_TRUE(!!deserNetworkPacket);

  // Check deserialized packet size and data
  idx = 0;
  EXPECT_EQ(deserNetworkPacket->size(), 3);
  for (auto d : data)
  {
    EXPECT_EQ(d, deserNetworkPacket->data.at(idx++));
  }
}  // Float_DeSerialize_NoThrow

TEST(NetworkPacket, Int_DeSerialize_NoThrow) {
  using namespace optilab;
  using namespace optnet;

  const std::vector<int> data = {1, 2, 3};

  NetworkPacket<int>::UPtr packet;
  ASSERT_NO_THROW(packet = NetworkPacket<int>::UPtr(new NetworkPacket<int>(data)));

  // Check packet size and data
  int idx = 0;
  EXPECT_EQ(packet->size(), 3);
  for (auto d : data)
  {
    EXPECT_EQ(d, packet->data.at(idx++));
  }

  // Serialize packet
  std::pair<BaseNetworkPacket::BaseNetworkPacketType, std::string> spacket;
  ASSERT_NO_THROW(spacket = packet->serialize());

  // Deserialize packet
  BaseNetworkPacket::UPtr deserBasePacket;
  ASSERT_EQ(spacket.first, BaseNetworkPacket::BaseNetworkPacketType::BNPT_NETWORK_PACKET);
  ASSERT_NO_THROW(deserBasePacket = deserializeNetworkPacket(spacket.second));

  NetworkPacket<int>::UPtr deserNetworkPacket;
  ASSERT_NO_THROW(deserNetworkPacket = castBaseToNetworkPacket<int>(std::move(deserBasePacket)));
  ASSERT_TRUE(!!deserNetworkPacket);

  // Check deserialized packet size and data
  idx = 0;
  EXPECT_EQ(deserNetworkPacket->size(), 3);
  for (auto d : data)
  {
    EXPECT_EQ(d, deserNetworkPacket->data.at(idx++));
  }
}  // Int_DeSerialize_NoThrow

TEST(NetworkPacket, Uint_DeSerialize_NoThrow) {
  using namespace optilab;
  using namespace optnet;

  const std::vector<uint32_t> data = {1, 2, 3};

  NetworkPacket<uint32_t>::UPtr packet;
  ASSERT_NO_THROW(packet = NetworkPacket<uint32_t>::UPtr(new NetworkPacket<uint32_t>(data)));

  // Check packet size and data
  int idx = 0;
  EXPECT_EQ(packet->size(), 3);
  for (auto d : data)
  {
    EXPECT_EQ(d, packet->data.at(idx++));
  }

  // Serialize packet
  std::pair<BaseNetworkPacket::BaseNetworkPacketType, std::string> spacket;
  ASSERT_NO_THROW(spacket = packet->serialize());

  // Deserialize packet
  BaseNetworkPacket::UPtr deserBasePacket;
  ASSERT_EQ(spacket.first, BaseNetworkPacket::BaseNetworkPacketType::BNPT_NETWORK_PACKET);
  ASSERT_NO_THROW(deserBasePacket = deserializeNetworkPacket(spacket.second));

  NetworkPacket<uint32_t>::UPtr deserNetworkPacket;
  ASSERT_NO_THROW(deserNetworkPacket =
      castBaseToNetworkPacket<uint32_t>(std::move(deserBasePacket)));
  ASSERT_TRUE(!!deserNetworkPacket);

  // Check deserialized packet size and data
  idx = 0;
  EXPECT_EQ(deserNetworkPacket->size(), 3);
  for (auto d : data)
  {
    EXPECT_EQ(d, deserNetworkPacket->data.at(idx++));
  }
}  // Uint_DeSerialize_NoThrow

TEST(NetworkPacket, Bool_DeSerialize_NoThrow) {
  using namespace optilab;
  using namespace optnet;

  const std::vector<bool> data = {true, false, true};

  NetworkPacket<bool>::UPtr packet;
  ASSERT_NO_THROW(packet = NetworkPacket<bool>::UPtr(new NetworkPacket<bool>(data)));

  // Check packet size and data
  int idx = 0;
  EXPECT_EQ(packet->size(), 3);
  for (auto d : data)
  {
    EXPECT_EQ(d, packet->data.at(idx++));
  }

  // Serialize packet
  std::pair<BaseNetworkPacket::BaseNetworkPacketType, std::string> spacket;
  ASSERT_NO_THROW(spacket = packet->serialize());

  // Deserialize packet
  BaseNetworkPacket::UPtr deserBasePacket;
  ASSERT_EQ(spacket.first, BaseNetworkPacket::BaseNetworkPacketType::BNPT_NETWORK_PACKET);
  ASSERT_NO_THROW(deserBasePacket = deserializeNetworkPacket(spacket.second));

  NetworkPacket<bool>::UPtr deserNetworkPacket;
  ASSERT_NO_THROW(deserNetworkPacket =
      castBaseToNetworkPacket<bool>(std::move(deserBasePacket)));
  ASSERT_TRUE(!!deserNetworkPacket);

  // Check deserialized packet size and data
  idx = 0;
  EXPECT_EQ(deserNetworkPacket->size(), 3);
  for (auto d : data)
  {
    EXPECT_EQ(d, deserNetworkPacket->data.at(idx++));
  }
}  // Bool_DeSerialize_NoThrow

TEST(NetworkPacket, String_DeSerialize_NoThrow) {
  using namespace optilab;
  using namespace optnet;

  const std::vector<std::string> data = {"ab", "bc", "de"};

  NetworkPacket<std::string>::UPtr packet;
  ASSERT_NO_THROW(packet = NetworkPacket<std::string>::UPtr(new NetworkPacket<std::string>(data)));

  // Check packet size and data
  int idx = 0;
  EXPECT_EQ(packet->size(), 3);
  for (auto d : data)
  {
    EXPECT_EQ(d, packet->data.at(idx++));
  }

  // Serialize packet
  std::pair<BaseNetworkPacket::BaseNetworkPacketType, std::string> spacket;
  ASSERT_NO_THROW(spacket = packet->serialize());

  // Deserialize packet
  BaseNetworkPacket::UPtr deserBasePacket;
  ASSERT_EQ(spacket.first, BaseNetworkPacket::BaseNetworkPacketType::BNPT_NETWORK_PACKET);
  ASSERT_NO_THROW(deserBasePacket = deserializeNetworkPacket(spacket.second));

  NetworkPacket<std::string>::UPtr deserNetworkPacket;
  ASSERT_NO_THROW(deserNetworkPacket =
      castBaseToNetworkPacket<std::string>(std::move(deserBasePacket)));
  ASSERT_TRUE(!!deserNetworkPacket);

  // Check deserialized packet size and data
  idx = 0;
  EXPECT_EQ(deserNetworkPacket->size(), 3);
  for (auto d : data)
  {
    EXPECT_EQ(d, deserNetworkPacket->data.at(idx++));
  }
}  // String_DeSerialize_NoThrow

TEST(NetworkPacket, Bad_Cast_Base_To_Network_Packet_Throw) {
  using namespace optilab;
  using namespace optnet;

  const std::vector<int> data = {1, 2, 3};

  BaseNetworkPacket::UPtr packet;
  ASSERT_NO_THROW(packet = NetworkPacket<int>::UPtr(new NetworkPacket<int>(data)));
  EXPECT_THROW(castBaseToNetworkPacket<bool>(std::move(packet)), std::runtime_error);
}  // Bad_Cast_Base_To_Network_Packet_Throw

TEST(NetworkPacket, Cast_Null_Base_To_Network_Packet_NoThrow) {
  using namespace optilab;
  using namespace optnet;

  NetworkPacket<int>::UPtr castPacket;
  ASSERT_NO_THROW(castPacket = castBaseToNetworkPacket<int>(nullptr));
  EXPECT_FALSE(castPacket);
}  // Cast_Null_Base_To_Network_Packet_NoThrow
