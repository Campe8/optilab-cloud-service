#include <memory>     // for std::shared_ptr
#include <stdexcept>  // for std::invalid_argument
#include <string>

#include <gtest/gtest.h>

#include "optimizer_network/network_base_hub.hpp"
#include "optimizer_network/network_constants.hpp"

namespace {

const char TCP_NETWORK_PREFIX[] = "tcp://";

}  // namespace

namespace optilab {
namespace optnet {

class NetworkBaseHubTest : public NetworkBaseHub {
 public:
  using SPtr = std::shared_ptr<NetworkBaseHubTest>;

 public:
  NetworkBaseHubTest(const int inPort, const int outPort, const int broadcastInPort,
                     const int broadcastOutPort, const std::string& inAddr = std::string(),
                     const std::string& outAddr = std::string(),
                     const std::string& broadcastInAddr = std::string(),
                     const std::string& broadcastOutAddr = std::string())
      : NetworkBaseHub(inPort, outPort, broadcastInPort, broadcastOutPort, inAddr, outAddr,
                       broadcastInAddr, broadcastOutAddr) {
  }

  void run() override {
  }
  void synchNetwork() override {
  }

  std::string getNetworkInSocketAddr() {
    return NetworkBaseHub::getNetworkInSocketAddr();
  }
  std::string getNetworkOutSocketAddr() {
    return NetworkBaseHub::getNetworkOutSocketAddr();
  }
  std::string getNetworkInBroadcastSocketAddr() {
    return NetworkBaseHub::getNetworkInBroadcastSocketAddr();
  }
  std::string getNetworkOutBroadcastSocketAddr() {
    return NetworkBaseHub::getNetworkOutBroadcastSocketAddr();
  }
};

}  // namespace optnet
}  // namespace optilab

TEST(NetworkBaseHub, Constructor_Invalid_PortNumber_Throw) {
  using namespace optilab;
  using namespace optnet;

  EXPECT_THROW(
      NetworkBaseHubTest(networkconst::MIN_NETWORK_PORT_NUMBER - 1,
                         networkconst::MIN_NETWORK_PORT_NUMBER,
                         networkconst::MIN_NETWORK_PORT_NUMBER,
                         networkconst::MIN_NETWORK_PORT_NUMBER),
      std::invalid_argument);

  EXPECT_THROW(
      NetworkBaseHubTest(networkconst::MIN_NETWORK_PORT_NUMBER,
                         networkconst::MIN_NETWORK_PORT_NUMBER - 1,
                         networkconst::MIN_NETWORK_PORT_NUMBER,
                         networkconst::MIN_NETWORK_PORT_NUMBER),
      std::invalid_argument);

  EXPECT_THROW(
      NetworkBaseHubTest(networkconst::MIN_NETWORK_PORT_NUMBER,
                         networkconst::MIN_NETWORK_PORT_NUMBER,
                         networkconst::MIN_NETWORK_PORT_NUMBER - 1,
                         networkconst::MIN_NETWORK_PORT_NUMBER),
      std::invalid_argument);

  EXPECT_THROW(
      NetworkBaseHubTest(networkconst::MIN_NETWORK_PORT_NUMBER,
                         networkconst::MIN_NETWORK_PORT_NUMBER,
                         networkconst::MIN_NETWORK_PORT_NUMBER,
                         networkconst::MIN_NETWORK_PORT_NUMBER - 1),
      std::invalid_argument);

  EXPECT_THROW(
      NetworkBaseHubTest(networkconst::MAX_NETWORK_PORT_NUMBER + 1,
                         networkconst::MAX_NETWORK_PORT_NUMBER,
                         networkconst::MAX_NETWORK_PORT_NUMBER,
                         networkconst::MAX_NETWORK_PORT_NUMBER),
      std::invalid_argument);

  EXPECT_THROW(
      NetworkBaseHubTest(networkconst::MAX_NETWORK_PORT_NUMBER,
                         networkconst::MAX_NETWORK_PORT_NUMBER + 1,
                         networkconst::MAX_NETWORK_PORT_NUMBER,
                         networkconst::MAX_NETWORK_PORT_NUMBER),
      std::invalid_argument);

  EXPECT_THROW(
      NetworkBaseHubTest(networkconst::MAX_NETWORK_PORT_NUMBER,
                         networkconst::MAX_NETWORK_PORT_NUMBER,
                         networkconst::MAX_NETWORK_PORT_NUMBER + 1,
                         networkconst::MAX_NETWORK_PORT_NUMBER),
      std::invalid_argument);

  EXPECT_THROW(
      NetworkBaseHubTest(networkconst::MAX_NETWORK_PORT_NUMBER,
                         networkconst::MAX_NETWORK_PORT_NUMBER,
                         networkconst::MAX_NETWORK_PORT_NUMBER,
                         networkconst::MAX_NETWORK_PORT_NUMBER + 1),
      std::invalid_argument);
}  // Constructor_Invalid_PortNumber_Throw

TEST(NetworkBaseHub, Constructor_Valid_PortNumber_NoThrow) {
  using namespace optilab;
  using namespace optnet;

  EXPECT_NO_THROW(
      NetworkBaseHubTest(networkconst::MIN_NETWORK_PORT_NUMBER,
                         networkconst::MIN_NETWORK_PORT_NUMBER,
                         networkconst::MIN_NETWORK_PORT_NUMBER,
                         networkconst::MIN_NETWORK_PORT_NUMBER));

}  // Constructor_Valid_PortNumber_NoThrow

TEST(NetworkBaseHub, Valid_Socket_Addr) {
  using namespace optilab;
  using namespace optnet;

  NetworkBaseHubTest::SPtr hub;
  ASSERT_NO_THROW(hub =
      std::make_shared<NetworkBaseHubTest>(networkconst::MIN_NETWORK_PORT_NUMBER + 0,
                                           networkconst::MIN_NETWORK_PORT_NUMBER + 1,
                                           networkconst::MIN_NETWORK_PORT_NUMBER + 2,
                                           networkconst::MIN_NETWORK_PORT_NUMBER + 3));
  ASSERT_TRUE(!!hub);
  EXPECT_FALSE(hub->getNetworkInSocketAddr().empty());
  EXPECT_FALSE(hub->getNetworkOutSocketAddr().empty());
  EXPECT_FALSE(hub->getNetworkInBroadcastSocketAddr().empty());
  EXPECT_FALSE(hub->getNetworkOutBroadcastSocketAddr().empty());

  // Check that the addresses contain the TCP prefix
  EXPECT_NE(hub->getNetworkInSocketAddr().find(std::string(TCP_NETWORK_PREFIX)), std::string::npos);
  EXPECT_NE(hub->getNetworkOutSocketAddr().find(std::string(TCP_NETWORK_PREFIX)),
            std::string::npos);
  EXPECT_NE(hub->getNetworkInBroadcastSocketAddr().find(std::string(TCP_NETWORK_PREFIX)),
            std::string::npos);
  EXPECT_NE(hub->getNetworkOutBroadcastSocketAddr().find(std::string(TCP_NETWORK_PREFIX)),
            std::string::npos);

  // Check that the addresses contain the right port number
  EXPECT_NE(
      hub->getNetworkInSocketAddr().find(std::to_string(networkconst::MIN_NETWORK_PORT_NUMBER + 0)),
      std::string::npos);
  EXPECT_NE(hub->getNetworkOutSocketAddr().find(
      std::to_string(networkconst::MIN_NETWORK_PORT_NUMBER + 1)), std::string::npos);
  EXPECT_NE(hub->getNetworkInBroadcastSocketAddr().find(
      std::to_string(networkconst::MIN_NETWORK_PORT_NUMBER + 2)), std::string::npos);
  EXPECT_NE(hub->getNetworkOutBroadcastSocketAddr().find(
      std::to_string(networkconst::MIN_NETWORK_PORT_NUMBER + 3)), std::string::npos);
}  // Valid_Socket_Addr
