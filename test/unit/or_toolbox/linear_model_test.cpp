#include <memory>     // for std::make_shared
#include <stdexcept>  // for std::invalid_argument
#include <string>

#include <gtest/gtest.h>

#include "or_mocks.hpp"

namespace {
constexpr int kVarIdx{1};
constexpr double kVarLB{-10.0};
constexpr double kVarUB{+10.0};
constexpr bool kVarIsInt{false};
constexpr char kVarName[] = "var";
}  // namespace

class LinearModelFixture : public testing::Test {
 protected:
  void SetUp() override
  {
    using namespace optilab;
    using namespace toolbox;

    // Instantiate a new solver
    pSolver.reset(new MockMPSolver());
  }

  optilab::toolbox::MPVariable::SPtr getVariable(
          int index = kVarIdx,
          double lb = kVarLB,
          double ub = kVarUB,
          bool isInteger = kVarIsInt,
          const std::string& name = std::string(kVarName))
  {
    return std::make_shared<optilab::toolbox::MockMPVariable>(
            index,
            lb,
            ub,
            isInteger,
            name,
            pSolver->getInterface());
  }

 private:
  std::unique_ptr<optilab::toolbox::MockMPSolver> pSolver;
};

TEST_F(LinearModelFixture, Variable_Constructor_Destructor_No_Throw)
{
  using namespace optilab;
  using namespace toolbox;

  MPVariable::SPtr variable;
  EXPECT_NO_THROW(variable = getVariable());
  EXPECT_EQ(variable->getIndex(), kVarIdx);
  EXPECT_FALSE(variable->isInteger());
  EXPECT_DOUBLE_EQ(variable->lowerBound(), kVarLB);
  EXPECT_DOUBLE_EQ(variable->upperBound(), kVarUB);
  EXPECT_DOUBLE_EQ(variable->origLowerBound(), kVarLB);
  EXPECT_DOUBLE_EQ(variable->origUpperBound(), kVarUB);
  EXPECT_EQ(variable->branchingPriority(), 0);
  EXPECT_DOUBLE_EQ(variable->getSolutionValue(), 0.0);
  EXPECT_EQ(variable->basisStatus(), lmcommon::BasisStatus::FREE);
  EXPECT_NO_THROW(variable.reset());
}  // Variable_Constructor_Destructor_No_Throw
