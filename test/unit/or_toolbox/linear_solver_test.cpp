#include <memory>     // for std::make_shared
#include <stdexcept>  // for std::invalid_argument
#include <string>

#include <gtest/gtest.h>

#include "or_toolbox/linear_model_common.hpp"
#include "or_toolbox/linear_solver.hpp"

namespace {
constexpr char kSolverName[] = "solver";
}  // namespace

TEST(LinearSolver, Constructor_Destructor_NoThrow)
{
  using namespace optilab;
  using namespace toolbox;

  MPSolver::SPtr solver;
  EXPECT_NO_THROW(solver = std::make_shared<MPSolver>(
          kSolverName, lmcommon::OptimizationSolverPackageType::OSPT_SCIP));
  EXPECT_NO_THROW(solver.reset());
}  // Constructor_Destructor_NoThrow

TEST(LinearSolver, Solve_NoThrow)
{
  using namespace optilab;
  using namespace toolbox;

  MPSolver::SPtr solver;
  EXPECT_NO_THROW(solver = std::make_shared<MPSolver>(
          kSolverName, lmcommon::OptimizationSolverPackageType::OSPT_SCIP));

  // x, y, [0, +inf)
  std::shared_ptr<MPVariable> x, y;
  ASSERT_NO_THROW(x = solver->buildIntVar(0.0, MPSolver::infinity(), "x"));
  ASSERT_NO_THROW(y = solver->buildIntVar(0.0, MPSolver::infinity(), "y"));

  // x + 7 * y <= 17.5
  std::shared_ptr<MPConstraint> c1;
  ASSERT_NO_THROW(c1 = solver->buildRowConstraint(-MPSolver::infinity(), 17.5, "c1"));
  EXPECT_NO_THROW(c1->setCoefficient(x, 1.0));
  EXPECT_NO_THROW(c1->setCoefficient(y, 7.0));

  // x <= 3.5
  std::shared_ptr<MPConstraint> c2;
  ASSERT_NO_THROW(c2 = solver->buildRowConstraint(-MPSolver::infinity(), 3.5, "c2"));
  EXPECT_NO_THROW(c2->setCoefficient(x, 1.0));

  // Maximize x + 10 * y
  auto obj = solver->getObjectivePtr();
  ASSERT_TRUE(obj);
  obj->setCoefficient(x, 1.0);
  obj->setCoefficient(y, 10.0);
  obj->setMaximization();

  lmcommon::ResultStatus result{lmcommon::ResultStatus::NOT_SOLVED};
  EXPECT_NO_THROW(result = solver->solve());
  EXPECT_EQ(result, lmcommon::ResultStatus::OPTIMAL);

  // x = 3
  // y = 2
  // Obj = 23
  EXPECT_DOUBLE_EQ(x->getSolutionValue(), 3.0);
  EXPECT_DOUBLE_EQ(y->getSolutionValue(), 2.0);
  EXPECT_DOUBLE_EQ(obj->getValue(), 23.0);
}  // Solve_NoThrow
