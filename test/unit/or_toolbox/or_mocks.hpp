#pragma once

#include <memory>   // for std::shared_ptr

// #include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "or_toolbox/linear_model.hpp"
#include "or_toolbox/linear_model_common.hpp"
#include "or_toolbox/linear_solver.hpp"

namespace {
const char kSolverName[] = "SolverName";
}

namespace optilab {
namespace  toolbox {

class MockMPSolver : public MPSolver {
 public:
  using SPtr = std::shared_ptr<MockMPSolver>;

 public:
  MockMPSolver(lmcommon::OptimizationSolverPackageType solverType =
          lmcommon::OptimizationSolverPackageType::OSPT_SCIP)
 : MPSolver(kSolverName, solverType)
 {
 }

  /// Returns internal interface
  inline MPSolverInterface* getInterface() const noexcept { return MPSolver::getInterface(); }
};

class MockMPVariable : public MPVariable {
 public:
  using SPtr = std::shared_ptr<MockMPVariable>;

 public:
  MockMPVariable(int index, double lb, double ub, bool isInteger, const std::string& name,
                 MPSolverInterface* const solverInterface)
 : MPVariable(index, lb, ub, isInteger, name, solverInterface)
 {
 }
};

}  // namespace toolbox
}  // namespace optilab
